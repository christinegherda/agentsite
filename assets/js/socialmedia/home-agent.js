
jQuery(document).ready(function($){

    $(".alert-flash").delay(6000) .fadeOut(2000);

    //social-post button
    $('#cmdPost').click(
        function(){
            if($('#post_facebook').val() == 0 && $('#post_twitter').val() == 0 && $('#post_linkedin').val() == 0 && $('#post_googleplus').val() == 0){
            $('#post_select_sm_status').removeClass('hide');
            $('#post_select_sm_status').html('Please select at least 1 social media.');
            } else {
            $('#socialPost').submit();
            }
        }
    );

    // $('button.btn-preview').click(function() {
    //      $('.preview-text-post').text($('#post-area').val());
    // });

    $('button.btn-save-modal').on('click', function() {
        var $this = $(this);
        $this.button('loading');
        setTimeout(function() {
            $this.button('reset');
        }, 8000);
    });

        $("#propertySelected").change(function(){

             var data_type = $("#propertySelected option:selected").data("type");

             if(data_type === "spw_property"){

                 var url = base_url+"agent_social_media/get_property_activate_data";

             } else {

                var url = base_url+"agent_social_media/get_property_listing_data";
             }

             var selectedProp = $("#propertySelected option:selected").val();

            $.ajax({
               type: 'POST',
                url: url,
                data : { id: selectedProp },  
               //data : postData,           
                dataType: 'json',
                success: function(data) {
                    console.log(data);

                    if( data.success )
                    {
                          
                        //$("#post-area").val(data.unparseAddress);
                        $("#link-area").html(data.site_url); 
                        $("#photo-area").html(data.primary_photo);
                        $("#title-area").html(data.Address);

                    }
                },
                error: function(e) {
                   console.log(e);
                }
            });
        });

        $("#postSelected").change(function(){

             var data_type = $("#propertySelected option:selected").data("type");

            if(data_type === "spw_property"){

                 var url = base_url+"agent_social_media/get_property_activate_data";

             } else {

                var url = base_url+"agent_social_media/get_property_listing_data";
             }

            // var url = base_url+"agent_social_media/get_property_activate_data"; 
             var selectedProp = $("#propertySelected option:selected").val();
             var selectedOpt = $("#postSelected option:selected").val();

            //console.log(selectedProp);
            $.ajax({
               type: 'POST',
                url: url,
                data : { id: selectedProp, select: selectedOpt },            
                dataType: 'json',
                success: function(data) {
                    //console.log(data);

                    // get the current date 
                    var d = new Date();
                    var month = d.getMonth()+1;
                    var day = d.getDate();
                    var dateToday = d.getFullYear() + '/' +
                        ((''+month).length<2 ? '0' : '') + month + '/' +
                        ((''+day).length<2 ? '0' : '') + day;
  

                    if( data.success )
                    { 
                        var price = numeral(data.price).format('0,0.00');
                        var price_previous = numeral(data.price_previous).format('0,0.00');

                        if(data_type === "spw_property"){

                            $("#selected-option1").html('Check out my new listing at '+data.unparseAddress+'! ');
                            $("#selected-option2").html('Come to my open house at '+data.unparseAddress+' on '+dateToday+'! ');
                            $("#selected-option3").html('Price Reduction at '+data.unparseAddress+' was $'+price_previous+' now only $'+price+' ');
                            $("#selected-option4").html('Sold '+data.unparseAddress+' in [days] at [x%] of $'+price+' '); 

                         } else {

                            $("#selected-option1").html('Check out my new listing at '+data.address+'! ');
                            $("#selected-option2").html('Come to my open house at '+data.address+' on '+dateToday+'! ');
                            $("#selected-option3").html('Price Reduction at '+data.address+' was $'+price_previous+' now only $'+price+' ');
                            $("#selected-option4").html('Sold '+data.address+' in [days] at [x%] of $'+price+' '); 
                         }

                    }
                },
                error: function(e) {
                   console.log(e);
                }
            });
        });

         $("#btn-preview-post").click(function(){

             var data_type = $("#propertySelected option:selected").data("type");

            if(data_type === "spw_property"){

                 var url = base_url+"agent_social_media/get_property_activate_data";

             } else {

                var url = base_url+"agent_social_media/get_property_listing_data";
             }

             var selectedProp = $("#propertySelected option:selected").val();
            $.ajax({
               type: 'POST',
                url: url,
                data : { id: selectedProp },            
                dataType: 'json',
                success: function(data) {
                    //console.log(data);

                    if( data.success )
                    { 
                        var full_desc = data.site_description;
                        var limited_desc = full_desc.substring(0,180);

                         $('.preview-text-post').text($('#post-area').val());
                         $('#preview-img').html('<img src= '+data.primary_photo+' />');
                         $(".property-description").html(limited_desc+'..');
                         $(".property-address").html('<strong>'+data.address+'</strong>');
                         $(".property-broker").html('Listed by: '+data.agent_name+' , Phone: '+data.agent_phone+' ');
                         $('.property-url').html('<a href='+data.site_url+'>'+data.site_url+'</a>');
                    }
                },
                error: function(e) {
                   console.log(e);
                }
            });
        });

        //disable second select option when option 1 is not selected
        $('#btn-preview-post').attr('disabled', true);

         $("#propertySelected").on("change",function(){
            if(this.selectedIndex != 0){
                $("#postSelected").removeAttr("disabled");
                $("#btn-preview-post").removeAttr("disabled"); 
            }
        });

    });
    



