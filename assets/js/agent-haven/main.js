function testFunction(a) {
    console.log(a)
}
$('.dropdown-menu').click(function(e) {
    e.stopPropagation();
});
$( ".more-hasdropdown" ).click(
  function() {
      if ($(".more-isdropdown").is( ":hidden" ) ) {
       $(".more-isdropdown").show();
      } else {
       $(".more-isdropdown").hide();
      }
  }
);
$(".show-more-link").click(function() {
  $(".more-isdropdown").addClass("open");
  $(this).fadeOut();
});

$(".show-less-link").click(function() {
  $(".more-isdropdown").removeClass("open");
  $(".show-more-link").fadeIn();
  $(".more-isdropdown").scrollTop(0);
});
// ========= MODAL LOGIN/SIGNUP ======= //
$(".save_search_modalogin").click(function() {
    $(".content-signup").show();
    $(".content-signin").hide();
    $(".content-reset").hide();
});
$(".modalogin").click(function() {
    $(".content-signin").show();
    $(".content-signup").hide();
    $(".content-reset").hide();
});
$(".modalsignup").click(function() {
    $(".content-signup").show();
    $(".content-signin").hide();
    $(".content-reset").hide();
});
$(".modalreset").click(function() {
    $(".content-reset").show();
    $(".content-signup").hide();
    $(".content-signin").hide();
});

$(".leads-login").click(function() {
    $(".content-leads-login").show();
    $(".content-leads-signup").hide();
    $(".content-leads-reset").hide();
});
$(".leads-signup").click(function() {
    $(".content-leads-signup").show();
    $(".content-leads-login").hide();
    $(".content-leads-reset").hide();
});

$(".leads-resetpass").click(function() {
    $(".content-leads-reset").show();
    $(".content-leads-login").hide();
    $(".content-leads-signup").hide();
});

$(".sign-in-message").delegate(".failed_login_reset", "click", function() {
    $(".content-reset").show();
    $(".content-signup").hide();
    $(".content-signin").hide();
});


$(document).ready(function() {

    if(leads_config_set==true) {

        // pop up sign up modal when someone lands on website

        if(!firstLaunch)  {
            if(leads_config.viewSite==1) {
                if(leads_config.allowSkip==1) {
                    $('#capture-leads-login').modal();
                    $('.capture-leads-login-btn').show();
                } else {
                    $('#capture-leads-login').modal({
                        backdrop: 'static',
                        keyboard: false
                    });
                }
            }
        }

        // before view search results (quick search only), display login modal
        if(leads_config.searchSubmit==1) {

            /*$("#basic_search_form .submit-button").on("click", function(e) {
                e.preventDefault();
                if(leads_config.allowSkip==1) {
                    $("#capture-leads-login").modal();
                    $(".capture-leads-login-btn").show();
                } else {
                    $("#capture-leads-login").modal({
                        backdrop: 'static',
                        keyboard: false
                    });
                }
            });
            $("input[name='Search']").on("keypress", function(e) {
                if(e.keyCode == 13) {
                    if(leads_config.allowSkip==1) {
                        $("#capture-leads-login").modal();
                        $(".capture-leads-login-btn").show();
                    } else {
                        $("#capture-leads-login").modal({
                            backdrop: 'static',
                            keyboard: false
                        });
                    }
                }
            });*/

            $('#basic_search_form').on('submit', function(e) {

                e.preventDefault();

                if(leads_config.allowSkip==1) {
                    $('#capture-leads-login').modal();
                    $('.capture-leads-login-btn').show();
                } else {
                    $('#capture-leads-login').modal({
                        backdrop: 'static',
                        keyboard: false
                    });
                }

            });
        }

        /**** Start of Search Count ****/

        if(leads_config.countSearchPerf==1) {

            if(myStorage.getItem('search_count') !== null) {

                if(parseInt(myStorage.getItem('search_count')) >= parseInt(myStorage.getItem('current_search_limit'))) {

                    if(leads_config.allowSkip==1) {
                        $('#capture-leads-login').modal();
                        $('.capture-leads-login-btn').show();
                    } else {
                        $('#capture-leads-login').modal({
                            backdrop: 'static',
                            keyboard: false
                        });
                    }

                    return true;
                }

            }

            if(myStorage.getItem('current_search_limit')===null) { //if current search limit is not yet set
                myStorage.setItem('current_search_limit', parseInt(leads_config.search_count));
                myStorage.removeItem('search_count');
            }

            if(parseInt(myStorage.getItem('current_search_limit')) != parseInt(leads_config.search_count)) { //if search limit is updated in leads dashboard
                myStorage.setItem('current_search_limit', parseInt(leads_config.search_count));
                myStorage.removeItem('search_count');
            }

            var search_count = (myStorage.getItem('search_count')) ? myStorage.getItem('search_count') : 0;

            $('.sbt-search-from, #basic_search_form').on('submit', function(e) {

                search_count++;
                myStorage.setItem('search_count', parseInt(search_count));

                if(parseInt(myStorage.getItem('search_count')) >= parseInt(myStorage.getItem('current_search_limit'))) {

                    if(leads_config.allowSkip==1) {
                        $('#capture-leads-login').modal();
                        $('.capture-leads-login-btn').show();
                    } else {
                        $('#capture-leads-login').modal({
                            backdrop: 'static',
                            keyboard: false
                        });
                    }

                }

            });

        } else {
            myStorage.removeItem("search_count");
            myStorage.removeItem("current_search_limit");
        }

        /**** End Search Count ****/


        /**** Start of View Listing Count ****/

        if(myStorage.getItem("current_view_prop_limit")===null) {
            myStorage.setItem("current_view_prop_limit", parseInt(leads_config.listing_view_count));
            myStorage.removeItem("view_listing_count");
        }

        if(parseInt(myStorage.getItem("current_view_prop_limit")) != parseInt(leads_config.listing_view_count)) {
            myStorage.setItem("current_view_prop_limit", parseInt(leads_config.listing_view_count));
            myStorage.removeItem("view_listing_count");
        }

        if(current_page == "property-details" || current_page == "other-property-details") {

            if(leads_config.viewLIsting==1) {

                if(leads_config.allowSkip==1) {
                    $("#capture-leads-login").modal();
                    $(".capture-leads-login-btn").show();
                } else {
                    $("#capture-leads-login").modal({
                        backdrop: 'static',
                        keyboard: false
                    });
                }

            }

            if(leads_config.countListingView==1) {
                var view_listing_count = (myStorage.getItem("view_listing_count")) ? myStorage.getItem("view_listing_count") : 0;
                myStorage.setItem("view_listing_count", parseInt(view_listing_count)+1);
            }

        }

        if(leads_config.countListingView==1) {
            if(parseInt(myStorage.getItem("view_listing_count")) >= parseInt(myStorage.getItem("current_view_prop_limit"))) {
                if(leads_config.allowSkip==1) {
                    $("#capture-leads-login").modal();
                    $(".capture-leads-login-btn").show();
                } else {
                    $("#capture-leads-login").modal({
                        backdrop: 'static',
                        keyboard: false
                    });
                }
            }
        } else {
            myStorage.removeItem("view_listing_count");
            myStorage.removeItem("current_view_prop_limit");
        }
        /**** End of View Listing Count ****/

        /**** Start of Minutes Count ****/
        if(leads_config.minutesPass==1) {

            if(myStorage.getItem("current_time_lapse")===null) {
                myStorage.setItem("current_time_lapse", (parseInt(leads_config.minute_capture) * 60000));
                myStorage.removeItem("min_countdown");
            }

            if(parseInt(myStorage.getItem("current_time_lapse")) != (parseInt(leads_config.minute_capture) * 60000)) {
                myStorage.setItem("current_time_lapse", (parseInt(leads_config.minute_capture) * 60000));
                myStorage.removeItem("min_countdown");
            }

            var min_countdown = (myStorage.getItem("min_countdown")) ? myStorage.getItem("min_countdown") : parseInt(myStorage.getItem("current_time_lapse"));

            var x = setInterval(function() {

                min_countdown -= 1000;
                myStorage.setItem("min_countdown", parseInt(min_countdown));
                console.log(min_countdown);
                
                if(min_countdown <= 0) {
                    clearInterval(x);
                    if(leads_config.allowSkip==1) {
                        $("#capture-leads-login").modal();
                        $(".capture-leads-login-btn").show();
                    } else {
                        $("#capture-leads-login").modal({
                            backdrop: 'static',
                            keyboard: false
                        });
                    }
                }
                
            }, 1000);

        } else {
            myStorage.removeItem("min_countdown");
            myStorage.removeItem("current_time_lapse");
        }
        /**** End of Minutes Count ****/

        console.log(myStorage)

    } else {
        myStorage.clear();
    }
});

$(".customer_logout_btn").on("click", function(e) {

    e.preventDefault();

    $.ajax({
        url: base_url+'logout',
        type: 'POST',
        dataType: 'json',
        success: function(data) {
            console.log(data);
            myStorage.clear();
            window.location.href = data.redirect;
        }
    });
});

// ========= MODAL LOGIN/SIGNUP ======= //


$(".request_info").on("click", function() {
    $(".listing_id").val($(this).attr('data-listingId'));
    $(".property_type").val($(this).attr('data-propertyType'));
    $("textarea[name='comments']").val("Please send me more information on "+$(this).data("address"));
});

$(".featured_listing").on("click", function() {
    $(".listing_id").val($(this).attr("data-listingId"));
    $(".property_type").val($(this).attr("data-propertyType"));
});

 //Submit Testimonials
$(".submit-review").on("click", function() {
    $(".redirect_customer").val($(this).attr("data-redirect-customer"));
});

//Format the phone and mobile number
$(".phone_number").on("keypress", function(e) {

    var charCode = (typeof e.which == "number") ? e.which : e.keyCode;

    //Allow backspace and spaces only
    if (!charCode || charCode==8 || charCode == 32) {
        return;
    }

    var typedChar = String.fromCharCode(charCode);

    // Allow numeric characters
    if (/\d/.test(typedChar)) {
        return;
    }

    // Allow the minus sign (-) (- sign has no ascii value so i have to use this)
    if (typedChar == "-") {
        return;
    }

    // In all other cases, suppress the event
    return false;

});

$(".signupForm").on("submit", function(e) {
    e.preventDefault();

    var listing_id      = $(this).find("input[name='listing_id']").val();
    var prop_name       = $(this).find("input[name='property_name']").val();
    var phone_number    = $(this).find("input[name='phone_signup']").val();
    var phone_length    = phone_number.replace(/[^0-9]/g,"").length;

    $dataString = $(this).serialize();

    $.ajax({
        type: 'POST',
        url: $(this).attr('action'),
        data: $dataString,
        dataType: 'json',
        beforeSend: function() {
            console.log('sending');
            $(".signup_sbmt_btn").prop("disabled",true);
            $(".signup_sbmt_btn").text("Loading...");
        },
        success: function(data) {

            if(data.success) {
                
                myStorage.clear();

                if($(".submission_type").val()=="save_search") {

                    $save_search_data = $("#save-search-btn").attr("data-save-search");

                    $.ajax({
                        url: base_url + "home/home/save_search_properties",
                        type: "GET",
                        data: $save_search_data,
                        dataType: "json",
                        beforeSend: function() {
                            console.log("sending save search data");
                        },
                        success: function($save) {
                            console.log($save);
                            $("#save-search-btn").addClass("isSaved");
                            if(!/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
                                $("#save-search-btn").html("<i class='fa fa-save'></i> Saved Search");
                            } else {
                                $("#save-search-btn").html("<i class='fa fa-save'></i>");
                            }
                            
                            $to_redirect = "search-results?"+$save.redirect;
                            window.location.href = $to_redirect;
                            return true;
                        }

                    });

                } else if($(".submission_type").val()=="testimonial") {

                    $rate = $(".new").rateYo();
                    $rating = $rate.rateYo("rating");
                    $("#rating").val($rating);

                    $.ajax({
                        type: 'POST',
                        url: $("#review_form").attr("action"),
                        data: $("#review_form").serialize(),
                        dataType: 'json',
                        beforeSend: function(data) {
                            $("#submit_review").prop("disabled", true);
                        },
                        success: function(data) {
                            $("#submit_review").prop("disabled", false);
                        }
                    });

                    window.location.href = data.redirect;

                } else if($(".submission_type").val()=="save-favorate" || $(".submission_type").val()=="save-search-favorate") {

                    var href = base_url + "home/home/save_favorates_properties";
                    var c = href + "/" + listing_id;

                    $.ajax({
                        url: c,
                        type: "POST",
                        data: {
                            'property_name': prop_name
                        },
                        dataType: "json",
                        success: function(a) {
                            console.log(a);
                            if(a.success) {
                                console.log($("#isSaveFavorate_" + listing_id));
                                $("#isSaveFavorate_" + listing_id).parent().addClass("saveproperty");
                                $("#isSaveFavorate_" + listing_id).addClass("yesheart");
                                $("#isSaveFavorate_" + listing_id).html("<i class='fa fa-heart'></i>");
                                $("#isSavedFavorate_" + listing_id).html("<small>Saved</small>");
                            } 
                        }
                    });

                    if($(".submission_type").val()=="save-favorate")
                        window.location.reload(true);
                    else
                        window.location.href = base_url+"search-results?"+$("#save-search-btn").attr("data-save-search");

                    return true;

                } else {

                    window.location.href = data.redirect;
                    
                }
                
            } else {
                $(".signup_sbmt_btn").prop("disabled", false);
                $(".signup_sbmt_btn").text("Sign Up");
                $(".sign-up-message").html("<div class='alert alert-danger alert-dismissible' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>"+data.message+"</div>");
            }
        }

    });

});

$(".loginForm").on("submit", function(e) {

    e.preventDefault();

    $dataString = $(this).serialize();

    var listing_id  = $(this).find("input[name='listing_id']").val();
    var prop_name   = $(this).find("input[name='property_name']").val();

    $.ajax({
        type: 'POST',
        url: $(this).attr('action'),
        data: $dataString,
        dataType: 'json',
        beforeSend: function() {
            console.log('sending');
            $(".login_sbmt_btn").prop("disabled",true);
            $(".login_sbmt_btn").text("Loading...");
        },
        success: function(data) {

            if(data.success) {

                myStorage.clear();

                if($(".submission_type").val()=="save_search") {

                    $save_search_data = $("#save-search-btn").attr("data-save-search");

                    $.ajax({
                        url: base_url + "home/home/save_search_properties",
                        type: "GET",
                        data: $save_search_data,
                        dataType: "json",
                        beforeSend: function() {
                            console.log("sending save search data");
                        },
                        success: function($save) {
                            console.log($save);
                            $("#save-search-btn").addClass("isSaved");
                            if(!/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
                                $("#save-search-btn").html("<i class='fa fa-save'></i> Saved Search");
                            } else {
                                $("#save-search-btn").html("<i class='fa fa-save'></i>");
                            }

                            $to_redirect = "search-results?"+$save.redirect;
                            window.location.href = $to_redirect;

                            return true;
                        }

                    });

                } else if($(".submission_type").val()=="testimonial") {

                    $rate = $(".new").rateYo();
                    $rating = $rate.rateYo("rating");
                    $("#rating").val($rating);

                    $.ajax({
                        type: 'POST',
                        url: $("#review_form").attr("action"),
                        data: $("#review_form").serialize(),
                        dataType: 'json',
                        beforeSend: function(data) {
                            $("#submit_review").prop("disabled", true);
                        },
                        success: function(data) {
                            $("#submit_review").prop("disabled", false);
                        }
                    });

                    window.location.href = data.redirect;

                } else if($(".submission_type").val()=="save-favorate" || $(".submission_type").val()=="save-search-favorate") {

                    var href = base_url + "home/home/save_favorates_properties";
                    var c = href + "/" + listing_id;

                    $.ajax({
                        url: c,
                        type: "POST",
                        data: {
                            'property_name': prop_name
                        },
                        dataType: "json",
                        success: function(a) {
                            console.log(a);
                            if(a.success) {
                                console.log($("#isSaveFavorate_" + listing_id));
                                $("#isSaveFavorate_" + listing_id).parent().addClass("saveproperty");
                                $("#isSaveFavorate_" + listing_id).addClass("yesheart");
                                $("#isSaveFavorate_" + listing_id).html("<i class='fa fa-heart'></i>");
                                $("#isSavedFavorate_" + listing_id).html("<small>Saved</small>");
                            } 
                        }
                    });

                    if($(".submission_type").val()=="save-favorate")
                        window.location.reload(true);
                    else
                        window.location.href = base_url+"search-results?"+$("#save-search-btn").attr("data-save-search");

                    return true;

                } else {

                    window.location.href = data.redirect;
                    
                }
               
            } else {
                $(".login_sbmt_btn").prop("disabled", false);
                $(".login_sbmt_btn").text("Login to customer dashboard");
                $(".sign-in-message").html("<div class='alert alert-danger alert-dismissible' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>"+data.message+"</div>");
            }
        }
    });
});

$(".resetPhoneForm").on("submit", function(e) {
    e.preventDefault();

    $dataString = $(this).serialize();
    console.log($dataString);

    $.ajax({
        type: 'POST',
        url: $(this).attr('action'),
        data: $dataString,
        dataType: 'json',
        beforeSend: function() {
            console.log('sending');
        },
        success: function(data) {
            if(data.success) {
                console.log(data);
                $(".reset-message").html("<div class='alert alert-success alert-dismissible' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>"+data.message+"</div>");
            } else {
                $(".reset-message").html("<div class='alert alert-danger alert-dismissible' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>"+data.message+"</div>");
            }
        }
    });
});

function nFormatter(num) {
     if (num >= 1000000000) {
        return (num / 1000000000).toFixed(1).replace(/\.0$/, '') + 'B';
     }
     if (num >= 1000000) {
        return (num / 1000000).toFixed(1).replace(/\.0$/, '') + 'M';
     }
     if (num >= 1000) {
        return (num / 1000).toFixed(1).replace(/\.0$/, '') + 'K';
     }
     return num;
}

function setFieldValue(a, b) {
    $(document).ready(function() {
        "Any" === b ? ($("#" + a).val(""), $("#" + a + "_label").html(b)) : ($("#" + a).val(b), $("#" + a + "_label").html(b + "+"));
    });
}

function setBedValue(a, b) {
    $(document).ready(function() {
        "Any" === b ? ($("#" + a).val(""), $("#" + a + "_label").html(b)) : ($("#" + a).val(b), $("#" + a + "_label").html(b + "+"));
        $("input[name*='BedsTotal']").val(b);
    });
}

function setBedVal(a, b) {
    $(document).ready(function() {
        var b = $(".beds-mobile").val();
        $("input[name*='BedsTotal']").val(b);
    });
}

function setFieldTextValue(a, b) {
    $(document).ready(function() {
        "Any" === b ? ($("#" + a).val(""), $("#" + a + "_label").html(b)) : ($("#" + a).val(b), $("#" + a + "_label").html(b));
    });
}

function addCommas(a) {
    a += "", x = a.split("."), x1 = x[0], x2 = x.length > 1 ? "." + x[1] : "";
    for (var b = /(\d+)(\d{3})/; b.test(x1);) x1 = x1.replace(b, "$1,$2");
    return x1 + x2;
}

function calculate() {
    var a = $("#mortgageCalculator").serialize(),
        b = $("input[name='calculator_url']").val();
    $.ajax({
        url: b,
        type: "POST",
        data: a,
        dataType: "json",
        beforeSend: function() {
          console.log(a),
          console.log("sending data");
          //$("#morg_calc_btn").text("Calculating...")
        },
        success: function(a) {
          if(a.success) {
            $("#mortgage_msg").text("");
            $("#mortgage-result").html("<h4 class='total-mortgage'>Monthly: <strong>$" + a.monthly_payment + "</strong></h4>"), $("#morg_calc_btn").text("Calculate");
          } else {
            $("#mortgage_msg").html("<div class='alert alert-danger text-center'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Error: </strong> "+a.message+"</div>");
          }
        }
    })
}


function customer_fb_login() {
    var a = screen.width / 6,
        b = screen.height / 6;
    window.open(customer_fb_url, "targetWindow", "toolbar=no,titlebar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no,width=700,height=450,top=" + b + ",left=" + a)
}

function modal_fb_login() {
    var a = screen.width / 6,
        b = screen.height / 6;
    window.open(modal_fb_url, "targetWindow", "toolbar=no,titlebar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no,width=700,height=450,top=" + b + ",left=" + a)
}

$(document).ready(function() {
    $(".sold-property-container").owlCarousel({
        navigation : true,
        pagination : true,
        slideSpeed : 700,
        paginationSpeed : 400,
        navigationText: ["<i class='fa fa-chevron-left'></i> <span>prev</span>","<span>next</span> <i class='fa fa-chevron-right'></i>"],
        items : 4,
        itemsDesktop : [1199,3],
        itemsDesktopSmall : [979,3]
    });

    $("#saved-searches").owlCarousel({
        navigation : true,
        pagination : true,
        slideSpeed : 700,
        paginationSpeed : 400,
        navigationText: ["<i class='fa fa-chevron-left'></i> <span>prev</span>","<span>next</span> <i class='fa fa-chevron-right'></i>"],
        items : 6,
        itemsDesktop : [1199,3],
        itemsDesktopSmall : [979,3]
    });

    // $("#save-search-grid").owlCarousel({
    //       navigation : true, // Show next and prev buttons
    //       slideSpeed : 300,
    //       paginationSpeed : 400,
    //       pagination: true,
    //       responsive: true,
    //       autoPlay: true,
    //       navigationText: ["<i class='fa fa-chevron-left'></i> <span>prev</span>","<span>next</span> <i class='fa fa-chevron-right'></i>"]
    //       // singleItem:true

    // });
    //$(".owl-carousel-saved-search").owlCarousel();

    //property-detail
    var photosync1 = $("#photosync1");
    var photosync2 = $("#photosync2");

    photosync1.owlCarousel({
        singleItem : true,
        slideSpeed : 1000,
        pagination:false,
        afterAction : syncPosition,
        responsiveRefreshRate : 200,
    });

    photosync2.owlCarousel({
        items : 5,
        itemsDesktop      : [1199,10],
        itemsDesktopSmall     : [979,10],
        itemsTablet       : [768,8],
        itemsMobile       : [479,4],
        navigation: true,
        pagination:false,
        navigationText: ["<i class='fa fa-chevron-left'></i> <span>prev</span>","<span>next</span> <i class='fa fa-chevron-right'></i>"],
        responsiveRefreshRate : 100,
        afterInit : function(el){
          el.find(".owl-item").eq(0).addClass("synced");
        }
    });

    function syncPosition(el) {

        var current = this.currentItem;
        $("#photosync2")
            .find(".owl-item")
            .removeClass("synced")
            .eq(current)
            .addClass("synced")

        if($("#photosync2").data("owlCarousel") !== undefined) {
          center(current)
        }
    }

    $("#photosync2").on("click", ".owl-item", function(e){
        e.preventDefault();
        var number = $(this).data("owlItem");
        photosync1.trigger("owl.goTo",number);
    });

    function center(number) {
        var photosync2visible = photosync2.data("owlCarousel").owl.visibleItems;
        var num = number;
        var found = false;

        for(var i in photosync2visible){
            if(num === photosync2visible[i]){
                var found = true;
            }
        }

        if(found===false){
            if(num>photosync2visible[photosync2visible.length-1]){
                photosync2.trigger("owl.goTo", num - photosync2visible.length+2)
            } else {
                if(num - 1 === -1){
                    num = 0;
                }
                photosync2.trigger("owl.goTo", num);
            }
        } else if(num === photosync2visible[photosync2visible.length-1]){
            photosync2.trigger("owl.goTo", photosync2visible[1])
        } else if(num === photosync2visible[0]){
            photosync2.trigger("owl.goTo", num-1)
        }

    }

    $('.city-search').selectize({
        maxOptions:10,
        maxItems:5,
        openOnFocus:false,
        sortField: {
            field: 'text',
            direction: 'asc'
        }
    });

    function f(a) {
        $(a.target).prev(".panel-heading").find("i.indicator").toggleClass("glyphicon-chevron-up glyphicon-chevron-down")
    }

    $("#mortgageCalculator").on("submit", function(a) {
        calculate();
        a.preventDefault();
    });

    $("#sale_price").on("keypress keyup", function(event) {
        keymonitor(event);
        var price = $(this).val();
        $("#sale_price_label").html(addCommas(priceFormatter(price)));
    })

    $("#down_percent").on("keypress keyup", function(event) {
        keymonitor(event);
        $("#down_percent_label").html($(this).val());
    });

    $("#mortgage_interest_percent").on("keypress keyup", function(event) {
        keymonitor(event);
        $("#mortgage_interest_percent_label").html($(this).val());
    });

    $("#year_term").on("keypress keyup", function(event) {
        keymonitor(event);
        $("#year_term_label").html($(this).val());
    });

    function keymonitor(event) {

        event = (event) ? event : window.event;
        var charCode = (event.which) ? event.which : event.keyCode;

        if (!charCode || charCode==8 || charCode == 32) {
            return true;//event.preventDefault();
        }

        var typedChar = String.fromCharCode(charCode);
        // Allow numeric characters
        if (/\d/.test(typedChar)) {
            return true;
        }

        // Allow the decimal value
        if (typedChar == ".") {
            return true;
        }

        // In all other cases, suppress the event
        event.preventDefault();

    }

    function priceFormatter(price) {
        var price_parts = price.toString().split(".");
        price_parts[0] = price_parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        return price_parts;
    }

    //advance feature
    $('.defaultval-property-type').change(function() {
        var property_type_val = $(this).val();
        if(property_type_val == ''){
           $(".subtype-class").css("display","none");
           $(".set-property-sub").val("");
           return false;
        }
        $(".subtype-class").css("display","block");
        var property_type_code = this.selectedOptions[0].dataset.propertyclassCode;
        var codes = ["A","B","C","D","E","F"];
        codes.forEach(function(item){
            if(item == property_type_code){
                console.log(item);
                $("."+property_type_code).css("display","block");
                $("."+item).attr("name","PropertySubType");
            }else{
                $("."+item).css("display","none");
                $("."+item).attr("name"," ");
                $("."+item).val("");
            }
        });
    });

    var a = 0;
    $("#A").click(function() {
        $(this).is(":checked") ? ($("#type_a").css("display", "block"), a += 1) : ($("#type_a").css("display", "none"), $("input[name='a_type[]']").each(function() {
            $(this).prop("checked", !1)
        }), a -= 1)
    }), $("#B").click(function() {
        $(this).is(":checked") ? ($("#type_b").css("display", "block"), a += 1) : ($("#type_b").css("display", "none"), $("input[name='b_type[]']").each(function() {
            $(this).prop("checked", !1)
        }), a -= 1)
    }), $(window).scroll(function(a) {
        var b = $(this).scrollTop();
        b > 25 ? ($(".home-navbar").addClass("nav-lighten"), $(".main-nav").removeClass("nav-darken")) : ($(".home-navbar").removeClass("nav-lighten"), $(".main-nav").addClass("nav-darken")), b > 100 ? $(".scrolltop").fadeIn() : $(".scrolltop").fadeOut()
    }), $(".scrolltop").click(function() {
        return $("html, body").animate({
            scrollTop: 0
        }, 600), !1
    }), $("#C").click(function() {
        $(this).is(":checked") ? ($("#type_c").css("display", "block"), a += 1) : ($("#type_c").css("display", "none"), $("input[name='c_type[]']").each(function() {
            $(this).prop("checked", !1)
        }), a -= 1)
    }), $("#D").click(function() {
        $(this).is(":checked") ? ($("#type_d").css("display", "block"), a += 1) : ($("#type_d").css("display", "none"), $("input[name='d_type[]']").each(function() {
            $(this).prop("checked", !1)
        }), a -= 1)
    }), $("#E").click(function() {
        $(this).is(":checked") ? ($("#type_e").css("display", "block"), a += 1) : ($("#type_e").css("display", "none"), $("input[name='e_type[]']").each(function() {
            $(this).prop("checked", !1)
        }), a -= 1)
    }), $("#F").click(function() {
        $(this).is(":checked") ? ($("#type_f").css("display", "block"), a += 1) : ($("#type_f").css("display", "none"), $("input[name='f_type[]']").each(function() {
            $(this).prop("checked", !1)
        }), a -= 1)
    }), $("#A, #B, #C, #D, #E, #F").on("click", function() {
        a > 1 ? ($("#property_sub_type_container").css("display", "none"), $("input[name='a_type[]']").each(function() {
            $(this).prop("checked", !1)
        }), $("input[name='b_type[]']").each(function() {
            $(this).prop("checked", !1)
        }), $("input[name='c_type[]']").each(function() {
            $(this).prop("checked", !1)
        }), $("input[name='d_type[]']").each(function() {
            $(this).prop("checked", !1)
        }), $("input[name='e_type[]']").each(function() {
            $(this).prop("checked", !1)
        }), $("input[name='f_type[]']").each(function() {
            $(this).prop("checked", !1)
        })) : 0 == a ? $("#property_sub_type_container").css("display", "none") : $("#property_sub_type_container").css("display", "block")
    }), $("#more-property-types").click(function() {
        $(".show-more-property-types").toggle()
    }), $(".moreFiltersToggle").click(function() {
        $("#more-filters-area").toggle()
    }), $(".dropdown-toggle").click(function() {
        $("#more-filters-area").toggle(), "block" == $("#more-filters-area").css("display") && $("#more-filters-area").hide();
        $(".price-input li.dropdown").removeClass("open");
    });

    $(".price-input li a").click(function(e) {
        e.preventDefault();
        $(this).parent().toggleClass("open");
    });
    $(".dismiss-dropdown").click(function(e) {
        e.preventDefault();
        $(this).parent().parent().parent().removeClass("open");
    });

    $("#price_min").on("input", function(event) {
        $(this).val($(this).val().replace(/[^0-9\.]/g,''));
        // if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
            $(this).keyup(function() {
                var minval = $("#price_min").val()
                var countInt = this.value.length;
                if (countInt > 3) {
                    var min = nFormatter(minval);
                    var lastChar = min[min.length -1];
                    $(".min-price").html(min);
                } else {
                    $(".min-price").html(minval);
                };
            });
        // } 
    });
    $("#price_max").on("input", function(event) {
        $(this).val($(this).val().replace(/[^0-9\.]/g,''));
        // if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
            $(this).keyup(function() {
                var maxval = $("#price_max").val()
                var countInt = this.value.length;
                if (countInt > 3) {
                    var max = nFormatter(maxval);
                    $(".max-price").html(max)
                } else {
                    $(".max-price").html(maxval)
                };
            });
        // } 
    });

    // prevent type of letters
    $(".input-number").on("input", function() {
        $(this).val($(this).val().replace(/[^0-9\.]/g,''));
        if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });

    var price = $("#price_label").text();
    if (price != "Any") {
        $("#price_label").text(function(index, currentText) {
            return addCommas(currentText.substr(0, 16));
        });
    }
    $("#accordion1 .panel-heading").click(function() {
        if ($(this).siblings().hasClass("in")) {
            $(this).find(".panel-title i").removeClass("glyphicon-chevron-up");
            $(this).find(".panel-title i").addClass("glyphicon-chevron-down");
        } else {
            console.log("remove up");
            $(this).find(".panel-title i").addClass("glyphicon-chevron-up");
            $(this).find(".panel-title i").removeClass("glyphicon-chevron-down");
        };
    })
    $("#bs-example-navbar-collapse-1 .dropdown-menu a").click(function(a) {
        a.preventDefault()
    })
    var c = ($(".photo-gallery .col-md-2").length, $(".photo-gallery .col-md-2")),
        d = 0;
    c.each(function() {
        8 == ++d ? c.css("display", "block") : d > 8 && $(this).css("display", "none")
    }), $(".view-map-button").click(function(a) {
        $(".property-map").toggleClass("toggle-map"), $(".property-map").hasClass("toggle-map") ? $(".view-map-button").html("<i class='fa fa-map'></i> <span class='show-map-label'>Hide Map</span>") : $(".view-map-button").html("<i class='fa fa-map'></i> <span class='show-map-label'>Show Map</span>")
    }), $(".filter-search-button").click(function() {
        $(".filter-panel,.contact-panel-show").slideToggle("slow")
    }), $(".contact-agent-button").click(function() {
        $(".contact-agent").slideToggle("slow")
    }), $(".filter-link").click(function() {
        $(this);
        $(this).stop().next(".sub-filter-simple,.sub-filter,.price-filter").css("display", "block").parent().siblings().find(".sub-filter-simple,.sub-filter,.price-filter").css("display", "none")
    }), $(".sub-filter-simple li a").on("click", function() {
        var a = $(this),
            b = a.text();
        a.parents("li").find(".chosen-option").text(b), a.stop().parents(".sub-filter-simple").slideUp(150)
    }), $(".property-main-content").click(function() {
        $(this);
        $(".sub-filter-simple,.sub-filter,.price-filter").stop().css("display", "none")
    }), $("#min-price").on("change", function() {
        var a = $(this),
            b = a.val();
        console.log(b), a.parents("li").find(".chosen-min").text(b)
    }), $("#max-price").on("change", function() {
        var a = $(this),
            b = a.val();
        console.log(b), a.parents("li").find(".chosen-max").text(b)
    }), $(".btn-show-filter").click(function() {
        $(".advance-search-form .navbar").slideToggle(250), $(".advance-search-form .navbar").is(":visible") && $("#more-filters-area").hide()
    }), $(".home-search").on("focus", function() {
        var a = $(this);
        0 == a.val() ? $(".help-info").stop().show() : $(".help-info").stop().slideUp()
    }), $(".home-search").on("keypress", function() {
        var a = $(this);
        0 == a.val() ? $(".help-info").stop().show() : $(".help-info").stop().slideUp()
    }), $(".home-search").on("mouseleave", function() {
        $(".help-info").stop().slideUp()
    });
    var e = window.location;
    $('ul.main-nav a[href="' + e + '"]').parent().addClass("active"), $("ul.main-nav a").filter(function() {
        return this.href == e
    }).parent().addClass("active"), $("#accordion").on("hidden.bs.collapse", f), $("#accordion").on("shown.bs.collapse", f), $(".view-full").click(function() {
        $("li.view-full-spec").addClass("active"), $("li.view-full-details").removeClass("active")
    }), $(".advance-search").click(function() {
        $(".search-info").show().delay(2e3).fadeOut(2e3)
    }), $("a").tooltip(), $(".alert-flash").delay(3e3).fadeOut(2e3)


    // iframe width switcher
    $('.size-container ul li').on('click',function(){
        var $this = $(this);
        var $switcher = $('#iframe-switcher');
        var $iframeWrapper = $('.iframe-wrapper');

        if($this.is('#phone-portrait')){

            $iframeWrapper.css('height',1000+'px');
            $this.addClass('active').siblings().removeClass('active');
            $switcher.removeClass().addClass('phone-portrait');
        }
        else if($this.is('#tablet-portrait')){

            $this.addClass('active').siblings().removeClass('active');
            $switcher.removeClass().addClass('tablet-portrait');
        }
        else if($this.is('#tablet-landscape')){

            $iframeWrapper.css('height',768+'px');
            $this.addClass('active').siblings().removeClass('active');
            $switcher.removeClass().addClass('tablet-landscape');
        }
        else if($this.is('#phone-landscape')){

            $iframeWrapper.css('height',375+'px');
            $this.addClass('active').siblings().removeClass('active');
            $switcher.removeClass().addClass('phone-landscape');
        }
        else{
            $iframeWrapper.css('height',1000+'px');
            $this.addClass('active').siblings().removeClass('active');
            $switcher.removeClass().addClass('desktop');
        }

    });

    // dynamic width for featured title
    $(window).load(function(){
        var $trapezoid = $('.trapezoid');
        var $titleWidth = $('.featured-listing-area h2.section-title span').width();

        $trapezoid.width($titleWidth+'px');
    });

});

//=================MENU JS============//

  //add dynamic class in default ul submenu
    $( "ul.nav li.parent-li ul" ).removeClass( "nav navbar-nav navbar-right main-nav nav-darken" ).addClass( "dropdown-menu" );
    $( "ul.nav li.parent-li ul li ul" ).removeClass( "dropdown-menu" ).addClass( "dropdown-submenu" );
    $( "ul.nav li.parent-li ul li" ).removeClass( "parent-li" ).addClass( "parent-submenu" );
    $( "ul.nav li.parent-li ul li ul li" ).removeClass( "parent-submenu" ).addClass( "parent-subsubmenu" );


    //add dynamic class in custom ul submenu
    $( "ul.custom-pages li.custom-li ul" ).removeClass( "custom-pages" ).addClass( "custom-pages-dropdown container hasDropdown-area");
    $( "ul.custom-pages li.custom-li ul li" ).removeClass( "custom-li" ).addClass( "custom-submenu" );
    $( "ul.custom-pages li.custom-li ul li ul" ).removeClass( "container hasDropdown-area" ).addClass( "" );
    $( "ul.custom-pages li.custom-li ul li ul li" ).removeClass( "custom-submenu" ).addClass( "custom-subsubmenu" );


    // add scrollbar for menu
    if(! /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
        $( ".custom-pages-hasdropdown" ).hover(function() {
           var wH = $( window ).height(),
               wW = $( window ).width(),
               customNavbar_H = $( "#custom-navbar" ).height(),
               mainNavbar_H = $( ".main-navbar" ).height(),
               header_H = customNavbar_H + mainNavbar_H,
               newsubmenu_H = wH - header_H - 50,
               dropdown = $(this).find(".custom-pages-dropdown.hasDropdown-area").height();
               dropdown_count = $(this).find(".custom-pages-dropdown.hasDropdown-area > li").length;


            var rt = ($(window).width() - ($(this).offset().left + $(this).outerWidth()));

            if (rt < 150) {

                $(this).find(".custom-pages-dropdown.hasDropdown-area").css({
                   'left' : 'unset',
                   'right' : '0',
                });

                $(this).find(".fa").removeClass("fa-angle-right").addClass("fa-angle-left");

                var firstDropdown = $(this).find(".custom-pages-hasdropdown.custom-submenu");

                firstDropdown.find(".custom-pages-dropdown").css({
                   'left' : 'unset',
                   'right' : '250px',
                });  
            }


            if (dropdown_count > 10) {
                // console.log(this);
                $(this).find(".custom-pages-dropdown.hasDropdown-area").css({
                   'overflow-y' : 'auto',
                   'height' : '500px',
                   'text-align' : 'left'
                });

                if (newsubmenu_H < 500) {
                    $(this).find(".custom-pages-dropdown.hasDropdown-area").css({
                       'height' : newsubmenu_H+'px'
                    });
                }
            }

            if ($(this).is(":last-child")) {

                var firstDropdown = $(this).find(".custom-pages-hasdropdown.custom-submenu");

                firstDropdown.find(".custom-pages-dropdown").css({
                   'left' : 'unset',
                   'right' : '250px',
                });
            }
        });

        // window resize
        window.onresize = function(event) {
            $( ".custom-pages-hasdropdown" ).hover(function() {
               var wH = $( window ).height(),
                   wW = $( window ).width(),
                   customNavbar_H = $( "#custom-navbar" ).height(),
                   mainNavbar_H = $( ".main-navbar" ).height(),
                   header_H = customNavbar_H + mainNavbar_H,
                   newsubmenu_H = wH - header_H - 50,
                   dropdown = $(this).find(".custom-pages-dropdown.hasDropdown-area").height();
                   dropdown_count = $(this).find(".custom-pages-dropdown.hasDropdown-area > li").length;

                if (dropdown_count > 10) {
                    // console.log(this);
                    $(this).find(".custom-pages-dropdown.hasDropdown-area").css({
                       'overflow-y' : 'auto',
                       'height' : '500px',
                       'text-align' : 'left'
                    });

                    if (newsubmenu_H < 500) {
                        $(this).find(".custom-pages-dropdown.hasDropdown-area").css({
                           'height' : newsubmenu_H+'px'
                        });
                    }
                }



            });
        };
    }
    $( ".custom-pages-hasdropdown.custom-submenu" ).hover(function(e) {
        var dropdown = $(this).find(".custom-pages-dropdown").height();
        if (dropdown > 350) {
            $(this).find(".custom-pages-dropdown").css({
               'overflow-y' : 'auto',
               'height' : '350'
            });
        }


    });

    if(! /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
        var menu_count = $(".custom-pages .custom-li").length;
        // console.log(menu_count);
        if (menu_count >= 5) {
            $("#custom-navbar .container").removeClass("container");
            $(".custom-pages").css({
                "float": "none",
                "text-align": "center"
            });
            $(".custom-pages > li > a").css("padding", "10px 7px 8px");
        }
    }


    // For Sherry Owen dropwdown Property Videos
    $( ".custom-pages-dropdown .custom-submenu a, .custom-pages .custom-li a" ).each(function() {
      if ($( this ).text() == " Property Videos") {
        $(this).attr('target', '_blank')
      }
    });

    if(! /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
        $( "ul.custom-pages li.custom-pages-hasdropdown ul.custom-pages-dropdown li.custom-submenu i" ).removeClass( "fa-angle-down" ).addClass( "fa-angle-right" );
    }

    //write a review
    $('.write-review-button').on('click', function() {
        $reviewArea = $('.review-area');
        $('#testimonial-message').value='';
        $reviewArea.css('display','block');
    });


    $( window ).scroll(function() {
      var b = $(this).scrollTop();

      if (b > 25) {
        $(".sub-navbar-menu").addClass("sub-dark-menu");
      } else {
        $(".sub-navbar-menu").removeClass("sub-dark-menu")
      }
    });


    $(window).load(function () {
        $("#custom-navbar").removeClass("hide-menu");
    });


    // toggle more pages button
    $(".subnavbar-menu-btn").click(function(){
        $( ".custom-pages" ).slideToggle("fast");
        $( ".custom-pages" ).toggleClass("open");
        $( ".subnavbar-menu-btn a i" ).toggleClass("fa-plus");
        $( ".subnavbar-menu-btn a i" ).toggleClass("fa-minus");
        var login = $('.pollSlider').length;
        var act = $('.custom-pages').hasClass("open");
        if(act) {
            if (login) {
                $('.pollSlider').css("z-index", "1");
            };
        } else { 
            if (login) {
                $('.pollSlider').css("z-index", "99");
            };
        }
    });

    $(".navbar-toggle").click(function(){
        $( ".custom-pages" ).hide();
        $( ".subnavbar-menu-btn a i" ).removeClass("fa-minus");
        $( ".subnavbar-menu-btn a i" ).addClass("fa-plus");
    });

    // adjust find-a-home's sub-navbar-menu and advance search when user is logged in
    if( /Android|webOS|iPhone|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {

        var headerTop = $(".main-navbar").height();
        var headerSub = $("#custom-navbar").height();
        var searchNav = $(".advance-search-area").height();
        var advancesearcharea = headerTop+headerSub+3;
        var advancesearcharea1 = advancesearcharea-1;
        var headerH = advancesearcharea+searchNav - 1;
        console.log(advancesearcharea1);
        if ($('.userlog').length != 1) {
            var subnavbarmenu = headerTop+1;
            var browserIssue = headerTop+51;
        } else {
            var subnavbarmenu = headerTop+4;
            var browserIssue = headerTop+54;
        }
        $('.sub-navbar-menu').css("cssText", "top: "+subnavbarmenu+"px !important");
        $('.browser-issue').css("margin-top", browserIssue);
        $('#features_modal').css("cssText", "top: "+subnavbarmenu+"px !important");
        $('.map-search-content').css("cssText", "margin-top: "+headerH+"px");
        $('.property-detail-section-nopadding').css("cssText", "margin-top: "+advancesearcharea+"px !important;");
        $('.advance-search-area').css("cssText", "margin-top: "+advancesearcharea1+"px");
    } else if ( /iPad|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
        var headerTop = $(".main-navbar").height();
        var headerSub = $("#custom-navbar").height();
        var searchNav = $(".advance-search-area").height();
        var advancesearcharea = headerTop+headerSub+3;
        var advancesearcharea1 = advancesearcharea-1;
        var headerH = advancesearcharea+searchNav - 1 ;
        console.log(advancesearcharea1);
        if ($('.userlog').length != 1) {
            var subnavbarmenu = headerTop+1;
            var browserIssue = headerTop+51;
        } else {
            var subnavbarmenu = headerTop+4;
            var browserIssue = headerTop+54;
        }
        $('.sub-navbar-menu').css("cssText", "top: "+subnavbarmenu+"px !important");
        $('.browser-issue').css("margin-top", browserIssue);
        $('#features_modal').css("cssText", "top: "+subnavbarmenu+"px !important");
        $('.map-search-content').css("cssText", "margin-top: "+headerH+"px");
        $('.property-detail-section-nopadding').css("cssText", "margin-top: "+advancesearcharea+"px !important;");
        $('.advance-search-area').css("cssText", "top: "+advancesearcharea1+"px");
    } else  {

        var window_height = $(window).height();
        var headerTop = $(".main-navbar").height();
        var headerSub = $("#custom-navbar").height();
        var searchNav = $(".advance-search-area").height();
        var advancesearcharea = headerTop+headerSub+3;
        var advancesearcharea1 = advancesearcharea-1;
        var headerH = advancesearcharea+searchNav - 1;
        if ($('.userlog').length != 1) {
            var subnavbarmenu = headerTop+1;
            var browserIssue = headerTop+51;
        } else {
            var subnavbarmenu = headerTop+4;
            var browserIssue = headerTop+54;
        }
        $(".page-content").css({
            'min-height' : window_height-290,
            'margin-top' : headerSub + headerTop
        });

        $('.sub-navbar-menu').css("cssText", "top: "+subnavbarmenu+"px !important");
        $('.browser-issue').css("margin-top", browserIssue);
        $('.property-detail-section-nopadding').css("cssText", "margin-top: "+advancesearcharea+"px !important;");
        $('.map-search-content').css("cssText", "margin-top: "+headerH+"px");
        $('.advance-search-area').css("cssText", "top: "+advancesearcharea1+"px");

    }

    // Applicable only for mobile
    if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {


          $('.custom-pages > li.custom-li > i, .custom-pages .custom-li.custom-pages-hasdropdown .hasDropdown-area .custom-submenu > i').on('hover', function(e){
              e.preventDefault();

              console.log($(this).next().css('display'));
              if($(this).next().css('display') === 'block'){
                $(this).next().css("cssText","display: none !important;")
              }else{
                // $('ul.custom-pages-dropdown').hide();
                $(this).next().css("cssText","display: block !important;")
              }
          });
          
          $('.custom-pages > li.custom-li > i, .custom-pages .custom-li.custom-pages-hasdropdown .hasDropdown-area .custom-submenu > i').on('click', function(e){
              e.preventDefault();

              console.log($(this).next().css('display'));
              if($(this).next().css('display') === 'block'){
                $(this).next().css("cssText","display: none !important;")
              }else{
                // $('ul.custom-pages-dropdown').hide();
                $(this).next().css("cssText","display: block !important;")
              }
          });
    }

//=================MENU JS============//

// dynamic width for featured title
$(window).load(function(){
    var $trapezoid = $('.trapezoid');
    var $titleWidth = $('.featured-listing-area h2.section-title span').width();

    $trapezoid.width($titleWidth+'px');
});

$(document).ready(function() {

    //RateYo (Getting Ratings From Review)
    $("div.ratefixed").each(function(){
        $rate = $(this).attr("data-rate");
        $(this).rateYo({
            rating: $rate,
            readOnly: true,
            fullStar: true
        });
    });

    //Display average ratings
    $(".average-rating").each(function(){

        $rate = $(this).attr("data-average-reviews");
        $rate = (isNaN($rate)) ? 0 : $rate;

        $(this).rateYo({
            rating:$rate,
            readOnly:true,
            fullStar:true,
        });
    });

    $("#reviews_order").on("change", function(){
        console.log($(this).val());
        $("#sort_reviews_form").submit();
    });

    $(".write-review-button").click(function(){
        $(".new").rateYo({
            fullStar: true,
            rating: 5,
            onInit: function (rating, rateYoInstance) {
              console.log("RateYo initialized! with " + rating);
            }
        });
    });

    $("#review_form").off('submit').on('submit', function(e) {

        e.preventDefault();
        $rate = $(".new").rateYo();
        $rating = $rate.rateYo("rating");
        $("#rating").val($rating);

        if($("#review_form input[name='customer_id']").val()=="") {

            $(".redirect_customer").val($("#review_form input[name='redirect-customer']").val());
            $(".submission_type").val("testimonial");
            $("#capture-leads-login").modal("show");

        } else {

            $.ajax({
                type: 'POST',
                url: $(this).attr("action"),
                data: $(this).serialize(),
                dataType: 'json',
                beforeSend: function(data) {
                    $("#submit_review").prop("disabled", true);
                },
                success: function(data) {
                    $("#submit_review").prop("disabled", false);
                    if(data.success) {
                        $("#ret-message").html("<div class='alert alert-success'><p><strong>Thanks for adding your testimonial. Your testimonial will be displayed in this page shortly after the admin approved it.</strong></p></div>");
                    } else {
                        $("#ret-message").html("<div class='alert alert-danger'><p><strong>Failed to create a testimonial!</strong></p></div>");
                    }
                    setTimeout(function() {
                        location.reload();
                    }, 3000);
                }
            });    

        }
        
        return false;
    });
});

var content = jQuery(".pollSlider");
var contentwidth = jQuery(".pollSlider").width();
var slideright = contentwidth + 32;
$('#pollSlider-button').click(function() {

    var cssValue = content.css('right'); 
    var parsedCssValue = parseInt(cssValue);

    if (slideright == parsedCssValue) {
        content.animate({right: 0});
        $(".backdrop").hide();
    } else {
        content.stop();
        content.animate({right: slideright}); 
        $(".backdrop").show();
    }
    

});
$(".backdrop").click(function() {
    $(this).show();
    $content.animate({right: 0});
});

/** Email validation */
(function () {
	// From http://www.whatwg.org/specs/web-apps/current-work/multipage/states-of-the-type-attribute.html#e-mail-state-%28type=email%29
	var emailRegex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	var tips = $('<div/>', {
		class: 'help-block'
	});

	function updateTips(t) {
		tips
			.text( t )
			.addClass( "text-error ui-state-highlight" );

		setTimeout(function() {
			tips.removeClass( "text-error ui-state-highlight", 1500 );
			tips.remove();
		}, 20000 );
	}

	function checkRegexp( o, regexp, n ) {
		if ( !( regexp.test( o.val() ) ) ) {
			o.addClass( "ui-state-error" );
			updateTips( n );
			return false;
		}
		return true;
	}

	$(document).on('keyup blur', '[type=email]', {}, function () {
	    var input = $(this);

	    if ( typeof input === undefined ||
            (undefined !== typeof input.attr('readonly') && undefined === typeof input.attr('disabled'))
        ) {
	        return;
        }

        if ( input.val().length === 0 ) {
	        input.unbind('invalid');
	        input.data('bind:invalid', false);
	        return;
        }

        if ( !input.data('bind:invalid') ) {
	        input.bind('invalid', function(e) {
		        var target = $(e.target);
	        });

	        input.data('bind:invalid', true);
        }

		if (! checkRegexp(input, emailRegex, "Invalid email address") ) {
			input.parent().prepend(tips).addClass('has-error');
			tips.show();
			input.get(0).checkValidity();
		}
		else {
			input.removeClass( "ui-state-error" );
			input.parent().removeClass('has-error');
			tips.remove();
		}
	})
})();
