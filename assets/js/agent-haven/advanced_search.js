$(document).ready(function() {

	var current_page = 1;
	var requestRunning = false;
	var infowindowMap = [];
	var enable_polygon = false;
	var isDraw = true;
	var map;
	var gcolor = "#d75151";

    var drawingManager;
    var selectedShape;
    var colors = ['#1E90FF', '#FF1493', '#32CD32', '#FF8C00', '#4B0082'];
    var selectedColor;
    var colorButtons = {};

    

	if(searchIsSaved) {
		$("#save-search-btn").addClass("isSaved");
	}
	var listPined = "";
	function numberWithCommas(x) {
	    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	}
	
	function mapSearch(filter='', page=1, $type='basic', $limit=20) {

		if(typeof filter == 'object') {

			$.map(filter, function(value, index) {
				console.log("value2: "+value);
				console.log("of: ",typeof value);
				listPined = value;
				//$("#main_polygon_val").val(value);
 				$("#save-search-btn, .save-search-btn").attr('data-save-search', 'Search='+value);
 			});

 		} else {
 			$("#save-search-btn, .save-search-btn").attr('data-save-search', filter);
 		}

		// console.log("search/advance/properties_ajax/10/"+page);
		if($type == 'advance'){
			method = "GET";
			url= "search/advance/advance_search_ajax/20/"+page;
			data = {search : filter};
		}else if($type == 'new-advance'){
			method = "GET";
			url= "search/advance/new_advance_search_ajax/20/"+page;
			data = filter;
			//console.log(filter.Search);
			if(filter){
				if(!filter.Search){
					$(".save-search-btn, #save-search-btn").attr('data-save-search', filter);
				}		
			}

		}else if($type=='polygon-search'){
			console.log($type);
			isDraw = false;
			url= "search/advance/polygon_search/20/"+page;
			method = "GET";
			data = filter;
		}else{
			console.log($limit);
			url= "search/advance/properties_ajax/"+$limit+"/"+page;
			method = "POST";
			data = {search : filter};
		}

		requestRunning = true;
		$(".map-pagination li a, #filter_search_submit").css("cursor","not-allowed");
		$.ajax({
		  	url: url,
		  	method: method,
		  	data: data,
		  	dataType: "html",
		  	context: document.body,
		  	beforeSend: function(  ) {
			   // console.log("fetching data..");
			   	$(".propmap-placeholder").show();
				$(".propmap").hide();
			}
		}).done(function(response) {

			$(".propmap").show();
			$(".propmap-placeholder").hide();
			$(".map-pagination").show();
			$(".search-nav-polygon").show();
			$(".btn-draw").show();
			// $(".btn-draw").html('<i class="fa fa-object-group"></i> Polygon Search');
			$(".search-nav-polygon").removeClass('enable');

			var obj = JSON.parse(response);
			var strProperties = '';
			var geocoder;
			var bounds = new google.maps.LatLngBounds();
			var markers = [];
			var iw = [];

			console.log(obj);
			if (obj.Success) {
				// if(leads_config_set==true && leads_config.countSearchPerf==1) {
				// 	var search_count = (myStorage.getItem("search_count")) ? myStorage.getItem("search_count") : 0;
				// 	myStorage.setItem("search_count", parseInt(search_count)+1);
				// 	if(parseInt(myStorage.getItem("search_count")) > parseInt(myStorage.getItem("current_search_limit"))) {
				// 		if(leads_config.allowSkip==1) {
				//       	$("#capture-leads-login").modal();
				//          $(".capture-leads-login-btn").show();
				//      } else {
				//      	$("#capture-leads-login").modal({
				//          	backdrop: 'static',
				//          	keyboard: false
				//          });
				//      }
				// 		}
				// }
				$("#save-search-btn").prop("disabled", false);
				// console.log(obj.Results.length);
				$("#map_canvas").show();
				if(obj.Results.length > 0) {

				var totalRow = obj.Pagination.TotalRows;
				var cursor = '';
				current_page = obj.Pagination.CurrentPage;
				if (enable_polygon) {
					cursor = 'crosshair';
				} else {
					cursor = 'pointer';
				}

				console.log(cursor);


						var lat = obj.Results['0'].StandardFields.Latitude;
						var long = obj.Results['0'].StandardFields.Longitude;
						// var lat = "********";
						// var long = "********";

						if (lat != "********" && long != "********") {

							lat = obj.Results['0'].StandardFields.Latitude;
							long = obj.Results['0'].StandardFields.Longitude;

						} else {

							address = obj.Results['0'].StandardFields.UnparsedAddress;
							fulladdress = encodeURIComponent(address);

						      $.ajax({
						        url: base_url+"search/advance/getlatlon_ajax",
						        type: 'GET',
						        data: {
						        	address : address
						        },
						        async: false,
						        success:function(response) {
						        	console.log(response);

						        	if (response) {

							        	var result = JSON.parse(response);

										if (result) {
											lat = result.latlon.lat;
											long = result.latlon.lon;


											console.log(lat);
											console.log(long);
											console.log("ajax success");


										} else {

											lat = '';
											long = '';
										}
						        	} else {

										lat = '';
										long = '';
							    	}
						        }
						      });

							/** 
							*
							* END
							**
							*/ 
						}

						map = new google.maps.Map(
					        document.getElementById("map_canvas"), {
					            center: new google.maps.LatLng(lat, long),
					            zoom: 10,
					            // scrollwheel: false,
					            // draggable: false
					            zoomControl: true,
					            draggableCursor: cursor,
					            mapTypeId: google.maps.MapTypeId.ROADMAP
					    });



			            /******* Polygon Pattern ********/ 
			            
			            var destinations = new google.maps.MVCArray();

			            if (enable_polygon) {
			            	//enableDrawTools ();
			              //sample lat lat
			              //var sampleDestinations = '[{"lat":33.67336050078159,"long":-111.97948843926446},{"lat":33.63678090918729,"long":-111.46313101738946},{"lat":33.40092802764046,"long":-111.98635489434258}]';

			              var pinedDestinations = $("#main_polygon_val").val(); 
			              //console.log(pinedDestinations);
			              if(pinedDestinations){
			              	//map.setOptions({zoom:15});
			                pinedDestinations = JSON.parse(pinedDestinations);
			                pinedDestinations.forEach( (item) => {
			                  console.log('lat', item.lat);
			                  console.log('long', item.long);
			                  destinations.push ( new google.maps.LatLng(item.lat, item.long) );
			                });

			                var polygonOptions = {path: destinations, strokeColor: gcolor, strokeWeight: 2, fillColor: gcolor};
			                //polygonOptions.fillColor
			                var polygon = new google.maps.Polygon(polygonOptions);
			                polygon.setMap(map);

			              }
			          
			              // google.maps.event.addListener(map, 'click', function(e) {
			              //   if(isDraw){
			              //       var pattern = [];
			              //       $("#polygonval").val("");      
			              //       //$("#main_polygon_val").val("");          
			              //       destinations.push ( new google.maps.LatLng(e.latLng.lat(), e.latLng.lng()) );
			                    
			              //       var polygonOptions = {path: destinations, strokeColor: "#d75151", strokeWeight: 2, fillColor: "#fff"};
			              //       var polygon = new  google.maps.Polygon(polygonOptions);
			              //       polygon.setMap(map);

			              //       var vertices = polygon.getPath();

			              //       for (var i =0; i < vertices.getLength(); i++) {
			              //         var xy = vertices.getAt(i);
			              //         pattern.push({ lat:xy.lat(), long:xy.lng() })
			              //       }
			                    
			              //     //console.log(pattern);                
			              //     $("#polygonval").val(JSON.stringify(pattern));
			              //     $("#main_polygon_val").val(JSON.stringify(pattern));
			              //   }
			              // });              

			            } else {
			              var filteredDistination = '';
			              $("#polygonval").val("");
			              //$("#main_polygon_val").val("");
			              var pinedDestinations = $("#main_polygon_val").val(); 
			              // console.log('fdsf', pinedDestinations);

			              if(pinedDestinations != ""){

			              	var query = (function(a) {
							    if (a == "") return {};
							    var b = {};
							    for (var i = 0; i < a.length; ++i) {
							        var p=a[i].split('=', 2);
							        if (p.length == 1)
							            b[p[0]] = "";
							        else
							            b[p[0]] = decodeURIComponent(p[1].replace(/\+/g, " "));
							    }
							    return b;
							})(window.location.search.substr(1).split('&'));

			              	$.map(query, function(value, index) {
					        	filteredDistination = value;
					        	$("#main_polygon_val").val(filteredDistination);
				 			});

			              	pinedDestinations = JSON.parse(filteredDistination);
			                pinedDestinations.forEach( (item) => {
			                  console.log('lat', item.lat);
			                  console.log('long', item.long);
			                  destinations.push ( new google.maps.LatLng(item.lat, item.long) );
			                });

			                var polygonOptions = {path: destinations, strokeColor: "#d75151", strokeWeight: 2, fillColor: "#fff"};
			                var polygon = new google.maps.Polygon(polygonOptions);
			                polygon.setMap(map);

			              }
			            }
						/******* Polygon Pattern ********/ 

					var x = 1;
					var infoWindow = new google.maps.InfoWindow({maxWidth: 275});
					for (var i = 0; i < obj.Results.length; i++) {
						var lat = obj.Results[i].StandardFields.Latitude;
						var long = obj.Results[i].StandardFields.Longitude;
						// var lat = "********";
						// var long = "********";


						
						//if (lat != "********" && long != "********") {
							var listingKey = obj.Results[i].StandardFields.ListingKey;
							var photo = obj.Results[i].StandardFields.Photos[0].Uri300;
							var address = obj.Results[i].StandardFields.UnparsedFirstLineAddress;
							var fullAddress = obj.Results[i].StandardFields.UnparsedAddress;
							var price = numberWithCommas(obj.Results[i].StandardFields.CurrentPrice);
							var propertyClass = obj.Results[i].StandardFields.PropertyClass;
							var bed = obj.Results[i].StandardFields.BedsTotal;
							var bath = obj.Results[i].StandardFields.BathsTotal;
							// var area = obj.Results[i].StandardFields.BuildingAreaTotal;
							var city = obj.Results[i].StandardFields.City;
							var state = obj.Results[i].StandardFields.StateOrProvince;
							var postalcode = obj.Results[i].StandardFields.PostalCode;
							var propertySubClass = obj.Results[i].StandardFields.PropertySubType;
							var no_units = obj.Results[i].StandardFields.NumberOfUnitsTotal;
							var mlsid = obj.Results[i].StandardFields.MlsId;
							var bathsfull = obj.Results[i].StandardFields.BathsFull;
							var bathshalf = obj.Results[i].StandardFields.BathsHalf;
							var mls_status = obj.Results[i].StandardFields.MlsStatus;
				              var bedbatharea = "";
				              var bedbatharea1 = "";
				              var lotArea = "";
				              var lotType ="";



				              if(obj.Results[i].StandardFields.BuildingAreaTotal === "" || obj.Results[i].StandardFields.BuildingAreaTotal != "********"){
				              	lotArea = obj.Results[i].StandardFields.BuildingAreaTotal;
				              	lotType = "";
				              }else if(obj.Results[i].StandardFields.LotSizeSquareFeet === "" || obj.Results[i].StandardFields.LotSizeSquareFeet != "********"){
				              	lotArea = obj.Results[i].StandardFields.LotSizeSquareFeet;
				              	lotType = "Lot Size:";
				              }

				              
				              if(lotArea !== null){
				              	var area = lotArea.toLocaleString();
				              }else{
				              	var area = lotArea;
				              }              
              

							if (lat == "********" && long == "********") {
								// address = obj.Results[i].StandardFields.UnparsedAddress;
								// fulladdress = encodeURIComponent(address);
								// console.log(fulladdress);

								// var request = new XMLHttpRequest();
								// request.open( 'GET', 'https://maps.googleapis.com/maps/api/geocode/json?address=' + fulladdress +'&key=AIzaSyCrMzetdv25NdoUlfkVQuBVUcRw4_NGXcc', false);
								// request.send( null );
								// var res = JSON.parse(request.response);
								// var result = res.result;
								// console.log(res.result);

								// if (result) {
								// 	lat = res.results[0].geometry.location.lat;
								// 	long = res.results[0].geometry.location.lng;
								// } else {
								// 	lat = "";
								// 	long = "";
								// }




								address = obj.Results[i].StandardFields.UnparsedAddress;
								fulladdress = encodeURIComponent(address);

							    $.ajax({
							        url: base_url+"search/advance/getlatlon_ajax",
							        type: 'GET',
							        data: {
							        	address : address
							        },
							        async: false,
							        success:function(response) {
							        	console.log(response);

							        	if (response) {

								        	var result = JSON.parse(response);

											if (result) {
												lat = result.latlon.lat;
												long = result.latlon.lon;


												console.log(lat);
												console.log(long);
												console.log("ajax success");


											} else {

												lat = '';
												long = '';
											}
							        	} else {

											lat = '';
											long = '';
								    	}
								    }
							    });

							};
		
                            if(address == 'null' || address == null || address == '' || address == 'undefined'){
                            	address = "";
                            }
                            
                            if(area == 'null' || area == null || area == '' || area == 'undefined'){
                            	area = "not specified";
                            }

                            if(mlsid != '20081211185657097606000000'){
                            	switch(propertyClass){                            	
	                            	case 'Residential':
	                            		if (bath != "********") {
	                            			bedbatharea = "<p  class='property-spec'>BATH <span class='prop-bath'>" + bath + "</span> | BED <span class='prop-bed'>" + bed + "</span> | "+ lotType +" <span class='prop-area'>" + area + " sqft</span></p>";
	                            			bedbatharea1 = " <p style='font-size:10px;'>BATH " + bath + " | BED " + bed + " | "+ lotType +" " + area + " sqft</p>";
	                            		}else{
	                            			bedbatharea = "<p  class='property-spec'>BED <span class='prop-bed'>" + bed + "</span> | "+ lotType +" <span class='prop-area'>" + area + " sqft</span></p>";
	                            			bedbatharea1 = " <p style='font-size:10px;'>BED " + bed + " | "+ lotType +" " + area + " sqft</p>";
	                            		}
										break;
	                            	case 'Land':
										if (area != "********") {
		                                	bedbatharea = "<p  class='property-spec'> "+ lotType +" <span class='prop-area'>" + area + " sqft</span></p>";
		                                	bedbatharea1 = " <p style='font-size:10px;'> "+ lotType +" " + area + " sqft</p>";
		                            	} else {
		                            		bedbatharea = "";
		                            		bedbatharea1 = "";
		                            	};
		                            	
										break;
									case 'MultiFamily':
										bedbatharea = "<p  class='property-spec'>NO. UNITS <span class='prop-bed'>" + no_units + "</span></p>";
										bedbatharea1 = " <p style='font-size:10px;'>NO. UNITS <span class='prop-bed'>" + no_units + "</span></p>";
	                            		break;
	                            	case 'Rental':
	                            		if (bath != "********") {
	                            			bedbatharea = "<p  class='property-spec'>BATH <span class='prop-bath'>" + bath + "</span> | BED <span class='prop-bed'>" + bed + "</span> | "+ lotType +" <span class='prop-area'>" + area + " sqft</span></p>";
	                            			bedbatharea1 = " <p style='font-size:10px;'>BATH " + bath + " | BED " + bed + " | "+ lotType +" " + area + " sqft</p>";
	                            		}else{
	                            			bedbatharea = "<p  class='property-spec'>BED <span class='prop-bed'>" + bed + "</span> | "+ lotType +" <span class='prop-area'>" + area + " sqft</span></p>";
	                            			bedbatharea1 = " <p style='font-size:10px;'>BED " + bed + " |"+ lotType +" " + area + " sqft</p>";
	                            		}
										break;
	                            	case 'Commercial':
										if (area != "********") {
		                                	bedbatharea = "<p  class='property-spec'> "+ lotType +" <span class='prop-area'>" + area + " sqft</span></p>";
		                                	bedbatharea1 = " <p style='font-size:10px;'>"+ lotType +" " + area + " sqft</p>";
		                            	} else {
		                            		bedbatharea = "";
		                            		bedbatharea1 = "";
		                            	};	

										break;		
								}

								vcontentString = "<div class='panel panel-default property-panel' id='"+i+"' data-key='"+x+"' data-listing='"+listingKey+"' data-fulladdress='"+fullAddress+"'>";
								vcontentString += "<div class='panel-body'><div class='col-md-4 col-sm-4 col-xs-4 no-padding'><a href='javascript:void(0)' class='img_link'><img src='"+photo+"' alt='' class='img-responsive property-image'></a></div>";
								vcontentString += "<div class='col-md-6 col-sm-6 col-xs-6'><p class='property-address property_item_"+i+"' data-lat='"+lat+"' data-long='"+long+"' ><a href='other-property-details/"+listingKey+"' class='address-text' target='_blank'>"+address+"</a></p>";

								stype = $(".sub-property-item:checked").length;

								


								if(propertySubClass != "********"){
									
									//if true; used template for armls
									if( mlsid == '20070913202326493241000000'){
										if(stype>0){
											propertyClass = propertySubClass;
										}
										vcontentString += "<p class='property-city'>"+city+", "+state+", "+postalcode+"</p><b class='prop-price'>$"+price+"</b><p class='property-class'> <span class='label label-info'><i class='glyphicon glyphicon-home'></i> " + mls_status + "</span>&nbsp;<span class='label label-default'><i class='glyphicon glyphicon-tags'></i> " + propertyClass + "</span></p>"+ bedbatharea +"</div>";
										acontentString = "<div class='col-md-5 no-padding'><a href='other-property-details/"+listingKey+"' target='_blank'><img src='"+photo+"' alt='' class='img-responsive' style='width: 100%;height: 100px;margin:5% 0;'></a></div><div class='col-md-7 nopadding-right'>	<p style='font-size:12px;margin: 0px;color: #0000bd;'><a href='other-property-details/"+listingKey+"' target='_blank'>"+address+"</a></p><p class='prop-city' style='margin-bottom: 2px;'>"+city+", "+state+", "+postalcode+"</p><b style='font-size: 20px;'>$"+price+"</b> <br><span class='label label-info' style='display: inline-block;'><i class='glyphicon glyphicon-home'></i> " + mls_status + "</span><br /><span class='label label-default' style='display: inline-block;'><i class='glyphicon glyphicon-tags'></i> " + propertyClass + "</span>"+bedbatharea1;
									}else{
										vcontentString += "<p class='property-city'>"+city+", "+state+", "+postalcode+"</p><b class='prop-price'>$"+price+"</b><p class='property-class'> <span class='label label-info'><i class='glyphicon glyphicon-home'></i> " + propertyClass + "</span>&nbsp;<span class='label label-default'><i class='glyphicon glyphicon-tags'></i> " + propertySubClass + "</span></p>"+ bedbatharea +"</div>";
										acontentString = "<div class='col-md-5 no-padding'><a href='other-property-details/"+listingKey+"' target='_blank'><img src='"+photo+"' alt='' class='img-responsive' style='width: 100%;height: 100px;margin:5% 0;'></a></div><div class='col-md-7 nopadding-right'>	<p style='font-size:12px;margin: 0px;color: #0000bd;'><a href='other-property-details/"+listingKey+"' target='_blank'>"+address+"</a></p><p class='prop-city' style='margin-bottom: 2px;'>"+city+", "+state+", "+postalcode+"</p><b style='font-size: 20px;'>$"+price+"</b> <br><span class='label label-info' style='display: inline-block;'><i class='glyphicon glyphicon-home'></i> " + propertyClass + "</span><br /><span class='label label-default' style='display: inline-block;'><i class='glyphicon glyphicon-tags'></i> " + propertySubClass + "</span>"+bedbatharea1;
									}

									//vcontentString += "<p class='property-city'>"+city+", "+state+", "+postalcode+"</p><b class='prop-price'>$"+price+"</b><p class='property-class'> <span class='label label-info'><i class='glyphicon glyphicon-home'></i> " + propertyClass + "</span>&nbsp;<span class='label label-default'><i class='glyphicon glyphicon-tags'></i> " + propertySubClass + "</span></p>"+ bedbatharea +"</div>";
									//acontentString = "<div class='col-md-5 no-padding'><a href='other-property-details/"+listingKey+"' target='_blank'><img src='"+photo+"' alt='' class='img-responsive' style='width: 100%;height: 100px;margin:5% 0;'></a></div><div class='col-md-7 nopadding-right'>	<p style='font-size:12px;margin: 0px;color: #0000bd;'><a href='other-property-details/"+listingKey+"' target='_blank'>"+address+"</a></p><p class='prop-city' style='margin-bottom: 2px;'>"+city+", "+state+", "+postalcode+"</p><b style='font-size: 20px;'>$"+price+"</b> <br><span class='label label-info' style='display: inline-block;'><i class='glyphicon glyphicon-home'></i> " + propertyClass + "</span><br /><span class='label label-default' style='display: inline-block;'><i class='glyphicon glyphicon-tags'></i> " + propertySubClass + "</span>"+bedbatharea1;
								}else{

									if( mlsid == '20070913202326493241000000'){
										if(stype>0){
											propertyClass = propertySubClass;
										}
										vcontentString += "<p class='property-city'>"+city+", "+state+", "+postalcode+"</p><b class='prop-price'>$"+price+"</b><p class='property-class'> <span class='label label-info'><i class='glyphicon glyphicon-home'></i> " + mls_status + "</span>&nbsp;<span class='label label-default'><i class='glyphicon glyphicon-tags'></i> " + propertyClass + "</span></p>"+ bedbatharea +"</div>";
										acontentString = "<div class='col-md-5 no-padding'><a href='other-property-details/"+listingKey+"' target='_blank'><img src='"+photo+"' alt='' class='img-responsive' style='width: 100%;height: 100px;margin:5% 0;'></a></div><div class='col-md-7 nopadding-right'>	<p style='font-size:12px;margin: 0px;color: #0000bd;'><a href='other-property-details/"+listingKey+"' target='_blank'>"+address+"</a></p><p class='prop-city' style='margin-bottom: 2px;'>"+city+", "+state+", "+postalcode+"</p><b style='font-size: 20px;'>$"+price+"</b> <br><span class='label label-info' style='display: inline-block;'><i class='glyphicon glyphicon-home'></i> " + propertyClass + "</span>"+bedbatharea1;
									}else{
										vcontentString += "<p class='property-city'>"+city+", "+state+", "+postalcode+"</p><b class='prop-price'>$"+price+"</b><p class='property-class'> <span class='label label-info'><i class='glyphicon glyphicon-home'></i> " + propertyClass + "</span></p>"+ bedbatharea +"</div>";
										acontentString = "<div class='col-md-5 no-padding'><a href='other-property-details/"+listingKey+"' target='_blank'><img src='"+photo+"' alt='' class='img-responsive' style='width: 100%;height: 100px;margin:5% 0;'></a></div><div class='col-md-7 nopadding-right'>	<p style='font-size:12px;margin: 0px;color: #0000bd;'><a href='other-property-details/"+listingKey+"' target='_blank'>"+address+"</a></p><p class='prop-city' style='margin-bottom: 2px;'>"+city+", "+state+", "+postalcode+"</p><b style='font-size: 20px;'>$"+price+"</b> <br><span class='label label-info' style='display: inline-block;'><i class='glyphicon glyphicon-home'></i> " + propertyClass + "</span>"+bedbatharea1;
									}
									//vcontentString += "<p class='property-city'>"+city+", "+state+", "+postalcode+"</p><b class='prop-price'>$"+price+"</b><p class='property-class'> <span class='label label-info'><i class='glyphicon glyphicon-home'></i> " + propertyClass + "</span></p>"+ bedbatharea +"</div>";
									//acontentString = "<div class='col-md-5 no-padding'><a href='other-property-details/"+listingKey+"' target='_blank'><img src='"+photo+"' alt='' class='img-responsive' style='width: 100%;height: 100px;margin:5% 0;'></a></div><div class='col-md-7 nopadding-right'>	<p style='font-size:12px;margin: 0px;color: #0000bd;'><a href='other-property-details/"+listingKey+"' target='_blank'>"+address+"</a></p><p class='prop-city' style='margin-bottom: 2px;'>"+city+", "+state+", "+postalcode+"</p><b style='font-size: 20px;'>$"+price+"</b> <br><span class='label label-info' style='display: inline-block;'><i class='glyphicon glyphicon-home'></i> " + propertyClass + "</span>"+bedbatharea1;
								}
								vcontentString += "<div class='col-md-2 col-sm-2 col-xs-2 ipad-nopadding'><div class='property-links'><p class=''>";

								
                            }else{
                            	switch(propertyClass){                            	
	                            	case 'Residential':
										bedbatharea = "<p  class='property-spec'>BED <span class='prop-bed'>" + bed + "</span> | Full Baths <span class='prop-bed'>" + bathsfull + "</span> | Half Baths <span class='prop-bed'>" + bathshalf + "</span> | "+ lotType +" <span class='prop-area'>" + area + " sqft</span></p>";
	                        			bedbatharea1 = " <p style='font-size:10px;'>BATH " + bath + " | BED " + bed + " | "+ lotType +" " + area + " sqft</p>";
	                            		break;
	                            	case 'Land':
										if (area != "********") {
		                                	bedbatharea = "<p  class='property-spec'> "+ lotType +" <span class='prop-area'>" + area + " sqft</span></p>";
		                                	bedbatharea1 = " <p style='font-size:10px;'> "+ lotType +" " + area + " sqft</p>";
		                            	} else {
		                            		bedbatharea = "";
		                            		bedbatharea1 = "";
		                            	};
		                            	
										break;
									case 'MultiFamily':
										bedbatharea = "<p  class='property-spec'>NO. UNITS <span class='prop-bed'>" + no_units + "</span></p>";
										bedbatharea1 = " <p style='font-size:10px;'>NO. UNITS <span class='prop-bed'>" + no_units + "</span></p>";
	                            		break;
	                            	case 'Rental':
										bedbatharea = "<p  class='property-spec'>BED <span class='prop-bed'>" + bed + "</span> | Full Baths <span class='prop-bed'>" + bathsfull + "</span> | Half Baths <span class='prop-bed'>" + bathshalf + "</span> | "+ lotType +" <span class='prop-area'>" + area + " sqft</span></p>";
	                        			bedbatharea1 = " <p style='font-size:10px;'>Full Baths " + bathsfull + " | Half Baths " + bathshalf + " | BED " + bed + " | "+ lotType +" " + area + " sqft</p>";
	                            		break;
	                            	case 'Commercial':
										if (area != "********") {
		                                	bedbatharea = "<p  class='property-spec'> "+ lotType +" <span class='prop-area'>" + area + " sqft</span></p>";
		                                	bedbatharea1 = " <p style='font-size:10px;'>"+ lotType +" " + area + " sqft</p>";
		                            	} else {
		                            		bedbatharea = "";
		                            		bedbatharea1 = "";
		                            	};	

										break;		
								}

	                        	vcontentString = "<div class='panel panel-default property-panel' id='"+i+"' data-key='"+x+"' data-listing='"+listingKey+"' data-fulladdress='"+fullAddress+"'>";
								vcontentString += "<div class='panel-body'><div class='col-md-4 col-sm-4 col-xs-3 no-padding'><a href='javascript:void(0)' class='img_link'><img src='"+photo+"' alt='' class='img-responsive property-image'></a></div>";
								vcontentString += "<div class='col-md-6 col-sm-6 col-xs-7'><p class='property-address' data-lat='"+lat+"' data-long='"+long+"' ><a href='other-property-details/"+listingKey+"' class='address-text' target='_blank'>"+address+"</a></p>";
								vcontentString += "<p class='property-city'>"+city+"</p><b class='prop-price'>$"+price+"</b><p class='property-class'> <span class='label label-info'><i class='glyphicon glyphicon-home'></i> " + propertyClass + "</span>&nbsp;</p>"+ bedbatharea +"</div>";
								vcontentString += "<div class='col-md-2 col-sm-2 col-xs-2 ipad-nopadding'><div class='property-links'><p class=''>";
								
	                            acontentString = "<div class='col-md-5 no-padding'><a href='other-property-details/"+listingKey+"' target='_blank'><img src='"+photo+"' alt='' class='img-responsive' style='width: 100%;height: 100px;margin:5% 0;'></a></div><div class='col-md-7 nopadding-right'>	<p style='font-size:12px;margin: 0px;color: #0000bd;'><a href='other-property-details/"+listingKey+"' target='_blank'>"+address+"</a></p><p class='prop-city' style='margin-bottom: 2px;'>"+city+"</p><b style='font-size: 20px;'>$"+price+"</b> <br><span class='label label-info' style='display: inline-block;'><i class='glyphicon glyphicon-home'></i> " + propertyClass + "</span>"+bedbatharea1;

	                        }
              
              
							if(isCustomLoggedin == true) {

								var property_saved = false;

								if(save_prop_json !== null) {
									for(var p=0; p<save_prop_json.length; p++) {
										if(save_prop_json[p]==listingKey) {
											save_prop_json[p];
											property_saved=true;
										}
									}
								}
								var isFavorate = '';

								if(property_saved) {
									isFavorate = 'saveproperty';
								} else {
									isFavorate = '';
								}

								vcontentString += "<a href='javascript:void(0)' class='save-favorate "+isFavorate+"' id='save-favorate' title='Save Property' data-pro-id="+listingKey+" data-property-name='"+fullAddress+"'><span id='isSaveFavorate_"+listingKey+"'>";
								
								if(property_saved) {
									vcontentString += "<i class='fa fa-heart'></i></span></a>";
								} else {
									vcontentString += "<i class='fa fa-heart-o'></i></span></a>";
								}

							} else {
								vcontentString += "<a href='javascript:void(0);' class='save-favorate search_hearty modalsignup' data-original-title='title' data-pro-id="+listingKey+" data-property-name='"+fullAddress+"'><span id='isSaveFavorate_"+listingKey+"'><i class='fa fa-heart-o'></i></span></a>";
							}

							vcontentString += "<span class='hidethis-text'></span></a></p><a href='#' type='button' class='requestprop request_info' data-toggle='modal' data-target='#request-info-modal' data-listingid='"+listingKey+"' data-propertytype='other_property' data-address='"+fullAddress+"' data-original-title=' title='><i class='fa fa-question-circle'></i><span class='hidethis-text'></span></a></div></div></div></div>";
              
							strProperties += vcontentString;
							var myLatLng = {lat: lat, lng: long};

				            markers[i] = new google.maps.Marker({
				                position: myLatLng,
				                map: map,
				                icon: 'assets/img/home/map-marker.png'
				            });
                            
							var contentString = '<div id="content" class="infowindowmap'+i+'">'+
				        	acontentString+
				           	'</div>';
                            iw[i] = contentString;
                            // infowindowMap.push(markers[i]);
                            // hideAllInfoWindows(infowindowMap);
				            var ismove = true;
				            (function(marker,i,iw){
				                google.maps.event.addListener(marker, 'click', function(res){

	                                infoWindow.setContent(iw);
	                                infoWindow.open(map, marker);
	                                infowindowMap.push(marker);
				                });

				            }(markers[i],i,iw[i]));

					x++; }

					requestRunning = false;
					$(".next").show();
					$(".map-pagination li a, #filter_search_submit").css("cursor","pointer");
					var total_pages = obj.Pagination.TotalPages;
					$(".listingProperties").html(strProperties);
					$(".map-pagination .disabled .total").text(totalRow);
					$(".map-pagination .disabled .start-range").text(current_page * 20 - 19);
					$(".map-pagination .disabled .end-range").text(current_page * 20);
					if( (20 * current_page) > totalRow ){
						diff_pages = (20 * total_pages) - totalRow;
						$(".map-pagination .disabled .end-range").text((current_page * 20) - diff_pages);
						$(".next").css("cursor","not-allowed");
						$(".next").hide();
					}
					
					if (totalRow < 20) {
						$(".map-pagination .map-prev, .map-pagination .map-next").hide();
						$(".map-pagination .disabled .total").text(totalRow);
						$(".map-pagination .disabled .start-range").text("1");
						$(".map-pagination .disabled .end-range").text(totalRow);
					} else {
						$(".map-pagination .map-prev, .map-pagination .map-next").show();
					}

					if ( "undefined" !== typeof obj && obj.Pagination.CurrentPage === 1 ) {
						$(".map-pagination .map-prev").hide();
					}

		            $('.property-panel').hover(function () {
		            	var i = $(this).attr("id");
		            	google.maps.event.trigger(markers[i], 'click');

		            }, function () {
		            	var i = $(this).attr("id");
		            	google.maps.event.trigger(markers[i], 'mouseout');
		            });


				}
				else {

					// console.log("No Data");
					console.log(infowindowMap);

					for (var i = infowindowMap.length - 1; i >= 0; i--) {
						console.log(infowindowMap[i]);
						infowindowMap[i].setMap(null);
					};
					var noResult = '<div class="panel panel-default property-panel"><div class="panel-body result-empty"><p><i class="fa fa-exclamation-triangle"></i> Results not found. Please search again.</p></div></div>';

					$(".property-pane").hide();
					$(".map-pagination").hide();
					$("#map_canvas").hide();
					$(".listingProperties").html(noResult);
					$("#save-search-btn").prop("disabled", true);
				}
			} else {
				// console.log(obj.Message);
					for (var i = infowindowMap.length - 1; i >= 0; i--) {
						console.log(infowindowMap[i]);
						infowindowMap[i].setMap(null);
					};
				//var noResult = '<div class="panel panel-default property-panel"><div class="panel-body result-empty"><p><i class="fa fa-exclamation-triangle"></i> You have entered an invalid keyword. Please try again. </p></div></div>';
				var noResult = '<div class="panel panel-default property-panel"><div class="panel-body result-empty"><p><i class="fa fa-exclamation-triangle"></i> Results not found. Please search again.</p></div></div>';

				$(".property-pane").hide();
				$(".map-pagination").hide();
					$("#map_canvas").hide();
				$(".listingProperties").html(noResult);
				$("#save-search-btn").prop("disabled", true);
			};

		});
	}


	// Run mapSearch() function if page is mapped-search
	// firstUrl = window.location.pathname.replace(/^\/([^\/]*).*$/, '$1');
	// if (firstUrl == "mapped-search") {
	// 	mapSearch();
	// };

	firstUrl = window.location.pathname.replace(/^\/([^\/]*).*$/, '$1');

	if (firstUrl == "search-results" || firstUrl == "searched-results") {

		var query = (function(a) {
		    if (a == "") return {};
		    var b = {};
		    for (var i = 0; i < a.length; ++i) {
		        var p=a[i].split('=', 2);
		        if (p.length == 1)
		            b[p[0]] = "";
		        else
		            b[p[0]] = decodeURIComponent(p[1].replace(/\+/g, " "));
		    }
		    return b;
		})(window.location.search.substr(1).split('&'));
			console.log(query);
			if(Object.keys(query).length > 0 ){
				mapSearch(query,1,'new-advance');
			} else{
				mapSearch();
			}
		//get = '<?php echo $_GET;?>';
		//console.log(get);
	}else if(firstUrl == "advanced-search-results"){
		var query = (function(a) {
		    if (a == "") return {};
		    var b = {};
		    for (var i = 0; i < a.length; ++i) {
		        var p=a[i].split('=', 2);
		        if (p.length == 1)
		            b[p[0]] = "";
		        else
		            b[p[0]] = decodeURIComponent(p[1].replace(/\+/g, " "));
		    }
		    return b;
		})(window.location.search.substr(1).split('&'));

			console.log(query);

			if(query !== null && query !== ''){
				mapSearch(query,1,'advance');
			} else{
				mapSearch();
			}
	};

	$("#filter_search_field").keyup(function(event){
	    if(event.keyCode == 13){
	        $("#filter_search_submit").click();
	        
	        //remove &_GET[Search] from url
	   		if(query["Search"]){
				history.pushState(null, null, 'search-results');
	   		}
	    }
	});

	var parseQueryString = function( queryString ) {
    	var params = {}, queries, temp, i, l;
	    // Split into key/value pairs
	    queries = queryString.split("&");
	    // Convert the array of strings into an object
	    var params = [];
	    for ( i = 0, l = queries.length; i < l; i++ ) {
	        temp = queries[i].split('=');
	        params.push(temp);
	    }
	    return params;
	};

	$(".sbt-search-from").on("submit", function(e) {
		var query = $(".sbt-search-from").serialize();
		console.log(query);
   		var getKeywords = parseQueryString(query);
		$(".keyword-item").remove();
		$(".search-keyword").hide();

   		$.each(getKeywords, function( index, value ) {
   			if (value[1]) {
   			var searchname = value[0].replace(/%5B%5D/g, '');
   			var searchKey = value[1].replace(/[^\w\s]/g, ' ').replace(/2C/g, ', ');
   			var searchnameArray = ["g_street_number",'g_route','G_locality','g_locality','g_administrative_area_level_1','g_country','g_postal_code','InteriorLevels','polygonval ','polygonval'];
				$(".search-keyword").show();
				if(searchnameArray.indexOf(searchname) == -1){
					if (searchname == 'BedsTotal') {
						$(".search-keyword p").append("<span class='keyword-item'>"+searchname+ " : "+searchKey+"+</span>");
					} else {
						$(".search-keyword p").append("<span class='keyword-item'>"+searchname+ " : "+searchKey+"</span>");
					}
				}
   			};
		});
		$(".save-search-btn").removeClass("isSaved");
   		$(".form-search").removeClass("has-error");
   		$(".form-search .error-msg").remove();

   		if(!/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
			$(".save-search-btn").html("<i class='fa fa-save'></i> Save Search");
		} else {
			$(".save-search-btn").html("<i class='fa fa-save'></i>");
		}

   		// check if price is set
   		var min_price = $("#price_min").val();
   		var max_price = $("#price_max").val();
   		if( min_price >= 0 && min_price != ''){
   			if(max_price == ''){
   				$("#msg-txt").text("Please enter maximum price!");
   				$("#msg-err").modal("show");
   				return false;
   			}
   		}

   		if(isNaN(min_price) || isNaN(max_price)){
			$("#msg-txt").text("Please enter a valid price!");
			$("#msg-err").modal("show");
			return false;
		}

   		if( max_price >= 0 && max_price != ''){
   			if(min_price == ''){
   				$("#msg-txt").text("Please enter minimum price!");
   				$("#msg-err").modal("show");
   				return false;
   			}
   		}

   		mapSearch(query,1,'new-advance');	
   		return false;
   	});

	$(".search-button .submit-float").click(function() {
	  $(".more-isdropdown").removeClass("open");
	  $(".show-more-link").fadeIn();
	  $(".more-isdropdown").scrollTop(0);
	});
	$(".form-search input").on('focus',function(event){
	    var input = $(this);
	        $(".search-suggestion").show();
		   	$(".search-suggestion ul li a.keywords-history").click(function(e){
		   		e.preventDefault();
		   		var searchText = $(this).find("span").text();
		   		$("#filter_search_field").val(searchText);
		   		$(".search-suggestion").hide();
		   	});
	});

	$(document).mouseup(function(e) 
	{
	    var container = $(".form-search");
	    if (!container.is(e.target) && container.has(e.target).length === 0) 
	    {
	        $(".search-suggestion").hide();
	        // $(".more-isdropdown").removeClass("open");
	        // $(".more-isdropdown").hide();
	    }
	    if(! /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
		    var isDropdown = $(".more-dropdown");
		    if (!isDropdown.is(e.target) && isDropdown.has(e.target).length === 0) 
		    {
		    	$(".more-isdropdown").scrollTop(0);
		        $(".more-isdropdown").removeClass("open");
		        $(".show-more-link").show();
		        $(".more-isdropdown").hide();
		    }
	    }
	});

   	$(".next").on("click", function(e) {
		if (requestRunning) {
			e.preventDefault();
		}else {
			var searchTerm = $("#filter_search_field").val();
			var query = $(".sbt-search-from").serialize();
			//var query = {'Search':searchTerm};
			if(query){
				console.log(current_page++);
		    	mapSearch(query, current_page++,'new-advance');
			}else{
				console.log('heeeee');
				console.log(current_page++);
		    	mapSearch(null,current_page++,'new-advance');
			}
		}
	});
	$(".prev").on("click", function(e) {
		if (requestRunning) {
			e.preventDefault();
		} else {
			if (current_page == 1) {
				e.preventDefault();
			} else {
				var searchTerm = $("#filter_search_field").val();
				var query = $(".sbt-search-from").serialize();

				if(query){
					console.log(current_page--);
			    	mapSearch(query, current_page--,'new-advance');
				}else{
					console.log(current_page--);
			    	mapSearch(null,current_page--,'new-advance');
				}

				
			};
		};
	});

	function updateQueryStringParameter(uri, key, value) {
	  var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
	  var separator = uri.indexOf('?') !== -1 ? "&" : "?";
	  if (uri.match(re)) {
	    return uri.replace(re, '$1' + key + "=" + value + '$2');
	  }
	  else {
	    return uri + separator + key + "=" + value;
	  }
	}

	$(".sort-by").change(function() {

	var hashVal = $(this).val();
	var currentloc = window.location.href;
	var param = "order";
	var newURL = updateQueryStringParameter(currentloc, param, hashVal);

	window.location = newURL;

	});

	$(".city-field").change(function() {
		var city = $(this).val();
	console.log($(this).val());

		if (city != '') {
			$(".subdivision-form").show();
		};

	});

	$(".button-reset").on("click", function(event) {
	    $("#advance_search_form input[type='text']").each(function(){
		 var input = $(this);
		 input.attr("value", "");
		});
	    $("#advance_search_form input[type='checkbox']").each(function(){
		 var checkbox = $(this);
		 checkbox.removeAttr('checked');
		});
	    $("#advance_search_form select").each(function(){
    		// console.log($(this));
			var $select = $(this).selectize();
			var control = $select[0].selectize;
	    	if ($(this).hasClass("defaultval-property-type")) {
	    		if ($(this).val() != "Residential") {
	    			control.setValue('Residential', false);
	    		};
	    	} else if($(this).hasClass("defaultval-mls-status")){
	    		if ($(this).val() != "Active") {
	    			control.setValue('Active', false);
	    		};
	    	}else {
				control.clear();
	    	};
		});
    });

	//Prepare url string for redirecting customer after signup or login
	var query_string=firstUrl+"?";
	var counter=0;

	for(var prop in query) {
		if(counter==0) {
			query_string += prop+"="+query[prop];
		} else {
			query_string += "&"+prop+"="+query[prop];
		}
		counter++;
	}

	console.log(query_string);

	//Save property as favorite
	$(".listingProperties").delegate(".save-favorate", "click", function() {

		var b = $(this).data("pro-id");
        var prop_name = $(this).data("property-name");

		if(isCustomLoggedin) {

	        if($(this).hasClass("saveproperty")) {

				$.ajax({
					url: base_url+"home/home/remove_favorite_property/"+b,
					type: "POST",
					dataType: "json",
					success: function(data) {
						console.log(data);
						if(data.success) {
							$("#isSaveFavorate_" + b).parent().removeClass("saveproperty");
							$("#isSaveFavorate_" + b).html("<i class='fa fa-heart-o'></i>");
						}
					} 
				});

	        } else  {
		        
		        var href = base_url + "home/home/save_favorates_properties";
		        var c = href + "/" + b;

		        $.ajax({
			        url: c,
			        type: "post",
			        data: {
	                    'property_name': prop_name
	                },
			        dataType: "json",
			        success: function(a) {
			            console.log(a);
			            if(a.success) {
			            	console.log(b);
			            	console.log($("#isSaveFavorate_" + b));
			            	$("#isSaveFavorate_" + b).parent().addClass("saveproperty");
			            	$("#isSaveFavorate_" + b).html("<i class='fa fa-heart'></i>");
			            	//$("#isSavedFavorate_" + b).html("<small>Saved</small>");
			            } else {
			            	console.log("failed");
			            	//window.location.href = a.redirect;
			            }	
			        }
			    });
			}

		} else {
			
			$(".listing_id").val(b);
			$(".property_name").val(prop_name);
			$(".redirect_customer").val(query_string);
    		$(".submission_type").val("save-search-favorate");
			$("#capture-leads-login").modal("show");

		}

    });

    //Request Infomation Submit
    $("#customer-request-info").on("submit", function(e) {

        e.preventDefault();
        $dataString = $(this).serialize();

        $.ajax({
            type: 'POST',
            url: $(this).attr('action'),
            data: $dataString,
            dataType: 'json',
            beforeSend: function() {
                console.log('sending');
                $(".submit-loader").show();
            },
            success: function( data){
                console.log(data);
                if(data.success) {
                    $(".submit-loader").hide();
                    $(".show-mess").html(data.message);
                    $(".show-mess").fadeOut(3000);
                    setTimeout(function() { $('#request-info-modal').modal('hide'); }, 3000);

                    if(!data.is_login){
                        window.location.href = query_string;
                    }
                }
                else {
                    $(".show-mess").html(data.errors);
                }
            }
        });
    });

    $('.has-sub-dropdown .checkbox .property-parent').change(function() {
		  if (this.checked) {
		    
		    $(".is-sub-dropdown").hide();
		    if ($( this ).closest(".has-sub-dropdown").children(".is-sub-dropdown").find("li").length) {
		    	$( this ).closest(".has-sub-dropdown").children(".is-sub-dropdown").show();
		    	$( this ).closest(".has-sub-dropdown").children(".checkbox").find("input").prop("checked", true);
		    	// $(".is-sub-dropdown").show();
		    }
		  } else {
		  	$( this ).closest(".has-sub-dropdown").children(".is-sub-dropdown").hide();
		  	$( this ).closest(".has-sub-dropdown").children(".is-sub-dropdown").find("input").each(function() {
			  $( this ).prop("checked", false);
			});
		  }
		  console.log("test");
    });

	$('.dropdown-menu').click(function(e) {
	    e.stopPropagation();
	});
    
	$('.dropdown.default-option .dropdown-toggle').click(function(e) {
		if(! /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
		    $(".dropdown-menu.more-isdropdown").hide();
		}
	});
	$( ".btn-show-map" ).click(
	  function() {
	      if ($("#map_canvas").is( ":hidden" ) ) {
	       $(".btn-show-map .fa").removeClass("fa-map");
	       $(".btn-show-map .fa").addClass("fa-list-ul");
	       $("#map_canvas").css("cssText", "display: block !important");
	       $(".listingProperties").hide();
	      } else {
	       // $("#map_canvas").hide();
	       $("#map_canvas").css("cssText", "display: none !important");
	       $(".listingProperties").show();
	       $(".btn-show-map .fa").addClass("fa-map");
	       $(".btn-show-map .fa").removeClass("fa-list-ul");
	      }

	  }
	);

	$(".show-more-link").click(function() {
	  $(".more-isdropdown").addClass("open");
	  $(this).fadeOut();
	});

	$(".show-less-link").click(function() {
	  $(".more-isdropdown").removeClass("open");
	  $(".show-more-link").fadeIn();
	  $(".more-isdropdown").scrollTop(0);
	});

	$("#cheapest-tab").on("click", function(e) {
		$("#order_by").val("lowest");
		$(this).closest("li").addClass("active");
		$('#newest-tab').closest("li").removeClass("active");
		var query = $(".sbt-search-from").serialize();
   		mapSearch(query,1,'new-advance');

   		return false;
   	});

   	$("#newest-tab").on("click", function(e) {
		$("#order_by").val("newest");
		$(this).closest("li").addClass("active");
		$('#cheapest-tab').closest("li").removeClass("active");
		var query = $(".sbt-search-from").serialize();
   		mapSearch(query,1,'new-advance');

   		return false;
   	});

   	$(".save-search-btn").on("click", function() {
   		//$input_search = $("#filter_search_field").val(); 
   		$input_search = true; 
   		$(".form-search").removeClass("has-error");
   		$(".form-search .error-msg").remove();		
   		if(!$input_search){
   			$input_search = $(".form-search").addClass("has-error");
   			$(".form-search .visible-xs").after("<small class='error-msg'>Please input something here..</small>");
   			return false;			
   		} else {
   			$input_search = $(".form-search").removeClass("has-error");
   			$(".form-search .error-msg").remove();
	       	var data = $(this).attr("data-save-search");
	        var href = base_url + "home/home/save_search_properties";
	        
	        $.ajax({
		        url: href,
		        type: "GET",
		        data: data,
		        dataType: "json",
		        beforeSend:function(){
		        	if(! /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
		        		$(".save-search-btn").html("<i class='fa fa-save'></i> Saving...");
		        	} else {
		        		$(".save-search-btn").html("<i class='preloader'></i>");
		        	}
		        },success: function(a) {
		            console.log(a);
		            if(a.success) {
		            	$(".form-search").removeClass("has-error");
		            	$(".form-search .error-msg").remove();
		            	// $(".save-search-btn").html("<i class='fa fa-save'></i> Saved Search");
		            	$(".save-search-btn").addClass("isSaved");
			        	if(! /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
			        		$(".save-search-btn").html("<i class='fa fa-save'></i> Saved Search");
			        	} else {
			        		$(".save-search-btn").html("<i class='fa fa-save'></i>");
			        	}
		            } else {
		            	console.log(a);
		            	console.log("failed");
		            	$(".form-search").addClass("has-error");
		            	$(".form-search .visible-xs").after("<small class='error-msg'>You haven't search yet..</small>");
		            	$(".save-search-btn").removeClass("isSaved");
			        	if(! /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
			        		$(".save-search-btn").html("<i class='fa fa-save'></i> Save Search");
			        	} else {
			        		$(".save-search-btn").html("<i class='fa fa-save'></i>");
			        	}
		            }	
		        }
		    });
   		}


    });

    $(".save-button").delegate(".modalsignup", "click", function() {
    	$(".redirect_customer").val(query_string);
		$(".submission_type").val("save_search");
		$("#capture-leads-login").modal("show");
    });

    //Request Information Clicked
	$(".listingProperties").delegate(".request_info", "click", function() {
 		//populating values to the customer-request-info form
		$(".customer-request-info input[name='property_id']").val($(this).attr('data-listingid'));
		$(".customer-request-info input[name='property_type']").val($(this).attr('data-propertytype'));
		$(".customer-request-info input[name='property_address']").val($(this).attr('data-address'));
		$(".customer-request-info textarea[name='comments']").val("Please send me more information on "+$(this).data("address"));
	});

    if ($(".no-property-type").length) {
    	$(".no-property-type").parent().addClass("no-property");
    };
    
    // show search button in mobile 
    $(window).scroll(function() {
	    if ($(window).scrollTop() > 100) {
	       $(".navigation-search .search-button").addClass("open");
	    }
	    else {
	        // <= 100px from top - hide div
	        $(".navigation-search .search-button").removeClass("open");
	    }
	});

	// check if IE then show browser issue message
	var isIE = /*@cc_on!@*/false || !!document.documentMode;

	if (isIE) {
	    $(".advance-search-content").hide();
	    $(".browser-issue").show();
	};


	if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
		$(".listingProperties").delegate(".img_link", "click", function(e) { 
			e.preventDefault();
			
			var listing_id = $(this).closest(".property-panel").data("listing");
			var propName = $(this).closest(".property-panel").data("fulladdress");

			$(this).addClass("image-selected");

	        $.ajax({
	            type: 'POST',
	            url: base_url+"search/advance/get_photos/"+listing_id,
	            dataType: 'json',
	            success: function(data) {
	            	console.log(data)
	            	$(".property-panel .img_link").removeClass("image-selected");
	            	var arr = data.propertyphotos;
	            	var photos=[];
					arr.forEach(function(item){
						photos.push( { href:item, title:propName } );
					});
					$.swipebox(
						photos,
						{ 
							initialIndexOnArray: 0, 
							hideCloseButtonOnMobile : false,
							removeBarsOnMobile : false
						}
					);
	            },
	            error: function(err) {
	            	console.log(err);
	            }

	        });
		});
	}

	$(".btn-draw").click(function() {
	  	// $(".propmap-placeholder").show();
		// $(".propmap").hide();
	    $(".btn-draw").hide();
	    $(".btn-draw-cancel").hide();
	    $(".empty-polygon").text("");
	    
	    console.log(enable_polygon);
	      if (enable_polygon) {
	        enable_polygon = false;
	        $("#main_polygon_val").val(""); 
	        $(".btn-draw").html('<i class="fa fa-object-group"></i>Polygon Search');
	        $(".search-nav-polygon").removeClass("enable");
	        $(".search-disable").remove();
	        $(".col-lg-4.col-md-4.col-sm-5.left-disable").remove();
	        $("#map_canvas").removeClass('polygon-enable');
	        $("#color-palette").html('');
	        $(".btn-apply").hide();
	        $(".btn-remove-selected").hide();
	        $(".polygon-text").hide();
	        mapSearch('', 1, 'basic',25);
	      } else {
	        enable_polygon = true;
	        isDraw = true;
	        $(".advance-search-area").after('<div class="search-disable"></div>');
	        $(".col-lg-4.col-md-4.col-sm-5.left").after('<div class="col-lg-4 col-md-4 col-sm-5 left-disable"></div>');
	        $(".search-nav-polygon").addClass("enable");
	        $(".btn-draw").html('Cancel');
        	$(".btn-apply").html('<i class="fa fa-search"></i>Apply');
        	$("#map_canvas").addClass('polygon-enable');
	        $(".btn-apply").show();
	        $(".btn-remove-selected").show();
	        $(".polygon-text").show();


	          /*** When polygon search is enable ***/

	          var search      = $(".advance-search-area"),
	              left        = $(".col-lg-4.col-md-4.col-sm-5.left"),
	              propmap     = $(".col-lg-4.col-md-4.col-sm-5.property-placeholder"),
	              searchNav_H = search.outerHeight(),
	              searchNav_p = search.position(),
	              left_H      = left.outerHeight();
	              leftprop_H  = propmap.outerHeight();

	              console.log(leftprop_H);
	          $(".search-disable").css({
	            height: searchNav_H,
	            top: searchNav_p.top
	          });

	          $(".left-disable").css({
	            height: leftprop_H
	          });

	          /*** When polygon search is enable ***/ 
	      }

	      	var isHavePolySearch = $("#main_polygon_val").val(); 

            if(!isHavePolySearch){
            	 /******* Polygon Pattern ********/ 
            
	            var destinations = new google.maps.MVCArray();

	            if (enable_polygon) {
	              //sample lat lat
	              //var sampleDestinations = '[{"lat":33.67336050078159,"long":-111.97948843926446},{"lat":33.63678090918729,"long":-111.46313101738946},{"lat":33.40092802764046,"long":-111.98635489434258}]';

	              var pinedDestinations = $("#main_polygon_val").val(); 
	              //console.log(pinedDestinations);
	              if(pinedDestinations){
	                pinedDestinations = JSON.parse(pinedDestinations);
	                pinedDestinations.forEach( (item) => {
	                  console.log('lat', item.lat);
	                  console.log('long', item.long);
	                  destinations.push ( new google.maps.LatLng(item.lat, item.long) );
	                });

	                var polygonOptions = {path: destinations, strokeColor: "#d75151", strokeWeight: 2, fillColor: "#fff"};
	                var polygon = new google.maps.Polygon(polygonOptions);
	                polygon.setMap(map);

	              }

	              enableDrawTools();

	              // google.maps.event.addListener(map, 'click', function(e) {
	              //   if(isDraw){
	              //   	console.log(e);
	              //       var pattern = [];
	              //       $("#polygonval").val("");      
	              //       //$("#main_polygon_val").val("");          
	              //       destinations.push ( new google.maps.LatLng(e.latLng.lat(), e.latLng.lng()) );
	                    
	              //       var polygonOptions = {path: destinations, strokeColor: "#d75151", strokeWeight: 2, fillColor: "#fff"};
	              //       var polygon = new  google.maps.Polygon(polygonOptions);
	              //       polygon.setMap(map);

	              //       var vertices = polygon.getPath();

	                    
	              //       for (var i =0; i < vertices.getLength(); i++) {
	              //         var xy = vertices.getAt(i);
	              //         pattern.push({ lat:xy.lat(), long:xy.lng() })
	              //       }
	                    
	              //     //console.log(pattern);                
	              //     $("#polygonval").val(JSON.stringify(pattern));
	              //     $("#main_polygon_val").val(JSON.stringify(pattern));
	              //   }
	              // });              

	            } else {
	             var filteredDistination;
	              $("#polygonval").val("");
	              //$("#main_polygon_val").val("");
	              var pinedDestinations = $("#main_polygon_val").val(); 
	              console.log(listPined);

	              if(pinedDestinations != ""){
	              	$.map(pinedDestinations, function(value, index) {
			        	filteredDistination = value;
			        	$("#main_polygon_val").val(filteredDistination);
		 			});
	              	pinedDestinations = JSON.parse(filteredDistination);
	                pinedDestinations.forEach( (item) => {
	                  console.log('lat', item.lat);
	                  console.log('long', item.long);
	                  destinations.push ( new google.maps.LatLng(item.lat, item.long) );
	                });

	                var polygonOptions = {path: destinations, strokeColor: "#d75151", strokeWeight: 2, fillColor: "#fff"};
	                var polygon = new google.maps.Polygon(polygonOptions);
	                polygon.setMap(map);

	              }
	            }
				/******* Polygon Pattern ********/ 
            }else{
            	$("#main_polygon_val").val(""); 
            	mapSearch();	
            }




	});


	function enableDrawTools () {

	    var polyOptions = {
	        strokeWeight: 0,
	        fillOpacity: 0.45,
	        editable: true,
	        draggable: false,
	        strokeColor: '#1E90FF'
	    };
	    // Creates a drawing manager attached to the map that allows the user to draw
	    // markers, lines, and shapes.
	    drawingManager = new google.maps.drawing.DrawingManager({
	        drawingMode: google.maps.drawing.OverlayType.POLYGON,
	        // markerOptions: {
	        //     draggable: true
	        // },
			drawingControlOptions: {
			// position: google.maps.ControlPosition.LEFT_TOP,
			//drawingModes: ['polygon', 'rectangle']
			drawingModes: []

			},
	        rectangleOptions: polyOptions,
	        // circleOptions: polyOptions,
	        polygonOptions: polyOptions,
	        map: map
	    });

	    var latLngPattern = [];
	    var destinations = [];
	    var newLatLng;
        // Rectangle
		google.maps.event.addListener(drawingManager, 'rectanglecomplete', function (e) {
			//console.log(e.getPath());
        	var ne = e.getBounds().getNorthEast();
        	var sw = e.getBounds().getSouthWest();
			console.log('ne',ne.lat());
			console.log('ne',ne.lng());
			console.log('sw',sw.lat());
			console.log('sw',sw.lng());
			latLngPattern.push({ lat:ne.lat(), long:ne.lng() },{ lat:sw.lat(), long:sw.lng() });
			$("#polygonval").val(JSON.stringify(latLngPattern));
			$("#main_polygon_val").val(JSON.stringify(latLngPattern));
		});


        // Polygon
		google.maps.event.addListener(drawingManager, 'polygoncomplete', function (polygon) {
		    // assuming you want the points in a div with id="info"
		    document.getElementsByClassName('info').innerHTML += "polygon points:" + "<br>";
		    for (var i = 0; i < polygon.getPath().getLength(); i++) {
		        document.getElementsByClassName('info').innerHTML += polygon.getPath().getAt(i).toUrlValue(6) + "<br>";
		        latLngPattern.push({ lat:polygon.getPath().getAt(i).lat(), long:polygon.getPath().getAt(i).lng() });
		    }

		    drawingManager.setDrawingMode(null);
		    // drawingManager.setDrawingMode(google.maps.drawing.OverlayType.MARKER);
		    console.log(latLngPattern);
		    $("#polygonval").val(JSON.stringify(latLngPattern));
			$("#main_polygon_val").val(JSON.stringify(latLngPattern));
		    //latLngPattern.push({ lat:xy.lat(), long:xy.lng() })
		});
		
		// if(enable_polygon){
		// 	var pinedDestinations = $("#main_polygon_val").val(); 
	 //          //console.log(pinedDestinations);
	 //        if(pinedDestinations){
  //             	//map.setOptions({zoom:15});
  //               pinedDestinations = JSON.parse(pinedDestinations);
  //               pinedDestinations.forEach( (item) => {
  //                 console.log('lat', item.lat);
  //                 console.log('long', item.long);
  //                 destinations.push ( new google.maps.LatLng(item.lat, item.long) );
  //               });

  //               var polygonOptions = {path: destinations, strokeColor: "#d75151", strokeWeight: 2, fillColor: "#fff"};
  //               var polygon = new google.maps.Polygon(polygonOptions);
  //               polygon.setMap(map);

  //           }
		// }


	    google.maps.event.addListener(drawingManager, 'overlaycomplete', function (e) {

	        var newShape = e.overlay;
	        
	        newShape.type = e.type;

	        if (e.type !== google.maps.drawing.OverlayType.MARKER) {
	            // Switch back to non-drawing mode after drawing a shape.
	            drawingManager.setDrawingMode(google.maps.drawing.OverlayType.POLYGON);

	            // Add an event listener that selects the newly-drawn shape when the user
	            // mouses down on it.
	            google.maps.event.addListener(newShape, 'click', function (e) {
	            	if (e.vertex !== undefined) {
	                    if (newShape.type === google.maps.drawing.OverlayType.POLYGON) {
	                        var path = newShape.getPaths().getAt(e.path);
	                        path.removeAt(e.vertex);
	                        if (path.length < 3) {
	                            newShape.setMap(null);
	                        }
	                    }
	                    if (newShape.type === google.maps.drawing.OverlayType.POLYLINE) {
	                        var path = newShape.getPath();
	                        path.removeAt(e.vertex);
	                        if (path.length < 2) {
	                            newShape.setMap(null);
	                        }
	                    }
	                }
	                setSelection(newShape);
	            });
	            setSelection(newShape);
	        }
	        else {
	            google.maps.event.addListener(newShape, 'click', function (e) {
	                setSelection(newShape);
	            });
	            setSelection(newShape);
	        }
	    });


	    // Clear the current selection when the drawing mode is changed, or when the
	    // map is clicked.
	    google.maps.event.addListener(drawingManager, 'drawingmode_changed', clearSelection);
	    google.maps.event.addListener(map, 'click', clearSelection);
	    google.maps.event.addDomListener(document.getElementById('delete-button'), 'click', deleteSelectedShape);

	    buildColorPalette();
	}

	function clearSelection () {
	    if (selectedShape) {
	        if (selectedShape.type !== 'marker') {
	            selectedShape.setEditable(false);
	        }
	        
	        selectedShape = null;
	    }
	}

	function setSelection (shape) {
	    if (shape.type !== 'marker') {
	        clearSelection();
	        shape.setEditable(true);
	        selectColor(shape.get('fillColor') || shape.get('strokeColor'));
	    }
	    
	    selectedShape = shape;
	}

	function deleteSelectedShape () {
		// alert("test");
		$("#polygonval").val("");

	    if (selectedShape) {
	        selectedShape.setMap(null);
	        drawingManager.setDrawingMode(google.maps.drawing.OverlayType.POLYGON);
	    }
	}

	function selectColor (color) {
	    selectedColor = color;
	    for (var i = 0; i < colors.length; ++i) {
	        var currColor = colors[i];
	        colorButtons[currColor].style.border = currColor == color ? '2px solid #789' : '2px solid #fff';
	    }

	    // Retrieves the current options from the drawing manager and replaces the
	    // stroke or fill color as appropriate.
	    // var polylineOptions = drawingManager.get('polylineOptions');
	    // polylineOptions.strokeColor = color;
	    // drawingManager.set('polylineOptions', polylineOptions);

	    var rectangleOptions = drawingManager.get('rectangleOptions');
	    rectangleOptions.fillColor = color;
	    drawingManager.set('rectangleOptions', rectangleOptions);

	    var polygonOptions = drawingManager.get('polygonOptions');
	    polygonOptions.fillColor = color;
	    console.log('color:', polygonOptions.fillColor);
	    // set global color
	    gcolor = color;

	    drawingManager.set('polygonOptions', polygonOptions);
	}

	function setSelectedShapeColor (color) {
	    if (selectedShape) {
	        if (selectedShape.type == google.maps.drawing.OverlayType.POLYLINE) {
	            selectedShape.set('strokeColor', color);
	        } else {
	            selectedShape.set('fillColor', color);
	        }
	    }
	}

	function makeColorButton (color) {
	    var button = document.createElement('span');
	    button.className = 'color-button';
	    button.style.backgroundColor = color;
	    google.maps.event.addDomListener(button, 'click', function () {
	        selectColor(color);
	        setSelectedShapeColor(color);
	    });

	    return button;
	}

	function buildColorPalette () {
	    var colorPalette = document.getElementById('color-palette');
	    for (var i = 0; i < colors.length; ++i) {
	        var currColor = colors[i];
	        var colorButton = makeColorButton(currColor);
	        colorPalette.appendChild(colorButton);
	        colorButtons[currColor] = colorButton;
	    }
	    selectColor(colors[0]);
	}


	$("#sbt-poly-search").on("submit", function(e) {
		var query = $("#sbt-poly-search").serialize();
		var val = $("#polygonval").val();
		console.log(query);

		if (val) {

			$(".empty-polygon").text("");
	   		// $(".search-nav-polygon").removeClass("enable");
	   		$(".btn-draw").html('<i class="fa fa-object-ungroup"></i> Remove Pattern');
	        $(".search-disable").remove();
	        $(".col-lg-4.col-md-4.col-sm-5.left-disable").remove();
	        $("#color-palette").html('');
	        $(".btn-apply").hide();
	        $(".btn-remove-selected").hide();
	        $(".polygon-text").hide();
		    $("#map_canvas").removeClass('polygon-enable');

	   		mapSearch(query,1,'polygon-search');

		} else {
			$(".empty-polygon").text("No polygon selected!");
		}
   		return false;
   	});


});
