$(document).ready(function() {

	$.ajax({
        url: base_url+"home/customer/check_subscription_customer",
        dataType: 'json',
        success: function(data) {
            if(data.is_subscribe_email == 1) {
                $('input[name="email_updates"]').bootstrapSwitch('state', true);
                $('select[name="email_preference"]').val(data.email_schedule);
            } else {
                $('input[name="email_updates"]').bootstrapSwitch('state', false);
                $('select[name="email_preference"]').val(data.email_schedule);
            }
        }
    });
    
    $('input[name="email_updates"]').on('switchChange.bootstrapSwitch', function(event, state) {
        if(state) {
            $post = 'subscribe';
        } else {
            $post = 'unsubscribe';
        }

        $.ajax({
            url: base_url+"home/customer/set_email_subscription",
            type: "POST",
            data: { 
                switch_post:$post
            },
            dataType:'json'
        });
    });

    $("select[name='email_preference']").on("change", function(e) {
        e.preventDefault();
        var $schedule = $(this).val();

        $.ajax({
            url: base_url+"home/customer/set_email_schedule",
            type: "POST",
            data: {
                schedule:$schedule
            },
            dataType:'json',
            success:function(data) {
                if(data.success) {
                    $(".display-status").fadeIn(2000);
                    $(".display-status").fadeOut(2000);
                }
            }
        });
    });

    var delete_save_property = function (e)
    {
        e.preventDefault();
        var $this = $(this);
        var link = $this.attr("href");
        var property_id = $this.data("id");
        var mls_id = $this.data("property-id");
        var action = link+"/"+property_id+"/"+mls_id;
        // var baseurl = '<?php base_url(); ?>';
        // alert(baseurl);
        //alert(property_id)
        $.msgBox({
            title: "Are You Sure",
            content: "Would you like to remove this property?",
            type: "confirm",
            buttons: [{ value: "Yes" }, { value: "Cancel"}],
            success: function (result) {
                if (result == "Yes") {
                    $.ajax({
                        url : action,
                        type : "post",
                        dataType: "json",
                        beforeSend: function(){
                            $this.text("Loading...");
                        },
                        success: function( data){
                            console.log(data);
                            
                            if(data.success)
                            {
                                $( "#"+property_id ).fadeOut( "slow", function() {
                                    $.msgBox({
                                        title:"Success",
                                        content:"Property has been removed!",
                                        type:"info"
                                    });
                                 }); 

                                $(".propCount").html("<h3>My Saved Favorites ( "+data.savedProp+" )</h3>");
                            }
                            else
                            {
                                $.msgBox({
                                    title: "Error",
                                    content: "Failed to Delete property!",
                                    type: "error",
                                    buttons: [{ value: "Ok" }]                                
                                });
                            }
                        }
                    });
                }
            }
        });

    };

    var delete_save_searches = function (e)
    {
        e.preventDefault();
        var $this = $(this);
        var link = $this.attr("href");
        var property_id = $this.data("id");
        var action = link+"/"+property_id;
        //alert(property_id)
        $.msgBox({
            title: "Are You Sure",
            content: "Would you like to remove this property search?",
            type: "confirm",
            buttons: [{ value: "Yes" }, { value: "Cancel"}],
            success: function (result) {
                if (result == "Yes") {
                    $.ajax({
                        url : action,
                        type : "post",
                        dataType: "json",
                        beforeSend: function(){
                            $this.text("Loading...");
                        },
                        success: function( data){
                            console.log(data);
                            if(data.success)
                            {
                                $( "#search_"+property_id ).fadeOut( "slow", function() {
                                    $.msgBox({
                                        title:"Success",
                                        content:"Record has been removed!",
                                        type:"info"
                                    });
                                 });

                                $(".saveSearch").html("<h3>My Saved Searches ( "+data.savedSearch+" )</h3>");
                            }
                            else
                            {
                                $.msgBox({
                                    title: "Error",
                                    content: "Failed to Delete record!",
                                    type: "error",
                                    buttons: [{ value: "Ok" }]                                
                                });
                            }
                        }
                    });
                }
            }
        });

    }

    var delete_schedule_showing = function (e)
    {
        e.preventDefault();
        var $this = $(this);
        var link = $this.attr("href");
        var property_id = $this.data("id");
        var action = link+"/"+property_id;
        //alert(property_id)
        $.msgBox({
            title: "Are You Sure",
            content: "Would you like to remove this schedule showing?",
            type: "confirm",
            buttons: [{ value: "Yes" }, { value: "Cancel"}],
            success: function (result) {
                if (result == "Yes") {
                    $.ajax({
                        url : action,
                        type : "post",
                        dataType: "json",
                        beforeSend: function(){
                            $this.text("Loading...");
                        },
                        success: function( data){
                            console.log(data);
                            if(data.success)
                            {
                                $( "#showing_"+property_id ).fadeOut( "slow", function() {
                                    $.msgBox({
                                        title:"Success",
                                        content:"Record has been removed!",
                                        type:"info"
                                    });
                                 });

                                $(".schedShow").html("<h3>My Schedule Showings ( "+data.schedShowing+" )</h3>");
                            }
                            else
                            {
                                $.msgBox({
                                    title: "Error",
                                    content: "Failed to Delete record!",
                                    type: "error",
                                    buttons: [{ value: "Ok" }]                                
                                });
                            }
                        }
                    });
                }
            }
        });

    }

    var update_email_settings = function(e)
    {
        var val = $(this).val();
        var url = base_url+"home/customer/update_email_preferences_settings";
        $.ajax({
            url : url,
            type : "post",
            data : ( {"settings" : val }),
            dataType: "json",
            success: function( data){
                console.log(data);
                if(data.success)
                {
                    $.msgBox({
                        title:"Success",
                        content: "Alerts and Email Preferences has been successfully updated",
                        type:"info",
                        buttons: [{ value: "Ok" }]
                    });
                }
                else
                {
                    $.msgBox({
                        title: "Error",
                        content: "Failed to update Alerts and Email Preferences!",
                        type: "error",
                        buttons: [{ value: "Ok" }]                                
                    });
                }
            }
        });

    }

    $(".delete-save-property").on("click", delete_save_property);
    $(".delete-save-searches").on("click", delete_save_searches);
    $(".delete-customer-showing").on("click", delete_schedule_showing);
    $(".updates-email-settings").on("click", update_email_settings);


    $("#customer-request-info").on("submit", function(e) {

        e.preventDefault();
        $dataString = $(this).serialize();
        
        $.ajax({
            type: 'POST',
            url: $(this).attr('action'),
            data: $dataString,
            dataType: 'json',
            beforeSend: function() {
                console.log('sending');
                $(".submit-loader").show();
            },
            success: function( data){
                console.log(data);
                if(data.success) {
                    $(".submit-loader").hide();
                    $(".show-mess").html(data.message);
                    $(".show-mess").fadeOut(3000);
                    setTimeout(function() { $('#request-info-modal').modal('hide'); }, 3000);

                    if(!data.is_login){
                        location.reload();//window.location.href = data.redirect;
                    }

                } else {
                    $(".show-mess").html(data.errors);
                }
            }
        });
    });

    $(".change-phone").on("click", function() {
        $("#new-phone").val($(this).attr("data-phone-num"));
        console.log($("#new-phone").val());
    });

    $("#customer-change-phone").on("submit", function(e) {

        e.preventDefault();

        var phone_number = $("input[name='new_phone']").val();
        console.log(phone_number);
        var phone_length = phone_number.replace(/[^0-9]/g,"").length;

        if(phone_length < 10) {

            $(".show-mess").html("<span style='color:red'> Please enter a valid phone number!</span>");

        } else {

            $dataString = $(this).serialize();

            $.ajax({
                type: 'POST',
                url: $(this).attr('action'),
                data: $dataString,
                dataType: 'json',
                beforeSend: function() {
                    console.log('changing phone number to '+phone_number);
                },
                success: function( data){
                    console.log(data);
                    if(data.success) {
                        $("#customer-profile #phone").val(phone_number);
                        $("#new-phone").val(phone_number);
                        $(".change-phone").attr('data-phone-num', phone_number);
                        $(".show-mess").html(data.message);
                        $(".show-mess").show();
                        $(".show-mess").fadeOut(3000);
                        setTimeout(function() { 
                            $('#change-phone-modal').modal('hide'); 
                        }, 3000);
                    } else {
                        $(".show-mess").html(data.errors);
                    }
                }
            });
        }
        
    });
    
    $(".request_info").on("click", function() {
        //console.log($(this).attr('data-listingId'));
        //console.log($(this).attr('data-propertyType'));

        $("input[name='property_id']").val($(this).attr('data-listingId'));
        $("input[name='property_type']").val($(this).attr('data-propertyType'));
        $("input[name='property_address']").val($(this).attr('data-address'));

    });

});

//Reset edit profile form values when modal is closed
$('.modal').on('hidden.bs.modal', function(){
    $(this).find('form')[0].reset();
});

$("#customer-profile").on("submit", function(e) {
    e.preventDefault();
    $(".edit-profile-btn").prop('disabled', true);
    $(".edit-profile-btn").text('Loading..');

    var phone_number = $("input[name='profile[phone]']").val();
    var phone_length = phone_number.replace(/[^0-9]/g,"").length;

    if(phone_length < 10) {
        $.msgBox({
            title: "Error",
            content: "Please enter a valid phone number!",
            type: "error",
            buttons: [{ value: "Ok" }]                                
        });
        
        $(".edit-profile-btn").prop('disabled', false);
        $(".edit-profile-btn").text('Submit');
        return;
    }

    var $url = $(this).attr("action");
    var $dataString = $(this).serialize();

	$.ajax({
		url: $url,
		type: "POST",
		data: $dataString,
		dataType: "json",
		success: function(data) {
			if(data.success) {
				$.msgBox({
					title:"Success",
					content: data.message,
					type:"info",
					afterClose: function () {
						location.reload();
					}
				});
			} else {
				$.msgBox({
					title: "Error",
					content: data.message,
					type: "error",
					buttons: [{ value: "Ok" }]
				});
			}
			$(".edit-profile-btn").prop('disabled',false);
			$(".edit-profile-btn").text('Submit')
		}
	});
});

$(".delete_note_btn").on("click", function() {

    $.ajax({
        url: base_url+"home/customer/delete_customer_notes",
        type: "POST",
        data: {
            "property_id": $(this).attr("data-property_id")
        },
        dataType: "json",
        beforeSend: function() {
            $(this).prop("disabled", true);
        },
        success: function(data) {

            $(this).prop("disabled", false);

            if(data.success) {
                $(".show-mess").html("<span style='color:green'>"+data.message+"</span>");
                $(".show-mess").fadeOut(5000);
                location.reload();
            } else {
                $(".show-mess").html("<span style='color:red'>"+data.message+"</span>");
                $(".show-mess").fadeOut(5000);
            }
        }
    });

});

$(window).load(function() {

    //var phones = [{ "mask": "(###) ###-####"}, { "mask": "(###) ###-##############"}];
    /*var phones = [{"mask": "(###) ###-####"}];

    $('.phone_number').inputmask({
        mask: phones, 
        greedy: false, 
        definitions: { '#': { validator: "[0-9]", cardinality: 1 }}
    });*/

    //Format the phone and mobile number
    $(".phone_number").on("keypress", function(e) {

        var charCode = (typeof e.which == "number") ? e.which : e.keyCode;

        //Allow backspace and spaces only
        if (!charCode || charCode==8 || charCode == 32) {
            return;
        }

        var typedChar = String.fromCharCode(charCode);

        // Allow numeric characters
        if (/\d/.test(typedChar)) {
            return;
        }

        // Allow the minus sign (-) (- sign has no ascii value so i have to use this)
        if (typedChar == "-") {
            return;
        }

        // In all other cases, suppress the event
        return false;

    });

    $('#request-info-modal').on('hidden.bs.modal', function() {
        $(this).find('form')[0].reset();
    });

});
