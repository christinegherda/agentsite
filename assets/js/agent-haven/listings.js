$(document).ready(function() {
    //Save favorites Property Details
    $(".hearty").on("click", function() {
        $(".listing_id").val($(this).data("pro-id"));
        $(".property_type").val($(this).data("property-type"));
    });

    //Schedule Showing Modal
    $(".sched-modal").on("click", function() {
        $(".listing_id").val($(this).data("pro-id"));
        $(".property_type").val($(this).data("property-type"));
    });

    //Saved favorites on Search results
    $(".search_hearty").on("click", function() {
        $(".redirect_customer").val($(this).attr("data-redirect-customer"));
    });

    //Follow <span class="savethis-text">Saved Search</span>
    $(".saved_search").on("click", function() {
        $(".redirect_customer").val($(this).attr("data-redirect-customer"));
    });

    //Request Information
    $(".request_info_search").on("click", function() {
        $(".redirect_customer").val($(this).attr("data-redirect-customer"));
    });

    //Save Favorites
    var a = function(a) {

        a.preventDefault();

        if(isCustomLoggedin) {

            var b = $(this).data("pro-id");
            var prop_name = $(this).data("property-name");

            if($(this).hasClass("saveproperty")) {

                $.ajax({
                    url: base_url+"home/home/remove_favorite_property/"+b,
                    type: "POST",
                    dataType: "json",
                    success: function(data) {
                        console.log(data);
                        if(data.success) {
                            $("#isSaveFavorate_" + b).parent().removeClass("saveproperty");
                            $("#isSaveFavorate_" + b).html("<i class='fa fa-heart-o'></i>");
                            $("#isSavedFavorate_" + b).html("<small>Save</small>");
                        }
                    } 
                });

            } else  {
                
                var href = base_url + "home/home/save_favorates_properties";
                var c = href + "/" + b;

                $.ajax({
                    url: c,
                    type: "post",
                    data: {
                        'property_name': prop_name
                    },
                    dataType: "json",
                    success: function(a) {
                        console.log(a);
                        if(a.success) {
                            console.log(b);
                            console.log($("#isSaveFavorate_" + b));
                            $("#isSaveFavorate_" + b).parent().addClass("saveproperty");
                            $("#isSaveFavorate_" + b).addClass("yesheart");
                            $("#isSaveFavorate_" + b).html("<i class='fa fa-heart'></i>");
                            $("#isSavedFavorate_" + b).html("<small>Saved</small>");
                        } else {
                            window.location.href = a.redirect;
                        }   
                    }
                });
            }
            
        } else {

            $(".submission_type").val("save-favorate");
            $(".listing_id").val($(this).data("pro-id"));
            $(".property_name").val($(this).data("property-name"));
            $("#capture-leads-login").modal("show");

        }
    }

    $(".save-favorate").on("click", a)

    //Submit Sched Showing
    $(".sched_showing").off('submit').on('submit', function(e) {

        e.preventDefault();

        var b = $(this),
            c = b.attr("action"),
            d = b.serialize();

        $sched_msg = $(this).find("textarea[name='sched_for_showing']").val();

        if($sched_msg=="") {

            $("#resultInsert").html("<div class='alert alert-danger'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><i class='fa fa-exclamation-triangle' aria-hidden='true'></i> "+ 'You forgot to write a message!' +"</div>");

        } else {

            $.ajax({
                url: c,
                type: "post",
                data: d,
                dataType: "json",
                beforeSend: function() {
                    $("#sbt-showing").prop("disabled", !0), $("#sbt-showing").text("Loading...")
                },
                success: function(a) {

                    console.log(a);

                    $("#resultInsert").fadeIn("slow", function() {
                        $("#resultInsert").html("<div class='alert alert-success'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><i class='fa fa-info-circle'></i>" + a.message +"</div>");
                        $("#sbt-showing").prop("disabled", !1);
                        $("#sbt-showing").text("Submit");
                        setTimeout(function() {
                            $("#schedModal").modal('toggle');
                        }, 3000);
                    });
                     
                }
            });
        }
    });

    //Submit Customer questions
    $(".customer_questions").on('submit', function(e) {

        e.preventDefault();

        var b = $(this),
            c = b.attr("action"),
            d = b.serialize();

        /*var phone_number = $(".phone_number").val();
        var phone_length = phone_number.replace(/[^0-9]/g,"").length;

        if(phone_length < 10) {
            $(".question-mess").html("<div class='alert alert-danger alert-dismissible' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button><span class='alert alert-danger' style='display:block;'>Sending Failed: Please enter a valid phone number!</span></div>");
        } else {*/
            $.ajax({
                url: c,
                type: "post",
                data: d,
                dataType: "json",
                beforeSend: function() {
                    $(".submit-question-button").text("Loading...");
                }, success: function(a) {
                    if(a.success) {

                        $(".submit-question-button").text("Submit")
                        $(".question-mess").html(a.message)

                        if(!a.is_login) {
                            window.location.href = base_url+"customer-dashboard?success";
                        }
                        
                    } else {
                        $(".submit-question-button").text("Submit");
                        $(".question-mess").html(a.message);
                    }
                } 
                    
            });
        //}
    });

    //Submit Customer questions
    $("#seller_form").on('submit', function(e) {
        
        e.preventDefault();

        /*var phone_number = $(".phone_number").val();
        var phone_length = phone_number.replace(/[^0-9]/g,"").length;

        if(phone_length < 10) {
            $(".seller-return-msg").html("<div class='alert alert-danger alert-dismissible' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>Failed to submit: Please enter a valid phone number and mobile number</div>");
        } else {*/

            $.ajax({
                url: $(this).attr("action"),
                type: "POST",
                data: $(this).serialize(),
                dataType: "json",
                beforeSend: function() {
                    $(".seller-sbt-btn").prop("disable", true);
                    $(".seller-sbt-btn").text("Sending...");
                    console.log($(this).serialize());
                },
                success: function(data) {

                    $(".seller-sbt-btn").prop("disable", false);
                    $(".seller-sbt-btn").text("Get my home's worth");

                    if(data.success) {
                        $(".seller-return-msg").html("<div class='alert alert-success alert-dismissible' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>"+data.message+"</div>");
                        setTimeout(function() { 
                            window.location.href = data.redirect;
                        }, 3000);
                    } else {
                        $(".seller-return-msg").html("<div class='alert alert-danger alert-dismissible' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>"+data.message+"</div>");
                    }
                }
            });
        //}

    });

    //Submit Customer questions
    $("#buyer_form").on('submit', function(e) {
        
        e.preventDefault();
        var b = $(this),
            c = b.attr("action"),
            d = b.serialize();

        /*var phone_number = $(".phone_number").val();
        var phone_length = phone_number.replace(/[^0-9]/g,"").length;

        if(phone_length < 10) {
            $(".buyer-return-msg").html("<div class='alert alert-danger alert-dismissible' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>Failed to submit: Please enter a valid phone number and mobile number</div>");
        } else {*/

            $.ajax({
                url: c,
                type: "POST",
                data: d,
                dataType: "json",
                beforeSend: function() {
                    $(".buyer-sbt-btn").prop("disable", true);
                    $(".buyer-sbt-btn").text("Sending...");
                },
                success: function(data) {

                    $(".buyer-sbt-btn").prop("disable", false);
                    $(".buyer-sbt-btn").text("Submit Form");

                    if(data.success) {
                        $(".buyer-return-msg").html("<div class='alert alert-success alert-dismissible' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>"+data.message+"</div>");
                        setTimeout(function() { 
                            window.location.href = data.redirect;
                        }, 3000);

                    } else {
                        $(".buyer-return-msg").html("<div class='alert alert-danger alert-dismissible' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>"+data.message+"</div>");
                    }
                }
            });
        //}

    });

    //Save <span class="savethis-text">Saved Search</span>es
    $(".follow-search").off('click').on('click', function(e) {

            e.preventDefault();
            var b = $(this).attr("href");
            $.ajax({
                url: b,
                type: "post",
                dataType: "json",
                success: function(a) {
                    console.log(a);
                    if(a.success){
                        $("#moreFiltersToggle").html("<i class='fa fa-save'></i> <span class='savethis-text'>Saved Search</span>").prop("disable", !0).addClass("saved-search").removeClass("follow-search").css("background-color", "#e24545");
                        $(".saved-search").removeAttr("href");
                    } else{
                        window.location.href = a.redirect;
                    }
                    
                }
            })
    });

    $(".follow-search-red").on('click', function(e) {
        e.preventDefault();
    });

});