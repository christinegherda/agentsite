(function($) {

  var sbt_domain = function (eve){
    eve.preventDefault();
    $("#check_domain_sbt").prop('disabled', true);

    var datastring = $("#domain-form").serialize();
        $.ajax({
            type: "GET",
            url: $("#domain-form").attr('action'),
            data: datastring,
            beforeSend: function() {

                $("#checkDomain").hide("fast");
                $("#checkDomainLoad").show("fast");
            },
            success: function(e) {
                $("#checkDomain").show("fast");
                $("#checkDomainLoad").hide("fast");
                
                var c = jQuery.parseJSON(e);   
                // console.log(c.domainName);
                var domainName = $.map(c.domainName, function(value, index) {
                    return [value];
                });

                if( c.status == 1 ) {
                    // checkDomain
                    var domainPrice = $.map(c.domainPrice, function(value, index) {
                        return [value];
                    });
                     /*$("#domainResult").html("<h4>Domain <span>" + domainName + "</span> is available at price <strong>$" + domainPrice + "</strong>. Click <a href='javacript:void(0);'>here</a> to purchase this domain</h4>"); */
                    $("#domainResult").html("<h4>Domain <span>" + domainName + "</span> is available. Click <a href='javascript:void(0)' class='purchase-domain' data-domain_name_avail="+domainName+" >here</a> to reserve this domain</h4>"); 
                }
                else {
                    console.log(c);
                    window.location = $("#domainUrl").val() + "?domain=" + c.domain + "&tld=" + c.tld + "&status=0";
                    // $("#domainResult").html("<h4 class='taken'><span>"+domainName+"</span> is already taken.</h4><h4 class='featured'>Featured Domains:</h4>");
                }

                $('#check_domain_sbt').prop('disabled', false);
                $(".purchase-domain").on("click", show_domain_modal_available);
            } 
        });
}

$("#domain-form").on("submit", sbt_domain);

var purchase_domain_avail = function (){
        var $this = $(this);            
        var domain_name = $this.data("domain-name");
        var user_id = $("#user_agent_id").val();
        var url = $("#purchaseDomainUrl").val();
        var user_name = $("#user_name").val();

        $.ajax({
            type: "GET",
            url: url,
            data: {
                domain_name: domain_name,
                agentId : user_id,
                user_name : user_name
            },
            dataType : "json",
            beforeSend: function() {
                console.log("Sending..");
                $this.text("Loading...");
            },
            success: function(data) {
                
                if(data.success)
                {   location.reload();
                    // $("#domainModal").fadeOut("slow", function () {
                    //     $('#domainModal').modal('hide');
                    // });

                    // $( "#custom_domain_panel" ).fadeOut( "slow", function() {
                    //     $("#custom_domain_panel").hide();
                    //     var message = '<div class="alert alert-success" role="alert">'+data.message+'<div>';
                    //     $(".response_domain").append(message);
                    // });           
                }
                else
                {   
                    location.reload();
                    // $("#domainModal").fadeOut("slow", function () {
                    //     $('#domainModal').modal('hide');
                    // });
                    // $this.text("reserve this domain free for me");
                    // var message = '<div class="alert alert-danger" role="alert">'+data.error+'<div>';
                    // $(".moreDetails").append(message);
                }
            
            }
        });

        console.log(url);
      };

   var show_domain_modal_available = function () {        

        var $this = $(this);            
        var domain_name = $(this).attr("data-domain_name_avail");
        
        $(".confirm-purchase-domain").attr("data-domain-name", domain_name);

        $("#domainModal").modal('show');
        $(".confirm-purchase-domain").on("click",purchase_domain_avail);
    };

})(jQuery);

$(document).ready(function (){
    console.log("Domains Loaded"); 
     $("#domainUrl").val();
     $.ajax({
        type: "GET",
        url: $("#domainUrl").val(),
        data: {
            domain_tld_get: "1",
            domain : $("#getDomain").val()
        },
        beforeSend: function() {
            console.log("Sending..");
        },
        success: function(e) {
            $(".fetch-available-domains").hide();
            $(".availableDomains").show();
            var c = jQuery.parseJSON(e);
            // var domainCount = c.tlds.DomainCount[0];   

            var domainCount = $.map(c.domainCount, function(value, index) {
                return [value];
            });
            var splitStr = '';
            var domainStatus = [];
            var statusCode = [];
            console.log(c);
            for(var i = 0; i < c.domains.length; i ++ ) {
                splitStr = c.domains[i].split("_");
                for (var j = 0; j < domainCount; j++) {
                    if( splitStr[0] == "Domain" + (j + 1) ) {
                        domainStatus.push(splitStr[1]); 
                    }
                    if( splitStr[0] == "RRPCode" + (j + 1) ) {
                        statusCode.push(splitStr[1]); 
                    }
                }
            }
            var htmlInsertString = "";
            var other_ext = false;
            for(var i = 0; i < domainStatus.length; i ++) {
                if(statusCode[i] == '210') {                    
                    var domainName = domainStatus[i].split(".");
                    var user_id = $("#user_agent_id").val();                    
                    
                    allowDomainExt  = ["com","org","net"];

                    if( jQuery.inArray(domainName[1], allowDomainExt) != -1 )
                    {
                        other_ext = true;
                        // Domain is Available
                        htmlInsertString += '<li><div class="col-md-5 col-sm-5">'+domainStatus[i]+'</div><div class="col-md-6 col-sm-5"><a class="btn btn-default btn-create purchase-domain" data-user_id="'+user_id+'" data-domain_name="'+domainStatus[i]+'" >Reserve this domain free for me</a></div></li>';    
                    }
                                        
                }
                else {
                    // Domain is Unavailable
                }
            }

            if( !other_ext )
            {
                for(var i = 0; i < domainStatus.length; i ++) {
                    if(statusCode[i] == '210') {                    
                        var domainName = domainStatus[i].split(".");
                        var user_id = $("#user_agent_id").val();                    
                                               
                            // Domain is Available
                            htmlInsertString += '<li><div class="col-md-5 col-sm-5">'+domainStatus[i]+'</div><div class="col-md-6 col-sm-5"><a class="btn btn-default btn-create purchase-domain" data-user_id="'+user_id+'" data-domain_name="'+domainStatus[i]+'" >Reserve this domain free for me</a></div></li>';    
                        
                                            
                    }
                    else {
                        // Domain is Unavailable
                    }
                }
            }

            $("#domainResult ul.domain-list").html(htmlInsertString);
            $(".purchase-domain").on("click", show_domain_modal);

            console.log(domainStatus);
            console.log(statusCode);
        }
    });

var purchase_domain = function (){
        var $this = $(this);            
        var domain_name = $this.data("domain-name");
        var user_id = $("#user_agent_id").val();
        var url = $("#purchaseDomainUrl").val();
        var user_name = $("#user_name").val();

        $.ajax({
            type: "GET",
            url: url,
            data: {
                domain_name: domain_name,
                agentId : user_id,
                user_name : user_name
            },
            dataType : "json",
            beforeSend: function() {
                console.log("Sending..");
                $this.text("Loading...");
            },
            success: function(data) {
                
                if(data.success)
                {
                    location.reload();

                    // $("#domainModal").fadeOut("slow", function () {
                    //     $('#domainModal').modal('hide');
                    // });

                    // $( "#custom_domain_panel" ).fadeOut( "slow", function() {
                    //     $("#custom_domain_panel").hide();
                    //     var message = '<div class="alert alert-success" role="alert">'+data.message+'<div>';
                    //     $(".response_domain").append(message);
                    // });           
                }
                else
                {
                    location.reload(); 

                    // $("#domainModal").fadeOut("slow", function () {
                    //     $('#domainModal').modal('hide');
                    // });
                    // $this.text("reserve this domain free for me");
                    // var message = '<div class="alert alert-danger" role="alert">'+data.error+'<div>';
                    // $(".moreDetails").append(message);
                }
            
            }
        });

        //console.log(url);
      };

   var show_domain_modal = function () {        

        var $this = $(this);            
        var domain_name = $this.data("domain_name");

        $(".confirm-purchase-domain").attr("data-domain-name", domain_name);

        $("#domainModal").modal('show');
        $(".confirm-purchase-domain").on("click",purchase_domain);
    };
});
