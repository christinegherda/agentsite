function testFunction(a) {
    console.log(a)
}

function setFieldValue(a, b) {
    $(document).ready(function() {
        "Any" === b ? ($("#" + a).val(""), $("#" + a + "_label").html(b)) : ($("#" + a).val(b), $("#" + a + "_label").html(b + "+"))
    })
}

function addCommas(a) {
    a += "", x = a.split("."), x1 = x[0], x2 = x.length > 1 ? "." + x[1] : "";
    for (var b = /(\d+)(\d{3})/; b.test(x1);) x1 = x1.replace(b, "$1,$2");
    return x1 + x2
}

function calculate() {
    var a = $("#mortgageCalculator").serialize(),
        b = $("input[name='calculator_url']").val();
    $.ajax({
        url: b,
        type: "POST",
        data: a,
        dataType: "json",
        beforeSend: function() {
            console.log(a), 
            console.log("sending data"), 
            $("#morg_calc_btn").text("Calculating...")
        },
        success: function(a) {
            $("#mortgage-result").html("<h4 class='total-mortgage'>Monthly: $" + a.monthly_payment + "</h4>"), $("#morg_calc_btn").text("Calculate")
        }
    })
}

function customer_fb_login() {
    var a = screen.width / 6,
        b = screen.height / 6;
    window.open(customer_fb_url, "targetWindow", "toolbar=no,titlebar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no,width=700,height=450,top=" + b + ",left=" + a)
}
$(document).ready(function() {
    function f(a) {
        $(a.target).prev(".panel-heading").find("i.indicator").toggleClass("glyphicon-chevron-down glyphicon-chevron-up")
    }
    $("#mortgageCalculator").on("submit", function(a) {
        calculate(), a.preventDefault()
    }), $("#sale_price").on("input", function() {
        $("#sale_price").val() && $("#sale_price_label").html(addCommas($("#sale_price").val()))
    }), $("#down_percent").on("input", function() {
        $("#down_percent").val() && $("#down_percent_label").html($("#down_percent").val())
    }), $("#mortgage_interest_percent").on("input", function() {
        $("#mortgage_interest_percent").val() && $("#mortgage_interest_percent_label").html($("#mortgage_interest_percent").val())
    }), $("#year_term").on("input", function() {
        $("#year_term").val() && $("#year_term_label").html($("#year_term").val())
    });
    var a = 0;
    $("#residential").click(function() {
        $(this).is(":checked") ? ($("#residential_types").css("display", "block"), a += 1) : ($("#residential_types").css("display", "none"), $("input[name='residential_type[]']").each(function() {
            $(this).prop("checked", !1)
        }), a -= 1)
    }), $("#multifamily").click(function() {
        $(this).is(":checked") ? ($("#multifamily_types").css("display", "block"), a += 1) : ($("#multifamily_types").css("display", "none"), $("input[name='multifamily_type[]']").each(function() {
            $(this).prop("checked", !1)
        }), a -= 1)
    }), $(window).scroll(function(a) {
        var b = $(this).scrollTop();
        b > 25 ? ($(".home-navbar").addClass("nav-lighten"), $(".main-nav").removeClass("nav-darken")) : ($(".home-navbar").removeClass("nav-lighten"), $(".main-nav").addClass("nav-darken")), b > 100 ? $(".scrolltop").fadeIn() : $(".scrolltop").fadeOut()
    }), $(".scrolltop").click(function() {
        return $("html, body").animate({
            scrollTop: 0
        }, 600), !1
    }), $("#land").click(function() {
        $(this).is(":checked") ? ($("#land_types").css("display", "block"), a += 1) : ($("#land_types").css("display", "none"), $("input[name='land_type[]']").each(function() {
            $(this).prop("checked", !1)
        }), a -= 1)
    }), $("#commercial").click(function() {
        $(this).is(":checked") ? ($("#commercial_types").css("display", "block"), a += 1) : ($("#commercial_types").css("display", "none"), $("input[name='commercial_type[]']").each(function() {
            $(this).prop("checked", !1)
        }), a -= 1)
    }), $("#rental").click(function() {
        $(this).is(":checked") ? ($("#rental_types").css("display", "block"), a += 1) : ($("#rental_types").css("display", "none"), $("input[name='rental_type[]']").each(function() {
            $(this).prop("checked", !1)
        }), a -= 1)
    }), $("#residential, #multifamily, #land, #commercial, #rental").on("click", function() {
        a > 1 ? ($("#property_sub_type_container").css("display", "none"), $("input[name='residential_type[]']").each(function() {
            $(this).prop("checked", !1)
        }), $("input[name='multifamily_type[]']").each(function() {
            $(this).prop("checked", !1)
        }), $("input[name='land_type[]']").each(function() {
            $(this).prop("checked", !1)
        }), $("input[name='commercial_type[]']").each(function() {
            $(this).prop("checked", !1)
        }), $("input[name='rental_type[]']").each(function() {
            $(this).prop("checked", !1)
        })) : 0 == a ? $("#property_sub_type_container").css("display", "none") : $("#property_sub_type_container").css("display", "block")
    }), $("#more-property-types").click(function() {
        $(".show-more-property-types").toggle()
    }), $(".moreFiltersToggle").click(function() {
        $("#more-filters-area").toggle()
    }), $(".dropdown-toggle").click(function() {
        $("#more-filters-area").toggle(), "block" == $("#more-filters-area").css("display") && $("#more-filters-area").hide()
    }), $("#min_price").on("input", function() {
        $("#min_price").val() && $("#price_label").html(addCommas($("#min_price").val())), console.log("Value: " + $("#min_price").val())
    }), $("#max_price").on("input", function() {
        $("#min_price").val() && $("#max_price").val() ? $("#price_label").html(addCommas($("#min_price").val()) + " - " + addCommas($("#max_price").val())) : $("#price_label").html(addCommas($("#max_price").val()))
    }), $(".dropdown-menu a").click(function(a) {
        a.preventDefault()
    });
    var c = ($(".photo-gallery .col-md-2").length, $(".photo-gallery .col-md-2")),
        d = 0;
    c.each(function() {
        8 == ++d ? c.css("display", "block") : d > 8 && $(this).css("display", "none")
    }), $(".view-map-button").click(function(a) {
        $(".property-map").toggleClass("toggle-map"), $(".property-map").hasClass("toggle-map") ? $(".view-map-button").html("<i class='fa fa-map'></i> <span class='show-map-label'>Hide Map</span>") : $(".view-map-button").html("<i class='fa fa-map'></i> <span class='show-map-label'>Show Map</span>")
    }), $(".filter-search-button").click(function() {
        $(".filter-panel,.contact-panel").slideToggle("slow")
    }), $(".contact-agent-button").click(function() {
        $(".contact-agent").slideToggle("slow")
    }), $(".filter-link").click(function() {
        $(this);
        $(this).stop().next(".sub-filter-simple,.sub-filter,.price-filter").css("display", "block").parent().siblings().find(".sub-filter-simple,.sub-filter,.price-filter").css("display", "none")
    }), $(".sub-filter-simple li a").on("click", function() {
        var a = $(this),
            b = a.text();
        a.parents("li").find(".chosen-option").text(b), a.stop().parents(".sub-filter-simple").slideUp(150)
    }), $(".property-main-content").click(function() {
        $(this);
        $(".sub-filter-simple,.sub-filter,.price-filter").stop().css("display", "none")
    }), $("#min-price").on("change", function() {
        var a = $(this),
            b = a.val();
        console.log(b), a.parents("li").find(".chosen-min").text(b)
    }), $("#max-price").on("change", function() {
        var a = $(this),
            b = a.val();
        console.log(b), a.parents("li").find(".chosen-max").text(b)
    }), $(".btn-show-filter").click(function() {
        $(".advance-search-form .navbar").slideToggle(250), $(".advance-search-form .navbar").is(":visible") && $("#more-filters-area").hide()
    }), $(".home-search").on("focus", function() {
        var a = $(this);
        0 == a.val() ? $(".help-info").stop().show() : $(".help-info").stop().slideUp()
    }), $(".home-search").on("keypress", function() {
        var a = $(this);
        0 == a.val() ? $(".help-info").stop().show() : $(".help-info").stop().slideUp()
    }), $(".home-search").on("mouseleave", function() {
        $(".help-info").stop().slideUp()
    });
    var e = window.location;
    $('ul.main-nav a[href="' + e + '"]').parent().addClass("active"), $("ul.main-nav a").filter(function() {
        return this.href == e
    }).parent().addClass("active"), $("#accordion").on("hidden.bs.collapse", f), $("#accordion").on("shown.bs.collapse", f), $(".view-full").click(function() {
        $("li.view-full-spec").addClass("active"), $("li.view-full-details").removeClass("active")
    }), $(".advance-search").click(function() {
        $(".search-info").show().delay(2e3).fadeOut(2e3)
    }), $("a").tooltip(), $(".alert-flash").delay(3e3).fadeOut(2e3)


    	// iframe width switcher
	$('.size-container ul li').on('click',function(){
		var $this = $(this); 
		var $switcher = $('#iframe-switcher');
		var $iframeWrapper = $('.iframe-wrapper');

		if($this.is('#phone-portrait')){
			
			$iframeWrapper.css('height',1000+'px');
			$this.addClass('active').siblings().removeClass('active');
			$switcher.removeClass().addClass('phone-portrait');
		}
		else if($this.is('#tablet-portrait')){

			$this.addClass('active').siblings().removeClass('active');
			$switcher.removeClass().addClass('tablet-portrait');	
		}
		else if($this.is('#tablet-landscape')){
			
			$iframeWrapper.css('height',768+'px');
			$this.addClass('active').siblings().removeClass('active');
			$switcher.removeClass().addClass('tablet-landscape');	
		}
		else if($this.is('#phone-landscape')){

			$iframeWrapper.css('height',375+'px');
			$this.addClass('active').siblings().removeClass('active');
			$switcher.removeClass().addClass('phone-landscape');	
		}
		else{
			$iframeWrapper.css('height',1000+'px');
			$this.addClass('active').siblings().removeClass('active');
			$switcher.removeClass().addClass('desktop');
		}

	});

	// dynamic width for featured title
	$(window).load(function(){
		var $trapezoid = $('.trapezoid');
		var $titleWidth = $('.featured-listing-area h2.section-title span').width();
		
		$trapezoid.width($titleWidth+'px');
	});

});

$(window).ready(function() {
	'use strict';
	$(function() {
	    $('.slider-area').vegas({
	        slides: [
	            { src: '../assets/images/dashboard/demo/agent-haven/property4.jpg' },
	            { src: '../assets/images/dashboard/demo/agent-haven/property3.jpg' }
		        ]
	    });
	});
});

