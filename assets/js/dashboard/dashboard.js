$(document).ready( function (){

  $leadsChart = $("#leads-chart").data("leads-chart");
  $chartDays = $("#leads-chart").data("chart-days");
  $propertyChart = $("#propertylist-chart").data("property-chart");
  console.log( $leadsChart );
  $(function () {
      "use strict";

      //Date picker
      $('#datepickerfrom').datepicker({
        autoclose: true
      });
      $('#datepickerto').datepicker({
        autoclose: true
      });
      $('#datepickerfrom1').datepicker({
        autoclose: true
      });
      $('#datepickerto1').datepicker({
        autoclose: true
      });

      // get json data
     

      // LEADS CHART
      var line = new Morris.Line({
        element: 'leads-chart',
        resize: true,
        data: $leadsChart, 
        xkey: 'period',
        ykeys: ['a_leads', 'b_leads'],
        labels: ['New Lead', 'Scheduled for showing for new leads'],
        //events: $chartDays,
        lineColors: ['#0096ff', '#ffa200'],
        eventLineColors: ['#e2e2e2'],
        hideHover: 'auto'
      });

     // PROPERTY LIST CHART
      var line = new Morris.Line({
        element: 'propertylist-chart',
        resize: true,
        // data: [
        //   { period: '2011', a: 0, b: 0, c: 0 },
        //   { period: '2012', a: 0,  b: 0 , c: 0},
        //   { period: '2013', a: 0,  b: 0, c: 0 },
        //   { period: '2014', a: 0,  b: 0 , c: 0},
        //   { period: '2015', a: 0,  b: 0, c: 0 },
        //   { period: '2016', a: 75,  b: 65, c: 90 },
        //   { period: '2017', a: 0, b: 0, c: 0 }
        // ],
        data: $propertyChart,
        xkey: 'period',
        ykeys: ['a_leads', 'b_leads', 'c_leads'],
        labels: ['Active Listing', 'Closed Listing', 'Pending Listing'],
        lineColors: ['#0bdf0b', '#ff0600', '#ffa200'],
        eventLineColors: ['#e2e2e2'],
        hideHover: 'auto'
      });


    });

});


(function($) {

   var chart_leads_filter = function(e){
      e.preventDefault();

      var $ajaxLeadsChart = "";
      var $ajaxLeadsChartDays = "";

      $("#chart-morris-area").fadeIn("slow");
      
      var url = $(this).attr("action");
      var datas = $(this).serialize();

      $.ajax({
            type: "POST",
            url: url,
            data: datas,
            dataType : "json",
            beforeSend: function(){
              console.log("fitching...")
            },
            success: function(datas) {

               $('#leads-chart').empty();
               var $ajaxLeadsChart = datas.leads_chart;
               var $ajaxLeadsChartDays = datas.chart_days;

               var line = new Morris.Line({
                  element: 'leads-chart',
                  resize: true,
                  data: $ajaxLeadsChart, 
                  xkey: 'period',
                  ykeys: ['a_leads', 'b_leads'],
                  labels: ['New Lead', 'Scheduled for showing for new leads'],
                  events: $ajaxLeadsChartDays,
                  lineColors: ['#0096ff', '#ffa200'],
                  eventLineColors: ['#e2e2e2'],
                  hideHover: 'auto'
                });

            }

        });
     

       $("#chart-morris-area").fadeOut("slow");
   };

   var chart_property_filter = function(e){
      e.preventDefault();

      $("#chart-morris-area-property").fadeIn("slow");
      
      var url = $(this).attr("action");
      var datas = $(this).serialize();

      $.ajax({
            type: "POST",
            url: url,
            data: datas,
            dataType : "json",
            beforeSend: function(){
              console.log("fitching...")
            },
            success: function(datas) {

               $('#propertylist-chart').empty();
               var $ajaxPropertyChart = datas.property_chart;
               var $ajaxPropertyChartDays = datas.chart_days;

                // PROPERTY LIST CHART
                var line = new Morris.Line({
                  element: 'propertylist-chart',
                  resize: true,
                  data:$ajaxPropertyChart,
                  xkey: 'period',
                  ykeys: ['a_leads', 'b_leads', 'c_leads'],
                  labels: ['Active Listing', 'Pending Listing', 'Sold Listing'],
                  events: $ajaxPropertyChartDays,
                  lineColors: ['#0bdf0b', '#ff0600', '#ffa200'],
                  eventLineColors: ['#e2e2e2'],
                  hideHover: 'auto'
                });

            }

        });     

       $("#chart-morris-area-property").fadeOut("slow");
   };

   $("#chart-leads-filter-date").on("submit", chart_leads_filter );
   $("#chart-property-filter-date").on("submit", chart_property_filter );
    
})(jQuery);
