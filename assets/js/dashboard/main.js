$(document).ready(function() {
    // Detailed Target
    $(".exclude-link").click(function(){
        $(this, ".or-text").toggle();
        $(".exclude-detail").toggle();
    });
    $(".narrow-link").click(function(){
        $(this, ".or-text").toggle();
        $(".narrow-detail").toggle();
    });
    $(".target").click(function(){
        console.log($(this).parent().parent());
        $(this).parent().parent().find(".is-dropdown.browse").toggle();
        $(this).parent().parent().find(".has-dropdown-browse").toggleClass("btn-active");
    });

    $(".dropdown-submenu a").click(function(){
        $(this).parent().find(".sub-dropdown-menu").toggle();
        
    });

     $(".dropdown-submenu-2 a").click(function(){
        $(this).parent().find(".sub-dropdown-menu-2").toggle();
    });

    $(".has-dropdown-suggested").click(function(){
        $(this).toggleClass("btn-active");
        $(".has-dropdown-browse").removeClass("btn-active");
        $(this).parent().parent().find(".suggested").toggle();
        $(this).parent().parent().find(".browse").hide();
    });
    $(".has-dropdown-browse").click(function(){
        $(this).toggleClass("btn-active");
        $(".has-dropdown-suggested").removeClass("btn-active");
        $(this).parent().parent().find(".browse").toggle();
        $(this).parent().parent().find(".suggested").hide();
    });
    $(".close-detail").click(function(){
        $(this).parent().parent().hide();
    });

    // accordion
    $(".panel-collapse").hide();
    $(".moreDetails").show();
    $(".panel-heading").on("click",function(){
        var $this = $(this);
        
        $this.stop().next().slideDown(250).parent().siblings().find('.panel-collapse').slideUp(250);
        $this.find('.fa-plus-circle').removeClass('fa-plus-circle').addClass('fa-minus-circle')
            .parents('.panel-default').siblings().find('.fa-minus-circle').removeClass('fa-minus-circle').addClass('fa-plus-circle');
    });

    //preview
    $('.calltoaction').on('change', function() {
      console.log( this.value ); // or $(this).val()
      if (this.value == "No Button") {
        $('.calltoactionbtn').hide();
      } else {
        $('.calltoactionbtn').show();
      }
      $('.calltoactionbtn').text(this.value);
    });

    $('.headline').on('change', function() {
      var hline = $(".headline option:selected").text();
      $('.head-line').text(hline);
    });

    $('.headline').keyup(function() {
        var headline = this.value;
        $('.head-line').text(this.value);
    });
    $('#abouttext').keyup(function() {
        var abouttext = this.value;
        $(".abouttext").text(abouttext);
    });
    $('#linkdesc').keyup(function() {
        var linkdesc = this.value;
        $(".linkdesc").text(linkdesc);
    });
    $('#displaylink').keyup(function() {
        var displaylink = this.value;
        $(".displaylink").text(displaylink);
    });

    // add photo upload
    $('#three').fileUploader({
        useFileIcons: false,
        fileMaxSize: 50,
        totalMaxSize: 50,
        name: "three",
        linkButtonContent: '<i>Open</i>',
        deleteButtonContent: '<i>Remove</i>',
        filenameTest: function(fileName, fileExt, $container) {
            var allowedExts = ["jpg", "jpeg" , "png"];
            var $info = $('<div class="center"></div>');
            var proceed = true;

            // length check
            if (fileName.length > 50) {
                $info.html('Name too long!');
                proceed = false;
            }
            // replace not allowed characters
            fileName = fileName.replace(" ", "-");

            // extension check
            if (allowedExts.indexOf(fileExt) < 0) {
                $info.html('Filetype not allowed!');
                proceed = false;
            }

            // show an error message
            if (!proceed) {
                $container.append($info);

                setTimeout(function() {
                    $info.animate({opacity: 0}, 300, function() {
                        $(this).remove();
                    });
                }, 2000);
                return false;
            }
            console.log($container);
            return fileName;
        },
        langs: {
            "en":{
                dropZone_msg: "Drop your images here or click to upload."
            }
        }
    });

    $("#update-account-info").on("click", function(e) {
        e.preventDefault();
        $this = $(this);
        $url = window.location.protocol + "//" + window.location.host + "/" + "agent-site/dashboard/update_account_info";
        $.ajax({
            url: $url,
            dataType: 'json',
            beforeSend: function() {
                console.log("Loading");
                $this.text("Updating Account Information...");
            },
            success: function(data) {
                console.log(data);
                if(data.success) {
                    location.reload();
                }
            }
        });
    });

    $("#update-system-info").on("click", function(e) {
        e.preventDefault();
        $this = $(this);
        $url = window.location.protocol + "//" + window.location.host + "/" + "agent-site/dashboard/update_system_info";
        $.ajax({
            url: $url,
            dataType: 'json',
            beforeSend: function() {
                console.log("Loading");
                $this.text("Updating System Information...");
            },
            success: function(data) {
                console.log(data);
                if(data.success) {
                    location.reload();
                }
            }
        });
    });

    var str  = document.URL;
    var n=str.split("/");
    var word = n[n.length - 1];
    // console.log(word);
    //
    $(".disabled-agent-btn").click(function(){
        //alert("Hello World!");
        $("#agent-idx-website-modal").modal("show");
    });

    if(word == "messages" || word == "buyer_leads" || word == "seller_leads") {
        var b = $(window).height() - 200;
        $(".message-clients").css("min-height", b - $(".main-footer").outerHeight());
    }

    //Sync My Saved Searches 
    $("#sync_saved_searches").on("click", function(e) {
        e.preventDefault();

        $.ajax({
            url: 'property/saved_search_db',
            dataType: 'json',
            beforeSend: function() {
                console.log("loading...");
                $("#sync_saved_searches").text("Syncing Saved Searches...");
            },
            success: function(data) { 
                console.log(data); 
                if(data.success) {
                    window.location.href = data.redirect;
                } else {
                    window.location.href = data.redirect;
                }
            }
        });
    });

    //Update Saved Searches 
    $("#update_saved_searches").on("click", function(e) {
        e.preventDefault();

        $.ajax({
            url: 'property/update_saved_search_db',
            dataType: 'json',
            beforeSend: function() {
                console.log("loading...");
                $("#update_saved_searches").text("Updating Saved Searches...");
            },
            success: function(data) { 
                console.log(data); 
                if(data.success) {
                    window.location.href = data.redirect;
                } else {
                    window.location.href = data.redirect;
                }
            }
        });
    });

    //Sync My Contacts
    $("#sync_contacts").on("click", function(e) {
        e.preventDefault();

        $.ajax({
            url: 'leads/SaveContactList',
            dataType: 'json',
            beforeSend: function() {
                console.log("loading...");
                $("#sync_contacts").text("Syncing Contacts...");
            },
            success: function(data) { 
                console.log(data); 
                if(data.success) {
                    window.location.href = data.redirect;
                } else {
                    window.location.href = data.redirect;
                }
            }
        });
    });

    //Submitting property details
    $("button#property-details-submit").click(function() {
        submit_property_details();
    });

    function submit_property_details() {

        var dataString = $("#property-form-description, #property-form-specification").serialize();
        console.log(dataString);

        $.ajax( {
            type: 'POST',
            url: 'save_property_details',
            data: dataString,
            dataType: 'json',
            success: function(data) {
                console.log(data);
                if(data.success)
                {
                    window.location.href = data.redirect;
                    location.reload();
                }
            }
        });
    }
    
    //Choose theme
    $("#agent-haven-btn").click(function() {
        $("input[name='theme']").val("agent-haven");
    });

    $("#greenery-btn").click(function() {
        $("input[name='theme']").val("greenery");
    });

    $("#proper-btn").click(function() {
        $("input[name='theme']").val("proper");
    });

	// toggle option
    $('.toggle-option').click(function(){
    	$(this).stop().parents('.page-actions').find('.options').slideToggle(150);
    });

    // close option
    $('.btn-cancel, .options .btn-submit').click(function(){
    	$(this).stop().parents('.page-actions').find('.options').hide(150);
    });
	
    // option status
    $('.options .btn-submit').click(function(){
        if($(this).parents('.page-actions').find('.options input').is(':checked')){
            var checkedValue = $(this).parents('.page-actions').find('.options input:checked').val();
            $(this).parents('.page-actions').find('.toggle-option').html(checkedValue);
        }
    });

	// Choose Theme
	$("div#selected").show();
	$("div#preview").hide();
    $("span.flash-data").fadeOut(3000);
	$("div.flash-data").fadeOut(5000);

    function saveIdx() {
        
        $('.save-loading').show();

        var formData = $('#saveIdxForm').serialize();

        $.ajax({
            url: 'idx_integration/check_idx',
            type: 'POST',
            data: formData,
            dataType: 'json',
            beforeSend: function() {
                console.log(formData);
            },
            success: function(data) {
                if(data.success) {
                    console.log("valid");
                    $.ajax({
                        url: 'idx_integration/add_credentials',
                        type: 'POST',
                        data: formData,
                        dataType: 'json',
                        success: function(data) {
                            if(data.success == 'insert') {
                                $("span.display-status").text("Successfully Inserted").show().fadeOut(3000).css({
                                    "display": "block",
                                    "color": "green",
                                    "text-align": "center"
                                }); 
                                $('.save-loading').hide(); 
                            } else {
                                $("span.display-status").text("Successfully Updated").show().fadeOut(3000).css({
                                    "display": "block",
                                    "color": "green",
                                    "text-align": "center"
                                }); 
                                $('.save-loading').hide(); 
                            }
                        }
                    });
                } else {
                    console.log("invalid");
                    $("span.display-status").text("Invalid IDX Key").show().fadeOut(3000).css({
                        "display": "block",
                        "color": "green",
                        "text-align": "center"
                    });  
                    $('.save-loading').hide();  
                }
            }
        }); 
    }

    $('#saveIdxForm').submit(function(e) {
        saveIdx();
        e.preventDefault();
    });

   //===============autosave Property  Status ==================//
   
     var timeoutId;

    $('.select-stat').on('select propertychange change', function() {    
        console.log('Textarea Change');

        var form_id = $(this).attr('id');
        console.log(form_id);

        clearTimeout(timeoutId);
        timeoutId = setTimeout(function() {
            // Runs 1 second (1000 ms) after the last change    
            save_property_status( form_id );
        }, 1000);
    });

    function save_property_status( form_id )
    {

        console.log('Saving to the db');

        $('.save-loading-'+form_id+'').show();

       var property_status = $('#'+form_id).val();
       var querystring = "property_status="+property_status+"&property_ListingID="+form_id;
       //console.log(querystring);
       // alert( $('#propertyStatusForm-'+form_id+'').attr('action') );

        $.ajax({
            url: $('#propertyStatusForm-'+form_id+'').attr('action'),
            type: "POST",
            data : querystring,
            success: function(data) {
                
                location.reload(true);
            },
        });
    }

    
    //added delay on alert flash data
    $(".alert-flash").delay(3000) .fadeOut(2000); 

    function toggleChevron(e) {
        $(e.target)
            .prev('.panel-heading')
            .find("i.indicator")
            .toggleClass('fa-caret-down fa-caret-up');
    }
    $('#menus-accordion').on('hidden.bs.collapse', toggleChevron);
    $('#menus-accordion').on('shown.bs.collapse', toggleChevron);

     function toggleChevron(e) {
        $(e.target)
            .prev('.panel-heading')
            .find("i.indicator")
            .toggleClass('fa-caret-down fa-caret-up');
    }
    $('#page-accordion').on('hidden.bs.collapse', toggleChevron);
    $('#page-accordion').on('shown.bs.collapse', toggleChevron);

    // file uploader
    $('#two').fileUploader({
        useFileIcons: false,
        fileMaxSize: 1.7,
        totalMaxSize: 5,
        name: "two",
        linkButtonContent: '<i>Open</i>',
        deleteButtonContent: '<i>Remove</i>',
        filenameTest: function(fileName, fileExt, $container) {
            var allowedExts = ["jpg", "jpeg" , "png"];
            var $info = $('<div class="center"></div>');
            var proceed = true;

            // length check
            if (fileName.length > 13) {
                $info.html('Name too long...');
                proceed = false;
            }
            // replace not allowed characters
            fileName = fileName.replace(" ", "-");

            // extension check
            if (allowedExts.indexOf(fileExt) < 0) {
                $info.html('Filetype not allowed...');
                proceed = false;
            }

            // show an error message
            if (!proceed) {
                $container.append($info);

                setTimeout(function() {
                    $info.animate({opacity: 0}, 300, function() {
                        $(this).remove();
                    });
                }, 2000);
                return false;
            }

            return fileName;
        },
        langs: {
            "en":{
                dropZone_msg: "Drop your images here or click to upload."
            }
        }
    });


   // declare height of modal
   var wHeight = $(window).height();
   $(".spw-modal .modal-body").css("min-height", wHeight-100);

    // check domain
    $('.checkdomainbtn').click(function(){
        $('.domain-result').show();
    });

    // change profile pic
    $('.change-profile-container button').click(function(){
        var $imageUpload = $('.image-upload');

        $imageUpload.click();
        $imageUpload.change(function(){
            var $agentImg = $('.agent-img img');
            $('.user-menu').addClass('open');
            $('.show-btn').show();
            // console.log($agentImg);
            // var $image = $imageUpload.val().split('\\').pop();
            // var $image = 'img/' + $image;
            // console.log($image);
            // $agentImg.attr('src', $image);
        });

    });

    //check all in PAGE module
    $("#checkAll").change(function () {
        $("input:checkbox").prop('checked', $(this).prop("checked"));
    });


});

