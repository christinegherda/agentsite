

  jQuery(document).ready(function($){


     //Change the form action based on selected value ie.. move_trash
    $('#bulk-action').change(function(){
      var formAction = $("#bulk-select").val() == "trash" ? "bulk_trash" : "";
      $("#bulk-action").attr("action", "/agent-site/pages/" + formAction);
    });

    //Change the form action based on selected value ie.. restore/delete
    $('#bulk-trash').change(function(){
      var formAction = $("#bulk-select").val() == "restore" ? "bulk_restore" : "bulk_delete";
      $("#bulk-trash").attr("action", "/agent-site/pages/" + formAction);
    });

    //Change the form action based on selected value ie.. filter
    $('#filter-date').change(function(){
      var formAction = $("#select-pages").val();
      $("#filter-date").attr("action", "/agent-site/pages/filter/" + formAction);
    });

     //Change the form action based on selected value ie.. filter_trash
    $('#filter-trash').change(function(){
      var formAction = $("#select-trash").val();
      $("#filter-trash").attr("action", "/agent-site/pages/filter_trash/" + formAction);
    });

    // Change the select name based on selected action ie.. restore/delete
    $('#bulk-select').change(function(){
      var selectedVal = $("#bulk-select").val() == "restore" ? "bulk_restore[]" : "bulk_delete[]";
      $(".select-action").attr("name", selectedVal);
    });

    //disable publish button when text area/title area is empty
    $(document).ready(function() {
       $('.page-publish').attr('disabled', true);

        $('.page-titlearea, .note-editable').on('keyup',function() {
            var textarea_value = $(".note-editable").text();
            var title_value = $(".page-titlearea").val();
            if(title_value != '' && textarea_value != '') {
                $('.page-publish').attr('disabled' , false);
            }else{
                $('.page-publish').attr('disabled' , true);
            }
        });
    });

});
