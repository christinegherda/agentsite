

  jQuery(document).ready(function($){


     //Change the form action based on selected value ie.. bulk_delete GRID VIEW
    $('#bulk-action-grid').change(function(){
      var formAction = $("#bulk-select-grid").val() == "delete" ? "bulk_delete_grid" : "";
      $("#bulk-action-grid").attr("action", "/agent-site/media/" + formAction);
    });


     //Change the form action based on selected value ie.. bulk_delete LIST VIEW
    $('#bulk-action-list').change(function(){
      var formAction = $("#bulk-select-list").val() == "delete" ? "bulk_delete_list" : "";
      $("#bulk-action-list").attr("action", "/agent-site/media/" + formAction);
    });

    //Change the form action based on selected value ie.. filter_date GRID VIEW
    $('#filter-date-grid').change(function(){
      var formAction = $("#select-pages").val();
      $("#filter-date-grid").attr("action", "/agent-site/media/filter_grid/" + formAction);
    });

     //Change the form action based on selected value ie.. filter_date LIST VIEW
    $('#filter-date-list').change(function(){
      var formAction = $("#select-pages").val();
      $("#filter-date-list").attr("action", "/agent-site/media/filter_list/" + formAction);
    });

    // Change the select name based on selected action ie.. restore/delete
    $('#bulk-select').change(function(){
      var selectedVal = $("#bulk-select").val() == "delete" ? "bulk_delete[]" : "";
      $(".select-action").attr("name", selectedVal);
    });

    //show checkbox on HOVER
    $( ".list-item-grid" ).hover(
      function() {
        $( this ).addClass( "display-checkbox" );
      }, function() {
        $( this ).removeClass( "display-checkbox" );
      }
    );

    //show media actions on hover list view
    $( ".media-hover-actions" ).hover(
      function() {
        $( this ).addClass( "display-action" );
      }, function() {
        $( this ).removeClass( "display-action" );
      }
    );

    //show all actions on Grid view on chech checkbox
    $('input').change(function(){
    if ($(this).is(':checked')) {
        $('.media-grid-actions').addClass("show-grid-actions");
    }
});

});
