jQuery(document).ready(function($) {


//===============autosave Facebook Url==================//

    var timeoutId;
    $('.facebook-url-form input').on('input propertychange change', function() {
        console.log('Textarea Change');
        
        clearTimeout(timeoutId);
        timeoutId = setTimeout(function() {
            // Runs 1 second (1000 ms) after the last change    
            saveFacebookUrl();
        }, 1000);
    });

    function saveFacebookUrl()
    {
        console.log('Saving to the db');
        var fb = $("#facebook_url").val();
        var tw = $("#twitter_url").val();
        var li = $("#linkedin_url").val();
        var gp = $("#googleplus_url").val();
        var data = {facebook:fb,twitter:tw,linkedin:li,googleplus:gp}
        //console.log(data);
            $('.facebook-loading').show();
        $.ajax({
            url: $("#facebookUrlForm").attr('action'),
            type: "POST",
            data: data,
            beforeSend: function(xhr) {
            //     // Let them know we are saving
            //     $('.agent-info-form-status').html('Saving...');
            },
            success: function(data) {
                var jqObj = jQuery(data); // You can get data returned from your ajax call here. ex. jqObj.find('.returned-data').html()
                //Now show them we saved and when we did
                $("span.display-status").text("Saved!").show().fadeOut(3000).css({
                "display": "block",
                "color": "green",
                "text-align": "center"
                });

                $('.facebook-loading').hide();
            },
        });
    }

     // This is just so we don't go anywhere  
    // and still save if you submit the form
    $('#facebookUrlForm').submit(function(e) {
        saveFacebookUrl();
        e.preventDefault();
    });

    //===============autosave Twitter Url==================//

    var timeoutId;
    $('.twitter-url-form input').on('input propertychange change', function() {
        console.log('Textarea Change');
        
        clearTimeout(timeoutId);
        timeoutId = setTimeout(function() {
            // Runs 1 second (1000 ms) after the last change    
            saveTwitterUrl();
        }, 1000);
    });

    function saveTwitterUrl()
    {
        console.log('Saving to the db');
        var fb = $("#facebook_url").val();
        var tw = $("#twitter_url").val();
        var li = $("#linkedin_url").val();
        var gp = $("#googleplus_url").val();
        var data = {facebook:fb,twitter:tw,linkedin:li,googleplus:gp}
        //console.log(data);
            $('.twitter-loading').show();
        $.ajax({
            url: $("#twitterUrlForm").attr('action'),
            type: "POST",
            data: data,
            beforeSend: function(xhr) {
            //     // Let them know we are saving
            //     $('.agent-info-form-status').html('Saving...');
            },
            success: function(data) {
                var jqObj = jQuery(data); // You can get data returned from your ajax call here. ex. jqObj.find('.returned-data').html()
                //Now show them we saved and when we did
                $("span.display-status").text("Saved!").show().fadeOut(3000).css({
                "display": "block",
                "color": "green",
                "text-align": "center"
                });

                $('.twitter-loading').hide();
            },
        });
    }

     // This is just so we don't go anywhere  
    // and still save if you submit the form
    $('#twitterUrlForm').submit(function(e) {
        saveTwitterUrl();
        e.preventDefault();
    });

    //===============autosave LinkedIn Url==================//

    var timeoutId;
    $('.linkedin-url-form input').on('input propertychange change', function() {
        console.log('Textarea Change');
        
        clearTimeout(timeoutId);
        timeoutId = setTimeout(function() {
            // Runs 1 second (1000 ms) after the last change    
            saveLinkedinUrl();
        }, 1000);
    });

    function saveLinkedinUrl()
    {
        console.log('Saving to the db');
        var fb = $("#facebook_url").val();
        var tw = $("#twitter_url").val();
        var li = $("#linkedin_url").val();
        var gp = $("#googleplus_url").val();
        var data = {facebook:fb,twitter:tw,linkedin:li,googleplus:gp}
        //console.log(data);
            $('.linkedin-loading').show();
        $.ajax({
            url: $("#linkedinUrlForm").attr('action'),
            type: "POST",
            data: data,
            beforeSend: function(xhr) {
            //     // Let them know we are saving
            //     $('.agent-info-form-status').html('Saving...');
            },
            success: function(data) {
                var jqObj = jQuery(data); // You can get data returned from your ajax call here. ex. jqObj.find('.returned-data').html()
                //Now show them we saved and when we did
                $("span.display-status").text("Saved!").show().fadeOut(3000).css({
                "display": "block",
                "color": "green",
                "text-align": "center"
                });

                $('.linkedin-loading').hide();
            },
        });
    }

     // This is just so we don't go anywhere  
    // and still save if you submit the form
    $('#linkedinUrlForm').submit(function(e) {
        saveLinkedinUrl();
        e.preventDefault();
    });

    //===============autosave GooglePlus Url==================//

    var timeoutId;
    $('.googleplus-url-form input').on('input propertychange change', function() {
        console.log('Textarea Change');
        
        clearTimeout(timeoutId);
        timeoutId = setTimeout(function() {
            // Runs 1 second (1000 ms) after the last change    
            saveGooglePlusUrl();
        }, 1000);
    });

    function saveGooglePlusUrl()
    {
        console.log('Saving to the db');

        var fb = $("#facebook_url").val();
        var tw = $("#twitter_url").val();
        var li = $("#linkedin_url").val();
        var gp = $("#googleplus_url").val();
        var data = {facebook:fb,twitter:tw,linkedin:li,googleplus:gp}
        //console.log(data);
            $('.googleplus-loading').show();
        $.ajax({
            url: $("#googleplusUrlForm").attr('action'),
            type: "POST",
            data: data,
            beforeSend: function(xhr) {
            //     // Let them know we are saving
            //     $('.agent-info-form-status').html('Saving...');
            },
            success: function(data) {
                var jqObj = jQuery(data); // You can get data returned from your ajax call here. ex. jqObj.find('.returned-data').html()
                //Now show them we saved and when we did
                $("span.display-status").text("Saved!").show().fadeOut(3000).css({
                "display": "block",
                "color": "green",
                "text-align": "center"
                });

                $('.googleplus-loading').hide();
            },
        });
    }

     // This is just so we don't go anywhere  
    // and still save if you submit the form
    $('#googleplusUrlForm').submit(function(e) {
        saveGooglePlusUrl();
        e.preventDefault();
    });

});


//Social Media Popup Activate
    function fb_login(){
            var w_left = (screen.width/6);
            var w_top = (screen.height/6);
            window.open(fb_url,'targetWindow','toolbar=no,titlebar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no,width=700,height=450,top='+w_top+',left='+w_left+'');   
    }

    function linkdn_login(){

            var w_left = (screen.width/6);
            var w_top = (screen.height/6);
            window.open(linkdn_url,'targetWindow','toolbar=no,titlebar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no,width=700,height=450,top='+w_top+',left='+w_left+''); 
    }

    function twitt_login(){

        var w_left = (screen.width/6);
        var w_top = (screen.height/6);
        window.open(twitt_url,'targetWindow','toolbar=no,titlebar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no,width=700,height=450,top='+w_top+',left='+w_left+''); 
    }