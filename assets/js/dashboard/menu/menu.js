$(document).ready(function() {


	//===============autosave Menu Label ==================//

    // var timeoutId;
    // $('.navigation-label input').on('input propertychange change', function() {
    //     console.log('Textarea Change');
        
    //     clearTimeout(timeoutId);
    //     timeoutId = setTimeout(function() {
    //         // Runs 1 second (1000 ms) after the last change    
    //         saveLabel();
    //     }, 1000);
    // });

    // function saveLabel()
    // {
        
    //     var formData = $('#navLabel').serialize();// serializes the form's elements.
    //     console.log(formData);
    //         $('.save-label').show();
    //     $.ajax({
    //         url: $("#navLabel").attr('action'),
    //         type: "POST",
    //         data: formData,
    //         beforeSend: function(xhr) {
    //         //     // Let them know we are saving
    //         //     $('.agent-info-form-status').html('Saving...');
    //         },
    //         success: function(data) {
    //             var jqObj = jQuery(data); // You can get data returned from your ajax call here. ex. jqObj.find('.returned-data').html()
    //             //Now show them we saved and when we did
    //             $("span.display-status").text("Saved!").show().fadeOut(3000).css({
    //                 "display": "block",
    //                 "color": "green",
    //                 "text-align": "center",
    //                 "margin-bottom": "10px",
    //             });

    //             location.reload(true);

    //             $('.save-label').hide();
    //         },
    //     });
    // }

    //fadeout alert data
    $(".alert-flash").fadeOut(1000);


    //disable editbutton when input field is empty
    $('#editButton').attr('disabled',true);
    $('#editInputName').keyup(function(){
        if($(this).val.length !=0){
            $('#editButton').attr('disabled', false);
        }
    });

    $("#custom-page-area").hide();

    $(".customMenu").click(function(){
        $("#custom-page-area").show();
        $("#default-page-area").hide(); 
    });

    $(".defaultMenu").click(function(){
        $("#custom-page-area").hide();
        $("#default-page-area").show(); 
    });

});