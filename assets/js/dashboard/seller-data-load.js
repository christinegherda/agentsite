$(document).ready(function(){
	var $this = $('.message-clients li.selected');
	var $name = $this.find('a').text();
	
	//$.getJSON('http://localhost/agent-site/assets/js/dashboard/seller-data.json', function(data){
	$.getJSON(base_url+'crm/seller_leads/get_seller_json', function(data){	
		if($this.hasClass('selected')){
			var output="<h4>Contact Information</h4>";
			output += '<ul class="lead-items">';
			$.each(data, function(key, val){
				if($name == val.fname+" "+val.lname){
					output += '<li><div class="col-md-4"><label>Name:</label></div>';
					output += '<div class="col-md-8"><p>'+ val.fname +' '+val.lname +'</p></div></li>';
					output += '<li><div class="col-md-4"><label>Email:</label></div>';
					output += '<div class="col-md-8"><p>'+ val.email + '</p></div></li>';
					output += '<li><div class="col-md-4"><label>Phone:</label></div>';
					output += '<div class="col-md-8"><p>'+ val.phone + '</p></div></li>';
					// output += '<li><div class="col-md-4"><label>Mobile:</label></div>';
					// output += '<div class="col-md-8"><p>'+ val.mobile + '</p></div></li>';
					// output += '<li><div class="col-md-4"><label>Fax:</label></div>';
					// output += '<div class="col-md-8"><p>'+ val.fax + '</p></div></li>';
					// output += '<li><div class="col-md-4"><label>Address:</label></div>';
					// output += '<div class="col-md-8"><p>'+ val.address + '</p></div></li>';
					// output += '<li><div class="col-md-4"><label>City:</label></div>';
					// output += '<div class="col-md-8"><p>'+ val.city + '</p></div></li>';
					// output += '<li><div class="col-md-4"><label>State:</label></div>';
					// output += '<div class="col-md-8"><p>'+ val.state + '</p></div></li>';
					// output += '<li><div class="col-md-4"><label>Zipcode:</label></div>';
					// output += '<div class="col-md-8"><p>'+ val.zip_code + '</p></div></li>';
					output += '<h4>Home Information</h4>'
					output += '<li><div class="col-md-5"><label>Address:</label></div>';
					output += '<div class="col-md-7"><p>'+ val.address + '</p></div></li>';
					output += '<li><div class="col-md-5"><label>Zipcode:</label></div>';
					output += '<div class="col-md-7"><p>'+ val.zip_code + '</p></div></li>';
					output += '<li><div class="col-md-5"><label>Number of Bedrooms:</label></div>';
					output += '<div class="col-md-7"><p>'+ val.bedrooms + '</p></div></li>';
					output += '<li><div class="col-md-5"><label>Number of Bathrooms:</label></div>';
					output += '<div class="col-md-7"><p>'+ val.bathrooms + '</p></div></li>';
					output += '<li><div class="col-md-5"><label>Square Feet:</label></div>';
					output += '<div class="col-md-7"><p>'+ val.square_feet + '</p></div></li>';
					// output += '<li><div class="col-md-5"><label>Type of Property:</label></div>';
					// output += '<div class="col-md-7"><p>'+ val.property_type + '</p></div></li>';
					// output += '<li><div class="col-md-5"><label>Price Range:</label></div>';
					// output += '<div class="col-md-7"><p>'+ val.price_range + '</p></div></li>';
					// output += '<h4>Additional Information</h4>'
					// output += '<li><div class="col-md-7"><label>When do you plan to sell?</label></div>';
					// output += '<div class="col-md-5"><p>'+ val.when_do_you_plan_to_sell + '</p></div></li>';
					// output += '<li><div class="col-md-7"><label>Comments</label></div>';
					// output += '<div class="col-md-5"><p>'+ val.comments + '</p></div></li>';
				}
			});

			output += '</ul>';
		}
		else
		{
			output = '';
		}
		
		$('.lead-details').html(output);
	});
	

	$('.message-clients li').click(function(){
		var $this = $(this);
		var $name = $(this).find('a').text();

		$this.addClass('selected').siblings().removeClass('selected');

		//$.getJSON('http://localhost/agent-site/assets/js/dashboard/seller-data.json', function(data){
		$.getJSON(base_url+'crm/seller_leads/get_seller_json', function(data){	
			if($this.hasClass('selected')){
				var output="<h4>Contact Information</h4>";
				output += '<ul class="lead-items">';
				$.each(data, function(key, val){
					if($name == val.fname+" "+val.lname){
						output += '<li><div class="col-md-4"><label>Name:</label></div>';
						output += '<div class="col-md-8"><p>'+ val.fname +' '+val.lname +'</p></div></li>';
						output += '<li><div class="col-md-4"><label>Email:</label></div>';
						output += '<div class="col-md-8"><p>'+ val.email + '</p></div></li>';
						output += '<li><div class="col-md-4"><label>Phone:</label></div>';
						output += '<div class="col-md-8"><p>'+ val.phone + '</p></div></li>';
						// output += '<li><div class="col-md-4"><label>Mobile:</label></div>';
						// output += '<div class="col-md-8"><p>'+ val.mobile + '</p></div></li>';
						// output += '<li><div class="col-md-4"><label>Fax:</label></div>';
						// output += '<div class="col-md-8"><p>'+ val.fax + '</p></div></li>';
						// output += '<li><div class="col-md-4"><label>Address:</label></div>';
						// output += '<div class="col-md-8"><p>'+ val.address + '</p></div></li>';
						// output += '<li><div class="col-md-4"><label>City:</label></div>';
						// output += '<div class="col-md-8"><p>'+ val.city + '</p></div></li>';
						// output += '<li><div class="col-md-4"><label>State:</label></div>';
						// output += '<div class="col-md-8"><p>'+ val.state + '</p></div></li>';
						// output += '<li><div class="col-md-4"><label>Zipcode:</label></div>';
						// output += '<div class="col-md-8"><p>'+ val.zip_code + '</p></div></li>';
						output += '<h4>Home Information</h4>'
						output += '<li><div class="col-md-5"><label>Address:</label></div>';
						output += '<div class="col-md-7"><p>'+ val.address + '</p></div></li>';
						output += '<li><div class="col-md-5"><label>Zipcode:</label></div>';
						output += '<div class="col-md-7"><p>'+ val.zip_code + '</p></div></li>';
						output += '<li><div class="col-md-5"><label>Number of Bedrooms:</label></div>';
						output += '<div class="col-md-7"><p>'+ val.bedrooms + '</p></div></li>';
						output += '<li><div class="col-md-5"><label>Number of Bathrooms:</label></div>';
						output += '<div class="col-md-7"><p>'+ val.bathrooms + '</p></div></li>';
						output += '<li><div class="col-md-5"><label>Square Feet:</label></div>';
						output += '<div class="col-md-7"><p>'+ val.square_feet + '</p></div></li>';
						// output += '<li><div class="col-md-5"><label>Type of Property:</label></div>';
						// output += '<div class="col-md-7"><p>'+ val.property_type + '</p></div></li>';
						// output += '<li><div class="col-md-5"><label>Price Range:</label></div>';
						// output += '<div class="col-md-7"><p>'+ val.price_range + '</p></div></li>';
						// output += '<h4>Additional Information</h4>'
						// output += '<li><div class="col-md-7"><label>When do you plan to sell?</label></div>';
						// output += '<div class="col-md-5"><p>'+ val.when_do_you_plan_to_sell + '</p></div></li>';
						// output += '<li><div class="col-md-7"><label>Comments</label></div>';
						// output += '<div class="col-md-5"><p>'+ val.comments + '</p></div></li>';
					}
				});
			}
			output += '</ul>';
			$('.lead-details').html(output);
		});
	});
});