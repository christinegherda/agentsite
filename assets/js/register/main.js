$(document).ready(function() {
    console.log($base_url);
    $("#submit_info_btn").prop("disabled", true);

    $("#r_banner_tagline").on("change", function() {
        if($(this).val()=="") {
            $(".empty").html("<i class='fa fa-ban'></i> Some fields are empty!");
        }
        else if($("#r_tag_line").val()=="" || $("#r_site_name").val()=="") {
            $(".empty").html("<i class='fa fa-ban'></i> Some fields are empty!");
        } 
        else {
            $(".empty").html("");
            $("#submit_info_btn").prop("disabled", false);
        }
    });


    $("#reg_submit_btn").on("click", function() {
        $("#brand_info_form").submit();
    });

    /*$("#brand_info_form").on("submit", function(eve) {
        eve.preventDefault();
        $.ajax({
            type:"POST",
            url:$base_url+"register/save_branding_info",
            data:$("#brand_info_form").serialize(),
            dataType:'json',
            beforeSend: function() {
                console.log($("#brand_info_form").serialize());
            },
            success:function(data) {
                if(data.success) {
                    
                }
            }
        });
    });*/

    $("#existing_domain_btn").click(function() {
        $("#brand_existing_domain_form").submit();
    });

    $("#brand_existing_domain_form").on("submit", function(eve) {
        eve.preventDefault();
        $.ajax({
            type:"POST",
            url:$base_url+"register/add_existing_domain",
            data:$("#brand_existing_domain_form").serialize(),
            dataType:'json',
            beforeSend: function() {
                console.log($("#brand_existing_domain_form").serialize());
                $(".result-domain").show();
                $(".available").hide();
                $(".taken").hide();
            },
            success:function(data) {
                if(data.success) {
                    console.log(data);
                    $(".available").show();
                    $(".available").html("<i class='fa fa-info-circle'></i> "+data.message);
                } else {
                    $(".taken").show();
                    $(".taken").html("<i class='fa fa-ban'></i> "+data.message);
                }
            }
        });
    });

    $("#check_domain_btn").click(function() {
        $("#reg-domain-form").submit();
    });

    $("#reg-domain-form").on("submit", function(eve) {
        eve.preventDefault();
        console.log($("#reg-domain-form").serialize());

        $.ajax({
            type:"GET",
            url: $base_url+"register/domains",
            data: $("#reg-domain-form").serialize(),
            dataType: "json",
            beforeSend: function() {
                $("#check_domain_btn").text("Checking...");
                $("#check_domain_btn").prop("disabled", true);
                $(".result-domain").show();
                $(".available").hide();
                $(".taken").hide();
            }, 
            success: function(e) {
                $("#check_domain_btn").text("Check Domain");
                $("#check_domain_btn").prop("disabled", false);
                console.log(e);
                var domainName = $.map(e.domainName, function(value, index) {
                    return [value];
                });

                if(e.status == 1) {
                    var domainPrice = jQuery.map(e.domainPrice, function(value, index) {
                        return [value];
                    });
                    $(".available").show();
                    $(".available").html("<i class='fa fa-info-circle'></i> Domain <strong>" + domainName + "</strong> ($"+ domainPrice + ") is available. Click <a href='javascript:void(0)' class='purchase-domain' data-domain_name_avail="+domainName+" data-domain_price="+domainPrice+">here</a> to reserve this domain.");
                } else {
                    console.log("already taken");
                    $(".taken").show();
                    $(".taken").html("<i class='fa fa-ban'></i> <strong>" + domainName + "</strong> is already taken.");
                    
                    $domain_name = e.domain;

                    $.ajax({
                        type:"GET",
                        url: $base_url+"register/suggested_domains",
                        data: {
                            domain_tld_get: "1",
                            domain : $domain_name
                        },
                        dataType: 'json',
                        beforeSend: function() {
                            console.log("Sending");
                            $(".result-domain").show();
                            $(".fetching-text").show();
                        },
                        success: function(data) {
                            $("#domain-list").show()
                            $(".domain-list").show();
                            $(".fetching-text").hide();
                            console.log(data);

                            var domainCount = jQuery.map(data.domainCount, function(value, index) {
                                return [value];
                            });

                            var splitStr = '';
                            var domainStatus = [];
                            var statusCode = [];
                            console.log(data.domains.length);

                            for(var i = 0; i < data.domains.length; i++) {
                                splitStr = data.domains[i].split("_");
                                for (var j = 0; j < domainCount; j++) {
                                    if( splitStr[0] == "Domain" + (j + 1) ) {
                                        domainStatus.push(splitStr[1]); 
                                    }
                                    if( splitStr[0] == "RRPCode" + (j + 1) ) {
                                        statusCode.push(splitStr[1]); 
                                    }
                                }
                            }

                            var htmlInsertString = "";
                            var other_ext = false;
                            for(var i = 0; i < domainStatus.length; i ++) {
                                console.log(data.tldPrice.product.length);
                                var getDomainName = domainStatus[i].split(".");
                                allowDomainExt  = ["com","org","net"];

                                for(var x = 0; x < data.tldPrice.product.length; x ++) {

                                    if(data.tldPrice.product[x].tld == getDomainName[1]) {
                                        var domainPrice = data.tldPrice.product[x].registerprice;
                                        break;
                                    }

                                }

                                if(statusCode[i] == '210') {
                                    var domainName = domainStatus[i].split(".");
                                    var user_id = $("#user_agent_id").val();

                                    if(jQuery.inArray(domainName[1], allowDomainExt) != -1) {
                                        other_ext = true;
                                        //Domain is Available
                                        htmlInsertString += '<li><a type="button" class="purchase-domain" data-domain_name="'+domainStatus[i]+'" data-domain_price="'+domainPrice+'">'+domainStatus[i]+' - $'+ domainPrice +'</a></li>';                 
                                    }
                                }
                            }
                            if(!other_ext) {
                                for(var i = 0; i < domainStatus.length; i ++) {
                                    if(statusCode[i] == '210') {                    
                                        var domainName = domainStatus[i].split(".");
                                        var user_id = $("#user_agent_id").val();                              
                                         htmlInsertString += '<li><a type="button" class="purchase-domain" data-domain_name="'+domainStatus[i]+'" data-domain_price="'+domainPrice+'">'+domainStatus[i]+' - $'+ domainPrice +'</a></li>';
                                        //htmlInsertString += '<li><div class="col-md-5 col-sm-5">'+domainStatus[i]+' - $'+ domainPrice +'</div><div class="col-md-6 col-sm-5"><a class="btn btn-default btn-create purchase-domain" data-user_id="'+user_id+'" data-domain_name="'+domainStatus[i]+'" data-domain_price="'+domainPrice+'" >Reserve this domain free for me</a></div></li>';                 
                                    }
                                }
                            }
                            if(htmlInsertString == "") {
                                htmlInsertString += '<li>No Domain Available!</li>';
                            }
                            $(".result-domain ul.domain-list").html(htmlInsertString);
                            $(".purchase-domain").on("click", show_domain_modal);
                        }
                    });
                }
                $('#check_domain_sbt').prop('disabled', false);
                $(".purchase-domain").on("click", show_domain_modal_available);
            }
        });
    });
    
    var show_domain_modal_available = function () {   

        var domain_name = $(this).data("domain_name_avail");
        var domain_price = $(this).data("domain_price");

        $(".confirm-purchase-domain").attr("data-domain-name", domain_name);
        $(".confirm-purchase-domain").attr("data-domain-price", domain_price);

        $("#domain_purchase_modal").modal('show');
        $(".confirm-purchase-domain").on("click", purchase_domain_avail);

    };

    var show_domain_modal = function () {        
        
        var domain_name = $(this).data("domain_name");
        var domain_price = $(this).data("domain_price");

        $(".confirm-purchase-domain").attr("data-domain-name", domain_name);
        $(".confirm-purchase-domain").attr("data-domain-price", domain_price);

        $("#domain_purchase_modal").modal('show');
        $(".confirm-purchase-domain").on("click", purchase_domain);
    };

    var purchase_domain = function(event) {

        var domain_name = $(this).data("domain-name");
        var user_id = $("#r_user_id").val();
        var app_type = $("#app_type").val();

        $.ajax({
            type: "GET",
            url: $base_url+"register/purchase_domain",
            data: {
                domain_name: domain_name,
                user_id: user_id,
                app_type: app_type
            },
            dataType: "json",
            beforeSend:function() {
                $(this).text("Loading...");
            },
            success:function(data) {
                if(data.success) {    
                    $(this).text("Continue");
                    $("#domain_purchase_modal").modal('hide');
                    $(".taken").html("<i class='fa fa-info-circle'></i> " +data.message+"<br> Please Click Submit to proceed.");
                } else {
                    $(this).text("Continue");
                    $("#domain_purchase_modal").modal('hide');
                    $(".taken").html("<i class='fa fa-ban'></i> "+ data.message);
                }
            }
        });

    };

    var purchase_domain_avail = function (event) {

        var $this = $(this);            
        var domain_name = $this.data("domain-name");
        var user_id = $("#r_user_id").val();
        var app_type = $("#app_type").val();

        $.ajax({
            type: "GET",
            url: $base_url+"register/purchase_domain",
            data: {
                domain_name: domain_name,
                user_id: user_id,
                app_type: app_type
            },
            dataType : "json",
            beforeSend: function() {
                $this.text("Loading...");
            },
            success: function(data) {
                if(data.success) {    
                    $this.text("Continue");
                    $("#domain_purchase_modal").modal('hide');
                    $(".available").html("<i class='fa fa-info-circle'></i> " +data.message+"<br> Please Click Submit to proceed.");
                } else {
                    $this.text("Continue");
                    $("#domain_purchase_modal").modal('hide');
                    $(".available").html("<i class='fa fa-info-circle'></i> " +data.message);
                }
            }
        });
        event.preventDefault();
    };

    $('#domain_purchase_modal').on('shown.bs.modal', function () {
        $(".modal-backdrop.in").hide();
    })

    $('#logo').fileUploader({
        useFileIcons: false,
        fileMaxSize: 50,
        totalMaxSize: 50,
        name: "two",
        linkButtonContent: '<i>Open</i>',
        deleteButtonContent: '<i>Remove</i>',
        filenameTest: function(fileName, fileExt, $container) {
            var allowedExts = ["jpg", "jpeg" , "png"];
            var $info = $('<div class="center"></div>');
            var proceed = true;

            // length check
            if (fileName.length > 50) {
                $info.html('Name too long!');
                proceed = false;
            }
            // replace not allowed characters
            fileName = fileName.replace(" ", "-");

            // extension check
            if (allowedExts.indexOf(fileExt) < 0) {
                $info.html('Filetype not allowed!');
                proceed = false;
            }

            // show an error message
            if (!proceed) {
                $container.append($info);

                setTimeout(function() {
                    $info.animate({opacity: 0}, 300, function() {
                        $(this).remove();
                    });
                }, 2000);
                return false;
            }

            return fileName;
        },
        langs: {
            "en":{
                dropZone_msg: "Drop your logo file here <br> 250 x 40"
            }
        }
    });
    $('#icon').fileUploader({
        useFileIcons: false,
        fileMaxSize: 50,
        totalMaxSize: 50,
        name: "two",
        linkButtonContent: '<i>Open</i>',
        deleteButtonContent: '<i>Remove</i>',
        filenameTest: function(fileName, fileExt, $container) {
            var allowedExts = ["jpg", "jpeg" , "png"];
            var $info = $('<div class="center"></div>');
            var proceed = true;

            // length check
            if (fileName.length > 50) {
                $info.html('Name too long!');
                proceed = false;
            }
            // replace not allowed characters
            fileName = fileName.replace(" ", "-");

            // extension check
            if (allowedExts.indexOf(fileExt) < 0) {
                $info.html('Filetype not allowed!');
                proceed = false;
            }

            // show an error message
            if (!proceed) {
                $container.append($info);

                setTimeout(function() {
                    $info.animate({opacity: 0}, 300, function() {
                        $(this).remove();
                    });
                }, 2000);
                return false;
            }

            return fileName;
        },
        langs: {
            "en":{
                dropZone_msg: "Drop your icon file here <br> 16 x 16"
            }
        }
    });


    $( ".section-flow2 .btn-green" ).click(function() {
        $( "#logo" ).hide();
        $( "#icon" ).hide();
    });
    $( ".section-flow1 .btn-green" ).click(function() {
        $( "#logo" ).show();
        $( "#icon" ).show();
    });
    
    $( ".pt-page-3 .btn-back" ).click(function() {
        $( "#logo" ).show();
        $( "#icon" ).show();
    });

    $('.have-domain input').on('change', function() {
        $( ".result-domain" ).hide();
       var domain = $('input[name=checkdomain]:checked', '.have-domain').val();
       if (domain == "yes") {
        $( ".yes-domain" ).show();
        $( ".no-domain" ).hide();
       } else {
        $( ".no-domain" ).show();
        $( ".yes-domain" ).hide();
       }
    });

    $( ".check-domain-exist" ).click(function() {
        // alert(1);
        $( ".result-domain" ).show();
        $( ".fetching-text" ).show();
        setTimeout(function() {
            $( ".fetching-text" ).hide();
        }, 3000);
        setTimeout(function() {
            $( ".domain-list" ).show();
        }, 3000);
    });

});