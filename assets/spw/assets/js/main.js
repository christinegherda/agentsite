$(document).ready(function(){
    // accordion
    $(".panel-collapse").hide();
    $(".moreDetails").show();
    $(".panel-heading").on("click",function(){
        var $this = $(this);
        
        $this.stop().next().slideDown(250).parent().siblings().find('.panel-collapse').slideUp(250);
        $this.find('.fa-plus-circle').removeClass('fa-plus-circle').addClass('fa-minus-circle')
            .parents('.panel-default').siblings().find('.fa-minus-circle').removeClass('fa-minus-circle').addClass('fa-plus-circle');
    });

    // ease scrolling
    $('a[href^="#"]').on("click", function(t) {
        t.preventDefault();
        var o = this.hash,
            n = $(o);
        $("html, body").stop().animate({
            scrollTop: n.offset().top
        }, 900, "swing", function() {
            window.location.hash = o;
            var $navbar = $('#navbar');
            $navbar.removeClass('in');
        });
    });
    
    // show scroll to top button
    $(window).scroll(function(){
        var $navoffset = $('.navbar').offset(); 
        var $scrollToTop = $('.scroll-to-top');
        var $this = $(this);
        $navoffset = $navoffset.top;

        if($navoffset >= 100){
            $scrollToTop.fadeIn(250);
        }
        if($this.scrollTop() == 0){
            $scrollToTop.fadeOut(250);  
        }
    });

    $('.scroll-to-top').click(function() {
        $('html, body').animate({
            scrollTop: 0
        }, 800);
        return false;
    });

     // Schedule showing form
    $("#scheduleShowing").submit(
        function(event) {
            event.preventDefault();
            var datastring = $("#scheduleShowing").serialize();

             //console.log(datastring);
            $.ajax({
                type: "POST",
                url: $("#scheduleShowing").attr("action"),
                data: datastring,
                beforeSend: function(e) {
                    $("#schedule-submit").val("Loading...");
                },
                success: function(e) {
                    var c = jQuery.parseJSON(e);
                    console.log(c);
                    $("#schedule-submit").val("Submit");
                    if(c.success) {
                        $("#scheduleShowing .alert.alert-success").show();
                        $("#scheduleShowing .alert.alert-success").fadeIn().delay(5000).fadeOut();

                        setTimeout(function() { $('#schedule-interview').modal('hide'); }, 3000);
                        $("#scheduleShowing input.form-control, #scheduleShowing textarea.form-control").val("");
                    }
                    else {
                        $("#scheduleShowing .alert.alert-danger").text(c.message);
                        $("#scheduleShowing .alert.alert-danger").fadeIn().delay(5000).fadeOut();
                    }
                    
                }
            });
        }
    );

    //show the capture leads signup form
    $(document).ready(function() {

        var myStorage = window.localStorage;

        $.ajax({
            url: '../spw/base/checkSpwCaptureLeads',
            dataType: 'json',
            beforeSend: function() {
                console.log('cheking spw Leads');
            },
            success: function(data) {

                if(data.spw_capture_leads == 1) {

                    if(data.leads_config.view_site_spw == 1) {

                        if(data.leads_config.allow_skip_spw == 1) {

                            $('#capture-leads-signup').modal('show');
                            $('.capture-leads-login-btn').show();

                        } else {

                            $('.capture-leads-login-btn').hide();
                            $('#capture-leads-signup').modal({
                                backdrop: 'static',
                                keyboard: false
                            });

                        }

                    }

                    if(data.leads_config.minute_capture_spw == 1) {

                        if(myStorage.getItem('current_time_lapse_spw')===null) {
                            myStorage.setItem('current_time_lapse_spw', (parseInt(data.leads_config.minutes_pass_spw) * 60000));
                            myStorage.removeItem('min_countdown_spw');
                        }

                        if(parseInt(myStorage.getItem('current_time_lapse_spw')) != (parseInt(data.leads_config.minutes_pass_spw) * 60000)) {
                            myStorage.setItem('current_time_lapse_spw', (parseInt(data.leads_config.minutes_pass_spw) * 60000));
                            myStorage.removeItem('min_countdown_spw');
                        }

                        var min_countdown = (myStorage.getItem('min_countdown_spw')) ? myStorage.getItem('min_countdown_spw') : parseInt(myStorage.getItem('current_time_lapse_spw'));

                        var x = setInterval(function() {

                            min_countdown -= 1000;
                            myStorage.setItem("min_countdown_spw", parseInt(min_countdown));
                            console.log(min_countdown);

                            if(min_countdown <= 0) {
                                clearInterval(x);
                                if(data.leads_config.allow_skip_spw==1) {
                                    $('#capture-leads-signup').modal('show');
                                    $('.capture-leads-login-btn').show();
                                } else {
                                    $('.capture-leads-login-btn').hide();
                                    $('#capture-leads-signup').modal({
                                        backdrop: 'static',
                                        keyboard: false
                                    });
                                }
                            }

                        }, 1000);

                    } else {

                        myStorage.removeItem('min_countdown_spw');
                        myStorage.removeItem('current_time_lapse_spw');

                    }

                }
            }
        });
    });

    // Submit Capture Leads signup form
    $("#spwSignup").submit(
        function(event) {
            event.preventDefault();
            var datum = $("#spwSignup").serialize();

            $.ajax({
                type: "POST",
                url: $("#spwSignup").attr("action"),
                data: datum,
                beforeSend: function(e) {
                    $("#signup-submit").val("Loading...");
                },
                success: function(e) {
                    var c = jQuery.parseJSON(e);
                    console.log(c);
                    $("#signup-submit").val("Submit");
                    if(c.success) {
                        $("#spwSignup .alert.alert-success").text(c.message);
                        $("#spwSignup .alert.alert-success").fadeIn().delay(5000).fadeOut();

                        setTimeout(function() { $('#capture-leads-signup').modal('hide'); }, 3000);
                        $("#spwSignup input.form-control, #spwSignup textarea.form-control").val("");
                    }else {
                        $("#spwSignup .alert.alert-danger").text(c.message);
                        $("#spwSignup .alert.alert-danger").fadeIn().delay(5000).fadeOut();
                    }
                }
            });
        }
    );

     //reset schedule showing form modal after submit
    $('#scheduleShowing').on('hidden.bs.modal', function(){
        $(this).find('form')[0].reset();
    });

    //Validate Name 
    $("input[name='customer_name']").on('change', function() {
        var $name = $(this);

        if($name.val() == "") {
            $("span.valid-customer-name").text("Please enter your name!").show().css({
                "display": "block",
                "color": "red",
            });
        } else {
            $("span.valid-customer-name").hide();
        }
    });

    //Validate Message 
    $("textarea[name='customer_message']").on('change', function() {
        var $message = $(this);

        if($message.val() == "") {
            $("span.valid-customer-message").text("Please enter your message!").show().css({
                "display": "block",
                "color": "red",
            });
        } else {
            $("span.valid-customer-message").hide();
        }
    });

     //Validate Schedule 
    $("input[name='schedule_date']").on('change', function() {
        var $schedule = $(this);

        if($schedule.val() == "") {
            $("span.valid-customer-schedule").text("Please enter date schedule!").show().css({
                "display": "block",
                "color": "red",
            });
        } else {
            $("span.valid-customer-schedule").hide();
        }
    });

     //Validation for phone numbers
    $("input[name='customer_number']").on('change', function() {
        var $phone = $(this);

        if($phone.val() != "" && !validatePhone($phone.val())) {
            $("span.valid-customer-number").text("Please use a valid phone number!").show().css({
                "display": "block",
                "color": "red",
            });
        } else {
            $("span.valid-customer-number").hide();
        }
    });
    function validatePhone(phone) {
        var ph = phone
        var filter = /^((\+[1-9]{1,4}[ \-]*)|(\([0-9]{2,3}\)[ \-]*)|([0-9]{2,4})[ \-]*)*?[0-9]{3,4}?[ \-]*[0-9]{3,4}?$/;
        if (filter.test(ph)) {
            return true;
        } else {
            return false;
        }
    }


    //Validation for email address
    $("input[name='customer_email']").on('change', function() {   
        var $email = $(this);

        if($email.val() != "" && !validateEmail($email.val())) {
            $("span.valid-customer-email").text("Please use a valid email address!").show().css({
                "display": "block",
                "color": "red",
            });
        } else {
            $("span.valid-customer-email").hide();
        }
    });
    function validateEmail(email) {
        var em = email;
        var filter = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        if(filter.test(em))
            return true;
        else
            return false;
    }

    //close modal on browser back button
    if (window.history && window.history.pushState) {
        $('#tourModal').on('show.bs.modal', function (e) {
            window.history.pushState('forward', null, './#tourModal');
        });

        $(window).on('popstate', function () {
            $('#tourModal').modal('hide');
        });
    }

});
