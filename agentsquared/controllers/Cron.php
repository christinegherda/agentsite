<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cron extends MX_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model("idx_login/idx_model");
	}

	function index() {
		$this->auto_refresh_token();
		$this->check_token_validity();
	}

	public function grant_tokens(){

		$idx_token = $this->idx_model->fetch_all_token();

		foreach($idx_token as $token) {
			$this->grant( $token->access_token );
		}

	}

	public function grant( $code = "83pk5r9tclgm1904j6xsbkgg0" ){
		
		$client_id = "d1e85y67j0cjb2axv04yhpe0i";
		$client_secret = "d3wdp13uu2uf45jokphqle0fk";
		$redirect_uri = getenv('AGENT_DASHBOARD_URL') . "idx_login/idx_callback";

		$url = "https://sparkapi.com/v1/oauth2/grant";

		$post = array(
			'client_id' 	=> $client_id,
			'client_secret' => $client_secret,
			'grant_type'	=> 'refresh_token',
			'refresh_token'	=> $code,
			'redirect_uri' 	=> $redirect_uri,
		);

		$headers = ['X-SparkApi-User-Agent: PHP-APP-In-Fifteen-Minutes/1.0'];

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 0);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post));
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST"); 
		curl_setopt($ch, CURLOPT_URL, $url);

		$result = curl_exec($ch);
		$result_info = json_decode($result, true);
		printA( $result_info ); exit;
	}

	public function auto_update_tokens(){

		$idx_token = $this->idx_model->fetch_all_token();

		foreach($idx_token as $token) {
			$this->update_spark_token( $token->user_id, $token->agent_id, $token->access_token, $token->refresh_token );
		}

	}

	public function update_spark_token($user_id, $agent_id, $access_token, $refresh_token) {

		$url = getenv('DSL_SERVER_URL') . "agent_idx_token/".$agent_id."/update";

    	$headers = array(
    		'Content-Type: application/json',
    		'X-Auth-Token: ERI9I4ku3vDyLKlOit7m_A7hbXiuOakCLgIuszmbGnw',
    		'X-User-Id: wfnbdL5z4Z7e6Ef6i'
    	);

    	$data = array(
    		'system_user_id'		=> (int)$user_id,
    		'agent_id' 				=> $agent_id,
			'access_token' 			=> $access_token,
			'refresh_token' 		=> $refresh_token,
			'date_created' 			=> date("Y-m-d h:m:s"),
			'date_modified' 		=> date("Y-m-d h:m:s"),
			'expected_token_expired' => date("Y-m-d h:m:s", strtotime('+24 hours'))
		);

		$data_json = json_encode($data, true);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 0);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT"); 
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
		curl_setopt($ch, CURLOPT_URL, $url);

		$result  = curl_exec($ch);
		$result_info = json_decode($result, true);

	}
	
	public function post_spark_token($user_id, $agent_id, $access_token, $refresh_token) {

		$url = getenv('DSL_SERVER_URL') . "agent_idx_token";

    	$headers = array(
    		'Content-Type: application/json',
    		'X-Auth-Token: ERI9I4ku3vDyLKlOit7m_A7hbXiuOakCLgIuszmbGnw',
    		'X-User-Id: wfnbdL5z4Z7e6Ef6i'
    	);

    	$data = array(
    		'system_user_id'		=> (int)$user_id,
    		'agent_id' 				=> $agent_id,
			'access_token' 			=> $access_token,
			'refresh_token' 		=> $refresh_token,
			'date_created' 			=> date("Y-m-d h:m:s"),
			'date_modified' 		=> date("Y-m-d h:m:s"),
			'expected_token_expired' => date("Y-m-d h:m:s", strtotime('+24 hours'))
		);

		$data_json = json_encode($data, true);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 0);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST"); 
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
		curl_setopt($ch, CURLOPT_URL, $url);

		$result  = curl_exec($ch);
		$result_info = json_decode($result, true);
		return TRUE;
	}

}
