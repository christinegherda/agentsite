<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Agent extends MX_Controller {

	protected $domain_user;

	function __construct() {
		parent::__construct();
		$this->domain_user = domain_to_user();
		$this->load->model('home/home_model');
	}

	public function agent_syncer() {
		
		$user_id 		= $this->domain_user->id;
		$agent_id 		= $this->domain_user->agent_id;
		$token 			= $this->home_model->getUserAccessToken($agent_id);
		$access_token 	= $token->access_token;

		$call_url = getenv('AGENT_SYNCER_URL').'onboard-agent-listings-updater/'.$user_id.'/'.$agent_id.'/onboarding?access_token='.$access_token;
		
		$headers = array('Content-Type: application/json');

		$body=array();
		$body_json = json_encode($body, true);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 0);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $body_json);
		curl_setopt($ch, CURLOPT_URL, $call_url);

		$result_info  = curl_exec($ch);

		$this->session->set_userdata('website_synced', TRUE);

		exit(json_encode(array('success'=>TRUE)));

	}

}