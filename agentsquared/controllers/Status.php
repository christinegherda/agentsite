<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Status extends MX_Controller {
	public function index()
	{
        header('Content-Type: application/json');
        header('X-Powered-By: AgentSquared');

        $status = array(
            'env' => ENVIRONMENT,
            'ci_version' => CI_VERSION,
            'as_version' => AS_VERSION,
            'database' => array(
                'stat' => $this->db->conn_id->stat,
                'benchmark' => $this->db->benchmark
            )
        );

        echo json_encode($status);
	}
}
