<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu extends MX_Controller  {

	protected $domain_user;

	function __construct() {
		parent::__construct();

		$this->domain_user = domain_to_user();
		$this->load->model("Menu_model","menu");
		$this->load->model("home_model");
	}

	public function get_custom_menu_data(){

		$custom_menu_data = $this->get_custom_menu("custom_menu");
        $build_custom_menu = $this->build_custom_menu(0,$custom_menu_data);
        return $build_custom_menu;
	}


	public function get_default_menu_data(){

		$default_menu_data = $this->get_default_menu("default_menu");
        $build_default_menu = $this->build_default_menu(0,$default_menu_data);
        return $build_default_menu;
	}

	 public function get_custom_menu($menu_type = NULL){

	  $custom_menu_data = $this->menu->get_menu_data($menu_type);

	  $custom_menu = array(
		  'items' => array(),
		  'parents' => array(),
		);

	  if(!empty($custom_menu_data)){

		  foreach($custom_menu_data as $menu){

			  $custom_menu['items'][$menu['id']] = $menu;
		    $custom_menu['parents'][$menu['parent_menu_id']][] = $menu['id'];
			}
	  }

		return $custom_menu;
	}

	 public function get_default_menu($menu_type = NULL){

	   	$default_menu_data = $this->menu->get_menu_data($menu_type);	

	   	$default_menu = array(
		    'items' => array(),
		    'parents' => array(),
		);

	  if(!empty($default_menu_data)){

	  	foreach($default_menu_data as $menu){

		  	$default_menu['items'][$menu['id']] = $menu;
	      $default_menu['parents'][$menu['parent_menu_id']][] = $menu['id'];
			}
	  }

		return $default_menu;
	}


	public function build_custom_menu($parent_menu_id, $custom_menuData)
		{
		    $html = '';
		    if (isset($custom_menuData['parents'][$parent_menu_id]))
 
		    {
		    	$base_url = base_url();

		        $html = "<ul class='custom-pages'>";

		        foreach ($custom_menuData['parents'][$parent_menu_id] as $itemId)
		        {
		        	$slug           = $custom_menuData['items'][$itemId]['slug'];
		        	$page_type      = $custom_menuData['items'][$itemId]['page_type'];
		        	$title          = ucwords($custom_menuData['items'][$itemId]['title']);
		        	$have_sub_menu  = $custom_menuData['items'][$itemId]['sub_menu'];
		        	
		        	$is_submenu_icon = !empty($have_sub_menu) ? " <i class='fa fa-angle-down' aria-hidden='true'></i>" : "" ;
		        	$has_dropdown    = !empty($have_sub_menu) ? "custom-pages-hasdropdown" : "" ;
		        	

		        	if($slug === "home" || $slug === "buyer" || $slug === "seller" || $slug === "find-a-home" || $slug === "contact" || $slug === "blog" || $slug === "testimonials"){

		        		$html .= "<li class='custom-li ".$has_dropdown."'><a href=".$base_url.$slug."> ".$title."</a>" . $is_submenu_icon;

		        	} elseif($page_type === "custom link"){

		        		$html .= "<li class='custom-li ".$has_dropdown."'><a href=".$slug." target='_blank'> ".$title."</a>" . $is_submenu_icon;

		        	} else {


		        		$html .= "<li class='custom-li ".$has_dropdown."'><a href=".$base_url."page/".$slug."> ".$title."</a>" . $is_submenu_icon;
		        	}

		            // find childitems recursively
		            $html .= $this->build_custom_menu($itemId, $custom_menuData);

		             $html .= '</li>';
		        }

		        $html .= '</ul>';
		    }

		    return $html;
	}


	public function build_default_menu($parent_menu_id, $default_menuData)
		{
			$user_info = $this->home_model->getUsersInfo($this->domain_user->id);
		    $html = '';
		    $ishide = '';

		    if (isset($default_menuData['parents'][$parent_menu_id]))
 
		    {
		    	$base_url = base_url();

		        $html = "<ul class='menu nav navbar-nav navbar-right main-nav nav-darken' itemscope itemtype=\"http://www.schema.org/SiteNavigationElement\">";

		        foreach ($default_menuData['parents'][$parent_menu_id] as $itemId)
		        {
		        	$slug           = $default_menuData['items'][$itemId]['slug'];
		        	$page_type      = $default_menuData['items'][$itemId]['page_type'];
		        	$title          = $default_menuData['items'][$itemId]['title'];
		        	$have_sub_menu  = $default_menuData['items'][$itemId]['sub_menu'];
		        	
		        	$is_submenu_icon = !empty($have_sub_menu) ? " <span class='fa fa-angle-down pull-right' aria-hidden='true'></span>" : "" ;
		        	$has_dropdown    = !empty($have_sub_menu) ? "has-dropdown" : "" ;
		        	
		        	if (!empty($user_info->plan_id) && $user_info->plan_id != 2) {
		        		$ishide = 'hide';
		        	}

		        	if($slug === "home"){
                $active = is_view($slug)? 'active ': '';
		        		$html .= "<li class='parent-li ".$active.$has_dropdown."' itemprop=\"name\"><a itemprop=\"url\" href=".$base_url."> ".$title."</a>" . $is_submenu_icon;

		        	} elseif($slug === "find-a-home" || $slug === "contact" || $slug === "blog" || $slug === "testimonials"){
                $active = is_view($slug)? 'active ': '';
		        		$html .= "<li class='parent-li ".$active.$has_dropdown."' itemprop=\"name\"><a itemprop=\"url\" href=".$base_url.$slug."> ".$title."</a>" . $is_submenu_icon;

		        	} elseif($slug === "buyer" || $slug === "seller") {
		         $active = is_view($slug)? 'active ': '';
		        		$html .= "<li class='parent-li ".$active.$has_dropdown." ".$ishide."' itemprop=\"name\"><a itemprop=\"url\" href=".$base_url.$slug."> ".$title."</a>" . $is_submenu_icon;

		        	} elseif($page_type === "custom link"){
                $active = is_view($slug)? 'active ': '';
		        		$html .= "<li class='parent-li ".$active.$has_dropdown."' itemprop=\"name\"><a itemprop=\"url\" href=".$base_url.$slug."> ".$title."</a>" . $is_submenu_icon;

		        	} else {
                $active = is_view($slug)? 'active ': '';
		        		$html .= "<li class='parent-li ".$active.$has_dropdown."' itemprop=\"name\"><a itemprop=\"url\" href=".$base_url."page/".$slug."> ".$title."</a>" . $is_submenu_icon;
		        	}

		            // find childitems recursively
		            $html .= $this->build_default_menu($itemId, $default_menuData);

		             $html .= '</li>';
		        }

		        $html .= '</ul>';
		    }

		    return $html;
	}

	public function get_custom_menu_data_raw(){

		$custom_menu_data = $this->get_custom_menu("custom_menu");
        return $custom_menu_data;
	}
	
}?>