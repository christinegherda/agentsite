<?php 
    $this->load->view('session/header'); 
    $this->load->view('session/top-nav');
?>
    <div class="content-wrapper"> 
      <div class="page-title">
        <h3 class="heading-title">
          <span> Menu Configuration</span>
        </h3>
        <p>Choose between your created pages and add it on your menu.</p>
      </div>        
        <section class="content">
            <div class="container-fluid">
                <?php if( !empty(config_item("idx_api_key")) AND !empty(config_item("idx_api_secret")) ) : ?>
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="alert alert-warning"><i class="fa fa-exclamation-triangle"></i> Your website is not live yet and is not yet visible to the public. <a href="javascript:void(0)" class="btn btn-warning btn-warning-right btn-submit pull-right"><i class="fa fa-globe"></i> Launch your site now</a></h4>
                    </div>
                </div>
                <?php endif; ?>
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-4">
                      <div class="flash-data">                           
                                <?php $session = $this->session->flashdata('session');                                  
                                    if( isset($session) )
                                    {
                                ?>
                                    <div class="alert-flash alert <?php echo ($session["status"]) ? "alert-success" : "alert-danger" ?>" role="alert"><?php echo $session["message"]; ?></div>  
                                <?php } ?>
                            
                        </div>
                        <!-- Default Menu Area -->
                        <div id="default-page-area" class="panel-group" role="tablist" aria-multiselectable="true">
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab">
                                    <h4 class="panel-title">
                                        <a href="javascript:void(0);">Default Pages</a>
                                        <i class="fa fa-angle-down pull-right" aria-hidden="true"></i>
                                    </h4>
                                </div>
                                <div class="panel-collapse collapse in moreDetails" role="tabpanel">
                                    <div class="panel-body">
                                        <div>
                                            <ul class="nav nav-tabs" role="tablist">
                                                <li role="presentation" class="active"><a href="#default-recent" aria-controls="home" role="tab" data-toggle="tab">Recent</a></li>
                                                <li role="presentation"><a href="#all-default-pages" aria-controls="profile" role="tab" data-toggle="tab">View All</a></li>
                                            </ul>
                                            
                                            <div class="tab-content">
                                                <div role="tabpanel" class="tab-pane active" id="default-recent">
                                                 <form action="<?= base_url()?>menu/add_default_menu" method="POST" class="form-horizontal add-menu-form">
                                                    <ul class="menu-list">
                                                    
                                                    <?php if( !empty($default_recent_pages) ) { 
                                                                foreach ($default_recent_pages as $rp) :
                                                    ?>  
                                                        <li>
                                                            <label class="checkbox-inline">
                                                                <input type="checkbox" name="add_default_menu[]" value="<?php echo $rp->id;?>" <?php echo ( $rp->is_menu == "1" ) ? 'checked="checked"': ""; ?> > <?php echo ucwords($rp->title);?>
                                                            </label>
                                                        </li>
                                                        
                                                        <?php endforeach; ?>

                                                        <?php } else { ?>

                                                            <p class="no-page-created text-center">
                                                                <i style="display:block" class="fa fa-2x fa-exclamation-triangle"></i> No page created yet!
                                                            </p>

                                                        <?php }?>

                                                    </ul>
                                                        <?php if(!empty($default_recent_pages)){?>
                                                             <button type="submit" class="btn btn-default btn-submit">Add to Default menu</button>
                                                        <?php }?>
                                                    </form>
                                                </div>
                                                <div role="tabpanel" class="tab-pane" id="all-default-pages">
                                                 <form action="<?= base_url()?>menu/add_default_menu" method="POST" class="form-horizontal add-menu-form">
                                                    <ul class="menu-list">
                                                       <?php if( !empty($default_pages) ) { 
                                                                foreach ($default_pages as $page) :
                                                        ?>  
                                                        <li>
                                                            <label class="checkbox-inline">
                                                                <input type="checkbox" name="add_default_menu[]" value="<?php echo $page->id;?>" <?php echo ( $page->is_menu == "1" ) ? 'checked="checked"': ""; ?> > <?php echo ucwords($page->title);?>
                                                            </label>
                                                        </li>
                                                        
                                                        <?php endforeach; ?>
                                                        <?php } else { ?>

                                                            <p class="no-page-created text-center">
                                                                <i style="display:block" class="fa fa-2x fa-exclamation-triangle"></i> No page created yet!
                                                            </p>

                                                        <?php }?>

                                                    </ul>
                                                        <?php if(!empty($default_pages)){?>
                                                            <button type="submit" class="btn btn-default btn-submit">Add to Default menu</button>
                                                        <?php }?>  
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                         <!-- Custom Menu Area -->
                        <div id="custom-page-area" class="panel-group" role="tablist" aria-multiselectable="true">
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab">
                                    <h4 class="panel-title">
                                        <a href="javascript:void(0);">Custom Pages</a>
                                        <i class="fa fa-angle-down pull-right" aria-hidden="true"></i>
                                    </h4>
                                </div>
                                <div class="panel-collapse collapse in moreDetails" role="tabpanel">
                                    <div class="panel-body">
                                        <div>
                                            <ul class="nav nav-tabs" role="tablist">
                                                <li role="presentation" class="active"><a href="#custom-recent" aria-controls="home" role="tab" data-toggle="tab">Recent</a></li>
                                                <li role="presentation"><a href="#all-custom-pages" aria-controls="profile" role="tab" data-toggle="tab">View All</a></li>
                                            </ul>
                                            
                                            <div class="tab-content">
                                                <div role="tabpanel" class="tab-pane active" id="custom-recent">
                                                 <form action="<?= base_url()?>menu/add_custom_menu" method="POST" class="form-horizontal add-menu-form">
                                                    <ul class="menu-list">
                                                    
                                                    <?php if( !empty($custom_recent_pages) ) { 
                                                                foreach ($custom_recent_pages as $rp) :
                                                    ?>  
                                                        <li>
                                                            <label class="checkbox-inline">
                                                                <input type="checkbox" name="add_custom_menu[]" value="<?php echo $rp->id;?>" <?php echo ( $rp->is_menu == "1" ) ? 'checked="checked"': ""; ?> > <?php echo ucwords($rp->title);?>
                                                            </label>
                                                        </li>
                                                        
                                                        <?php endforeach; ?>

                                                        <?php } else { ?>

                                                            <p class="no-page-created text-center">
                                                                <i style="display:block" class="fa fa-2x fa-exclamation-triangle"></i> No page created yet!
                                                            </p>

                                                        <?php }?>

                                                    </ul>
                                                        <?php if(!empty($custom_recent_pages)){?>
                                                             <button type="submit" class="btn btn-default btn-submit">Add to Custom menu</button>
                                                        <?php }?>
                                                    </form>
                                                </div>
                                                <div role="tabpanel" class="tab-pane" id="all-custom-pages">
                                                 <form action="<?= base_url()?>menu/add_custom_menu" method="POST" class="form-horizontal add-menu-form">
                                                    <ul class="menu-list">
                                                       <?php if( !empty($custom_pages) ) { 
                                                                foreach ($custom_pages as $page) :
                                                        ?>  
                                                        <li>
                                                            <label class="checkbox-inline">
                                                                <input type="checkbox" name="add_custom_menu[]" value="<?php echo $page->id;?>" <?php echo ( $page->is_menu == "1" ) ? 'checked="checked"': ""; ?> > <?php echo ucwords($page->title);?>
                                                            </label>
                                                        </li>
                                                        
                                                        <?php endforeach; ?>
                                                        <?php } else { ?>

                                                            <p class="no-page-created text-center">
                                                                <i style="display:block" class="fa fa-2x fa-exclamation-triangle"></i> No page created yet!
                                                            </p>

                                                        <?php }?>

                                                         <div class="col-md-12">
                                                            <?php if( isset($custom_ajax_pagination) ) : ?>
                                                                <div class="pagination-area pull-right">
                                                                    <nav>
                                                                        <ul class="pagination">
                                                                            <?php echo $custom_ajax_pagination; ?>
                                                                        </ul>
                                                                    </nav>
                                                                </div>
                                                            <?php endif; ?>
                                                        </div>

                                                    </ul>
                                                        <?php if(!empty($custom_pages)){?>
                                                            <button type="submit" class="btn btn-default btn-submit">Add to Custom menu</button>
                                                        <?php }?>  
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab">
                                    <h4 class="panel-title">
                                        <a href="javascript:void(0);"> Custom Link</a>
                                        <i class="fa fa-angle-down pull-right" aria-hidden="true"></i>
                                    </h4>
                                </div>
                                <div class="panel-collapse collapse in" role="tabpanel">
                                    <div class="panel-body">
                                         <form action="<?= base_url()?>menu/add_custom_link_menu" method="POST" id="customLinkMenu" class="form-horizontal add-custom-menu-form">
                                             <div class="form-group">
                                                <label for="menu-url">Url</label>
                                                <input type="text" class="form-control" id="custom-menu-url" name="custom_menu_url" placeholder="http://page.com">
                                              </div>
                                              <div class="form-group">
                                                <label for="menu-name">Menu Name</label>
                                                <input type="text" class="form-control" id="custom-menu-name" name="custom_link_menu_name">
                                              </div>
                                            <button type="submit" class="btn btn-default btn-submit">Add to Custom menu</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-8">

                        <div class="save-property-listing menu-tab">

                          <!-- Nav tabs -->
                          <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a class="defaultMenu" href="#default-menu" aria-controls="home" role="tab" data-toggle="tab">Default Menu</a></li>
                            <li role="presentation"><a class="customMenu" href="#custom-menu" aria-controls="profile" role="tab" data-toggle="tab">Custom Menu</a></li>
                          </ul>

                          <!-- Tab panes -->
                          <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="default-menu">

                              <!-- Display Default Menu -->
                            <?php
                                if (isset($default_dd_menu)) { 

                                    if (count($default_dd_menu) > 0) {
                                        ?>
                                        <div class="row">
                                            <p>Drag and Drop Menu to set Menu order and Submenu.</p> 
                                            <div class="clear"></div>
                                        </div>
                                        <div class="dd nestable" id="nestable">
                                            <?php
                                                if(isset($build_default_menu_dashboard) && !empty($build_default_menu_dashboard)){

                                                echo $build_default_menu_dashboard;
                                            }?>
                                             
                                        </div>

                                    <?php } else { ?>

                                         <p class="text-center">No Menu Created!</p>  

                                    <?php }

                                } else { ?>

                                    <p class="text-center">No Pages created!</p>

                                <?php } ?>


                                <?php if(isset($build_default_menu_dashboard) && !empty($build_default_menu_dashboard)){?>

                                     <button style="margin-top: 10px;" class="btn btn-default btn-submit" id="saveDefaultMenuOrder">Save</button>

                                <?php }?>

                               
                              <!-- <form style="display: none;" id="menu-editor">
                                    <h3>Editing <span id="currentEditName"></span></h3>
                                    <div class="form-group">
                                      <label for="addInputName">Name</label>
                                        <input type="hidden" id="menu-id" name="currentEditId" value="">
                                        <input type="hidden" id="menu-slug" name="currentEditSlug" value="">
                                        <input type="hidden" id="menu-page-type" name="currentEditPageType" value="">
                                        <input type="text" class="form-control input-name" name="page_title" id="editInputName" placeholder="Item name" value="" required>
                                    </div>
                                    <button type="submit" class="btn btn-default btn-submit" id="editButton">Save</button>
                                </form> -->

                            </div>
                            <div role="tabpanel" class="tab-pane" id="custom-menu">

                                 <!-- Display Custom Menu -->
                                <?php
                                    if (isset($custom_dd_menu)) { 

                                        if (count($custom_dd_menu) > 0) {
                                            ?>
                                            <div class="row">
                                                <p>Drag and Drop Menu to set Menu order and Submenu.</p> 
                                                <div class="clear"></div>
                                            </div>
                                            <div class="dd nestable" id="nestable-custom">
                                                <?php
                                                    if(isset($build_custom_menu_dashboard) && !empty($build_custom_menu_dashboard)){

                                                    echo $build_custom_menu_dashboard;
                                                }?>
                                                 
                                            </div>

                                        <?php } else { ?>

                                             <p class="text-center">No Menu Created!</p>  

                                        <?php }

                                    } else { ?>

                                        <p class="text-center">No Pages created!</p>

                                    <?php } ?>


                                    <?php if(isset($build_custom_menu_dashboard) && !empty($build_custom_menu_dashboard)){?>

                                         <button style="margin-top: 10px;" class="btn btn-default btn-submit" id="saveCustomMenuOrder">Save</button>

                                    <?php }?>

                                   <!-- <form style="display: none;" id="menu-editor1">
                                        <h3>Editing <span id="currentEditName1"></span></h3>
                                        <div class="form-group">
                                          <label for="addInputName">Name</label>
                                            <input type="hidden" id="menu-id" name="currentEditId" value="">
                                            <input type="hidden" id="menu-slug" name="currentEditSlug" value="">
                                            <input type="hidden" id="menu-page-type" name="currentEditPageType" value="">
                                            <input type="text" class="form-control input-name" name="page_title" id="editInputName1" placeholder="Item name" value="" required>
                                        </div>
                                        <button type="submit" class="btn btn-default btn-submit" id="editButton1">Save</button>
                                    </form> -->
                            </div>

                            <form style="display: none;" id="menu-editor">
                                <h3>Editing <span id="currentEditName"></span></h3>
                                <div class="form-group">
                                  <label for="addInputName">Name</label>
                                    <input type="hidden" id="menu-id" name="currentEditId" value="">
                                    <input type="hidden" id="menu-slug" name="currentEditSlug" value="">
                                    <input type="hidden" id="menu-page-type" name="currentEditPageType" value="">
                                    <input type="text" class="form-control input-name" name="page_title" id="editInputName" placeholder="Item name" value="" required>
                                </div>
                                <button type="submit" class="btn btn-default btn-submit" id="editButton">Save</button>
                            </form>

                          </div>
                        </div> 
                         
                    </div>
                </div>
            </div>
        </section>
    </div>

    <?php $this->load->view('session/footer'); ?>
    
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

    <script type="text/javascript">
        $(document).ready(function(){
            //Save Menu order
            $("#saveDefaultMenuOrder").off("click").on("click", function() {

                    var product_data = $("#nestable").nestable("serialize");
                    var data = {product_data: product_data};
                    var url = base_url + "menu/update_default_menu_priority";
                    console.log(data);

                    $.ajax({
                        url: url,
                        type: "post",
                        dataType: "json",
                        data: data,
                        beforeSend: function() { },
                        success: function(result) {
                            if (result['status'] == "success") {
                                location.reload();
                            }
                        }
                    });
            });

             $("#saveCustomMenuOrder").off("click").on("click", function() {

                    var product_data = $("#nestable-custom").nestable("serialize");
                    var data = {product_data: product_data};
                    var url = base_url + "menu/update_custom_menu_priority";
                    console.log(data);

                    $.ajax({
                        url: url,
                        type: "post",
                        dataType: "json",
                        data: data,
                        beforeSend: function() { },
                        success: function(result) {
                            if (result['status'] == "success") {
                                location.reload();
                            }
                        }
                    });
            });




          // Edit Menu Page Title
            $("#editButton").off("click").on("click", function() {

                    var data = $("#menu-editor").serialize();
                    var menu_id = $("#menu-id").val();
                    var menu_slug = $("#menu-slug").val();
                    var page_type = $("#menu-page-type").val();

                    if(menu_slug == "find-a-home" || menu_slug == "testimonials"){

                        var url = base_url + "menu/edit_title/"+menu_id;

                    } else if(page_type == "custom link"){ 

                        var url = base_url + "menu/edit_custom_title/"+menu_id;

                    } else {

                        var url = base_url + "menu/edit_page_title/"+menu_id;
                    }

                    console.log(url);

                    $.ajax({
                        url: url,
                        type: "post",
                        dataType: "json",
                        data: data,
                        beforeSend: function() { },
                        success: function(result) {
                            if (result['status'] == "success") {
                                location.reload();
                            }
                        }
                    });
    
            });

            $('#nestable').nestable({
                maxDepth: 5,
                //group : 1
            })
            .on('change', updateOutput);

             $('#nestable-custom').nestable({
                maxDepth: 5,
                //group : 1
            })
            .on('change', updateOutput);
    
        });
    </script>
  




