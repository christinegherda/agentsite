<?php if( ! defined('BASEPATH') ) exit('No direct script access allowed');

class Menu_model extends CI_Model { 

	protected $domain_user;

    public function __construct()
    {
        parent::__construct();

        $this->db_slave =  $this->load->database('db_slave', TRUE);
        $this->domain_user = domain_to_user();
    }
	
    public function get_menu_data($menu_type = NULL) {

        $query = $this->db_slave->query("SELECT id,parent_menu_id,title,slug,page_type,(select GROUP_CONCAT(id) as where_in_id from parent_menu where parent_menu_id = pages.id) as sub_menu FROM pages WHERE is_menu = 1 AND status = 'published' AND menu_type = '".$menu_type."' AND agent_id = ".$this->domain_user->agent_id." ORDER BY menu_order");

        $result = $query->result_array();

        return (!empty($result)) ? $result : FALSE;
    }
}
