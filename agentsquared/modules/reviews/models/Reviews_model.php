<?php if( ! defined('BASEPATH') ) exit('No direct script access allowed');

class Reviews_model extends CI_Model { 

	protected $domain_user;

    public function __construct()
    {
        parent::__construct();

        $this->domain_user = domain_to_user();
        $this->db_slave =  $this->load->database('db_slave', TRUE);
    }

    public function fetch_testimonials() {

    	$this->db_slave->select("t.*, c.first_name, c.last_name, c.email")
    			->from("testimonials t")
    			->join("contacts c", "t.customer_id=c.id", "left")
    			->where('t.user_id', $this->domain_user->id)
    			->order_by("t.id", "desc");

    	$query = $this->db_slave->get();

    	return ($query->num_rows() > 0) ? $query->result() : FALSE;
    }

    public function fetch_approved_reviews($param=array()) {
        
    	$this->db_slave->select("t.*, c.first_name, c.last_name, c.email")
    			->from("testimonials t")
    			->join("contacts c", "t.customer_id=c.contact_id", "left")
    			->where('t.user_id', $this->domain_user->id)
    			->where('t.is_approved', 1);

        if(!empty($param["limit"])) { 
            $this->db_slave->limit($param["limit"]);
        } 

        if(!empty($param["offset"])) {
            $this->db_slave->offset($param["offset"]);
        }

        $this->db_slave->order_by("t.id", "desc");

    	$query = $this->db_slave->get();

    	return ($query->num_rows() > 0) ? $query->result() : FALSE;
    }

    public function count_approved_reviews() {
        $this->db_slave->select("*")
                ->from("testimonials")
                ->where("user_id", $this->domain_user->id)
                ->where("is_approved", 1);

        return $this->db_slave->count_all_results();
    }

    public function sort_by_highest($param=array()) {
        $this->db_slave->select("t.*, c.first_name, c.last_name, c.email")
                ->from("testimonials t")
                ->join("contacts c", "t.customer_id=c.id", "left")
                ->where('t.user_id', $this->domain_user->id)
                ->where('t.is_approved', 1)
                ->order_by("t.rating","desc");

        if(!empty($param["limit"])) { 
                $this->db_slave->limit($param["limit"]);
        } 

        if(!empty($param["offset"])) {
            $this->db_slave->offset($param["offset"]);
        }

        $query = $this->db_slave->get();

        return ($query->num_rows() > 0) ? $query->result() : FALSE;
    }

    public function sort_by_lowest($param=array()) {
        $this->db_slave->select("t.*, c.first_name, c.last_name, c.email")
                ->from("testimonials t")
                ->join("contacts c", "t.customer_id=c.id", "left")
                ->where('t.user_id', $this->domain_user->id)
                ->where('t.is_approved', 1)
                ->order_by("t.rating");

        if(!empty($param["limit"])) { 
                $this->db_slave->limit($param["limit"]);
        } 

        if(!empty($param["offset"])) {
            $this->db_slave->offset($param["offset"]);
        }

        $query = $this->db_slave->get();

        return ($query->num_rows() > 0) ? $query->result() : FALSE;
    }

    public function sort_by_newest($param=array()) {
        $this->db_slave->select("t.*, c.first_name, c.last_name, c.email")
            ->from("testimonials t")
            ->join("contacts c", "t.customer_id=c.id", "left")
            ->where('t.user_id', $this->domain_user->id)
            ->where('t.is_approved', 1)
            ->order_by("t.date_created", "desc");

        if(!empty($param["limit"])) { 
                $this->db_slave->limit($param["limit"]);
        } 

        if(!empty($param["offset"])) {
            $this->db_slave->offset($param["offset"]);
        }

        $query = $this->db_slave->get();

        return ($query->num_rows() > 0) ? $query->result() : FALSE;
    }

    public function sort_by_oldest($param=array()) {
        $this->db_slave->select("t.*, c.first_name, c.last_name, c.email")
                ->from("testimonials t")
                ->join("contacts c", "t.customer_id=c.id", "left")
                ->where('t.user_id', $this->domain_user->id)
                ->where('t.is_approved', 1)
                ->order_by("t.date_created");

        if(!empty($param["limit"])) { 
                $this->db_slave->limit($param["limit"]);
        } 

        if(!empty($param["offset"])) {
            $this->db_slave->offset($param["offset"]);
        }

        $query = $this->db_slave->get();

        return ($query->num_rows() > 0) ? $query->result() : FALSE;
    }

    public function changed_status($id=NULL, $data=array()) {
    	$ret = FALSE;

    	if($id && $data) {
    		$this->db->where('id',$id);
    		$this->db->update('testimonials', $data);

 			$ret = ($this->db->affected_rows() > 0) ? TRUE : FALSE;
    	}

    	return $ret;
    }

    public function delete_testimonial($id=NULL) {
    	$ret = FALSE;
    	if($id) {
    		$this->db->where('id', $id);
    		$this->db->delete('testimonials');
    		$ret = ($this->db->affected_rows() > 0) ? TRUE : FALSE;
    	}

    	return $ret;
    }
}