<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reviews extends MX_Controller {

	function __construct() {
		parent::__construct();

		if(!$this->session->userdata('logged_in_auth'))
			redirect('login/auth/login', 'refresh');

        $this->load->model('reviews_model', 'review');
	}

	function index() {
		$data['title'] = "Reviews";
		$siteInfo = Modules::run('agent_sites/getInfo');
		$data['branding'] = $siteInfo['branding'];
		$data["testimonials"] = $this->review->fetch_testimonials();
		$this->load->view('reviews', $data);
	}

	function approve() {
		if($this->input->post('id')) {
			$data = array('is_approved'=>1);
			$status = $this->review->changed_status($this->input->post('id'), $data);
			if($status)
				$this->session->set_flashdata("message", "<p>Testimonial approved and can now be seen in home page!</p>" );
		}
		redirect('reviews', 'refresh');
	}

	function remove() {
		if($this->input->post('id')) {
			$status = $this->review->delete_testimonial($this->input->post('id'));
			if($status)
				$this->session->set_flashdata("message", "<p>Testimonial successfully removed!</p>" );
		}
		redirect('reviews', 'refresh');
	}
	function fetch_approved_reviews($param=array()) {
		$result = $this->review->fetch_approved_reviews($param);
		return ($result) ? $result : FALSE;
	}
	function compute_average_review() {
		$param["offset"] = 0;
		$param["limit"] = 0;

		$result = $this->fetch_approved_reviews($param);
		$count=0;
		$total=0;
		$average=0;

		foreach($result as $res) {
			$total+=$res->rating;
			$count++;
		}

		$average = $total/$count;
		$data = array('average'=>round($average), 'total'=>$count);

		return $data;
	}
	function get_sort_reviews($param=array()) {
		if($this->input->get('reviews_order')) {
			$sort_by = $this->input->get('reviews_order');
			switch ($sort_by) {
			    case "highest":
			    	$sort = $this->review->sort_by_highest($param);
			    	return ($sort) ? $sort : FALSE;
			        break;
			    case "lowest":
			    	$sort = $this->review->sort_by_lowest($param);
			    	return ($sort) ? $sort : FALSE;
			        break;
			    case "newest":
			    	$sort = $this->review->sort_by_newest($param);
			    	return ($sort) ? $sort : FALSE;
			        break;
			   	case "oldest":
			   		$sort = $this->review->sort_by_oldest($param);
			    	return ($sort) ? $sort : FALSE;
			   		break;
			    default:
			    	$sort = $this->review->fetch_approved_reviews($param);
					return ($sort) ? $result : FALSE;
			}
		}
	}
}
