<?php 
    $this->load->view('session/header'); 
    $this->load->view('session/top-nav');
?> 
<div class="content-wrapper">
  <div class="page-title">
    <h3 class="heading-title">
      <span>Reviews</span>
    </h3>
  </div>
  <section class="content">
    <div class="row">
  		<div class="col-md-12">
  		  <div class="table-responsive">
          <?php $message = $this->session->flashdata("message");?>
          <?php if(isset($message)) : ?>
              <div class="alert alert-success flash-data" role="alert"><?php echo $message; ?></div>
          <?php endif;?>
          <table class="table table-striped tablesorter" id="myTable">
            <thead>
              <tr>
                <th class="col-md-1">Status</th>
                <th class="col-md-2">Name</th>
                <th class="col-md-2">Email</th>
                <th class="col-md-2">Rating</th>
                <th class="col-md-2">Subject</th>
                <th class="col-md-4">Message</th>                                
                <th class="col-md-2">Action</th>                               
              </tr>
            </thead>
            <tbody>
            <?php 
              if($testimonials) { 
                foreach($testimonials as $review) { ?>
                  <tr>
                    <td><?=($review->is_approved) ? "Approved" : "Pending";?></td>
                    <td><?=$review->first_name." ".$review->last_name?></td>
                    <td><?=$review->email?></td>
                    <td><div class="ratefixed" data-rate="<?=$review->rating > 5 ? 5 : $review->rating; ?>"></div></td>
                    <td><?=$review->subject?></td>
                    <td class="testimonial-message">
                      <?php 
                        $message = strip_tags($review->message);
                        $message = substr($message, 0, 50);
                        echo $message.'... <a href="javascript:;" data-toggle="modal" data-target="#review-modal" data-title="'.$review->subject.'" data-content="'.$review->message.'" class="message-modal">read more</a>';
                      ?>
                    </td>
                    <td>
                      <?php 
                        if($review->is_approved) { ?>
                          <form action="<?=base_url();?>reviews/remove" method="POST">
                            <input type="hidden" name="id" value="<?=$review->id?>">
                            <button class='btn btn-default approved-review-button'>Remove</button>
                          </form>
                      <?php
                        } else { ?>
                          <form action="<?=base_url();?>reviews/approve" method="POST">
                            <input type="hidden" name="id" value="<?=$review->id?>">
                            <button type="submit" class='btn btn-default btn-warning pending-review-button'>Approve</button>
                          </form>
                      <?php
                        }
                      ?>
                    </td>
                  </tr>
            <?php
                }
              } 
            ?>
            </tbody>
          </table>
  		  </div>
      </div>
    </div>
  </section>
  <!-- Modal -->
  <div class="modal fade" id="review-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="modal-body">
          <p class="message-content">
          </p>
        </div>
      </div>
    </div>
  </div>
</div>

<?php $this->load->view('session/footer'); ?>
