<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Idx_login extends MX_Controller {

	protected $domain_user;

	public function __construct()
	{
		parent::__construct();

		$this->domain_user = domain_to_user();
		$this->load->model('pages/page_model', 'page');
		$this->load->model('customize_homepage/customize_homepage_model', 'custom');
		$this->load->model('idx_login/idx_model', 'idx_model');
	}

	public function index()
	{
		//Identify where the user came from (is it from spark or from landing page)
		$from = ($this->input->get("from")) ? $this->input->get("from") : "";
		$pricing = ($this->input->get("pricing")) ? $this->input->get("pricing") : "";
		$app_type = ($this->input->get("app_type")) ? $this->input->get("app_type") : "";

		if($from == "website") {

			$website['pricing'] = $pricing;
			$website['app_type'] = $app_type;
			$website['from_website'] = TRUE;

			$this->session->set_userdata($website);
		}

		if(!$this->session->userdata('access_token')) {
			$this->spark_login();
			exit;
		}

	    $account_information = $this->myaccount($this->session->userdata('access_token'));

		if(!$account_information && !$account_information["D"]["Results"][0]["Emails"]["Address"]) {

			$data['title'] = "Invalid MLS";
			$data['message'] = "Sorry we cannot create an agent site for your account...Please make sure your MLS setup is correct in flexMLS.";
			$this->load->view('invalid_mls', $data);

		} else {
			//Set email and agent id
			$email 		= $account_information["D"]["Results"][0]["Emails"][0]["Address"];
			$agent_id 	= $account_information["D"]["Results"][0]["Id"];
			$agent_id_exist = $this->idx_model->readAgentID($agent_id);

			if($agent_id_exist) {

				$row = $this->idx_model->check_sync_status($agent_id);

				$this->update_spark_token($row->id, $row->agent_id, $this->session->userdata("access_token"), $this->session->userdata("refresh_token"));

				if($row->sync_status == 0) {

					$arr = array(
						'user_id'		=> $row->id,
						'agent_id'		=> $row->agent_id,
						'email'	 		=> $row->email,
						'code' 			=> $row->code,
						'identity' 		=> $row->email,
						'old_last_login' => $row->created_on,
						'logged_in_auth' => true,
					);

					$this->session->set_userdata($arr);
					$this->resync_data_view($row->id, $row->agent_id);

				} else {

					$arr = array(
						'user_id'		=> $row->id,
						'agent_id'		=> $row->agent_id,
						'email'	 		=> $row->email,
						'code' 			=> $row->code,
						'identity' 		=> $row->email,
						'old_last_login' => $row->created_on,
						'logged_in_auth' => true,
					);

					$this->session->set_userdata($arr);

					$this->agent_subscription($row->agent_id);

					if($this->session->userdata('agent_idx_website')) {
						redirect('dashboard', 'refresh');
					} else {
						redirect('agent_sites/single_listings', 'refresh');
					}
				}

			} else {

				$code = generate_key(8);

				$user = array(
					'id' 			=> $account_information["D"]["Results"][0]["Id"],
					'first_name'	=> (isset($account_information["D"]["Results"][0]["FirstName"]) && !empty($account_information["D"]["Results"][0]["FirstName"])) ? $account_information["D"]["Results"][0]["FirstName"] : "Not Specified",
					'last_name'		=> (isset($account_information["D"]["Results"][0]["LastName"]) && !empty($account_information["D"]["Results"][0]["LastName"])) ? $account_information["D"]["Results"][0]["LastName"] : "Not Specified",
					'company' 		=> (isset($account_information["D"]["Results"][0]["Office"]) && !empty($account_information["D"]["Results"][0]["Office"])) ? $account_information["D"]["Results"][0]["Office"] : "Not Specified",
					'email' 		=> $email,
					'password' 		=> 'test1234',
					'phone' 		=> (isset($account_information["D"]["Results"][0]["Phones"][0]["Number"]) && !empty($account_information["D"]["Results"][0]["Phones"][0]["Number"])) ? $account_information["D"]["Results"][0]["Phones"][0]["Number"] : "Not Specified",
					'city' 			=> (isset($account_information["D"]["Results"][0]["Addresses"]["City"]) && !empty($account_information["D"]["Results"][0]["Addresses"]["City"])) ? $account_information["D"]["Results"][0]["Addresses"]["City"] : "Not Specified",
					'zip' 			=> (isset($account_information["D"]["Results"][0]["Addresses"]["PostalCode"]) && !empty($account_information["D"]["Results"][0]["Addresses"]["PostalCode"])) ? $account_information["D"]["Results"][0]["Addresses"]["PostalCode"] : "Not Specified",
					'address' 		=> (isset($account_information["D"]["Results"][0]["Addresses"]["Address"]) && !empty($account_information["D"]["Results"][0]["Addresses"]["Address"])) ? $account_information["D"]["Results"][0]["Addresses"]["Address"] : "Not Specified",
					'broker' 		=> (isset($account_information["D"]["Results"][0]["Office"]) && !empty($account_information["D"]["Results"][0]["Office"])) ? $account_information["D"]["Results"][0]["Office"] : "Not Specified",
					'theme' 		=> 'agent-haven',
					'code' 			=> $code,
					'ip_address' 	=> $this->input->ip_address(),
					'date_signed' 	=> date('Y-m-d H:i:s'),
				);

				//insert pages
				$this->page->insert_home_page($user);
				$this->page->insert_find_home_page($user);
				$this->page->insert_seller_page($user);
				$this->page->insert_buyer_page($user);
				$this->page->insert_contact_page($user);
				$this->page->insert_blog_page($user);
				$this->page->insert_testimonial_page($user);

				//insert home options
				$this->custom->insert_active_option($user);
				$this->custom->insert_new_option($user);
				$this->custom->insert_nearby_option($user);
				$this->custom->insert_office_option($user);

				//Identify if the user launches from spark or from pricing page
				if($from == "spark") {

					$user['pricing'] = "";
					$user['trial'] = 0;

					$user_id = $this->idx_model->insert_spark_user($user);

					if($user_id) {

						$user = $this->idx_model->fetch_user($user_id);

						//Save tokens to DSL
						$this->save_spark_tokens($user[0]['id'], $user[0]['agent_id'], $this->session->userdata("access_token"), $this->session->userdata("refresh_token"));

						$arr = array(
							'user_id' 		=> $user[0]['id'],
							'agent_id' 		=> $user[0]['agent_id'],
							'email' 		=> $user[0]['email'],
							'code' 			=> $user[0]['code'],
							'identity' 		=> $user[0]['email'],
							'old_last_login' => $user[0]['created_on'],
							'logged_in_auth' => true
						);

						$this->session->set_userdata($arr);

						if($this->session->userdata("agent_idx_website")) {
							$this->send_notification_to_user_idx($user);
						}

						$this->sync_data_view($user_id, $user[0]['agent_id']);
					}

				} else {

					if($app_type == "idx") {
						$ApplicationType = "Agent_IDX_Website";
					} elseif($app_type == "spw") {
						$ApplicationType = "Single_Property_Website";
					} elseif($app_type == "idxpro") {
						$ApplicationType = "Agent_IDX_Website_Pro";
					}

					//Insert to agent_subscription
					$data = array(
					   'ApplicationName' 	=> ($app_type == "idx") ? "Instant IDX Website powered by Flexmls" : "Single Property Website Social Media Sharing Tool",
					   'ApplicationUri' 	=> "http://apiwebsites.net/pricing/",
					   'CallbackName' 		=> "Agent Purchase",
					   'EventType' 			=> "purchased",
					   'PricingPlan' 		=> $pricing,
					   'TestOnly' 			=> "false",
					   'Timestamp' 			=> date('Y-m-d H:i:s'),
					   'UserEmail' 			=> $email,
					   'UserId' 			=> $account_information["D"]["Results"][0]["Id"],
					   'UserName' 			=> $account_information["D"]["Results"][0]["Name"],
					   'ApplicationType' 	=> 'Agent_IDX_Website',
					);

					$this->idx_model->insert_agent_subscription($data);

					//Insert to users
					$user['pricing'] = $pricing;
					$user['trial'] = 1;

					$user_id = $this->idx_model->insert_spark_user($user);

					if($user_id) {
						//Save the token to DSL
						$this->save_spark_tokens($user_id, $user['id'], $this->session->userdata("access_token"), $this->session->userdata("refresh_token"));
						redirect(base_url()."register/wizard?user_id=".$user_id."&pricing=".$pricing."&app_type=".$app_type);
					}
				}
			}
		}
	}

	public function spark_login() {

		$client_id = "d1e85y67j0cjb2axv04yhpe0i";
		$client_secret = "d3wdp13uu2uf45jokphqle0fk";
		$redirect_uri = "https://agentsquared.com/as-agent-site-v3/idx_login/idx_callback";

		$url = "https://sparkplatform.com/openid?openid.mode=checkid_setup&openid.return_to=".$redirect_uri."&openid.spark.client_id=".$client_id."&openid.spark.combined_flow=true";
		header('Location: ' . $url);
		exit;
	}

	public function myaccount($access_token) {

		if($access_token) {
			$url = "https://sparkapi.com/v1/my/account";

			$headers = array(
				'Content-Type: application/json',
				'User-Agent: Spark API PHP Client/2.0',
				'X-SparkApi-User-Agent: PHP-APP-In-Fifteen-Minutes/1.0',
			    'Authorization: OAuth '. $access_token,
			);

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_URL, $url);

			$result = curl_exec($ch);
			$result_info = json_decode($result, true);

			if(empty($result_info)) {
				$result_info = curl_strerror($ch);
			}
            curl_close($ch);
			//printA($result_info);
			return $result_info;
		}
	}

	public function get_listing($access_token) {

		if($access_token) {

			$obj = modules::load("dsl");

			$filterStr = "(MlsStatus Eq 'Active' Or MlsStatus Eq 'Active Contingent')";
			$filter_url = rawurlencode($filterStr);
			$filter_string = substr($filter_url, 3, -3);

			$body = array("access_token"=>$access_token);

			$endpoint = "spark-listings?_limit=1&_pagination=1&_page=1&_filter=(".$filter_string.")&_expand=PrimaryPhoto";

			$results = $obj->post_endpoint($endpoint, $body);
			$return = json_decode($results, true);

			return $return["data"];
		}
	}

	public function send_notification_to_user_idx($user = array())
	{
	 	$this->load->library("email");

		$email = $user[0]['email'];
		$first_name = $user[0]['first_name'];

	 	$content = "Hi ".$first_name.", <br><br>

			Congratulations, your Instant IDX website is now ready! <br><br>

			We used the information inside your Flexmls dashboard to automatically create your very own Instant IDX website. Most of the heavy lifting has already been done but we would like you to login to your account to add your own personal flare and make your website standout from the rest. We recommend that you create a custom domain name, add a site name, add a banner tagline and fill out the About Me section to tell your clients what they can expect by choosing you to represent them for their real estate needs. All this can be found inside your agent dashboard. <br><br>

			Please use the login credentials below to access your IDX website dashboard: <br><br>

			Login URL: " . getenv('AGENT_DASHBOARD_URL') . "login/auth/login  <br>
			Email: ".$email." <br>
			Password: test1234

			<br><br>
			If you have any questions or would like assistance in setting up your IDX website feel free contacting us direct at (800) 901-4428 or send an email to support@agentsquared.com
			<br>
			We look forward to serving you!
			<br><br>
			All the best, <br>
			Team AgentSquared <br>
			<a href='www.AgentSquared.com'>www.AgentSquared.com</a>
	 	";

	 	$subject ="Your IDX website is ready!" ;

	 	return $this->email->send_email(
	 		'sales@agentsquared.com',
	 		'Agentsquared',
	 		$email,
	 		$subject,
	 		'email/template/default-email-body',
	 		array(
	 			"message" => $content,
	 			"subject" => $subject,
	 			"to_bcc" => "troy@agentsquared.com,eugene@agentsquared.com,agentsquared888@gmail.com,albert@agentsquared.com,sales@agentsquared.com,rolbru12@gmail.com"
	 		)
	 	);
	}

	public function sync_data_view($user_id, $agent_id) {

		$query = $this->db->query('SET GLOBAL max_allowed_packet=256*1024*1024');

		$data['title'] = "Sync MLS Data";
		$data['user_id'] = $user_id;

		//identify agent subscription and set it to session
		$this->agent_subscription($agent_id);
		$data['agent_idx_type'] = ($this->session->userdata('agent_idx_website')) ? "TRUE" : "FALSE";

		$this->load->view('idx_login/sync_loader', $data);
	}

	public function resync_data_view($user_id, $agent_id) {
		$query = $this->db->query('SET GLOBAL max_allowed_packet=256*1024*1024');
		$data['title'] = "Resync MLS Data";
		$data['user_id'] = $user_id;

		//identify agent subscription and set it to session
		$this->agent_subscription($agent_id);
		$data['agent_idx_type'] = ($this->session->userdata('agent_idx_website')) ? "TRUE" : "FALSE";

		$this->load->view('idx_login/resync', $data);
	}

	//{POST} /sync-new-agent (metas)
	public function sync_new_agent() {

		$dsl = modules::load('dsl/dsl');

		$endpoint = "sync-new-agent";

		$body = array("access_token" => $this->session->userdata("access_token"),
					"bearer_token" => "asdf1234"
				);

		$result = $dsl->post_endpoint($endpoint, $body);

		$check_syncing = "sync-status/".$this->domain_user->id;

		do {

			$ch = $dsl->get_endpoint($check_syncing);
			$check = json_decode($ch, true);

		} while($check['account_status'] == true);

		$response = (!empty($result)) ? $response = array("success" => TRUE) : $response = array("success" => FALSE);

		exit(json_encode($response));
	}

	//{POST} /listings/nearby (4 nearby)
	public function sync_nearby_listings() {

		$listing = $this->get_listing($this->session->userdata("access_token"));
		//printA($listing);exit;
		$latitude = $listing[0]["StandardFields"]["Latitude"];
		$longitude = $listing[0]["StandardFields"]["Longitude"];

		$dsl = modules::load('dsl/dsl');

		$endpoint = "listings/nearby?_lat=".$latitude."&_lon=".$longitude."&_expand=PrimaryPhoto";

		$body = array("access_token" => $this->session->userdata("access_token"),
					"bearer_token" => "asdf1234"
				);

		$result = $dsl->post_endpoint($endpoint, $body);

		$check_syncing = "sync-status/".$this->domain_user->id;

		do {

			$ch = $dsl->get_endpoint($check_syncing);
			$check = json_decode($ch, true);

		} while($check['nearby_status'] == true);

		$response = (!empty($result)) ? $response = array("success" => TRUE) : $response = array("success" => FALSE);

		exit(json_encode($response));
	}

	//{POST} /listings/new (reserve)
	public function sync_new_listings() {

		$dsl = modules::load('dsl/dsl');

		$endpoint = "listings/new";

		$body = array("access_token" => $this->session->userdata("access_token"),
					"bearer_token" => "asdf1234"
				);

		$result = $dsl->post_endpoint($endpoint, $body);

		$check_syncing = "sync-status/".$this->domain_user->id;

		do {

			$ch = $dsl->get_endpoint($check_syncing);
			$check = json_decode($ch, true);

		} while($check['newlistings_status'] == true);

		$response = (!empty($result)) ? $response = array("success" => TRUE) : $response = array("success" => FALSE);

		exit(json_encode($response));
	}

	//{POST} /sync-new-agent-listing (property_listing_data)
	public function sync_agent_listings() {

		$dsl = modules::load('dsl/dsl');

		$endpoint = "sync-new-agent-listings";

		$body = array("access_token" => $this->session->userdata("access_token"),
					"bearer_token" => "asdf1234"
				);

		$result = $dsl->post_endpoint($endpoint, $body);

		$check_syncing = "sync-status/".$this->domain_user->id;

		do {

			$ch = $dsl->get_endpoint($check_syncing);
			$check = json_decode($ch, true);

		} while($check['listings_status'] == true);

		$response = (!empty($result)) ? $response = array("success" => TRUE) : $response = array("success" => FALSE);

		exit(json_encode($response));

	}

	//http://138.68.10.202:3000/api/v1/sync-status/500
	public function check_dsl_sync() {

		$dsl = modules::load('dsl/dsl');

		$endpoint = "sync-status/528";//.$this->domain_user->id;

		do {
			$ch = $dsl->get_endpoint($endpoint);
			$check = json_decode($ch, true);
			printA($check);
		} while($check['listings_status'] == true);

	}


	public function update_sync_status() {

		$id = $this->input->post('id');
		$sync_status = $this->input->post('sync_status');

		$data = array('sync_status' => $sync_status);
		$success = $this->idx_model->update_sync_status($id, $data);

		$response = ($success) ? array("success" => TRUE) : $response = array("success" => FALSE);

		if($this->session->userdata('from_website')) {
			$this->session->unset_userdata('pricing');
			$this->session->unset_userdata('app_type');
			$this->session->unset_userdata('from_website');
		}

		exit(json_encode($response));
	}

	private function set_token() {
		if($this->session->userdata("access_token")) {
			$this->access_token = $this->session->userdata("access_token");
			$this->refresh_token = $this->session->userdata("refresh_token");
			return TRUE;
		}
		return FALSE;
	}

	public function agent_subscription($agent_id) {

		$ret = FALSE;

		if(isset($agent_id)) {
			$arr = array(
				'UserId' => $agent_id,
				'EventType' => 'purchased'
			);

			$info = $this->idx_model->fetch_agent_subscription($arr);

			foreach ($info as $key) {
				if($key->ApplicationName == 'Instant Agent IDX Website' || $key->ApplicationType == 'Agent_IDX_Website') {
					$ret = TRUE;
				}
			}
		}

		if($ret) {
			$this->session->set_userdata('agent_idx_website', TRUE);
		} else {
			$this->session->set_userdata('agent_idx_website', FALSE);
		}
	}

	public function save_spark_tokens($user_id, $agent_id, $access_token, $refresh_token) {
        $url = rtrim(getenv('DSL_SERVER_URL', '/')."/agent_idx_token");

    	$headers = array(
    		'Content-Type: application/json',
    		'X-Auth-Token: ERI9I4ku3vDyLKlOit7m_A7hbXiuOakCLgIuszmbGnw',
    		'X-User-Id: wfnbdL5z4Z7e6Ef6i'
    	);

    	$data = array(
    		'system_user_id'		=> (int)$user_id,
			'agent_id' 				=> $agent_id,
			'access_token' 			=> $access_token,
			'refresh_token' 		=> $refresh_token,
			'date_created' 			=> date("Y-m-d h:m:s"),
			'date_modified' 		=> date("Y-m-d h:m:s"),
			'expected_token_expired' => date("Y-m-d h:m:s", strtotime('+24 hours'))
		);

		$data_json = json_encode($data, true);

    	$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 0);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
		curl_setopt($ch, CURLOPT_URL, $url);

		$result = curl_exec($ch);
		$result_info = json_decode($result, true);
        curl_close($ch);

	}

	public function update_spark_token($user_id, $agent_id, $access_token, $refresh_token) {
        $url = rtrim(getenv('DSL_SERVER_URL', '/')."/agent_idx_token/".$agent_id."/update");

    	$headers = array(
    		'Content-Type: application/json',
    		'X-Auth-Token: ERI9I4ku3vDyLKlOit7m_A7hbXiuOakCLgIuszmbGnw',
    		'X-User-Id: wfnbdL5z4Z7e6Ef6i'
    	);

    	$data = array(
    		'system_user_id'		=> (int)$user_id,
    		'agent_id' 				=> $agent_id,
			'access_token' 			=> $access_token,
			'refresh_token' 		=> $refresh_token,
			'date_created' 			=> date("Y-m-d h:m:s"),
			'date_modified' 		=> date("Y-m-d h:m:s"),
			'expected_token_expired' => date("Y-m-d h:m:s", strtotime('+24 hours'))
		);

		$data_json = json_encode($data, true);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 0);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
		curl_setopt($ch, CURLOPT_URL, $url);

		$result  = curl_exec($ch);
		$result_info = json_decode($result, true);
        curl_close($ch);

	}

	public function test() {
		$session['access_token'] = "5wfwfprva5cj1b9je7r4uwhis";
		$session['refresh_token'] = "424ubhh3h1mfu27ydargyhric";
		$this->session->set_userdata($session);

		var_dump($this->session->all_userdata());
	}
}
