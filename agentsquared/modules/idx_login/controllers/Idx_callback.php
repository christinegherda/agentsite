<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Idx_callback extends MX_Controller {

	public function __construct() {
        parent::__construct();

        $this->load->model("idx_model");
	}

	public function index() {
		if(isset($_GET['openid_spark_code']) AND $_GET['openid_spark_code'] != '') { // We've been authorized by the end user.
		    
		    $tokens = $this->authorization($_GET['openid_spark_code']);

		    if($tokens) {

				$account_information = $this->myaccount($tokens);//get agent informations

				if($account_information) {

					$agent_id = $account_information["D"]["Results"][0]["Id"];
					
					//check if agentId exist in users_idx_token table
					$is_agent_exist = $this->idx_model->fetch_agent_token($agent_id);

					if($is_agent_exist) {//If agent already exist, update the token

						//update token info
						$token_info = array(	
							'access_token' => $tokens['access_token'],
							'refresh_token' => $tokens['refresh_token'],
							'date_modified' => date("Y-m-d h:m:s"),
							'expected_token_expired' => date("Y-m-d h:m:s", strtotime('+24 hours')),
							'is_valid' => 1,
							'counter' => 0,
						);

						$this->idx_model->update_token($agent_id, $token_info);
					}
					else {//Save the token

						//insert agent info
						$agent_info = array( 
							'agent_id' => $agent_id,
							'access_token' => $tokens['access_token'],
							'refresh_token' => $tokens['refresh_token'],
							'date_created' => date("Y-m-d h:m:s"),
							'date_modified' => date("Y-m-d h:m:s"),
							'expected_token_expired' => date("Y-m-d h:m:s", strtotime('+24 hours')),
							'is_valid' => 1,
							'counter' => 0,
						);

						$this->idx_model->save_token($agent_info);
					}

					//Set tokens as sessions
					$session['access_token'] = $tokens['access_token'];
					$session['refresh_token'] = $tokens['refresh_token'];
					$this->session->set_userdata($session);

					if($this->session->userdata("from_website")) {
						redirect(base_url()."idx_login?from=website&pricing=".$this->session->userdata('pricing')."&app_type=".$this->session->userdata('app_type'), "refresh");
					} else {
						redirect(base_url()."idx_login?from=spark", "refresh");
					}
				}

		    } else {
		      echo("We're sorry, but the Grant request failed!");
		    }
	    } elseif(isset($_GET['openid_mode']) && $_GET['openid_mode'] == 'error') {
	        echo "Please verify that \$GLOBALS['redirect_uri'] set in sparkle_functions.php matches exactly the redirect URI on record for your key.";
	    } else { 
	    	echo "Noob! Leave me in peace!";
	    }
	}

	public function authorization($code=NULL) {

		$result_info = FALSE;

		if($code) {

			$client_id = "d1e85y67j0cjb2axv04yhpe0i";
			$client_secret = "d3wdp13uu2uf45jokphqle0fk";
			$redirect_uri = "https://agentsquared.com/as-agent-site-v3/idx_login/idx_callback";

			$url = "https://sparkapi.com/v1/oauth2/grant";

			$post = array(
				'client_id' 	=> $client_id,
				'client_secret' => $client_secret,
				'grant_type'	=> 'authorization_code',
				'code'			=> $code,
				'redirect_uri' 	=> $redirect_uri,
			);

			$headers = ['X-SparkApi-User-Agent: PHP-APP-In-Fifteen-Minutes/1.0'];

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_TIMEOUT, 0);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
			curl_setopt($ch, CURLOPT_URL, $url);

			$result = curl_exec($ch);
            curl_close($ch);
			$result_info = json_decode($result, true);
		}

		return $result_info;
	}

	public function myaccount($tokens) {

		if($tokens) {
			$url = "https://sparkapi.com/v1/my/account";

			$headers = array(
				'Content-Type: application/json',
				'User-Agent: Spark API PHP Client/2.0',
				'X-SparkApi-User-Agent: PHP-APP-In-Fifteen-Minutes/1.0',
			    'Authorization: OAuth '. $tokens['access_token'],
			);

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_URL, $url);

			$result = curl_exec($ch);
			$result_info = json_decode($result, true);

			if(empty($result_info)) {
				$result_info = curl_strerror($ch);
			}

			return $result_info;
		}  
	}

}
