<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title><?php echo (isset($title)) ? $title : 'AgentSquared'; ?></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="<?= base_url()?>assets/bootstrap/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="<?= base_url()?>assets/css/sync/register.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <script src="<?= base_url()?>assets/js/sync/modernizr.custom.js"></script>
</head>
<body>
	<section class="content-wrapper section-loader">
		<div class="container">
			<div class="section-load" style="display:none;">
				<div class="as-brand">
					<img src="<?= base_url().'assets/images/logo-brand-no-shadow'?>">
				</div>
				<p>
					Please wait while we are preparing your data. <br>
					This might take a while...
				</p>
				<div class="text-center">
			    	<img src="<?= base_url().'assets/img/sync/squares.gif'?>">
			    </div>
			</div>

			<div id="sync_modal" class="modal fade" role="dialog">
				<div class="modal-dialog">
				    <!-- Modal content-->
				    <div class="modal-content">
				      	<div class="modal-header" style="background:#FEA42D;">
				        	<h2 class="modal-title" style="color:#FFFFFF; text-align:center;">Important!</h2>
				      	</div>
				      	<div class="modal-body" style="height:175px; padding: 45px;">
				        	<h4 class="text">
				        		It seems that the last time you sync your MLS Data was unsuccessful. <br><br/> Please Click the <span style="color:green">CONTINUE</span> button to sync it once again.</h4>
				      	</div>
				      	<div class="modal-footer">
				        	<button type="button" class="btn btn-danger no-radius" data-dismiss="modal" id="cancel_sync">CLOSE</button>
				        	<button type="button" class="btn btn-success no-radius" id="confirm_sync">CONTINUE</button>
				      	</div>
				    </div>
				</div>
			</div>
		</div>
	</section>
	<script src="<?= base_url()?>assets/js/jquery/jquery.min.js"></script>
    <script src="<?= base_url()?>assets/bootstrap/js/bootstrap.min.js"></script> 
    <script type="text/javascript">
	    $(document).ready(function() {

	        var window_height = $(window).height();
	        $(".content-wrapper").css('min-height', window_height);

	        $("#sync_modal").modal('show');

			$("#confirm_sync").on("click", function() {

				$("#sync_modal").modal('hide');
				$(".section-load").show('slow');

				var $base_url = "<?php echo base_url(); ?>";
		        var $user_id = "<?php echo $user_id; ?>";
				var $agent_web_type = "<?php echo $agent_idx_type; ?>";
				var formData = {'user_id' : $user_id};

				//Fetching metas for new agent
				$.ajax({
					method: "POST",
					url: $base_url+"idx_login/sync_new_agent",
					data: formData,
					dataType: 'json',
					beforesSend: function() {
						console.log("Fetching data");
					},
					success: function(data) {
						nearby_listings();
					}
				});

				//Fetching nearby listings
				function nearby_listings() {
					$.ajax({
						method: "POST",
						url: $base_url+"idx_login/sync_nearby_listings",
						data: formData,
						dataType: 'json',
						beforesSend: function() {
							console.log("Fetching Nearby Listings");
						},
						success: function(data) {
							new_listings();
						}
					});
				}

				//Fetch new listings
				function new_listings() {
					$.ajax({
						method: "POST",
						url: $base_url+"idx_login/sync_new_listings",
						data: formData,
						dataType: 'json',
						beforesSend: function() {
							console.log("Fetching New Listings");
						},
						success: function(data) {
							listings();
						}
					});
				}	

				//Fetch all agent's listings
				function listings() {
					$.ajax({
						method: "POST",
						url: $base_url+"idx_login/sync_agent_listings",
						data: formData,
						dataType: 'json',
						beforesSend: function() {
							console.log("Fetching Agent Listings");
						},
						success: function(data) {
							update_sync_status();
						}
					});
				}

				//Update Sync Status
				function update_sync_status() {
					var syncStatus = {'id' : $user_id, 'sync_status' : 1};
					$.ajax({
						method: "POST",
						url: $base_url+"idx_login/update_sync_status",
						data: syncStatus,
						dataType: 'json',
						beforeSend: function() {
							console.log("Updating Sync Status");
						},
						success: function(data) {
							console.log(data);
							if(data.success) {
								console.log("Successfully Synced!");
								if($agent_web_type == "TRUE") {
									window.location.href = $base_url+"dashboard";
			    				} else {
			    					window.location.href = $base_url+"agent_sites";
			    				}
							}
						}
					});
				}
			});
	    });
    </script>
</body>
</html>