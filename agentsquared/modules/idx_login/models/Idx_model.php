<?php 

if( ! defined('BASEPATH') ) exit('No direct script access allowed');

class Idx_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }  

    public function update_sync_status($id = NULL, $arr = array()) {

        $ret = false;

        if($id && $arr) {
            $this->db->where("id", $id);
            $this->db->update("users", $arr);
            $ret = true;
        }

        return $ret;
    }   

    public function readAgentID($agent_id) {

        $ret = FALSE;

        $this->db->select("*")
                ->from("users")
                ->where('agent_id', $agent_id);

        $query = $this->db->get()->row();

        if($query) {
            $ret = TRUE;
        }

        return $ret;
    }

    public function check_sync_status($agent_id = NULL) {

        $ret = false;

        if($agent_id) {
            $ret = $this->db->select("*")
                            ->from("users")
                            ->where("agent_id", $agent_id)->get()->row();
        }

        return $ret;
    }

    public function fetch_user($user_id) {

        $ret = FALSE;

        if($user_id) {

            $array = array('id' => $user_id);
            $this->db->where($array);
            $query = $this->db->get('users')->row();

            if(isset($query)) {

                $data[] = array(
                    'email' => $query->email,
                    'id' => $query->id,
                    'code' => $query->code,
                    'agent_id' => $query->agent_id,
                    'first_name' => $query->first_name,
                    'created_on' => $query->created_on
                );

                $ret = $data;
            } 
        }

        return $ret;
    }

    public function insert_agent_subscription($data) {
        $this->db->insert("agent_subscription", $data);
    }

    public function insert_spark_user($arr) {

        $user = array(
            'agent_id'      => $arr['id'],
            'ip_address'    => $arr['ip_address'],
            'email'         => $arr['email'],  
            'password'      => password_hash($arr['password'], PASSWORD_DEFAULT),
            'first_name'    => $arr['first_name'],
            'last_name'     => $arr['last_name'],
            'company'       => $arr['company'],
            'phone'         => $arr['phone'],
            'city'          => $arr['city'],
            'address'       => $arr['address'],
            'zip'           => $arr['zip'],
            'broker'        => $arr['broker'],
            'theme'         => $arr['theme'],
            'active'        => 1,
            'created_on'    => date('Y-m-d H:i:s'),
            'code'          => $arr["code"],
            'sync_status'   => 0,
            'pricing'       => $arr["pricing"],
            'trial'         => $arr["trial"],
            'date_signed'   => $arr["date_signed"],
        );

        $this->db->insert('users', $user);

        $user_id = $this->db->insert_id();

        $group["group_id"] = 4; 
        $group["user_id"] = $user_id;

        $this->db->insert('users_groups', $group);

        return $user_id;
    }
    
    public function fetch_agent_token($agent_id=NULL) {

        $ret = FALSE;

        if($agent_id) {

            $this->db->select("*")
                    ->from("users_idx_token")
                    ->where("agent_id", $agent_id);

            $query = $this->db->get()->row();

            if(!$query) {
                $this->db->select("*")
                        ->from("user_bearer_token")
                        ->where("agent_id", $agent_id);

                $query = $this->db->get()->row();
            }

            $ret = $query;
        }

        return $ret;
    }

    public function save_token($agent_info = array()) {
        $this->db->insert("users_idx_token", $agent_info);
        return ( $this->db->insert_id() ) ? TRUE : FALSE ;
    }

    public function update_token($agent_id, $token_info = array()) {
        $this->db->where("agent_id", $agent_id);
        $this->db->update("users_idx_token", $token_info);

        if( $this->db->affected_rows() > 0)
            return TRUE;
        else
            return TRUE;
    }

    public function fetch_all_token() {

        $this->db->select("users_idx_token.*, users.id as user_id")
                ->from("users_idx_token")
                ->join("users", "users.agent_id  = users_idx_token.agent_id", "left")
                ->where("users.id !=", 0)
                ->where("users.id !=", NULL);

        return $this->db->get()->result();
    }

    public function update_refresh_token($id = NULL, $token_info = array()) {

        if($id) {
            $this->db->where("id", $id);
            $this->db->update("users_idx_token", $token_info);

            if($this->db->affected_rows() > 0)
                return TRUE;
            else 
                return TRUE;
        }
    }

    public function fetch_agent_subscription($agent = array()) {

        $ret = FALSE;

        if(isset($agent)) {
            $this->db->select("*")
                    ->from("agent_subscription")
                    ->where($agent);

            $ret = $this->db->get()->result();
        }

        return $ret;
    }

    public function fetch_agent_user_id($agent_id) {
        $ret = FALSE;

        if($agent_id) {
            $this->db->select("id")->from("users")->where("agent_id", $agent_id);
            $id = $this->db->get()->row();

            if($id)
                $ret = $id->id;
        }

        return $ret;
    }

    public function save_bearer_token($data=array()) {

        $ret=FALSE;

        if($data) {
            $this->db->insert("user_bearer_token", $data);
        }

        return $ret;
    }
}