<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class As_ga extends MX_Controller {
  
  function __construct(){
    parent::__construct();
    
    $this->load->model('as_ga/as_ga_model', 'as_ga');
    
  }
  
  public function getTrackingCode(){
    
		if($this->as_ga->checkTable()){
      
			$profile = $this->getClientData();
      
      if($profile){
        if($profile->is_enabled){
          $data['profile'] = $profile; // for debugging purposes
          $data['property_id'] = $profile->property_id;
          $code = '';
          
          //$data['client_ga'] = false;
          if(isset($profile->client_tracking_code) && $profile->client_tracking_code != null){
            $code .= "<!-- Agent Google Analytics tracking code -->\n";
            $code .= $profile->client_tracking_code;
            $code .= "\n";
            $code .= "<!-- ./ Agent Google Analytics tracking code -->\n";
          }
          
          $code .= "<!-- Agent Google Analytics tracking code -->\n";
          $code .= "<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');\n";
          
          if(isset($profile->client_property_id) && $profile->client_property_id != null){
            $code .= "    ga('create', '" . $profile->client_property_id ."', 'auto');\n";
          }
          $code .= "    ga('create', '" . $profile->property_id ."', 'auto', {'name': 'astracker'});\n";
          if(isset($profile->client_property_id) && $profile->client_property_id != null){
            $code .= "    ga('send', 'pageview');\n";
          }
          $code .= "    ga('astracker.send', 'pageview');\n";
          $code .= "  </script>\n";
          $code .= "<!-- ./ Agent Google Analytics tracking code -->\n";
          
          return $code;
        }
      }
		}
    return '';
	}
  
  private function getClientData(){
    //$domain = $_SERVER['SERVER_NAME'];
    $domain_user = domain_to_user();
		//$query = $this->as_ga_model->get($domain_user->agent_id, $domain);
		$query = $this->as_ga->get($domain_user->agent_id);
		//$query = $this->db->get_where('users_ga', array('agent_id' => $domain_user->agent_id));
		if($query){
			return $query;
		}
		return false;
	}
}
