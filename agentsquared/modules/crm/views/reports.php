<?php 
    $this->load->view('session/header'); 
    $this->load->view('session/top-nav');
    // $this->load->view('session/left-nav');
?>
  <div class="content-wrapper">
  <div class="page-title">
    <h3 class="heading-title">
      <span> Reports</span>
    </h3>
    <p>Analytics on your site's activity</p>
  </div>
    <section class="content">
        <div class="row">
            <div class="reports-content">
                <div class="col-md-12">
                    <div class="col-md-8 col-xs-6 no-padding-left">
                        <h4>Properties Sold</h4>
                    </div>
                    <div class="col-md-4 col-xs-6 no-padding-right">
                        <select name="" id="" class="form-control">
                            <option value="">January</option>
                            <option value="">February</option>
                            <option value="">March</option>
                            <option value="">April</option>
                            <option value="">May</option>
                            <option value="">June</option>
                            <option value="">July</option>
                            <option value="">August</option>
                            <option value="">September</option>
                            <option value="">October</option>
                            <option value="">November</option>
                            <option value="">December</option>
                        </select>    
                    </div>
                    <table class="table table-responsive table-striped">
                        <thead>
                            <tr>
                                <th>Property Name</th>
                                <th>Property Type</th>
                                <th>Property Sites</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if( !empty($property_sold) ) :
                                foreach ($property_sold as $key ) :
                            ?>
                                    <tr>
                                        <td><?=$key->unparsedaddr?></td>
                                        <td><?=$key->property_type?></td>
                                        <td><a href="<?php echo $key->site_url?>" target="_blank">View</td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php else : ?>
                                <tr class="center"><td coslpan="10"><i>no records found!</i></td><td></td><td></td></tr>
                            <?php endif;?>
                            
                        </tbody>
                    </table>
                </div>
                <!-- <div class="col-md-6">
                    <h4>2016 Properties Sold per Month</h4>
                    <hr>
                    <div id="salesummary" style=""></div>
                </div> -->
                <div class="col-md-6">
                    <div class="col-md-8 col-xs-6 no-padding-left">
                        <h4>Favorites per Property</h4>
                    </div>
                    <div class="col-md-4 col-xs-6 no-padding-right">
                        <select name="" id="" class="form-control">
                            <option value="">January</option>
                            <option value="">February</option>
                            <option value="">March</option>
                            <option value="">April</option>
                            <option value="">May</option>
                            <option value="">June</option>
                            <option value="">July</option>
                            <option value="">August</option>
                            <option value="">September</option>
                            <option value="">October</option>
                            <option value="">November</option>
                            <option value="">December</option>
                        </select>    
                    </div>
                    <table class="table table-responsive table-striped">
                        <thead>
                            <tr>
                                <th>Property Name</th>
                                <th>No. of Favorites</th>
                                <th>Date</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if( !empty($favorates_properties) ) : 
                                    foreach ($favorates_properties as $property) :
                            ?>
                                        <tr>
                                            <td><?php echo $property["properties_details"]["StandardFields"]["UnparsedFirstLineAddress"]?></td>
                                            <td><?php echo ($property['counter'] > 0) ? $property['counter'] : 0?></td>
                                            <td><?php echo date("F d, Y", strtotime($property['date_created'])) ; ?></td>
                                        </tr>
                                     <?php endforeach; ?>
                            <?php else : ?>
                                <tr class="center"> <td coslpan="4"><i>no records found!</i></td><td></td><td></td> </tr>
                            <?php endif;?>
                            
                        </tbody>
                    </table>
                </div>
                <div class="col-md-6">
                    <div class="col-md-8 col-xs-6 no-padding-left">
                        <h4>Views per Property</h4>
                    </div>
                    <div class="col-md-4 col-xs-6 no-padding-right">
                        <select name="" id="" class="form-control">
                            <option value="">January</option>
                            <option value="">February</option>
                            <option value="">March</option>
                            <option value="">April</option>
                            <option value="">May</option>
                            <option value="">June</option>
                            <option value="">July</option>
                            <option value="">August</option>
                            <option value="">September</option>
                            <option value="">October</option>
                            <option value="">November</option>
                            <option value="">December</option>
                        </select>    
                    </div>
                    <table class="table table-responsive table-striped">
                        <thead>
                            <tr>
                                <th>Property City</th>
                                <th>No. of Views</th>
                                <th>Date</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if( !empty($search_properties) ) :  
                                    foreach ($search_properties as $property) : 
                            ?>
                                        <tr>
                                            <?php 
                                                $city = str_replace("&type", "", $property['details'] ) ;
                                                $city = str_replace("&PropertyClass", "", $city ) ;
                                            ?>
                                            <td><?php echo $city; ?></td>
                                            <td><?php echo ($property['counter'] > 0) ? $property['counter'] : 0?></td>
                                            <td><?php echo date("F d, Y", strtotime($property['date_created'])) ; ?></td>
                                        </tr>
                                     <?php endforeach; ?>
                            <?php else : ?>
                                <tr class="center"> <td coslpan="4"><i>no records found!</i></td><td></td><td></td> </tr>
                            <?php endif;?>
                            
                        </tbody>
                    </table>
                </div>
                
            </div>
        </div>
    </section>
  </div>
<?php $this->load->view('session/footer'); ?>