<?php 
    $this->load->view('session/header'); 
    $this->load->view('session/top-nav');
?>
  <!-- Full Width Column -->
  <div class="content-wrapper profile-content">
<!--     <div class="page-title">
      <h3 class="heading-title">
        <span> Messages</span>
      </h3>
      <p>You've got mail!</p>
    </div> -->
      
      <h3 class="heading-title">
        <span>Contact Details</span>
      </h3>
      
      <!-- Main content -->
      <section class="content">
        <div class="row">
          <div class="col-md-12">                       
            <?php $session = $this->session->flashdata('session');                            
              if( isset($session) )
              {
            ?>
              <div class="alert-flash alert <?php echo ($session["status"]) ? "alert-success" : "alert-danger" ?>" role="alert"><?php echo $session["message"]; ?></div>  
            <?php } ?>
          </div>

          <div class="col-md-6">
            <div class="col-md-12">
              <h1 class="col-md-5 text-center pr-30 profile-name"><?php echo $profile->first_name." ".$profile->last_name ?></h1>
            </div>
            <form action="<?php echo site_url("crm/leads/update_profile/".$profile->contact_id); ?>" method="post" id="sbt-update-profile" class="form-horizontal">
                <div class="form-group">
                  <label for="" class="col-md-3 text-right">Email Address: </label>
                  <div class="col-md-6">
                    <input type="email" name="profile[email]"  class="form-control" value="<?=(isset($profile->email)) ? $profile->email : ''; ?>" id="email" placeholder="Email">
                  </div>
                  <div class="col-md-3 invisible-xs">
                     <button type="submit" class="btn btn-green pull-right">Update Profile</button>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-md-3 text-right">Phone: </label>
                  <div class="col-md-4">
                    <input type="text" name="profile[phone]" class="form-control" value="<?=(isset($profile->phone)) ? $profile->phone : ''; ?>" id="phone" placeholder="Phone">
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-md-3 text-right">Mobile: </label>
                  <div class="col-md-4">
                    <input type="text" name="profile[mobile]" class="form-control" value="<?=(isset($profile->mobile)) ? $profile->mobile : ''; ?>" id="mobile" placeholder="Mobile">
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-md-3 text-right">Address: </label>
                  <div class="col-md-7">
                    <input type="text" name="profile[address]" class="form-control" value="<?=(isset($profile->address)) ? $profile->address : ''; ?>" id="address" placeholder="Address">
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-md-3 text-right">City: </label>
                  <div class="col-md-3">
                    <input type="text" name="profile[city]" class="form-control" value="<?=(isset($profile->city)) ? $profile->city : ''; ?>" id="City" placeholder="City">
                  </div>
                  <label for="" class="col-md-1 text-right">State: </label>
                  <div class="col-md-2">
                    <input type="text" name="profile[state]" class="form-control" value="<?=(isset($profile->state)) ? $profile->state : ''; ?>" id="State" placeholder="State">
                  </div>
                  <label for="" class="col-md-1 text-right">Zip Code: </label>
                  <div class="col-md-2">
                    <input type="text" name="profile[zipcode]" class="form-control" value="<?=(isset($profile->zipcode)) ? $profile->zipcode : ''; ?>" id="zipcode" placeholder="Zip Code">
                  </div>
                </div>
                <div class="form-group visible-xs">
                  <div class="col-md-3">
                     <button type="submit" class="btn btn-green pull-right">Update Profile</button>
                  </div>
                </div>

            </form>
            
            <div class="mb-30px">
              <!-- Mail -->
              <h3>Mail</h3>
              <div class="table-responsive">
                <table class="table table-striped">
                  <thead>
                    <tr>
                      <th>Message</th>
                      <th>Date</th>
                      <th>Sent By</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php if(!empty($profile->messages)) : ?>
                      <?php foreach($profile->messages as $message) : ?>
                        <tr>
                          <td><a href="javascript:void(0)" data-message-id="<?php echo $message->id; ?>" class="email-view" title="view all"> <?php //echo substr($message->message ,0,60); ?> View Email</a></td>
                          <td><?= date( "Y-m-d" ,strtotime($message->created));?></td>
                          <td><?= (($message->type == 1) ? $message->agent_name : (!empty($message->contact_name) ? $message->contact_name : $profile->first_name." ".$profile->last_name)); ?></td>
                        </tr>
                      <?php endforeach; ?>

                    <?php else : ?>
                      <tr class="center"> <td colspan="8"> <i> No records found!</i> </td> </tr>
                    <?php endif; ?>
                    
                  </tbody>

                </table>
                
              </div>
              <div class="">
                  <button type="submit" class="btn btn-green pull-right" data-toggle="modal" data-target="#SendEmail" >Send Email</button>
                </div>
            </div>

            
            <!-- Activities -->
            <div class="activities-list">

              <!-- Nav tabs -->
              <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#activities" aria-controls="activities" role="tab" data-toggle="tab">All Activities</a></li>
                <li role="presentation"><a href="#email-listing" aria-controls="email-listing" role="tab" data-toggle="tab">Email</a></li>
                <li role="presentation"><a href="#phonenumber" aria-controls="phonenumber" role="tab" data-toggle="tab">Phone</a></li>
                <li role="presentation"><a href="#meetings" aria-controls="meetings" role="tab" data-toggle="tab">Meetings</a></li>
                <li role="presentation"><a href="#notes" aria-controls="notes" role="tab" data-toggle="tab">Notes</a></li>
                <li role="presentation"><a href="#tasks" aria-controls="tasks" role="tab" data-toggle="tab">Tasks</a></li>
              </ul>

              <!-- Tab panes -->
              <div class="tab-content">

                <!-- All activities -->
                <div role="tabpanel" class="tab-pane active" id="activities">
                  <div class="table-responsive">
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th width="25%">Action</th>
                          <th width="50%">Details</th>
                          <th>Date</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php if( !empty($activities) ) : ?>
                            <?php foreach ($activities as $activity) : 
                              
                              if( $activity->activity_type == "send email"){
                                $label = '<span class="label-alert label-green"><i class="fa fa-envelope"></i> Emailed</span>';
                              }
                              else if( $activity->activity_type == "phone" )
                              {
                                $label = '<span class="label-alert label-yellow"><i class="fa fa-phone"></i> Left Voicemail</span>';
                              }
                              else if( $activity->activity_type == "meetings" )
                              {
                                $label = '<span class="label-alert label-yellow"><i class="fa fa-phone"></i> Left Voicemail</span>';
                              }
                              else
                              {
                                $label = '<span class="label-alert label-orange"><i class="fa fa-users"></i> '.ucwords($activity->activity_type).'</span>';
                              }

                            ?>
                              <tr>
                                <td width="20%">
                                  <p class="activity-name"><?php echo ($activity->user_type == "agent") ? "You" : "Customer"?></p>
                                  <?php echo $label; ?>
                                  <p class="lead-label">Lead:</p>
                                  <p class="activity-name"><?php echo $profile->first_name." ".$profile->last_name ?></p>
                                </td>
                                <td width="50%">
                                  <?php echo $activity->activity; ?>
                                </td>
                                <td class="date-column">
                                  <p><?php echo date("l", strtotime($activity->created)); ?></p>
                                  <p><?php echo date("F d, Y", strtotime($activity->created)); ?> </p>
                                  <p><?php echo date("H i a", strtotime($activity->created)); ?></p>
                                </td>
                              </tr>
                            <?php endforeach; ?>
                        <?php else : ?>
                          <tr class="center"><td colspan="8"><i>No records found!</i></td></tr>
                        <?php endif; ?> 

                      </tbody>
                    </table>
                  </div>
                </div>

                <!-- Email -->
                <div role="tabpanel" class="tab-pane" id="email-listing">
                  <div class="table-responsive">
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th width="25%">Action</th>
                          <th width="50%">Details</th>
                          <th>Date</th>
                        </tr>
                      </thead>
                      <tbody>
                         <?php if( !empty($activities) ) : ?>
                            <?php foreach ($activities as $activity) : 
                              
                              if( $activity->activity_type == "send email"){
                                $label = '<span class="label-alert label-green"><i class="fa fa-envelope"></i> Emailed</span>';                              
                            ?>
                              <tr>
                                <td width="20%">
                                  <p class="activity-name"><?php echo ($activity->user_type == "agent") ? "You" : "Customer"?></p>
                                  <?php echo $label; ?>
                                  <p class="lead-label">Lead:</p>
                                  <p class="activity-name"><?php echo $profile->first_name." ".$profile->last_name ?></p>
                                </td>
                                <td width="50%">
                                  <?php echo $activity->activity; ?>
                                </td>
                                <td class="date-column">
                                  <p><?php echo date("l", strtotime($activity->created)); ?></p>
                                  <p><?php echo date("F d, Y", strtotime($activity->created)); ?> </p>
                                  <p><?php echo date("H i a", strtotime($activity->created)); ?></p>
                                </td>
                              </tr>
                              <?php } ?>
                            <?php endforeach; ?>
                        <?php else : ?>
                          <tr class="center"><td colspan="8"><i>No records found!</i></td></tr>
                        <?php endif; ?> 

                      </tbody>
                    </table>
                  </div>
                </div>
                
                <!-- Phone -->
                <div role="tabpanel" class="tab-pane" id="phonenumber">
                  <div class="table-responsive">
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th width="25%">Action</th>
                          <th width="50%">Details</th>
                          <th>Date</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php if( !empty($activities) ) : ?>
                            <?php foreach ($activities as $activity) : 
                              
                              if( $activity->activity_type == "phone"){
                                $label = '<span class="label-alert label-yellow"><i class="fa fa-phone"></i> Left Voicemail</span>';                              
                            ?>
                              <tr>
                                <td width="20%">
                                  <p class="activity-name"><?php echo ($activity->user_type == "agent") ? "You" : "Customer"?></p>
                                  <?php echo $label; ?>
                                  <p class="lead-label">Lead:</p>
                                  <p class="activity-name"><?php echo $profile->first_name." ".$profile->last_name ?></p>
                                </td>
                                <td width="50%">
                                  <?php echo $activity->activity; ?>
                                </td>
                                <td class="date-column">
                                  <p><?php echo date("l", strtotime($activity->created)); ?></p>
                                  <p><?php echo date("F d, Y", strtotime($activity->created)); ?> </p>
                                  <p><?php echo date("H i a", strtotime($activity->created)); ?></p>
                                </td>
                              </tr>
                              <?php } ?>
                            <?php endforeach; ?>
                        <?php else : ?>
                          <tr class="center"><td colspan="8"><i>No records found!</i></td></tr>
                        <?php endif; ?> 
                        
                      </tbody>
                    </table>
                  </div>
                </div>

                <!-- Meetings -->
                <div role="tabpanel" class="tab-pane" id="meetings">
                  <div class="table-responsive">
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th width="25%">Action</th>
                          <th width="50%">Details</th>
                          <th>Date</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php if( !empty($activities) ) : ?>
                            <?php foreach ($activities as $activity) : 
                              
                              if( $activity->activity_type == "phone"){
                                $label = '<span class="label-alert label-orange"><i class="fa fa-users"></i> Meetings</span>';                              
                            ?>
                              <tr>
                                <td width="20%">
                                  <p class="activity-name"><?php echo ($activity->user_type == "agent") ? "You" : "Customer"?></p>
                                  <?php echo $label; ?>
                                  <p class="lead-label">Lead:</p>
                                  <p class="activity-name"><?php echo $profile->first_name." ".$profile->last_name ?></p>
                                </td>
                                <td width="50%">
                                  <?php echo $activity->activity; ?>
                                </td>
                                <td class="date-column">
                                  <p><?php echo date("l", strtotime($activity->created)); ?></p>
                                  <p><?php echo date("F d, Y", strtotime($activity->created)); ?> </p>
                                  <p><?php echo date("H i a", strtotime($activity->created)); ?></p>
                                </td>
                              </tr>
                              <?php } ?>
                            <?php endforeach; ?>
                        <?php else : ?>
                          <tr class="center"><td colspan="8"><i>No records found!</i></td></tr>
                        <?php endif; ?> 
                      </tbody>
                    </table>
                  </div>
                </div>

                <!-- Notes -->
                <div role="tabpanel" class="tab-pane" id="notes">
                  <div class="table-responsive">

                    <button type="button" class="btn btn-default pull-right" data-toggle="modal" data-target="#addNotes"  style="margin:5px" > Notes <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> </button>

                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th width="50%">Notes</th>
                          <th>Created By</th>
                          <th>Date Created</th>
                          <th width="10%">Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php if( !empty($profile->notes) ) : ?>
                          <?php foreach( $profile->notes as $note ) : ?>
                              <tr id="notesRow<?=$note->id;?>" >
                                <td width="50%">
                                  <?php echo $note->notes; ?>
                                </td>
                                <td>
                                  <?php echo $note->created_by; ?>
                                </td>
                                <td >
                                  <p><?php echo date("Y-m-d H:i:s", strtotime($note->date_created)); ?></p>
                                </td>
                                <td width="10%">
                                  <a href="<?php echo site_url('crm/leads/delete_notes/'.$note->id); ?>" data-id="<?=$note->id;?>" class="delete_notes"><span class="glyphicon glyphicon-remove" title="Delete Notes" aria-hidden="true"></span></a>
                                </td>
                              </tr>
                             
                            <?php endforeach; ?>
                        <?php else : ?>
                          <tr class="center"><td colspan="8"><i>No records found!</i></td></tr>
                        <?php endif; ?> 
                      </tbody>
                    </table>
                  </div>
                </div>

                <!-- Tasks -->
                <div role="tabpanel" class="tab-pane" id="tasks">
                  <div class="table-responsive">
                    <button type="button" class="btn btn-default pull-right" data-toggle="modal" data-target="#addTasks"  style="margin:5px" > Tasks <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> </button>
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th width="50%">Tasks</th>
                          <th >Created By</th>
                          <th>Date Created</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php if( !empty($profile->tasks) ) : ?>
                          <?php foreach( $profile->tasks as $task ) : ?>
                              <tr id="taskRow<?=$task->id;?>" >
                                <td width="50%">
                                  <?php echo $task->tasks; ?>
                                </td>
                                <td>
                                  <?php echo $task->created_by; ?>
                                </td>
                                <td >
                                  <p><?php echo date("Y-m-d H:i:s", strtotime($task->date_created)); ?></p>
                                </td>
                                <td width="10%">
                                  <a href="<?php echo site_url('crm/leads/delete_tasks/'.$task->id); ?>" data-id="<?=$task->id;?>" class="delete_tasks"><span class="glyphicon glyphicon-remove" title="Delete Tasks" aria-hidden="true"></span></a>
                                </td>
                              </tr>
                              
                            <?php endforeach; ?>
                        <?php else : ?>
                          <tr class="center"><td colspan="8"><i>No records found!</i></td></tr>
                        <?php endif; ?> 
                      </tbody>
                    </table>
                  </div>
                </div>


              </div>
            </div>
          </div>
          <div class="col-md-6">

            <!-- Custom Tabs -->
            <div class="save-property-listing">
              
              <!-- Nav tabs -->
              <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#favorites" aria-controls="favorites" role="tab" data-toggle="tab">Favorites</a></li>
                <li role="presentation"><a href="#searches" aria-controls="searches" role="tab" data-toggle="tab">Saved Searches</a></li>
                <li role="presentation"><a href="#scheduleforshowing" aria-controls="scheduleforshowing" role="tab" data-toggle="tab">Schedule for Showing</a></li>
              </ul>

              <!-- Tab panes -->
              <div class="tab-content">
                <!-- Favorites -->
                <div role="tabpanel" class="tab-pane active" id="favorites">
                  <div class="table-responsive">
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th>Image</th>
                          <th>ListingID</th>
                          <th>Date</th>
                          <th>Address</th>
                          <th>Details</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php if( !empty($profile->favourites) ) : ?>
                          <?php foreach( $profile->favourites as $favourite ) : ?>
                            <tr>
                              <td class="table-img"><a href="#"><img src="<?= base_url()?>/assets/images/dashboard/property4.jpg" alt=""></a></td>
                              <td><?= $favourite->properties_details["StandardFields"]["ListingId"]; ?></td>
                              <td><?= date("Y-m-d", strtotime($favourite->date_created)); ?></td>
                              <td><?= $favourite->properties_details["StandardFields"]["UnparsedFirstLineAddress"]; ?></td>
                              <td><p><?= $favourite->properties_details["StandardFields"]["UnparsedAddress"]; ?></p></td>
                            </tr>
                          <?php endforeach;?>

                        <?php else : ?>
                          <tr class="center"><td colspan="8"><i>No records found!</i></td></tr>
                        <?php endif; ?> 
                        
                      </tbody>
                    </table>
                  </div>
                </div>

                <!-- Searches -->
                <div role="tabpanel" class="tab-pane" id="searches">
                  <div class="table-responsive">
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th>Link</th>
                          <th>Details</th>
                          <th>Created</th>
                          <th>Email Frequency</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php if( !empty($profile->saveSearches) ) : ?>
                          <?php foreach( $profile->saveSearches as $savedSearches ) : ?>
                            <?php if( !isset($savedSearches->json_decoded) ) { ?>
                              <tr>
                                <td> <a href="<?php echo base_url()."home/listings?".$savedSearches->url;?>" target="_blank">Run this search >></a> </td>
                                <td><?=substr($savedSearches->url,0,50);?>...</td>
                                <td><?= date("Y-m-d", strtotime($savedSearches->date_created)); ?></td>
                                <td><?=ucfirst($profile->email_schedule);?></td>
                              </tr>
                            <?php }else{ ?>
                              <tr>
                                <td> <a href="<?php echo base_url()."home/popular_searches/".$savedSearches->json_decoded->Id;?>" target="_blank">Run this search >></a> </td>
                                <td><?=substr($savedSearches->json_decoded->Name,0,60);?></td>
                                <td><?= date("Y-m-d", strtotime($savedSearches->date_modified)); ?></td>
                                <td><?= ucfirst($profile->email_schedule); ?></td>
                              </tr>
                            <?php } ?>
                          <?php endforeach;?>

                        <?php else : ?>
                          <tr class="center"><td colspan="8"><i>No records found!</i></td></tr>
                        <?php endif; ?> 
                        
                      </tbody>
                    </table>
                  </div>
                </div>

                <!-- Schedule for showing -->
                <div role="tabpanel" class="tab-pane" id="scheduleforshowing">
                  <div class="table-responsive">
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th>Name</th>
                          <th>Email</th>
                          <th>Schedule Date</th>
                          <th>Notes</th>
                          <th>Date Created</th>
                          <th>Property</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php if( !empty($profile->schedule_showing) ) : ?>
                          <?php foreach( $profile->schedule_showing as $schedule ) : ?>
                            <tr>
                              <td> <a href="<?php echo base_url()."other-property-details/".$schedule->property_id;?>" target="_blank"> <?=$schedule->properties_details["StandardFields"]["UnparsedFirstLineAddress"]?> </a> </td>
                              <td><?=$profile->email;?></td>
                              <td><?=date("Y-m-d", strtotime($schedule->date_scheduled))?></td>
                              <td><?=$schedule->message;?></td>
                              <td><?=date("Y-m-d", strtotime($schedule->created))?></td>
                              <td><?=$schedule->properties_details["StandardFields"]["UnparsedAddress"]?></td>
                            </tr>
                          <?php endforeach;?>
                        <?php else : ?>
                          <tr class="center"><td colspan="8"><i>No records found!</i></td></tr>
                        <?php endif; ?>                       
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <?php if($profile->type=="seller" || $profile->type=="buyer") { ?>
            <div class="col-md-6">
              <h3>Customer Type: <strong><?=ucfirst($profile->type);?></strong></h3>
              <h3 style="text-align:center;">Home Information</h3>
              <div class="table-responsive">
                <table class="table table-striped">
                  <thead>
                    <tr>
                    <?php if($profile->type=="seller") { ?>
                      <th>Address</th>
                      <th>Zip Code</th>
                      <th># of Bedrooms</th>
                      <th># of Bathrooms</th>
                      <th>Square Ft.</th>
                    <?php } else { ?>
                      <th># of Bedrooms</th>
                      <th># of Bathrooms</th>
                      <th>Squaret Ft.</th>
                      <th>Contact You By</th>
                      <th>Price Range</th>
                      <th>When do you want to move?</th>
                      <th>When did you start looking?</th>
                      <th>Where would you like to own?</th>
                      <th>Are you currently with an agent?</th>
                      <th>Describe your dream home</th>
                    <?php } ?>
                    </tr>
                  </thead>
                  <tbody>
                    <?php 
                      if(isset($customer) && $customer) {
                        if($profile->type=="seller") { ?>
                        <tr>
                          <td><?=$customer->address;?></td>
                          <td><?=$customer->zip_code;?></td>
                          <td><?=$customer->bedrooms;?></td>
                          <td><?=$customer->bathrooms;?></td>
                          <td><?=$customer->square_feet;?></td>
                        </tr>
                    <?php
                        } else { ?>
                        <tr>
                          <td><?=$customer->bedrooms;?></td>
                          <td><?=$customer->bathrooms;?></td>
                          <td><?=$customer->square_feet;?></td>
                          <td><?=$customer->contact_by;?></td>
                          <td><?=$customer->price_range;?></td>
                          <td><?=$customer->when_do_you_want_to_move;?></td>
                          <td><?=$customer->when_did_you_start_looking;?></td>
                          <td><?=$customer->wher_would_you_like_to_own;?></td>
                          <td><?=($customer->are_you_currently_with_an_agent) ? 'Yes' : 'No';?></td>
                          <td><?=$customer->describe_your_dream_home;?></td>
                        </tr>
                    <?php 
                        }
                      }
                    ?>
                  </tbody>
                </table>
              </div>
            </div>
          <?php } ?>
        </div>
      </section>
  </div>

<div class="modal fade bs-example-modal-lg" id="SendEmail" data-width="500px" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" data-backdrop="static">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Send Email</h4>
      </div>
      <div class="modal-body">
        <form action="<?php echo site_url("crm/leads/send_email/".$contact_id); ?>" method="POST" id="sbt-send-email" class="form-horizontal">
            <textarea name="message" class="form-control summernote" rows="15"></textarea>       
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary" id="btn-send" >Send</button>
      </div>

      </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade bs-example-modal-lg" id="SentEmailView" data-width="500px" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" data-backdrop="static">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Sent Email Content</h4>
      </div>
      <div class="modal-body">
        <span id="sent-email-content"></span>     
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>

    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<div class="modal fade bs-example-modal-lg" id="addNotes" data-width="500px" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" data-backdrop="static">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Add Notes</h4>
      </div>
      <div class="modal-body">
        <form action="<?php echo site_url("crm/leads/add_notes/".$contact_id); ?>" method="POST" id="sbt-add-notes" class="form-horizontal">
            <textarea name="notes" class="form-control summernote" rows="15"></textarea>       
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary" id="btn-notes" >Submit</button>
      </div>

      </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade bs-example-modal-lg" id="addTasks" data-width="500px" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" data-backdrop="static">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Add Tasks</h4>
      </div>
      <div class="modal-body">
        <form action="<?php echo site_url("crm/leads/add_tasks/".$contact_id); ?>" method="POST" id="sbt-add-tasks" class="form-horizontal">
            <textarea name="tasks" class="form-control summernote" rows="15"></textarea>       
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary" id="btn-tasks" >Submit</button>
      </div>

      </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


  <!-- /.content-wrapper -->
<?php $this->load->view('session/footer'); ?>
<script>
    $(document).ready(function(){
        $('.summernote').summernote({
            height: 400,
            // placeholder: 'Write content here...'
        });
    });
</script>