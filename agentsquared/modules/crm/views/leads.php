 <?php 
    $this->load->view('session/header'); 
    $this->load->view('session/top-nav');
?>
<style type="text/css">
  /* tables */
 


</style>

  <!-- Full Width Column -->
  <div class="content-wrapper lead-list">

      <div class="page-title">
        <h3 class="heading-title">
          <span>Leads</span>
        </h3>
      </div>

      <!-- Main content -->
      <section class="content">

            <div class="row mb-30px ">
              <div class="col-md-4 no-padding-right">
                <h4 class="m-0" style="display:inline-block;">Capture Leads: <a href="#" data-toggle="tooltip" title="enable sign up form when viewing property"><i class="fa fa-question-circle" aria-hidden="true"></i></a> <input type="checkbox" name="my-checkbox" data-url="<?php echo base_url(); ?>crm/leads/set_lead_capture"></h4>
                <h4 class="m-0 pull-right" style="display:inline-block;">Email Marketing: <a href="#" data-toggle="tooltip" title="Send new listing update to customers based on their saved search"><i class="fa fa-question-circle" aria-hidden="true"></i></a> <input type="checkbox" name="email_marketing" data-url="<?php echo base_url(); ?>crm/leads/set_email_market"></h4>
              </div>
              <div class="col-md-3 no-padding-left text-center">
                  <!-- <a href="<?php echo site_url('crm/leads/import_contacts'); ?>" type="button" class="btn btn-success" id="import_contacts">Import Contacts</a>  -->  
                  <a href="<?php echo site_url('crm/leads/export_contacts'); ?>" type="button" class="btn btn-success">Export Contacts</a>
              </div>
              <div class="col-md-5 text-right">
                <form class="search-form form-inline" >
                  <div class="form-group">
                    <select id="type"  name="type" class="form-control">
                        <option value="">Type</option>
                        <option value="question" <?php echo (isset($_GET["type"]) && $_GET["type"] == "question" ) ? 'selected="selected"' : "" ?>>Question</option>
                        <option value="signup_form" <?php echo (isset($_GET["type"]) && $_GET["type"] == "sign_up" ) ? 'selected="selected"' : "" ?>>Signup Form</option>
                        <option value="buyer" <?php echo (isset($_GET["type"]) && $_GET["type"] == "buyer" ) ? 'selected="selected"' : "" ?> >Buyer</option>
                        <option value="seller" <?php echo (isset($_GET["type"]) && $_GET["type"] == "seller" ) ? 'selected="selected"' : "" ?>>Seller</option>
                        <option value="flex" <?php echo (isset($_GET["type"]) && $_GET["type"] == "flex" ) ? 'selected="selected"' : "" ?>>Flex</option>
                    </select>
                   </div>
                   <div class="form-group">
                    <div class="input-group input-group-md" style="width: 150px;">
                      <input type="text" name="keywords" value="<?php echo (isset($_GET["keywords"]) && !empty($_GET["keywords"])) ? $_GET["keywords"] : "" ?>" class="form-control pull-right" placeholder="Search">
                      <div class="input-group-btn">
                        <button type="submit" class="btn btn-default btn-search-submit"><i class="fa fa-search"></i></button>
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>

            <?php if( empty($count_contacts) ) { ?>
              <div class="row mb-30px">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <h4 id="saved_search_tagline">You haven't saved your contacts yet. Please the click the button below.</h4>
                    <button type="button" class="btn btn-default btn-submit" id="sync_contacts">Sync My Contacts</button>
                </div>
              </div>
            <?php } ?> 

            <div class="clearfix"></div>

            <?php $session = $this->session->flashdata('session');                            
              if( isset($session) )
              {
            ?>
              <div class="alert-flash alert <?php echo ($session["status"]) ? "alert-success" : "alert-danger" ?>" role="alert"><?php echo $session["message"]; ?></div>  
              <div class="clearfix"></div>
            <?php } ?>

            <div class="table-responsive">
              <table class="table table-striped tablesorter" id="myTable" >
                <thead>
                  <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Type</th>
                    <th>Phone</th>
                    <th>Last Activity</th>
                    <th>Messages</th>
                    <th>Favorites</th>
                    <th>Saved Searches</th>
                    <th>Action</th>                    
                  </tr>
                </thead>
                <tbody>
                  <?php if( !empty($contacts) ) : 
                    foreach ($contacts as $contact) :
                  ?>
                      <tr id="lead_tr_<?php echo $contact->contact_id; ?>">
                        <td><a href="<?php echo site_url("crm/leads/profile/".$contact->contact_id); ?>"><?php echo ( isset($contact->first_name) && !empty($contact->first_name) ) ? $contact->first_name." ".$contact->last_name : ""; ?></a></td>
                        <td><?php echo ( isset($contact->email) && !empty($contact->email) ) ? $contact->email : "";?></td>
                        <td><?php echo ( isset($contact->type) && !empty($contact->type) ) ? $contact->type : "";?></td>
                        <td><?php echo ( isset($contact->phone) && !empty($contact->phone) ) ? $contact->phone : "";?></td>
                        <td><?php echo $contact->modified; ?></td>
                        <td><a href="#"><?php echo ( isset($contact->messages->count) && !empty($contact->messages->count) ) ? $contact->messages->count : "0";?></a></td>  
                        <td><a href="#"><?php echo ( isset($contact->favourites->count) && !empty($contact->favourites->count) ) ? $contact->favourites->count : "0";?></a></td>
                        <td><a href="#"><?php echo ( isset($contact->save_search) && !empty($contact->save_search) ) ? $contact->save_search : "0";?></a></td>
                        <td> 
                          <a href="<?php echo site_url('crm/leads/send_email/'.$contact->contact_id); ?>" class="leads_send_email" ><span class="glyphicon glyphicon-envelope" title="Send Email" aria-hidden="true"></span></a> &nbsp; <!--<a href="#" ><span class="glyphicon glyphicon-list-alt" title="Send Login Email" aria-hidden="true"></span></a>-->
                          &nbsp; <a href="<?php echo site_url("crm/leads/profile/".$contact->contact_id); ?>" ><span class="glyphicon glyphicon-pencil" title="Edit Profile" aria-hidden="true"></span></a>
                          &nbsp; <a href="<?php echo site_url("crm/leads/delete_leads/".$contact->contact_id); ?>" class="delete-leads" ><span class="glyphicon glyphicon-trash" title="Edit Profile" aria-hidden="true"></span></a> 
                        </td>
                      </tr>
                    <?php endforeach; ?>
                  <?php else : ?>
                    <tr class="center"><td colspan="10"><i>no records found!</i></td></tr>
                  <?php endif; ?>
                </tbody>
              </table>                  
            </div>
                
            <div class="row">
              <div class="col-md-12 search-pagination">
                <div class="pull-right">
                    <ul class="pagination-list">
                      <?php echo $pagination; ?>                     
                    </ul>
                </div>
              </div>
            </div>
          </div>
        <!-- /.box -->
      </section>
      <!-- /.content -->

  </div>

<div class="modal fade bs-example-modal-lg" id="SendEmailLead" data-width="500px" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" data-backdrop="static">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Send Email</h4>
      </div>
      <div class="modal-body">
        <form action="#" method="POST" id="sbt-send-email" class="form-horizontal sbt-send-email-leads">
            <textarea name="message" class="form-control summernote" rows="15"></textarea>       
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary" id="btn-send" >Send</button>
      </div>

      </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade bs-example-modal-lg" id="importContacts" data-width="500px" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" data-backdrop="static">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Import Contacts</h4>
      </div>
      <div class="modal-body">
        <form action="#" method="POST" id="sbtImportContacts" class="form-horizontal sbtImportContacts" enctype="multipart/form-data">
            <input type="file" name="userfile" id="fileToImport">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary" id="btn-send" >Submit</button>
      </div>

      </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

  <!-- /.content-wrapper -->
<?php $this->load->view('session/footer'); ?>
<script type="text/javascript">

  $(document).ready(function() 
    { 
      $("#myTable").tablesorter( ); 

      $('.summernote').summernote({
            height: 400,
            // placeholder: 'Write content here...'
      });

    } 
  ); 
</script>

