<?php 
    $this->load->view('session/header'); 
    $this->load->view('session/top-nav');
    // $this->load->view('session/left-nav');
?>
  <div class="content-wrapper">
  <div class="page-title">
    <h3 class="heading-title">
      <span> Buyer Leads</span>
    </h3>
    <p>Potential buyers to your property</p>
  </div>
    <section class="content">
        <div class="row">
            <div class="col-md-4">
                <h4>Buyer List</h4>
                
                    <?php if( !empty($buyers) ) : $i=1; ?>
                    <ul class="message-clients">
                    <?php
                        foreach( $buyers as $buyer) : 
                    ?>
                            <li class="<?=($i==1) ? "selected" : ""?>">
                                <div class="col-md-3">
                                    <img src="<?= base_url().'assets/images/no-profile-img.gif'?>" alt="" class="img-circle img-responsive">
                                </div>
                                <div class="col-md-6">
                                    <a href="#"><?=$buyer->fname." ".$buyer->lname;?></a> 
                                </div>
                                <div class="col-md-3">
                                    <p><?=date("g:i A", strtotime($buyer->date_created));?></p>
                                </div>
                            </li>
                        <?php $i++; endforeach; ?>
                    </ul>
                    <?php else: ?>
                        <p>No records found!</p>
                    <?php endif; ?>
                
            </div>

           <div class="col-md-8 messaging-area">
                <div class="col-md-6">
                    <div class="lead-details">
                    </div>   
                </div>
            </div>
        </div>
    </section>
  </div>
<?php $this->load->view('session/footer'); ?>