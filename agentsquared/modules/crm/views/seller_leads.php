<?php 
    $this->load->view('session/header'); 
    $this->load->view('session/top-nav');
    // $this->load->view('session/left-nav');
?>  
  <div class="content-wrapper">
  <div class="page-title">
    <h3 class="heading-title">
      <span> Seller Leads   </span>
    </h3>
    <p>They want to sell in your website.</p>
  </div>
    <section class="content">
        <div class="row">
            <div class="col-md-4">
                <h4>Seller List</h4>

                    <?php if( !empty($sellers) ) : $i=1; ?>
                    <ul class="message-clients">
                    <?php
                        foreach( $sellers as $seller) : 
                    ?>
                            <li class="<?=($i==1) ? "selected" : ""?>">
                                <div class="col-md-3">
                                    <img src="<?= base_url().'assets/images/no-profile-img.gif'?>" alt="" class="img-circle img-responsive">
                                </div>
                                <div class="col-md-6">
                                    <a href="#"><?=$seller->fname." ".$seller->lname;?></a> 
                                </div>
                                <div class="col-md-3">
                                    <p><?=date("g:i A", strtotime($seller->date_created));?></p>
                                </div>
                            </li>
                        <?php $i++; endforeach; ?>
                    </ul>
                    <?php else: ?>
                        <p>No records found!</p>
                    <?php endif; ?>
            </div>

           <div class="col-md-8 messaging-area">
                <div class="col-md-6">
                    <div class="lead-details">
                    </div>
                </div>
           </div>
        </div>
    </section>
  </div>
<?php $this->load->view('session/footer'); ?>