<?php 
    $this->load->view('session/header'); 
    $this->load->view('session/top-nav');
    // $this->load->view('session/left-nav');
?>
<div class="content-wrapper">
  <div class="page-title">
    <h3 class="heading-title">
      <span> Customer Leads</span>
    </h3>
    <p>Monitor your site visits, saved properties and etc.</p>
  </div>
    <section class="content">
                <div class="row">
                   <!--  <div class="col-md-6">
                        <h4>Site Visits per Month</h4>
                        <div id="sitevisits" style=""></div>
                    </div> -->
                    <div class="col-md-12">
                        <h4>Newly Signed Customers</h4>
                        <table class="table table-responsive table-striped">
                            <thead>
                                <tr>
                                    <th>Customer Name</th>
                                    <th>Email</th>
                                    <th>Date</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if( !empty($customers) ) : 
                                    foreach ( $customers as $customer ) : 
                                ?>
                                    <tr>
                                        <td><?=(isset($customer->first_name)) ? $customer->first_name.' '.$customer->last_name : "Customer"; ?></td>
                                        <td><?=(isset($customer->email)) ? $customer->email : $customer->orig_email;?></td>
                                        <td><?=date("F d, Y", $customer->created_on)?></td>
                                    </tr>
                                   
                                    <?php endforeach; ?>
                                <?php else: ?>
                                    <tr class="center"><td colspan="8"><i>no records found!</i></td></tr>
                                <?php endif; ?>
                            </tbody>
                        </table>
                        <a href="<?php echo site_url('crm/customer_leads/customers');?>">View all customers</a>
                    </div>
                    <br/><br/>
                    <p class="clearfix"></p>
                    <p class="clearfix"></p>
                    <div class="col-md-6">
                        <h4>Saved Searches</h4>
                        <table class="table table-responsive table-striped">
                            <thead>
                                <tr>
                                    <th>Customer</th>
                                    <th>Saved Searches</th>
                                    <th>Date</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if( !empty($searches_properties) ) : 
                                    foreach ( $searches_properties as $property ) : 

                                    $city = str_replace("&type", "", $property['details'] ) ;
                                    $city = str_replace("&PropertyClass", "", $city ) ;
                                ?>
                                    <tr>
                                        <td><?=($property["first_name"]) ? $property["first_name"].' '.$property["last_name"] : $property["orig_email"];?></td>
                                        <td>
                                            <ul class="lead-item-list">
                                                <li><a href="<?php echo site_url("listings?".$property["url"])?>" target="_blank"><?=$city?></a></li>
                                            </ul>
                                        </td>
                                        <td><?=date("F d, Y", strtotime($property["date_created"]));?></td>
                                    </tr>
                                    <?php endforeach; ?>
                                <?php else: ?>
                                    <tr class="center"><td colspan="8"><i>no records found!</i></td></tr>
                                <?php endif; ?>
                                
                            </tbody>
                        </table>
                        <a href="<?php echo site_url('crm/customer_leads/saved_searches');?>">View all saved searches</a>
                    </div>
                    <div class="col-md-6">
                        <h4>Favorites</h4>
                        <table class="table table-responsive table-striped">
                            <thead>
                                <tr>
                                    <th>Customer</th>
                                    <th>Favorite Property</th>
                                    <th>Date</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if( !empty($favorates_properties) ) : 
                                    foreach ( $favorates_properties as $favorate ) : 
                                ?>
                                    <tr>
                                        <td><?=(isset($favorate["first_name"])) ? $favorate["first_name"].' '.$favorate["last_name"] : $favorate["orig_email"]?></td>
                                        <td>
                                            <ul class="lead-item-list">
                                                <li><a href="<?php echo site_url("other-property-details/".$favorate["property_id"]); ?>" target="_blank"><?php echo $favorate["properties_details"]["StandardFields"]["UnparsedFirstLineAddress"]; ?></a></li>
                                            </ul>
                                        </td>
                                        <td><?=date("F d, Y", strtotime($favorate["date_created"]));?></td>
                                    </tr>
                                    <?php endforeach; ?>
                                <?php else: ?>
                                    <tr class="center"><td colspan="8"><i>no records found!</i></td></tr>
                                <?php endif; ?>
                                
                            </tbody>
                        </table>
                        <a href="<?php echo site_url('crm/customer_leads/saved_favorates');?>">View all saved favorates</a>
                    </div>
                </div>
    </section>
</div>
<?php $this->load->view('session/footer'); ?>

    <script>
    new Morris.Bar({
          // ID of the element in which to draw the chart.
          element: 'sitevisits',
          // Chart data records -- each entry in this array corresponds to a point on
          // the chart.
          data: [
            { visits: 'January', Visits:2 },
            { visits: 'February', Visits:5 },
            { visits: 'March', Visits:2 },
            { visits: 'April', Visits:8 },
            { visits: 'May', Visits:1 }
          ],
          // The name of the data record attribute that contains x-values.
          xkey: 'visits',
          // A list of names of data record attributes that contain y-values.
          ykeys: ['Visits'],
          // Labels for the ykeys -- will be displayed when you hover over the
          // chart.
          labels: ['Site Visits']
        });
    </script>