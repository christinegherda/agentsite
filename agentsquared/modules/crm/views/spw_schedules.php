<?php 
    $this->load->view('session/header'); 
    $this->load->view('session/top-nav');
    // $this->load->view('session/left-nav');
?>
  <div class="content-wrapper">
  <div class="page-title">
    <h3 class="heading-title">
      <span> Scheduled Showings   </span>
    </h3>
    <p>Enter schedules for meetings with your client</p>
  </div>
    <section class="content">
        <div class="row">
            <!-- <div class="col-md-4">
                <div class="datepicker-area">
                    <div class="input-group date" data-provide="datepicker">
                    </div>    
                </div>
            </div> -->
            <div class="col-md-12">
                <div class="meeting-list">
                   <div class="col-md-10 col-sm-12 col-xs-12 pull-left date-search">                         
                             <form class="form-inline" action="" method="get" >                                  
                                  <div class="form-group">
                                    <label class="sr-only" for="exampleInputEmail3"></label>
                                    <input type="input" name="start_date" value="<?php echo (isset($_GET["start_date"])) ? $_GET["start_date"] : ""; ?>" class="form-control" id="startdatepicker" placeholder="Start Date">
                                  </div>
                                  <div class="form-group">
                                    <label class="sr-only" for="exampleInputPassword3"></label>
                                    <input type="input" name="end_date" value="<?php echo (isset($_GET["end_date"])) ? $_GET["end_date"] : ""; ?>" class="form-control" id="enddatepicker" placeholder="End Date">
                                  </div>                                 
                                  <button type="submit" class="btn btn-success">Search</button>
                            </form>
                        </div>
                        <div class="col-md-2  col-sm-12 col-xs-12">                         
                             <form class="form-inline" action="" method="get" >
                                  <div class="form-group">
                                    <label class="sr-only" for="exampleInputEmail3"></label>
                                    <input type="input" name="keywords"  value="<?php echo (isset($_GET["keywords"])) ? $_GET["keywords"] : ""; ?>" class="form-control" id="keywords" placeholder="keywords">
                                  </div>
                                                                
                                  <button type="submit" class="btn btn-success">Search</button>
                            </form>
                        </div>
                    <p class="clearfix"></p>
                    <p class="clearfix"></p>
                    <table class="table-responsive table table-striped">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Number</th>
                                <th>Message</th>
                                <th>Property</th>
                                <th>Date Schedule</th>
                                <th>Date Created</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if( !empty($spw_schedules) ) : 
                                foreach( $spw_schedules as $schedule) : 
                            ?>
                                    <tr>
                                        <td><?php echo (isset($schedule->customer_name) AND !empty($schedule->customer_name)) ? $schedule->customer_name: "Customer"; ?></td>
                                        <td><?php echo (isset($schedule->customer_email)) ? $schedule->customer_email : ""; ?></td>
                                        <td><?php echo (isset($schedule->customer_number)) ? $schedule->customer_number : ""; ?></td>
                                        <td><?php echo $schedule->message;?></td>
                                        <td><?php echo $schedule->property_name;?></td>
                                        <td><?php echo date("F d, Y", strtotime($schedule->date_scheduled))?></td>
                                        <td><?php echo date("F d, Y H:m:s",strtotime($schedule->created))?></td>                                        
                                    </tr>
                                <?php endforeach;?>
                            <?php else : ?>
                                <tr align="center"><td colspan="10"><i>no records found!</i></td></tr>
                            <?php endif;?>
                            
                        </tbody>
                    </table>

                    <div class="col-md-12">
                        <?php if( $pagination ) : ?>
                            <div class="pagination-area pull-right">
                                <nav>
                                    <ul class="pagination">
                                        <?php echo $pagination; ?>
                                    </ul>
                                </nav>
                            </div>
                        <?php endif; ?>
                        
                    </div>
                    
                </div>
            </div>
        </div>
    </section>
  </div>
<?php $this->load->view('session/footer'); ?>