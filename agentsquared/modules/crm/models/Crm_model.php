<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Crm_model extends CI_Model { 

	protected $domain_user;

    public function __construct() {
        parent::__construct();

        $this->domain_user = domain_to_user();
        $this->db_slave =  $this->load->database('db_slave', TRUE);

    }

    public function get_customers_info( $counter = FALSE, $param = array() )
    {
        $this->db_slave->select("u.id as user_id, u.email as orig_email, u.first_name as orig_fname, u.last_name as orig_lname, u.created_on as created_on,cp.*")
                ->from("users u")
                ->join("customer_profile cp", "u.id = cp.customer_id", "left")
                ->join("users_groups ug", "ug.user_id = u.id", "left")
                ->where("u.code", $this->session->userdata("code"))
                ->where("ug.group_id", 3)
                ->order_by("u.id");

        if( isset($param["start_date"]) AND isset($param["end_date"]) )
        {
            $start_date = strtotime($param["start_date"]);
            $end_date = strtotime($param["end_date"]);

            $this->db_slave->where('created_on >= "'.$start_date.'" AND created_on <= "'.$end_date.'" ' );
        } 

        if( isset($param["keywords"]) AND !empty($param["keywords"]) )
        {
            $this->db_slave->where("(u.first_name LIKE CONCAT('%$param[keywords]%') ");
            $this->db_slave->where("u.last_name LIKE CONCAT('%$param[keywords]%') ");
            $this->db_slave->or_where("cp.first_name LIKE CONCAT('%$param[keywords]%')");
            $this->db_slave->or_where("cp.last_name LIKE CONCAT('%$param[keywords]%')");
            $this->db_slave->or_where("u.email LIKE '%$param[keywords]%'");
            $this->db_slave->or_where("u.phone LIKE '%$$param[keywords]%')");
        }

        if( $counter )
        {
            return $this->db_slave->count_all_results();
        }

        if( isset($param["limit"]) AND $param["limit"] != 0)
        {
            $this->db_slave->limit( $param["limit"] );
        }           

        if( !empty($param["offset"]) )
        {
            $this->db_slave->offset( $param["offset"] );
        }

        return $this->db_slave->get()->result();
    }

    public function get_save_search_properties( $counter = FALSE, $param = array() )
    {
        $this->db_slave->select("cs.*, cp.*, u.email as orig_email")
                ->from("customer_searches cs")
                ->join("users u", "cs.customer_id = u.id", "left")
                ->join("customer_profile cp", "cp.customer_id = u.id", "left")
                ->join("users_groups ug", "ug.user_id = u.id", "left")
                ->where("u.code", $this->session->userdata("code"))
                ->where("ug.group_id", 3)
                ->order_by("cs.csid");

        if( isset($param["start_date"]) AND isset($param["end_date"]) )
        {
            $start_date = strtotime($param["start_date"]);
            $end_date = strtotime($param["end_date"]);

            $this->db_slave->where('created_on >= "'.$start_date.'" AND created_on <= "'.$end_date.'" ' );
        } 

        if( isset($param["keywords"]) AND !empty($param["keywords"]) )
        {
            $this->db_slave->where("(u.first_name LIKE CONCAT('%$param[keywords]%') ");
            $this->db_slave->where("u.last_name LIKE CONCAT('%$param[keywords]%') ");
            $this->db_slave->or_where("cp.first_name LIKE CONCAT('%$param[keywords]%')");
            $this->db_slave->or_where("cp.last_name LIKE CONCAT('%$param[keywords]%')");
            $this->db_slave->or_where("u.email LIKE '%$param[keywords]%'");
            $this->db_slave->or_where("u.phone LIKE '%$$param[keywords]%')");
        }

        if( $counter )
        {
            $query = $this->db_slave->get();
            return $query->num_rows();
        }        

        if( isset($param["limit"]) AND $param["limit"] != 0)
        {
            $this->db_slave->limit( $param["limit"] );
        }           

        if( !empty($param["offset"]) )
        {
            $this->db_slave->offset( $param["offset"] );
        }

        $query = $this->db_slave->get();
        
        return $query->result_array();
        
    }

    private function get_customer_searches( $customer_id = NULL )
    {
        $this->db_slave->select("cs.*")
                ->from("customer_searches cs")
                ->where("cs.customer_id", $customer_id);

        return $this->db_slave->get()->result_array(); 
    }

    public function get_save_favorates_properties( $counter = FALSE, $param = array() )
    {
        $this->db_slave->select("cp.*,cpp.*, u.email as orig_email")
                ->from("customer_properties cp")
                ->join("users u", "cp.customer_id = u.id", "left")
                ->join("customer_profile cpp", "cpp.customer_id = u.id", "left")
                ->join("users_groups ug", "ug.user_id = u.id", "left")
                ->where("u.code", $this->session->userdata("code"))
                ->where("ug.group_id", 3)
                ->order_by("cp.cpid");

        if( isset($param["start_date"]) AND isset($param["end_date"]) )
        {
            $start_date = strtotime($param["start_date"]);
            $end_date = strtotime($param["end_date"]);

            $this->db_slave->where('created_on >= "'.$start_date.'" AND created_on <= "'.$end_date.'" ' );
        } 

        if( isset($param["keywords"]) AND !empty($param["keywords"]) )
        {
            $this->db_slave->where("(u.first_name LIKE CONCAT('%$param[keywords]%') ");
            $this->db_slave->where("u.last_name LIKE CONCAT('%$param[keywords]%') ");
            $this->db_slave->or_where("cp.first_name LIKE CONCAT('%$param[keywords]%')");
            $this->db_slave->or_where("cp.last_name LIKE CONCAT('%$param[keywords]%')");
            $this->db_slave->or_where("u.email LIKE '%$param[keywords]%'");
            $this->db_slave->or_where("u.phone LIKE '%$$param[keywords]%')");
        }
        
        if( $counter )
        {
            $query = $this->db_slave->get();
            return $query->num_rows();
        }

        if( isset($param["limit"]) AND $param["limit"] != 0)
        {
            $this->db_slave->limit( $param["limit"] );
        }           

        if( !empty($param["offset"]) )
        {
            $this->db_slave->offset( $param["offset"] );
        }

        $query = $this->db_slave->get();

        if( $counter )
        {
            return $query->num_rows();
        }
        
        return $query->result_array();
    }

    public function get_buyers_info()
    { 
        $this->db_slave->select("*")
                ->from("customer_buyer")
                ->where("agent_id", $this->domain_user->id)
                ->order_by("id", "desc");

        return $this->db_slave->get()->result();
    }

    public function get_seller_info()
    {
        $this->db_slave->select("*")
                ->from("customer_seller")
                ->where("agent_id", $this->domain_user->id)
                ->order_by("id", "desc");

        return $this->db_slave->get()->result();
    }

    public function get_messages_info()
    {
        $this->db_slave->select("cq.*")
                ->from("customer_questions cq")
                ->where("agent_id", $this->domain_user->id)
                ->order_by("cqid", "desc");

        $query =  $this->db_slave->get();

        if( $query->num_rows() > 0 )
        {
            $datas = $query->result();

            foreach ($datas as &$key) {
                $key->messages = $this->get_message( $key->cqid );
            }
            
            return $datas;

        }
        return FALSE;

    }

    public function get_message( $qid = NULL )
    {
        $this->db_slave->select("*")
                ->from("customer_messaging")
                ->where("mid", $qid);

        return $this->db_slave->get()->result();
    }

    public function insert_reply()
    {
        $reply["message"] = $this->input->post("message");
        $reply["type"] = 1;
        $reply["mid"] = $this->input->post("mid");
        $reply["created"] = date("Y-m-d H:m:s");

        $this->db->insert("customer_messaging", $reply);

        return ($this->db->insert_id()) ? $this->db->insert_id() : FALSE;
    }

    public function get_customer_info( $message_id = NULL)
    {
        $this->db_slave->select("*")
                ->from("customer_questions")
                ->where("cqid", $message_id)
                ->limit(1);

        return $this->db_slave->get()->row();
    }

    public function get_agent_info()
    {
        $this->db_slave->select("*")
                ->from("users")
                ->where("id", $this->domain_user->id)
                ->limit(1);

        return $this->db_slave->get()->row(); 
    }

    public function get_schedule_of_showing( $counter = FALSE, $param = array() )
    {
        $this->db_slave->select("css.*, css.created as showing_created ,cp.*, (select u.email from users u where u.id = css.customer_id LIMIT 1) as orig_email")
                ->from("customer_schedule_of_showing css")
                ->join("customer_profile cp", "cp.customer_id = css.customer_id", "left")
                //->join("users u", "u.id = css.customer_id", "left")
                ->where("css.agent_id", $this->domain_user->id)
                 ->where("css.type", "agent_site")
                ->order_by("cssid", "desc");

        if( isset($param["start_date"]) AND isset($param["end_date"]) )
        {
            $start_date = date("Y-m-d", strtotime($param["start_date"]) );
            $end_date = date("Y-m-d", strtotime($param["end_date"]) ); 

            $this->db_slave->where('css.date_scheduled >= "'.$start_date.'" AND css.date_scheduled <= "'.$end_date.'" ' );
        } 

        if( isset($param["keywords"]) AND !empty($param["keywords"]) )
        {
            $this->db_slave->where("(css.property_name LIKE '%$param[keywords]%' ");
            $this->db_slave->or_where("cp.first_name LIKE CONCAT('%$param[keywords]%')");
            $this->db_slave->or_where("cp.last_name LIKE CONCAT('%$param[keywords]%')");
            $this->db_slave->or_where("u.email LIKE '%$param[keywords]%'");
            $this->db_slave->or_where("u.phone LIKE '%$$param[keywords]%')");
        }
        
        if( $counter )
        {
            $query = $this->db_slave->get();
            return $query->num_rows();
        }

        if( isset($param["limit"]) AND $param["limit"] != 0)
        {
            $this->db_slave->limit( $param["limit"] );
        }           

        if( !empty($param["offset"]) )
        {
            $this->db_slave->offset( $param["offset"] );
        }

        $query = $this->db_slave->get();

        if( $counter )
        {
            return $query->num_rows();
        }
        
        return $query->result();

    }

     public function get_spw_schedule_of_showing( $counter = FALSE, $param = array() )
    {

        $this->db_slave->select("*")
                ->from("customer_schedule_of_showing")
                ->where("agent_id", $this->domain_user->id)
                //->where("type", "spw")
                ->where('(type="spw" or type="agent_site")')
                ->order_by("cssid", "desc");



        if( isset($param["start_date"]) AND isset($param["end_date"]) )
        {
            $start_date = date("Y-m-d", strtotime($param["start_date"]) );
            $end_date = date("Y-m-d", strtotime($param["end_date"]) ); 

            $this->db_slave->where('date_scheduled >= "'.$start_date.'" AND date_scheduled <= "'.$end_date.'" ' );
        } 

        if( isset($param["keywords"]) AND !empty($param["keywords"]) )
        {
            $this->db_slave->where("(property_name LIKE '%$param[keywords]%' ");
            $this->db_slave->or_where("customer_name LIKE CONCAT('%$param[keywords]%')");
            $this->db_slave->or_where("customer_email LIKE '%$param[keywords]%'");
            $this->db_slave->or_where("customer_number LIKE '%$$param[keywords]%')");
        }
        
        if( $counter )
        {
            $query = $this->db_slave->get();
            return $query->num_rows();
        }

        if( isset($param["limit"]) AND $param["limit"] != 0)
        {
            $this->db_slave->limit( $param["limit"] );
        }           

        if( !empty($param["offset"]) )
        {
            $this->db_slave->offset( $param["offset"] );
        }

        $query = $this->db_slave->get();

        if( $counter )
        {
            return $query->num_rows();
        }
        
        return $query->result();

    }

    public function get_property_sold( $counter = FALSE, $param = array() ){

        $this->db_slave->select("*")
                ->from("property_activated")
                ->where("property_status", "Sold");

        if($counter)
        {   
            return $this->db_slave->count_all_results();
        }

        $query = $this->db_slave->get();

        if( $query->num_rows() > 0 )
        {
            return $query->result();
        }

        return FALSE;
    }

    public function get_favorites_property( $counter = FALSE, $param = array() ){
        //echo $this->session->userdata("code"); exit;
        $this->db_slave->select("cp.*,u.code")
                ->from("customer_properties cp")
                ->join("users u", "cp.customer_id = u.id", "left")
                ->join("users_groups ug", "ug.user_id = 3", "left")
                ->where("u.code", $this->session->userdata("code"))
                ->group_by('cp.property_id');

        if($counter)
        {   
            return $this->db_slave->count_all_results();
        }

        $query = $this->db_slave->get();
        //printA($query->result()); exit;
        if( $query->num_rows() > 0 )
        {
            $datas =  $query->result_array();
            foreach ($datas as &$key) {
               $key["counter"] = $this->count_favorates_properties($key["property_id"]);
            }

            return $datas;
        }

        return FALSE;
    }

    private function count_favorates_properties( $property_id = NULL )
    {
        $this->db_slave->select()
                ->from("customer_properties")
                ->where("property_id", $property_id);

        $query = $this->db_slave->get();

        return $query->num_rows();
    }

    public function get_search_property( $counter = FALSE, $param = array() ){
       
        $this->db_slave->select("cs.*")
                ->from("customer_searches cs")
                ->join("users u", "cs.customer_id = u.id", "left")
                ->join("users_groups ug", "ug.user_id = 3", "left")
                ->where("u.code", $this->session->userdata("code"))
                ->group_by('cs.details');

        if($counter)
        {   
            return $this->db_slave->count_all_results();
        }

        $query = $this->db_slave->get();
        //printA($query->result()); exit;
        if( $query->num_rows() > 0 )
        {
            $datas =  $query->result_array();

            foreach ($datas as &$key) {
               $key["counter"] = $this->count_search_properties($key["details"]);
            }

            return $datas;
        }

        return FALSE;
    }

    private function count_search_properties( $details = NULL )
    {
        $this->db_slave->select()
                ->from("customer_searches")
                ->like("details", $details);

        $query = $this->db_slave->get();

        return $query->num_rows();
    }

}