<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Leads_model extends CI_Model { 

	protected $domain_user;

    public function __construct() {
        parent::__construct();

        $this->domain_user = domain_to_user();
        $this->db_slave =  $this->load->database('db_slave', TRUE);

    }

    public function save_contacts( $datas = NULL){

        $password = "agentsquared1234";

        $save["contact_id"] = $datas["Id"];
        $save["first_name"] = ( isset($datas["GivenName"]) && !empty($datas["GivenName"]) ) ? $datas["GivenName"] : $datas["DisplayName"]; 
        $save["last_name"] = ( isset($datas["FamilyName"]) && !empty($datas["FamilyName"]) ) ? $datas["FamilyName"] : 0; 
        $save["email"] = ( isset($datas["PrimaryEmail"]) && !empty($datas["PrimaryEmail"]) ) ? $datas["PrimaryEmail"] : 0; 
        $save["phone"] = ( isset($datas["PrimaryPhoneNumber"]) && !empty($datas["PrimaryPhoneNumber"]) ) ? $datas["PrimaryPhoneNumber"] : 0;
        $save["modified"] = ( isset($datas["ModificationTimestamp"]) && !empty($datas["ModificationTimestamp"]) ) ? $datas["ModificationTimestamp"] : 0;
        $save["created"] = date("Y-m-d H:m:s");
        $save["agent_id"] = $this->domain_user->agent_id;
        $save["user_id"] = $this->domain_user->id;
        $save["type"] = "flex";
        $save["ip_address"] = $_SERVER['REMOTE_ADDR'];
        $save["password"] = password_hash( $password, PASSWORD_DEFAULT);
        $save["active"] = "1";

        $this->db->insert( "contacts", $save );   

        return ($this->db->insert_id()) ? FALSE : TRUE;

    }
    public function get_contacts( $counter = FALSE, $param = array() )
    {
        $this->db_slave->select("*")
                ->from("contacts")
                ->where("user_id", $this->domain_user->agent_id)
                ->where("agent_id", $this->domain_user->agent_id)
                ->where("is_deleted", 0);

        if( isset($_GET["keywords"]) && !empty($_GET["keywords"]) )
        {
            $this->db_slave->where("(first_name like '%".$_GET["keywords"]."%' || 
                    last_name like '%".$_GET["keywords"]."%' ||
                    email like '%".$_GET["keywords"]."%' ||
                    phone like '%".$_GET["keywords"]."%' )");
        }

        if( isset($_GET["type"]) && !empty($_GET["type"]))
        {
            $this->db_slave->where("type", $this->input->get('type') );
        }

        if( $counter )
        {
            return $this->db_slave->count_all_results();
        }

        if( !empty($param["limit"]) )
        { 
            $this->db_slave->limit( $param["limit"] );
        } 

        if( !empty($param["offset"]) )
        {
            $this->db_slave->offset( $param["offset"] );
        }

        $this->db_slave->order_by("created", "desc");

        $query = $this->db_slave->get();

        if( $query->num_rows() > 0 ){
            $contacts =  $query->result();

            foreach ($contacts as &$contact) {
               $contact->favourites = $this->CountFavorites( $contact->id );
               $save_search_db = $this->CountSaveSearch( $contact->id );
               $save_search_flex = $this->CountSaveSearchFlex( $contact->contact_id );               
               $contact->save_search = $save_search_db->count + $save_search_flex;
               $contact->messages = $this->CountMessages( $contact->id );              
            }
            
            return $contacts;
        }

        return FALSE; 
    }

    public function get_customer($data=array(),$type=NULL) {
        $ret = FALSE;

        if($data && $type) {
            $customer = $this->db_slave->select("*")->from("customer_".$type)->where($data)->get()->row();
            $ret = ($customer) ? $customer : FALSE;
        }

        return $ret;

    }

    public function CountFavorites( $id = NULL ){

        $this->db_slave->select(" COUNT(cpid) as count")
                ->from("customer_properties")
                ->where("customer_id", $id);

        return $this->db_slave->get()->row();    

    }

    public function CountSaveSearch( $id = NULL ){

        $this->db_slave->select(" COUNT(csid) as count")
                ->from("customer_searches")
                ->where("customer_id", $id);

        return $this->db_slave->get()->row();    

    }

    public function CountMessages( $id = NULL ){

        $this->db_slave->select(" COUNT(id) as count")
                ->from("customer_messaging")
                ->where("customer_id", $id);

        return $this->db_slave->get()->row();    

    }

    public function CountSaveSearchFlex( $contact_id = NULL )
    {

        $this->db_slave->select("id,json_saved_search_updated,img,date_modified")
                ->from("saved_searches")
                ->where("FIND_IN_SET('".$contact_id."',ContactIds) != ",0);

        $query = $this->db_slave->get(); 

        return $query->num_rows();
        
    }

    public function get_profile_info( $contact_id = NULL ){
        $this->db_slave->select("*")
                ->from("contacts")
                ->where("contact_id", $contact_id)
                ->limit(1);

        $query = $this->db_slave->get();

        if($query->num_rows() > 0) {

            $profile = $query->row();
            
            $profile->favourites = $this->getFavouritesList( $profile->id );
            $profile->save_searches = $this->getSaveSearches( $profile->id );
            $profile->messages = $this->getMessages( $profile->id );
            $profile->schedule_showing = $this->getSchedule( $profile->id );
            $profile->notes = $this->getNotes( $contact_id );
            $profile->tasks = $this->getTasks( $contact_id );

            $save_searches = $this->getSaveSearches( $profile->id );
            $save_searches_flex = $this->getSaveSearchesFlex( $contact_id );
            $save_searches_flex = ( empty($save_searches_flex) ) ? array() : $save_searches_flex;

            $profile->saveSearches = array_merge($save_searches, $save_searches_flex);

            return $profile;
        }

        return FALSE;

    }

    public function getFavouritesList( $id = NULL ){

        $this->db_slave->select("*")
                ->from("customer_properties")
                ->where("customer_id", $id);

        return $this->db_slave->get()->result();    
    }

    public function getSaveSearches( $id = NULL ){
        $this->db_slave->select("*")
                ->from("customer_searches")
                ->where("customer_id", $id);

        return $this->db_slave->get()->result();    
    }

    public function getMessages( $id = NULL ){

        $this->db_slave->select("customer_messaging.*, CONCAT(contacts.first_name ,' ', contacts.last_name) as contact_name, CONCAT(users.first_name ,' ', users.last_name) as agent_name ")
                ->from("customer_messaging")
                ->join("contacts", "customer_messaging.contact_id = contacts.contact_id", "left")
                ->join("users", "users.agent_id = customer_messaging.agent_id", "left")
                ->where("customer_messaging.customer_id", $id)
                ->where("customer_messaging.agent_id !=", 0)
                ->order_by("id", "desc");


        return $this->db_slave->get()->result();    
    }

    public function getSchedule( $id = NULL ){
        $this->db_slave->select("*")
                ->from("customer_schedule_of_showing")
               ->where("customer_id", $id);

        return $this->db_slave->get()->result();    
    }

    public function update_profile( $contact_id = NULL ){

        $profile = $this->input->post("profile");

        $this->db->where("contact_id", $contact_id);
        $this->db->update("contacts", $profile);

        return ($this->db->affected_rows() > 0) ? TRUE : FALSE ;

    }

    public function save_email( $customer_id = "", $contact_id = "" ){

        $save["customer_id"] =  $customer_id;
        $save["contact_id"] =  $contact_id;
        $save["agent_id"] =  $this->domain_user->agent_id;
        $save["sent_by"] =  $this->domain_user->agent_id;
        $save["created"] =  date("Y-m-d H:m:s");
        $save["type"] = 1;
        $save["message"] = $_POST["message"];

        $this->db->insert("customer_messaging", $save);

        return ( $this->db->insert_id() ) ? $this->db->insert_id() : FALSE;
    }

    public function get_activities( $contact_id = NULL ) 
    {
        $this->db_slave->select("*")
                ->from("core_activities")
                ->where("contact_id", $contact_id)
                ->where("agent_id", $this->domain_user->agent_id)
                ->order_by("id","desc");

        return $this->db_slave->get()->result();
    }

    public function getSaveSearchesFlex( $contact_id = NULL )
    {

        $this->db_slave->select("id,json_saved_search_updated,img,date_modified")
                ->from("saved_searches")
                ->where("FIND_IN_SET('".$contact_id."',ContactIds) != ",0);

        $query = $this->db_slave->get(); 

        if( $query->num_rows() > 0 )
        {
            $data = $query->result();
            
            foreach ($data as &$key) {
                $key->json_decoded = json_decode($key->json_saved_search_updated);
            }

            return $data;
        }

        return FALSE;
    }

    public function get_message_contect_ajax( $message_id = NULL )
    {
        $this->db_slave->select("*")
                ->from("customer_messaging")
                ->where("id", $message_id)
                ->limit(1);

        return $this->db_slave->get()->row();
    }

    public function add_notes( $contact_id = "" ){
        
        $notes["created_by"] = $this->domain_user->agent_id;
        $notes["notes"] = $this->input->post("notes");
        $notes["contact_id"] = $contact_id;
        $notes["date_created"] = date("Y-m-d H:i:s");

        $this->db->insert("contact_notes", $notes);

        return ( $this->db->insert_id() ) ? $this->db->insert_id() : FALSE;
    }

    public function add_tasks( $contact_id = "" ){
        
        $tasks["created_by"] = $this->domain_user->agent_id;
        $tasks["tasks"] = $this->input->post("tasks");
        $tasks["contact_id"] = $contact_id;
        $tasks["date_created"] = date("Y-m-d H:i:s");

        $this->db->insert("contact_tasks", $tasks);

        return ( $this->db->insert_id() ) ? $this->db->insert_id() : FALSE;
    }

    public function getNotes( $contact_id = NULL )
    {
        $this->db_slave->select("cn.*, CONCAT(u.first_name ,' ', u.last_name) as created_by")
                ->from("contact_notes cn")
                ->join("users u", "cn.created_by = u.agent_id", "left")
                ->where("cn.contact_id", $contact_id)
                ->order_by("cn.id", "desc");

        return $this->db_slave->get()->result();

    }

    public function getTasks( $contact_id = NULL )
    {
        $this->db_slave->select("ct.*, CONCAT(u.first_name ,' ', u.last_name) as created_by")
                ->from("contact_tasks ct")
                ->join("users u", "ct.created_by = u.agent_id", "left")
                ->where("ct.contact_id", $contact_id)
                ->order_by("ct.id", "desc");

        return $this->db_slave->get()->result();
    }

    public function delete_notes( $id = NULL ){

        $this->db->where("id", $id);
        $this->db->delete("contact_notes");

        return ($this->db->affected_rows() > 0) ? TRUE : FALSE;
    }

    public function delete_tasks( $id = NULL ){

        $this->db->where("id", $id);
        $this->db->delete("contact_tasks");

        return ($this->db->affected_rows() > 0) ? TRUE : FALSE;
    }

    public function get_contacts_lists()
    {
        $this->db_slave->select("*")
                ->from("contacts")
                ->where("agent_id", $this->domain_user->agent_id)
                ->order_by("id", "asc");

        return $this->db_slave->get()->result();
    }

    public function insert_csv( $insert_csv = array() )
    {   
        $this->db->insert("contacts", $insert_csv );

        return ($this->db->insert_id()) ? TRUE : FALSE; 
    }

    public function is_contacts_exists( $datas )
    {
        $this->db_slave->select("email")
                ->from("contacts")
                ->where("email", $datas["PrimaryEmail"])
                ->where("agent_id", $this->domain_user->agent_id)
                ->limit(1);

        $query = $this->db_slave->get();

        return ($query->num_rows() > 0) ? TRUE : FALSE;

    }

    public function get_capture_leads($agent_id) {

        $this->db_slave->select('capture_leads, is_email_market')
                ->from('users')
                ->where('agent_id', $agent_id);

        return $this->db_slave->get()->row();
    }

    public function set_capture_leads($data) {

        $this->db->where('agent_id', $this->domain_user->agent_id);
        $this->db->update('users', $data);

    }

    public function get_agent_domain() {

        $arr = array(
            'agent' => $this->domain_user->id,
            'type'  => 'agent_site_domain',
            //'status' => 'completed' 
        );

        $this->db_slave->select("domains, subdomain_url, status, existing_domain, is_subdomain, type")
                        ->from("domains")
                        ->where($arr);

        $query = $this->db_slave->get();

        return ($query->num_rows() > 0) ? $query->row() : FALSE;
    }

    public function delete_lead( $contact_id = NULL )
    {

        $this->db->where( "contact_id", $contact_id);
        $this->db->update("contacts", array( "is_deleted" => 1 ) );

        return ( $this->db->affected_rows() > 0 ) ? TRUE : FALSE;
    }
}
