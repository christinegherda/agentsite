<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Leads extends MX_Controller {

	protected $domain_user;

	function __construct() {
		parent::__construct();
		//printA($this->session->userdata()); exit;
		/*if(!$this->session->userdata('logged_in_auth'))
			redirect('login/auth/login', 'refresh');*/
		$this->domain_user = domain_to_user();

		$this->load->model("Crm_model", "crm");
		$this->load->model("Leads_model", "leads");
		$this->load->model("agent_sites/Agent_sites_model");
		$this->load->library("idx_auth", $this->domain_user->agent_id);
	}

	public function index() {

		$data['title'] = "Leads";
		$this->load->library('pagination');

		$siteInfo = Modules::run('agent_sites/getInfo');
		$data['branding'] = $siteInfo['branding'];
		$data["is_reserved_domain"] = $this->Agent_sites_model->is_reserved_domain();
		$data["domain_info"] = $this->Agent_sites_model->fetch_domain_info();

		$param["offset"] = (isset($_GET["offset"])) ? $_GET["offset"] : 0;
		$param["limit"] = 25;

		$count_contacts = $this->leads->get_contacts(TRUE);
		$total = $this->leads->get_contacts(TRUE, $param );
		$contacts = $this->leads->get_contacts(FALSE, $param );

		$config["base_url"] = base_url().'/crm/leads';
		$config["total_rows"] = $total;
		$config["per_page"] = $param["limit"];
		$config['page_query_string'] = TRUE;
		$config['query_string_segment'] = 'offset';

		$this->pagination->initialize($config);
		$data["pagination"] = $this->pagination->create_links();

		//printA($contacts); exit;
		$data["contacts"] = $contacts;
		$data["count_contacts"] = $count_contacts;

		$data["js"] = array("plugins/tablesorter/jquery.tablesorter.js","leads/leads.js");

		$this->load->view('leads', $data);

	}

	public function profile( $contact_id = NULL ) {
		$data['title'] = "Leads Profile";
		$siteInfo = Modules::run('agent_sites/getInfo');
		$data['branding'] = $siteInfo['branding'];
		$data["is_reserved_domain"] = $this->Agent_sites_model->is_reserved_domain();
		$data["domain_info"] = $this->Agent_sites_model->fetch_domain_info();

		$profile = $this->leads->get_profile_info($contact_id);

		if(!empty($profile))
		{
			$profile->infos = $this->idx_auth->GetContact( $contact_id );

			if($profile->type=="seller" || $profile->type=="buyer") {
				$customer = $this->leads->get_customer(array('contact_id'=>$profile->id),$profile->type);
				if($customer)
					$data["customer"] = $customer;
			}
		}

		if(!empty($profile->favourites)) {
			foreach ($profile->favourites as &$favourite) {
				$favourite->properties_details = $this->idx_auth->GetListing( $favourite->property_id );
			}
		}

		if(!empty($profile->schedule_showing)) {
			foreach ($profile->schedule_showing as &$schedule) {
				$schedule->properties_details = $this->idx_auth->GetListing( $schedule->property_id );
			}
		}

		//$message = $this->api->GetContactsMessages($contact_id, array('_orderby' => "CreatedTimestamp"));

		$data["activities"] = $this->leads->get_activities( $contact_id );
		$data["contact_id"] = $contact_id;
		$data["profile"] = $profile;
		$data["js"] = array("leads/leads.js");

		$this->load->view('profile', $data);

	}

	public function update_profile( $contact_id = NULL )
	{
		if( $_POST )
		{
			if($this->leads->update_profile( $contact_id ))
			{
				$this->session->set_flashdata('session' , array('status' => TRUE,'message' => 'Profile has been successfully update!'));
				redirect("crm/leads/profile/".$contact_id, "refresh");
			}
			else
			{

				$this->session->set_flashdata('session' , array('status' => FALSE,'message' => 'Failed to update profile!'));
				redirect("crm/leads/profile/".$contact_id, "refresh");
			}
		}

	}

	public function SaveContactList(){

		$flag = FALSE;
		$contacts = $this->idx_auth->GetContacts(1);
		//printA( $this->domain_user->agent_id ); exit;
		for ($i=1; $i <= $contacts["data"]->total_pages; $i++) {

			$contactsave = $this->idx_auth->GetContacts($i);


			if( !empty( $contactsave["results"] ) )
			{
				foreach ($contactsave["results"] as $contacts) {

					if( ! $this->leads->is_contacts_exists( $contacts ) )
					{
						$this->leads->save_contacts( $contacts );
					}

					// send email to each contacts
					//$this->send_login_portal_to_contacts( $contacts );
				}

				$flag = TRUE;
			}
			else
			{
				$flag = FALSE;
			}

		}

		if( $flag ) {
			$response = array("success" => TRUE, "redirect" => base_url().'crm/leads');
			exit(json_encode($response));
			//return TRUE;
		} else {
			$response = array("success" => FALSE, "redirect" => base_url().'crm/leads');
			exit(json_encode($response));
			//return FALSE;
		}

		return TRUE;
	}

	public function getContactsSaveSearch(){
		$contacts = $this->api->GetSavedSearch("20130513191420112900000000");
		//$contacts = $this->api->GetContactSaveSearches( "20130322030414211302000000", "20130513191420112900000000");
		//var_dump($contacts); exit;
		printA( $contacts ); exit;
	}

	public function send_email( $contact_id = NULL)
	{
		if( empty($contact_id) ) return FALSE;

		$profile = $this->leads->get_profile_info( $contact_id );

		if( $_POST ){

			if( $email_id = $this->leads->save_email( $profile->id, $contact_id ) )
			{
				//save to activtiy
				save_activities( $profile->id, $email_id, $_POST["message"], "send email", "agent", $contact_id );
				$this->send_email_to_customer( $profile );

				exit(json_encode(array("success" => TRUE)));
			}
			else
			{
				exit(json_encode(array("success" => FALSE)));
			}

		}

	}


	public function send_email_to_customer( $profile = ""  )
	{
	 	$this->load->library("email");

		$email = $profile->email;
		$first_name = $profile->first_name;

		$content = "Hi ".$first_name.", <br><br>

			You have new message from AgentSquared Team! <br><br>

			<a href='".base_url("/home?customer_login")."' target = '_blank'> Read Message </a> <br><br>

			Regards, <br>

			Team AgentSquared
			";

	 	$subject ="New message from AgentSquared" ;

	 	return $this->email->send_email(
	 		'automail@agentsquared.com',
	 		'AgentSquared',
	 		$email,
	 		$subject,
	 		'email/template/default-email-body',
	 		array(
	 			"message" => $content,
	 			"subject" => $subject,
	 			"to_bcc" => "rolbru12@gmail.com"
	 		)
	 	);
	}

	public function send_login_credential_to_customer() {

		$this->load->library("email");

		$siteInfo 		= Modules::run('agent_sites/getInfo');

		$agent_name 	= ($siteInfo['account_info']) ? $siteInfo['account_info']->Name : $siteInfo['branding']->first_name." ".$siteInfo['branding']->last_name;
		$agent_org 		= ($siteInfo['account_info']->Office) ? $siteInfo['account_info']->Office : "";
		$agent_email 	= $siteInfo['branding']->email;
		$agent_phone 	= $siteInfo['branding']->phone;
		$agent_domain 	= "https://www.agentsquared.com/";
		$logo_url 		= getenv('AWS_S3_ASSETS') . "images/email-logo.png";
		$agent_website_photo = getenv('AWS_S3_ASSETS')."images/email-logo.png";
		$domain_info 	= $this->leads->get_agent_domain();

		if($domain_info) {
			$agent_domain = ($domain_info->status == "completed" || $domain_info->is_subdomain) ? ucfirst($domain_info->domains) : $domain_info->subdomain_url;
		}

		$agent_domain_url = $agent_domain.'/home?customer_login';

		//get agent_logo
		if(isset($siteInfo['branding']->logo) && !empty($siteInfo['branding']->logo)) {
			$logo_url = getenv('AWS_S3_ASSETS')."uploads/logo/".$siteInfo['branding']->logo;
		} else {
			if(isset($siteInfo['account_info']->Images) && !empty($siteInfo['account_info']->Images)) {
				foreach ($siteInfo['account_info']->Images as $key) {
					if($key->Type == "Logo") {
						$logo_url = $key->Uri;
						break;
					}
				}
			}
		}

		if(isset($siteInfo['branding']->agent_photo) && !empty($siteInfo['branding']->agent_photo)) {
			$agent_website_photo = getenv('AWS_S3_ASSETS')."uploads/photo/".$siteInfo['branding']->agent_photo;
		} else {
			if(isset($siteInfo['account_info']->Images) && !empty($siteInfo['account_info']->Images)) {
				foreach ($siteInfo['account_info']->Images as $key) {
					if($key->Type=="Photo" || $key->Type == "Other Image" || $key->Type == "Logo") {
						$agent_website_photo = $key->Uri;
						break;
					}
				}
			}
		}

		$customer_email = $this->input->post("email");
		$customer_phone = $this->input->post("phone_signup");
		$customer_fname = $this->input->post("fname");
		$customer_lname = $this->input->post("lname");

		$subs = array(
			'customer_name'		=> $customer_fname." ".$customer_lname,
			'customer_email' 	=> $customer_email,
			'customer_phone'	=> $customer_phone,
			'agent_name'		=> $agent_name,
			'logo_url'			=> $logo_url,
			'agent_company'		=> $agent_org,
			'agent_phone'		=> $agent_phone,
			'agent_email'		=> $agent_email,
			'agent_site' 		=> $agent_domain,
			'agent_website_url' => $agent_domain_url,
			'agent_website_photo' => $agent_website_photo
		);
		//printA($subs);exit;
		$result = $this->email->send_email_v3(
        	$agent_email, //sender email
        	$agent_name, //sender name
        	$customer_email, //recipient email
        	'paul@agentsquared.com,aldwinj12345@gmail.com', //bccs
        	"b9b68c9b-519f-4d8c-baae-a4ab5f60b8d5", //"39414402-14e5-4af6-9155-ea397de9095a", //template_id
        	$email_data = array('subs'=>$subs)
        );
	}

	public function send_customer_question() {

		$this->load->library("email");

		$siteInfo = Modules::run('agent_sites/getInfo');

		$subs = array(
			'page_title'	=> $this->input->post('property_address'),
			'first_name'	=> $siteInfo['branding']->first_name,
			'customer_name' => $this->input->post("first_name")." ".$this->input->post("last_name"),
			'customer_email'=> $this->input->post("email"),
			'customer_phone'=> $this->input->post("phone"),
			'lead_message' 	=> $this->input->post("message")
		);

		$this->email->send_email_v3(
        	'support@agentsquared.com', //sender email
        	'AgentSquared', //sender name
        	$siteInfo['branding']->email, //recipient email
        	'paul@agentsquared.com,aldwinj12345@gmail.com', //bccs
        	'2dfb2762-d3da-48e5-b7e4-21b171744967', //"82960f90-120e-4b2d-9d9a-053a814e4277", //template_id
        	$email_data = array('subs'=>$subs, 'reply_to'=>$this->input->post('email'))
        );
		
	}

	public function respond_to_customer_question() {

		$this->load->library("email");

		$siteInfo = Modules::run('agent_sites/getInfo');

		$agent_name = ($siteInfo['account_info']) ? $siteInfo['account_info']->Name : $siteInfo['branding']->first_name." ".$siteInfo['branding']->last_name;
		$agent_org = ($siteInfo['account_info']->Office) ? $siteInfo['account_info']->Office : "";
		$agent_email = $siteInfo['branding']->email;
		$agent_num = ($siteInfo['branding']->phone) ? $siteInfo['branding']->phone : $siteInfo['account_info']->Phones[0]->Number;
		$mls_name = $siteInfo['account_info']->Mls;
		$img_tag = "";
		$agent_domain = "https://agentsquared.com/";
		$logo_url = AGENT_DASHBOARD_URL . "assets/images/email-logo.png";

		//get agent_photo
		if($siteInfo['branding']->agent_photo) {

			$agent_photo = $siteInfo['branding']->agent_photo;
			$img_tag = "<img src='" . AGENT_DASHBOARD_URL . "assets/upload/photo/$agent_photo' width='140' height='140'>";

		} else {

			foreach($siteInfo['account_info']->Images as $key) {

				if($key->Type == "Photo") {

					$agent_photo = $key->Uri;
					$img_tag = "<img src='$agent_photo' width='140' height='140'>";
					break;

				} elseif($key->Type == "Other Image") {

					$agent_photo = $key->Uri;
					$img_tag = "<img src='$agent_photo' width='140' height='140'>";
					break;

				} elseif($key->Type =="Logo") {

					$agent_photo = $key->Uri;
					$img_tag = "<img src='$agent_photo' width='140' height='140'>";	
					break;

				}

			}
		}

		//get agent_logo
		if(isset($siteInfo['branding']->logo) && !empty($siteInfo['branding']->logo)) {
			$logo_url = AGENT_DASHBOARD_URL . "assets/upload/logo/".$siteInfo['branding']->logo;
		} elseif(isset($siteInfo['account_info']->Images) && !empty($siteInfo['account_info']->Images)) {
			foreach ($siteInfo['account_info']->Images as $key) {
				if($key->Type == "Logo") {
					$logo_url = $key->Uri;
					break;
				}
			}
		}

		$domain_info = $this->leads->get_agent_domain();

		if($domain_info) {
			$agent_domain = ($domain_info->status == "completed" || $domain_info->is_subdomain) ? ucfirst($domain_info->domains) : $domain_info->subdomain_url;
		}

		$customer_name = $this->input->post("first_name"). " " .$this->input->post("last_name");
	    $customer_email = $this->input->post("email");

		$content = "Hi ".$customer_name."!<br><br>

			Thanks for your message.<br><br>

			My name is ".$agent_name." and I am with ".$agent_org.".
			I received the request form you recently submitted on my website.
			You can always reach me directly at ".$agent_num." or <a href='mailto:$agent_email' target='_blank'>".$agent_email."</a><br><br>

			Feel free to view as many homes as you like and let me know if you have any questions regarding any of the properties that appeal to you.
			The listings on my website are made available through the ".$mls_name." so your home search will always be current and complete.<br><br>

			I look forward to assist you with your Real Estate needs.<br><br>

			All the best,<br><br>"
			.$agent_name."<br>"
			.$agent_domain."<br>"
			.$agent_num."<br>"
			.$img_tag."<br>";

		$subject ="Thanks for your inquiry";

        $this->email->send_email(
            $agent_email,
            $agent_name,
            $customer_email,
            $subject,
            'email/template/default-email-body',
            array(
                "message" => $content,
                "subject" => $subject,
                "logo_url" => $logo_url,
                "to_bcc" => "honradokarljohn@gmail.com,joce@agentsquared.com,aldwinj12345@gmail.com"
            )
        );
	}

	public function respond_to_buyer() {

		$this->load->library("email");
		
		$domain_info 		= $this->leads->get_agent_domain();
		$siteInfo 			= Modules::run('agent_sites/getInfo');

		$agent_name 		= (!empty($siteInfo['branding']->first_name) && !empty($siteInfo['branding']->last_name)) ? $siteInfo['branding']->first_name." ".$siteInfo['branding']->last_name : $siteInfo['account_info']->Name;
		$agent_org 			= (!empty($siteInfo['account_info']->Office)) ? $siteInfo['account_info']->Office : "";
		$agent_email 		= $siteInfo['branding']->email;
		$agent_num 			= ($siteInfo['branding']->phone) ? $siteInfo['branding']->phone : $siteInfo['account_info']->Phones[0]->Number;
		$mls_name 			= $siteInfo['account_info']->Mls;
		$agent_domain 		= "https://www.agentsquared.com/";
		$agent_website_photo= getenv('AWS_S3_ASSETS')."images/email-logo.png";
		$logo_url			= getenv('AWS_S3_ASSETS')."images/email-logo.png";

		//get agent_logo
		if(isset($siteInfo['branding']->logo) && !empty($siteInfo['branding']->logo)) {
			$logo_url = getenv('AWS_S3_ASSETS')."uploads/logo/".$siteInfo['branding']->logo;
		} elseif(isset($siteInfo['account_info']->Images) && !empty($siteInfo['account_info']->Images)) {
			foreach ($siteInfo['account_info']->Images as $key) {
				if($key->Type == "Logo") {
					$logo_url = $key->Uri;
					break;
				}
			}
		}

		//get agent photo
		if($siteInfo['branding']->agent_photo) {
			$agent_website_photo = getenv('AWS_S3_ASSETS')."uploads/photo/".$siteInfo['branding']->agent_photo;
		} else {
			if(isset($siteInfo['account_info']->Images) && !empty($siteInfo['account_info']->Images)) {
				foreach($siteInfo['account_info']->Images as $key) {
					if($key->Type=="Photo" || $key->Type == "Other Image" || $key->Type == "Logo") {
						$agent_website_photo = $key->Uri;
						break;
					}
				}
			}
		}

		$domain_info = $this->leads->get_agent_domain();

		if($domain_info) {
			$agent_domain = ($domain_info->status == "completed" || $domain_info->is_subdomain) ? ucfirst($domain_info->domains) : $domain_info->subdomain_url;
		}

		$buyer = $this->input->post("buyer");

		$subs = array(
			'buyers_name'			=> $buyer['fname'],
			'agent_name'			=> $agent_name,
			'logo_url' 				=> $logo_url,
			'agent_company'			=> $agent_org,
			'agent_phone'			=> $agent_num,
			'agent_email' 			=> $agent_email,
			'mls_name' 				=> $mls_name,
			'agent_site' 			=> ($domain_info->status == "completed" || $domain_info->is_subdomain) ? $domain_info->domains : $domain_info->subdomain_url,
			'agent_website_url' 	=> ($domain_info->status == "completed" || $domain_info->is_subdomain) ? 'http://'.$domain_info->domains : $domain_info->subdomain_url,
			'agent_website_photo' 	=> $agent_website_photo
		);

		$this->email->send_email_v3(
        	$agent_email, //sender email
        	$agent_name, //sender name
        	$buyer['email'], //recipient email
        	'paul@agentsquared.com,aldwinj12345@gmail.com', //bccs
        	'7a8155dc-be55-4b2d-863f-b77ac4e2b233', //"d03e23e4-698b-4d35-a558-d5276a13f80a", //template_id
        	$email_data = array('subs'=>$subs)
        );
		
	}
	
	public function respond_to_seller() {

		$this->load->library("email");

		$domain_info 	= $this->leads->get_agent_domain();
		$siteInfo 		= Modules::run('agent_sites/getInfo');

		$agent_name 	= (!empty($siteInfo['branding']->first_name) && !empty($siteInfo['branding']->last_name)) ? $siteInfo['branding']->first_name." ".$siteInfo['branding']->last_name : $siteInfo['account_info']->Name;
		$agent_org 		= (!empty($siteInfo['account_info']->Office)) ? $siteInfo['account_info']->Office : "";
		$agent_email 	= $siteInfo['branding']->email;
		$agent_num 		= (!empty($siteInfo['branding']->phone)) ? $siteInfo['branding']->phone : $siteInfo['account_info']->Phones[0]->Number;
		$mls_name 		= $siteInfo['account_info']->Mls;
		$agent_domain 	= "https://www.agentsquared.com/";
		$logo_url		= getenv('AWS_S3_ASSETS')."images/email-logo.png";
		$agent_website_photo = getenv('AWS_S3_ASSETS')."images/email-logo.png";

		//get agent_logo
		if(isset($siteInfo['branding']->logo) && !empty($siteInfo['branding']->logo)) {
			$logo_url = getenv('AWS_S3_ASSETS')."uploads/logo/".$siteInfo['branding']->logo;
		} elseif(isset($siteInfo['account_info']->Images) && !empty($siteInfo['account_info']->Images)) {
			foreach ($siteInfo['account_info']->Images as $key) {
				if($key->Type == "Logo") {
					$logo_url = $key->Uri;
					break;
				}
			}
		}

		//get agent photo
		if($siteInfo['branding']->agent_photo) {
			$agent_website_photo = getenv('AWS_S3_ASSETS')."uploads/photo/".$siteInfo['branding']->agent_photo;
		} else {
			foreach($siteInfo['account_info']->Images as $key) {
				if($key->Type=="Photo" || $key->Type == "Other Image" || $key->Type == "Logo") {
					$agent_website_photo = $key->Uri;
					break;
				}
			}
		}

		$seller = $this->input->post("seller");

		$subs = array(
			'sellers_name'		=> $seller['fname'],
			'agent_name'		=> $agent_name,
			'logo_url' 			=> $logo_url,
			'agent_company'		=> $agent_org,
			'agent_phone'		=> $agent_num,
			'agent_email' 		=> $agent_email,
			'mls_name' 			=> $agent_org,
			'agent_site' 		=> ($domain_info->status == "completed" || $domain_info->is_subdomain) ? $domain_info->domains : $domain_info->subdomain_url,
			'agent_website_url' => ($domain_info->status == "completed" || $domain_info->is_subdomain) ? 'http://'.$domain_info->domains : $domain_info->subdomain_url,
			'agent_website_photo' => $agent_website_photo
		);

		$this->email->send_email_v3(
        	$agent_email, //sender email
        	$agent_name, //sender name
        	$seller['email'], //recipient email
        	'paul@agentsquared.com,aldwinj12345@gmail.com', //bccs
        	'9dd1aa70-b515-44d6-a42a-62858107a56b', //"1bac4741-271e-4e37-a697-655f86295f76", //template_id
        	$email_data = array('subs'=>$subs)
        );
		
	}

	public function send_leads_notification_to_agent() {
        
        $this->load->library("email");

		$siteInfo = Modules::run('agent_sites/getInfo');
		$agent_name = $siteInfo['branding']->first_name." ".$siteInfo['branding']->last_name;
		$agent_email = $siteInfo['branding']->email;

		$lead_name = $this->input->post("fname").' '.$this->input->post("lname");
		$lead_email = $this->input->post("email");
		$lead_phone = $this->input->post("phone_signup");

		$subs = array(
			'first_name'	=> $agent_name,
			'customer_name' => $lead_name,
			'customer_email'=> $lead_email,
			'customer_phone'=> $lead_phone,
			'lead_page_url' => AGENT_DASHBOARD_URL.'crm/leads'
		);

		$this->email->send_email_v3(
        	'support@agentsquared.com', //sender email
        	'AgentSquared', //sender name
        	$agent_email, //recipient email
        	'paul@agentsquared.com,aldwinj12345@gmail.com', //bccs
        	"2b1f7a2f-9a23-4239-972b-2b23b012b09b", //"0a4b6f19-0010-4f0f-b999-ccddc3eee7bf", //template_id
        	$email_data = array('subs'=>$subs,'reply_to'=>$lead_email)
        );
		
    }

    public function send_leads_notification_to_agent_fb($infos = array()) {

		/*$siteInfo = Modules::run('agent_sites/getInfo');
		$agent_name = $siteInfo['branding']->first_name." ".$siteInfo['branding']->last_name;
		$agent_email = $siteInfo['branding']->email;

		$lead_name = $infos["name"];
		$lead_email = $infos["email"];

        $this->load->library("email");

       	$content = "Hi ".$agent_name."!<br><br>

                Congratulations, you have a new website lead!<br><br>

                Lead Name: ".$lead_name."<br>
                Lead Email Address: ".$lead_email."<br><br>

                To view your lead now, please login to your AgentSquared dashboard and go to CRM or use
                the link below:<br><br>

                <a href='".AGENT_DASHBOARD_URL."crm/leads' target='_blank'>".AGENT_DASHBOARD_URL."crm/leads</a><br><br/>

                Happy Selling!<br><br>

                Team AgentSquared<br>
                <a href='mailto:support@agentsquared.com'>support@agentsquared.com</a><br>
                <a href='http://www.agentsquared.com/' href='_blank'>www.AgentSquared.com</a><br>
            ";

        $subject ="New lead had been created";

        $this->email->send_email(
            'automail@agentsquared.com',
            'AgentSquared',
            $agent_email,
            $subject,
            'email/template/default-email-body',
            array(
                "message" => $content,
                "subject" => $subject,
                "to_bcc" => "honradokarljohn@gmail.com, joce@agentsquared.com"
            )
        );*/

        return TRUE;
    }

    public function send_seller_lead($data = array()) {

		$this->load->library("email");

    	$siteInfo = Modules::run('agent_sites/getInfo');
		$agent_name = $siteInfo['branding']->first_name." ".$siteInfo['branding']->last_name;
		$agent_email = $siteInfo['branding']->email;

		$seller = $this->input->post("seller");

		$subs = array(
			'first_name'		=> $agent_name,
			'field_firstname'	=> $seller['fname'],
			'field_lastname' 	=> $seller['lname'],
			'field_email'		=> $seller['email'],
			'field_phone'		=> $seller['phone'],
			'field_mobile'		=> $seller['mobile'],
			'field_address' 	=> $seller['address'],
			'field_zipcode' 	=> $seller['zip_code'],
			'field_bedrooms' 	=> $seller['bedrooms'],
			'field_bathrooms' 	=> $seller['bathrooms'],
			'field_area' 		=> $seller['square_feet']
		);

		$this->email->send_email_v3(
        	'support@agentsquared.com', //sender email
        	'AgentSquared', //sender name
        	$agent_email, //recipient email
        	'paul@agentsquared.com,aldwinj12345@gmail.com', //bccs
        	'261f0508-0e21-4a2a-9b19-f7c0885e9c42', //"a2756a8b-79de-4e86-8d51-992bb1c91280", //template_id
        	$email_data = array('subs'=>$subs,'reply_to'=>$seller['email'])
        );

    }

    public function send_buyer_lead($data = array()) {

    	$this->load->library("email");

    	$siteInfo = Modules::run('agent_sites/getInfo');
		$agent_name = $siteInfo['branding']->first_name." ".$siteInfo['branding']->last_name;
		$agent_email = $siteInfo['branding']->email;

		$buyer = $this->input->post("buyer");

		$subs = array(
			'first_name'		=> $agent_name,
			'field_name'		=> $buyer['fname']." ".$buyer['lname'],
			'field_email' 		=> $buyer['email'],
			'field_phone'		=> $buyer['phone'],
			'field_mobile'		=> $buyer['mobile'],
			'field_address' 	=> $buyer['address'],
			'field_city' 		=> $buyer['city'],
			'field_state' 		=> $buyer['state'],
			'field_zip' 		=> $buyer['zip_code'],
			'field_bed' 		=> $buyer['bedrooms'],
			'field_bath' 		=> $buyer['bathrooms'],
			'field_contact_by' 	=> $buyer['contact_by'],
			'field_price_range' => $buyer['price_range'],
			'field_when_move' 	=> $buyer['when_do_you_want_to_move'],
			'field_when_start' 	=> $buyer['when_did_you_start_looking'],
			'field_where_own' 	=> $buyer['wher_would_you_like_to_own'],
			'field_with_agent' 	=> $buyer['are_you_currently_with_an_agent'],
			'field_dream_home' 	=> $buyer['describe_your_dream_home']
		);

		$this->email->send_email_v3(
        	'support@agentsquared.com', //sender email
        	'AgentSquared', //sender name
        	$agent_email, //recipient email
        	'paul@agentsquared.com,aldwinj12345@gmail.com', //bccs
        	'08584c90-4f3b-406a-8a27-c7c426c25095', //"74dba1cd-0be2-459c-964c-bc15a6997eec", //template_id
        	$email_data = array('subs'=>$subs,'reply_to'=>$buyer['email'])
        );

    }

	public function send_login_portal_to_contacts( $datas = array() ){

		return TRUE;

		$this->load->library("email");

		$email = "rolbru12@gmail.com";
		$first_name = ( isset($datas["GivenName"]) && !empty($datas["GivenName"]) ) ? $datas["GivenName"] : $datas["DisplayName"];

		$content = "Hi ".$first_name.", <br><br>

			You have new message from AgentSqaured Team! <br><br>

			<a href='".base_url("/home/login")."' target = '_blank'> Read Message </a> <br><br>

			Regards, <br>

			Team AgentSquared
			";

	 	$subject ="New message from AgentSquared" ;

	 	return $this->email->send_email(
	 		'automail@agentsquared.com',
	 		'AgentSquared',
	 		$email,
	 		$subject,
	 		'email/template/default-email-body',
	 		array(
	 			"message" => $content,
	 			"subject" => $subject,
	 			"to_bcc" => "rolbru12@gmail.com"
	 		)
	 	);
	}

	public function get_message_contect_ajax( $message_id = NULL )
	{

		if( $this->input->is_ajax_request() )
		{
			$message = $this->leads->get_message_contect_ajax( $message_id );
			$response = array("success" => TRUE, "message" => nl2br($message->message) );
			exit(json_encode($response));
		}

		exit(json_encode( array("success" => FALSE)) );
	}

	public function add_notes( $contact_id = NULL)
	{
		if( $this->input->is_ajax_request() ) {

			if( empty($contact_id) ) return FALSE;

			$profile = $this->leads->get_profile_info( $contact_id );

			if( $_POST ){

				if( $id = $this->leads->add_notes( $contact_id ) )
				{
					//save to activtiy
					save_activities( $profile->id, $id, $_POST["notes"], "added notes", "agent", $contact_id );
					exit(json_encode(array("success" => TRUE)));
				}
				else
				{
					exit(json_encode(array("success" => FALSE)));
				}

			}
		}
	}

	public function add_tasks( $contact_id = NULL)
	{
		if( $this->input->is_ajax_request() ) {

			if( empty($contact_id) ) return FALSE;

			$profile = $this->leads->get_profile_info( $contact_id );

			if( $_POST ){

				if( $id = $this->leads->add_tasks( $contact_id ) )
				{
					//save to activtiy
					save_activities( $profile->id, $id, $_POST["tasks"], "added tasks", "agent", $contact_id );
					exit(json_encode(array("success" => TRUE)));
				}
				else
				{
					exit(json_encode(array("success" => FALSE)));
				}

			}
		}
	}

	public function  delete_notes( $id = NULL )
	{
		if( $this->input->is_ajax_request() ) {
			if( $id == NULL )
			{
				exit(json_encode(array("success" => FALSE)));
			}

			if( $this->leads->delete_notes($id) ){
				//save_activities( $profile->id, $id, "deleted notes", "agent", $contact_id );
				exit(json_encode(array("success" => TRUE)));
			}
			else
			{
				exit(json_encode(array("success" => FALSE)));
			}
		}
	}

	public function  delete_tasks( $id = NULL )
	{
		if( $this->input->is_ajax_request() ) {
			if( $id == NULL )
			{
				exit(json_encode(array("success" => FALSE)));
			}

			if( $this->leads->delete_tasks($id) ){
				//save_activities( $profile->id, $id, "deleted notes", "agent", $contact_id );
				exit(json_encode(array("success" => TRUE)));
			}
			else
			{
				exit(json_encode(array("success" => FALSE)));
			}
		}
	}

	public function export_contacts( $status = "" )
	{
		$export = "";
		$param["status"] = $status;

		$contacts = $this->leads->get_contacts_lists(FALSE, "*", $param);

		if( !empty( $contacts ) )
		{
			//printA( $data["agents"] ); exit;
			foreach($contacts as $result){

		        $fname = $result->first_name;
		        $lname = $result->last_name;
		        $email = $result->email;
				$contact_id = $result->contact_id;
				$agent_id = $result->agent_id;
				$phone = $result->phone;
				$address = $result->address;
				$mobile = $result->mobile;
				$city = $result->city;
				$state = $result->state;
				$zipcode = $result->zipcode;
				$type = $result->type;
				$modified = $result->modified;

		        $export.= $fname.",".$lname.",".$email.",".$contact_id.",".$agent_id.",".$phone.",".$address.",".$mobile.",".$city.",".$state.",".$zipcode.",".$type.",".$modified." \n";

		    }

			$file_name="Contact Lists";
			$header = "first_name".","."last_name".","."email".","."contact_id".","."agent_id".","."phone".","."address".","."mobile".","."city".","."state".","."zipcode".","."type".","."modified"." \n";

			header("Content-type: application/vnd.ms-excel");
		    header("Content-disposition: csv" . date("Y-m-d") . ".csv");
		    header("Content-disposition: filename=".$file_name.".csv");
		    print "$header\n$export";

		    exit;
		}

		redirect(base_url()."crm/leads");
	}

	public function import_contacts()
	{
		if( $_FILES )
		{
			$this->load->library('csvimport');

			$config['allowed_types'] = 'csv';
	        $config['max_size'] = '1000';

	 		if( $_SERVER["SERVER_NAME"] == "localhost")
			{
				$config['upload_path'] = FCPATH.'assets/upload/contacts';
				$csvPath= FCPATH.'assets/upload/contacts';
			}
			else
			{
				$config['upload_path'] = $_SERVER["DOCUMENT_ROOT"].'/agent-site/assets/upload/contacts';
				$csvPath = $_SERVER["DOCUMENT_ROOT"].'/agent-site/assets/upload/contacts';
			}

	        $this->load->library('upload', $config);

	        // If upload failed, display error
	        if (!$this->upload->do_upload()) {
	        	printA( $this->upload->display_errors() ); exit;
	            $data['error'] = $this->upload->display_errors();

	        } else {

	            $file_data = $this->upload->data();
	            $file_path =  $csvPath.'/'.$file_data['file_name'];

	            if ($this->csvimport->get_array($file_path)) {

	                $csv_array = $this->csvimport->get_array($file_path);
	             	//printA( $csv_array ); exit;

	             	if( $this->domain_user->agent_id == '20160617145903922931000000')
	             	{
	             		foreach ($csv_array as $row) {

	             			$contact_id1 = str_replace("{", "", $row['Contact ID'] );
	                		$contact_id2 = str_replace("}", "", $contact_id1 );

		                	$password = "agentsquared1234";

							$insert_data = array(
		                        'first_name'=> (isset($row['first_name']) ? $row['first_name'] : (isset($row['GivenName']) ? $row['GivenName'] : $row['Primary FirstName'] )),
		                        'last_name'=> (isset($row['last_name']) ? $row['last_name'] : (isset($row['FamilyName']) ? $row['FamilyName'] : $row['Primary LastName'])),
		                        'email'=> (isset($row['email']) ? $row['email'] : (isset($row['PrimaryEmail']) ? $row['PrimaryEmail'] : $row['Email Address'])),
		                        'phone'=> (isset($row['phone']) ? $row['phone'] : (isset($row['PrimaryPhoneNumber']) ? $row['PrimaryPhoneNumber'] : $row['Home Phone'])),
		                        'address'=> (isset($row['address']) && !empty($row['address'])) ? $row['address'] : $row['Street'],
		                        'mobile'=> (isset($row['mobile']) && !empty($row['mobile'])) ? $row['mobile'] : $row['Mobile Phone'],
		                        'city'=> (isset($row['city']) && !empty($row['city'])) ? $row['city'] : $row['City'],
		                        'state'=> (isset($row['state']) && !empty($row['state'])) ? $row['state'] :  $row['State'],
		                        'zipcode'=> (isset($row['zipcode']) && !empty($row['zipcode'])) ? $row['zipcode'] : $row['Zip'],
		                        'type'=> $row['Contact Type'],
		                        'agent_id'=> $this->domain_user->agent_id,
		                        'contact_id' => (isset($row['Id']) && !empty($row['Id'])) ? $row['Id'] : $contact_id2,
		                        "ip_address" => $_SERVER['REMOTE_ADDR'],
						        "password" => password_hash( $password, PASSWORD_DEFAULT),
						        "active" => "1",
						        "created" => date("Y-m-d h:i:sa"),
						        "modified" => ( isset($row["ModificationTimestamp"]) && !empty($row["ModificationTimestamp"])) ? $row["ModificationTimestamp"] : 0,
						        "user_id" => $this->domain_user->id
		                    );

		                    $this->leads->insert_csv($insert_data);

		                }
	             	}
	                else
	                {
	                	foreach ($csv_array as $row) {


		                	$password = "agentsquared1234";

							$insert_data = array(
		                        'first_name'=> (isset($row['first_name']) ? $row['first_name'] : (isset($row['GivenName']) ? $row['GivenName'] : "Not defined")),
		                        'last_name'=> (isset($row['last_name']) ? $row['last_name'] : (isset($row['FamilyName']) ? $row['FamilyName'] : "Not defined")),
		                        'email'=> (isset($row['email']) ? $row['email'] : (isset($row['PrimaryEmail']) ? $row['PrimaryEmail'] : "Not defined")),
		                        'phone'=> (isset($row['phone']) ? $row['phone'] : (isset($row['PrimaryPhoneNumber']) ? $row['PrimaryPhoneNumber'] : "Not defined")),
		                        'address'=> (isset($row['address']) && !empty($row['address'])) ? $row['address'] : "Not defined",
		                        'mobile'=> (isset($row['mobile']) && !empty($row['mobile'])) ? $row['mobile'] : "Not defined",
		                        'city'=> (isset($row['city']) && !empty($row['city'])) ? $row['city'] : "Not defined",
		                        'state'=> (isset($row['state']) && !empty($row['state'])) ? $row['state'] : "Not defined",
		                        'zipcode'=> (isset($row['zipcode']) && !empty($row['zipcode'])) ? $row['zipcode'] : "Not defined",
		                        'type'=> "flex",
		                        'agent_id'=> $this->domain_user->agent_id,
		                        'contact_id' => (isset($row['Id']) && !empty($row['Id'])) ? $row['Id'] : "Not defined",
		                        "ip_address" => $_SERVER['REMOTE_ADDR'],
						        "password" => password_hash( $password, PASSWORD_DEFAULT),
						        "active" => "1",
						        "created" => date("Y-m-d h:i:sa"),
						        "modified" => ( isset($row["ModificationTimestamp"]) && !empty($row["ModificationTimestamp"])) ? $row["ModificationTimestamp"] : 0,
						        "user_id" => $this->domain_user->id
		                    );

		                    $this->leads->insert_csv($insert_data);

		                }
	                }

	                $this->session->set_flashdata('session' , array('status' => TRUE,'message' => 'Csv Data Imported Succesfully!'));

	                redirect(base_url()."crm/leads");
	                //echo "<pre>"; print_r($insert_data);
	            } else {
	                $data['error'] = "Error occured";
	                $this->session->set_flashdata('session' , array('status' => FALSE,'message' => 'Error occured!'));
	                redirect(base_url()."crm/leads");
	            }
	     	}

		}

	}

	public function AddSavedSearches()
	{
		$datas = array(
				"AutoName" => 1,
				"QuickSearchId" => "false",
				"Name" => "Test Save Searches",
				"Filter" => "City Eq 'My City'",
				"Description" => "An optional longer description",
				"Tags" => "Favorites"
			);

		$save_me = $this->api->AddSavedSearches( "20130322023747504655000000", $datas );

		printA($save_me); exit;
	}

	public function check_capture_leads() {
		$switch = $this->leads->get_capture_leads($this->domain_user->agent_id);
		exit(json_encode(array('capture_leads' => $switch->capture_leads, 'is_email_market' => $switch->is_email_market)));
	}

	public function set_lead_capture() {

		if($this->input->post('switch_post')) {
			$post = $this->input->post('switch_post');
			if($post == 'capture_leads') {
				$data = array('capture_leads'=> 1);
			} else {
				$data = array('capture_leads'=> 0);
			}
			$this->leads->set_capture_leads($data);
		}

	}

	public function set_email_market() {

		if($this->input->post('switch_post')) {
			$post = $this->input->post('switch_post');
			if($post == 'set_email') {
				$data = array('is_email_market'=> 1);
			} else {
				$data = array('is_email_market'=> 0);
			}
			$this->leads->set_capture_leads($data);
		}

	}

	public function delete_leads( $lead_id = 0 )
	{
		if( $this->input->is_ajax_request() )
		{
			$return = array("status" => FALSE );

			if( $this->leads->delete_lead($lead_id) )
			{
				$return = array("status" => TRUE, "lead_id" => $lead_id );
			}

			exit( json_encode($return));
		}
	}
}
