<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customer_leads extends IDX_Auth {

	function __construct() {
		parent::__construct();

		if(!$this->session->userdata('logged_in_auth'))
			redirect('login/auth/login', 'refresh');

		$this->load->model("Crm_model", "crm");
		$this->load->model("agent_sites/Agent_sites_model");
	}

	public function index() {
		//printA( $this->session->userdata ); exit;
		$data['title'] = "Customer Leads";

		$param = array();
		$param["limit"] = 5;

		$data["customers"] = $this->crm->get_customers_info(FALSE, $param);
		//printA($data["customers"]); exit;
		$data["searches_properties"] = $this->crm->get_save_search_properties(FALSE, $param);
		$favorates_properties = $this->crm->get_save_favorates_properties(FALSE, $param);
		//printA($favorates_properties); exit;
		if( !empty($favorates_properties) )
		{
			foreach ($favorates_properties as &$properties) {
				$properties["properties_details"] = $this->api->GetListing( $properties["property_id"] );
			}
		}
		
		//printA($favorates_properties); exit;
		$data["favorates_properties"] = $favorates_properties;
		$data['js'] = array("/buyer-data-load.js");
		$siteInfo = Modules::run('agent_sites/getInfo');
		$data['branding'] = $siteInfo['branding'];
		$data["is_reserved_domain"] = $this->Agent_sites_model->is_reserved_domain();
		$data["domain_info"] = $this->Agent_sites_model->fetch_domain_info();
		$this->load->view('customer_leads', $data);
	}

	public function customers()
	{
		//printA( $this->session->userdata ); exit;
		$this->load->library('pagination');

		$data['title'] = "CRM - Customers";

		$param = $this->input->get();
		$param["offset"] = (isset($_GET["offset"])) ? $_GET["offset"] : 0;
		$param["limit"] = 25;

		$total = $this->crm->get_customers_info(TRUE, $param);
		$data["customers"] = $this->crm->get_customers_info(FALSE, $param);
		$data["total"] = $total;

		$config["base_url"] = base_url().'crm/customer_leads/customers';
		$config["total_rows"] = $total;
		$config["per_page"] = $param["limit"];
		$config['page_query_string'] = TRUE;
		$config['query_string_segment'] = 'offset';

		$siteInfo = Modules::run('agent_sites/getInfo');
		$data['branding'] = $siteInfo['branding'];

		$this->pagination->initialize($config);

		$data["pagination"] = $this->pagination->create_links();

		$this->load->view('customer-table', $data);
	}

	public function saved_searches()
	{
		//printA( $this->session->userdata ); exit;
		$this->load->library('pagination');

		$data['title'] = "CRM - Saved Searches";

		$param = $this->input->get();
		$param["offset"] = (isset($_GET["offset"])) ? $_GET["offset"] : 0;
		$param["limit"] = 25;

		$total = $this->crm->get_save_search_properties(TRUE, $param);
		$data["searches_properties"] = $this->crm->get_save_search_properties(FALSE, $param);
		$data["total"] = $total;

		$config["base_url"] = base_url().'crm/customer_leads/saved_searches';
		$config["total_rows"] = $total;
		$config["per_page"] = $param["limit"];
		$config['page_query_string'] = TRUE;
		$config['query_string_segment'] = 'offset';

		$siteInfo = Modules::run('agent_sites/getInfo');
		$data['branding'] = $siteInfo['branding'];

		$this->pagination->initialize($config);

		$data["pagination"] = $this->pagination->create_links();

		$this->load->view('saved-searches-table', $data);
	}

	public function saved_favorates()
	{
		//printA( $this->session->userdata ); exit;
		$this->load->library('pagination');

		$data['title'] = "CRM - Saved Favorites";

		$param = $this->input->get();
		$param["offset"] = (isset($_GET["offset"])) ? $_GET["offset"] : 0;
		$param["limit"] = 25;

		$total = $this->crm->get_save_favorates_properties(TRUE, $param);		
		$data["total"] = $total;

		$favorates_properties = $this->crm->get_save_favorates_properties(FALSE, $param);
		//printA($favorates_properties); exit;
		if( !empty($favorates_properties) )
		{
			foreach ($favorates_properties as &$properties) {
				$properties["properties_details"] = $this->api->GetListing( $properties["property_id"] );
			}
		}
		
		//printA($favorates_properties); exit;
		$data["favorates_properties"] = $favorates_properties;

		$config["base_url"] = base_url().'crm/customer_leads/saved_favorates';
		$config["total_rows"] = $total;
		$config["per_page"] = $param["limit"];
		$config['page_query_string'] = TRUE;
		$config['query_string_segment'] = 'offset';

		$siteInfo = Modules::run('agent_sites/getInfo');
		$data['branding'] = $siteInfo['branding'];

		$this->pagination->initialize($config);

		$data["pagination"] = $this->pagination->create_links();

		$this->load->view('saved-favorites-table', $data);
	}

}