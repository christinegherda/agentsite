<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Robots extends MX_Controller {
  private $agent_id = '';
  private $user_id = '';
  protected $domain_user;
  
  function __construct(){
    parent::__construct();
    
    $this->load->model('robots/robots_model', 'robots');
    $this->domain_user = domain_to_user();
    $this->agent_id = $this->domain_user->agent_id;
  }
  
  public function index(){
    $result = $this->robots->get($this->agent_id);
    
    header('Content-Type: text/plain');
    
    if($result && $result->robots !== ''){
      print($result->robots);
    }else{
      echo "User-agent: *" . "\r\n";
      echo "Disallow: /.git" . "\r\n";
      //echo "Disallow: /other-property-details/" . "\r\n";
      echo "Disallow: /listings" . "\r\n";
      echo "Disallow: /signup" . "\r\n";
      echo "Disallow: /login" . "\r\n";
      echo "Disallow: /property-details/signup" . "\r\n";
      echo "Disallow: /search-results" . "\r\n";
      //echo "Disallow: /nearby_listings" . "\r\n";
      
      /* echo "Disallow: /home/login" . "\r\n";
      echo "Disallow: /home/sold_property" . "\r\n";
      echo "Disallow: /home/all_saved_searches" . "\r\n";
      echo "Disallow: /home/signup" . "\r\n";
      echo "Disallow: /home/home/save_search_properties" . "\r\n";
      echo "Disallow: /home/saved_searches" . "\r\n";
      echo "Disallow: /home/home/save_favorates_properties" . "\r\n";
      echo "Disallow: /home/listings" . "\r\n";
      echo "Disallow: //home/saved_searches/signup" . "\r\n";
      echo "Disallow: //home/saved_searches" . "\r\n\r\n"; */
    }
    echo "Sitemap: ".base_url()."sitemap.xml";
  }
}
?>
