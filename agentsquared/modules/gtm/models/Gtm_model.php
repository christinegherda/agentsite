<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gtm_model extends CI_Model{ 

  function __construct() {
    parent:: __construct();

     $this->db_slave =  $this->load->database('db_slave', TRUE);
  }
	
	function checkTable(){
		if($this->db_slave->table_exists('users_ga')){
			return true;
		}
		return false;
	}
	
	function get($agent_id = '', $domain = ''){
		if($agent_id === ''){
			return false;
		}
    
		$this->db_slave->where('agent_id', $agent_id);
    if($domain != ''){
      $this->db_slave->where('domain', $domain);
    }
		$query = $this->db_slave->get('users_ga')->row();
		if( $query ){
			return $query;
		}
		return false;
	}

}

