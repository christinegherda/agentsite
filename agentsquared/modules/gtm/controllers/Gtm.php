<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gtm extends MX_Controller {
  
  function __construct(){
    parent::__construct();
    
    $this->load->model('gtm/gtm_model', 'gtm');
    
  }
  
  public function getGTMid(){
    
		if($this->gtm->checkTable()){
			$profile = $this->getClientData();
      
      if($profile){
        if(isset($profile->gtm_id) && property_exists($profile, 'gtm_id')){
          return $profile->gtm_id;
        }
      }
		}
    return false;
	}
  
  private function getClientData(){
    $domain_user = domain_to_user();
		$query = $this->gtm->get($domain_user->agent_id);
		if($query){
			return $query;
		}
		return false;
	}
}
?>
