<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['home'] = 'home';
$route['find-a-home'] = 'home/find_a_home';
$route['contact'] = 'home/contact';
$route['about'] = 'home/about';
$route['buyer'] = 'home/buyer';
$route['seller'] = 'home/seller';
$route['testimonials'] = 'home/testimonials';
$route['blog'] = 'home/blog';
$route['tx-real-estate-commission-consumer-protection-notice'] = 'home/tx_reccp_notice';
$route['texas-real-estate-commission-information-about-brokerage-services'] = 'home/tx_reciab_services';

$route['page/(:any)'] = 'home/page/$1';
$route['post/(:any)'] = 'home/post/$1';

$route['archives/(:any)'] = 'home/archives/$1';
$route['active_listings'] = 'home/active_listings';
$route['nearby_listings'] = 'home/nearby_listings';
$route['new_listings'] = 'home/new_listings';
$route['new_listings_search'] = 'home/new_listings_search';
$route['office_listings'] = 'home/office_listings';
$route['saved_searches/(:num)'] = 'home/saved_searches/$1';
$route['all_saved_searches'] = 'home/all_saved_searches';
$route['sold_listings'] = 'home/sold_listings';
$route['ajax_sold_listings'] = 'home/ajax_sold_listings';

$route['property-details/(:num)'] = 'home/property/details/_/$1';
$route['property-details/(:any)/(:num)'] = 'home/property/details/$1/$2';
$route['other-property-details/(:num)'] = 'home/property/other_property_details/$1';

$route['home/customer-dashboard'] = 'home/customer_dashboard';
$route['customer/customer-dashboard'] = 'home/customer/customer_dashboard';
$route['customer-dashboard'] = 'home/customer/customer_dashboard';
$route['login'] = 'home/login';
$route['signup'] = 'home/signup';
$route['logout'] = 'home/logout';
$route['customer/change_password'] = 'home/customer/change_password';
$route['customer/request_information'] = 'home/customer/request_information';
$route['customer/delete_customer_property/(:num)'] = 'home/customer/delete_customer_property/(:num)';

// Advance Search
$route['advanced-search'] = 'home/advance_search';
$route['mapped-search'] = 'home/mapped_search';

$route['about/sitemap'] = 'home/sitemap';
$route['privacy-policy'] = 'home/privacy_policy';
$route['(:any).html'] = 'home/gsc/$1';

//routes for new search results
$route['search-results'] = 'home/mapped_search';
$route['searched-results'] = 'home/mapped_search';
$route['advanced-search-results'] = 'home/mapped_search';

//SPW
$route['property-site/(:any)'] = 'spw/base/app/$1';

//Debugger 
$route['debugger'] = 'home/debugger';
$route['debugger/new_listings_debugger'] = 'home/debugger/new_listings_debugger';
$route['debugger/savedsearch_debugger_dsl_proxy'] = 'home/debugger/savedsearch_debugger_dsl_proxy';
$route['debugger/savedsearch_debugger_spark/(:any)'] = 'home/debugger/savedsearch_debugger_spark/$1';
$route['debugger/listings_debugger_spark/(:any)/(:num)'] = 'home/debugger/listings_debugger_spark/$1/$2';
$route['debugger/active_listings_debugger_spark/(:any)/(:num)'] = 'home/debugger/active_listings_debugger_spark/$1/$2';
$route['debugger/homepage_debugger'] = 'home/debugger/homepage_debugger';
$route['debugger/school_debugger/(:any)'] = 'home/debugger/school_debugger/$1';
$route['debugger/city_debugger'] = 'home/debugger/city_debugger';
$route['debugger/lost_token_debugger'] = 'home/debugger/lost_token_debugger';
$route['debugger/dsl_proxy_debugger'] = 'home/debugger/dsl_proxy_debugger';
$route['debugger/dsl_listings_debugger'] = 'home/debugger/dsl_listings_debugger';
$route['debugger/dsl_saved_search_debugger'] = 'home/debugger/dsl_saved_search_debugger';
$route['debugger/office_listings_proxy'] = 'home/debugger/office_listings_proxy';
$route['debugger/latlon_debugger'] = 'home/debugger/latlon_debugger';
$route['debugger/print_cookies'] = 'home/debugger/print_cookies';
$route['debugger/listingsFilterById/(:num)'] = 'home/debugger/listingsFilterById/$1';
$route['debugger/savedSearchFilterById/(:num)/(:num)'] = 'home/debugger/savedSearchFilterById/$1/$2';
$route['debugger/savedSearchFilterDebugger/(:num)/(:any)/(:num)'] = 'home/debugger/savedSearchFilterDebugger/$1/$2/$3';
$route['debugger/previousOfficeSoldListingsSpark/(:num)'] = 'home/debugger/getPreviousOfficeSoldListings/$1';
$route['debugger/currentOfficeSoldListingsSpark/(:num)'] = 'home/debugger/getCurrentOfficeSoldListings/$1';
$route['debugger/soldListingsDebugger'] = 'home/debugger/soldListingsDebugger';


