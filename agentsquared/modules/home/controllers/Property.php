<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Property extends MX_Controller {

	protected $domain_user;

	function __construct() {
 		parent::__construct();

 		$this->domain_user = domain_to_user();
 		$this->load->model("customer_model", "customer");
 		$this->load->model("home_model");
 		$this->load->model("Page_model","page");
    	$this->load->model("Meta_model");
 		$this->load->model("dsl/dsl_model");
 		$this->load->model("agent_sites/Agent_sites_model");
 	}

 	public function details($address='', $id=NULL) {

 		if($id) {

 			$dsl = modules::load('dsl/dsl');
 			$obj = Modules::load("mysql_properties/mysql_properties");

 			$user_info = $this->home_model->getUsersInfo($this->domain_user->id);
 			$data["theme"] 	= $user_info->theme;
			$data["header"] = "header-page";
			$data["page"] 	= "property";

		    // SEO meta title and meta description  - jovanni
		    $page_meta = $this->Meta_model->get_meta($this->domain_user->agent_id, 'property-details/' . $id);
		    $data['canonical'] = (isset($page_meta['canonical']) && $page_meta['canonical'] != '')? $page_meta['canonical'] : '';
		    // ===================

			$data['default_menu_data'] = Modules::run('menu/get_default_menu_data');
			$data['custom_menu_data'] = Modules::run('menu/get_custom_menu_data');
			$data['account_info'] =  $obj->get_account($this->domain_user->id, "account_info");
			$SiteInfo = Modules::run('agent_sites/getInfo');
			$data['branding'] = $SiteInfo['branding'];
			$data['system_info'] = $obj->get_account($this->domain_user->id, "system_info");
			$data['mlsLogo'] = isset($data['system_info']->Configuration[0]->MlsLogos[0]->Uri) ? $data['system_info']->Configuration[0]->MlsLogos[0]->Uri : "" ;
			$data['mlsName'] = isset($data['system_info']->Mls) ? $data['system_info']->Mls : "";
			$data['socialmedia_link'] = Modules::run('agent_social_media/agent_social_media/linksInfo');
			$data['property_id'] = $id;
			$data["user_info"] = $user_info;

			$standard_info = $dsl->get_endpoint("property/".$id."/standard-fields/".$this->domain_user->id);
			$data['property_standard_info'] = json_decode($standard_info,true);
			$data['property_name'] = (isset($data['property_standard_info']['data']['UnparsedAddress'])) ? $data['property_standard_info']['data']['UnparsedAddress'] : '';

			$latitude = ($data['property_standard_info']['data']['Latitude']) ? $data['property_standard_info']['data']['Latitude']  : "";
	    $longitude = ($data['property_standard_info']['data']['Longitude']) ? $data['property_standard_info']['data']['Longitude'] :"";

      if(($latitude != "********" && $longitude != "********") || ($latitude != "" && $longitude != "")) {
      		$latitude = $data['property_standard_info']['data']['Latitude'];
      		$longitude = $data['property_standard_info']['data']['Longitude'];
      } else {
	      	$coordinates = Modules::run('home/getlatlon', $data['property_standard_info']['data']['UnparsedAddress']);

	      	$latitude = ($coordinates["lat"]) ? $coordinates["lat"] : "";
	      	$longitude = ($coordinates["lon"]) ? $coordinates["lon"] : "";
      }

	    $data['lat'] = $latitude;
	    $data['lon'] = $longitude;

			/*$property_details = $dsl->get_endpoint("property/".$id."/custom-fields/".$this->domain_user->id);
 			$data['property_full_spec'] = json_decode($property_details, true);*/

 			$token = $this->home_model->getUserAccessToken($this->domain_user->agent_id);
 			$type = $this->session->userdata('auth_type');

			if($type == 0) {
				$body = array('access_token' => $token->access_token);
			} else {
				$body = array('bearer_token' => $token->access_token);
			}

 			$data['property_full_spec'] = $this->get_property_custom_field($id);
			$data['photos'] = isset($data['property_standard_info']['data']['Photos']) ? $data['property_standard_info']['data']['Photos'][0]['Uri1280'] : "";

      		$data["as_ga"] = Modules::run('as_ga/getTrackingCode');

      		/*$domain_info = $this->Agent_sites_model->fetch_domain_info();
      		$is_reserved_domain = $this->Agent_sites_model->is_reserved_domain();

	    	if(!empty($domain_info)){
	      		if($domain_info[0]->status == "completed" || $domain_info[0]->is_subdomain == "1") {
	        		$base = "http://";
	        		$base_url = $base.$is_reserved_domain['domain_name'].'/';
	      		} elseif($domain_info[0]->status != "completed" && $domain_info[0]->is_subdomain == "1") {
	        		$base = "http://";
	        		$base_url = $base.$is_reserved_domain['domain_name'].'/';
	      		} else {
	        	$base_url = base_url();
	      		}
	    	} else {
	      		$base_url = base_url();
	    	}

      		//redirect to homepage if property does not exist
			if(empty($data['property_standard_info']['data'])){
				redirect($base_url,'location',301);
			}*/

			$data["isMyProperty"] = TRUE;
			$data["js"] = array("customer.js","listings.js");
			$data["protocol"] = Modules::run('home/protocol');
			$data["is_capture_leads"] = $this->home_model->get_capture_leads()->capture_leads;
			$data["profile"] = $this->customer->get_customer_profile();

			$data = add_filter('before-property-view', $data);

 			$this->load->view("page", $data);
 		}
 	}

 	public function other_property_details($id=NULL) {

 		if($id) {

 			$dsl = modules::load('dsl/dsl');

 			$token = $this->home_model->getUserAccessToken($this->domain_user->agent_id);
			$type = $this->session->userdata('auth_type');

			if($type == 0) {
				$body = array(
					'access_token' => $token->access_token
				);
			}
			else {
				$body = array(
					'bearer_token' => $token->access_token
				);
			}

 			$user_info = $this->home_model->getUsersInfo($this->domain_user->id);
 			$data["theme"] 	= $user_info->theme;
			$data["header"] = "header-page";
			$data["page"] 	= "property";

		    // SEO meta title and meta description  - jovanni
		    $page_meta = $this->Meta_model->get_meta($this->domain_user->agent_id, 'other-property-details/' . $id);
		    $data['canonical'] = (isset($page_meta['canonical']) && $page_meta['canonical'] != '')? $page_meta['canonical'] : '';
		    // ===================

			$obj = Modules::load("mysql_properties/mysql_properties");

			$data['default_menu_data'] = Modules::run('menu/get_default_menu_data');
	      	$data['custom_menu_data'] = Modules::run('menu/get_custom_menu_data');
	      	$data['account_info'] = $obj->get_account($this->domain_user->id, "account_info");
	      	$SiteInfo = Modules::run('agent_sites/getInfo');
	      	$data['branding'] = $SiteInfo['branding'];
			$data['system_info'] = $obj->get_account($this->domain_user->id, "system_info");
			$data['mlsLogo'] = isset($data['system_info']->Configuration[0]->MlsLogos[0]->Uri) ? $data['system_info']->Configuration[0]->MlsLogos[0]->Uri : "" ;
			$data['mlsName'] = isset($data['system_info']->Mls) ? $data['system_info']->Mls : "";
			$data['socialmedia_link'] = Modules::run('agent_social_media/agent_social_media/linksInfo');
			$data['property_id'] = $id;
			$data["user_info"] = $user_info;

			$standard_info = $dsl->post_endpoint('spark-listing/'.$id.'/standard-fields?_expand=PrimaryPhoto,Supplement', $body);
			$data['property_standard_info'] = json_decode($standard_info, true);
			$latitude = ($data['property_standard_info']['data']['Latitude']) ? $data['property_standard_info']['data']['Latitude']  : "";
      		$longitude = ($data['property_standard_info']['data']['Longitude']) ? $data['property_standard_info']['data']['Longitude'] :"";

		    if($latitude != "********" && $longitude != "********" && $latitude != "" && $longitude != "") {
		        $latitude = $data['property_standard_info']['data']['Latitude'];
		        $longitude = $data['property_standard_info']['data']['Longitude'];
		    } else {
				/**
				 * Note: Temp. Disabled this google api call
				 * To minimize the Google Geolocation API calls
				 *
				 *  $coordinates = Modules::run('home/getlatlon', $data['property_standard_info']['data']['UnparsedAddress']);
		         *  $latitude = ($coordinates["lat"]) ? $coordinates["lat"] : "";
		         *  $longitude = ($coordinates["lon"]) ? $coordinates["lon"] : "";
				 */
		    }

      		$data['lat'] = $latitude;
      		$data['lon'] = $longitude;

			$data['property_full_spec'] = $this->get_property_custom_field($id);
			$data['photos'] = isset($data['property_standard_info']['data']['Photos']) ? $data['property_standard_info']['data']['Photos'][0]['Uri1280'] : "";

			$data["isMyProperty"] = FALSE;
	      	$data["as_ga"] = Modules::run('as_ga/getTrackingCode');
	      	$data["js"] = array("customer.js","listings.js");
	      	$data["protocol"] = Modules::run('home/protocol');
	      	$data["is_capture_leads"] = $this->home_model->get_capture_leads()->capture_leads;
			$data["profile"] = $this->customer->get_customer_profile();

			$data = add_filter('before-other-property-view', $data);

 			$this->load->view("page", $data);
 		}
 	}

 	public function get_property_custom_field($id=NULL) {

 		$ret=array();

 		if($id) {

			$token = $this->home_model->getUserAccessToken($this->domain_user->agent_id);

			if($token) {

	 			$url = "https://sparkapi.com/v1/listings/".$id."?_select=CustomFields&_expand=CustomFields";

				$headers = array(
					'Authorization:OAuth '.$token->access_token,
					'Accept:application/json',
					'X-SparkApi-User-Agent:MyApplication',
				);

				$ch = curl_init();
				curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_URL, $url);

				$result = curl_exec($ch);
				$result_info = json_decode($result, true);

                curl_close($ch);
				if(isset($result_info["D"]["Results"]) && !empty($result_info["D"]["Results"])) {
					$ret["data"] = $result_info["D"]["Results"][0]["CustomFields"][0];
				}
			}
 		}

 		return $ret;
 	}

 	public function property_photos( $id= NULL) {

 		$user_info = $this->home_model->getUsersInfo($this->domain_user->id);
		$theme = $user_info->theme;
		$dsl = modules::load('dsl/dsl');
		$standard_info = $dsl->get_endpoint("property/".$id."/standard-fields/".$this->domain_user->id);
		$data['property_standard_info'] = json_decode($standard_info,true);

 		if($this->uri->segment(1) == "property-details"){

 			$endpoints = "property/".$id."/photos/".$this->domain_user->id;
 			$data['photos'] = Modules::run('dsl/get_endpoint', $endpoints);

 		} else {

 			$dsl = modules::load('dsl/dsl');

 			$token = $this->home_model->getUserAccessToken($this->domain_user->agent_id);
			$type = $this->session->userdata('auth_type');

			if($type == 0) {
				$body = array(
					'access_token' => $token->access_token
				);
			}
			else {
				$body = array(
					'bearer_token' => $token->access_token
				);
			}

			$data['photos'] = $dsl->post_endpoint('spark-listing/'.$id.'/photos?_select=Photos.Uri640,Photos.Name', $body);

 		}

		$this->load->view($theme.'/pages/property_view/photo-gallery',$data);

	}

	public function property_nearby( $id= NULL) {

 		$user_info = $this->home_model->getUsersInfo($this->domain_user->id);
		$theme = $user_info->theme;

		$limit = 8;
		$selectFields = "UnparsedAddress,CurrentPrice,StateOrProvince,UnparsedFirstLineAddress,City,PostalCity,PostalCode,ListingKey,PropertyClass,Photos.Uri300";

 		if($this->uri->segment(1) == "property-details"){

 			$endpoints = "property/".$id."/standard-fields/".$this->domain_user->id;

 			$standard_info = Modules::run('dsl/get_endpoint', $endpoints);
 			$property_standard_info = json_decode($standard_info,true);

			$latitude = ($property_standard_info["data"]["Latitude"]) ? $property_standard_info["data"]["Latitude"]  : "";
			$longitude = ($property_standard_info["data"]["Longitude"]) ? $property_standard_info["data"]["Longitude"] :"";
			$latlon = Modules::run('home/getlatlon', $property_standard_info["data"]["UnparsedAddress"]);

				if($latitude != "********" && $longitude != "********"){

					$lat = ($property_standard_info["data"]["Latitude"]) ? $property_standard_info["data"]["Latitude"]  : "";
					$lon = ($property_standard_info["data"]["Longitude"]) ? $property_standard_info["data"]["Longitude"] :"";

				} elseif(!empty($latlon)) {

					$lat = ($latlon["lat"]) ? $latlon["lat"] : "";
					$lon = ($latlon["lon"]) ? $latlon["lon"] : "";

				} else {

					$lat = "";
					$lon = "";
				}

			$mlsStatusStr = Modules::run('search/mlsStatusFilterString');
			$filterStr = "(".$mlsStatusStr.")";
			$filter_url = rawurlencode($filterStr);
			$filter_string = substr($filter_url, 3, -3);

			$nearby_listings = $dsl->post_endpoint('spark-nearby-listings?_expand=PrimaryPhoto&_limit='.$limit.'&_select='.$selectFields.'&_filter=('.$filter_string.')&_lat='.$lat.'&_lon='.$lon.'', $body);

 			if( !empty($lat) && !empty($lon)){
				$data['nearby_listings'] = json_decode($nearby_listings, true);
			}


 		} else {

 			$dsl = modules::load('dsl/dsl');

 			$token = $this->home_model->getUserAccessToken($this->domain_user->agent_id);
			$type = $this->session->userdata('auth_type');

			if($type == 0) {
				$body = array(
					'access_token' => $token->access_token
				);
			}
			else {
				$body = array(
					'bearer_token' => $token->access_token
				);
			}

		$standard_info = $dsl->post_endpoint('spark-listing/'.$id.'/standard-fields?_select=Latitude,Longitude,UnparsedAddress', $body);
		$property_standard_info = json_decode($standard_info, true);

		if(!empty($property_standard_info["data"])){

			$latitude = ($property_standard_info["data"]["Latitude"]) ? $property_standard_info["data"]["Latitude"]  : "";
			$longitude = ($property_standard_info["data"]["Longitude"]) ? $property_standard_info["data"]["Longitude"] :"";
			$latlon = Modules::run('home/getlatlon', $property_standard_info["data"]["UnparsedAddress"]);

				if($latitude != "********" && $longitude != "********"){

					$lat = ($property_standard_info["data"]["Latitude"]) ? $property_standard_info["data"]["Latitude"]  : "";
					$lon = ($property_standard_info["data"]["Longitude"]) ? $property_standard_info["data"]["Longitude"] :"";

				} elseif(!empty($latlon)) {

					$lat = ($latlon["lat"]) ? $latlon["lat"] : "";
					$lon = ($latlon["lon"]) ? $latlon["lon"] : "";

				} else {

					$lat = "";
					$lon = "";
				}


				$filterStr = "(MlsStatus Ne 'Closed' Or MlsStatus Ne 'Sold')";
				$filter_url = rawurlencode($filterStr);
				$filter_string = substr($filter_url, 3, -3);

				$nearby_listings = $dsl->post_endpoint('spark-nearby-listings?_expand=PrimaryPhoto&_limit='.$limit.'&_select='.$selectFields.'&_filter=('.$filter_string.')&_lat='.$lat.'&_lon='.$lon.'', $body);

				if(!empty($lat) && !empty($lon)){
					$data['nearby_listings'] = json_decode($nearby_listings, true);
				}
			}

 		}
 		$obj = Modules::load("mysql_properties/mysql_properties");
 		$data['account_info'] = $obj->get_account($this->domain_user->id, "account_info");
 		$data = isset($data) ? $data : "";

		$this->load->view($theme.'/pages/property_view/nearby-listings',$data);

	}

	public function property_virtual_tours( $id= NULL) {

 		$user_info = $this->home_model->getUsersInfo($this->domain_user->id);
		$theme = $user_info->theme;


 		if($this->uri->segment(1) == "property-details"){

 			$endpoints = "property/".$id."/virtual-tours/".$this->domain_user->id;

 			$vt = Modules::run('dsl/get_endpoint', $endpoints);
 			$virtual_tours = json_decode($vt, true);

 			$data['virtual_tours'] = "";
 			if( isset($virtual_tours['data'][0]['StandardFields']['VirtualTours']) ){
 				$data['virtual_tours'] = (!empty($virtual_tours['data'])) ? $virtual_tours['data'][0]['StandardFields']['VirtualTours'] : FALSE;
 			}


 		} else {

 			$dsl = modules::load('dsl/dsl');

 			$token = $this->home_model->getUserAccessToken($this->domain_user->agent_id);
 			$type = $this->session->userdata('auth_type');

			if($type == 0) {
				$body = array(
					'access_token' => $token->access_token
				);
			}
			else {
				$body = array(
					'bearer_token' => $token->access_token
				);
			}

 			$virtual_tours = $dsl->post_endpoint('spark-listing/'.$id.'/virtual-tours', $body);

 			$vt = json_decode($virtual_tours, true);
			if(!isset($vt['response'])){
				$data['virtual_tours'] = $vt['data'];
			}

 		}

 		$data = isset($data) ? $data : "";

		$this->load->view($theme.'/pages/property_view/virtual-tours',$data);
	}

	public function property_rooms( $id= NULL) {

 		$user_info = $this->home_model->getUsersInfo($this->domain_user->id);
		$theme = $user_info->theme;


		$dsl = modules::load('dsl/dsl');

		$token = $this->home_model->getUserAccessToken($this->domain_user->agent_id);
		$type = $this->session->userdata('auth_type');

		if($type == 0) {
			$body = array(
				'access_token' => $token->access_token
			);
		}
		else {
			$body = array(
				'bearer_token' => $token->access_token
			);
		}

			$rooms = $dsl->post_endpoint('spark-listing/'.$id.'/rooms', $body);
			$data['rooms'] = json_decode($rooms, true);

		$this->load->view($theme.'/pages/property_view/rooms',$data);

	}

}
