<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Endpoint extends MX_Controller {

	protected $domain_user;

	function __construct() {

 		parent::__construct();

 		$this->domain_user = domain_to_user();
 		$this->load->driver('cache', array('adapter' => 'apc'));
 		$userData = $this->domain_user->id;

        if(empty($userData)) {
            $this->session->set_flashdata('flash_data', 'You don\'t have access!');
            redirect('login/auth/login', 'refresh');
        }

        $this->load->library("idx_auth", $this->domain_user->agent_id);
 		$this->load->model("home_model");
 	} 

	public function default_active_listings() {
		$token = $this->home_model->getUserAccessToken($this->domain_user->agent_id);
		if($token) {
			$data["token_checker"] = $this->idx_auth->checker();
		} else {
			$data["token_checker"] = TRUE;
		}
		$user_info = $this->home_model->getUsersInfo($this->domain_user->id);
		$theme = $user_info->theme;
		$data["is_capture_leads"] = $this->home_model->get_capture_leads()->capture_leads;
		$data['new_listings'] = $this->cache->get('new_listings_'.$this->domain_user->id);
		$data['active_listings'] = $this->cache->get('active_listings_'.$this->domain_user->id);
		$data['office_listings'] = $this->cache->get('office_listings_'.$this->domain_user->id);

		if(!empty($data['active_listings'][0]->StandardFields)){
			$lat = $data['active_listings'][0]->StandardFields->Latitude;
			$lon = $data['active_listings'][0]->StandardFields->Longitude;
		} elseif (!empty($data['new_listings'][0]->StandardFields)){
			$lat = $data['new_listings'][0]->StandardFields->Latitude;
			$lon = $data['new_listings'][0]->StandardFields->Longitude;
		} elseif(!empty($data['office_listings'][0]['StandardFields'])){
			$lat = $data['office_listings'][0]['StandardFields']['Latitude'];
			$lon = $data['office_listings'][0]['StandardFields']['Longitude'];
		} else {
			$lat = "";
			$lon = "";
		}
		//$data['nearby_listings'] = $this->cache->get('nearby_listings_'.$this->domain_user->id);
		$data['nearby_listings'] = Modules::run('dsl/getNearbyListingsProxy', array("lat" => $lat,  "lon" => $lon));
		$this->cache->save('nearby_listings_'.$this->domain_user->id, $data['nearby_listings'], 86400);
		$this->load->view($theme.'/pages/home_view/default-active-listings',$data);
	}

	public function custom_active_listings() { 
		$token = $this->home_model->getUserAccessToken($this->domain_user->id);
		if($token) {
			$data["token_checker"] = $this->idx_auth->checker();
		} else {
			$data["token_checker"] = TRUE;
		}
		$user_info = $this->home_model->getUsersInfo($this->domain_user->id);
		$theme = $user_info->theme;
		$data["active_title"] = $this->home_model->get_active_title();
		$data["is_active_featured"] = $this->home_model->get_active_featured();
		$data["is_capture_leads"] = $this->home_model->get_capture_leads()->capture_leads;
		$data['new_listings'] = $this->cache->get('new_listings_'.$this->domain_user->id);
		$data['active_listings'] = $this->cache->get('active_listings_'.$this->domain_user->id);
		$data['office_listings'] = $this->cache->get('office_listings_'.$this->domain_user->id);

		if(!empty($data['active_listings'][0]->StandardFields)){
			$lat = $data['active_listings'][0]->StandardFields->Latitude;
			$lon = $data['active_listings'][0]->StandardFields->Longitude;
		} elseif (!empty($data['new_listings'][0]->StandardFields)){
			$lat = $data['new_listings'][0]->StandardFields->Latitude;
			$lon = $data['new_listings'][0]->StandardFields->Longitude;
		} elseif(!empty($data['office_listings'][0]['StandardFields'])){
			$lat = $data['office_listings'][0]['StandardFields']['Latitude'];
			$lon = $data['office_listings'][0]['StandardFields']['Longitude'];
		} else {
			$lat = "";
			$lon = "";
		}
		//$data['nearby_listings'] = $this->cache->get('nearby_listings_'.$this->domain_user->id);
		$data['nearby_listings'] = Modules::run('dsl/getNearbyListingsProxy', array("lat" => $lat,  "lon" => $lon));
		$this->cache->save('nearby_listings_'.$this->domain_user->id, $data['nearby_listings'], 86400);

		$this->load->view($theme.'/pages/home_view/default-active-listings',$data);	
	}

	public function default_nearby_listings() { 
		$token = $this->home_model->getUserAccessToken($this->domain_user->agent_id);
		if($token) {
			$data["token_checker"] = $this->idx_auth->checker();
		} else {
			$data["token_checker"] = TRUE;
		}
		$user_info = $this->home_model->getUsersInfo($this->domain_user->id);
		$theme = $user_info->theme;
		$data["is_capture_leads"] = $this->home_model->get_capture_leads()->capture_leads;
		$data['new_listings'] = $this->cache->get('new_listings_'.$this->domain_user->id);
		$data['active_listings'] = $this->cache->get('active_listings_'.$this->domain_user->id);
		$data['office_listings'] = $this->cache->get('office_listings_'.$this->domain_user->id);

		if(!empty($data['active_listings'][0]->StandardFields)){
			$lat = $data['active_listings'][0]->StandardFields->Latitude;
			$lon = $data['active_listings'][0]->StandardFields->Longitude;

		} elseif (!empty($data['new_listings'][0]->StandardFields)){
			$lat = $data['new_listings'][0]->StandardFields->Latitude;
			$lon = $data['new_listings'][0]->StandardFields->Longitude;

		} elseif(!empty($data['office_listings'][0]['StandardFields'])){
			$lat = $data['office_listings'][0]['StandardFields']['Latitude'];
			$lon = $data['office_listings'][0]['StandardFields']['Longitude'];
		} else {
			$lat = "";
			$lon = "";
		}
		//$data['nearby_listings'] = $this->cache->get('nearby_listings_'.$this->domain_user->id);
		if(!$data['nearby_listings'] = $this->cache->get('nearby_listings_'.$this->domain_user->id))
		{
			$nearby_listings = Modules::run('dsl/getNearbyListingsProxy', array("lat" => $lat,  "lon" => $lon));
			$data['nearby_listings'] = $this->cache->save('nearby_listings_'.$this->domain_user->id, $nearby_listings, 86400);
		}

		$this->load->view($theme.'/pages/home_view/default-nearby-listings',$data);
	}

	public function custom_nearby_listings() { 
		$token = $this->home_model->getUserAccessToken($this->domain_user->agent_id);
		if($token) {
			$data["token_checker"] = $this->idx_auth->checker();
		} else {
			$data["token_checker"] = TRUE;
		}
		$user_info = $this->home_model->getUsersInfo($this->domain_user->id);
		$theme = $user_info->theme;
		$data["is_capture_leads"] = $this->home_model->get_capture_leads()->capture_leads;
		$data["nearby_title"] = $this->home_model->get_nearby_title();
		$data["is_nearby_featured"] = $this->home_model->get_nearby_featured();
		$data['new_listings'] = $this->cache->get('new_listings_'.$this->domain_user->id);
		$data['active_listings'] = $this->cache->get('active_listings_'.$this->domain_user->id);
		$data['office_listings'] = $this->cache->get('office_listings_'.$this->domain_user->id);

		if(!empty($data['active_listings'][0]->StandardFields)){
			$lat = $data['active_listings'][0]->StandardFields->Latitude;
			$lon = $data['active_listings'][0]->StandardFields->Longitude;
		} elseif (!empty($data['new_listings'][0]->StandardFields)){
			$lat = $data['new_listings'][0]->StandardFields->Latitude;
			$lon = $data['new_listings'][0]->StandardFields->Longitude;
		} elseif(!empty($data['office_listings'][0]['StandardFields'])){
			$lat = $data['office_listings'][0]['StandardFields']['Latitude'];
			$lon = $data['office_listings'][0]['StandardFields']['Longitude'];
		} else {
			$lat = "";
			$lon = "";
		}

		//$data['nearby_listings'] = $this->cache->get('nearby_listings_'.$this->domain_user->id);
		if(!$data['nearby_listings'] = $this->cache->get('nearby_listings_'.$this->domain_user->id))
		{
			$nearby_listings = Modules::run('dsl/getNearbyListingsProxy', array("lat" => $lat,  "lon" => $lon));
			$data['nearby_listings'] = $this->cache->save('nearby_listings_'.$this->domain_user->id, $nearby_listings, 86400);
		}

		$this->load->view($theme.'/pages/home_view/custom-nearby-listings',$data);
	}

	public function custom_office_listings() { 
		$token = $this->home_model->getUserAccessToken($this->domain_user->agent_id);
		if($token) {
			$data["token_checker"] = $this->idx_auth->checker();
		} else {
			$data["token_checker"] = TRUE;
		}
		$user_info = $this->home_model->getUsersInfo($this->domain_user->id);
		$theme = $user_info->theme;
		$data["is_capture_leads"] = $this->home_model->get_capture_leads()->capture_leads;
		$data["is_office_featured"] = $this->home_model->get_office_featured();
		$data["office_title"] = $this->home_model->get_office_title();
		$data['office_listings'] = $this->cache->get('office_listings_'.$this->domain_user->id);
		$this->load->view($theme.'/pages/home_view/custom-office-listings',$data);
	}

	public function default_new_listings() { 
		$token = $this->home_model->getUserAccessToken($this->domain_user->agent_id);
		if($token) {
			$data["token_checker"] = $this->idx_auth->checker();
		} else {
			$data["token_checker"] = TRUE;
		}
		$user_info = $this->home_model->getUsersInfo($this->domain_user->id);
		$theme = $user_info->theme;
		$data["is_capture_leads"] = $this->home_model->get_capture_leads()->capture_leads;
		$data['new_listings'] = $this->cache->get('new_listings_'.$this->domain_user->id);
		$this->load->view($theme.'/pages/home_view/default-new-listings',$data);
	}

	public function custom_new_listings() { 
		$token = $this->home_model->getUserAccessToken($this->domain_user->agent_id);
		if($token) {
			$data["token_checker"] = $this->idx_auth->checker();
		} else {
			$data["token_checker"] = TRUE;
		}
		$user_info = $this->home_model->getUsersInfo($this->domain_user->id);
		$theme = $user_info->theme;
		$data["is_new_featured"] = $this->home_model->get_new_featured();
		$data["new_title"] = $this->home_model->get_new_title();
		$data["is_capture_leads"] = $this->home_model->get_capture_leads()->capture_leads;
		$data['new_listings'] = $this->cache->get('new_listings_'.$this->domain_user->id);
		$this->load->view($theme.'/pages/home_view/custom-new-listings',$data);
	}

	public function featured_saved_search() {
		$token = $this->home_model->getUserAccessToken($this->domain_user->agent_id);
		if($token) {
			$data["token_checker"] = $this->idx_auth->checker();
		} else {
			$data["token_checker"] = TRUE;
		}
		$user_info = $this->home_model->getUsersInfo($this->domain_user->id);
 		$theme = $user_info->theme;
		$data['is_saved_search_featured'] = $this->cache->get('is_saved_search_featured_'.$this->domain_user->id);
	 	$dsl = modules::load('dsl/dsl');
		$token = $this->home_model->getUserAccessToken($this->domain_user->agent_id);
		$access_token = array(
			'access_token' => $token->access_token
		);

		if(!empty($data["token_checker"])) {
			if(!empty($savedSearchFeatured)){ 
				foreach($savedSearchFeatured as $savedFeatured) {

					$filterStr = "(".$savedFeatured['Filter'].")";
					$filter_url = rawurlencode($filterStr);
					$filter_string = substr($filter_url, 3, -3);

					$endpoint = "spark-listings?_limit=4&_expand=PrimaryPhoto&_filter=(".$filter_string.")";
					$results = $dsl->post_endpoint($endpoint, $access_token);
					$savedFeatured['results'] = json_decode($results, true);
			    } 
		    }
		}
		if(!empty($savedSearchFeatured)){
			if(!empty($savedSearchFeatured[0]["results"]["data"])){
				$data["saved_searches_featured"] = $savedSearchFeatured;
			}
		}

		$data["is_capture_leads"] = $this->home_model->get_capture_leads()->capture_leads;

		$this->load->view($theme.'/pages/home_view/featured-saved-searches', $data);
	}

	public function selected_saved_search() {
		$token = $this->home_model->getUserAccessToken($this->domain_user->agent_id);
		if($token) {
			$data["token_checker"] = $this->idx_auth->checker();
		} else {
			$data["token_checker"] = TRUE;
		}
		$user_info = $this->home_model->getUsersInfo($this->domain_user->id);
 		$theme = $user_info->theme;
		$data['is_saved_search_selected'] = $this->cache->get('is_saved_search_selected_'.$this->domain_user->id);
		$data["is_capture_leads"] = $this->home_model->get_capture_leads()->capture_leads;

		$this->load->view($theme.'/pages/home_view/custom-saved-searches',$data);
	}

	public function sold_properties() {
		$token = $this->home_model->getUserAccessToken($this->domain_user->agent_id);
		if($token) {
			$data["token_checker"] = $this->idx_auth->checker();
		} else {
			$data["token_checker"] = TRUE;
		}
		$user_info = $this->home_model->getUsersInfo($this->domain_user->id);
 		$theme = $user_info->theme;
 		if(!$data['sold_listings'] = $this->cache->get('sold_listings_'.$this->domain_user->id))
 		{
 			$sold_listings = Modules::run('dsl/getAgentSoldPropertyListings', 6);
 			if(!empty($sold_listings)){
 				$data['sold_listings'] = $this->cache->save('sold_listings_'.$this->domain_user->id, $sold_listings, 86400);
 			} else{
 				$data['sold_listings'] = array();	
 			}	
			
 		} 
	 	$data["is_capture_leads"] = $this->home_model->get_capture_leads()->capture_leads;
 		$this->load->view($theme.'/pages/home_view/sold-properties', $data);
 	}
	
}

	