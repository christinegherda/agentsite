<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MX_Controller {

	protected $domain_user;

	function __construct() {

 		parent::__construct();

 		$this->domain_user = domain_to_user();
 		$this->load->model("customer_model", "customer");
 		$this->load->model("agent_sites/Agent_sites_model");
 		$this->load->model("search/search_model","search_model");
 		$this->load->model("home_model");
 		$this->load->model("dsl/dsl_model");
 		$this->load->model("Page_model","page");
 		$this->load->model("Meta_model");
 		$this->load->model("Blog_model","blog");
    	$this->load->model('sitemap/sitemap_model', 'sitemap');
 		$this->load->library("facebook");
 		$this->load->library("idx_auth", $this->domain_user->agent_id);
	    $this->include_functions(array('user_info' => $this->home_model->getUsersInfo($this->domain_user->id)));
 	}

	public function index() {

		$domain_info = $this->Agent_sites_model->fetch_domain_info();
        $is_reserved_domain = $this->Agent_sites_model->is_reserved_domain();

		if($this->uri->segment(1) == 'home') {

	        if(!empty($domain_info)) {
	            if($domain_info[0]->status == "completed" || $domain_info[0]->is_subdomain == "1") {
	               $base = "http://";
	               $base_url = $base.$is_reserved_domain['domain_name'].'/';
	            }
	            elseif($domain_info[0]->status != "completed" && $domain_info[0]->is_subdomain == "1") {
	                $base = "http://";
	                $base_url = $base.$is_reserved_domain['domain_name'].'/';
	            }
	            else {
	                $base_url = base_url();
	            }
	        }
	        else {
	            $base_url = base_url();
	        }

			redirect($base_url,'location',301);
		}

		$user_info = $this->home_model->getUsersInfo($this->domain_user->id);

		$data["theme"] = $user_info->theme;
		$data["header"] = "header";
	  	set_view('home');
   		$data['default_menu_data'] = Modules::run('menu/get_default_menu_data');
		$data["user_info"] = $user_info;
		//$token = $this->dsl_model->getUserAccessToken($this->domain_user->agent_id);
		$data["token_checker"] = $this->idx_auth->checker();//($token) ? $this->idx_auth->checker() : TRUE;
		//$token = $this->home_model->getUserAccessToken($this->domain_user->agent_id);

		//Set dynamic Metas
		if(isset($data['account_info'])){
			$data["broker"] = isset($data['account_info']->Office) ? $data['account_info']->Office : "" ;
			$data["broker_number"] = isset($data['account_info']->Phones[0]->Number) ? $data['account_info']->Phones[0]->Number : "" ;
		}

	  	// SEO meta title and meta description  - jovanni
		$page_meta = $this->Meta_model->get_meta($this->domain_user->agent_id, 'home');
		$agent_name = $user_info->first_name . ' ' . $user_info->last_name;
		$data['agent_city'] = $agent_city = ($user_info->city != 'Not Specified')? ucwords(str_replace('  ', '', $user_info->city)) : '';

		$data['title'] = (isset($page_meta['meta_title']) && $page_meta['meta_title'] != '')? $page_meta['meta_title'] : "{$agent_city} Real Estate - {$agent_city} Homes | {$agent_name}";
		$data['description'] = (isset($page_meta['meta_description']) && $page_meta['meta_description'] != '')? $page_meta['meta_description'] : "Explore {$agent_city} real estate with {$agent_name}. MLS listings for {$agent_city} homes updated every day. Find real estate in {$agent_city} today!";
		$data['canonical'] = (isset($page_meta['canonical']) && $page_meta['canonical'] != '')? $page_meta['canonical'] : '';
		$data['keywords'] = isset($page_meta['keywords'])? $page_meta['keywords'] : strtolower($agent_city) . ' real estate, ' . strtolower($agent_city) . ' homes, real estate in ' . strtolower($agent_city);
		// ===================

	 	$data["protocol"] = $this->protocol();
	 	$data["is_capture_leads"] = $this->home_model->get_capture_leads()->capture_leads;
	 	$data["profile"] = $this->customer->get_customer_profile();
	 	$data["saved_search_title"] = $this->home_model->get_saved_search_title();
	 	$data["sold_listings_title"] = $this->home_model->get_sold_listings_title();
	 	$data["sold_listings_option"] = $this->home_model->get_sold_listings_option();
	 	$data["total_sold_listings"] = $this->home_model->getSoldListingsData(TRUE);

    	$data = add_filter('before-home-view', $data);
		$this->load->view("page", $data);
	}

	public function other_property() {
		 //get custom domain url
        $domain_info = $this->Agent_sites_model->fetch_domain_info();
        $is_reserved_domain = $this->Agent_sites_model->is_reserved_domain();

        if(!empty($domain_info)) {
            if($domain_info[0]->status == "completed" || $domain_info[0]->is_subdomain == "1") {
               $base = "http://";
               $base_url = $base.$is_reserved_domain['domain_name'].'/';
            }
            elseif($domain_info[0]->status != "completed" && $domain_info[0]->is_subdomain == "1") {
                $base = "http://";
                $base_url = $base.$is_reserved_domain['domain_name'].'/';
            }
            else {
                $base_url = base_url();
            }

        }
        else {
            $base_url = base_url();
        }
	 	redirect($base_url,'location',301);
	}

	public function featured_property() {

		$domain_info = $this->Agent_sites_model->fetch_domain_info();
        $is_reserved_domain = $this->Agent_sites_model->is_reserved_domain();

        if(!empty($domain_info)){

            if($domain_info[0]->status == "completed" || $domain_info[0]->is_subdomain == "1") {

               $base = "http://";
               $base_url = $base.$is_reserved_domain['domain_name'].'/';

             } elseif($domain_info[0]->status != "completed" && $domain_info[0]->is_subdomain == "1") {

                $base = "http://";
                $base_url = $base.$is_reserved_domain['domain_name'].'/';

             } else {

                    $base_url = base_url();
                }

        } else {

            $base_url = base_url();
        }

	 	redirect($base_url,'location',301);
	}

	public function find_a_home() {
	  redirect('search-results','location',301);
	}

	function find_a_home_old() {

		$this->load->model("search/search_model","search_model");
		$obj = Modules::load("mysql_properties/mysql_properties");
		$data['account_info'] = $obj->get_account($this->domain_user->id, "account_info");
		$data['system_info'] = $obj->get_account($this->domain_user->id, "system_info");

        $data['page_data'] = $page_data = $this->page->get_page( isset($slug) ? $slug : '' );
		$user_info = $this->home_model->getUsersInfo($this->domain_user->id);
		$obj = Modules::load("mysql_properties/mysql_properties");

		$data["theme"] = $user_info->theme;
		$data["header"] = "header-page";
		$data["page"] = "find_a_home_new";

    	//SEO meta title and meta description  - jovanni
		$page_meta = $this->Meta_model->get_meta($this->domain_user->agent_id, 'find-a-home');
	    $agent_name = $user_info->first_name . ' ' . $user_info->last_name;
	    $agent_city = ($user_info->city != 'Not Specified')? ucwords(str_replace('  ', '', $user_info->city)) : '';

		$data['title'] = (isset($page_meta['meta_title']) && $page_meta['meta_title'] != '')? $page_meta['meta_title'] : "Find Houses for Sale in {$agent_city} | {$agent_name}";
		$data['description'] = (isset($page_meta['meta_description']) && $page_meta['meta_description'] != '')? $page_meta['meta_description'] : "Find Houses for Sale in {$agent_city}. {$agent_name} provides regularly updated MLS listings for homes for sale in {$agent_city}.";
		$data['canonical'] = (isset($page_meta['canonical']) && $page_meta['canonical'] != '')? $page_meta['canonical'] : '';
		$data['keywords'] = isset($page_meta['keywords'])? $page_meta['keywords'] : 'houses for sale in ' . strtolower($agent_city) . ', ' . strtolower($agent_city) . ' homes for sale, homes for sale in ' . strtolower($agent_city);
		$data['h1'] = (isset($page_meta['h1']) && $page_meta['h1'] != '')? $page_meta['h1'] : 'Find a Home';
	  	//===================

		$data['default_menu_data'] = Modules::run('menu/get_default_menu_data');
	    $data['custom_menu_data'] = Modules::run('menu/get_custom_menu_data');

	    $data["user_info"] = $user_info;

	    $data["property_types"] = Modules::run('dsl/get_property_types');
 		$data["property_subtypes"] = Modules::run('dsl/get_property_subtypes');
		$data['account_info'] = $obj->get_account($this->domain_user->id, "account_info");
		$data['system_info'] = $obj->get_account($this->domain_user->id, "system_info");
		$data['socialmedia_link'] = Modules::run('agent_social_media/agent_social_media/linksInfo');
		$data['office_listings'] = $obj->get_properties($this->domain_user->id, "office");
		$data['new_listings'] = $obj->get_properties($this->domain_user->id, "new");

		$this->load->model("home/home_model","home_model");
		$dsl = modules::load('dsl/dsl');

		$page = ($this->input->get('page')) ? $this->input->get('page') : 1;

		$limit = 10;
		$totalrows = (isset($_GET['totalrows'])) ? $_GET['totalrows'] : 0;

		if(isset($_GET['offset']) && !isset($_GET['page'])) {

			$total_page = $totalrows / $limit;
			$remainder = $totalrows % $limit;

			if($remainder != 0){
				$total_page++;
			}

			$total_page = intval($total_page);

			$actual_page = $_GET['offset'] / $limit;
			$actual_page = intval($actual_page);
			$actual_page = ($actual_page <= 0) ? 1 : $actual_page;

			if($actual_page < $total_page) {
				$page += $actual_page;
			}
		}
		$order = "-YearBuilt";
		if($this->input->get('order') == 'highest') {
			$order = "-ListPrice";
		}
		if($this->input->get('order') == 'lowest') {
			$order = "+ListPrice";
		}
		if($this->input->get('order') == 'newest') {
			$order = "-YearBuilt";
		}
		if($this->input->get('order') == 'oldest') {
			$order = "+YearBuilt";
		}

		$mls_status = $this->mls_status();
		$endpoint = "properties/".$this->domain_user->id."/status/".$mls_status."?_pagination=1&_page=".$page."&_limit=".$limit."&_expand=PrimaryPhoto&_orderby=".$order;

		$active_listings = $dsl->get_endpoint($endpoint);
		$activeListingData = json_decode($active_listings,true);
		$activeList = array();


		if( isset($activeListingData["data"]) && !empty($activeListingData["data"])){
				foreach ($activeListingData["data"] as $active){
			 		$activeList[] = $active["standard_fields"][0];
			 	}

			$data['active_listings'] = $activeList;
		}

		if(isset($activeListingData)) {
			$data['current_page'] = $activeListingData['current_page'];
			$data['totalrows'] = $activeListingData['total_count'];
		}

		/* Query builder for pagination: (the old pagination gets the entire url including the offset variable which cause the offset variable duplicate) */
		$query_arr = array();

		foreach($_GET as $key=>$val) {
			if($key!="offset") {
				$query_arr[$key]=$val;
			}
		}

		if(isset($data['totalrows']) && isset($data['current_page'])) {
			$query_arr['totalrows']=$data['totalrows'];
		}

		$build_query = http_build_query($query_arr);
		/* End of query builder */

		$this->load->library('pagination');
		if(!empty($activeListingData)) {
			$data['total_pages'] = $activeListingData["total_pages"];
			$data['current_page'] = $activeListingData["current_page"];
			$data['total_count'] = $activeListingData["total_count"];
			$data['page_size'] = $activeListingData["page_size"];

			$param["limit"] = 10;
			$total = $activeListingData["total_count"];

			$config["base_url"] = base_url().'/find-a-home?'.$build_query;
			$config["total_rows"] = $total;
			$config["per_page"] = $param["limit"];
			$config['page_query_string'] = TRUE;
			$config['query_string_segment'] = 'offset';

			$this->pagination->initialize($config);
		}

		//get coordinates according to search
		$latitude = isset($data['active_listings'][0]['StandardFields']['Latitude']) ? $data['active_listings'][0]['StandardFields']['Latitude'] : "";
		$longitude = isset($data['active_listings'][0]['StandardFields']['Longitude']) ? $data['active_listings'][0]['StandardFields']['Longitude'] : "";

		if(empty($latitude) || empty($longitude) || $latitude == "********" || $longitude == "********") {

			$latlon = $this->getlatlon(isset($data['active_listings'][0]['StandardFields']['UnparsedAddress'])? $data['active_listings'][0]['StandardFields']['UnparsedAddress'] : "");
			$latitude = $latlon['lat'];
			$longitude = $latlon['lon'];
		}

		$data['coordinates'] = array('lat'=>$latitude, 'lon'=>$longitude);
		$data["pagination"] = $this->pagination->create_links();

		$data["js"] = array("listings.js","customer.js","selectize.js","advanced_search.js");
		$data["css"] = array("selectize.bootstrap3.css");

		$savesearches = $this->customer->get_customer_searches();
		$savesearchesflex = $this->customer->getSaveSearchesFlex();
		$save_searches_flex = ( empty($savesearchesflex) ) ? array() : $savesearchesflex;
        $data["saved_searches_db"] = array_merge($savesearches, $save_searches_flex);

		$data["protocol"] = $this->protocol();
		$data["is_capture_leads"] = $this->home_model->get_capture_leads()->capture_leads;
		$data["profile"] = $this->customer->get_customer_profile();
		$mls_id = $data['account_info']->MlsId;
		$data['search_fields'] = $this->search_model->get_mls_search_data($mls_id);
		$data['search_fields_groups'] = $this->search_model->get_mls_search_group($mls_id);

		$this->load->view('page', $data);
	}
	public function page($slug = NULL) {

		if($this->uri->segment(1) == 'home' && $this->uri->segment(2) == 'page' ) {

			redirect("page/$slug", "refresh");
		}

    	$page_data = $this->page->get_page($slug);
		$user_info = $this->home_model->getUsersInfo($this->domain_user->id);
		$obj = Modules::load("mysql_properties/mysql_properties");

		$data["theme"] = $user_info->theme;
		$data["header"] = "header-page";
		$data["page"] = "page_view";

	    // SEO meta title and meta description  - jovanni
		$page_meta = $this->Meta_model->get_meta($this->domain_user->agent_id, $slug);
	    $data['title'] = (isset($page_meta['meta_title']) && $page_meta['meta_title'] != '')? $page_meta['meta_title'] : $page_data["title"] . ' | ' . $user_info->first_name . ' ' . $user_info->last_name . ' | ' .$user_info->phone;
	    $data['description'] = (isset($page_meta['meta_description']) && $page_meta['meta_description'] != '')? $page_meta['meta_description'] : $user_info->banner_tagline;
	    $data['canonical'] = (isset($page_meta['canonical']) && $page_meta['canonical'] != '')? $page_meta['canonical'] : '';
	    // ===================

		$data['default_menu_data'] = Modules::run('menu/get_default_menu_data');
    	$data['custom_menu_data'] = Modules::run('menu/get_custom_menu_data');

    	$data["user_info"] = $user_info;
		$data['account_info'] = $obj->get_account($this->domain_user->id, "account_info");
		$data['system_info'] = $obj->get_account($this->domain_user->id, "system_info");
		$data['socialmedia_link'] = Modules::run('agent_social_media/agent_social_media/linksInfo');

		$data["page_data"] = $page_data;
		$data["search_id"] = $this->page->get_search_id($slug);
		$search_id = $data["search_id"]["saved_search_id"];
		$data["search_id"] = $search_id;
		$savedSearch = $this->idx_auth->getSavedSearch($search_id);

		if(!empty($savedSearch)){

			$dsl = modules::load('dsl/dsl');
			$token = $this->home_model->getUserAccessToken($this->domain_user->agent_id);
			$access_token = array(
					'access_token' => $token->access_token
			);

			$filterStr = "(".$savedSearch[0]['Filter'].")";
			$filter_url = rawurlencode($filterStr);
			$filter_string = substr($filter_url, 3, -3);

			$endpoint = "spark-listings?_limit=6&_expand=PrimaryPhoto&_filter=(".$filter_string.")";
			$results = $dsl->post_endpoint($endpoint, $access_token);
			$savedSearches['results'] = json_decode($results, true);

			$data["saved_searches"] = $savedSearches['results'];
		}

		$data["is_capture_leads"] = $this->home_model->get_capture_leads()->capture_leads;
		$data["js"] = array("listings.js","customer.js");
		$data["profile"] = $this->customer->get_customer_profile();
		$data["protocol"] = $this->protocol();

		$this->load->view('page',$data);
	}
	public function about() {
		$user_info = $this->home_model->getUsersInfo($this->domain_user->id);
		$obj = Modules::load("mysql_properties/mysql_properties");

		$data["theme"] = $user_info->theme;
		$data["header"] = "header-page";
		$data["page"] = "about";

		// SEO meta title and meta description  - jovanni
	    $page_meta = $this->Meta_model->get_meta($this->domain_user->agent_id, 'about');
	    $data['title'] = (isset($page_meta['meta_title']) && $page_meta['meta_title'] != '')? $page_meta['meta_title'] : 'About | ' . $user_info->first_name . ' ' . $user_info->last_name . ' | ' .$user_info->phone;
	    $data['description'] = (isset($page_meta['meta_description']) && $page_meta['meta_description'] != '')? $page_meta['meta_description'] : $user_info->banner_tagline;
	    $data['canonical'] = (isset($page_meta['canonical']) && $page_meta['canonical'] != '')? $page_meta['canonical'] : '';
	    // ===================

		$data['default_menu_data'] = Modules::run('menu/get_default_menu_data');
    	$data['custom_menu_data'] = Modules::run('menu/get_custom_menu_data');

		$data["user_info"] = $user_info;
		$data['account_info'] = $obj->get_account($this->domain_user->id, "account_info");
		$data['system_info'] = $obj->get_account($this->domain_user->id, "system_info");
		$data['socialmedia_link'] = Modules::run('agent_social_media/agent_social_media/linksInfo');

		$data["js"] = array("listings.js");
		$data["profile"] = $this->customer->get_customer_profile();
		$data["protocol"] = $this->protocol();
		$data = add_filter('before-about-view', $data);
		$this->load->view("page", $data);
	}


	public function contact() {
		$user_info = $this->home_model->getUsersInfo($this->domain_user->id);
		$obj = Modules::load("mysql_properties/mysql_properties");

		$data["theme"] = $user_info->theme;
		$data["header"] = "header-page";
		$data["page"] = "contact";

    	// SEO meta title and meta description  - jovanni
		$page_meta = $this->Meta_model->get_meta($this->domain_user->agent_id, 'contact');
    	$data['title'] = (isset($page_meta['meta_title']) && $page_meta['meta_title'] != '')? $page_meta['meta_title'] : 'Contact | ' . $user_info->first_name . ' ' . $user_info->last_name . ' | ' .$user_info->phone;
    	$data['description'] = (isset($page_meta['meta_description']) && $page_meta['meta_description'] != '')? $page_meta['meta_description'] : $user_info->banner_tagline;
    	$data['canonical'] = (isset($page_meta['canonical']) && $page_meta['canonical'] != '')? $page_meta['canonical'] : '';
    	// ===================

		$data['default_menu_data'] = Modules::run('menu/get_default_menu_data');
    	$data['custom_menu_data'] = Modules::run('menu/get_custom_menu_data');

		$data["user_info"] = $user_info;
		$data['account_info'] = $obj->get_account($this->domain_user->id, "account_info");
		$data['system_info'] = $obj->get_account($this->domain_user->id, "system_info");
		$data['is_premium'] = $obj->freemium_subscription($this->domain_user->id);
		//printA($data['is_freemium']);exit;
		$data['socialmedia_link'] = Modules::run('agent_social_media/agent_social_media/linksInfo');
		$data["profile"] = $this->customer->get_customer_profile();
		$data["page_data"] = $this->page->get_page_content($data["page"]);
		$data["js"] = array("listings.js");
		$data["protocol"] = $this->protocol();
		$data = add_filter('before-contact-view', $data);
		$this->load->view("page", $data);
	}

	public function buyer() {

		$user_info = $this->home_model->getUsersInfo($this->domain_user->id);
		$obj = Modules::load("mysql_properties/mysql_properties");

		if (!empty($user_info->plan_id) && $user_info->plan_id == "2") {
			$data["theme"] = $user_info->theme;
			$data["header"] = "header-page";
			$data["page"] = "buyer";
	    
	    	set_view($data['page']);
			$data['default_menu_data'] = Modules::run('menu/get_default_menu_data');

			$data["user_info"] = $user_info;
			$data['account_info'] = $obj->get_account($this->domain_user->id, "account_info");
			$data['system_info'] = $obj->get_account($this->domain_user->id, "system_info");
			$data['socialmedia_link'] = Modules::run('agent_social_media/agent_social_media/linksInfo');
			$data["page_data"] = $page_data =  $this->page->get_page_content($data["page"]);

			// SEO meta title and meta description  - jovanni
			$page_meta = $this->Meta_model->get_meta($this->domain_user->agent_id, 'buyer');
			$agent_name = $user_info->first_name . ' ' . $user_info->last_name;
			$agent_city = ($user_info->city != 'Not Specified')? ucwords(str_replace('  ', '', $user_info->city)) : '';

			$data['title'] = (isset($page_meta['meta_title']) && $page_meta['meta_title'] != '')? $page_meta['meta_title'] : "{$agent_city} Homes, Buy a House in {$agent_city} | {$agent_name}";
			$data['description'] = (isset($page_meta['meta_description']) && $page_meta['meta_description'] != '')? $page_meta['meta_description'] : "If you are looking to buy a house in {$agent_city}, or perhaps relocating, {$agent_name} provides useful tools and information about {$agent_city} homes.";
			$data['canonical'] = (isset($page_meta['canonical']) && $page_meta['canonical'] != '')? $page_meta['canonical'] : '';
			$data['keywords'] = isset($page_meta['keywords'])? $page_meta['keywords'] : strtolower($agent_city) . ' homes, buy a house in '.strtolower($agent_city).', buying a home in ' . strtolower($agent_city);
			$data['h1'] = (isset($page_meta['h1']) && $page_meta['h1'] != '')? $page_meta['h1'] : $page_data['title'];
			// ===================

			$data["js"] = array("listings.js");
			$data["protocol"] = $this->protocol();
			$data["profile"] = $this->customer->get_customer_profile();
	    
	    	$data = add_filter('before-buyer-view', $data);
			$this->load->view("page", $data);

		} else {
			redirect($base_url,'location',301);

		}
	}

	public function seller() {

		$user_info = $this->home_model->getUsersInfo($this->domain_user->id);
		$obj = Modules::load("mysql_properties/mysql_properties");

		if (!empty($user_info->plan_id) && $user_info->plan_id == "2") { 
			$data['plan_id'] = $user_info->plan_id;
			$data["theme"] = $user_info->theme;
			$data["header"] = "header-page";
			$data["page"] = "seller";
	    	set_view('seller');
			// SEO meta title and meta description  - jovanni
			$page_meta = $this->Meta_model->get_meta($this->domain_user->agent_id, 'seller');
			$agent_name = $user_info->first_name . ' ' . $user_info->last_name;
			$agent_city = ($user_info->city != 'Not Specified')? ucwords(str_replace('  ', '', $user_info->city)) : '';

			$data['title'] = (isset($page_meta['meta_title']) && $page_meta['meta_title'] != '')? $page_meta['meta_title'] : "How Much is My House Worth? - Home Appraisal {$agent_city}";
			$data['description'] = (isset($page_meta['meta_description']) && $page_meta['meta_description'] != '')? $page_meta['meta_description'] : "How much is my house worth? {$agent_name} provides a free {$agent_city} home appraisal and actively updated MLS. Find out what your home is worth today!";
			$data['canonical'] = (isset($page_meta['canonical']) && $page_meta['canonical'] != '')? $page_meta['canonical'] : '';
			$data['keywords'] = isset($page_meta['keywords'])? $page_meta['keywords'] : 'how much is my house worth, what is my home worth, house appraisal, house appraisal ' . strtolower($agent_city);
			// ================
			$data['default_menu_data'] = Modules::run('menu/get_default_menu_data');
	    	$data['custom_menu_data'] = Modules::run('menu/get_custom_menu_data');

			$data["user_info"] = $user_info;
			$data['account_info'] = $obj->get_account($this->domain_user->id, "account_info");

			$data['system_info'] = $obj->get_account($this->domain_user->id, "system_info");
			$data['socialmedia_link'] = Modules::run('agent_social_media/agent_social_media/linksInfo');
			$data["page_data"] = $this->page->get_page_content($data["page"]);
			$data["js"] = array("listings.js");
			$data["protocol"] = $this->protocol();
			$data["profile"] = $this->customer->get_customer_profile();
			$data['settings'] = $this->home_model->get_seo_settings($this->domain_user->agent_id);
	    $data = add_filter('before-seller-view', $data);

	    
			$this->load->view("page", $data);

		} else {
			redirect($base_url,'location',301);
		}
	}

	public function testimonials($slug = NULL) {
    $data['page_data'] = $page_data = $this->page->get_page($slug);
		$user_info = $this->home_model->getUsersInfo($this->domain_user->id);
		$obj = Modules::load("mysql_properties/mysql_properties");

		$data["theme"] = $user_info->theme;
		$data["header"] = "header-page";
		$data["page"] = "testimonials";

		// SEO meta title and meta description  - jovanni
	    $page_meta = $this->Meta_model->get_meta($this->domain_user->agent_id, 'testimonials');
	    $data['title'] = (isset($page_meta['meta_title']) && $page_meta['meta_title'] != '')? $page_meta['meta_title'] : 'Testimonials | ' . $user_info->first_name . ' ' . $user_info->last_name . ' | ' .$user_info->phone;
	    $data['description'] = (isset($page_meta['meta_description']) && $page_meta['meta_description'] != '')? $page_meta['meta_description'] : $user_info->banner_tagline;
	    $data['canonical'] = (isset($page_meta['canonical']) && $page_meta['canonical'] != '')? $page_meta['canonical'] : '';
	    // ===================

		$data['default_menu_data'] = Modules::run('menu/get_default_menu_data');
    	$data['custom_menu_data'] = Modules::run('menu/get_custom_menu_data');

		$data["user_info"] = $user_info;
		$data['account_info'] = $obj->get_account($this->domain_user->id, "account_info");
		$data['system_info'] = $obj->get_account($this->domain_user->id, "system_info");
		$data['socialmedia_link'] = Modules::run('agent_social_media/agent_social_media/linksInfo');

		$this->load->model('reviews/reviews_model', 'review');
		$param["offset"] = (isset($_GET["offset"])) ? $_GET["offset"] : 0;
		$param["limit"] = 10;

    	$data['reviews'] = ($this->input->get('reviews_order')) ? Modules::run('reviews/reviews/get_sort_reviews', $param) : Modules::run('reviews/reviews/fetch_approved_reviews', $param);
		$data['average_reviews'] = Modules::run('reviews/reviews/compute_average_review');
		$total = $this->review->count_approved_reviews();
		$this->load->library('pagination');
		$config["base_url"] = base_url().'/testimonials';
		$config["total_rows"] = $total;
		$config["per_page"] = $param["limit"];
		$config['page_query_string'] = TRUE;
		$config['query_string_segment'] = 'offset';
		$this->pagination->initialize($config);
		$data["pagination"] = $this->pagination->create_links();

		$data["js"] = array("jquery.rateyo.min.js");
		$data["css"] = array("jquery.rateyo.min.css");
		$data["protocol"] = $this->protocol();
		$data = add_filter('before-testimonial-view', $data);
		$this->load->view("page", $data);
	}

	function add_testimonial() {

		if($this->input->post('subject') && $this->input->post('message')) {

			$data = array(
				'user_id' 		=> $this->domain_user->id,
				'customer_id' 	=> $this->session->userdata('contact_id'),
				'rating' 		=> $this->input->post('rating'),
				'subject' 		=> $this->input->post('subject'),
				'message' 		=> $this->input->post('message'),
				'date_created' 	=> date('Y-m-d H:i:s')
			);

			$insert = $this->home_model->save_testimonial($data);
			
			$response = ($insert) ? array("success"=>TRUE, "redirect"=>base_url().'testimonials') : array("success" => FALSE, "redirect"=>base_url().'testimonials');
			exit(json_encode($response));
		}
	}

	function blog($slug = NULL) {
    $data['page_data'] = $page_data = $this->blog->get_blog($slug);
		$user_info = $this->home_model->getUsersInfo($this->domain_user->id);
		$obj = Modules::load("mysql_properties/mysql_properties");

		$data["theme"] = $user_info->theme;
		$data["header"] = "header-page";
		$data["page"] = "blog";

		// SEO meta title and meta description  - jovanni
	    $page_meta = $this->Meta_model->get_meta($this->domain_user->agent_id, 'blog');
	    $data['title'] = (isset($page_meta['meta_title']) && $page_meta['meta_title'] != '')? $page_meta['meta_title'] : 'Blog | ' . $user_info->first_name . ' ' . $user_info->last_name . ' | ' .$user_info->phone;
	   	$data['description'] = (isset($page_meta['meta_description']) && $page_meta['meta_description'] != '')? $page_meta['meta_description'] : $user_info->banner_tagline;
	    $data['canonical'] = (isset($page_meta['canonical']) && $page_meta['canonical'] != '')? $page_meta['canonical'] : '';
	    // ===================

		$data['default_menu_data'] = Modules::run('menu/get_default_menu_data');
    	$data['custom_menu_data'] = Modules::run('menu/get_custom_menu_data');

		$data["user_info"] = $user_info;
		$data['account_info'] = $obj->get_account($this->domain_user->id, "account_info");
		$data['system_info'] = $obj->get_account($this->domain_user->id, "system_info");
		$data['socialmedia_link'] = Modules::run('agent_social_media/agent_social_media/linksInfo');

		$this->load->library('pagination');
		$param = array();
		$param["offset"] = (isset($_GET["offset"])) ? $_GET["offset"] : 0;
		$param["limit"] = 5;
		$total = $this->blog->get_all_blogs( TRUE, $param );
		$data["blogs"] = $this->blog->get_all_blogs( FALSE, $param );
		$config["base_url"] = base_url().'/blog';
		$config["total_rows"] = $total;
		$config["per_page"] = $param["limit"];
		$config['page_query_string'] = TRUE;
		$config['query_string_segment'] = 'offset';
		$this->pagination->initialize($config);
		$data["pagination"] = $this->pagination->create_links();
		$data["protocol"] = $this->protocol();
		$data["all_blog_archives"] = $this->blog->get_all_blog_archives();

		$this->load->view("page", $data);
	}

	function post($slug = NULL) {
		$data['page_data'] = $page_data = $this->blog->get_blog($slug);
    	$post_data = $this->blog->get_blog( $slug);
		$user_info = $this->home_model->getUsersInfo($this->domain_user->id);
		$obj = Modules::load("mysql_properties/mysql_properties");

		$data["theme"] = $user_info->theme;
		$data["header"] = "header-page";
		$data["page"] = "post";

		// SEO meta title and meta description  - jovanni
	    $page_meta = $this->Meta_model->get_meta($this->domain_user->agent_id, 'post/' . $slug);
	    $data['title'] = (isset($page_meta['meta_title']) && $page_meta['meta_title'] != '')? $page_meta['meta_title'] : $post_data["title"] . ' | ' . $user_info->first_name . ' ' . $user_info->last_name . ' | ' .$user_info->phone;
	    $data['description'] = (isset($page_meta['meta_description']) && $page_meta['meta_description'] != '')? $page_meta['meta_description'] : $user_info->banner_tagline;
	    $data['canonical'] = (isset($page_meta['canonical']) && $page_meta['canonical'] != '')? $page_meta['canonical'] : '';
	    // ===================

		$data['default_menu_data'] = Modules::run('menu/get_default_menu_data');
    	$data['custom_menu_data'] = Modules::run('menu/get_custom_menu_data');

		$data["user_info"] = $user_info;
		$data['account_info'] = $obj->get_account($this->domain_user->id, "account_info");
		$data['system_info'] = $obj->get_account($this->domain_user->id, "system_info");
		$data['socialmedia_link'] = Modules::run('agent_social_media/agent_social_media/linksInfo');

		$param = array();
		$data["blogs"] = $this->blog->get_all_blogs( FALSE, $param );
		$data["recent_posts"] = $this->blog->get_recent_post();
		$data["all_blog_archives"] = $this->blog->get_all_blog_archives();
		$data["blog_post"] = $post_data;
    	$data["protocol"] = $this->protocol();

		$this->load->view("page",$data);
	}

	function archives($filter = NULL) {
		$user_info = $this->home_model->getUsersInfo($this->domain_user->id);
		$obj = Modules::load("mysql_properties/mysql_properties");

		$data["theme"] = $user_info->theme;
		$data["header"] = "header-page";
		$data["page"] = "archive";

		// SEO meta title and meta description  - jovanni
	    $page_meta = $this->Meta_model->get_meta($this->domain_user->agent_id, 'archives');
	    $data['title'] = (isset($page_meta['meta_title']) && $page_meta['meta_title'] != '')? $page_meta['meta_title'] : 'Archives | ' . $user_info->first_name . ' ' . $user_info->last_name . ' | ' .$user_info->phone;
	   	$data['description'] = (isset($page_meta['meta_description']) && $page_meta['meta_description'] != '')? $page_meta['meta_description'] : $user_info->banner_tagline;
	    $data['canonical'] = (isset($page_meta['canonical']) && $page_meta['canonical'] != '')? $page_meta['canonical'] : '';
	    // ===================

		$data['default_menu_data'] = Modules::run('menu/get_default_menu_data');
    	$data['custom_menu_data'] = Modules::run('menu/get_custom_menu_data');

		$data["user_info"] = $user_info;
		$data['account_info'] = $obj->get_account($this->domain_user->id, "account_info");
		$data['system_info'] = $obj->get_account($this->domain_user->id, "system_info");
		$data['socialmedia_link'] = Modules::run('agent_social_media/agent_social_media/linksInfo');

		$this->load->library('pagination');
		$param = array();
		$param["offset"] = (isset($_GET["offset"])) ? $_GET["offset"] : 0;
		$param["limit"] = 5;
		$total = $this->blog->filter_archives($filter, TRUE, $param );
		$data["filter_archives"] = $this->blog->filter_archives($filter, FALSE, $param );
		$config["base_url"] = base_url().'/archives';
		$config["total_rows"] = $total;
		$config["per_page"] = $param["limit"];
		$config['page_query_string'] = TRUE;
		$config['query_string_segment'] = 'offset';
		$this->pagination->initialize($config);
		$data["pagination"] = $this->pagination->create_links();

		$data["all_blog_archives"] = $this->blog->get_all_blog_archives();
		$data["protocol"] = $this->protocol();

		$this->load->view("page", $data);
	}

	function saved_searches( $search_id = NULL){

 		if(empty($search_id)) {
 			redirect("/home", "refresh");
 		}

 		$user_info = $this->home_model->getUsersInfo($this->domain_user->id);
 		$obj = Modules::load("mysql_properties/mysql_properties");

 		$data["theme"] = $user_info->theme;
 		$data["header"] = "header-page";
 		$data["page"] = "advance_search";
 		$data["title"] = "Saved Search";

 		$data['default_menu_data'] = Modules::run('menu/get_default_menu_data');
     	$data['custom_menu_data'] = Modules::run('menu/get_custom_menu_data');

 		$data["user_info"] = $user_info;
 		$data['account_info'] = $obj->get_account($this->domain_user->id, "account_info");
 		$data['system_info'] = $obj->get_account($this->domain_user->id, "system_info");
 		$data['socialmedia_link'] = Modules::run('agent_social_media/agent_social_media/linksInfo');

		$page = ($this->input->get('page')) ? $this->input->get('page') : 1;

 		$data['limit'] = $limit = 10;
		$totalrows = (isset($_GET['totalrows'])) ? $_GET['totalrows'] : 0;

		if(isset($_GET['offset']) && !isset($_GET['page'])) {

 		$total_page = $totalrows / $limit;
 			$remainder = $totalrows % $limit;

 			if($remainder != 0){
 				$total_page++;
 			}

 			$total_page = intval($total_page);

 			$actual_page = $_GET['offset'] / $limit;
 			$actual_page = intval($actual_page);
 			$actual_page = ($actual_page <= 0) ? 1 : $actual_page;

 			if($actual_page < $total_page) {
				$page += $actual_page;
 			}
 		}
 		$order = "-OnMarketDate";
 		if($this->input->get('order') == 'highest') {
 			$order = "-ListPrice";
 		}
 		if($this->input->get('order') == 'lowest') {
 			$order = "+ListPrice";
 		}
 		if($this->input->get('order') == 'newest') {
 			$order = "-OnMarketDate";
 		}
 		if($this->input->get('order') == 'oldest') {
 			$order = "+YearBuilt";
 		}

 		$savedSearch = $this->idx_auth->getSavedSearch($search_id);

 		$dsl = modules::load('dsl/dsl');
	 	$token = $this->home_model->getUserAccessToken($this->domain_user->agent_id);
	 	$access_token = array(
	 		'access_token' => $token->access_token
	 	);

	 	$filterStr = "(".$savedSearch[0]['Filter'].")";
	 	$filter_url = rawurlencode($filterStr);
	 	$filter_string = substr($filter_url, 3, -3);

	 	$endpoint = "spark-listings?_pagination=1&_page=".$page."&_limit=".$limit."&_expand=PrimaryPhoto&_filter=(".$filter_string.")&_orderby=".$order;
	 	$results = $dsl->post_endpoint($endpoint, $access_token);

	 	$savedSearches['results'] = json_decode($results, true);
 		if($this->input->is_ajax_request()){ // very useful in saved search widget
    	header('Content-Type: application/json');

    	echo json_encode($savedSearches);

    }else{

	 		$data['saved'] = $savedSearches['results'];

	 		if(isset($data['saved']['pagination'])) {
				$data['current_page'] = $data['saved']['pagination']['CurrentPage'];
	 			$data['totalrows'] = $data['saved']['pagination']['TotalRows'];
	 		}

			$query_arr = array();

	 		foreach($_GET as $key=>$val) {
	 			if($key!="offset") {
	 				$query_arr[$key]=$val;
				}
	 		}

	 		if(isset($data['totalrows']) && isset($data['current_page'])) {
	 			$query_arr['totalrows']=$data['totalrows'];
			}
	 		$build_query = http_build_query($query_arr);

	 		$this->load->library('pagination');


	 		if(!isset($data['saved']["response"])) {
	 			$data['total_pages'] = $data['saved']['pagination']['TotalPages'];
	 			$data['current_page'] = $data['saved']['pagination']['CurrentPage'];
	 			$data['total_count'] = $data['saved']['pagination']['TotalRows'];
	 			$data['page_size'] = $data['saved']['pagination']['PageSize'];

	 			$param["limit"] = 10;
	 			$total = $data['saved']['pagination']['TotalRows'];
	 			$config["base_url"] = base_url().'/saved_searches/'.$search_id.'?'.$build_query;
	 			$config["total_rows"] = $total;
	 			$config["per_page"] = $param["limit"];
	 			$config['page_query_string'] = TRUE;
	 			$config['query_string_segment'] = 'offset';

	 			$this->pagination->initialize($config);
	 		}

	 		$data["pagination"] = $this->pagination->create_links();
	 		$data['search_id'] = $search_id;
	 		if(!isset($savedSearches['results']['response'])){
	 			$data["featured"] = $savedSearches['results']['data'];
	 		}
	 		$data['saved_search_name'] = $savedSearch[0]["Name"];
	 		//printA($data["featured"]);exit;

	    	// SEO meta title and meta description  - jovanni
	 	    $page_meta = $this->Meta_model->get_meta($this->domain_user->agent_id, 'saved_searches/' . $search_id);
	 	    $data['title'] = (isset($page_meta['meta_title']) && $page_meta['meta_title'] != '')? $page_meta['meta_title'] : $user_info->city . ' Real Estate | ' . $savedSearch[0]["Name"] . ' | ' . $user_info->first_name . ' ' . $user_info->last_name;
	 	   	$data['description'] = (isset($page_meta['meta_description']) && $page_meta['meta_description'] != '')? $page_meta['meta_description'] : $user_info->banner_tagline;
	 	    $data['canonical'] = (isset($page_meta['canonical']) && $page_meta['canonical'] != '')? $page_meta['canonical'] : '';
	 	    // ===================

	  		//get coordinates according to search
	  		$latitude = isset($data["featured"][0]['StandardFields']['Latitude']) ? $data["featured"][0]['StandardFields']['Latitude'] : "";
	  		$longitude = isset($data["featured"][0]['StandardFields']['Longitude']) ? $data["featured"][0]['StandardFields']['Longitude'] : "";

			if(empty($latitude) || empty($longitude) || $latitude == "********" || $longitude == "********") {

				$latlon = $this->getlatlon(isset($data["featured"][0]['StandardFields']['UnparsedAddress']) ? $data["featured"][0]['StandardFields']['UnparsedAddress'] :"");
				$latitude = $latlon['lat'];
				$longitude = $latlon['lon'];
			}

	  		$data["is_capture_leads"] = $this->home_model->get_capture_leads()->capture_leads;

	  		$data["property_types"] = Modules::run('dsl/get_property_types');
	   		$data["property_subtypes"] = Modules::run('dsl/get_property_subtypes');
	  		$data["js"] = array("listings.js","customer.js","selectize.js","advanced_search.js", "responsive-tabs.js");
			$data["css"] = array("selectize.bootstrap3.css");

	 		$savesearches = $this->customer->get_customer_searches();
	 		$savesearchesflex = $this->customer->getSaveSearchesFlex();
	 		$save_searches_flex = ( empty($savesearchesflex) ) ? array() : $savesearchesflex;
	        $data["saved_searches_db"] = array_merge($savesearches, $save_searches_flex);

	  		$data["protocol"] = $this->protocol();
	  		$data["profile"] = $this->customer->get_customer_profile();
	  		$mls_id = $data['account_info']->MlsId;
	 		$data['search_fields'] = $this->search_model->get_mls_search_data($mls_id);
	 		$data['search_fields_groups'] = $this->search_model->get_mls_search_group($mls_id);

    	$data = add_filter('before-saved-searches-view', $data);

  		$this->load->view("page", $data);
    }
  }

	function all_saved_searches(){
		$user_info = $this->home_model->getUsersInfo($this->domain_user->id);
		$obj = Modules::load("mysql_properties/mysql_properties");

		$data["theme"] = $user_info->theme;
		$data["header"] = "header-page";
		$data["page"] = "all_saved_searches";
		$data["title"] = "Saved Search";

	    // SEO meta title and meta description  - jovanni
    	$page_meta = $this->Meta_model->get_meta($this->domain_user->agent_id, 'saved_searches');
	    $data['title'] = (isset($page_meta['meta_title']) && $page_meta['meta_title'] != '')? $page_meta['meta_title'] : 'All Saved Searches | ' . $user_info->first_name . ' ' . $user_info->last_name . ' | ' .$user_info->phone;
	   	$data['description'] = (isset($page_meta['meta_description']) && $page_meta['meta_description'] != '')? $page_meta['meta_description'] : $user_info->banner_tagline;
	    $data['canonical'] = (isset($page_meta['canonical']) && $page_meta['canonical'] != '')? $page_meta['canonical'] : '';
	    // ===================

		$data['default_menu_data'] = Modules::run('menu/get_default_menu_data');
    	$data['custom_menu_data'] = Modules::run('menu/get_custom_menu_data');

    	$dsl = modules::load('dsl/dsl');
		$token = $this->home_model->getUserAccessToken($this->domain_user->agent_id);
		$access_token = array(
			'access_token' => $token->access_token
		);

		$this->load->library('pagination');
		$offset = (isset($_GET["offset"])) ? $_GET["offset"] : 0;
		$limit = 6;
		$selected_saved_search_count= Modules::run('dsl/getSelectedAgentSavedSearch', TRUE);
		$selected_saved_search = Modules::run('dsl/getSelectedAgentSavedSearch', FALSE, $limit, $offset);

		//check if dsl selected savedsearch is not empty
		if(!empty($selected_saved_search)){
			$savedSearchSelected = $selected_saved_search ;
		}else{
			$savedSearchSelected = $this->home_model->get_selected_saved_search();
		}

		if(!empty($savedSearchSelected)){

			$total = $selected_saved_search_count;
			$config["base_url"] = base_url().'/all_saved_searches';
			$config["total_rows"] = $total;
			$config["per_page"] = $limit;
			$config['page_query_string'] = TRUE;
			$config['query_string_segment'] = 'offset';
			$this->pagination->initialize($config);
			$data["pagination"] = $this->pagination->create_links();

			if( !empty( $this->idx_auth->checker() ) ){
				if( !empty($savedSearchSelected) ){
					foreach($savedSearchSelected as &$savedSelected) {

						$filterStr = "(".$savedSelected['Filter'].")";
						$filter_url = rawurlencode($filterStr);
						$filter_string = substr($filter_url, 3, -3);

						$endpoint = "spark-listings?_limit=1&_expand=PrimaryPhoto&_select=UnparsedFirstLineAddress,Photos.Uri300&_filter=(".$filter_string.")";
						$results = $dsl->post_endpoint($endpoint, $access_token);
						$data_results = json_decode($results, true);
						$savedSelected['data'] = (!empty($data_results["data"])) ? $data_results["data"][0]["StandardFields"] : "";
						$savedSelected['photo'] = (!empty($data_results["data"])) ? $data_results["data"][0]["Photos"]["Uri300"] : "";
		    	}
			  }
			}
			$data["saved_searches_selected"] = $savedSearchSelected;
		}

		$data["user_info"] = $user_info;
		$data['account_info'] = $obj->get_account($this->domain_user->id, "account_info");
		$data['system_info'] = $obj->get_account($this->domain_user->id, "system_info");
		$data['socialmedia_link'] = Modules::run('agent_social_media/agent_social_media/linksInfo');
		$data["is_capture_leads"] = $this->home_model->get_capture_leads()->capture_leads;
		$data["property_types"] = Modules::run('dsl/get_property_types');
 		$data["property_subtypes"] = Modules::run('dsl/get_property_subtypes');

		$data["js"] = array("listings.js");
		$data["protocol"] = $this->protocol();
		$this->load->view("page", $data);
	}

	function save_favorates_properties($property_id = NULL) {

		if($this->input->is_ajax_request()) {
			
			$response["success"] = FALSE;

			if(!empty($property_id)) {
				$response["success"] = ($this->customer->save_favorates_properties($property_id)) ? TRUE : FALSE;

				if($response["success"]){

					$liondesk_id = Modules::run('liondesk/getLiondeskContactId', $this->session->userdata('customer_id'));

					if($liondesk_id){
						$result = Modules::run('liondesk/createTask', array(
							'contact_id' => $liondesk_id,
					    'reminder_type_id' => 1,
					    'due_at' => date('Y-m-d H:i:s', strtotime("+5 min")) . 'Z',
					    'description' => 'Customer saved a property as favorite.',
					    'notes' => 'Saved Favorite Property ID : ' . $property_id,
						));
					}
				}
			}

			exit(json_encode($response));
		}
	}

	function remove_favorite_property($property_id = NULL) {

		if($this->input->is_ajax_request()) {

			$response["success"] = FALSE;

			if(!empty($property_id)) {
				$response["success"] = ($this->customer->remove_favorite_property($property_id)) ? TRUE : FALSE;
			}

			exit(json_encode($response));
		}
	}

	function save_search_properties() {

		if($this->input->is_ajax_request()) {

			$url = http_build_query($_GET);

			if(empty($url)) exit(json_encode(array("success" => FALSE)));

			$flag = ($this->customer->save_search_properties(urldecode($url))) ? TRUE : FALSE;

			if($flag){
				$liondesk_id = Modules::run('liondesk/getLiondeskContactId', $this->session->userdata('customer_id'));

				if($liondesk_id){
					$result = Modules::run('liondesk/createTask', array(
						'contact_id' => $liondesk_id,
				    'reminder_type_id' => 1,
				    'due_at' => date('Y-m-d H:i:s', strtotime("+5 min")) . 'Z',
				    'description' => 'Customer saved properties.',
				    'notes' => 'Saved search url: ' . base_url() . 'search-results?' . $url,
					));
				}
			}

			exit(json_encode(array("success"=>$flag, "redirect"=>urldecode($url))));
		}
	}

	function listings($slug="") {

		$getData = http_build_query($this->input->get());
		$searchProperty = Modules::run('search/get_search_property', $getData);

		if(isset($searchProperty['pagination'])) {
			$data['current_page'] = $searchProperty['pagination']['CurrentPage'];
			$data['totalrows'] = $searchProperty['pagination']['TotalRows'];
		}

		/* Query builder for pagination: (the old pagination gets the entire url including the offset variable which cause the offset variable duplicate) */
		$query_arr = array();

		foreach($_GET as $key=>$val) {
			if($key!="offset") {
				$query_arr[$key]=$val;
			}
		}

		if(isset($data['totalrows']) && isset($data['current_page'])) {
			$query_arr['totalrows']=$data['totalrows'];
		}

		$build_query = http_build_query($query_arr);
		/* End of query builder */

    	$data['page_data'] = $page_data = $this->page->get_page($slug);
		$this->load->library('pagination');

		$token = $this->dsl_model->getUserAccessToken($this->domain_user->agent_id);

		if($token) {

			if(!empty($this->idx_auth->checker())){

				if(isset($searchProperty["data"]) && !empty($searchProperty["data"])) {

					$data["featured"] = $searchProperty["data"];

					if($searchProperty["pagination"]) {

						$data["total_pages"] = $searchProperty["pagination"]["TotalPages"];
						$data["current_page"] = $searchProperty["pagination"]["CurrentPage"];
						$data["total_count"] = $searchProperty["pagination"]["TotalRows"];
						$data["page_size"] = $searchProperty["pagination"]["PageSize"];

						$param["limit"] = 25;
						$total = $searchProperty["pagination"]["TotalRows"];
						//$config["base_url"] = base_url().'/listings?'.http_build_query($_GET);
						$config["base_url"] = base_url().'/listings?'.$build_query;
						$config["total_rows"] = $total;
						$config["per_page"] = $param["limit"];
						$config['page_query_string'] = TRUE;
						$config['query_string_segment'] = 'offset';

						$this->pagination->initialize($config);
					}
				}
			}

		} else {

			$data["featured"] = $searchProperty["data"];

			if($searchProperty["pagination"]) {

				$data["total_pages"] = $searchProperty["pagination"]["TotalPages"];
				$data["current_page"] = $searchProperty["pagination"]["CurrentPage"];
				$data["total_count"] = $searchProperty["pagination"]["TotalRows"];
				$data["page_size"] = $searchProperty["pagination"]["PageSize"];

				$param["limit"] = 25;
				$total = $searchProperty["pagination"]["TotalRows"];
				//$config["base_url"] = base_url().'/listings?'.http_build_query($_GET);
				$config["base_url"] = base_url().'/listings?'.$build_query;
				$config["total_rows"] = $total;
				$config["per_page"] = $param["limit"];
				$config['page_query_string'] = TRUE;
				$config['query_string_segment'] = 'offset';

				$this->pagination->initialize($config);
			}
		}
		$data["pagination"] = $this->pagination->create_links();

		$savesearches = $this->customer->get_customer_searches();
		$savesearchesflex = $this->customer->getSaveSearchesFlex();
		$save_searches_flex = ( empty($savesearchesflex) ) ? array() : $savesearchesflex;
        $data["saved_searches_db"] = array_merge($savesearches, $save_searches_flex);

		$data["is_capture_leads"] = $this->home_model->get_capture_leads()->capture_leads;

 		$user_info = $this->home_model->getUsersInfo($this->domain_user->id);
 		$obj = Modules::load("mysql_properties/mysql_properties");

 		$data["property_types"] = Modules::run('dsl/get_property_types');
 		$data["property_subtypes"] = Modules::run('dsl/get_property_subtypes');
		$data["theme"] = $user_info->theme;
		$data["header"] = "header-page";
		$data["page"] = "listings";
		$data["title"] = "Search Results";

	    // SEO meta title and meta description  - jovanni
	    $page_meta = $this->Meta_model->get_meta($this->domain_user->agent_id, 'listings?' . http_build_query($_GET));
	    $data['title'] = (isset($page_meta['meta_title']) && $page_meta['meta_title'] != '')? $page_meta['meta_title'] : 'Search Results Listings | ' . $user_info->first_name . ' ' . $user_info->last_name . ' | ' .$user_info->phone;
	   	$data['description'] = (isset($page_meta['meta_description']) && $page_meta['meta_description'] != '')? $page_meta['meta_description'] : $user_info->banner_tagline;
	    $data['canonical'] = (isset($page_meta['canonical']) && $page_meta['canonical'] != '')? $page_meta['canonical'] : '';
	    $data['h1'] = 'Search Listings';
	    // ===================

		$data['default_menu_data'] = Modules::run('menu/get_default_menu_data');
    	$data['custom_menu_data'] = Modules::run('menu/get_custom_menu_data');

    	$data["user_info"] = $user_info;
		$data['account_info'] = $obj->get_account($this->domain_user->id, "account_info");
		$data['system_info'] = $obj->get_account($this->domain_user->id, "system_info");
		$data['socialmedia_link'] = Modules::run('agent_social_media/agent_social_media/linksInfo');

		$data["js"] = array("listings.js","customer.js");
		$data["protocol"] = $this->protocol();
		$data["profile"] = $this->customer->get_customer_profile();
    	//printA($data["featured"]); exit;
		$this->load->view("page", $data);

	}

	function nearby_listings() {
		$this->load->library('pagination');

		$user_info = $this->home_model->getUsersInfo($this->domain_user->id);
		$obj = Modules::load("mysql_properties/mysql_properties");

		$data["theme"] = $user_info->theme;
		$data["header"] = "header-page";
		$data["page"] = "advance_search";
		$data["title"] = "Nearby Listings";

	    // SEO meta title and meta description  - jovanni
	    $page_meta = $this->Meta_model->get_meta($this->domain_user->agent_id, 'nearby-listings');
	    $data['title'] = (isset($page_meta['meta_title']) && $page_meta['meta_title'] != '')? $page_meta['meta_title'] : 'Nearby Listings | ' . $user_info->first_name . ' ' . $user_info->last_name . ' | ' .$user_info->phone;
	   	$data['description'] = (isset($page_meta['meta_description']) && $page_meta['meta_description'] != '')? $page_meta['meta_description'] : $user_info->banner_tagline;
	    $data['canonical'] = (isset($page_meta['canonical']) && $page_meta['canonical'] != '')? $page_meta['canonical'] : '';
	    // ===================

		$data['default_menu_data'] = Modules::run('menu/get_default_menu_data');
    	$data['custom_menu_data'] = Modules::run('menu/get_custom_menu_data');

    	$data["user_info"] = $user_info;
    	$data["property_types"] = Modules::run('dsl/get_property_types');
 		$data["property_subtypes"] = Modules::run('dsl/get_property_subtypes');
		$data['account_info'] = $obj->get_account($this->domain_user->id, "account_info");
		$data['system_info'] = $obj->get_account($this->domain_user->id, "system_info");
		$data['socialmedia_link'] = Modules::run('agent_social_media/agent_social_media/linksInfo');
		$gsf_fields = $this->home_model->get_gsf_fields($this->domain_user->agent_id);
		$filterGeo = '';
		
		if(isset($_GET['page'])) {
			$page = $_GET['page'];
		}
		else {
			$page = 1;
		}

		$data['limit'] = $limit = 10;
		$totalrows = (isset($_GET['totalrows'])) ? $_GET['totalrows'] : 0;

		if(isset($_GET['offset']) && !isset($_GET['page'])) {

			$total_page = $totalrows / $limit;
			$remainder = $totalrows % $limit;

			if($remainder != 0){
				$total_page++;
			}

			$total_page = intval($total_page);

			$actual_page = $_GET['offset'] / $limit;
			$actual_page = intval($actual_page);
			$actual_page = ($actual_page <= 0) ? 1 : $actual_page;

			if($actual_page < $total_page) {
				$page += $actual_page;
			}
		}

		$order = "-OnMarketDate";
		if($this->input->get('order') == 'highest') {
			$order = "-ListPrice";
		}
		if($this->input->get('order') == 'lowest') {
			$order = "+ListPrice";
		}
		if($this->input->get('order') == 'newest') {
			$order = "-OnMarketDate";
		}
		if($this->input->get('order') == 'oldest') {
			$order = "+YearBuilt";
		}

		$dsl = modules::load('dsl/dsl');
		$token = $this->home_model->getUserAccessToken($this->domain_user->agent_id);
		$access_token = array(
			'access_token' => $token->access_token
		);

		$filter_string = createFilterString($gsf_fields);

		$latlon_info = $this->home_model->get_account_latlon($this->domain_user->agent_id);

			//latlon from DB
		if(!empty($latlon_info->latitude) && !empty($latlon_info->longitude)){

				$latlon = array(

					"lat" => $latlon_info->latitude,
					"lon" => $latlon_info->longitude
				);

			//latlon from google geocoding api
		} elseif(!empty($data["account_info"]->Addresses[0]->Address) && $data["account_info"]->Addresses[0]->Address != "********"){

			$latlon = $this->getlatlon(isset($data["account_info"]->Addresses[0]->Address) ? $data["account_info"]->Addresses[0]->Address : "");

		} else {

			$latlon = Modules::run('mysql_properties/get_nearby_coordinates', $this->domain_user->id);
		}

		$selectFields = "UnparsedAddress,CurrentPrice,StateOrProvince,UnparsedFirstLineAddress,City,PostalCity,PostalCode,Latitude,Longitude,LotSizeArea,LotSizeUnits,LotSizeSquareFeet,LotSizeAcres,ListingKey,ListAgentId,LotSizeDimensions,ListingUpdateTimestamp,ModificationTimestamp,MajorChangeTimestamp,BathsTotal,BedsTotal,BuildingAreaTotal,PropertyClass,Photos.Uri300,MlsStatus";

		$nearby_listings_data = Modules::run('dsl/getSparkProxyComplete', 'listings', "_limit=".$limit."&_page=".$page."&_pagination=1&_lat=".$latlon['lat']."&_lon=".$latlon['lon']."&_distance=20&_units=miles&_expand=PrimaryPhoto&_filter=(".$filter_string.")&_select=".$selectFields."&_orderby=".$order, true);
		//printA($nearby_listings_data);exit;

		if(isset($nearby_listings_data['D']['Pagination'])) {
			$data['current_page'] = $nearby_listings_data['D']['Pagination']['CurrentPage'];
			$data['totalrows'] = $nearby_listings_data['D']['Pagination']['TotalRows'];
		}

		$query_arr = array();

		foreach($_GET as $key=>$val) {
			if($key!="offset") {
				$query_arr[$key]=$val;
			}
		}

		if(isset($data['totalrows']) && isset($data['current_page'])) {
			$query_arr['totalrows']=$data['totalrows'];
		}
		$build_query = http_build_query($query_arr);

		if(isset($nearby_listings_data['D']['Pagination'])) {
			$data['total_pages'] = $nearby_listings_data['D']['Pagination']['TotalPages'];
			$data['current_page'] = $nearby_listings_data['D']['Pagination']['CurrentPage'];
			$data['total_count'] = $nearby_listings_data['D']['Pagination']['TotalRows'];
			$data['page_size'] = $nearby_listings_data['D']['Pagination']['PageSize'];

			$param["limit"] = 10;
			$total = $nearby_listings_data['D']['Pagination']['TotalRows'];
			$config["base_url"] = base_url().'/nearby_listings?'.$build_query;
			$config["total_rows"] = $total;
			$config["per_page"] = $param["limit"];
			$config['page_query_string'] = TRUE;
			$config['query_string_segment'] = 'offset';

			$this->pagination->initialize($config);
		}

		$data["featured"] = isset($nearby_listings_data['D']['Results']) ? $nearby_listings_data['D']['Results'] : "";

		//get coordinates according to search
		$latitude = isset($data["featured"][0]['StandardFields']['Latitude']) ? $data["featured"][0]['StandardFields']['Latitude'] : "";
		$longitude = isset($data["featured"][0]['StandardFields']['Longitude']) ? $data["featured"][0]['StandardFields']['Longitude'] : "";

		if(empty($latitude) || empty($longitude) || $latitude == "********" || $longitude == "********") {

			$latlon = $this->getlatlon(isset($data["featured"][0]['StandardFields']['UnparsedAddress']) ? $data["featured"][0]['StandardFields']['UnparsedAddress'] : "" );
			$latitude = $latlon['lat'];
			$longitude = $latlon['lon'];
		}

		$data['coordinates'] = array('lat'=>$latitude, 'lon'=>$longitude);
		$data["pagination"] = $this->pagination->create_links();
		$data["protocol"] = $this->protocol();
		$data["is_capture_leads"] = $this->home_model->get_capture_leads()->capture_leads;
		$data["profile"] = $this->customer->get_customer_profile();
		$data["js"] = array("listings.js","customer.js","selectize.js","advanced_search.js");
		$data["css"] = array("selectize.bootstrap3.css");
		$mls_id = $data['account_info']->MlsId;
		$data['search_fields'] = $this->search_model->get_mls_search_data($mls_id);
		$data['search_fields_groups'] = $this->search_model->get_mls_search_group($mls_id);

		$this->load->view("page", $data);
	}

	function new_listings() {

		$this->load->library('pagination');

		$user_info = $this->home_model->getUsersInfo($this->domain_user->id);
		$obj = Modules::load("mysql_properties/mysql_properties");

		$data["theme"] = $user_info->theme;
		$data["header"] = "header-page";
		$data["page"] = "advance_search";
		$data["title"] = "New Listings";

	    // SEO meta title and meta description  - jovanni
	    $page_meta = $this->Meta_model->get_meta($this->domain_user->agent_id, 'new-listings');
	    $data['title'] = (isset($page_meta['meta_title']) && $page_meta['meta_title'] != '')? $page_meta['meta_title'] : 'New Listings | ' . $user_info->first_name . ' ' . $user_info->last_name . ' | ' .$user_info->phone;
	   	$data['description'] = (isset($page_meta['meta_description']) && $page_meta['meta_description'] != '')? $page_meta['meta_description'] : $user_info->banner_tagline;
	    $data['canonical'] = (isset($page_meta['canonical']) && $page_meta['canonical'] != '')? $page_meta['canonical'] : '';
	    // ===================

		$data['default_menu_data'] = Modules::run('menu/get_default_menu_data');
    	$data['custom_menu_data'] = Modules::run('menu/get_custom_menu_data');

    	$data["user_info"] = $user_info;
    	$data["property_types"] = Modules::run('dsl/get_property_types');
 		$data["property_subtypes"] = Modules::run('dsl/get_property_subtypes');
		$data['account_info'] = $obj->get_account($this->domain_user->id, "account_info");
		$data['system_info'] = $obj->get_account($this->domain_user->id, "system_info");
		$data['socialmedia_link'] = Modules::run('agent_social_media/agent_social_media/linksInfo');
		$gsf_fields = $this->home_model->get_gsf_fields($this->domain_user->agent_id);
  		$filterGeo = '';

		if(isset($_GET['page'])) {
			$page = $_GET['page'];
		}
		else {
			$page = 1;
		}

		$data['limit'] = $limit = 10;
		$totalrows = (isset($_GET['totalrows'])) ? $_GET['totalrows'] : 0;

		if(isset($_GET['offset']) && !isset($_GET['page'])) {

			$total_page = $totalrows / $limit;
			$remainder = $totalrows % $limit;

			if($remainder != 0){
				$total_page++;
			}

			$total_page = intval($total_page);

			$actual_page = $_GET['offset'] / $limit;
			$actual_page = intval($actual_page);
			$actual_page = ($actual_page <= 0) ? 1 : $actual_page;

			if($actual_page < $total_page) {
				$page += $actual_page;
			}
		}

		$order = "-OnMarketDate";
		if($this->input->get('order') == 'highest') {
			$order = "-ListPrice";
		}
		if($this->input->get('order') == 'lowest') {
			$order = "+ListPrice";
		}
		if($this->input->get('order') == 'newest') {
			//$order = "-YearBuilt,OnMarketDate";
			$order = "-OnMarketDate";
		}
		if($this->input->get('order') == 'oldest') {
			$order = "+YearBuilt";
		}
		
		$dsl = modules::load('dsl/dsl');
		$token = $this->home_model->getUserAccessToken($this->domain_user->agent_id);
		$access_token = array(
			'access_token' => $token->access_token
		);

		$filter_string = createFilterString($gsf_fields, "new_listings");

		$selectFields = "UnparsedAddress,CurrentPrice,StateOrProvince,UnparsedFirstLineAddress,City,PostalCity,PostalCode,Latitude,Longitude,LotSizeArea,LotSizeUnits,LotSizeSquareFeet,LotSizeAcres,ListingKey,ListAgentId,LotSizeDimensions,ListingUpdateTimestamp,ModificationTimestamp,MajorChangeTimestamp,BathsTotal,BedsTotal,BuildingAreaTotal,PropertyClass,Photos.Uri300,MlsStatus,MlsId,OnMarketDate,OnMarketContractDate,PublicRemarks,CountyOrParish";

		$new_listings_data = Modules::run('dsl/getSparkProxyComplete', 'listings', "_limit=".$limit."&_page=".$page."&_pagination=1&_expand=PrimaryPhoto&_filter=(".$filter_string.")&_select=".$selectFields."&_orderby=".$order, true);
		//printA($new_listings_data);exit;

		if(isset($new_listings_data['D']['Pagination'])) {
			$data['current_page'] = $new_listings_data['D']['Pagination']['CurrentPage'];
			$data['totalrows'] = $new_listings_data['D']['Pagination']['TotalRows'];
		}

		$query_arr = array();

		foreach($_GET as $key=>$val) {
			if($key!="offset") {
				$query_arr[$key]=$val;
			}
		}

		if(isset($data['totalrows']) && isset($data['current_page'])) {
			$query_arr['totalrows']=$data['totalrows'];
		}
		$build_query = http_build_query($query_arr);

		if(isset($new_listings_data['D']['Pagination'])) {
			$data['total_pages'] = $new_listings_data['D']['Pagination']['TotalPages'];
			$data['current_page'] = $new_listings_data['D']['Pagination']['CurrentPage'];
			$data['total_count'] = $new_listings_data['D']['Pagination']['TotalRows'];
			$data['page_size'] = $new_listings_data['D']['Pagination']['PageSize'];

			$param["limit"] = 10;
			$total = $new_listings_data['D']['Pagination']['TotalRows'];
			$config["base_url"] = base_url().'/new_listings?'.$build_query;
			$config["total_rows"] = $total;
			$config["per_page"] = $param["limit"];
			$config['page_query_string'] = TRUE;
			$config['query_string_segment'] = 'offset';

			$this->pagination->initialize($config);
		}

		$data["featured"] = isset($new_listings_data['D']['Results']) ? $new_listings_data['D']['Results'] : "";

		//get coordinates according to search
		$latitude = isset($data["featured"][0]['StandardFields']['Latitude']) ? $data["featured"][0]['StandardFields']['Latitude'] : "";
		$longitude = isset($data["featured"][0]['StandardFields']['Longitude']) ? $data["featured"][0]['StandardFields']['Longitude'] : "";

		if(empty($latitude) || empty($longitude) || $latitude == "********" || $longitude == "********") {

			$latlon = $this->getlatlon(isset($data["featured"][0]['StandardFields']['UnparsedAddress']) ? $data["featured"][0]['StandardFields']['UnparsedAddress'] : "" );
			$latitude = $latlon['lat'];
			$longitude = $latlon['lon'];
		}

		$data['coordinates'] = array('lat'=>$latitude, 'lon'=>$longitude);
		$data["pagination"] = $this->pagination->create_links();
		$data["protocol"] = $this->protocol();
		$data["is_capture_leads"] = $this->home_model->get_capture_leads()->capture_leads;
		$data["profile"] = $this->customer->get_customer_profile();
		$data["js"] = array("listings.js","customer.js","selectize.js","advanced_search.js");
		$data["css"] = array("selectize.bootstrap3.css");
		$mls_id = $data['account_info']->MlsId;
		$data['search_fields'] = $this->search_model->get_mls_search_data($mls_id);
		$data['search_fields_groups'] = $this->search_model->get_mls_search_group($mls_id);

		$data = add_filter('before-new-listings-view', $data);

		$this->load->view("page", $data);
	}

	function new_listings_search() {

		$this->load->library('pagination');
        
		$user_info = $this->home_model->getUsersInfo($this->domain_user->id);
		$obj = Modules::load("mysql_properties/mysql_properties");

		$data["theme"] = $user_info->theme;
		$data["header"] = "header-page";
		$data["page"] = "advance_search";
		$data["title"] = "New Listings";

		$data['default_menu_data'] = Modules::run('menu/get_default_menu_data');
		$data['custom_menu_data'] = Modules::run('menu/get_custom_menu_data');
		
		$data['account_info'] = $obj->get_account($this->domain_user->id, "account_info");
		$data['system_info'] = $obj->get_account($this->domain_user->id, "system_info");
		$data['socialmedia_link'] = Modules::run('agent_social_media/agent_social_media/linksInfo');

		$page = isset($_GET['page']) ? $_GET['page'] : 1; 

		$data['limit'] = $limit = 10;
		$totalrows = (isset($_GET['totalrows'])) ? $_GET['totalrows'] : 0;

		if(isset($_GET['offset']) && !isset($_GET['page'])) {

			$total_page = $totalrows / $limit;
			$remainder = $totalrows % $limit;

			if($remainder != 0){
				$total_page++;
			}

			$total_page = intval($total_page);

			$actual_page = $_GET['offset'] / $limit;
			$actual_page = intval($actual_page);
			$actual_page = ($actual_page <= 0) ? 1 : $actual_page;

			if($actual_page < $total_page) {
				$page += $actual_page;
			}
		}

		$orderby = $this->input->get('order');

		switch ($orderby) {
			case 'highest':
				$order = "-ListPrice";
				break;
			
			case 'lowest':
				$order = "+ListPrice";
				break;

			case 'newest':
				$order = "-OnMarketDate";
				break;

			case 'oldest':
				$order = "+YearBuilt";
				break;

			default:
				$order = "-ListPrice";
				break;
		}

		$dsl = modules::load('dsl/dsl');
		$token = $this->home_model->getUserAccessToken($this->domain_user->agent_id);
		$access_token = array('access_token' => $token->access_token);

		$filterStr = "(MlsStatus Ne 'Closed' Or MlsStatus Ne 'Sold') And (OnMarketDate Bt ".$this->input->get('from_time').",".$this->input->get('to_time').") And (PublicRemarks Eq '*".$this->input->get('Search')."*' Or UnparsedAddress Eq '*".$this->input->get('Search')."*')";

		$filter_url = rawurlencode($filterStr);
		$filter_string = substr($filter_url, 3, -3);

		$endpoint = "spark-listings?_pagination=1&_page=".$page."&_limit=".$limit."&_expand=PrimaryPhoto&_filter=(".$filter_string.")&_orderby=".$order;
		$results = $dsl->post_endpoint($endpoint, $access_token);
		$new_listings['results'] = json_decode($results, true);
		$data['new_listings_data'] = $new_listings['results'];

		if(isset($data['new_listings_data']['pagination'])) {
			$data['current_page'] = $data['new_listings_data']['pagination']['CurrentPage'];
			$data['totalrows'] = $data['new_listings_data']['pagination']['TotalRows'];
		}

		$query_arr = array();

		foreach($_GET as $key=>$val) {
			if($key!="offset") {
				$query_arr[$key]=$val;
			}
		}

		if(isset($data['totalrows']) && isset($data['current_page'])) {
			$query_arr['totalrows']=$data['totalrows'];
		}

		$build_query = http_build_query($query_arr);

		if(!empty($data['new_listings_data']['data'])) {

			$data['total_pages'] = $data['new_listings_data']['pagination']['TotalPages'];
			$data['current_page'] = $data['new_listings_data']['pagination']['CurrentPage'];
			$data['total_count'] = $data['new_listings_data']['pagination']['TotalRows'];
			$data['page_size'] = $data['new_listings_data']['pagination']['PageSize'];

			$total = $data['new_listings_data']['pagination']['TotalRows'];
			$config["base_url"] = base_url().'/home/new_listings_search?'.$build_query;
			$config["total_rows"] = $total;
			$config["per_page"] = $limit;
			$config['page_query_string'] = TRUE;
			$config['query_string_segment'] = 'offset';

			$this->pagination->initialize($config);
		}


		$data["featured"] = isset($data['new_listings_data']['data']) ? $data['new_listings_data']['data'] : "";

		//get coordinates according to search
		$latitude = isset($data["featured"][0]['StandardFields']['Latitude']) ? $data["featured"][0]['StandardFields']['Latitude'] : "";
		$longitude = isset($data["featured"][0]['StandardFields']['Longitude']) ? $data["featured"][0]['StandardFields']['Longitude'] : "";

		if(empty($latitude) || empty($longitude) || $latitude == "********" || $longitude == "********") {
			$latlon = $this->getlatlon(isset($data["featured"][0]['StandardFields']['UnparsedAddress']) ? $data["featured"][0]['StandardFields']['UnparsedAddress'] :"" );
			$latitude = $latlon['lat'];
			$longitude = $latlon['lon'];
		}

		$data['coordinates'] = array('lat'=>$latitude, 'lon'=>$longitude);

		$data["pagination"] = $this->pagination->create_links();
		$data["protocol"] = $this->protocol();
		$data["is_capture_leads"] = $this->home_model->get_capture_leads()->capture_leads;
		$data["profile"] = $this->customer->get_customer_profile();
		$data["js"] = array("listings.js","customer.js","selectize.js","advanced_search.js");
		$data["css"] = array("selectize.bootstrap3.css");
		$mls_id = $data['account_info']->MlsId;
		$data['search_fields'] = $this->search_model->get_mls_search_data($mls_id);
		$data['search_fields_groups'] = $this->search_model->get_mls_search_group($mls_id);

		$this->load->view("page", $data);

	}

	function sold_listings(){

		$this->load->library('pagination');
		$user_info = $this->home_model->getUsersInfo($this->domain_user->id);
		$obj = Modules::load("mysql_properties/mysql_properties");

		$data["theme"] = $user_info->theme;
		$data["header"] = "header-page";
		$data["page"] = "all_sold_listings";
		$data["title"] = "Sold Listings";

	    // SEO meta title and meta description  - jovanni
	    $page_meta = $this->Meta_model->get_meta($this->domain_user->agent_id, 'sold-listings');
	    $data['title'] = (isset($page_meta['meta_title']) && $page_meta['meta_title'] != '')? $page_meta['meta_title'] : 'Sold Listings | ' . $user_info->first_name . ' ' . $user_info->last_name . ' | ' .$user_info->phone;
	   	$data['description'] = (isset($page_meta['meta_description']) && $page_meta['meta_description'] != '')? $page_meta['meta_description'] : $user_info->banner_tagline;
	    $data['canonical'] = (isset($page_meta['canonical']) && $page_meta['canonical'] != '')? $page_meta['canonical'] : '';
	    // ===================

		$data['default_menu_data'] = Modules::run('menu/get_default_menu_data');
    	$data['custom_menu_data'] = Modules::run('menu/get_custom_menu_data');
    	$data["user_info"] = $user_info;
		$data['account_info'] = $obj->get_account($this->domain_user->id, "account_info");
		$data['system_info'] = $obj->get_account($this->domain_user->id, "system_info");
		$data['socialmedia_link'] = Modules::run('agent_social_media/agent_social_media/linksInfo');
		
		$param = array();
		$param["offset"] = (isset($_GET["offset"])) ? $_GET["offset"] : 0;
		$param["limit"] = 12;

		$selected_sold_listings = $this->home_model->get_sold_listings_selected();
	 	if(!empty($selected_sold_listings)){
	 		$data["total_sold_listings"] = count($selected_sold_listings);
		} else{
			$data["total_sold_listings"] = $this->home_model->getSoldListingsData(TRUE);
		}
		$data["sold_listings"] = $this->getSoldListingsData(FALSE, $param);
		$config["base_url"] = base_url().'/sold_listings';
		$config["total_rows"] = $data["total_sold_listings"];
		$config["per_page"] = $param["limit"];
		$config['page_query_string'] = TRUE;
		$config['query_string_segment'] = 'offset';
		$this->pagination->initialize($config);
		$data["pagination"] = $this->pagination->create_links();

	 	$data["protocol"] = $this->protocol();
		$data["is_capture_leads"] = $this->home_model->get_capture_leads()->capture_leads;

		$data["js"] = array("listings.js");

		$this->load->view("page", $data);
	}

	public function ajax_sold_listings($counter = false, $param = array()){
		$param['limit'] = 8;
		echo json_encode($this->home_model->getSoldListingsData(false,$param));
	}

	public function getSoldListingsData($counter = FALSE, $param = array() ){

		$soldListings = $this->home_model->getSoldListingsData(FALSE, $param);

		return $soldListings;
	}

	function office_listings() {

		$this->load->library('pagination');

		$user_info = $this->home_model->getUsersInfo($this->domain_user->id);
		$obj = Modules::load("mysql_properties/mysql_properties");

		$data["theme"] = $user_info->theme;
		$data["header"] = "header-page";
		$data["page"] = "advance_search";
		$data["title"] = "Office Listings";

	    // SEO meta title and meta description  - jovanni
	    $page_meta = $this->Meta_model->get_meta($this->domain_user->agent_id, 'office-listings');
	    $data['title'] = (isset($page_meta['meta_title']) && $page_meta['meta_title'] != '')? $page_meta['meta_title'] : 'Office Listings | ' . $user_info->first_name . ' ' . $user_info->last_name . ' | ' .$user_info->phone;
	   	$data['description'] = (isset($page_meta['meta_description']) && $page_meta['meta_description'] != '')? $page_meta['meta_description'] : $user_info->banner_tagline;
	    $data['canonical'] = (isset($page_meta['canonical']) && $page_meta['canonical'] != '')? $page_meta['canonical'] : '';
	    // ===================

		$data['default_menu_data'] = Modules::run('menu/get_default_menu_data');
    	$data['custom_menu_data'] = Modules::run('menu/get_custom_menu_data');

    	$data["user_info"] = $user_info;
    	$data["property_types"] = Modules::run('dsl/get_property_types');
 		$data["property_subtypes"] = Modules::run('dsl/get_property_subtypes');
		$data['account_info'] = $obj->get_account($this->domain_user->id, "account_info");
		$data['system_info'] = $obj->get_account($this->domain_user->id, "system_info");
		$data['socialmedia_link'] = Modules::run('agent_social_media/agent_social_media/linksInfo');

		$page = (isset($_GET['page'])) ? $_GET['page'] : 1;

		$data['limit'] = $limit = 10;
		$totalrows = (isset($_GET['totalrows'])) ? $_GET['totalrows'] : 0;

		if(isset($_GET['offset']) && !isset($_GET['page'])) {

			$total_page = $totalrows / $limit;
			$remainder = $totalrows % $limit;

			if($remainder != 0){
				$total_page++;
			}

			$total_page = intval($total_page);

			$actual_page = $_GET['offset'] / $limit;
			$actual_page = intval($actual_page);
			$actual_page = ($actual_page <= 0) ? 1 : $actual_page;

			if($actual_page < $total_page) {
				$page += $actual_page;
			}
		}

		$order = "-OnMarketDate";
		if($this->input->get('order') == 'highest') {
			$order = "-ListPrice";
		}
		if($this->input->get('order') == 'lowest') {
			$order = "+ListPrice";
		}
		if($this->input->get('order') == 'newest') {
			$order = "-OnMarketDate";
		}
		if($this->input->get('order') == 'oldest') {
			$order = "+YearBuilt";
		}

		$dsl = modules::load('dsl/dsl');
		$token = $this->home_model->getUserAccessToken($this->domain_user->agent_id);
		$access_token = array(
			'access_token' => $token->access_token
		);

		$filterStr = "(MlsStatus Ne 'Closed','Sold','Leased','Pending','Rented')";
		$filter_url = rawurlencode($filterStr);
		$filter_string = substr($filter_url, 3, -3);

		$selectFields = "UnparsedAddress,CurrentPrice,StateOrProvince,UnparsedFirstLineAddress,City,PostalCity,PostalCode,Latitude,Longitude,LotSizeArea,LotSizeUnits,LotSizeSquareFeet,LotSizeAcres,ListingKey,ListAgentId,LotSizeDimensions,ListingUpdateTimestamp,ModificationTimestamp,BathsTotal,BedsTotal,BuildingAreaTotal,PropertyClass,Photos.Uri300,MlsStatus,CountyOrParish,MlsId,PublicRemarks";

		$office_listings_data = Modules::run('dsl/getSparkProxyComplete', 'office/listings', "_limit=".$limit."&_page=".$page."&_pagination=1&_expand=PrimaryPhoto&_filter=(".$filter_string.")&_select=".$selectFields."&_orderby=".$order, true);
		//printA($office_listings_data);exit;

		if(isset($office_listings_data['D']['Pagination'])) {
			$data['current_page'] = $office_listings_data['D']['Pagination']['CurrentPage'];
			$data['totalrows'] = $office_listings_data['D']['Pagination']['TotalRows'];
		}

		$query_arr = array();

		foreach($_GET as $key=>$val) {
			if($key!="offset") {
				$query_arr[$key]=$val;
			}
		}

		if(isset($data['totalrows']) && isset($data['current_page'])) {
			$query_arr['totalrows']=$data['totalrows'];
		}
		$build_query = http_build_query($query_arr);

		if(isset($office_listings_data['D']['Pagination'])) {
			$data['total_pages'] = $office_listings_data['D']['Pagination']['TotalPages'];
			$data['current_page'] = $office_listings_data['D']['Pagination']['CurrentPage'];
			$data['total_count'] = $office_listings_data['D']['Pagination']['TotalRows'];
			$data['page_size'] = $office_listings_data['D']['Pagination']['PageSize'];

			$param["limit"] = 10;
			$total = $office_listings_data['D']['Pagination']['TotalRows'];
			$config["base_url"] = base_url().'/office_listings?'.$build_query;
			$config["total_rows"] = $total;
			$config["per_page"] = $param["limit"];
			$config['page_query_string'] = TRUE;
			$config['query_string_segment'] = 'offset';

			$this->pagination->initialize($config);
		}

		$data["featured"] = isset($office_listings_data['D']['Results']) ? $office_listings_data['D']['Results'] : "";

		//get coordinates according to search
		$latitude = isset($data["featured"][0]['StandardFields']['Latitude']) ? $data["featured"][0]['StandardFields']['Latitude'] : "";
		$longitude = isset($data["featured"][0]['StandardFields']['Longitude']) ? $data["featured"][0]['StandardFields']['Longitude'] : "";

		if(empty($latitude) || empty($longitude) || $latitude == "********" || $longitude == "********") {

			$latlon = $this->getlatlon(isset($data["featured"][0]['StandardFields']['UnparsedAddress']) ? $data["featured"][0]['StandardFields']['UnparsedAddress'] : "" );
			$latitude = $latlon['lat'];
			$longitude = $latlon['lon'];
		}

		$data['coordinates'] = array('lat'=>$latitude, 'lon'=>$longitude);
		$data["pagination"] = $this->pagination->create_links();
		$data["js"] = array("listings.js","customer.js");
		$data["protocol"] = $this->protocol();
		$data["profile"] = $this->customer->get_customer_profile();
		$data["is_capture_leads"] = $this->home_model->get_capture_leads()->capture_leads;
		$data["js"] = array("listings.js","customer.js","selectize.js","advanced_search.js");
		$data["css"] = array("selectize.bootstrap3.css");
		$mls_id = $data['account_info']->MlsId;
		$data['search_fields'] = $this->search_model->get_mls_search_data($mls_id);
		$data['search_fields_groups'] = $this->search_model->get_mls_search_group($mls_id);

		$data = add_filter('before-office-listings-view', $data);
		$this->load->view("page", $data);
	}

	function active_listings() {

	  $this->load->library('pagination');

		$user_info = $this->home_model->getUsersInfo($this->domain_user->id);
		$obj = Modules::load("mysql_properties/mysql_properties");

		$data["theme"] = $user_info->theme;
		$data["header"] = "header-page";
		$data["page"] = "advance_search";
		$data["title"] = "Active Listings";

	    // SEO meta title and meta description  - jovanni
	    $page_meta = $this->Meta_model->get_meta($this->domain_user->agent_id, 'active-listings');
	    $data['title'] = (isset($page_meta['meta_title']) && $page_meta['meta_title'] != '')? $page_meta['meta_title'] : 'Active Listings | ' . $user_info->first_name . ' ' . $user_info->last_name . ' | ' .$user_info->phone;
	    $data['description'] = (isset($page_meta['meta_description']) && $page_meta['meta_description'] != '')? $page_meta['meta_description'] : $user_info->banner_tagline;
	    $data['canonical'] = (isset($page_meta['canonical']) && $page_meta['canonical'] != '')? $page_meta['canonical'] : '';
	    // ===================

	  	$data['default_menu_data'] = Modules::run('menu/get_default_menu_data');
	    $data['custom_menu_data'] = Modules::run('menu/get_custom_menu_data');

	    $data["user_info"] = $user_info;
	    $data["property_types"] = Modules::run('dsl/get_property_types');
	  	$data["property_subtypes"] = Modules::run('dsl/get_property_subtypes');
	  	$data['account_info'] = $obj->get_account($this->domain_user->id, "account_info");
	  	$data['system_info'] = $obj->get_account($this->domain_user->id, "system_info");
	  	$data['socialmedia_link'] = Modules::run('agent_social_media/agent_social_media/linksInfo');

	  	$page = (isset($_GET['page'])) ? $_GET['page'] : 1;

	  	$data['limit'] = $limit = 10;
	  	$totalrows = (isset($_GET['totalrows'])) ? $_GET['totalrows'] : 0;

	  	if(isset($_GET['offset']) && !isset($_GET['page'])) {

		    $total_page = $totalrows / $limit;
		    $remainder = $totalrows % $limit;

		    if($remainder != 0){
		      $total_page++;
		    }

		    $total_page = intval($total_page);

		    $actual_page = $_GET['offset'] / $limit;
		    $actual_page = intval($actual_page);
		    $actual_page = ($actual_page <= 0) ? 1 : $actual_page;

		    if($actual_page < $total_page) {
		      $page += $actual_page;
		    }
	  	}

	  	$order = "-OnMarketDate";

		if($this->input->get('order') == 'highest') {
			$order = "-ListPrice";
		}
		if($this->input->get('order') == 'lowest') {
			$order = "+ListPrice";
		}
		if($this->input->get('order') == 'newest') {
			$order = "-OnMarketDate";
		}
		if($this->input->get('order') == 'oldest') {
			$order = "+YearBuilt";
		}


		//=====================ACTIVE LISTINGS: DIRECT TO SPARK======================//
		$dsl = modules::load('dsl/dsl');
		$token = $this->home_model->getUserAccessToken($this->domain_user->agent_id);
		$access_token = array('access_token' => $token->access_token);
		$filterStr = "(MlsStatus Ne 'Closed','Sold','Leased','Pending','Rented')";
		
		//DEV-1765: Using this filter for specific MLS bec. we can't filter LEASED status on this MLS
		//Pasadena-Foothills Board of Realtors MLS
		if($data["system_info"]->MlsId == "20170119210918715503000000"){
			$filterStr = "(MlsStatus Eq 'Active' Or MlsStatus Eq 'Active Under Contract')";
		}
		$filter_url = rawurlencode($filterStr);
		$filter_string = substr($filter_url, 3, -3);

		$url = "https://sparkapi.com/v1/my/listings?_pagination=1&_page=".$page."&_expand=PrimaryPhoto&_limit=".$limit."&_filter=(".$filter_string.")&_orderby=".$order."";

		$headers = array(
			'Content-Type: application/json',
			'User-Agent: Spark API PHP Client/2.0',
			'X-SparkApi-User-Agent: PHP-APP-In-Fifteen-Minutes/1.0',
		  'Authorization: OAuth '. $access_token["access_token"],
		  'Accept-Encoding: gzip, deflate',
		);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_URL, $url);

		$result = curl_exec($ch);
		$unzipped_data = gunzip($result);
		$result_info = json_decode($unzipped_data, true);


		if(empty($result_info)) {
			$result_info = curl_strerror($ch);
		}
    curl_close($ch);

		if(isset($result_info["D"]["Results"]) && !empty($result_info["D"]["Results"])) {
		  	foreach ($result_info["D"]["Results"] as $active){
		    	$activeList[] = $active;
		  	}

			$data['active_listings'] = $activeList;
		}

	 	$data["featured"] = isset($data['active_listings']) ? $data['active_listings'] : "";

	 	if(isset($result_info["D"]["Pagination"])) {
			$data['current_page'] = $result_info["D"]["Pagination"]["CurrentPage"];;
			$data['totalrows'] = $result_info["D"]["Pagination"]["TotalRows"];;
		}

	  	/* Query builder for pagination: (the old pagination gets the entire url including the offset variable which cause the offset variable duplicate) */
		$query_arr = array();

		foreach($_GET as $key=>$val) {
			if($key!="offset") {
		  		$query_arr[$key]=$val;
			}
		}

		if(isset($data['totalrows']) && isset($data['current_page'])) {
			$query_arr['totalrows']=$data['totalrows'];
		}

		$build_query = http_build_query($query_arr);
		/* End of query builder */

		$this->load->library('pagination');

		if(!empty($result_info["D"]["Pagination"])) {
			$data['total_pages'] = $result_info["D"]["Pagination"]["TotalPages"];
			$data['current_page'] = $result_info["D"]["Pagination"]["CurrentPage"];
			$data['total_count'] = $result_info["D"]["Pagination"]["TotalRows"];
			$data['page_size'] = $result_info["D"]["Pagination"]["PageSize"];

			$param["limit"] = 10;
			$total = $result_info["D"]["Pagination"]["TotalRows"];

			$config["base_url"] = base_url().'/active_listings?'.$build_query;
			$config["total_rows"] = $total;
			$config["per_page"] = $param["limit"];
			$config['page_query_string'] = TRUE;
			$config['query_string_segment'] = 'offset';

			$this->pagination->initialize($config);
		 }


		//=====================ACTIVE LISTINGS: DATA FROM DSL======================//
		// $mls_status = $this->mls_status();
		// $endpoint = "properties/".$this->domain_user->id."/status/".$mls_status."?_pagination=1&_page=".$page."&_limit=".$limit."&_expand=PrimaryPhoto&_orderby=".$order;

		// $active_listings = $dsl->get_endpoint($endpoint);
		// $activeListingData = json_decode($active_listings,true);
		// $activeList = array();
		
		// if(isset($activeListingData["data"]) && !empty($activeListingData["data"])) {
		//   	foreach ($activeListingData["data"] as $active){
		//     	$activeList[] = $active["standard_fields"][0];
		//   	}

		// 	$data['active_listings'] = $activeList;
		// }

		// if(isset($activeListingData)) {
		// 	$data['current_page'] = $activeListingData['current_page'];
		// 	$data['totalrows'] = $activeListingData['total_count'];
		// }
		
		// if(!empty($activeListingData)) {
		// 	$data['total_pages'] = $activeListingData["total_pages"];
		// 	$data['current_page'] = $activeListingData["current_page"];
		// 	$data['total_count'] = $activeListingData["total_count"];
		// 	$data['page_size'] = $activeListingData["page_size"];

		// 	$param["limit"] = 10;
		// 	$total = $activeListingData["total_count"];

		// 	$config["base_url"] = base_url().'/active_listings?'.$build_query;
		// 	$config["total_rows"] = $total;
		// 	$config["per_page"] = $param["limit"];
		// 	$config['page_query_string'] = TRUE;
		// 	$config['query_string_segment'] = 'offset';

		// 	$this->pagination->initialize($config);
		// }

		//get coordinates according to search
		$latitude = isset($data['active_listings'][0]['StandardFields']['Latitude']) ? $data['active_listings'][0]['StandardFields']['Latitude'] : "";
		$longitude = isset($data['active_listings'][0]['StandardFields']['Longitude']) ? $data['active_listings'][0]['StandardFields']['Longitude'] : "";

		if(empty($latitude) || empty($longitude) || $latitude == "********" || $longitude == "********") {
			$latlon = $this->getlatlon(isset($data['active_listings'][0]['StandardFields']['UnparsedAddress'])? $data['active_listings'][0]['StandardFields']['UnparsedAddress'] : "");
			$latitude = $latlon['lat'];
			$longitude = $latlon['lon'];
		}

		$data['coordinates'] = array('lat'=>$latitude, 'lon'=>$longitude);
		$data["pagination"] = $this->pagination->create_links();

		$data["js"] = array("listings.js","customer.js");
		$data["protocol"] = $this->protocol();
		$data["profile"] = $this->customer->get_customer_profile();
		$data["is_capture_leads"] = $this->home_model->get_capture_leads()->capture_leads;
		$data["js"] = array("listings.js","customer.js","selectize.js","advanced_search.js");
		$data["css"] = array("selectize.bootstrap3.css");
		$mls_id = $data['account_info']->MlsId;
		$data['search_fields'] = $this->search_model->get_mls_search_data($mls_id);
		$data['search_fields_groups'] = $this->search_model->get_mls_search_group($mls_id);

		$data = add_filter('before-active-listing-view', $data);

		$this->load->view("page", $data);
	}

	public function login() {

		if($this->input->is_ajax_request()) {

			if($this->session->userdata('customer_id')) {
				exit(json_encode(array('success' => true, 'redirect' => base_url().'customer-dashboard')));
			  //	redirect(base_url()."customer-dashboard");
			}

			$response = array("success"=>FALSE);

			if($this->input->post()) {

				if(filter_var($this->input->post('email'), FILTER_VALIDATE_EMAIL)) {

					$this->form_validation->set_rules("email", "Email", "required|trim");
					$this->form_validation->set_rules("phone", "Phone", "required|trim");

					if($this->form_validation->run() == FALSE) {

						$response = array('success'=>FALSE, 'message'=>'Incorrect Login!');

					} else {

						$res = $this->customer->sign_in();

						if($res["success"]) {

							if($this->input->post('listing_id') && $this->input->post('property_type')) {
								$redirect = ($this->input->post('property_type') == "featured_property") ? base_url().'property-details/'.$this->input->post('listing_id') : base_url().'other-property-details/'.$this->input->post('listing_id');
							}elseif($this->input->post('popular_search')) {
								$redirect = base_url().$this->input->post('popular_search');
							}elseif($this->input->post('redirect_customer')) {
								$redirect = base_url().$this->input->post('redirect_customer');
							} else {
								$redirect = base_url().'customer-dashboard';
							}

							$response["success"] = TRUE;
							$response["redirect"] = $redirect;

						} else {

							$response["message"] = $res["message"];
						}
					}
					
				} else {
					$response = array('success'=>FALSE, 'message'=>'The email you provided was invalid!');
				}

				unset($_POST);
			}

			exit(json_encode($response));
		}
	}

	public function signup() {

		if($this->input->is_ajax_request()) {
			
			$response['success'] = FALSE;
			$response['message'] = 'Could not create your account. Please check all fields.';

			if($this->input->post()) {

				$this->form_validation->set_rules("fname", "First Name", "required|trim");
				$this->form_validation->set_rules("lname", "Last Name", "required|trim");
				$this->form_validation->set_rules("email", "Email", "required|trim|valid_email");
				$this->form_validation->set_rules("phone_signup", "Phone Number", "required|trim");
				// $this->form_validation->set_rules("g-recaptcha-response", "Captcha", "required");

				$data = array(
					'secret' 	=> "6Lf9eRsUAAAAABfHFErjABI8DUaozthQj06y_95G",
					'response' => $this->input->post('g-recaptcha-response'),
					'remoteip'	=> $this->input->ip_address(),
				);

				$captcha_res = $this->captcha($data);

				if($this->form_validation->run() == FALSE) {

					
					$response["message"] = validation_errors();
					exit(json_encode($response));
				}
				elseif ( function_exists('validateAddress') && !validateAddress($this->input->post('email'), 'pcre8') )
				{
					$response['message'] = '<span style="color:red;">Invalid Email Address</span>';
					exit(json_encode($response));
				}
				else {

          if(!$this->customer->is_email_exist()) {

						$save_return = $this->save_to_flexmls($this->input->post());

						$password = $this->input->post("phone_signup");
						$type = ($this->input->post('type')) ? $this->input->post('type') : 'signup_form';

						$contact_id = (isset($save_return[0]["Id"]) && !empty($save_return[0]["Id"])) ? $save_return[0]["Id"] :  generate_key("10");

						$data = array(
							'ip_address'	=> $_SERVER['REMOTE_ADDR'],
				            'first_name' 	=> $this->input->post("fname"),
				            'last_name' 	=> $this->input->post("lname"),
				            'email' 		=> $this->input->post("email"),
				            'phone' 		=> $this->input->post("phone_signup"),
				            'password' 		=> password_hash($password, PASSWORD_DEFAULT),
				            'active' 		=> 1,
				            'type' 			=> $type,
				            'user_id' 		=> $this->domain_user->id,
				            'agent_id' 		=> $this->domain_user->agent_id,
				            'contact_id' 	=> $contact_id,
				            'created' 		=> date("Y-m-d H:m:s"),
				            'modified' 		=> date("Y-m-d H:m:s")
						);

						if($this->customer->sign_up($data)) {

							if($this->input->post('listing_id') && $this->input->post('property_type')) {

								$property_url = ($this->input->post('property_type') == "featured_property") ? base_url().'property-details/'.$this->input->post('listing_id') : base_url().'other-property-details/'.$this->input->post('listing_id');
								$redirect = $property_url . '?success';

								$liondesk = array(
	    						'notes' => 'From: ' . $property_url,
								);

							} elseif($this->input->post('redirect_customer')) {
								$redirect = $this->input->post('redirect_customer'); //Note: The success variable has been added per data on page to avoid conflict on search get datas.

								$liondesk = array(
	    						'notes' => 'From: ' . $redirect,
								);
							} else {
								$redirect = base_url().'customer-dashboard?success';

								$liondesk = array(
	    						'notes' => 'From: ' . base_url(),
								);	
							}

							//Send email to agent and customer . don't forget to uncomment this.
							Modules::run('crm/leads/send_leads_notification_to_agent');
							Modules::run('crm/leads/send_login_credential_to_customer');

							// add to liondesk
							$result = Modules::run('liondesk/pushContact', $contact_id, array(
								'first_name' => $data['first_name'],
								'last_name' => $data['last_name'],
								'email' => $data['email'],
								'mobile_phone' => $data['phone'],
							));
							// add a task to that contact in Liondesk after signing up.
							if($result){

								$liondesk['reminder_type_id'] = 1;
								$liondesk['contact_id'] = $result->id;
								$liondesk['due_at'] = date('Y-m-d H:i:s', strtotime("+5 min")) . 'Z'; // I don't understand the Z part here, but this is the only format that works.
								$liondesk['description'] = 'Follow up on customer signup.';
								$liondesk['notes'] = "AS Contact ID: ".$contact_id;

								$result = Modules::run('liondesk/createTask', $liondesk);
							}

							$response['success'] = TRUE;
							$response['redirect']= $redirect;
							
						} 

					} else {
						$response['message'] = 'Email address is already exist!';
					}
        		}
      		}
      		
			exit(json_encode($response));
		}
	}

	public function reset_phone_number() {

		if($this->input->is_ajax_request()) {

			if($this->input->post('email')) {

				if(filter_var($this->input->post('email'), FILTER_VALIDATE_EMAIL)) {

					$ret = $this->customer->isHaveRecord($this->input->post('email'));

					if($ret) {
						$this->send_reset_phone($ret);
						$response = array('success'=>TRUE, 'message'=>'Hi! We just sent you an email with a link to reset your phone number.');
					} else {
						$response = array('success'=>FALSE, 'message'=>'You don\'t have a record yet!');
					}

				} else {
					$response = array('success'=>FALSE, 'message'=>'The email you provided was invalid!');
				}

			} else {
				$response = array('success'=>FALSE, 'message'=>'Please provide your email.');
			}

			exit(json_encode($response));
		}

	}

	private function send_reset_phone($data=array()) {

		$this->load->model("crm/leads_model", "leads");
		$this->load->library("email");

		$siteInfo 		= Modules::run('agent_sites/getInfo');
		$domain_info 	= $this->leads->get_agent_domain();

		$agent_name 	= ($siteInfo['account_info']) ? $siteInfo['account_info']->Name : $siteInfo['branding']->first_name." ".$siteInfo['branding']->last_name;
		$agent_org 		= ($siteInfo['account_info']->Office) ? $siteInfo['account_info']->Office : "" ;
		$agent_email 	= $siteInfo['branding']->email;
		$agent_num 		= ($siteInfo['branding']->phone) ? $siteInfo['branding']->phone : $siteInfo['account_info']->Phones[0]->Number;
		$mls_name 		= $siteInfo['account_info']->Mls;
		$agent_photo_url= AGENT_DASHBOARD_URL . "assets/images/no-profile-img.gif";
		$logo_url		= AGENT_DASHBOARD_URL . "assets/images/email-logo.png";

		//get agent_photo
		if($siteInfo['branding']->agent_photo) {
			$agent_photo_url = AGENT_DASHBOARD_URL . "assets/uploads/photo/".$siteInfo['branding']->agent_photo;
		} else {
			foreach($siteInfo['account_info']->Images as $key) {
				if($key->Type == "Photo") {
					$agent_photo_url = $key->Uri;
					break;
				} elseif($key->Type == "Other Image") {
					$agent_photo_url = $key->Uri;
					break;
				} elseif($key->Type == "Logo") {
					$agent_photo_url = $key->Uri;
					break;
				}
			}
		}

		//get agent_logo
		if(isset($siteInfo['branding']->logo) && !empty($siteInfo['branding']->logo)) {
			$logo_url = AGENT_DASHBOARD_URL . "assets/uploads/logo/".$siteInfo['branding']->logo;
		} elseif(isset($siteInfo['account_info']->Images) && !empty($siteInfo['account_info']->Images)) {
			foreach ($siteInfo['account_info']->Images as $key) {
				if($key->Type == "Logo") {
					$logo_url = $key->Uri;
					break;
				}
			}
		}

		$subs = array(
			'customer_name'		=> $data->first_name,
			'customer_email'	=> $data->customer_email,
			'reset_url' 		=> 'http://'.$data->domains.'/home/redirect_customer_dasboard?customer_id='.$data->customer_id.'&customer_email='.$data->customer_email.'&contact_id='.$data->contact_id,
			'agent_name'		=> $agent_name,
			'logo_url'			=> $logo_url,
			'agent_company' 	=> $agent_org,
			'agent_phone' 		=> $agent_num,
			'agent_email' 		=> $agent_email,
			'agent_site' 		=> ($domain_info->status == "completed" || $domain_info->is_subdomain) ? $domain_info->domains : $domain_info->subdomain_url,
			'agent_website_url' => ($domain_info->status == "completed" || $domain_info->is_subdomain) ? 'http://'.$domain_info->domains : $domain_info->subdomain_url,
			'site_link' 		=> ($domain_info->status == "completed" || $domain_info->is_subdomain) ? 'http://'.$domain_info->domains : $domain_info->subdomain_url,
			'agent_website_photo' => $agent_photo_url
		);

		$this->email->send_email_v3(
        	$agent_email, //sender email
        	$agent_name, //sender name
        	$data->customer_email, //recipient email
        	'paul@agentsquared.com,aldwinj12345@gmail.com', //bccs
        	'2245128f-e0ef-4073-bb4d-eb5053a34d80', //"1d34ac08-0892-4fc7-9163-6ec7302b6847", //template_id
        	$email_data = array('subs'=>$subs)
        );	
	}

	public function redirect_customer_dasboard() {

		if($this->input->get('customer_id') && $this->input->get('customer_email') && $this->input->get('contact_id')) {

			$customer["customer_id"] = $this->input->get('customer_id');
            $customer["customer_email"] = $this->input->get('customer_email');
            $customer["contact_id"] = $this->input->get('contact_id');

            $this->session->set_userdata($customer);

            redirect(base_url().'customer-dashboard?reset_password=TRUE');

		}

	}

	public function logout() {

		if($this->input->is_ajax_request()) {
			
			$this->session->unset_userdata(
				array(
					'customer_id',
					'customer_email',
					'contact_id',
					'facebook_customer_login',
					'save_properties',
					'isSavePropertyId',
					'isSaveSearchPropertyUrl',
					'featuredPropertyUrl',
					'popularsearchurl_session'
				)
			);

			$ip_address=str_replace('.', '_', $this->input->ip_address());
			$customer_cookie = 'customer_'.$this->domain_user->id.'_'.$ip_address;

			setcookie($customer_cookie, '', time() - 3600);

			exit(json_encode(array('success'=>TRUE, 'redirect'=>base_url())));

		}

	}

	public function facebook_login() {

    if($this->session->userdata('facebook_customer_login') != true && !isset($_SESSION["customer_id"]))
        {
            // Check if user is logged in
            if($token = $this->facebook->is_authenticated() )
            {
	            // User logged in, get user details
	            $user = $this->facebook->request('get', '/me?fields=name,email');
	            $customer_info = $this->customer->is_registered($user);

	            if(!$customer_info )
	            {

	            	//insert to flex
	            	$save_return = $this->save_fb_to_flexmls( $user );
	            	$contactId = ( isset( $save_return[0]["Id"] ) ) ? $save_return[0]["Id"] : false;
	            	//insert to database
	            	$this->customer->insert_fb_registration( $user , $contactId );

	            	//send email to agent
	            	echo Modules::run('crm/leads/send_leads_notification_to_agent_fb', $user);

	            	//Save favorite property to database
	            	if($this->session->userdata('isSavePropertyId'))
	            		$this->customer->save_favorates_properties($this->session->userdata('isSavePropertyId'));
	            	//Save followed search to database
	            	if($this->session->userdata('isSaveSearchPropertyUrl'))
	            		$this->customer->save_search_properties($this->session->userdata('isSaveSearchPropertyUrl'));

	            	$this->session->set_userdata('facebook_customer_login', true);

	            	redirect(base_url()."customer-dashboard?is_fb_login=TRUE", "refresh");
	            }
	            else
	            {
	            	//query to database
	            	$save_properties = $this->customer->get_save_properties($customer_info["id"]);

	                $c_session["save_properties"] = $save_properties;

	                $this->customer->set_customer_session( $c_session );

	            	$customer["customer_id"] = $customer_info["id"];
	            	$customer["contact_id"] = $customer_info["contact_id"];
		            $customer["customer_email"] = $customer_info["email"];

		            //set session
		            $this->customer->set_customer_session( $customer );
		            //Save favorite property to database
		            if($this->session->userdata('isSavePropertyId'))
	            		$this->customer->save_favorates_properties($this->session->userdata('isSavePropertyId'));
	            	//Save followed search to database
	            	if($this->session->userdata('isSaveSearchPropertyUrl'))
	            		$this->customer->save_search_properties($this->session->userdata('isSaveSearchPropertyUrl'));

		            $this->session->set_userdata('facebook_customer_login', true);

	            	redirect(base_url()."customer-dashboard?is_fb_login=TRUE", "refresh");
	            }
	        }
	  }

	  redirect(base_url()."customer-dashboard?is_fb_login=TRUE", "refresh");
  }

	private function save_to_flexmls($data=array()) {

		$arr = array(
			'DisplayName' 	=> $data["fname"].' '.$data["lname"],
            'GivenName' 	=> $data["fname"],
            'FamilyName' 	=> $data["lname"],
            'PrimaryEmail' 	=> $data["email"],
            'PrimaryPhoneNumber' => $data["phone_signup"],
            'HomePhoneNumber' => $data["phone_signup"],
			'Notify' 		=> 1
        );

		$result_contacts = $this->idx_auth->AddContact($arr);

		return $result_contacts;
	}

	private function save_fb_to_flexmls($infos = array()) {

		$name = explode(" ", $infos["name"] );

		$arr = array(
			'DisplayName' => $infos["name"],
            'GivenName' => (isset($name[0])) ? $name[0] : "",
            'FamilyName' => (isset($name[1])) ? $name[1] : "",
            'PrimaryEmail' =>  $infos["email"],
			'Notify' => 1
        );

		$result_contacts = $this->idx_auth->AddContact($arr);

		return $result_contacts;
	}

	function save_customer_buyer() {

		if($this->input->is_ajax_request()) {
			
			$response = array("success"=>FALSE, "message"=>"<p>Failed to add data!</p>");

			//Set form validations
			$this->form_validation->set_rules("buyer[fname]", "First Name", "required|trim");
			$this->form_validation->set_rules("buyer[lname]", "Last Name", "required|trim");
			$this->form_validation->set_rules("buyer[email]", "Email", "required|trim|valid_email");
			$this->form_validation->set_rules("buyer[mobile]", "Mobile", "required|trim");
			$this->form_validation->set_rules("buyer[bedrooms]", "Bed Rooms", "required|trim");
			$this->form_validation->set_rules("buyer[bathrooms]", "Bath Rooms", "required|trim");
			$this->form_validation->set_rules("buyer[contact_by]", "Contact By", "required|trim");
			$this->form_validation->set_rules("buyer[price_range]", "Price Range", "required|trim");
			$this->form_validation->set_rules("buyer[when_do_you_want_to_move]", "When do you want to move", "required|trim");
			$this->form_validation->set_rules("buyer[when_did_you_start_looking]", "When did you start looking", "required|trim");
			$this->form_validation->set_rules("buyer[wher_would_you_like_to_own]", "where would you like to own", "required|trim");
			$this->form_validation->set_rules("buyer[are_you_currently_with_an_agent]", "Are ou currently with an agent", "required|trim");
			$this->form_validation->set_rules("buyer[describe_your_dream_home]", "Describe your dream home", "required|trim");
			$this->form_validation->set_rules("g-recaptcha-response", "Captcha", "required");

			$data = array(
				'secret' 	=> "6Lf9eRsUAAAAABfHFErjABI8DUaozthQj06y_95G",
				'response' => $this->input->post('g-recaptcha-response'),
				'remoteip'	=> $this->input->ip_address(),
			);

			$captcha_res = $this->captcha($data);
			
			if($this->form_validation->run()) {

				$buyer_success = $this->customer->save_customer_buyer();

				if($buyer_success) {

					$response["success"] 	= TRUE;
					$response["message"] 	= "<p>Data has been successfully added!</p>";
					$response["redirect"]	= base_url().'buyer?success';

					//Send Email
					Modules::run('crm/leads/send_buyer_lead');
					Modules::run('crm/leads/respond_to_buyer');
					
					// add to liondesk
					$buyer = $this->input->post("buyer");

					$liondesk_id = Modules::run('liondesk/getLiondeskContactId', $this->session->userdata('customer_id')); 
 
          if($liondesk_id !== false && !empty($liondesk_id)){ 

          	$result = Modules::run('liondesk/createTask', array(
							'contact_id' => $liondesk_id,
						  'reminder_type_id' => 2,
						  'due_at' => date('Y-m-d H:i:s', strtotime("+5 min")) . 'Z',
						  'description' => 'Buyer Inquiry',
						  'notes' => 'Data: ' . "\n\r" . assoc_to_string($buyer) . "\n\r",
						));
					}else{ 
 
	          $result = Modules::run('liondesk/pushContact', null, array( 
	            'first_name' => $buyer["fname"], 
	            'last_name' => $buyer["lname"], 
	            'email' => $buyer["email"], 
	            'home_phone' => $buyer["phone"], 
	            'mobile_phone' => $buyer["mobile"], 
	          )); 
	           
	          if($result){ 
	 
	            $result = Modules::run('liondesk/createTask', array( 
	              'contact_id' => $result->id, 
	              'reminder_type_id' => 2, 
	              'due_at' => date('Y-m-d H:i:s', strtotime("+5 min")) . 'Z', 
	              'description' => 'Buyer Inquiry', 
	              'notes' => 'Data: ' . "\n\r" . assoc_to_string($buyer) . "\n\r", 
	            )); 
	          }
	        }
				} 

			} else {
				$response["message"] = "<p>".validation_errors()."<p>";
			}

			exit(json_encode($response));
		}
	}

	function save_customer_seller() {

		if($this->input->is_ajax_request()) {

			$response = array("success"=>FALSE, "message"=>"<p>Failed to add data!</p>");

			//Set seller form validations
			$this->form_validation->set_rules("seller[fname]", "First Name", "required|trim");
			$this->form_validation->set_rules("seller[lname]", "Last Name", "required|trim");
			$this->form_validation->set_rules("seller[email]", "Email", "required|trim|valid_email");
			$this->form_validation->set_rules("seller[phone]", "Phone", "required|trim");
			$this->form_validation->set_rules("seller[mobile]", "Mobile", "required|trim");
			$this->form_validation->set_rules("seller[bedrooms]", "Bed Rooms", "required|trim");
			$this->form_validation->set_rules("seller[bathrooms]", "Bath Rooms", "required|trim");
			$this->form_validation->set_rules("g-recaptcha-response", "Captcha", "required");

			$data = array(
				'secret' 	=> "6Lf9eRsUAAAAABfHFErjABI8DUaozthQj06y_95G",
				'response' => $this->input->post('g-recaptcha-response'),
				'remoteip'	=> $this->input->ip_address(),
			);

			$captcha_res = $this->captcha($data);

			if($this->form_validation->run()) {

				if($contact = $this->customer->save_customer_seller()) {

					$response["success"] 	= TRUE;
					$response["message"] 	= "<p>Data has been successfully added!</p>";
					$response["redirect"] 	= base_url().'seller?success';

					//Send Email
					Modules::run('crm/leads/send_seller_lead');
					Modules::run('crm/leads/respond_to_seller');

					// add to liondesk
					$seller = $this->input->post("seller");

					$liondesk_id = Modules::run('liondesk/getLiondeskContactId', $this->session->userdata('customer_id'));

					if($liondesk_id !== false && !empty($liondesk_id)){

						$result = Modules::run('liondesk/createTask', array(
							'contact_id' => $liondesk_id,
						  'reminder_type_id' => 2,
						  'due_at' => date('Y-m-d H:i:s', strtotime("+5 min")) . 'Z',
						  'description' => 'Seller Inquiry',
						  'notes' => 'Data: ' . "\n\r" . assoc_to_string($seller) . "\n\r",
						));
						
					}else{

						$result = Modules::run('liondesk/pushContact', null, array(
							'first_name' => $seller["fname"],
							'last_name' => $seller["lname"],
							'email' => $seller["email"],
							'home_phone' => $seller["phone"],
							'mobile_phone' => $seller["mobile"],
						));
						
						if($result){

							$result = Modules::run('liondesk/createTask', array(
								'contact_id' => $result->id,
							  'reminder_type_id' => 2,
							  'due_at' => date('Y-m-d H:i:s', strtotime("+5 min")) . 'Z',
							  'description' => 'Seller Inquiry',
							  'notes' => 'Data: ' . "\n\r" . assoc_to_string($seller) . "\n\r",
							));
						}
					}

				} 

			} else {

				$response["success"] = FALSE;
				$response["message"] = "<p>".validation_errors()."<p>";

			}

			exit(json_encode($response));

		}

	}

	function mortgage_calculator() {

		require_once APPPATH .'libraries/CentralApps/MortgageCalculator/Calculator.php';

		if(!$this->input->post('sale_price') || !$this->input->post('down_percent') || !$this->input->post('mortgage_interest_percent') || !$this->input->post('year_term')) {

			$response = array("success"=>FALSE, "message" => "Please complete all required fields!");

		} else {

			$calculator = new CentralApps\MortgageCalculator\Calculator();
			$calculator->setAmountBorrowed($this->input->post('sale_price'));
			$calculator->setDownpayment($this->input->post('down_percent'));
			$calculator->setInterestRate($this->input->post('mortgage_interest_percent'));
			$calculator->setYears($this->input->post('year_term'));

			$monthly_payment = $calculator->calculateMonthlyPayment();
			$monthly_payment_interest = $calculator->calculateMonthlyInterest();
			$annual_payment_interest = $calculator->calculateAnnualInterest();
			$downpayment = $calculator->calculateDownpayment();
			$amount_financed = $calculator->calculateAmountFinanced();

			$price_value           = $calculator->get_price_value();
			$downpayment_value     = $calculator->get_downpayment_value();
			$interest_rate_value   = $calculator->get_interest_rate_value();
			$loan_terms_value      = $calculator->get_loan_terms_value();

			$response = array("success"=>TRUE, "monthly_payment" => $monthly_payment);

		}

		exit(json_encode($response));
	}

	function customer_questions() {

		if($this->input->is_ajax_request()) {
			
			if($this->input->post()) {
				$this->form_validation->set_rules("first_name", "FirstName", "required|trim");
				$this->form_validation->set_rules("last_name", "LastName", "required|trim");
				$this->form_validation->set_rules("email", "Email", "required|trim|valid_email");
				$this->form_validation->set_rules("phone", "Phone Number", "required|trim");
				$this->form_validation->set_rules("message", "Message", "required|trim");
				$this->form_validation->set_rules("g-recaptcha-response", "Captcha", "required");

				$data = array(
					'secret' 	=> "6Lf9eRsUAAAAABfHFErjABI8DUaozthQj06y_95G",
					'response' => $this->input->post('g-recaptcha-response'),
					'remoteip'	=> $this->input->ip_address(),
				);

				$captcha_res = $this->captcha($data);

				$response = array("success" => FALSE, "message"=>"<span class='alert alert-danger alert-dismissible' role='alert' style='display:block'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>Failed to send message!</span>");

				if($this->form_validation->run() == FALSE) {
					$response["message"] = "<span class='alert alert-danger alert-dismissible' role='alert' style='display:block'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>".validation_errors()."</span>";
				} else {
					
					$ret = $this->customer->save_customer_questions();

					$response["success"] = $ret["success"];
					$response["message"] = $ret["message"];
					$response["is_login"]= $ret["is_login"];

					if($ret["success"]) {

						Modules::run('crm/leads/send_customer_question');
						Modules::run('crm/leads/respond_to_customer_question');

						// add to liondesk
						$result = Modules::run('liondesk/pushContact', null, array(
							'first_name' => $this->input->post('first_name'),
							'last_name' => $this->input->post('last_name'),
							'email' => $this->input->post('email'),
							'mobile_phone' => $this->input->post('phone'),
						));
						if($result){
							$result = Modules::run('liondesk/createTask', array(
								'contact_id' => $result->id,
							  'reminder_type_id' => 2,
							  'due_at' => date('Y-m-d H:i:s', strtotime("+5 min")) . 'Z',
							  'description' => '[URGENT] Customer Asking for Inquiry',
							  'notes' => 'Message: ' . $response["message"] . "\n\r",
							));
						}

					}

				}

			}

			exit(json_encode($response));
		}
	}

	function save_schedule_for_showing() {

		if($this->input->is_ajax_request()) {

			if($this->customer->save_schedule_for_showing()) {

				$link = $this->sched_send_email_to_agent($this->input->post());

				$response = array("success" => TRUE, "message" => " Showing has been successfully saved!");

				if($response['success']){

					$liondesk_id = Modules::run('liondesk/getLiondeskContactId', $this->session->userdata('customer_id'));

					if($liondesk_id !== false && !empty($liondesk_id)){
						$result = Modules::run('liondesk/createTask', array(
							'contact_id' => $liondesk_id,
					    'reminder_type_id' => 2,
					    'due_at' => date('Y-m-d H:i:s', strtotime("+5 min")) . 'Z',
					    'description' => '[URGENT] Customer Scheduled For Showing',
					    'notes' => 'Property Scheduled: ' . $link . "\n\r" . assoc_to_string(array(
					    	'message' => $this->input->post('sched_for_showing'),
					    	'schedule_date' => $this->input->post('date_scheduled'),
					    	'property_id' => $this->input->post('property_id'),
					    	'property_name' => $this->input->post('property_name'),
					    )),
						));
					}
				}
			} else {
				$response = array("success" => FALSE, "message" => " Failed to saved data!");
			}
			exit( json_encode($response) );
		}
	}

	public function myaccount($access_token) {

		if($access_token) {
			$url = "https://sparkapi.com/v1/my/account";

			$headers = array(
				'Content-Type: application/json',
				'User-Agent: Spark API PHP Client/2.0',
				'X-SparkApi-User-Agent: PHP-APP-In-Fifteen-Minutes/1.0',
			  'Authorization: OAuth '. $access_token,
			  'Accept-Encoding: gzip, deflate',
			);

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_URL, $url);

			$result = curl_exec($ch);
			$unzipped_data = gunzip($result);
			$result_info = json_decode($unzipped_data, true);

			if(empty($result_info)) {
				$result_info = curl_strerror($ch);
			}
      curl_close($ch);

			return $result_info;
		}
	}

	public function mls_status(){

		$mls_statuses = Modules::run('search/get_mls_statuses');
		$statuses = array();

		if(isset($mls_statuses) && !empty($mls_statuses)){

			foreach($mls_statuses as $status) {
			   if($status != "Closed" && $status !="Sold" && $status !="Leased" && $status !="Pending") {
			    $statuses[]= $status;
			   }
			}
		}

		$mls_status_implode = implode (",", $statuses);
		$mls_status = rawurlencode($mls_status_implode);
		//printA($mls_status);exit;
		return $mls_status;
	}

	public function sched_send_email_to_agent($post = array())
	{
	 	$this->load->library("email");

		$agent_profile = $this->customer->getAgentInfo();
		$contact_profile = $this->customer->getContactInfo();

		$link = base_url()."other-property-details/".$post['property_id'];

		$subs = array(
			'first_name'		=> $agent_profile->agent_name,
			'customer_name' 	=> $contact_profile->contact_name,
			'customer_email'	=> $contact_profile->email,
			'customer_phone'	=> $contact_profile->phone,
			'property_name'		=> $post["property_name"],
			'property_url'		=> $link,
			'showing_schedule'	=> $post['date_scheduled'],
			'lead_message' 		=> $post['sched_for_showing']
		);

		$this->email->send_email_v3(
        	'support@agentsquared.com', //sender email
        	'AgentSquared', //sender name
        	$agent_profile->email, //recipient email
        	'paul@agentsquared.com,aldwinj12345@gmail.com', //bccs
        	'560f02c6-2180-4c28-8859-5456ddf1fcf1', //"a5e9a5bf-8c0e-4513-be76-9b94d070d4f2", //template_id
        	$email_data = array('subs'=>$subs, 'reply_to'=>$contact_profile->email)
        );
    
    return $link;
	}

	public function protocol() {
		// if (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) {
		// 	$protocol = "https://";
		// } else {
		// 	$protocol = "http://";
		// }

		$protocol = "https://";
		return $protocol;
	}
	public function token_checker($access_token = NULL) {

		return $this->home_model->token_checker();

		/*$token = $this->home_model->getUserAccessToken($this->domain_user->agent_id);
		$access_token = $token->access_token;

		if($access_token) {
			$url = "https://sparkapi.com/v1/my/account";

			$headers = array(
				'Content-Type: application/json',
				'User-Agent: Spark API PHP Client/2.0',
				'X-SparkApi-User-Agent: PHP-APP-In-Fifteen-Minutes/1.0',
			    'Authorization: OAuth '. $access_token,
			);

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_URL, $url);

			$result = curl_exec($ch);
			$result_info = json_decode($result, true);

			if(empty($result_info)) {
				$result_info = curl_strerror($ch);
			}

			return $result_info;
		}*/
	}
	public function advance_search($slug = NULL) {
		$obj = Modules::load("mysql_properties/mysql_properties");
		$data['account_info'] = $obj->get_account($this->domain_user->id, "account_info");
		$data['system_info'] = $obj->get_account($this->domain_user->id, "system_info");
		$mls_id = $data['account_info']->MlsId;
		$isHere = Modules::run('search/advance/check_advance_search',$mls_id);
		// if(!$isHere) {
		// 	redirect('home');
		// }

		$data['page_data'] = $page_data = $this->page->get_page($slug);
		$this->load->library('pagination');
		$getData = http_build_query($this->input->get());

 		$user_info = $this->home_model->getUsersInfo($this->domain_user->id);

 		$data["property_types"] = Modules::run('dsl/get_property_types');
 		$data["property_subtypes"] = Modules::run('dsl/get_property_subtypes');
		$data["theme"] = $user_info->theme;
		$data["header"] = "header-page";
		$data['default_menu_data'] = Modules::run('menu/get_default_menu_data');
    	$data['custom_menu_data'] = Modules::run('menu/get_custom_menu_data');
    	$data["user_info"] = $user_info;

		$data['socialmedia_link'] = Modules::run('agent_social_media/agent_social_media/linksInfo');
		$data["js"] = array("listings.js","customer.js");
		$data["protocol"] = $this->protocol();
		$data["profile"] = $this->customer->get_customer_profile();
		$data["page"] = "advance_search_view/advance-search";
		$data["title"] = "Advanced Search";
		$data["js"] = array("selectize.js", "advanced_search.js");
		$data["css"] = array("selectize.bootstrap3.css");

		$data['search_fields'] = $this->search_model->get_mls_search_data($mls_id);
		$data['search_fields_groups'] = $this->search_model->get_mls_search_group($mls_id);

		$this->load->view("page", $data);
	}
	
	public function search_results() {
		/* Important Lead */
		/*
			- MlsStatus
			- PropertyType
			- PropertySubType
		*/
		$obj = Modules::load("mysql_properties/mysql_properties");
		$data['account_info'] = $obj->get_account($this->domain_user->id, "account_info");
		$data['system_info'] = $obj->get_account($this->domain_user->id, "system_info");
		$this->load->model("search/search_model","search_model");
		$mls_id = $data['account_info']->MlsId;
		$isHere = Modules::run('search/advance/check_advance_search',$mls_id);
		$data['search_fields'] = $this->search_model->get_mls_search_data($mls_id);
		$data['search_fields_groups'] = $this->search_model->get_mls_search_group($mls_id);
		$search_fields_list_data = $this->search_model->get_mls_data_search_fields_list_data(NULL,$data['search_fields'] );
		$account_meta = $this->idx_auth->get('accounts/meta');

		if(isset($account_meta["0"]["Mls"]) && !empty($account_meta["0"]["Mls"])){
			$ix = 0;
			foreach ($account_meta["0"]["Mls"] as $Ids) {
				if($ix === 0){
					$mlsIds = "'".$Ids["Id"]."'";
				}else{
					$mlsIds .= ", '".$Ids["Id"]."'";
				}
				$ix++;
			}
			$filterStr = "MlsId Eq ".$mlsIds."  And ";
		}
		else{
			$filterStr = "MlsId Eq '".$data['account_info']->MlsId."'  And ";
		}
		$filterStr .= '(PhotosCount Gt 0) And ';
		if($this->input->get('MlsStatus')) {
			$filterStr .= "(MlsStatus Eq '".$this->input->get('MlsStatus')."')";
		}
		else {
			$filterStr .= Modules::run('search/mlsStatusFilterString');
		}
		if($this->input->get('PropertyType')) {
			$filterStr .= (empty($filterStr)) ? "(PropertyType Eq '".$this->input->get('PropertyType')."')": " And (PropertyType Eq '".$this->input->get('PropertyType')."')";
		}
		if($this->input->get('PropertyClass')) {
			$filterStr .= (empty($filterStr)) ? "(PropertyClass Eq '".$this->input->get('PropertyClass')."')": " And (PropertyClass Eq '".$this->input->get('PropertyClass')."')";
		}
		if($this->input->get('PropertySubType')) {
			$filterStr .= (empty($filterStr)) ? "(PropertySubType Eq '".$this->input->get('PropertySubType')."')": " And (PropertySubType Eq '".$this->input->get('PropertySubType')."')";
		}
		/* Standard Fields */
		$filterStrP1 = '';
		$standardField = array();
		$standardFieldData = array();
		if($search = $this->input->get('Search')){
			if(isset($search_fields_list_data) && !empty($search_fields_list_data)){
				foreach($search_fields_list_data as $sfd) {
					$strHas = strcmp(strtolower($sfd->field_value),strtolower($search));
					if ($strHas == 0) {
						$standardField[] = $sfd->mls_searc_field_id;
					}
				}
			}
			if(isset($data['search_fields']) && !empty($data['search_fields'])){
				foreach($data['search_fields'] as $sf) {
					foreach($standardField as $sdf) {
						if($sdf == $sf->id) {
							$standardFieldData[] = $sf->field_name;
						}
					}
				}
			}

			$stData = "";
			$iCount = 0;
			foreach($standardFieldData as $standdata) {
				// $filterStr .= " And ".$standdata." Eq '".$search."'";
				if($iCount == 0) {
					$stData = " And (".$standdata." Eq '".$search."')";
				}
				$iCount++;
			}
			$filterStr .= $stData;

			if((count($standardField) == 0) && (count($standardFieldData) == 0)) {
				if(strlen($search) == 26 && is_numeric($search)) {
					$filterStr .= " And (ListingKey Eq '".$search."')";
				} else {
					$filterStr .= " And (UnparsedAddress Eq '*".str_replace(' ', '*', $search)."*')";
				}
			}
		}

		foreach($_GET as $key=>$val) {
			if(!empty($val)) {
				switch($key) {
					case 'City':
						$filterStrP1 .= ((empty($filterStrP1)) ? "" : " Or ")."City Eq '".$val."'";
					break;
					case 'PostalCode':
						$filterStrP1 .= ((empty($filterStrP1)) ? "" : " Or ")."PostalCode Eq '".$val."'";
					break;
					case 'CountyOrParish':
						$filterStrP1 .= ((empty($filterStrP1)) ? "" : " Or ")."CountyOrParish Eq '".$val."'";
					break;
				}
			}
		}
		$filterStrP2 = '';
		foreach($_GET as $key=>$val) {
			if(!empty($val)) {
				switch($key) {
					case 'HighSchool':
						$filterStrP2 .= ((empty($filterStrP2)) ? "" : " And ")."HighSchool Eq '".$val."'";
					break;
					case 'MiddleOrJuniorSchool':
						$filterStrP2 .= ((empty($filterStrP2)) ? "" : " And ")."MiddleOrJuniorSchool Eq '".$val."'";
					break;
					case 'ElementarySchool':
						$filterStrP2 .= ((empty($filterStrP2)) ? "" : " And ")."ElementarySchool Eq '".$val."'";
					break;
				}
			}
		}
		if($filterStrP1 != '') {
			$filterStr .= ' And ('.$filterStrP1.')';
		}
		if($filterStrP2 != '') {
			$filterStr .= ' And ('.$filterStrP2.')';
		}
		foreach($_GET as $key=>$val) {
			if(!empty($val)) {
				switch($key) {
					case 'StreetNumber':
						$filterStr .= ((empty($filterStr)) ? "" : " And ")."StreetNumber Eq '".$val."'";
					break;
					case 'StreetName':
						$filterStr .= ((empty($filterStr)) ? "" : " And ")."StreetName Eq '*".str_replace(' ', '*', $val)."*'";
					break;
					case 'Country':
						$filterStr .= ((empty($filterStr)) ? "" : " And ")."Country Eq '".$val."'";
					break;
					case 'price_min':
						$filterStr .= ((empty($filterStr)) ? "" : " And ")."ListPrice Ge ".$val."";
					break;
					case 'price_max':
						$filterStr .= ((empty($filterStr)) ? "" : " And ")."ListPrice Le ".$val."";
					break;
					case 'area_min':
						$filterStr .= ((empty($filterStr)) ? "" : " And ")."Approx Lot SqFt Ge ".$val."";
					break;
					case 'area_max':
						$filterStr .= ((empty($filterStr)) ? "" : " And ")."Approx Lot SqFt Le ".$val."";
					break;
					case 'house_min':
						$filterStr .= ((empty($filterStr)) ? "" : " And ")."BuildingAreaTotal Ge ".$val."";
					break;
					case 'house_max':
						$filterStr .= ((empty($filterStr)) ? "" : " And ")."BuildingAreaTotal Le ".$val."";
					break;
					case 'BedsTotal':
						$filterStr .= ((empty($filterStr)) ? "" : " And ")."BedsTotal Ge ".$val."";
					break;
					case 'BathsTotal':
						$filterStr .= ((empty($filterStr)) ? "" : " And ")."BathsTotal Ge ".$val."";
					break;
					case 'BathsFull':
						$filterStr .= ((empty($filterStr)) ? "" : " And ")."BathsFull Ge ".$val."";
					break;
					case 'Pool':
						$filterStr .= ((empty($filterStr)) ? "" : " And ")."PoolFeatures Eq '".$val."'";
					break;
					case 'YearBuilt':
						$filterStr .= ((empty($filterStr)) ? "" : " And ")."YearBuilt Eq ".$val."";
					break;
					case 'GarageSpaces':
						$filterStr .= ((empty($filterStr)) ? "" : " And ")."GarageSpaces Eq ".$val."";
					break;
					case 'CarportSpaces':
						$filterStr .= ((empty($filterStr)) ? "" : " And ")."CarportSpaces Eq ".$val."";
					break;
					case 'Slab_Parking_Spaces':
						$filterStr .= ((empty($filterStr)) ? "" : " And ").'"Parking Spaces"."Slab Parking Spaces" Eq '.$val;
					break;
					case 'RangeOven_Elec':
						$filterStr .= ((empty($filterStr)) ? "" : " And ").'"Kitchen Features"."RangeOven Elec" Eq '.$val;
					break;
					case 'Roof':
						if(count($val)) {
							$filterStr .= ((empty($filterStr)) ? "" : " And ")."Roof Eq ";
							$roof_items = "";
							foreach($val as $v) {
								if(empty($roof_items)) {
									$roof_items .= "'".$v."'";
								}
								else {
									$roof_items .= ",'".$v."'";
								}
							}
							$filterStr .= $roof_items;
						}
					break;
				}
			}
		}
		$this->load->model("home/home_model","home_model");
		$token = $this->home_model->getUserAccessToken($this->domain_user->agent_id);
		$access_token = array(
			'access_token' => $token->access_token
		);
		$dsl = modules::load('dsl/dsl');
		$filter_url = rawurlencode($filterStr);

		$page = ($this->input->get('page')) ? $this->input->get('page') : 1;

		$limit = 10;
		$totalrows = (isset($_GET['totalrows'])) ? $_GET['totalrows'] : 0;

		if(isset($_GET['offset']) && !isset($_GET['page'])) {

			$total_page = $totalrows / $limit;
			$remainder = $totalrows % $limit;

			if($remainder != 0){
				$total_page++;
			}

			$total_page = intval($total_page);

			$actual_page = $_GET['offset'] / $limit;
			$actual_page = intval($actual_page);
			$actual_page = ($actual_page <= 0) ? 1 : $actual_page;

			if($actual_page < $total_page) {
				$page += $actual_page;
			}
		}
		$order = "-YearBuilt";
		if($this->input->get('order') == 'highest') {
			$order = "-ListPrice";
		}
		if($this->input->get('order') == 'lowest') {
			$order = "+ListPrice";
		}
		if($this->input->get('order') == 'newest') {
			$order = "-YearBuilt";
		}
		if($this->input->get('order') == 'oldest') {
			$order = "+YearBuilt";
		}
		$endpoint = "spark-listings?_limit=".$limit."&_page=".$page."&_expand=PrimaryPhoto&_filter=(".$filter_url.")&_pagination=1&_orderby=".$order;
		$results = $dsl->post_endpoint($endpoint, $access_token);
		$dataObj = json_decode($results, true);

		$data['page_data'] = $page_data = $this->page->get_page(isset($slug) ? $slug :"");

		$searchProperty = $dataObj;

		if(isset($searchProperty['pagination'])) {
			$data['current_page'] = $searchProperty['pagination']['CurrentPage'];
			$data['totalrows'] = $searchProperty['pagination']['TotalRows'];
		}

		/* Query builder for pagination: (the old pagination gets the entire url including the offset variable which cause the offset variable duplicate) */
		$query_arr = array();

		foreach($_GET as $key=>$val) {
			if($key!="offset") {
				$query_arr[$key]=$val;
			}
		}

		if(isset($data['totalrows']) && isset($data['current_page'])) {
			$query_arr['totalrows']=$data['totalrows'];
		}

		$build_query = http_build_query($query_arr);
		/* End of query builder */

		$this->load->library('pagination');
		$token = $this->dsl_model->getUserAccessToken($this->domain_user->agent_id);

		if($token) {
			if(!empty($this->idx_auth->checker())){
				if(isset($searchProperty["data"]) && !empty($searchProperty["data"])) {
					$data["featured"] = $searchProperty["data"];
					if($searchProperty["pagination"]) {
						$data["total_pages"] = $searchProperty["pagination"]["TotalPages"];
						$data["current_page"] = $searchProperty["pagination"]["CurrentPage"];
						$data["total_count"] = $searchProperty["pagination"]["TotalRows"];
						$data["page_size"] = $searchProperty["pagination"]["PageSize"];
						$param["limit"] = 10;
						$total = $searchProperty["pagination"]["TotalRows"];
						$config["base_url"] = base_url().'/search-results?'.$build_query;
						$config["total_rows"] = $total;
						$config["per_page"] = $param["limit"];
						$config['page_query_string'] = TRUE;
						$config['query_string_segment'] = 'offset';

						$this->pagination->initialize($config);
					}
				}
			}

		} else {
			$data["featured"] = $searchProperty["data"];
			if($searchProperty["pagination"]) {
				$data["total_pages"] = $searchProperty["pagination"]["TotalPages"];
				$data["current_page"] = $searchProperty["pagination"]["CurrentPage"];
				$data["total_count"] = $searchProperty["pagination"]["TotalRows"];
				$data["page_size"] = $searchProperty["pagination"]["PageSize"];
				$param["limit"] = 10;
				$total = $searchProperty["pagination"]["TotalRows"];
				$config["base_url"] = base_url().'/search-results?'.$build_query;
				$config["total_rows"] = $total;
				$config["per_page"] = $param["limit"];
				$config['page_query_string'] = TRUE;
				$config['query_string_segment'] = 'offset';

				$this->pagination->initialize($config);
			}
		}

		// echo "total_pages ".$data["total_pages"];
		// echo "<br>";
		// echo "current_page ".$data["current_page"];
		// echo "<br>";
		// echo "total_count ".$data["total_count"];
		// echo "<br>";
		// echo "page_size ".$data["page_size"];
		// echo "<br>";

    	//get coordinates according to search
		$latitude = isset($data["featured"][0]['StandardFields']['Latitude']) ? $data["featured"][0]['StandardFields']['Latitude'] : "";
		$longitude = isset($data["featured"][0]['StandardFields']['Longitude']) ? $data["featured"][0]['StandardFields']['Longitude'] : "";

		if(empty($latitude) || empty($longitude) || $latitude == "********" || $longitude == "********") {

			$latlon = $this->getlatlon(isset($data["featured"][0]['StandardFields']['UnparsedAddress']) ? $data["featured"][0]['StandardFields']['UnparsedAddress'] : "" );
			$latitude = $latlon['lat'];
			$longitude = $latlon['lon'];
		}

		$data['coordinates'] = array('lat'=>$latitude, 'lon'=>$longitude);
		$savesearches = $this->customer->get_customer_searches();
		$savesearchesflex = $this->customer->getSaveSearchesFlex();
		$save_searches_flex = ( empty($savesearchesflex) ) ? array() : $savesearchesflex;
        $data["saved_searches_db"] = array_merge($savesearches, $save_searches_flex);

		$data['endpoint'] = $endpoint;
		$data['encoded_filter'] = $filterStr;
		$data['filter'] = $filter_url;
		$data["pagination"] = $this->pagination->create_links();
		$data["is_capture_leads"] = $this->home_model->get_capture_leads()->capture_leads;
 		$user_info = $this->home_model->getUsersInfo($this->domain_user->id);

 		$data["property_types"] = Modules::run('dsl/get_property_types');
 		$data["property_subtypes"] = Modules::run('dsl/get_property_subtypes');
		$data["theme"] = $user_info->theme;
		$data["header"] = "header-page";
		//$data["page"] = "advance_search";
		$data["page"] = "advance_search_view/mapped-search";

		$data["title"] = "Search Results";
		$data['default_menu_data'] = Modules::run('menu/get_default_menu_data');
    	$data['custom_menu_data'] = Modules::run('menu/get_custom_menu_data');
    	$data["user_info"] = $user_info;

		$data['socialmedia_link'] = Modules::run('agent_social_media/agent_social_media/linksInfo');
		$data["js"] = array("listings.js","customer.js","selectize.js","advanced_search.js","responsive-tabs.js");
		$data["css"] = array("selectize.bootstrap3.css");

		$data["protocol"] = $this->protocol();
		$data["profile"] = $this->customer->get_customer_profile();

		$this->load->view("home/page", $data);
	}

	public function show_map() {

		$obj = Modules::load("mysql_properties/mysql_properties");
		$data['account_info'] = $obj->get_account($this->domain_user->id, "account_info");
		$data['system_info'] = $obj->get_account($this->domain_user->id, "system_info");
		$this->load->model("search/search_model","search_model");
		$mls_id = $data['account_info']->MlsId;
		$data['search_fields'] = $this->search_model->get_mls_search_data($mls_id);
		$data['search_fields_groups'] = $this->search_model->get_mls_search_group($mls_id);
		$search_fields_list_data = $this->search_model->get_mls_data_search_fields_list_data(NULL,$data['search_fields'] );
		$account_meta = $this->idx_auth->get('accounts/meta');

		if(isset($account_meta["0"]["Mls"]) && !empty($account_meta["0"]["Mls"])){
			$ix = 0;
			foreach ($account_meta["0"]["Mls"] as $Ids) {
				if($ix === 0){
					$mlsIds = "'".$Ids["Id"]."'";
				}else{
					$mlsIds .= ", '".$Ids["Id"]."'";
				}
				$ix++;
			}
			$filterStr = "MlsId Eq ".$mlsIds." And ";
		}
		else{
			$filterStr = "MlsId Eq '".$data['account_info']->MlsId."'  And ";
		}
		$filterStr .= '(PhotosCount Gt 0) And ';
		if($this->input->get('MlsStatus')) {
			$filterStr .= "(MlsStatus Eq '".$this->input->get('MlsStatus')."')";
		}
		else {
			$filterStr .= Modules::run('search/mlsStatusFilterString');
		}
		if($this->input->get('PropertyType')) {
			$filterStr .= (empty($filterStr)) ? "(PropertyType Eq '".$this->input->get('PropertyType')."')": " And (PropertyType Eq '".$this->input->get('PropertyType')."')";
		}
		if($this->input->get('PropertyClass')) {
			$filterStr .= (empty($filterStr)) ? "(PropertyClass Eq '".$this->input->get('PropertyClass')."')": " And (PropertyClass Eq '".$this->input->get('PropertyClass')."')";
		}
		if($this->input->get('PropertySubType')) {
			$filterStr .= (empty($filterStr)) ? "(PropertySubType Eq '".$this->input->get('PropertySubType')."')": " And (PropertySubType Eq '".$this->input->get('PropertySubType')."')";
		}
		/* Standard Fields */
		$filterStrP1 = '';
		$standardField = array();
		$standardFieldData = array();
		if($search = $this->input->get('Search')){
			if(isset($search_fields_list_data) && !empty($search_fields_list_data)){
				foreach($search_fields_list_data as $sfd) {
					$strHas = strcmp(strtolower($sfd->field_value),strtolower($search));
					if ($strHas == 0) {
						$standardField[] = $sfd->mls_searc_field_id;
					}
				}
			}
			if(isset($data['search_fields']) && !empty($data['search_fields'])){
				foreach($data['search_fields'] as $sf) {
					foreach($standardField as $sdf) {
						if($sdf == $sf->id) {
							$standardFieldData[] = $sf->field_name;
						}
					}
				}
			}

			$stData = "";
			$iCount = 0;
			foreach($standardFieldData as $standdata) {
				if($iCount == 0) {
					$stData = " And (".$standdata." Eq '".$search."')";
				}
				$iCount++;
			}
			$filterStr .= $stData;

			if((count($standardField) == 0) && (count($standardFieldData) == 0)) {
				$filterStr .= " And (UnparsedAddress Eq '*".str_replace(' ', '*', $search)."*')";
			}
		}

		foreach($_GET as $key=>$val) {
			if(!empty($val)) {
				switch($key) {
					case 'City':
						$filterStrP1 .= ((empty($filterStrP1)) ? "" : " Or ")."City Eq '".$val."'";
					break;
					case 'PostalCode':
						$filterStrP1 .= ((empty($filterStrP1)) ? "" : " Or ")."PostalCode Eq '".$val."'";
					break;
					case 'CountyOrParish':
						$filterStrP1 .= ((empty($filterStrP1)) ? "" : " Or ")."CountyOrParish Eq '".$val."'";
					break;
				}
			}
		}
		$filterStrP2 = '';
		foreach($_GET as $key=>$val) {
			if(!empty($val)) {
				switch($key) {
					case 'HighSchool':
						$filterStrP2 .= ((empty($filterStrP2)) ? "" : " And ")."HighSchool Eq '".$val."'";
					break;
					case 'MiddleOrJuniorSchool':
						$filterStrP2 .= ((empty($filterStrP2)) ? "" : " And ")."MiddleOrJuniorSchool Eq '".$val."'";
					break;
					case 'ElementarySchool':
						$filterStrP2 .= ((empty($filterStrP2)) ? "" : " And ")."ElementarySchool Eq '".$val."'";
					break;
				}
			}
		}

		if($filterStrP1 != '') {
			$filterStr .= ' And ('.$filterStrP1.')';
		}
		if($filterStrP2 != '') {
			$filterStr .= ' And ('.$filterStrP2.')';
		}

		foreach($_GET as $key=>$val) {
			if(!empty($val)) {
				switch($key) {
					case 'StreetNumber':
						$filterStr .= ((empty($filterStr)) ? "" : " And ")."StreetNumber Eq '".$val."'";
					break;
					case 'StreetName':
						$filterStr .= ((empty($filterStr)) ? "" : " And ")."StreetName Eq '*".str_replace(' ', '*', $val)."*'";
					break;
					case 'Country':
						$filterStr .= ((empty($filterStr)) ? "" : " And ")."Country Eq '".$val."'";
					break;
					case 'price_min':
						$filterStr .= ((empty($filterStr)) ? "" : " And ")."ListPrice Ge ".$val."";
					break;
					case 'price_max':
						$filterStr .= ((empty($filterStr)) ? "" : " And ")."ListPrice Le ".$val."";
					break;
					case 'area_min':
						$filterStr .= ((empty($filterStr)) ? "" : " And ")."BuildingAreaTotal Ge ".$val."";
					break;
					case 'area_max':
						$filterStr .= ((empty($filterStr)) ? "" : " And ")."BuildingAreaTotal Le ".$val."";
					break;
					case 'BedsTotal':
						$filterStr .= ((empty($filterStr)) ? "" : " And ")."BedsTotal Ge ".$val."";
					break;
					case 'BathsTotal':
						$filterStr .= ((empty($filterStr)) ? "" : " And ")."BathsTotal Ge ".$val."";
					break;
					case 'BathsFull':
						$filterStr .= ((empty($filterStr)) ? "" : " And ")."BathsFull Ge ".$val."";
					break;
					case 'Pool':
						$filterStr .= ((empty($filterStr)) ? "" : " And ")."PoolFeatures Eq '".$val."'";
					break;
					case 'YearBuilt':
						$filterStr .= ((empty($filterStr)) ? "" : " And ")."YearBuilt Eq ".$val."";
					break;
					case 'GarageSpaces':
						$filterStr .= ((empty($filterStr)) ? "" : " And ")."GarageSpaces Eq ".$val."";
					break;
					case 'CarportSpaces':
						$filterStr .= ((empty($filterStr)) ? "" : " And ")."CarportSpaces Eq ".$val."";
					break;
					case 'Slab_Parking_Spaces':
						$filterStr .= ((empty($filterStr)) ? "" : " And ").'"Parking Spaces"."Slab Parking Spaces" Eq '.$val;
					break;
					case 'RangeOven_Elec':
						$filterStr .= ((empty($filterStr)) ? "" : " And ").'"Kitchen Features"."RangeOven Elec" Eq '.$val;
					break;
					case 'Roof':
						if(count($val)) {
							$filterStr .= ((empty($filterStr)) ? "" : " And ")."Roof Eq ";
							$roof_items = "";
							foreach($val as $v) {
								if(empty($roof_items)) {
									$roof_items .= "'".$v."'";
								}
								else {
									$roof_items .= ",'".$v."'";
								}
							}
							$filterStr .= $roof_items;
						}
					break;
				}
			}
		}

		$this->load->model("home/home_model","home_model");
		$token = $this->home_model->getUserAccessToken($this->domain_user->agent_id);
		$access_token = array('access_token' => $token->access_token);

		$dsl = modules::load('dsl/dsl');
		//$filterStr = "(PhotosCount Gt 0) And MlsStatus Eq 'Active','Pending','Backups','Contingent' And (City Eq 'palm bay')";
		$filter_url = rawurlencode($filterStr);

		$page = ($this->input->get('page')) ? $this->input->get('page') : 1;

		$limit = 10;
		$totalrows = ($this->input->get('totalrows')) ? $this->input->get('totalrows') : 0;

		$offset = $this->input->get('offset') ? $this->input->get('offset') : 0;
		$total_page = $totalrows / $limit;
		$remainder = $totalrows % $limit;

		if($remainder != 0){
			$total_page++;
		}

		$total_page = intval($total_page);

		$actual_page = $offset / $limit;
		$actual_page = intval($actual_page);
		$actual_page = ($actual_page <= 0) ? 1 : $actual_page;

		if($actual_page < $total_page) {
			$page += $actual_page;
		}

		$order = "-YearBuilt";

		if($this->input->get('order') == 'highest') {
			$order = "-ListPrice";
		}
		if($this->input->get('order') == 'lowest') {
			$order = "+ListPrice";
		}
		if($this->input->get('order') == 'newest') {
			$order = "-YearBuilt";
		}
		if($this->input->get('order') == 'oldest') {
			$order = "+YearBuilt";
		}

		$current_segment="";

		if($this->input->get('current_segment')=="nearby_listings") {

			$latlon = Modules::run('mysql_properties/get_nearby_coordinates', $this->domain_user->id);

			$filterStr = "(MlsStatus Ne 'Closed' Or MlsStatus Ne 'Sold')";
			$filter_url = rawurlencode($filterStr);
			$filter_string = substr($filter_url, 3, -3);
			$endpoint = "spark-nearby-listings?_pagination=1&_page=".$page."&_limit=".$limit."&_lat=".$latlon['lat']."&_lon=".$latlon['lon']."&_expand=PrimaryPhoto&_filter=(".$filter_string.")";

			$results = $dsl->post_endpoint($endpoint, $access_token);
			$dataObj = json_decode($results, true);
			$searchProperty = $dataObj;
			$current_segment = "nearby_listings";

		} elseif($this->input->get('current_segment')=="new_listings") {

			$filterStr = "(MlsStatus Ne 'Closed' Or MlsStatus Ne 'Sold' And MajorChangeType Eq 'New Listing')";
			$filter_url = rawurlencode($filterStr);
			$filter_string = substr($filter_url, 3, -3);
			$endpoint = "spark-listings?_pagination=1&_page=".$page."&_limit=".$limit."&_expand=PrimaryPhoto&_filter=(".$filter_string.")&_orderby=".$order;

			$results = $dsl->post_endpoint($endpoint, $access_token);
			$dataObj = json_decode($results, true);
			$searchProperty = $dataObj;
			$current_segment = "new_listings";

		} elseif($this->input->get('current_segment')=="office_listings") {

			$filterStr = "(MlsStatus Ne 'Closed' Or MlsStatus Ne 'Sold')";
			$filter_url = rawurlencode($filterStr);
			$filter_string = substr($filter_url, 3, -3);
			$endpoint = "office/listings/?_pagination=1&_page=".$page."&_limit=".$limit."&_expand=PrimaryPhoto&_filter=(".$filter_string.")";

			$results = $dsl->post_endpoint($endpoint, $access_token);
			$dataObj = json_decode($results, true);
			$searchProperty = $dataObj;
			$current_segment = "office_listings";

		} elseif($this->input->get('current_segment')=="find-a-home") {

			$mls_status = $this->mls_status();
			$endpoint = "properties/".$this->domain_user->id."/status/".$mls_status."?_pagination=1&_page=".$page."&_limit=".$limit."&_expand=PrimaryPhoto";

			$results = $dsl->get_endpoint($endpoint);
			$dataObj = json_decode($results, true);

			$activeList = array();

			if(isset($dataObj["data"]) && !empty($dataObj["data"])){
				foreach($dataObj["data"] as $active){
			 		$activeList[] = $active["standard_fields"][0];
			 	}
			}

			$searchProperty = $activeList;
			$current_segment = "find-a-home";

			//Fetch from office listings
			if(empty($searchProperty)) {

				$office_listings = $obj->get_properties($this->domain_user->id, "office");

				if(!empty($office_listings)) {

					$searchProperty["data"] = $office_listings;
					$current_segment = "office_listings_find";

				} else {

					$new_listings = $obj->get_properties($this->domain_user->id, "new");

					if(empty($new_listings)) {
						$filterStr = "(MlsStatus Ne 'Closed' Or MlsStatus Ne 'Sold' And MajorChangeType Eq 'New Listing')";
						$filter_url = rawurlencode($filterStr);
						$filter_string = substr($filter_url, 3, -3);
						$endpoint = "spark-listings?_pagination=1&_page=".$page."&_limit=".$limit."&_expand=PrimaryPhoto&_filter=(".$filter_string.")&_orderby=".$order;
						$results = $dsl->post_endpoint($endpoint, $access_token);
						$dataObj = json_decode($results, true);
						$searchProperty = $dataObj;
					} else {
						$searchProperty["data"] = $new_listings;
					}

					$current_segment = "new_listings_find";
				}

			}

		} elseif($this->input->get("current_segment")=="saved_searches") {

			$search_id = $this->input->get('search_id');

			$savedSearch = $this->idx_auth->getSavedSearch($search_id);
			$filterStr = "(".$savedSearch[0]['Filter'].")";
	 		$filter_url = rawurlencode($filterStr);
	 		$filter_string = substr($filter_url, 3, -3);

	 		$endpoint = "spark-listings?_pagination=1&_page=".$page."&_limit=".$limit."&_expand=PrimaryPhoto&_filter=(".$filter_string.")";
	 		$results = $dsl->post_endpoint($endpoint, $access_token);
	 		$dataObj = json_decode($results, true);
			$searchProperty = $dataObj;
			$current_segment = "saved_searches";

		} else {

			$endpoint = "spark-listings?_limit=".$limit."&_page=".$page."&_expand=PrimaryPhoto&_filter=(".$filter_url.")&_pagination=1&_orderby=".$order;

			$results = $dsl->post_endpoint($endpoint, $access_token);
			$dataObj = json_decode($results, true);
			$searchProperty = $dataObj;
			$current_segment = "my_listings";

		}

		$data["featured"] = ($current_segment=="find-a-home") ? $searchProperty : (isset($searchProperty["data"]) ? $searchProperty["data"] : NULL);
		$property_arr = array();

    	//get coordinates according to search
    	if($current_segment == "new_listings_find" || $current_segment == "office_listings_find") {

			if($data["featured"] != NULL) {

	    		for($i=0; $i<count($data["featured"]); $i++) {

	    			$latitude = (isset($data["featured"][$i]->StandardFields->Latitude)) ? $data["featured"][$i]->StandardFields->Latitude : "";
		    		$longitude = (isset($data["featured"][$i]->StandardFields->Longitude)) ? $data["featured"][$i]->StandardFields->Longitude : "";

		    		if(empty($latitude) || empty($longitude) || $latitude == "********" || $longitude == "********") {
						if(isset($data["featured"][$i]->StandardFields->UnparsedAddress)) {
							$latlon = $this->getlatlon($data["featured"][$i]->StandardFields->UnparsedAddress);
							$latitude = $latlon['lat'];
							$longitude = $latlon['lon'];
						}
					}

                    $address = ($data["featured"][$i]->StandardFields->UnparsedFirstLineAddress) ? $data["featured"][$i]->StandardFields->UnparsedFirstLineAddress : "";
                    $fullAddress = ($data["featured"][$i]->StandardFields->UnparsedAddress) ? $data["featured"][$i]->StandardFields->UnparsedAddress : "";
                    $price = ($data["featured"][$i]->StandardFields->CurrentPrice) ? number_format($data["featured"][$i]->StandardFields->CurrentPrice, 2) : 0;
                    $propertyClass = ($data["featured"][$i]->StandardFields->PropertyClass) ? $data["featured"][$i]->StandardFields->PropertyClass : "";
                    $bed = ($data["featured"][$i]->StandardFields->BedsTotal) ? $data["featured"][$i]->StandardFields->BedsTotal : "";
                    $bath = ($data["featured"][$i]->StandardFields->BathsTotal) ? $data["featured"][$i]->StandardFields->BathsTotal : "";
                    $area = ($data["featured"][$i]->StandardFields->BuildingAreaTotal) ? $data["featured"][$i]->StandardFields->BuildingAreaTotal : "";
                    $city = ($data["featured"][$i]->StandardFields->City) ? $data["featured"][$i]->StandardFields->City : "";
                    $listingKey = $data["featured"][$i]->StandardFields->ListingKey;
                    $photo = $data["featured"][$i]->Photos->Uri300;

                    $bedbatharea1 = ($propertyClass != "Land") ? " <p style='font-size:10px;'>BATH ".$bath." | BED ".$bed." | SQ. FT. ".$area."</p>" : (($area != "********") ? " <p style='font-size:10px;'> SQ. FT. ".$area."</p>" : "");

	    			$contentString = "<div class='col-md-5 no-padding'><a href='other-property-details/".$listingKey."'>";
				    $contentString .= "<img src='".$photo."' alt='' class='img-responsive' style='width: 100%;height: 100px;margin:5% 0;'></a></div><div class='col-md-7 nopadding-right'>";
				    $contentString .= "<p style='font-size:12px;margin: 0px;color: #0000bd;'>";
				    $contentString .= "<a href='".base_url()."other-property-details/".$listingKey."'>".$address."</a></p>";
				    $contentString .= "<p class='prop-city' style='margin-bottom: 2px;'>".$city."</p><b style='font-size: 20px;'>$".$price."</b><br>";
				    $contentString .= "<span class='label label-info' style='display: inline-block;'><i class='glyphicon glyphicon-home'></i>".$propertyClass."</span>".$bedbatharea1;

				    $property_arr[$i]['latitude'] = $latitude;
				    $property_arr[$i]['longitude'] = $longitude;
				    $property_arr[$i]['content_string'] = $contentString;

	    		}

	    	}

    	} else {

			if($data["featured"] != NULL) {

	    		for($i=0; $i<count($data["featured"]); $i++) {

	    			$latitude = (isset($data["featured"][$i]['StandardFields']['Latitude'])) ? $data["featured"][$i]['StandardFields']['Latitude'] : "";
		    		$longitude = (isset($data["featured"][$i]['StandardFields']['Longitude'])) ? $data["featured"][$i]['StandardFields']['Longitude'] : "";

		    		if(empty($latitude) || empty($longitude) || $latitude == "********" || $longitude == "********") {
						if(isset($data["featured"][$i]['StandardFields']['UnparsedAddress'])) {
							$latlon = $this->getlatlon($data["featured"][$i]['StandardFields']['UnparsedAddress']);
							$latitude = $latlon['lat'];
							$longitude = $latlon['lon'];
						}
					}

                    $address = ($data["featured"][$i]['StandardFields']['UnparsedFirstLineAddress']) ? $data["featured"][$i]['StandardFields']['UnparsedFirstLineAddress'] : "";
                    $fullAddress = ($data["featured"][$i]['StandardFields']['UnparsedAddress']) ? $data["featured"][$i]['StandardFields']['UnparsedAddress']: "";
                    $price = ($data["featured"][$i]['StandardFields']['CurrentPrice']) ? number_format($data["featured"][$i]['StandardFields']['CurrentPrice'], 2) : 0;
                    $propertyClass = ($data["featured"][$i]['StandardFields']['PropertyClass']) ? $data["featured"][$i]['StandardFields']['PropertyClass'] : "";
                    $bed = ($data["featured"][$i]['StandardFields']['BedsTotal']) ? $data["featured"][$i]['StandardFields']['BedsTotal'] : "";
                    $bath = ($data["featured"][$i]['StandardFields']['BathsTotal']) ? $data["featured"][$i]['StandardFields']['BathsTotal'] : "";
                    $area = ($data["featured"][$i]['StandardFields']['BuildingAreaTotal']) ? $data["featured"][$i]['StandardFields']['BuildingAreaTotal'] : "";
                    $city = ($data["featured"][$i]['StandardFields']['City']) ? $data["featured"][$i]['StandardFields']['City'] : "";
                    $listingKey = $data["featured"][$i]['StandardFields']['ListingKey'];

                    $photo = ($current_segment == "find-a-home") ? $data["featured"][$i]['StandardFields']['Photos'][0]['Uri300'] : $data["featured"][$i]['Photos']['Uri300'];

                   	$bedbatharea1 = ($propertyClass != "Land") ? " <p style='font-size:10px;'>BATH ".$bath." | BED ".$bed." | SQ. FT. ".$area."</p>" : (($area != "********") ? " <p style='font-size:10px;'> SQ. FT. ".$area."</p>" : "");

	    			$contentString = "<div class='col-md-5 no-padding'><a href='other-property-details/".$listingKey."'>";
				    $contentString .= "<img src='".$photo."' alt='' class='img-responsive' style='width: 100%;height: 100px;margin:5% 0;'></a></div><div class='col-md-7 nopadding-right'>";
				    $contentString .= "<p style='font-size:12px;margin: 0px;color: #0000bd;'>";
				    $contentString .= "<a href='".base_url()."other-property-details/".$listingKey."'>".$address."</a></p>";
				    $contentString .= "<p class='prop-city' style='margin-bottom: 2px;'>".$city."</p><b style='font-size: 20px;'>$".$price."</b><br>";
				    $contentString .= "<span class='label label-info' style='display: inline-block;'><i class='glyphicon glyphicon-home'></i>".$propertyClass."</span>".$bedbatharea1;

				    $property_arr[$i]['latitude'] = $latitude;
				    $property_arr[$i]['longitude'] = $longitude;
				    $property_arr[$i]['content_string'] = $contentString;

	    		}

	    	}

    	}

		exit(json_encode(array('success'=>(!empty($data["featured"])) ? TRUE : FALSE, 'properties'=>$property_arr, 'current_segment' => $current_segment)));

		//exit(json_encode(array('success'=>(!empty($data["featured"])) ? TRUE : FALSE, 'properties'=>$data["featured"], 'current_segment' => $current_segment, 'coordinates'=>$data['coordinates'])));
	}

	public function mapped_search() {
		if($this->uri->segment(1) == 'searched-results' || $this->uri->segment(1) == 'mapped-search' ) {
			redirect('search-results','location',301);
		}
		$useragent = $_SERVER['HTTP_USER_AGENT'];
		$data['browser'] = $this->get_browser_name($useragent);
		$gsf_fields = $this->home_model->get_gsf_fields($this->domain_user->agent_id);
		$data['gsf_standard'] = (isset($gsf_fields['gsf_standard_fields']) && $gsf_fields['gsf_standard_fields'] != '{}') ? json_decode($gsf_fields['gsf_standard_fields'],true) : NULL;
		$obj = Modules::load("mysql_properties/mysql_properties");
		$data['account_info'] = $obj->get_account($this->domain_user->id, "account_info");
		$data['system_info'] = $obj->get_account($this->domain_user->id, "system_info");
		$mls_id = $data['account_info']->MlsId;
		$data['mls_id'] = $data['account_info']->MlsId;
		$this->load->model("search/search_model","search_model");
 		$user_info = $this->home_model->getUsersInfo($this->domain_user->id);
 		$mlsStatus = $this->idx_auth->get('standardfields/MlsStatus');
 		$city_list = array();
 		$data["mlsStatus"] = $mlsStatus[0]["MlsStatus"]["FieldList"]; 		
 		$data["city_list"] = json_encode($city_list);
 		$data["property_types"] = $this->idx_auth->get('mls/'.$mls_id.'/propertytypes'); //Modules::run('dsl/getSparkProxy/propertytypes');
 		$data["property_subtypes"] = Modules::run('dsl/get_property_subtypes');
		$data["theme"] = $user_info->theme;
		$data["header"] = "header-page";
    
		$data['default_menu_data'] = Modules::run('menu/get_default_menu_data');
    	$data['custom_menu_data'] = Modules::run('menu/get_custom_menu_data');
    	$data["user_info"] = $user_info;
		$data['socialmedia_link'] = Modules::run('agent_social_media/agent_social_media/linksInfo');
		$data["protocol"] = $this->protocol();
		$data["profile"] = $this->customer->get_customer_profile();
    
		$data["title"] = "Search Results";
		$data["css"] = array("selectize.bootstrap3.css");
		$data["favorites"] = $this->customer->get_customer_properties(TRUE,FALSE,"desc");
		$data["save_searches"] = $this->customer->get_customer_searches();
		$data['search_fields'] = $this->search_model->get_mls_search_data($mls_id);
		$data['search_fields_groups'] = $this->search_model->get_mls_search_group($mls_id);
		$data["save_url"] = (isset($_GET)) ? http_build_query($this->input->get()) : '';
		$data["is_get"] = (isset($_GET)) ? TRUE : FALSE;
		
		$data = add_filter('before-mapped-search-view', $data);
		$this->load->view("page", $data);
	}

	public function getlatlon_old( $address = NULL){

		$location = array('address'=> $address, 'key' => getenv('GOOGLE_MAP_APIKEY'));
		$options = http_build_query($location);
		$api_call = "https://maps.googleapis.com/maps/api/geocode/json?".$options;
		$api_data = @file_get_contents($api_call);
		$data = json_decode($api_data, true);

	 	if($data["status"] === "OK"){

		  	$latlon = array(
		  		'lat' => $data["results"][0]["geometry"]["location"]["lat"],
		  		'lon' => $data["results"][0]["geometry"]["location"]["lng"],
		  	);

	  		return $latlon;
	  	}

	  	return FALSE;
	}

	public function getlatlon($address = NULL) {

		if(!empty($address)) {

			$agent_address = urlencode($address);
			$api_call = 'http://www.datasciencetoolkit.org/street2coordinates/'.$agent_address;
			$api_data = @file_get_contents($api_call);
			$decoded_data = json_decode($api_data, true);
			$data = $decoded_data[$address];

			if(!empty($data)){

				$latlon = array(
					'lat' => $data['latitude'],
					'lon' => $data['longitude'],
				);

				return $latlon;
			}

		}

		return FALSE;
	}

  public function getlatlon_ajax_old() {

		$location = array('address'=> $this->input->post('address'), 'key' => getenv('GOOGLE_MAP_APIKEY'));
		$options = http_build_query($location);
		$api_call = "https://maps.googleapis.com/maps/api/geocode/json?".$options;
		$api_data = @file_get_contents($api_call);
		$data = json_decode($api_data, true);

	  	if($data["status"] === "OK"){

	  		$latlon = array(
		  		'lat' => $data["results"][0]["geometry"]["location"]["lat"],
		  		'lon' => $data["results"][0]["geometry"]["location"]["lng"],
	  		);

	  		exit(json_encode(array('latlon'=>$latlon)));
	  	}
	}

	public function getlatlon_ajax() {

		  $agent_address = urlencode($this->input->get('address'));
			$api_call = 'http://www.datasciencetoolkit.org/street2coordinates/'.$agent_address;
			$api_data = @file_get_contents($api_call);
			$decoded_data = json_decode($api_data, true);
			$data = $decoded_data[$this->input->get('address')];

			if(!empty($data)){

				$latlon = array(
					'lat' => $data['latitude'],
					'lon' => $data['longitude'],
				);

				exit(json_encode(array('latlon'=>$latlon)));
			}
	}

  public function sitemap() {

	  $data['pages'] = array();
	    if($this->domain_user->agent_id != ''){
		      	$result = $this->sitemap->get(array(
		        'agent_id' => $this->domain_user->agent_id,
		        'status' => 'published',
		        'is_menu' => 1,
	      	));
	      	if($result){
	       		$data['pages'] = $result;
	      	}
	    }
	    //$data["token_checker"] = ($token) ? $this->idx_auth->checker() : TRUE;
	    $user_info = $this->home_model->getUsersInfo($this->domain_user->id);
	    $data['plan_id'] = $user_info->plan_id;
	    $data['settings'] = $this->home_model->get_seo_settings($this->domain_user->agent_id);
	    $data["header"] = "header-page";
		$data["page"] = "page_view/html";
	    $data["theme"] = $user_info->theme;
	    $data["user_info"] = $user_info;
	    $data['socialmedia_link'] = Modules::run('agent_social_media/agent_social_media/linksInfo');
	    $data['default_menu_data'] = Modules::run('menu/get_default_menu_data');
	    $data['custom_menu_data'] = Modules::run('menu/get_custom_menu_data');
	    $data["title"] = "HTML Sitemap";
	    $data["css"] = array("html.css");

		// SEO meta title and meta description  - jovanni
	    $page_meta = $this->Meta_model->get_meta($this->domain_user->agent_id, 'about');
	    $data['title'] = (isset($page_meta['meta_title']) && $page_meta['meta_title'] != '')? $page_meta['meta_title'] : 'About | ' . $user_info->first_name . ' ' . $user_info->last_name . ' | ' .$user_info->phone;
	    $data['description'] = (isset($page_meta['meta_description']) && $page_meta['meta_description'] != '')? $page_meta['meta_description'] : $user_info->banner_tagline;
	    $data['canonical'] = (isset($page_meta['canonical']) && $page_meta['canonical'] != '')? $page_meta['canonical'] : '';
	    // ===================
	    
	    $data = add_filter('before-sitemap-view', $data);
    	$this->load->view('home/page', $data);
  	}

    public function privacy_policy() {

	  $data['pages'] = array();
	    if($this->domain_user->agent_id != ''){
		      	$result = $this->sitemap->get(array(
		        'agent_id' => $this->domain_user->agent_id,
		        'status' => 'published',
		        'is_menu' => 1,
	      	));
	      	if($result){
	       		$data['pages'] = $result;
	      	}
	    }
	    //$data["token_checker"] = ($token) ? $this->idx_auth->checker() : TRUE;
	    $user_info = $this->home_model->getUsersInfo($this->domain_user->id);
	    $data['settings'] = $this->home_model->get_seo_settings($this->domain_user->agent_id);
	    $data["header"] = "header-page";
		$data["page"] = "privacy_policy";
	    $data["theme"] = $user_info->theme;
	    $data["user_info"] = $user_info;
	    $data['socialmedia_link'] = Modules::run('agent_social_media/agent_social_media/linksInfo');
	    $data['default_menu_data'] = Modules::run('menu/get_default_menu_data');
	    $data['custom_menu_data'] = Modules::run('menu/get_custom_menu_data');
	    $data["title"] = "Privacy Policy";

		// SEO meta title and meta description  - jovanni
	    $page_meta = $this->Meta_model->get_meta($this->domain_user->agent_id, 'about');
	    $data['title'] = (isset($page_meta['meta_title']) && $page_meta['meta_title'] != '')? $page_meta['meta_title'] : 'Privacy Policy | ' . $user_info->first_name . ' ' . $user_info->last_name . ' | ' .$user_info->phone;
	    $data['description'] = (isset($page_meta['meta_description']) && $page_meta['meta_description'] != '')? $page_meta['meta_description'] : $user_info->banner_tagline;
	    $data['canonical'] = (isset($page_meta['canonical']) && $page_meta['canonical'] != '')? $page_meta['canonical'] : '';
	    // ===================
	    
    	$this->load->view('home/page', $data);
  	}

  	public function gsc($code = '') {

    	if($code != '') {
      		$code = str_replace('.html', '', $code);
			if($this->Meta_model->verify_gsc($this->domain_user->agent_id, $code)) {
				header('Content-Type: text/html; charset=utf-8');
				echo 'google-site-verification: '.$code.'.html';
			exit();
      		}
    	}

    	redirect('/');
  	}

  	private function include_functions($data = array()){

	    $user_info = $this->home_model->getUsersInfo($this->domain_user->id);
	    $theme = APPPATH . 'modules/home/views/' . $user_info->theme . '/';

	    if(is_dir($theme)){
	      	if(file_exists( $theme . "functions.php" )){
	        	$this->load->view( $user_info->theme . "/functions", $data );
	      	}
	    }
  	}

  	public function get_city(){
  		$results = $this->idx_auth->get('standardfields/City');
  		printA($results); exit;
  	}

	public function list_of_school($name="ElementarySchool") {
		if($this->uri->segment(1) == 'searched-results') {

			redirect('search-results','location',301);
		}

		$obj = Modules::load("mysql_properties/mysql_properties");
		$data['account_info'] = $obj->get_account($this->domain_user->id, "account_info");
		$data['system_info'] = $obj->get_account($this->domain_user->id, "system_info");
		$mls_id = $data['account_info']->MlsId;
		
		$data['search_fields'] = $this->search_model->get_mls_search_data($mls_id);
		foreach ($data['search_fields'] as $sf) {
			if($sf->field_name == $name) {
				$search_fields_list = $this->search_model->get_mls_data_search_fields_list_data($sf->id);
				echo $name;
				printA($search_fields_list); exit;
			}
		}
		echo "Invalid Parameter";
	}

	public function advance_menu_viewer($name="PostalCode") {
		if($this->uri->segment(1) == 'searched-results') {

			redirect('search-results','location',301);
		}

		$obj = Modules::load("mysql_properties/mysql_properties");
		$data['account_info'] = $obj->get_account($this->domain_user->id, "account_info");
		$data['system_info'] = $obj->get_account($this->domain_user->id, "system_info");
		$mls_id = $data['account_info']->MlsId;
		
		$data['search_fields'] = $this->search_model->get_mls_search_data($mls_id);
		foreach ($data['search_fields'] as $sf) {
			if($sf->field_name == $name) {
				$search_fields_list = $this->search_model->get_mls_data_search_fields_list_data($sf->id);
				echo $name;
				printA($search_fields_list); exit;
			}
		}
		echo "Invalid Parameter";
	}

	public function get_browser_name($user_agent)
	{
	    if (strpos($user_agent, 'Opera') || strpos($user_agent, 'OPR/')) return 'Opera';
	    elseif (strpos($user_agent, 'Edge')) return 'Edge';
	    elseif (strpos($user_agent, 'Chrome')) return 'Chrome';
	    elseif (strpos($user_agent, 'Safari')) return 'Safari';
	    elseif (strpos($user_agent, 'Firefox')) return 'Firefox';
	    elseif (strpos($user_agent, 'MSIE') || strpos($user_agent, 'Trident/7')) return 'Internet Explorer';
	    
	    return 'Other';
	}

	function captcha($data) {
		$url = "https://www.google.com/recaptcha/api/siteverify";

		$ch = curl_init();

		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 0);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_URL, $url);

		$result = curl_exec($ch);
		$result_info = json_decode($result, true);

		return $result_info;
	}

	public function tx_reccp_notice() {

	  $data['pages'] = array();
	    if($this->domain_user->agent_id != ''){
		      	$result = $this->sitemap->get(array(
		        'agent_id' => $this->domain_user->agent_id,
		        'status' => 'published',
		        'is_menu' => 1,
	      	));
	      	if($result){
	       		$data['pages'] = $result;
	      	}
	    }
	    $obj = Modules::load("mysql_properties/mysql_properties");
	    $user_info = $this->home_model->getUsersInfo($this->domain_user->id);
	    $data['settings'] = $this->home_model->get_seo_settings($this->domain_user->agent_id);
	    $data["header"] = "header-page";
		$data["page"] = "tx_reccp_notice";
	    $data["theme"] = $user_info->theme;
	    $data["user_info"] = $user_info;
	    $data['account_info'] = $obj->get_account($this->domain_user->id, "account_info");
	    $data['system_info'] = $obj->get_account($this->domain_user->id, "system_info");
	    $data['socialmedia_link'] = Modules::run('agent_social_media/agent_social_media/linksInfo');
	    $data['default_menu_data'] = Modules::run('menu/get_default_menu_data');
	    $data['custom_menu_data'] = Modules::run('menu/get_custom_menu_data');
	    $data["title"] = "Texas Real Estate Commission Consumer Protection Notice";

		// SEO meta title and meta description  - jovanni
	    $page_meta = $this->Meta_model->get_meta($this->domain_user->agent_id, 'about');
	    $data['title'] = (isset($page_meta['meta_title']) && $page_meta['meta_title'] != '')? $page_meta['meta_title'] : 'Privacy Policy | ' . $user_info->first_name . ' ' . $user_info->last_name . ' | ' .$user_info->phone;
	    $data['description'] = (isset($page_meta['meta_description']) && $page_meta['meta_description'] != '')? $page_meta['meta_description'] : $user_info->banner_tagline;
	    $data['canonical'] = (isset($page_meta['canonical']) && $page_meta['canonical'] != '')? $page_meta['canonical'] : '';
	    // ===================
	    
    	$this->load->view('home/page', $data);
	}	

	public function tx_reciab_services() {

	  $data['pages'] = array();
	    if($this->domain_user->agent_id != ''){
		      	$result = $this->sitemap->get(array(
		        'agent_id' => $this->domain_user->agent_id,
		        'status' => 'published',
		        'is_menu' => 1,
	      	));
	      	if($result){
	       		$data['pages'] = $result;
	      	}
	    }
	    $obj = Modules::load("mysql_properties/mysql_properties");
	    $dsl = modules::load('dsl/dsl');
	    $user_info = $this->home_model->getUsersInfo($this->domain_user->id);
	    $data['settings'] = $this->home_model->get_seo_settings($this->domain_user->agent_id);
	    $data["header"] = "header-page";
		$data["page"] = "tx_reciab_services";
	    $data["theme"] = $user_info->theme;
	    $data["user_info"] = $user_info;
	    $data['account_info'] = $obj->get_account($this->domain_user->id, "account_info");
	    $account_info = $data['account_info'];
	    $data['broker_info'] = $dsl->getBrokerData($account_info->OfficeId);
	    $data['system_info'] = $obj->get_account($this->domain_user->id, "system_info");
	    $data['socialmedia_link'] = Modules::run('agent_social_media/agent_social_media/linksInfo');
	    $data['default_menu_data'] = Modules::run('menu/get_default_menu_data');
	    $data['custom_menu_data'] = Modules::run('menu/get_custom_menu_data');
	    $data["title"] = "Texas Real Estate Commission Information About Brokerage Services";

		// SEO meta title and meta description  - jovanni
	    $page_meta = $this->Meta_model->get_meta($this->domain_user->agent_id, 'about');
	    $data['title'] = (isset($page_meta['meta_title']) && $page_meta['meta_title'] != '')? $page_meta['meta_title'] : 'Privacy Policy | ' . $user_info->first_name . ' ' . $user_info->last_name . ' | ' .$user_info->phone;
	    $data['description'] = (isset($page_meta['meta_description']) && $page_meta['meta_description'] != '')? $page_meta['meta_description'] : $user_info->banner_tagline;
	    $data['canonical'] = (isset($page_meta['canonical']) && $page_meta['canonical'] != '')? $page_meta['canonical'] : '';
	    // ===================
		//testt
	    
    	$this->load->view('home/page', $data);
	}
}


