<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Debugger extends MX_Controller {

	protected $domain_user;

	function __construct() {
 		parent::__construct();

 		$this->domain_user = domain_to_user();
 		$this->load->model("customer_model", "customer");
 		$this->load->model("search/search_model","search_model");
 		$this->load->model("home_model");
 		$this->load->model("Page_model","page");
    	$this->load->model("Meta_model");
 		$this->load->model("dsl/dsl_model");
 		$this->load->model("agent_sites/Agent_sites_model");
 		$this->load->library("idx_auth", $this->domain_user->agent_id);
 	}

 	public function index() {

		$debugger_list = array(
			"homepage_debugger" => "<a target='_blank' href=".base_url()."debugger/homepage_debugger>".base_url()."debugger/homepage_debugger</a>",
			"listings_debugger_spark" => "<a target='_blank' href=".base_url()."debugger/listings_debugger_spark/access_token/page>".base_url()."debugger/listings_debugger_spark/{access_token}/{page}</a>",
			"active_listings_debugger_spark" => "<a target='_blank' href=".base_url()."debugger/active_listings_debugger_spark/access_token/page>".base_url()."debugger/active_listings_debugger_spark/{access_token}/{page}</a>",

			"new_listings_debugger" => "<a target='_blank' href=".base_url()."debugger/new_listings_debugger>".base_url()."debugger/new_listings_debugger</a>",
			"savedsearch_debugger_dsl_proxy" => "<a target='_blank' href=".base_url()."debugger/savedsearch_debugger_dsl_proxy>".base_url()."debugger/savedsearch_debugger_dsl_proxy</a>",
			"savedsearch_debugger_spark" => "<a target='_blank' href=".base_url()."debugger/savedsearch_debugger_spark/access_token>".base_url()."debugger/savedsearch_debugger_spark/{access_token}</a>",
			"school_debugger" => "<a target='_blank' href=".base_url()."debugger/school_debugger/type_of_school_level>".base_url()."debugger/school_debugger/{type_of_school_level}</a>",
			"city_debugger" => "<a target='_blank' href=".base_url()."debugger/city_debugger>".base_url()."debugger/city_debugger</a>",
			"lost_token_debugger" => "<a target='_blank' href=".base_url()."debugger/lost_token_debugger>".base_url()."debugger/lost_token_debugger</a>",
			"dsl_proxy_debugger" => "<a target='_blank' href=".base_url()."debugger/dsl_proxy_debugger>".base_url()."debugger/dsl_proxy_debugger</a>",
			"dsl_listings_debugger" => "<a target='_blank' href=".base_url()."debugger/dsl_listings_debugger>".base_url()."debugger/dsl_listings_debugger</a>",
			"dsl_saved_search_debugger" => "<a target='_blank' href=".base_url()."debugger/dsl_saved_search_debugger>".base_url()."debugger/dsl_saved_search_debugger</a>",
			"office_listings_proxy" => "<a target='_blank' href=".base_url()."debugger/office_listings_proxy>".base_url()."debugger/office_listings_proxy</a>",
			"latlon_debugger" => "<a target='_blank' href=".base_url()."debugger/latlon_debugger>".base_url()."debugger/latlon_debugger</a>",
			"listingsFilterById" => "<a target='_blank' href=".base_url()."debugger/listingsFilterById/listing_id>".base_url()."debugger/listingsFilterById/{listing_id}</a>",
			"savedSearchFilterById" => "<a target='_blank' href=".base_url()."debugger/savedSearchFilterById/savedsearch_id/page>".base_url()."debugger/savedSearchFilterById/{savedsearch_id}/{page}</a>",
			"savedSearchFilterDebugger" => "<a target='_blank' href=".base_url()."debugger/savedSearchFilterDebugger/savedsearch_id/filter/page>".base_url()."debugger/savedSearchFilterDebugger/{savedsearch_id}/{filter}/{page}</a>",

			"previousOfficeSoldListingsSpark" => "<a target='_blank' href=".base_url()."debugger/previousOfficeSoldListingsSpark/page>".base_url()."debugger/previousOfficeSoldListingsSpark/{page}</a>",

			"currentOfficeSoldListingsSpark" => "<a target='_blank' href=".base_url()."debugger/currentOfficeSoldListingsSpark/page>".base_url()."debugger/currentOfficeSoldListingsSpark/{page}</a>",

			"soldListingsDebugger" => "<a target='_blank' href=".base_url()."debugger/soldListingsDebugger>".base_url()."debugger/soldListingsDebugger</a>"
		);

		echo "\n<pre style=\"background: #DDD;\">\n";
	  	$var = print_r($debugger_list, true);
	  	echo $var . "\n</pre>\n";
 		//printA($debugger_list);exit;
 	}

 	public function new_listings_debugger(){

  		$dsl = modules::load('dsl/dsl');
		$token = $this->home_model->getUserAccessToken($this->domain_user->agent_id);
		$access_token = array(
			'access_token' => $token->access_token
		);

		//subtract 1 day from today's date
		$lastday = gmdate("Y-m-d\TH:i:s\Z",strtotime("-1 days"));
		$today =  gmdate("Y-m-d\TH:i:s\Z");
		//printA($lastday);exit;

		$filterStr = "(MlsStatus Ne 'Closed' Or MlsStatus Ne 'Sold') And (OnMarketDate Bt ".$lastday.",".$today.")";
		$filter_url = rawurlencode($filterStr);
		$filter_string = substr($filter_url, 3, -3);

		$endpoint = "spark-listings?_pagination=1&_page=1&_expand=PrimaryPhoto&_filter=(".$filter_string.")";
		$results = $dsl->post_endpoint($endpoint, $access_token);
		$new_listings['results'] = json_decode($results, true);
		printA($new_listings['results']);exit;
  	}


  	public function savedsearch_debugger_dsl_proxy() {

		$savedSearches = Modules::run('dsl/getAllAgentSavedSearchFromSpark');
		printA($savedSearches);exit;
	}

  	public function savedsearch_debugger_spark($access_token) {

		if($access_token) {
			$url = getenv('SPARK_API_URL')."savedsearches";

			$headers = array(
				'Content-Type: application/json',
				'User-Agent: Spark API PHP Client/2.0',
				'X-SparkApi-User-Agent: PHP-APP-In-Fifteen-Minutes/1.0',
			    'Authorization: OAuth '. $access_token,
			);

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_URL, $url);

			$result = curl_exec($ch);
			$result_info = json_decode($result, true);

			if(empty($result_info)) {
				$result_info = curl_strerror($ch);
			}
            curl_close($ch);

			printA($result_info);exit;
			return $result_info;
		}
	}

	public function listings_debugger_spark($access_token = NULL, $page = NULL) {

		if($access_token) {
			$url = getenv('SPARK_API_URL')."my/listings?_pagination=1&_page=".$page."&_expand=PrimaryPhoto&_limit=24";

			$headers = array(
				'Content-Type: application/json',
				'User-Agent: Spark API PHP Client/2.0',
				'X-SparkApi-User-Agent: PHP-APP-In-Fifteen-Minutes/1.0',
			    'Authorization: OAuth '. $access_token,
			);

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_URL, $url);

			$result = curl_exec($ch);
			$result_info = json_decode($result, true);

			if(empty($result_info)) {
				$result_info = curl_strerror($ch);
			}
            curl_close($ch);
			printA($result_info);
			return $result_info;
		}
	}

	public function homepage_debugger() {

  		$obj = Modules::load("mysql_properties/mysql_properties");
  		$data['listings_checker'] = array(
  			'active_listings' => $obj->listings_checker($this->domain_user->id, "active"),
  			'new_listings' => $obj->listings_checker($this->domain_user->id, "new"),
  			'nearby_listings' => $obj->listings_checker($this->domain_user->id, "nearby"),
  			'office_listings' => $obj->listings_checker($this->domain_user->id, "office"),
  			'sold_listings' => $obj->listings_checker($this->domain_user->id, "sold"),
  			'user_id' => $this->domain_user->id,
  			'agent_id' => $this->domain_user->agent_id,
  			'Note:' => "0 = still using the listings(BLOB type) column || 1 = using listing(TEXT) column the optimized version of listings."

  		);

  		$data['homepage_options'] = array(
  			'featured_listings' => $obj->featured_options($this->domain_user->agent_id),
  			'active_listings' => $obj->home_options($this->domain_user->agent_id, "active_listings"),
  			'new_listings' => $obj->home_options($this->domain_user->agent_id, "new_listings"),
  			'nearby_listings' => $obj->home_options($this->domain_user->agent_id, "nearby_listings"),
  			'office_listings' => $obj->home_options($this->domain_user->agent_id, "office_listings"),
  		);

  		$data['active_listings'] = $obj->get_properties($this->domain_user->id, "active");
		$data['new_listings'] = $obj->get_properties($this->domain_user->id, "new");
		$data['office_listings'] = $obj->get_office($this->domain_user->id, "office");
		$data['sold_listings'] = $obj->get_properties($this->domain_user->id, "sold");
		$data['nearby_listings'] = $obj->get_properties($this->domain_user->id, "nearby");
		$data['account_info'] = $obj->get_account($this->domain_user->id, "account_info");
		$data['system_info'] = $obj->get_account($this->domain_user->id, "system_info");
		$data['user_info'] = $this->home_model->getUsersInfo($this->domain_user->id);

		$dsl = modules::load('dsl/dsl');
		$token = $this->home_model->getUserAccessToken($this->domain_user->agent_id);
		$access_token = array(
		'access_token' => $token->access_token
		);
		$mls_status = Modules::run('home/mls_status');
		$endpoint = "properties/".$this->domain_user->id."/status/".$mls_status."?_pagination=1&_expand=PrimaryPhoto";
		$active_listings = $dsl->get_endpoint($endpoint);
		$activeListingData = json_decode($active_listings,true);
		$activeList = array();

		if(isset($activeListingData["data"]) && !empty($activeListingData["data"])){
		  	foreach ($activeListingData["data"] as $active){
		    	$activeList[] = $active["standard_fields"][0];
		  	}
		}
		$data['active_listings_dsl'] = isset($activeList) ? $activeList :"";
  		$data["saved_search_selected_dsl"] = Modules::run('dsl/getSelectedAgentSavedSearch');
		$data["saved_search_featured_dsl"] = Modules::run('dsl/getFeaturedAgentSavedSearch');
		$data["saved_search_selected_mysql"] = $this->home_model->get_selected_saved_search();
		$data["saved_search_featured_mysql"] = $this->home_model->get_featured_saved_search();




		$officeId = $data['account_info']->OfficeId;
		$endpoint = "my/accounts/".$officeId;
		$endpoint2 = "accounts/".$officeId;
		$officeData = $dsl->getBrokerFromSpark($endpoint2);

		$officeBrokerKey = $officeData['Results']['0']['OfficeBrokerKey'];
		$endpoint2 = "accounts/".$officeBrokerKey;
		$brokerage_data = $dsl->getBrokerFromSpark($endpoint2);

		$data["office_data"] = $officeData;
		$data["brokerage_data"] = $brokerage_data;

		printA($data); exit;
  	}

  	public function active_listings_debugger_spark($access_token = NULL, $page = NULL) {

		if($access_token) {

			//get standard MLsStatus
			$mls_status = Modules::run('dsl/getMlsStatus',$this->domain_user->agent_id);
			printA($mls_status);
			
			if(in_array("Sold",$mls_status)){
				$filter_sold = "Or MlsStatus Ne 'Sold'";
			}
			if(in_array("Leased",$mls_status)){
				$filter_leased = true;
			}
			if(in_array("Pending",$mls_status)){
				$filter_pending = true;
			}

			if(isset($filter_pending) && isset($filter_leased)){
				$lp_filter = "MlsStatus Ne 'Leased' Or MlsStatus Ne 'Pending'";
			}elseif(isset($filter_leased)){
				$lp_filter = "MlsStatus Ne 'Leased'";
			}elseif(isset($filter_pending)){
				$lp_filter = "MlsStatus Ne 'Pending'";
			}

			$sold_filter  = isset($filter_sold)? $filter_sold :"";
			$filter = isset($lp_filter)?  ") And (".$lp_filter.")" :"";
			$filterStr = "(MlsStatus Ne 'Closed' ".$sold_filter." ".$filter."";
			printA("Filter: ".$filterStr);
			$filter_url = rawurlencode($filterStr);
			$filter_string = substr($filter_url, 3, -3);

			$url = getenv('SPARK_API_URL')."my/listings?_pagination=1&_page=".$page."&_expand=PrimaryPhoto&_limit=24&_filter=(".$filter_string.")";

			$headers = array(
				'Content-Type: application/json',
				'User-Agent: Spark API PHP Client/2.0',
				'X-SparkApi-User-Agent: PHP-APP-In-Fifteen-Minutes/1.0',
			    'Authorization: OAuth '. $access_token,
			);

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_URL, $url);

			$result = curl_exec($ch);
			$result_info = json_decode($result, true);

			if(empty($result_info)) {
				$result_info = curl_strerror($ch);
			}
            curl_close($ch);
			printA($result_info);
			return $result_info;
		}
	}

	public function school_debugger($name= NULL) {
		
		$obj = Modules::load("mysql_properties/mysql_properties");
		$data['account_info'] = $obj->get_account($this->domain_user->id, "account_info");
		$data['system_info'] = $obj->get_account($this->domain_user->id, "system_info");
		$mls_id = $data['account_info']->MlsId;
		
		$data['search_fields'] = $this->search_model->get_mls_search_data($mls_id);
		foreach ($data['search_fields'] as $sf) {
			if($sf->field_name == $name) {
				$search_fields_list = $this->search_model->get_mls_data_search_fields_list_data($sf->id);
				echo $name;
				printA($search_fields_list); exit;
			}
		}
		echo "Invalid Parameter";
	}

	public function city_debugger(){
  		$results = $this->idx_auth->get('standardfields/City');
  		printA($results); exit;
  	}

  	public function lost_token_debugger(){
  		$valid_token = $this->home_model->get_valid_token();
  		$lost_token = $this->home_model->get_lost_token();
  		//printA($lost_token);exit;

  		if(!empty($lost_token)){
  			foreach($lost_token as $lost){

  				if($lost->status=="pending"){
  					$domain = $lost->subdomain_url;
  				}else{
  					$domain = $lost->domains;
  				}

  				$lost_token_data[] = "Domain: ".$domain." | agent_id: ".$lost->agent_id;
  			}
  		}

  		$lost_token_info = array(
  			"valid_token_count" => count($valid_token),
  			"lost_token_count" => count($lost_token),
  			"lost_token_info" => $lost_token_data,
  		);

  		printA($lost_token_info); exit;
  	}

  	public function dsl_saved_search_debugger(){

  		$endpoint = "savedsearches/".$this->domain_user->id."";

 		if($endpoint) {
 			$call_url = rtrim(getenv('DSL_SERVER_URL'), '/')."/".$endpoint; #'agent/'.$this->domain_user->id.'/system-info';

	 		$ch = curl_init();
	 		curl_setopt($ch, CURLOPT_URL, $call_url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER , TRUE);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-Auth-Token: '.$this->session->userdata('authToken'), 'X-User-Id: '.$this->session->userdata('userId'), 'Content-Type:application/json'));
			$result_info = curl_exec($ch);

			if(!$result_info) {
				$result_info = curl_error($ch);
			}
            curl_close($ch);
			printA(json_decode($result_info),true);exit;
 		}
 	}

 	public function dsl_listings_debugger(){

  		$endpoint = "properties/".$this->domain_user->id."";

 		if($endpoint) {
 			$call_url = rtrim(getenv('DSL_SERVER_URL'), '/')."/".$endpoint; #'agent/'.$this->domain_user->id.'/system-info';

	 		$ch = curl_init();
	 		curl_setopt($ch, CURLOPT_URL, $call_url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER , TRUE);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-Auth-Token: '.$this->session->userdata('authToken'), 'X-User-Id: '.$this->session->userdata('userId'), 'Content-Type:application/json'));
			$result_info = curl_exec($ch);

			if(!$result_info) {
				$result_info = curl_error($ch);
			}
            curl_close($ch);
			printA(json_decode($result_info),true);exit;
 		}
 	}

 	public function dsl_proxy_debugger(){

 		$token = $this->home_model->getUserAccessToken($this->domain_user->agent_id);
 		$access_token = array(
 			'access_token' => $token->access_token
 		);
 		$body = $access_token;
 		$endpoint = "spark-listings";

 		if($endpoint) {

 			$call_url = rtrim(getenv('DSL_SERVER_URL'), '/')."/".$endpoint;

 			$headers = array(
	    		'Content-Type: application/json',
	    		'X-Auth-Token: '.$this->session->userdata('authToken'),
	    		'X-User-Id: '.$this->session->userdata('userId')
	    	);

 			$body_json = json_encode($body, true);

 			$ch = curl_init();
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_TIMEOUT, 0);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $body_json);
			curl_setopt($ch, CURLOPT_URL, $call_url);

			$result_info  = curl_exec($ch);
			//$result_info = json_decode($result, true);

			if(!$result_info) {
				$result_info = curl_error($ch);
			}
            curl_close($ch);
			printA(json_decode($result_info),true);exit;

 		}
 	}

 	public function office_listings_proxy(){

 	  //get OFFICE listings DSL PROXY
 	  $dsl = modules::load('dsl/dsl');
 	  $token = $this->home_model->getUserAccessToken($this->domain_user->agent_id);
 	  $access_token = array(
 			'access_token' => $token->access_token
 	  );
      $filterStrOffice = "(MlsStatus Ne 'Closed' Or MlsStatus Ne 'Sold')";
      $filter_url_office = rawurlencode($filterStrOffice);
      $filter_string_office = substr($filter_url_office, 3, -3);
      $endpoint = "office/listings/?_limit=25&_expand=PrimaryPhoto&_filter=(".$filter_string_office.")";
      $office_listings = $dsl->post_endpoint($endpoint, $access_token);
      $officelistingData = json_decode($office_listings,true);

      $office_info = array(
      	"Note: " => "Only display up to 25 Office listings. Spark max default limit for listings.",
      	"count" => count($officelistingData["data"]),
      	"data" => $officelistingData
      );
      printA($office_info);exit;
 		
 	}

 	public function latlon_debugger() {

 		$obj = Modules::load("mysql_properties/mysql_properties");
		$account_info = $obj->get_account($this->domain_user->id, "account_info");

		$address = isset($account_info->Addresses[0]->Address)? $account_info->Addresses[0]->Address :"";
		
		if(!empty($address) && $address != "********"){

			$location = array('address'=> $address);
			$options = http_build_query($location);
      $api_call = 'https://maps.googleapis.com/maps/api/geocode/json?'.$options.'&key='.getenv('GOOGLE_MAP_APIKEY');
			$api_data = @file_get_contents($api_call);
			$data = json_decode($api_data, true);

		 	if($data['status'] === 'OK'){

			  	$latlon = array(
			  		'lat' => $data['results'][0]['geometry']['location']['lat'],
			  		'lon' => $data['results'][0]['geometry']['location']['lng'],
			  	);

		  		$ret = $latlon;
		  	}
		} 

	  	printA($data);exit;
	}

	public function print_cookies() {

		printA($_COOKIE);

	}

	public function listingsFilterById($property_id=null, $agent_id=null){
		$results = $this->idx_auth->GetListing($property_id);
  		printA($results); exit;
  	}

  	public function savedSearchFilterById($savedsearch_id = NULL,$page = NULL){

		$savedSearch = $this->idx_auth->getSavedSearch($savedsearch_id);

		$dsl = modules::load('dsl/dsl');
	 	$token = $this->home_model->getUserAccessToken($this->domain_user->agent_id);
	 	$access_token =  $token->access_token;

		$filterStr = "(".$savedSearch[0]['Filter'].")";
		$filter_url = rawurlencode($filterStr);
		$filter_string = substr($filter_url, 3, -3);

		$url = getenv('SPARK_API_URL')."listings?_pagination=1&_page=".$page."&_expand=PrimaryPhoto&_limit=24&_filter=(".$filter_string.")";

		$headers = array(
			'Content-Type: application/json',
			'User-Agent: Spark API PHP Client/2.0',
			'X-SparkApi-User-Agent: PHP-APP-In-Fifteen-Minutes/1.0',
		    'Authorization: OAuth '. $access_token,
		);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_URL, $url);

		$result = curl_exec($ch);
		$result_info = json_decode($result, true);

		if(empty($result_info)) {
			$result_info = curl_strerror($ch);
		}
            curl_close($ch);

		printA($savedSearch);
		printA("Endpoint: ".$url);
		printA($result_info);

  	}

  	public function savedSearchFilterDebugger($savedsearch_id = NULL,$filter = NULL,$page = NULL){

		$savedSearch = $this->idx_auth->getSavedSearch($savedsearch_id);

		$dsl = modules::load('dsl/dsl');
	 	$token = $this->home_model->getUserAccessToken($this->domain_user->agent_id);
	 	$access_token =  $token->access_token;

		$filterStr = "(".$savedSearch[0]['Filter'].")";
		$filter_url = rawurlencode($filterStr);
		$filter_string = substr($filter_url, 3, -3);

		$url = getenv('SPARK_API_URL')."listings?_pagination=1&_page=".$page."&_expand=PrimaryPhoto&_limit=24&_filter=(".$filter.")";

		$headers = array(
			'Content-Type: application/json',
			'User-Agent: Spark API PHP Client/2.0',
			'X-SparkApi-User-Agent: PHP-APP-In-Fifteen-Minutes/1.0',
		    'Authorization: OAuth '. $access_token,
		);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_URL, $url);

		$result = curl_exec($ch);
		$result_info = json_decode($result, true);

		if(empty($result_info)) {
			$result_info = curl_strerror($ch);
		}
        curl_close($ch);

		printA($savedSearch);
		printA("Filter: ".$filter_string);
		printA("Endpoint: ".$url);
		printA($result_info);

  	}

  	public function getPreviousOfficeSoldListings($page = NULL,$pagination = FALSE){
	
		$token = $this->home_model->getUserAccessToken($this->domain_user->agent_id);
		$access_token = array('access_token' => $token->access_token ); 

		$selectFields = 'UnparsedFirstLineAddress,ListingKey,City,PostalCode,StateOrProvince,PostalCity,ClosePrice,CloseDate,ListAgentName,MlsStatus';
		//printA($limit);exit;

		$filterStr = "(HistoricalListAgentId Eq '".$this->domain_user->agent_id."' And (MlsStatus Eq 'Closed' Or MlsStatus Eq 'Sold'))";
		$filter_url = rawurlencode($filterStr);
		$filter_string = substr($filter_url, 3, -3);

		$url = getenv('SPARK_API_URL')."listings?_pagination=1&_limit=25&_page=".$page."&_expand=PrimaryPhoto&_select=".$selectFields."&_page=".$page."&_filter=(".$filter_string.")&_orderby=-CloseDate";

		$headers = array(
			'Content-Type: application/json',
			'User-Agent: Spark API PHP Client/2.0',
			'X-SparkApi-User-Agent: PHP-APP-In-Fifteen-Minutes/1.0',
		    'Authorization: OAuth '. $access_token["access_token"],
		);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_URL, $url);

		$result = curl_exec($ch);
		$result_info = json_decode($result, true);

		if(!$pagination){
			printA($result_info);exit;
		}
		
        curl_close($ch);
		return $result_info["D"]["Success"] ? (($pagination) ? $result_info["D"]["Pagination"]["TotalRows"] : $result_info["D"]["Results"] )  : FALSE;

	}

	public function getCurrentOfficeSoldListings($page = NULL,$pagination = FALSE){

	    $token = $this->home_model->getUserAccessToken($this->domain_user->agent_id);
	    $access_token = array('access_token' => $token->access_token);

	    $selectFields = 'UnparsedFirstLineAddress,ListingKey,City,PostalCode,StateOrProvince,PostalCity,ClosePrice,CloseDate,ListAgentName,MlsStatus';
	    $filterStr = "(MlsStatus Eq 'Closed' Or MlsStatus Eq 'Sold')";
	    $filter_url = rawurlencode($filterStr);
	    $filter_string = substr($filter_url, 3, -3);

	    $url = getenv('SPARK_API_URL')."my/listings?_pagination=1&_limit=25&_page=".$page."&_expand=PrimaryPhoto&_select=".$selectFields."&_filter=(".$filter_string.")&_orderby=-CloseDate";

	    $headers = array(
	      'Content-Type: application/json',
	      'User-Agent: Spark API PHP Client/2.0',
	      'X-SparkApi-User-Agent: PHP-APP-In-Fifteen-Minutes/1.0',
	        'Authorization: OAuth '. $access_token["access_token"],
	    );

	    $ch = curl_init();
	    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    curl_setopt($ch, CURLOPT_URL, $url);

	    $result = curl_exec($ch);
	    $result_info = json_decode($result, true);
	    
		if(!$pagination){
			printA($result_info);exit;
		}
        curl_close($ch);
	    return $result_info["D"]["Success"] ? (($pagination) ? $result_info["D"]["Pagination"]["TotalRows"] : $result_info["D"]["Results"] )  : FALSE;

	}

	public function soldListingsDebugger(){

  		$currentSoldListingsDb = $this->home_model->getTotalSoldListingsData("new");
  		$previousSoldListingsDb = $this->home_model->getTotalSoldListingsData("old");
  		$previousSoldListingsSpark = $this->getPreviousOfficeSoldListings(1,TRUE);
  		$currentSoldListingsSpark = $this->getCurrentOfficeSoldListings(1,TRUE);
  		$soldListingsTotal = $this->home_model->getSoldListingsData(TRUE);

		$sold_data = array(
			'previous_sold_listings_db' => $previousSoldListingsDb,
			'current_sold_listings_db' => $currentSoldListingsDb,
			'previous_sold_listings_spark' => $previousSoldListingsSpark,
			'current_sold_listings_spark' => $currentSoldListingsSpark,
			'TotalSoldListingsDBWithGroupBy' => $soldListingsTotal,

		);
		printA($sold_data);exit;

	   
	}

}
