<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Customer extends MX_Controller {

	protected $domain_user;
	
	function __construct() {

		parent::__construct();

		$this->load->library('facebook');
		$this->load->helper('email');
		$this->load->model("home_model");
		$this->load->model("customer_model", "customer");
		$this->domain_user = domain_to_user();
	}

	public function customer_dashboard() {
		if (empty($this->session->userdata("customer_id"))) {
			redirect(base_url());
		}
	    $data['page_data'] = array('slug' => 'customer-dashboard','title' => 'Dashboard');
		$user_info = $this->home_model->getUsersInfo($this->domain_user->id);
		$obj = Modules::load("mysql_properties/mysql_properties");

		$data["theme"] = $user_info->theme;
		$data["header"] = "header-page";
		$data["page"] = "customer_dashboard";
		$data["title"] = "Dashboard";

		$data["default_menu_data"] = Modules::run('menu/get_default_menu_data');
        $data["custom_menu_data"] = Modules::run('menu/get_custom_menu_data');

        $data["user_info"] = $user_info;
		$data["tag_line"] = ($user_info->tag_line) ? $user_info->tag_line : "Your home away from home";
		$data["banner_tagline"] = ($user_info->banner_tagline) ? $user_info->banner_tagline : "Everyone needs a place to call HOME";
		$data['system_info'] = $obj->get_account($this->domain_user->id, "system_info");
		$data['account_info'] = $obj->get_account($this->domain_user->id, "account_info");
		$data["socialmedia_link"] = Modules::run('agent_social_media/agent_social_media/linksInfo');

		$data['offset'] = $offset = (isset($_GET["offset"])) ? $_GET["offset"] : 0;
		$data['limit'] = $limit = 5;
		$data['total_save_properties'] = $this->customer->get_customer_properties(TRUE);
		$save_properties = $this->customer->get_customer_properties(FALSE, $limit, $offset);
		$savesearches = $this->customer->get_customer_searches();

		//Clarify query details for saved searches
		$ss_count = 0;

		foreach($savesearches as $save) {

			$ss_query = array();
        	$pc_arr = array();
			$pst_arr = array();
			$pc_count=0;
			$pst_count=0;

        	$query_url = urldecode($save->url);
        	$explode_save = explode("&", $query_url);

        	foreach($explode_save as $key=>$val) {

        		$ex_val = explode("=", $val);

        		if($ex_val[1] !== "") {

        			$pos = strpos($ex_val[0], "[");

        			if($pos) {

        				$ss_text = substr($ex_val[0], 0, -3);

        				if($ss_text === "PropertyClass") {
        					$pc_arr[$pc_count] = $ex_val[1];
        					$pc_count++;
        				}

        				if($ss_text === "PropertySubType") {
        					$pst_arr[$pst_count] = $ex_val[1];
        					$pst_count++;
        				}

        			} else {
        				$ss_query[$ss_count][$ex_val[0]] = $ex_val[1];
        			}

        		}

        	}

        	if(!empty($pc_arr)) {
        		$ss_query[$ss_count]["PropertyClass"] = $pc_arr;
        	}

        	if(!empty($pst_arr)) {
        		$ss_query[$ss_count]["PropertySubType"] = $pst_arr;
        	}

        	$savesearches[$ss_count]->details = $ss_query;

        	$ss_count++;

        }

		$savesearchesflex = $this->customer->getSaveSearchesFlex();
		$schedule_showings = $this->customer->get_customer_schedule_showings();
		$save_searches_flex = (empty($savesearchesflex)) ? array() : $savesearchesflex;
        $save_searches = array_merge($savesearches, $save_searches_flex);

		$profile = $this->customer->get_customer_profile();
		$send_email = $this->customer->get_contact_send_email();

		$details_select = "ListingKey,UnparsedAddress,PublicRemarks,BedsTotal,BathsTotal,BuildingAreaTotal,CurrentPrice";
		$photos_select = "Photos(1).Uri300";

		if(!empty($save_properties)) {
			foreach ($save_properties as $properties) {
				 $properties->properties_details = Modules::run('dsl/getListing', $properties->property_id, $details_select);
				 $properties->property_photos = Modules::run('dsl/getListingPhotos', $properties->property_id, $photos_select);
			}
		}

		$data["save_properties"] = $save_properties;
		$data["save_searches"] = $save_searches;
		$data["schedule_showings"] = $schedule_showings;
		$data["profile"] = $profile;
		$data["send_email"] = $send_email;
		$data["token_checker"] =  Modules::run('home/token_checker');

		$this->load->library('pagination');
		$config["base_url"] = base_url().'/customer-dashboard';
		$config["total_rows"] = $data['total_save_properties'];
		$config["per_page"] = $limit;
		$config['page_query_string'] = TRUE;
		$config['query_string_segment'] = 'offset';
		$this->pagination->initialize($config);
		$data["pagination"] = $this->pagination->create_links();

		$data["css"] = array("msgBoxLight.css");
		$data["js"] = array("jquery.msgBox.js", "customer.js", "responsive-tabs.js");
		$data["cdn"] = array("https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.2/js/bootstrap-switch.min.js");
		$data = add_filter('before-customer-dashboard-view', $data);
		
		$this->load->view("page", $data);
	}

	public function get_customer_notes( $id = NULL) {
		return $this->customer->get_customer_notes($id);
	}

	public function delete_customer_notes() {

		if($this->input->is_ajax_request()) {

			$response=array('success'=>FALSE, 'message'=>'Failed to delete note!');

			if($this->input->post('property_id')) {

				$ret = $this->customer->delete_customer_note($this->input->post('property_id'));

				$response['success'] = ($ret) ? TRUE : FALSE;
				$response['message'] = ($ret) ? "Note successfully deleted!" : "Failed to delete note!";

			}

			exit(json_encode($response));

		}

	}

	public function change_phone() {

		if($this->input->is_ajax_request()) {

			if($_POST) {

				$this->form_validation->set_rules("new_phone", "New Phone Number", "required|trim");

				if($this->form_validation->run() == FALSE) {
					$response = array("success" => FALSE ,"errors" => "<span style='color:red'>".validation_errors()."</span>" );
				} else {
					if($this->customer->change_phone( $this->input->post("new_phone"))) {
						$response = array("success" => TRUE ,"message" => "<span style='color:green'>New Phone Number has been successfully changed!</span>" );
					} else {
						$response = array("success" => FALSE ,"errors" => "<span style='color:red'>".validation_errors()."</span>" );
					}

				}
			}

			exit( json_encode($response) );
		}
	}

	public function customer_notes(){

		if($_POST) {

			$this->form_validation->set_rules("notes", "Notes", "required|trim");

			if ($this->form_validation->run() == FALSE) {
				$response = array("success" => FALSE ,"errors" => "<span style='color:red'>".validation_errors()."</span>" );
			} else {

				$this->customer->customer_notes( $this->input->post("property_id"));
				$this->session->set_flashdata('msg', '<div class="alert alert-flash alert-success text-center">Successfully Added Notes!</div>');

			}
		}

		redirect(base_url()."customer-dashboard");
	}


	public function request_information() {

		if($this->input->is_ajax_request()) {

			if($_POST) {

				$this->form_validation->set_rules("first_name", "First Name", "required|trim");
				$this->form_validation->set_rules("last_name", "Last Name", "required|trim");
				$this->form_validation->set_rules("email", "Email", "required|trim");
				$this->form_validation->set_rules("phone", "Phone Number", "required|trim");
				$this->form_validation->set_rules("comments", "Comments, Questions, Special Requests", "required|trim");
				// $this->form_validation->set_rules("g-recaptcha-response", "Captcha", "required");

				if($this->form_validation->run() == FALSE) {
					$response = array("success" => FALSE ,"errors" => "<span style='color:red'>".validation_errors()."</span>" );
				} else {

					$arr = array(
						'first_name' => $this->input->post("first_name"),
						'last_name' => $this->input->post("last_name"),
						'email' => $this->input->post("email"),
						'phone' => $this->input->post("phone"),
						'comments' => $this->input->post("comments")
					);

					if($this->customer->request_info($arr)) {

						$link = $this->get_requested_property_info($arr);

						$redirect = base_url().'customer-dashboard?success';

						$is_login = $this->session->userdata("customer_login");

						$response = array(
							"success" 	=> TRUE,
							"message" 	=> "<span style='color:green'><i class='fa fa-check'></i> Successfully Submitted Request Information! We will contact you as soon as possible.</span>",
							"is_login" 	=> $is_login,
							"redirect" 	=> $redirect
						);

						if($response['success']){

							$liondesk_id = Modules::run('liondesk/getLiondeskContactId', $this->session->userdata('customer_id'));

							if($liondesk_id !== false && !empty($liondesk_id)){
								
								$result = Modules::run('liondesk/createTask', array(
									'contact_id' => $liondesk_id,
							    'reminder_type_id' => 2,
							    'due_at' => date('Y-m-d H:i:s', strtotime("+5 min")) . 'Z',
							    'description' => '[URGENT] Customer Requested Information',
							    'notes' => 'Property Link: ' . $link . "\n\r" . assoc_to_string(array(
							    	'comments' => $this->input->post('comments'),
							    	'property_id' => $this->input->post('property_id'),
							    	'property_address' => $this->input->post('property_address'),
							    )),
								));
							}
						}

					} else {
						$response = array("success" => FALSE ,"errors" => "<span style='color:red'>".validation_errors()."</span>" );
					}
				}
			}
			
			exit(json_encode($response));
		}
	}

	public function get_requested_property_info($data=array()) {

		$dsl = modules::load('dsl/dsl');

		$type = $this->input->post('property_type');
		$property_address = $this->input->post('property_address');
		$property_id = $this->input->post('property_id');

		$data['unparsed_address'] = $property_address;
		$data['link'] = ($type == "featured_property") ? base_url()."property-details/".$property_id : base_url()."other-property-details/".$property_id;

		$this->send_requested_property($data);

		return $data['link'];
	}

	public function send_requested_property($data=array()) {

		$this->load->library("email");

		$agent_profile = $this->customer->getAgentInfo();

		$subs = array(
			'first_name'	=> $agent_profile->agent_name,
			'page_title' 	=> $data['unparsed_address'],
			'page_url' 		=> $data['link'],
			'customer_name' => $data['first_name']." ".$data['last_name'],
			'customer_email'=> $data['email'],
			'customer_phone'=> $data['phone'],
			'lead_message' 	=> $data['comments']
		);

		$this->email->send_email_v3(
        	'support@agentsquared.com', //sender email
        	'AgentSquared', //sender name
        	$agent_profile->email, //recipient email
        	'paul@agentsquared.com,aldwinj12345@gmail.com', //bccs
        	'7cd1e55c-50af-4b37-aa61-4ffada18ae07', //"f776f95d-636e-49f2-8b94-b2cfd4594393", //template_id
        	$email_data = array('subs'=>$subs, 'reply_to'=>$data['email'])
        );

	}

	public function delete_customer_property($id = NULL, $property_id = NULL) {

		if(empty($id)) {
			return exit(json_encode(array("success" => FALSE)));
		}

		if($this->customer->delete_customer_property($id)) {

			//delete customer notes of the property
			$this->customer->delete_customer_notes($property_id);

			if( isset($_SESSION["save_properties"] ) )
			{
				foreach ($_SESSION["save_properties"] as $session_property ) {
					if( isset($session_property["property_id"]) AND $session_property["property_id"] == $property_id)
					{
						$this->session->unset_userdata( array( $session_property["property_id"] ) );
					}
				}

			}

			$this->customer->set_new_save_property_session( $this->session->userdata("customer_id") );

			$savePropCount = $this->customer->get_customer_properties(TRUE);

			return exit(json_encode(array("success" => TRUE,"savedProp" => $savePropCount)));
		}

		return exit(json_encode(array("success" => FALSE)));
	}

	public function delete_customer_search( $property_id = NULL )
	{
		if( empty($property_id) )
		{
			return exit(json_encode(array("success" => FALSE)));
		}



		if( $this->customer->delete_customer_search( $property_id ) )
		{
			$saveSearchCount = count($this->customer->get_customer_searches());

			return exit(json_encode(array("success" => TRUE,"savedSearch" => $saveSearchCount)));
		}

		return exit(json_encode(array("success" => FALSE)));
	}

	public function delete_customer_showing( $property_id = NULL )
	{
		if( empty($property_id) )
		{
			return exit(json_encode(array("success" => FALSE)));
		}



		if( $this->customer->delete_customer_showing( $property_id ) )
		{
			$schedShowCount = count($this->customer->get_customer_schedule_showings());

			return exit(json_encode(array("success" => TRUE,"schedShowing" => $schedShowCount)));
		}

		return exit(json_encode(array("success" => FALSE)));
	}

	public function update_profile($profile_id = NULL) {

		if($this->input->is_ajax_request()) {

			$response = array("success" => FALSE, "message" => "<span style='color:red'>Failed to edit profile!</span>");

			if($this->input->post()) {

				$this->form_validation->set_rules("profile[first_name]", "First Name", "required|trim");
				$this->form_validation->set_rules("profile[last_name]", "Last Name", "required|trim");
				$this->form_validation->set_rules("profile[email]", "Email", "required|trim|valid_email");
				$this->form_validation->set_rules("profile[phone]", "Phone Number", "required|trim");

				if( $this->form_validation->run() === FALSE )
				{
					$response = ['success' => FALSE, 'message' => '<span style="color:red;">'.validation_errors().'</span>'];
					exit(json_encode($response));
				}

				$post = $this->input->post();

				if ( function_exists('validateAddress') )
				{
					if ( ! validateAddress($post['profile']['email'], 'pcre8') )
					{
						exit(json_encode(['success' => FALSE, 'message' => '<span style="color:red;">Invalid Email Address</span>']));
					}
				}

				if($post['profile']['first_name'] != strip_tags($post['profile']['first_name']) || $post['profile']['last_name'] != strip_tags($post['profile']['last_name'])) {

				    $response["message"] = "<span style='color:red;'>Please enter your name correctly.</span>";

				} else {

					$ret = $this->customer->update_contact_profile($profile_id);

					$response["success"] = $ret["success"];
					$response["message"] = $ret["message"];

				}
			}

			exit(json_encode($response));
		}
	}

	public function update_email_preferences_settings()
	{
		if( $this->input->is_ajax_request() )
		{
			if( $this->customer->update_email_preferences_settings() )
			{
				return exit(json_encode(array("success" => TRUE )));
			}
			else
			{
				return exit(json_encode(array("success" => FALSE )));
			}
		}

		return exit(json_encode(array("success" => FALSE )));
	}

	public function user_send_email()
	{
		$customer_id = $this->session->userdata("customer_id");

		if( empty($customer_id) ) return FALSE;

		if( $_POST ){

			if( $email_id = $this->customer->save_email() )
			{
				//save to activtiy
				save_activities( $customer_id, $email_id, $_POST["message"], "send email", "customer", $this->session->userdata("contact_id"));
				$this->send_email_to_customer( );

				exit(json_encode(array("success" => TRUE)));
			}
			else
			{
				exit(json_encode(array("success" => FALSE)));
			}

		}
	}

	public function send_email_to_customer()
	{
	 	$this->load->library("email");

		$agent_profile = $this->customer->getAgentInfo();
		$contact_profile = $this->customer->getContactInfo();

		$content = "Hi ".$agent_profile->agent_name.", <br><br>

			You have a new message from ".$contact_profile->contact_name."! <br><br>

			<a href='".base_url("/login")."' target = '_blank'> Read Message </a> <br><br>

			Regards, <br>

			Team AgentSquared
			";

	 	$subject ="New message from ".$contact_profile->contact_name ;

	 	return $this->email->send_email(
	 		'automail@agentsquared.com',
	 		'AgentSquared',
	 		$agent_profile->email,
	 		$subject,
	 		'email/template/default-email-body',
	 		array(
	 			"message" => $content,
	 			"subject" => $subject,
	 			"to_bcc" => "rolbru12@gmail.com"
	 		)
	 	);
	}

	public function set_email_schedule() {
		if($this->input->post('schedule')) {
			$schedule = $this->input->post('schedule');

			$update = $this->customer->set_email_schedule(array('email_schedule'=>$schedule));

			if($update) {
				$response = array("success" => TRUE);
				exit(json_encode($response));
			}
		}

		$response = array("success" => FALSE);
		exit(json_encode($response));
	}

	public function check_subscription_customer() {
		$switch = $this->customer->get_contact_subscription();
		exit(json_encode(array('is_subscribe_email' => $switch->is_subscribe_email, 'email_schedule' => $switch->email_schedule)));
	}

	public function set_email_subscription() {
		if($this->input->post('switch_post')) {
			$post = $this->input->post('switch_post');
			if($post == 'subscribe') {
				$data = array('is_subscribe_email'=> 1);
			} else {
				$data = array('is_subscribe_email'=> 0);
			}
			$this->customer->set_email_subscription($data);
		}
	}
}
