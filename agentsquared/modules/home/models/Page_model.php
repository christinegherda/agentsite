<?php if( ! defined('BASEPATH') ) exit('No direct script access allowed');

class Page_model extends CI_Model { 

	protected $domain_user;

    public function __construct()
    {
        parent::__construct();

            $this->domain_user = domain_to_user();
    }
	
    public function get_page($slug = NULL )
    {
        $array = array(
            'slug' => $slug,
            'agent_id' => $this->domain_user->agent_id,
            'status' => "published"
             );
        $this->db->select("*")
                ->from("pages")
                ->where($array);

        return $this->db->get()->row_array();
    }

    public function get_search_id($slug = NULL)
    {
        $array = array(
            'slug' => $slug,
            'agent_id' => $this->domain_user->agent_id,
            'status' => "published"
             );
        $this->db->select("saved_search_id")
                ->from("pages")
                ->where($array);

        return $this->db->get()->row_array();
    }

     public function get_page_content($slug = NULL){
        $array = array(
            'slug' => $slug,
            'agent_id' => $this->domain_user->agent_id,
            'status' => "published"
             );
        $this->db->select("content,title")
                ->from("pages")
                ->where($array);

        return $this->db->get()->row_array();
    }
}