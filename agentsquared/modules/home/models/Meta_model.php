<?php if( ! defined('BASEPATH') ) exit('No direct script access allowed');

class Meta_model extends CI_Model { 

	public function __construct(){
    parent::__construct();
  }
    
  // public function get_meta($agent_id = '', $page_slug = ''){
  //   if(!empty($agent_id) && !empty($page_slug)){
  //     $where = array(
  //       'agent_id' => $agent_id,
  //       'page_slug' => $page_slug,
  //     );
  //     $this->db->where($where);
  //     $result = $this->db->get('pages_metas')->row_array();
  //     if($result != NULL){
  //       return $result;
  //     }
  //   }
  //   return false;
  // }

  public function get_meta($agent_id = '', $page_slug = ''){
    if(!empty($agent_id) && !empty($page_slug)){
      $where = array(
        'agent_id' => $agent_id,
        'slug' => $page_slug,
      );
      $this->db->where($where);
      $result = $this->db->get('pages')->row_array();
      if($result != NULL){
        return $result;
      }
    }
    return false;
  }
  
  public function verify_gsc($agent_id = '', $gsc = ''){
    if($agent_id != '' && $gsc != ''){
      $this->db->where('agent_id', $agent_id);
      $result = $this->db->get('seo')->row();
      if($result && $result != NULL){
        if(strpos($result->gsc_verification, $gsc) !== FALSE){
          return true;
        }
      }
    }
    return false;
  }
  
}