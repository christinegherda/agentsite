<?php if( ! defined('BASEPATH') ) exit('No direct script access allowed');

class Customer_model extends CI_Model {

    protected $domain_user;

    public function __construct() {
        parent::__construct();

        $this->domain_user = domain_to_user();
    }

    public function isHaveRecord($email=NULL) {

        $this->db->select("c.phone, c.email as customer_email, c.id as customer_id, c.contact_id, c.first_name, d.domains, CONCAT(u.first_name,' ',u.last_name) as agent_name, u.email as agent_email")
                ->from("contacts c")
                ->join("domains d", "c.user_id=d.agent")
                ->join("users u", "c.user_id=u.id")
                ->where("c.email", $email)
                ->where("c.is_deleted", 0)
                ->where("c.agent_id", $this->domain_user->agent_id)
                ->limit(1);

        $query = $this->db->get()->row();

        return (!empty($query)) ? $query : FALSE;
    }

    public function get_credits($domain = NULL) {
        //
        $user = $this->db
                        ->select("agent")
                        ->from("domains")
                        ->like('domains',$domain, 'after')
                        ->where('type','agent-site')
                        ->get()->row_array();

        if(isset($user['agent'])) {
            #return $user['agent'];
            $idx = $this->db
                    ->select("*")
                    ->from("property_idx")
                    ->where('user_id', $user['agent'])
                    ->get()->row_array();
            return $idx;

        }
        else {
            return false;
        }
    }

    public function sign_up($data=array()) {

        if($data) {

            $this->db->insert('contacts', $data);

            $customer_id = $this->db->insert_id();

            $customer["customer_id"] = $customer_id;
            $customer["customer_email"] = $data['email'];
            $customer["contact_id"] = $data["contact_id"];

            //set session
            $this->set_customer_cookie($data["contact_id"]);
            $this->set_customer_session($customer);

            return $customer_id;
        }

    }

    public function set_customer_cookie($contact_id=NULL) {

        if(!empty($contact_id)) {
            $ip_address=str_replace('.', '_', $this->input->ip_address());
            setcookie('customer_'.$this->domain_user->id.'_'.$ip_address, $contact_id, time() + (86400 * 360), "/");
        }

    }

    public function sign_up_spw($data=array()) {

        if($data) {

            $this->db->insert('contacts', $data);

            $customer_id = $this->db->insert_id();

            $customer["customer_id"] = $customer_id;
            $customer["customer_email"] = $data['email'];
            $customer["contact_id"] = $data["contact_id"];

            //set session
            $this->set_customer_session($customer);

            return $customer_id;
        } 

    }

    /*public function sign_in() {

        $email = $this->input->post("email");
        $phone = $this->input->post("phone");

        $customer = $this->db->select("id, email, contact_id, phone, mobile")
                            ->from("contacts")
                            ->where("email", $email)
                            ->where("agent_id", $this->domain_user->agent_id)
                            ->where("is_deleted", 0)
                            ->get()->row_array();

        if(!empty($customer["phone"]) || !empty($customer["mobile"])) {
            
            $flag = false;
            $post_phone = preg_replace('/[^a-zA-Z0-9]/', '', $phone);
            $db_phone = preg_replace('/[^a-zA-Z0-9]/', '', $customer["phone"]);

            if(trim($post_phone) === trim($db_phone)) {
                $flag=true;
            } else {
                $db_phone = preg_replace('/[^a-zA-Z0-9]/', '', $customer["mobile"]);

                if(trim($post_phone) === trim($db_phone)) {
                    $flag=true;
                }
            }

            if($flag) {

                $c_session["customer_id"] = $customer["id"];
                $c_session["customer_email"] = $customer["email"];
                $c_session["contact_id"] = $customer["contact_id"];

                $save_properties = $this->get_save_properties($customer["id"]);

                $c_session["save_properties"] = $save_properties;

                $this->set_customer_session($c_session);

                if($this->input->post("isHavePropertyId")) {
                    $this->save_favorates_properties($_POST["isHavePropertyId"]);
                }

                if($this->input->post('isHaveSearchPropertyUrl')) {
                    $this->save_search_properties( $_POST["isHaveSearchPropertyUrl"] );
                }

                return TRUE;

            }

        }

        return FALSE;

    }*/

    public function sign_in() {

        $response = array("success"=>FALSE);

        $email = $this->input->post("email");
        $phone = $this->input->post("phone");

        if(!empty($email) && !empty($phone)) {

            $this->db->select("id, email, contact_id, phone, mobile")->from("contacts");
            $sql_str = "agent_id='".$this->domain_user->agent_id."' AND email='".$email."' AND is_deleted=0";
            $customer = $this->db->where($sql_str)->get()->row_array();

            if(!empty($customer)) {

                $post_phone = preg_replace('/[^a-zA-Z0-9]/', '', $phone);
                $db_phone   = preg_replace('/[^a-zA-Z0-9]/', '', $customer["phone"]);
                $db_mobile  = preg_replace('/[^a-zA-Z0-9]/', '', $customer["mobile"]);

                $flag = (trim($post_phone) === trim($db_phone) || trim($post_phone) === trim($db_mobile)) ? true : false;

                if($flag) {

                    $response["success"] = TRUE;

                    $c_session["customer_id"]   = $customer["id"];
                    $c_session["customer_email"]= $customer["email"];
                    $c_session["contact_id"]    = $customer["contact_id"];

                    $save_properties = $this->get_save_properties($customer["id"]);

                    $c_session["save_properties"] = $save_properties;

                    $this->set_customer_cookie($customer["contact_id"]);
                    $this->set_customer_session($c_session);

                } else {
                    $response["success"] = FALSE;
                    $response["message"] = 'Incorrect Login! Having trouble signing in? <a href="#" class="failed_login_reset" style="text-decoration: underline; font-weight: 700;">Reset your phone number</a>';
                }
        
            } else {
                $response["message"] = "You don't have a record yet!";
            }
            
        }

        return $response;
        
    }

    public function set_new_save_property_session( $customer_id = NULL )
    {
        $save_properties = $this->get_save_properties($customer_id);
        $c_session["save_properties"] = $save_properties;

        $this->set_customer_session( $c_session );

        return TRUE;
    }

    public function get_save_properties( $customer_id = NULL )
    {
        $property = $this->db->select("property_id")
                        ->from("customer_properties")
                        ->where("customer_id", $customer_id)
                        ->get()->result_array();

        return $property;
    }

    public function get_save_searches( $customer_id = NULL )
    {
        $property = $this->db->select("url")
                        ->from("customer_searches")
                        ->where("customer_id", $customer_id)
                        ->get()->result_array();

        return $property;
    }

    public function set_customer_session( $customer = array() )
    {
        if( !empty( $customer ) )
        {
            $this->session->set_userdata( $customer );
        }

        return TRUE;
    }

    public function get_customer_properties($counter=FALSE, $limit='', $offset='', $order_by='')
    {
        $this->db->select('*')
                ->from('customer_properties')
                ->where("customer_id", $this->session->userdata("customer_id"))
                ->where("user_id", $this->domain_user->id);

       if( $counter) {
            return $this->db->count_all_results();
        }
        
        if(!empty($limit)){
            $this->db->limit($limit);
        }

        if(!empty($offset)) {
            $this->db->offset($offset);
        }

        if(!empty($order_by)){
            $this->db->order_by("cpid", $order_by);
        }else{
            $this->db->order_by("cpid", "asc");
        }

        return $this->db->get()->result();
    }

    public function get_customer_searches($limit='', $order_by='')
    {
        $this->db->select('*')
                ->from('customer_searches')
                ->where("customer_id", $this->session->userdata("customer_id"))
                ->where("user_id", $this->domain_user->id);
                //->order_by("csid");
        if(!empty($limit)){
            $this->db->limit($limit);
        }

        if(!empty($order_by)){
            $this->db->order_by("csid", $order_by);
        }else{
            $this->db->order_by("csid", "asc");
        }
        
        return $this->db->get()->result();
    }

    public function get_customer_schedule_showings()
    {
        $this->db->select("css.*,contacts.phone as customer_number,CONCAT(contacts.first_name ,' ', contacts.last_name) as customer_name")
                ->from("customer_schedule_of_showing css")
                ->join("contacts", "contacts.id = css.customer_id", "left")
                ->where("css.customer_id", $this->session->userdata("customer_id"))
                ->where("css.agent_id", $this->domain_user->id)
                ->where("css.type", "agent_site")
                ->order_by("cssid","desc");

        return $this->db->get()->result();
    }

    public function getSaveSearchesFlex()
    {

        $this->db->select("id,json_saved_search_updated,img,date_modified")
                ->from("saved_searches")
                ->where("FIND_IN_SET('".$this->session->userdata("contact_id")."',ContactIds) != ",0);

        $query = $this->db->get();

        if( $query->num_rows() > 0 )
        {
            $data = $query->result();

            foreach ($data as &$key) {
                $key->json_decoded = json_decode($key->json_saved_search_updated);
            }


            return $data;
        }

        return FALSE;
    }

    public function change_phone( $new_phone = NULL )
    {
        $update["phone"] = $new_phone;
        $this->db->where( "email", $this->session->userdata("customer_email") );

        $this->db->update("contacts", $update);

        return ( $this->db->affected_rows() > 0) ? TRUE : FALSE ;
    }

    public function customer_notes($id = NULL)
    {

        $customer_notes = $this->db->select("*")
            ->from("customer_notes")
            ->where("property_id", $id)
            ->get()->row_array();

        if(!empty($customer_notes) ) {

           if($customer_notes['property_id'] == $id){

                $update["notes"] = $this->input->post("notes");
                $update["date_modified"] = date("Y-m-d H:m:s");

                $this->db->where( "property_id", $id );

                if( $this->db->update("customer_notes", $update) ) {
                    return TRUE;
                } else {
                    return FALSE;
                }
           }

        } else {

            $save["property_id"] = $id;
            $save["customer_id"] = $this->session->userdata("customer_id");
            $save["agent_id"] = $this->domain_user->id;
            $save["notes"] = $this->input->post("notes");
            $save["date_created"] = date("Y-m-d H:m:s");

            if( $this->db->insert("customer_notes", $save) ) {
                return TRUE;
            } else {
                return FALSE;
            }

        }


    }

    public function get_customer_notes( $id = NULL)
    {
        $this->db->select("*")
                ->from("customer_notes")
                ->where("property_id", $id)
                ->limit(1);

        return $this->db->get()->row();
    }

    public function delete_customer_note($property_id=NULL) {
         
        if(!empty($property_id)) {

            $this->db->where("property_id", $property_id);
            $this->db->where("customer_id", $this->session->userdata("customer_id"));
            $this->db->where("agent_id", $this->domain_user->id);
            $this->db->delete("customer_notes");

            return ($this->db->affected_rows()>0) ? TRUE : FALSE;
        }

        return FALSE;

    }

    public function request_info($arr = "") {

        $customer = $this->db->select("*")
                            ->from("contacts")
                            ->where("user_id", $this->domain_user->id)
                            ->where("email", $arr["email"])
                            ->get()->row_array();


        if(!empty($customer)) {

            //set Customer login Session
            $customerid_session = $this->session->userdata("customer_id");
            $c_session["customer_id"] = $customer["id"];
            $c_session["customer_login"] = isset($customerid_session) ? true : false;
            $c_session["customer_email"] = $customer["email"];
            $c_session["contact_id"] = $customer["contact_id"];
            $this->set_customer_session($c_session);

            $update = array(
                'first_name' => $arr["first_name"],
                'last_name' => $arr["last_name"],
                'email' => $arr["email"],
                'phone' => $arr["phone"],
                'modified' => date("Y-m-d H:m:s")
            );

            $array = array(
                'email' => $arr["email"],
                'user_id' => $this->domain_user->id
            );

            $this->db->where($array);

            if($this->db->update("contacts", $update)) {

                $cid = $this->db->insert_id();

                $message["name"] = $arr["first_name"]." ".$arr["last_name"];
                $message["email"] = $arr["email"];
                $message["phone"] = $arr["phone"];
                $message["status"] = 0;
                $message["agent_id"] = $this->domain_user->id;
                $message["date_created"] = date("Y-m-d H:i:s");

                if($this->db->insert("customer_questions", $message)) {

                    $id = $this->db->insert_id();

                    $save["mid"] = $id;
                    $save["message"] = $this->input->post("comments");
                    $save["property_id"] = $this->input->post("property_id");
                    $save["property_address"] = $this->input->post("property_address");
                    $save["customer_id"] = isset($customer["id"]) ? $customer["id"] : $this->session->userdata("customer_id");
                    $save["agent_id"] = $this->domain_user->id;
                    $save["type"] = "property inquiry";
                    $save["created"] = date("Y-m-d H:i:s");

                    $this->db->select('id')
                            ->where(array(
                                        'customer_id' => $this->session->userdata('customer_id'),
                                        'property_id' => $this->input->post("property_id"),
                                        'agent_id' => $this->domain_user->id
                                    ))
                            ->from('customer_messaging')
                            ->limit(1);

                    $res = $this->db->get()->row();

                    if(empty($res)) {
                        $this->db->insert("customer_messaging", $save);
                    }

                    return TRUE;

                }
            }

        } else {

            $contact = array(
                'ip_address' => $_SERVER['REMOTE_ADDR'],
                'first_name' => $arr["first_name"],
                'last_name' => $arr["last_name"],
                'email' => $arr["email"],
                'phone' => $arr["phone"],
                'active' => 1,
                'type' => "property_question",
                'user_id' => $this->domain_user->id,
                'agent_id' => $this->domain_user->agent_id,
                'contact_id' => generate_key("10"),
                'created' => date("Y-m-d H:m:s"),
                'modified' => date("Y-m-d H:m:s")
            );

            if($this->db->insert("contacts", $contact)) {

                $cid = $this->db->insert_id();

                // Customer login Session
                $customerid_session = $this->session->userdata("customer_id");
                $customer["customer_id"] = $cid;
                $c_session["customer_login"] = isset($customerid_session) ? true : false;
                $customer["customer_email"] = $arr['email'];
                $customer["contact_id"] = generate_key("10");
                $this->set_customer_session($customer);

                $message["name"] = $arr["first_name"]." ".$arr["last_name"];
                $message["email"] = $arr["email"];
                $message["phone"] = $arr["phone"];
                $message["status"] = 0;
                $message["agent_id"] = $this->domain_user->id;
                $message["date_created"] = date("Y-m-d H:i:s");

                if($this->db->insert("customer_questions", $message)) {

                    $id = $this->db->insert_id();

                    $save["mid"] = $id;
                    $save["message"] = $this->input->post("comments");
                    $save["property_id"] = $this->input->post("property_id");
                    $save["property_address"] = $this->input->post("property_address");
                    $save["customer_id"] = $cid;
                    $save["agent_id"] = $this->domain_user->id;
                    $save["type"] = "property inquiry";
                    $save["created"] = date("Y-m-d H:i:s");

                    if($this->db->insert("customer_messaging", $save)) {
                        return  $id;
                    }
                }
            }

        }


        return FALSE;
    }

    public function save_favorates_properties($property_id = NULL) {

        $this->db->select("property_id")
                ->from("customer_properties")
                ->where("property_id", $property_id)
                ->where("customer_id", $this->session->userdata("customer_id"))
                ->where("user_id", $this->domain_user->id)
                ->limit(1);

        $query = $this->db->get();

        if($query->num_rows() > 0) {
            return TRUE;
        }

        $save["property_id"]    = $property_id;
        $save["user_id"]        = $this->domain_user->id;
        $save["customer_id"]    = $this->session->userdata("customer_id");
        $save["date_created"]   = date("Y-m-d");
        $save["date_modified"]  = date("Y-m-d");

        if($this->db->insert("customer_properties", $save)) {

            $save_properties = $this->get_save_properties($this->session->userdata("customer_id"));
            $c_session["save_properties"] = $save_properties;
            $this->set_customer_session($c_session);

            //Send email to agent
            $this->load->library("email");

            $agent_profile = $this->customer->getAgentInfo();
            $contact_profile = $this->customer->getContactInfo();

            $subs = array(
                'first_name'        => $agent_profile->agent_name,
                'customer_name'     => $contact_profile->contact_name,
                'customer_email'    => $contact_profile->email,
                'customer_phone'    => $contact_profile->phone,
                'property_name'     => $this->input->post('property_name'),
                'property_url'      => base_url()."other-property-details/".$property_id,
            );

            $this->email->send_email_v3(
                'support@agentsquared.com', //sender email
                'AgentSquared', //sender name
                $agent_profile->email, //recipient email
                'paul@agentsquared.com,aldwinj12345@gmail.com', //bccs
                '75817aac-076c-4c49-8334-932354745c7c', //'7657c903-150f-4988-ad20-fbfc3a32e327', //template_id
                array('subs'=>$subs, 'reply_to'=>$contact_profile->email)
            );

            return TRUE;
        }

        return FALSE;

    }

    public function remove_favorite_property($property_id=NULL) {

        $data=array();
        $count=0;

        $this->db->where('property_id', $property_id);
        $this->db->where('customer_id', $this->session->userdata('customer_id'));
        $this->db->where('user_id', $this->domain_user->id);
        $this->db->delete('customer_properties');

        //Remove property in session
        $save_properties = $this->session->userdata("save_properties");

        foreach($save_properties as $key) {

            if($key['property_id']!=$property_id) {
                $data[$count]['property_id'] = $key['property_id']; 
                $count++;
            }

        }

        $this->session->unset_userdata("save_properties");

        $c_session["save_properties"] = $data;

        $this->session->set_userdata($c_session);

        return ($this->db->affected_rows()>0) ? TRUE : FALSE;
        
    }

    public function delete_customer_property($id = NULL) {

        $this->db->where("cpid", $id);
        $this->db->delete("customer_properties");

        return ($this->db->affected_rows() > 0) ? TRUE : FALSE ;

    }

    public function delete_customer_notes($property_id = NULL) {

        $this->db->where("property_id", $property_id);
        $this->db->delete("customer_notes");

        return ($this->db->affected_rows() > 0) ? TRUE : FALSE ;
    }

    public function save_search_properties($url = "" , $details = "") {

        $arr = array();
        $arr = $_GET;
        $json_search = json_encode($arr);
        $url2 = explode("=", $url);
        $name = (isset($_GET['Search'])) ? strtolower($_GET['Search']) : "";
        $save["name"] = $name;
        $save["url"] = $url;
        $save["user_id"] = $this->domain_user->id;
        $save["customer_id"] = $this->session->userdata("customer_id");
        $save["details"] = (isset($url2[6]) AND !empty($url2[6])) ? $url2[6] : (isset($url2[1]) AND !empty($url2[1])) ? str_replace("&type", " ", $url2[1]) : "";
        $save["json_search"] = $json_search;
        $save["date_created"] = date("Y-m-d");
        $save["date_modified"] = date("Y-m-d");

        if($this->db->insert("customer_searches", $save)) {

            $saved_searches = $this->get_save_searches($this->session->userdata("customer_id"));
            $c_session["saved_searches"] = $saved_searches;
            $this->set_customer_session($c_session);

            if(isset($_COOKIE["saved_searches_".$this->session->userdata("customer_id")])) {
                $count = count($_COOKIE["saved_searches_".$this->session->userdata("customer_id")])-1;
                if(!in_array($url, $_COOKIE["saved_searches_".$this->session->userdata("customer_id")])) {
                    $count++;
                    setcookie("saved_searches_".$this->session->userdata("customer_id")."[".$count."]", $url, time() + (86400 * 360), "/");
                }    
            } else {
                setcookie("saved_searches_".$this->session->userdata("customer_id")."[]", $url, time() + (86400 * 360), "/");
            }

            return TRUE;

        } else {

            return FALSE;

        }
    }

    public function delete_customer_search( $id = NULL )
    {
        $this->db->where("csid", $id);
        $this->db->delete("customer_searches");

        return ($this->db->affected_rows() > 0) ? TRUE : FALSE ;
    }

    public function delete_customer_showing( $id = NULL )
    {
        $this->db->where("cssid", $id);
        $this->db->delete("customer_schedule_of_showing");

        return ($this->db->affected_rows() > 0) ? TRUE : FALSE ;
    }

    public function customer_edit_profile( $profile_id = NULL )
    {
        $profile = $this->input->post("profile");
        $profile['created'] = date("Y-m-d");
        $profile['customer_id'] = $this->session->userdata("customer_id");

        if( $profile_id )
        {
            $this->db->where("id", $profile_id);
            $this->db->update("customer_profile", $profile);

            if( $this->db->affected_rows() > 0 )
            {
                //update email to user table
                $this->db->where("id",$this->session->userdata("customer_id"));
                $this->db->update("users", array("email" => $profile['email'], "first_name" => $profile['first_name'], "last_name" => $profile['last_name']) );

                return TRUE;
            }

            return FALSE ;
        }
        else
        {
            $this->db->insert("customer_profile", $profile);
            $pro_id = $this->db->insert_id();
            if( $pro_id )
            {
                $this->db->where("id",$this->session->userdata("customer_id"));
                $this->db->update("users", array("email" => $profile['email'], "first_name" => $profile['first_name'], "last_name" => $profile['last_name']) );
            }

            return ($pro_id) ? $pro_id : FALSE ;
        }
    }
    public function update_contact_profile($profile_id = NULL) {

        $response = array("success"=>false);

        $profile = $this->input->post("profile");
        $profile['modified'] = date("Y-m-d H:i:s");

        $query = $this->db->select("id")
                            ->from("contacts")
                            ->where("email", $profile["email"])
                            ->where("is_deleted", 0)
                            ->limit(1)
                            ->get()->row();

        if(!empty($query)) {

            if($profile_id !== $query->id) { //If the user updating email using other user's email
                $response["message"] = "<span style='color:red'>You the email you entered is already owned by another user. Please choose another email.</span>";
                return $response;
            }

        }

        $this->db->where("id", $profile_id);
        $this->db->update("contacts", $profile);

        if($this->db->affected_rows()>0) {

            $this->set_customer_session(array('customer_email'=>  $profile["email"]));

            $response["success"] = true;
            $response["message"] = "<span style='color:green'>Profile has been successfully updated!</span>";
            
        }

        return $response;

    }

    public function get_customer_profile()
    {
        $this->db->select("*")
                ->from("contacts c")
                ->where("c.id", $this->session->userdata("customer_id"))
                ->limit(1);

        return $this->db->get()->row();
    }

    public function update_email_preferences_settings()
    {

        $this->db->where("customer_id", $this->session->userdata("customer_id"));
        $this->db->update("customer_profile", array("email_settings" => $this->input->post("settings")));

        return ( $this->db->affected_rows() ) ? TRUE : FALSE ;
    }

    public function save_customer_buyer() {

        $buyer = $this->input->post("buyer");
        $password = "buyer1234";

        if(!empty($buyer)) {

            //Check if the user already signed up before
            $contact_id = $this->db->select("id, contact_id")
                                    ->from("contacts")
                                    ->where("email", $buyer["email"])
                                    ->where("user_id", $this->session->userdata("user_id"))
                                    ->get()->row_array();

            if(!empty($contact_id)) {

                //Set User's session
                $this->set_customer_session(array(
                    'customer_id'   =>  $contact_id["id"],
                    'customer_email'=>  $buyer["email"],
                    'contact_id'    =>  $contact_id["contact_id"],
                ));

                //Update existing info for user
                $update = array(
                    'first_name'    => $buyer["fname"],
                    'last_name'     => $buyer["lname"],
                    'email'         => $buyer["email"],
                    'phone'         => $buyer["phone"],
                    'address'       => $buyer["address"],
                    'mobile'        => $buyer["mobile"],
                    'city'          => $buyer["city"],
                    'state'         => $buyer["state"],
                    'zipcode'       => $buyer["zip_code"],
                    'is_deleted'    => 0,
                    'modified'      => date("Y-m-d H:m:s")
                );

                //update contacts table
                $this->db->where("email", $buyer["email"]);
                $this->db->where("id", $contact_id["id"]);
                $this->db->update("contacts", $update);

                if($this->db->affected_rows()>0) {

                    //insert customer buyer 
                    $buyer["agent_id"]      = $this->domain_user->id;
                    $buyer["date_created"]  = date("Y-m-d H:i:s");
                    $buyer["date_modified"] = date("Y-m-d H:i:s");
                    $buyer["contact_id"]    = $contact_id["id"];

                    $this->db->insert("customer_buyer", $buyer);
                    
                }

            } else {

                $save = array(
                    'ip_address'    => $_SERVER['REMOTE_ADDR'],
                    'first_name'    => $buyer["fname"],
                    'last_name'     => $buyer["lname"],
                    'email'         => $buyer["email"],
                    'password'      => password_hash($password, PASSWORD_DEFAULT),
                    'phone'         => $buyer["phone"],
                    'address'       => $buyer["address"],
                    'mobile'        => $buyer["mobile"],
                    'city'          => $buyer["city"],
                    'state'         => $buyer["state"],
                    'zipcode'       => $buyer["zip_code"],
                    'active'        => 1,
                    'type'          => "buyer",
                    'user_id'       => $this->domain_user->id,
                    'agent_id'      => $this->domain_user->agent_id,
                    'contact_id'    => generate_key("10"),
                    'created'       => date("Y-m-d H:m:s"),
                    'modified'      => date("Y-m-d H:m:s")
                );

                if($this->db->insert("contacts", $save)) {

                    $id = $this->db->insert_id();

                    $buyer["agent_id"]      = $this->domain_user->id;
                    $buyer["date_created"]  = date("Y-m-d H:i:s");
                    $buyer["date_modified"] = date("Y-m-d H:i:s");
                    $buyer["contact_id"]    = $id;

                    if($this->db->insert("customer_buyer", $buyer)) {

                        $this->set_customer_session(array(
                            'customer_id'   =>  $id,
                            'customer_email'=>  $buyer["email"],
                            'contact_id'    =>  $save["contact_id"],
                        ));

                    }
                }
            }

            return (!empty($contact_id))? $contact_id["id"] : $id; 
        }

        return FALSE;

    }

    public function save_customer_seller() {

        $seller = $this->input->post("seller");
        $password = "seller1234";

        if(!empty($seller)) {

            //Check if user already exist
            $contact_id = $this->db->select("id, contact_id")
                                ->from("contacts")
                                ->where("email", $seller["email"])
                                ->where("user_id", $this->session->userdata("user_id"))
                                ->get()->row_array();

            if(!empty($contact_id)) { //If user already exist, update info

                $this->set_customer_session(array(
                    'customer_id'   =>  $contact_id["id"],
                    'customer_email'=>  $seller["email"],
                    'contact_id'    =>  $contact_id["contact_id"],
                ));

                $update = array(
                    'first_name'    => $seller["fname"],
                    'last_name'     => $seller["lname"],
                    'email'         => $seller["email"],
                    'phone'         => $seller["phone"],
                    'address'       => $seller["address"],
                    'mobile'        => $seller["mobile"],
                    'city'          => $seller["city"],
                    'state'         => $seller["state"],
                    'zipcode'       => $seller["zip_code"],
                    'is_deleted'    => 0,
                    'modified'      => date("Y-m-d H:m:s")
                );

                //update contacts table
                $this->db->where("email", $seller["email"]);
                $this->db->where("id", $contact_id["id"]);
                $this->db->update("contacts", $update);

                if($this->db->affected_rows()>0) {

                    //insert customer seller
                    $seller["agent_id"] = $this->domain_user->agent_id;
                    $seller["date_created"] = date("Y-m-d H:i:s");
                    $seller["contact_id"] = $contact_id["id"];

                    $this->db->insert("customer_seller", $seller);
                    
                    /*$this->db->select("email")
                            ->from("customer_seller")
                            ->where("contact_id", $contact_id["id"])
                            ->where("email", $seller["email"])
                            ->where("agent_id", $this->domain_user->agent_id)
                            ->limit(1);

                    $query = $this->db->get()->row();

                    if(empty($query)) {

                        //insert customer seller
                        $seller["agent_id"] = $this->domain_user->id;
                        $seller["date_created"] = date("Y-m-d H:i:s");
                        $seller["contact_id"] = $contact_id["id"];

                        $this->db->insert("customer_seller", $seller);

                    }*/ 
                }

            } else {

                $save = array(
                    'ip_address'    => $_SERVER['REMOTE_ADDR'],
                    'first_name'    => $seller["fname"],
                    'last_name'     => $seller["lname"],
                    'email'         => $seller["email"],
                    'phone'         => $seller["phone"],
                    'mobile'        => $seller["mobile"],
                    'address'       => $seller["address"],
                    'city'          => $seller["city"],
                    'state'         => $seller["state"],
                    'zipcode'       => $seller["zip_code"],
                    'password'      => password_hash($password, PASSWORD_DEFAULT),
                    'active'        => 1,
                    'type'          => "seller",
                    'user_id'       => $this->domain_user->id,
                    'agent_id'      => $this->domain_user->agent_id,
                    'contact_id'    => generate_key("10"),
                    'created'       => date("Y-m-d H:m:s"),
                    'modified'      => date("Y-m-d H:m:s")
                );

                // save for contacts
                if($this->db->insert("contacts", $save)) {

                    $id = $this->db->insert_id();

                    $seller["agent_id"]     = $this->domain_user->agent_id;
                    $seller["date_created"] = date("Y-m-d H:i:s");
                    $seller["contact_id"]   = $id;

                    if($this->db->insert("customer_seller", $seller)) {

                        $this->set_customer_session(array(
                            'customer_id'       =>  $id,
                            'customer_email'    =>  $seller["email"],
                            'contact_id'        =>  $save["contact_id"],
                        ));

                    }
                }

            }

            return (!empty($contact_id))? $contact_id["id"] : $id;

        }

        return FALSE;

    }

    public function save_customer_questions() {

        $response = array("success"=>FALSE, "message"=>"<span class='alert alert-danger alert-dismissible' role='alert' style='display:block'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>Failed to send message!</span>", "is_login"=>FALSE);

        if($this->session->userdata("customer_id")) {
            $message["name"]        = $this->input->post("first_name")." ".$this->input->post("last_name");
            $message["email"]       = $this->session->userdata("customer_email");
            $message["phone"]       = $this->input->post("phone");
            $message["status"]      = 0;
            $message["agent_id"]    = $this->domain_user->id;
            $message["date_created"]= date("Y-m-d H:i:s");

            if($this->db->insert("customer_questions", $message)) {

                $save["mid"]        = $this->db->insert_id();
                $save["message"]    = $this->input->post("message");
                $save["customer_id"]= $this->session->userdata("customer_id");
                $save["agent_id"]   = $this->domain_user->id;
                $save["type"]       = "general inquiry";
                $save["created"]    = date("Y-m-d H:i:s");

                $this->db->insert("customer_messaging", $save);

                $response["success"] = TRUE;
                $response["is_login"] = TRUE;
                $response["message"] = "<span class='alert alert-success alert-dismissible' role='alert' style='display:block'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>Message has been Sent!</span>";

            }

        } else {
            $customer = $this->db->select("id, email, contact_id, phone, mobile")
                                ->from("contacts")
                                ->where("email", $this->input->post("email"))
                                ->where("agent_id", $this->domain_user->agent_id)
                                ->where("is_deleted", 0)
                                ->get()->row_array();

            if(!empty($customer)) { //The customer already signed up before

                $post_phone = preg_replace('/[^a-zA-Z0-9]/', '', $this->input->post("phone"));
                $db_phone   = preg_replace('/[^a-zA-Z0-9]/', '', $customer["phone"]);
                $db_mobile  = preg_replace('/[^a-zA-Z0-9]/', '', $customer["mobile"]);

                $flag = (trim($post_phone) === trim($db_phone) || trim($post_phone) === trim($db_mobile)) ? true : false;

                if($flag) {

                    //Set customer sessions
                    $c_session["customer_login"]= TRUE;
                    $c_session["customer_id"]   = $customer["id"];
                    $c_session["customer_email"]= $customer["email"];
                    $c_session["contact_id"]    = $customer["contact_id"];
                    $this->set_customer_session($c_session);
                    //Update contact table
                    $update = array(
                        'first_name'=> $this->input->post("first_name"),
                        'last_name' => $this->input->post("last_name"),
                        'modified'  => date("Y-m-d H:m:s")
                    );

                    $array = array(
                        'email'     => $this->input->post("email"),
                        'user_id'   => $this->domain_user->id
                    );

                    $this->db->where($array);

                    if($this->db->update("contacts", $update)) {

                        //insert customer questions
                        $message["name"]        = $this->input->post("first_name")." ".$this->input->post("last_name");
                        $message["email"]       = $this->input->post("email");
                        $message["phone"]       = $this->input->post("phone");
                        $message["status"]      = 0;
                        $message["agent_id"]    = $this->domain_user->id;
                        $message["date_created"]= date("Y-m-d H:i:s");

                        if($this->db->insert("customer_questions", $message)) {

                            //insert customer messaging
                            $id = $this->db->insert_id();

                            $save["mid"]        = $id;
                            $save["message"]    = $this->input->post("message");
                            $save["customer_id"]= isset($customer["id"]) ? $customer["id"] : $this->session->userdata("customer_id");
                            $save["agent_id"]   = $this->domain_user->id;
                            $save["type"]       = "general inquiry";
                            $save["created"]    = date("Y-m-d H:i:s");

                            $this->db->insert("customer_messaging", $save);

                            $response["success"] = TRUE;
                            $response["message"] = "<span class='alert alert-success alert-dismissible' role='alert' style='display:block'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>Message has been Sent!</span>";
                        }

                    }

                } else {
                    $response["message"] = "<span class='alert alert-danger alert-dismissible' role='alert' style='display:block'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>The email address that you are using already exists in the agent’s dashboard system. Please use another email address to sign-up.</span>";
                }

            } else {
                $contact = array(
                    'ip_address'=> $_SERVER['REMOTE_ADDR'],
                    'first_name'=> $this->input->post("first_name"),
                    'last_name' => $this->input->post("last_name"),
                    'email'     => $this->input->post("email"),
                    'phone'     => $this->input->post("phone"),
                    'active'    => 1,
                    'type'      => "general_question",
                    'user_id'   => $this->domain_user->id,
                    'agent_id'  => $this->domain_user->agent_id,
                    'contact_id'=> generate_key("10"),
                    'created'   => date("Y-m-d H:m:s"),
                    'modified'  => date("Y-m-d H:m:s")
                );

                if($this->db->insert("contacts", $contact)) {

                    $cid = $this->db->insert_id();

                    // Customer login Session
                    $c_session["customer_login"]= TRUE;
                    $c_session["customer_id"]   = $cid;
                    $c_session["customer_email"]= $this->input->post("email");
                    $c_session["contact_id"]    = generate_key("10");
                    $this->set_customer_session($c_session);
                    
                    $message["name"]        = $this->input->post("first_name")." ".$this->input->post("last_name");
                    $message["email"]       = $this->input->post("email");
                    $message["phone"]       = $this->input->post("phone");
                    $message["status"]      = 0;
                    $message["agent_id"]    = $this->domain_user->id;
                    $message["date_created"]= date("Y-m-d H:i:s");

                    if($this->db->insert("customer_questions", $message)) {

                        $id = $this->db->insert_id();

                        $save["mid"]        = $id;
                        $save["message"]    = $this->input->post("message");
                        $save["customer_id"]= $cid;
                        $save["agent_id"]   = $this->domain_user->id;
                        $save["type"]       = "general inquiry";
                        $save["created"]    = date("Y-m-d H:i:s");

                        $this->db->insert("customer_messaging", $save);

                        $response["success"] = TRUE;
                        $response["message"] = "<span class='alert alert-success alert-dismissible' role='alert' style='display:block'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>Message has been Sent!</span>";

                    }
                }

            }

        }

        return $response;

    }

    /*public function save_customer_questions($arr = "") {

        $customer = $this->db->select("*")
                            ->from("contacts")
                            ->where("email", $arr["email"])
                            ->where("phone", $arr["phone"])
                            ->where("agent_id", $this->domain_user->agent_id)
                            ->where("is_deleted", 0)
                            ->get()->row_array();

        if(!empty($customer)) { //The customer already signed up before

            //set Customer login Session
            $customerid_session = $this->session->userdata("customer_id");

            $c_session["customer_login"]= isset($customerid_session) ? true : false;
            $c_session["customer_id"]   = $customer["id"];
            $c_session["customer_email"]= $customer["email"];
            $c_session["contact_id"]    = $customer["contact_id"];

            $this->set_customer_session($c_session);

            $update = array(
                'first_name'=> $arr["first_name"],
                'last_name' => $arr["last_name"],
                'email'     => $arr["email"],
                'phone'     => $arr["phone"],
                'modified'  => date("Y-m-d H:m:s")
            );

            $array = array(
                'email' => $arr["email"],
                'user_id' => $this->domain_user->id
            );

            $this->db->where($array);

            if($this->db->update("contacts", $update)) {

                $cid = $this->db->insert_id();

                $message["name"]        = $arr["first_name"]." ".$arr["last_name"];
                $message["email"]       = $arr["email"];
                $message["phone"]       = $arr["phone"];
                $message["status"]      = 0;
                $message["agent_id"]    = $this->domain_user->id;
                $message["date_created"]= date("Y-m-d H:i:s");

                if($this->db->insert("customer_questions", $message)) {

                    $id = $this->db->insert_id();

                    $save["mid"]        = $id;
                    $save["message"]    = $this->input->post("message");
                    $save["customer_id"]= isset($customer["id"]) ? $customer["id"] : $this->session->userdata("customer_id");
                    $save["agent_id"]   = $this->domain_user->id;
                    $save["type"]       = "general inquiry";
                    $save["created"]    = date("Y-m-d H:i:s");

                    if($this->db->insert("customer_messaging", $save)) {
                        return  $id;
                    }
                }
            }

        } else {

            $contact = array(
                'ip_address'=> $_SERVER['REMOTE_ADDR'],
                'first_name'=> $arr["first_name"],
                'last_name' => $arr["last_name"],
                'email'     => $arr["email"],
                'phone'     => $arr["phone"],
                'active'    => 1,
                'type'      => "general_question",
                'user_id'   => $this->domain_user->id,
                'agent_id'  => $this->domain_user->agent_id,
                'contact_id'=> generate_key("10"),
                'created'   => date("Y-m-d H:m:s"),
                'modified'  => date("Y-m-d H:m:s")
            );

            if($this->db->insert("contacts", $contact)) {

                $cid = $this->db->insert_id();

                // Customer login Session
                $customerid_session = $this->session->userdata("customer_id");
                $customer["customer_login"] = isset($customerid_session) ? true : false;
                $customer["customer_id"] = $cid;
                $customer["customer_email"] = $arr['email'];
                $customer["contact_id"] = generate_key("10");
                $this->set_customer_session($customer);

                $message["name"] = $arr["first_name"]." ".$arr["last_name"];
                $message["email"] = $arr["email"];
                $message["phone"] = $arr["phone"];
                $message["status"] = 0;
                $message["agent_id"] = $this->domain_user->id;
                $message["date_created"] = date("Y-m-d H:i:s");

                if($this->db->insert("customer_questions", $message)) {

                    $id = $this->db->insert_id();

                    $save["mid"] = $id;
                    $save["message"] = $this->input->post("message");
                    $save["customer_id"] = $cid;
                    $save["agent_id"] = $this->domain_user->id;
                    $save["type"] = "general inquiry";
                    $save["created"] = date("Y-m-d H:i:s");

                    if($this->db->insert("customer_messaging", $save)) {
                        return  $id;
                    }
                }
            }
        }

        return FALSE;
    }*/

    public function save_schedule_for_showing() {

        $schedule["message"] = $this->input->post("sched_for_showing");
        $schedule["property_id"] = $this->input->post("property_id");
        $schedule["customer_id"] = $this->session->userdata("customer_id");
        $schedule["customer_email"] = $this->session->userdata("customer_email");
        $schedule["created"] = date("Y-m-d H:m:s");
        $schedule["agent_id"] =$this->domain_user->id;
        $schedule["type"] = "agent_site";
        $schedule["property_name"] = $this->input->post("property_name");
        $schedule["date_scheduled"] = $this->input->post("date_scheduled");

        return ($this->db->insert("customer_schedule_of_showing", $schedule)) ? $this->db->insert_id() : FALSE;

    }

    public function save_schedule_for_showing_spw($id) {

        $schedule["message"] = $this->input->post("sched_for_showing");
        $schedule["property_id"] = $this->input->post("property_id");
        $schedule["customer_id"] = $id;
        $schedule["customer_name"] = $this->input->post("fname")." ".$this->input->post("lname");
        $schedule["customer_email"] = $this->input->post("email");
        $schedule["customer_number"] = $this->input->post("phone_signup");
        $schedule["created"] = date("Y-m-d H:m:s");
        $schedule["agent_id"] =$this->domain_user->id;
        $schedule["type"] = "spw";
        $schedule["property_name"] = $this->input->post("property_name");
        $schedule["date_scheduled"] = $this->input->post("date_scheduled");

        return ($this->db->insert("customer_schedule_of_showing", $schedule)) ? $this->db->insert_id() : FALSE;

    }

    public function is_email_exist()
    {
        $this->db->select("email")
                ->from("contacts")
                ->where("email", $this->input->post("email"))
                ->where("agent_id", $this->domain_user->agent_id)
                ->where("is_deleted", 0)
                ->limit(1);

        $query = $this->db->get();

        return ($query->num_rows() > 0) ? TRUE : FALSE;
    }

    public function is_registered( $infos = array() )
    {
        $array = array(
            'social_id' => $infos["id"],
            'email'     => $infos["email"],
            'user_id'   => $this->domain_user->id,
            'agent_id'  => $this->domain_user->agent_id,
            'is_deleted' => 0
        );

        $this->db->select("*")
                ->from("contacts")
                ->where($array)
                ->limit(1);

        $query = $this->db->get();

        return ( $query->num_rows() > 0 ) ? $query->row_array() : FALSE;

    }

    public function insert_fb_registration( $infos = array() , $contactId = NULL )
    {
        $contactId = ( !empty($contactId) ) ? $contactId : generate_key("10");
        $name = explode(" ", $infos["name"] );

        $type = "facebook";

        $arr = array(
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'first_name' => (isset($name[0])) ? $name[0] : "",
            'last_name' => (isset($name[1])) ? $name[1] : "",
            'email' => $infos["email"],
            'active' => 1,
            'type' => $type,
            'user_id' => $this->domain_user->id,
            'agent_id' => $this->domain_user->agent_id,
            'contact_id' => $contactId,
            'created' => date("Y-m-d H:m:s"),
            'modified' => date("Y-m-d H:m:s"),
            'social_id' =>$infos["id"],
        );

        if(  $this->db->insert('contacts', $arr) )
        {
            $customer_id = $this->db->insert_id();

            $customer["customer_id"] = $customer_id;
            $customer["customer_email"] = $infos["email"];
            //set session

            $this->set_customer_session( $customer );

            return $customer_id;
        }
    }

    public function get_contact_send_email()
    {
        $this->db->select("customer_messaging.*, CONCAT(contacts.first_name ,' ', contacts.last_name) as contact_name, CONCAT(users.first_name ,' ', users.last_name) as agent_name ")
                ->from("customer_messaging")
                ->join("contacts", "customer_messaging.contact_id = contacts.contact_id", "left")
                ->join("users", "users.agent_id = customer_messaging.agent_id", "left")
                ->where("customer_messaging.customer_id", $this->session->userdata("customer_id"))
                ->where("customer_messaging.agent_id !=", 0);


        return $this->db->get()->result();
    }

    public function save_email(){

        $save["customer_id"] =  $this->session->userdata("customer_id");
        $save["contact_id"] =  $this->session->userdata("contact_id");
        $save["agent_id"] =  $this->domain_user->agent_id;
        $save["sent_by"] =  $this->session->userdata("customer_id");
        $save["created"] =  date("Y-m-d H:m:s");
        $save["type"] = 0;
        $save["message"] = $_POST["message"];

        $this->db->insert("customer_messaging", $save);

        return ( $this->db->insert_id() ) ? $this->db->insert_id() : FALSE;
    }

    public function getContactInfo()
    {
        $this->db->select("CONCAT(first_name ,' ', last_name) as contact_name, email, phone")
                ->from("contacts")
                ->where("id", $this->session->userdata("customer_id"))
                ->limit(1);

        return $this->db->get()->row();
    }

    public function getContactInfo_spw($email)
    {
        $this->db->select("CONCAT(first_name ,' ', last_name) as contact_name, email, phone, id")
                ->from("contacts")
                ->where("email", $email)
                ->limit(1);

        return $this->db->get()->row();
    }

    public function getAgentInfo()
    {
        $this->db->select("CONCAT(first_name ,' ', last_name) as agent_name,email")
                ->from("users")
                ->where("agent_id", $this->domain_user->agent_id)
                ->limit(1);

        return $this->db->get()->row();
    }

    public function set_email_schedule($data=array()) {

        $ret = FALSE;

        if($data) {
            $this->db->where('id', $this->session->userdata('customer_id'));
            $this->db->update('contacts', $data);

            $ret = ($this->db->affected_rows() > 0) ? TRUE : FALSE;
        }

        return $ret;
    }

    public function get_contact_subscription() {

        $this->db->select('is_subscribe_email, email_schedule')
                ->from('contacts')
                ->where('id', $this->session->userdata('customer_id'));

        return $this->db->get()->row();
    }

    public function set_email_subscription($data=array()) {
        $this->db->where('id', $this->session->userdata('customer_id'));
        $this->db->update('contacts', $data);
    }

    public function getSpwLeadsConfig() {

        return $this->db->select('view_site_spw, minutes_pass_spw, minute_capture_spw, allow_skip_spw')
                        ->from('leads_config')
                        ->where('user_id', $this->session->userdata('user_id'))
                        ->get()->row();

    }
}
