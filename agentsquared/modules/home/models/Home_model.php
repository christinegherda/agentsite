<?php if( ! defined('BASEPATH') ) exit('No direct script access allowed');


class Home_model extends CI_Model { 

    protected $domain_user;

	public function __construct() {
        parent::__construct();

        $this->domain_user = domain_to_user();
        $this->db_slave =  $this->load->database('db_slave', TRUE);
    }

    public function getUsersAgentId($user_id = NULL) {
        $this->db_slave->select("agent_id")
                ->from("users")
                ->where("users.id", $user_id)
                ->limit(1);

        return $this->db_slave->get()->row();
    }

    public function getUsersInfo($user_id = NULL) {
        $this->db_slave->select("*")
                ->from("users")
                ->where("users.id", $user_id)
                ->join("users_idx_token uit", "users.agent_id=uit.agent_id", "left")
                ->join('freemium', 'freemium.subscriber_user_id=users.id', 'left')
                ->limit(1);

        return $this->db_slave->get()->row();
    }
    public function getUserAcessType($agent_id = NULL) {
        /* Distinguising between Bearer and Auth Token users */
        $this->db_slave->select("auth_type")
                ->from("users")
                ->where("agent_id", $agent_id);
        $ret = $this->db_slave->get()->row();
        $type = $ret->auth_type;

        return $type;
    }
    public function getUserAccessToken( $agent_id = NULL ) {
        /* Distinguising between Bearer and Auth Token users */
        $this->db_slave->select("auth_type")
                ->from("users")
                ->where("agent_id", $agent_id);
        $ret = $this->db_slave->get()->row();
        $type = $ret->auth_type;

        if($type) {
             $this->db_slave->select("user_bearer_token.bearer_token as access_token")
                ->from("user_bearer_token")
                ->where("user_bearer_token.agent_id", $agent_id);
        }
        else {
             $this->db_slave->select("access_token")
                ->from("users_idx_token")
                ->where("agent_id", $agent_id);
        }
        
        return $this->db_slave->get()->row();
    }

    public function getSliderPhotos() {
        $this->db_slave->select("photo_url,is_freemium")
                ->from("slider_photos")
                ->where("user_id", $this->domain_user->id);

        return $this->db_slave->get()->result();
    }

    public function get_capture_leads() {

        $this->db_slave->select('capture_leads, spw_capture_leads, is_email_market')
                ->from('users')
                ->where('agent_id', $this->domain_user->agent_id);

        return $this->db_slave->get()->row();
    }
    
     public function get_active_featured(){
        $array = array(
            'is_featured' => 1,
            'option_name' => 'active_listings',
            'agent_id' => $this->domain_user->agent_id
             );
        $this->db_slave->select("*")
                ->from("home_options")
                ->where($array);

        return $this->db_slave->get()->row_array();
    }

     public function get_new_featured(){

        $array = array(
            'is_featured' => 1,
            'option_name' => 'new_listings',
            'agent_id' => $this->domain_user->agent_id
             );
        $this->db_slave->select("*")
                ->from("home_options")
                ->where($array);

        return $this->db_slave->get()->row_array();
    }

     public function get_nearby_featured(){

        $array = array(
            'is_featured' => 1,
            'option_name' => 'nearby_listings',
            'agent_id' => $this->domain_user->agent_id
             );
        $this->db_slave->select("*")
                ->from("home_options")
                ->where($array);

        return $this->db_slave->get()->row_array();
    }

     public function get_office_featured(){

        $array = array(
            'is_featured' => 1,
            'option_name' => 'office_listings',
            'agent_id' => $this->domain_user->agent_id
             );
        $this->db_slave->select("*")
                ->from("home_options")
                ->where($array);

        return $this->db_slave->get()->row_array();
    }

    public function get_active_title(){

        $array = array(
            'option_name' => 'active_listings',
            'agent_id' => $this->domain_user->agent_id
             );
        $this->db_slave->select("*")
                ->from("home_options")
                ->where($array);

        return $this->db_slave->get()->row_array();
    }

    public function get_nearby_title(){

        $array = array(
            'option_name' => 'nearby_listings',
            'agent_id' => $this->domain_user->agent_id
             );
        $this->db_slave->select("*")
                ->from("home_options")
                ->where($array);

        return $this->db_slave->get()->row_array();
    }

    public function get_new_title(){

        $array = array(
            'option_name' => 'new_listings',
            'agent_id' => $this->domain_user->agent_id
             );
        $this->db_slave->select("*")
                ->from("home_options")
                ->where($array);

        return $this->db_slave->get()->row_array();
    }

    public function get_office_title(){

        $array = array(
            'option_name' => 'office_listings',
            'agent_id' => $this->domain_user->agent_id
             );
        $this->db_slave->select("*")
                ->from("home_options")
                ->where($array);

        return $this->db_slave->get()->row_array();
    }
    
    public function get_active_selected(){

        $array = array(
            'option_value' => 1,
            'is_featured'  => 0,
            'option_name' => 'active_listings',
            'agent_id' => $this->domain_user->agent_id
             );
        $this->db_slave->select("*")
                ->from("home_options")
                ->where($array);

        return $this->db_slave->get()->row_array();
    }

     public function get_new_selected(){

        $array = array(
            'option_value' => 1,
            'is_featured'  => 0,
            'option_name' => 'new_listings',
            'agent_id' => $this->domain_user->agent_id
             );
        $this->db_slave->select("*")
                ->from("home_options")
                ->where($array);

        return $this->db_slave->get()->row_array();
    }

     public function get_nearby_selected(){

        $array = array(
            'option_value' => 1,
            'is_featured'  => 0,
            'option_name' => 'nearby_listings',
            'agent_id' => $this->domain_user->agent_id
             );
        $this->db_slave->select("*")
                ->from("home_options")
                ->where($array);

        return $this->db_slave->get()->row_array();
    }

     public function get_office_selected(){

        $array = array(
            'option_value' => 1,
            'is_featured'  => 0,
            'option_name' => 'office_listings',
            'agent_id' => $this->domain_user->agent_id
             );
        $this->db_slave->select("*")
                ->from("home_options")
                ->where($array);

        return $this->db_slave->get()->row_array();
    }

    public function is_custom_home(){
        $array = array(
            'option_value' => 1,
            'agent_id' => $this->domain_user->agent_id
             );
        $this->db_slave->select("*")
                ->from("home_options")
                ->where($array);

        return $this->db_slave->get()->row_array();
    }

    public function get_saved_search_title(){

         $this->db_slave->select("*")
            ->from("home_options")
            ->where("agent_id", $this->domain_user->agent_id)
            ->where("option_name", "saved_search_title");
        
        return $this->db_slave->get()->row_array();
    }

     public function get_sold_listings_title(){

        $this->db_slave->select("listing_id")
            ->from("sold_listings_options")
            ->where("user_id", $this->domain_user->id)
            ->limit(1);

         $query = $this->db_slave->get()->row();
         if(!empty($query)){
            $this->db_slave->select("option_title")
                ->from("home_options")
                ->where("agent_id", $this->domain_user->agent_id)
                ->where("option_name", "sold_listings_title");

            return $this->db_slave->get()->row_array();
         }
         return FALSE;
    }

    public function save_testimonial($data=array()) {
        $ret = FALSE;
        if($data) {
            $this->db->insert('testimonials', $data);
            $ret = TRUE;
        }
        return $ret;
    }

    public function save_whitelisted_ips($data = array()) {
        $ret = FALSE;
        if($data) {
            $this->db->insert('whitelisted_ips', $data);
            $ret = TRUE;
        }
        return $ret;
    }

    public function get_sendgrid_rule_id_by_ip($ip) {
        $this->db->select("rule_id")
            ->from("whitelisted_ips")
            ->where("ip_address", $ip);

         return $this->db->get()->row();
    }

    public function get_featured_saved_search_data($user_id = NULL) {

        if($user_id) {

            $this->db_slave->select("listings")
                    ->from("featured_saved_search_listings")
                    ->where("user_id", $user_id)
                    ->limit(1);

            $query = $this->db_slave->get()->row();

            return ($query) ? $query : FALSE;

        }
    }

    public function get_theme_settings($agent_id = NULL){
        if($agent_id !== NULL){
            $this->db_slave->where('agent_id', $agent_id);
            $query = $this->db_slave->get('agent_theme_settings')->row();
            if(!empty($query)){
              return $query;
            }
        }
        return false;
    }

    public function get_seo_settings($agent_id = NULL){
        if($agent_id !== NULL){
            $this->db_slave->where('seo.agent_id', $agent_id);
            $query = $this->db_slave->get('seo')->row();
            if(!empty($query)){
              return $query;
            }
        }
        return false;
    }

    public function token_checker() {

        $this->db_slave->select('is_valid')
                ->from('users_idx_token')
                ->where('agent_id', $this->domain_user->agent_id)
                ->limit(1);

        $query = $this->db_slave->get()->row();

        if(!empty($query)) {

            return ($query->is_valid) ? '1' : '';

        } else {

            $bearer = $this->db_slave->select('user_bearer_token.bearer_token')
                                ->from('user_bearer_token')
                                ->where('user_bearer_token.agent_id', $this->domain_user->agent_id)
                                ->limit(1);

            return $bearer;

        }

    }

     public function get_valid_token(){
        $this->db_slave->select("users_idx_token.agent_id,users_idx_token.access_token,users_idx_token.date_created,users_idx_token.date_modified,
            users_idx_token.expected_token_expired,users_idx_token.is_valid")
                ->from("users_idx_token")
                ->join("agent_subscription", "agent_subscription.UserId = users_idx_token.agent_id", "left")
                ->where("agent_subscription.EventType", "purchased")
                ->where("agent_subscription.ApplicationType", "Agent_IDX_Website")
                ->where("users_idx_token.is_valid", 1)
                ->where("users_idx_token.counter", 0);
        return $this->db_slave->get()->result();
    }

    public function get_lost_token(){
        $this->db_slave->select("users_idx_token.agent_id,users_idx_token.access_token,users_idx_token.date_created,users_idx_token.date_modified,
            users_idx_token.expected_token_expired,users_idx_token.is_valid,domains.domains,domains.status,domains.subdomain_url")
                ->from("users_idx_token")
                ->join("agent_subscription", "agent_subscription.UserId = users_idx_token.agent_id", "left")
                ->join("users", "users.agent_id = users_idx_token.agent_id", "left")
                ->join("domains", "domains.agent = users.id", "left")
                ->where("agent_subscription.EventType", "purchased")
                ->where("agent_subscription.ApplicationType", "Agent_IDX_Website")
                ->where("users_idx_token.is_valid", 0)
                ->group_by("users_idx_token.agent_id")
                ->order_by("users.id");
        return $this->db_slave->get()->result();
    }

    public function get_sold_listings_option(){

        $this->db_slave->select("show_sold_listings")
            ->from("users")
            ->where("agent_id", $this->domain_user->agent_id);
        
        return $this->db_slave->get()->row();
    }

    public function get_static_custom_fields($agent_id = NULL){
        $this->db_slave->select("global_search_filters_string,gsf_standard")
            ->from("settings")
            ->where("agent_id", $agent_id)
            ->limit(1);
        
        $query = $this->db_slave->get()->row();
        if(!empty($query)){
            if(isset($query->global_search_filters_string) && isset($query->gsf_standard)){
                return array('gsf_custom_fields' => $query->global_search_filters_string, 'gsf_standard_fields' => $query->gsf_standard);
            }
            return false;
        }
        return false;
    }

    public function get_gsf_fields($agent_id = NULL){
        $this->db_slave->select("global_search_filters_string,gsf_standard")
            ->from("settings")
            ->where("agent_id", $agent_id)
            ->limit(1);
        
        $query = $this->db_slave->get()->row();
        if(!empty($query)){
            if(isset($query->global_search_filters_string) && isset($query->gsf_standard)){
                return array('gsf_custom_fields' => $query->global_search_filters_string, 'gsf_standard_fields' => $query->gsf_standard);
            }
            return false;
        }
        return false;
    }

    public function get_featured_saved_search(){

          $this->db_slave->select("id, user_id,saved_search_id as Id,saved_search_name as Name,saved_search_filter as Filter") 
            ->from("saved_search_options")
            ->where("user_id", $this->domain_user->id)
            ->where("saved_search_type", "featured");
        
        return $this->db_slave->get()->row_array();
    }

    public function get_selected_saved_search(){

          $this->db_slave->select("id, user_id,saved_search_id as Id,saved_search_name as Name,saved_search_filter as Filter")
            ->from("saved_search_options")
            ->where("user_id", $this->domain_user->id)
            ->where("saved_search_type", "selected");
        
        return $this->db_slave->get()->result_array();
    }

    public function getSoldListingsData($counter = FALSE,  $param = array()) {

        $this->db_slave->select("listing_id")
            ->from("sold_listings_options")
            ->where("user_id", $this->domain_user->id)
            ->limit(1);

        $query = $this->db_slave->get()->row();

        $json_listing_id                = "json_extract(sld.listings, '$.Id') AS ListingId";
        $json_city                      = "json_extract(sld.listings, '$.StandardFields.City') AS City";
        $json_close_date                = "json_extract(sld.listings, '$.StandardFields.CloseDate') AS CloseDate";
        $json_close_price               = "json_extract(sld.listings, '$.StandardFields.ClosePrice') AS ClosePrice";
        $json_postal_code               = "json_extract(sld.listings, '$.StandardFields.PostalCode') AS PostalCode";
        $json_postal_city               = "json_extract(sld.listings, '$.StandardFields.PostalCity') AS PostalCity";
        $json_state_or_province         = "json_extract(sld.listings, '$.StandardFields.StateOrProvince') AS StateOrProvince";
        $json_listing_key               = "json_extract(sld.listings, '$.StandardFields.ListingKey') AS ListingKey";
        $json_photo_uri                 = "json_extract(sld.listings, '$.StandardFields.Photos[0].Uri300') AS PhotosURi";
        $json_unparse_address           = "json_extract(sld.listings, '$.StandardFields.UnparsedFirstLineAddress') AS UnparseAddress";

         if($query){
            $this->db_slave->distinct();
            $this->db_slave->select("$json_listing_id,
                                 $json_city, 
                                 $json_close_date,
                                 $json_close_price,
                                 $json_postal_code,
                                 $json_postal_city, 
                                 $json_state_or_province,
                                 $json_listing_key,
                                 $json_photo_uri,
                                 $json_unparse_address,
                                 sld.user_id,
                                 sld.listing_id")
            ->from("sold_listings_data sld")
            ->join("sold_listings_options slo", "sld.listing_id=slo.listing_id", "inner")
            ->where("sld.user_id", $this->domain_user->id)
            ->group_by("sld.listing_id");

        } else {

            $this->db_slave->distinct();
            $this->db_slave->select("$json_listing_id,
                                 $json_city, 
                                 $json_close_date,
                                 $json_close_price,
                                 $json_postal_code,
                                 $json_postal_city, 
                                 $json_state_or_province,
                                 $json_listing_key,
                                 $json_photo_uri,
                                 $json_unparse_address,
                                 date_sold")
            ->from("sold_listings_data sld")
            ->where("user_id", $this->domain_user->id);

        }

        if($counter) {
            $this->db_slave->reset_query();
            $this->db_slave->distinct('listing_id');
            $this->db_slave->select('listing_id');
            $this->db_slave->from("sold_listings_data sld");
            $this->db_slave->where("user_id", $this->domain_user->id);

            return $this->db_slave->count_all_results();
        }

        if(!empty($param["limit"])) { 
            $this->db_slave->limit($param["limit"]);
        } 

        if(!empty($param["offset"])) {
            $this->db_slave->offset($param["offset"]);
        } 

        if(!empty($query)) {
            $this->db_slave->order_by("sld.date_sold", "desc");
        } else {
            $this->db_slave->order_by("date_sold", "desc");
        }

        $query = $this->db_slave->get();

        if($query->num_rows()>0) {
            return $query->result_array();
        }

        return FALSE;
    }

    public function getTotalSoldListingsData($type = NULL) {

        $this->db_slave->select("listings")
                ->from("sold_listings_data")
                ->where("user_id", $this->domain_user->id)
                ->where("type", $type);

        return $this->db_slave->count_all_results();
  
    }

    public function get_account_latlon($agent_id="") {

        $this->db_slave->select("latitude,longitude")
                ->from("users")
                ->where("agent_id", $agent_id)
                ->limit(1);

        return $this->db_slave->get()->row();
    }

    public function get_sold_listings_selected() {

        $json_unparse_address  = "json_extract(listings, '$.StandardFields.UnparsedFirstLineAddress') AS UnparseAddress";
        $this->db_slave->distinct();
        $this->db_slave->select("$json_unparse_address,sld.user_id,sld.listing_id")
            ->from("sold_listings_data sld")
            ->join("sold_listings_options slo", "sld.listing_id=slo.listing_id", "inner")
            ->where("sld.user_id", $this->session->userdata('user_id'))
            ->group_by("sld.listing_id")
            ->order_by("sld.date_sold","desc");

        $query = $this->db_slave->get();

        if($query->num_rows()>0) {
            return $query->result_array();
        }

        return FALSE;
    }
   
}   
