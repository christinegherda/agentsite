<?php

defined('BASEPATH') OR exit('No direct script access allowed');

global $user;
global $domain_user;

$user = $user_info;

$this->domain_user = domain_to_user();

hook('before_head_end_tag', function(){
  global $user;

  if($user->plan_id == 2 || (empty($user->plan_id) && !$user->trial)){
    $settings = $this->home_model->get_seo_settings($this->domain_user->agent_id);

    if(isset($settings->facebook_pixel) && $settings->facebook_pixel !== ''){
      echo '<!-- FACEBOOK PIXEL -->'."\n\r";
      print_r($settings->facebook_pixel);
      echo "\n\r".'<!-- ./ FACEBOOK PIXEL -->'."\n\r";
    }

    if(isset($settings->gtm_head) && $settings->gtm_head !== ''){
      echo '<!-- AS GTM HEAD -->'."\n\r";
      print_r($settings->gtm_head);
      echo "\n\r".'<!-- ./ AS GTM HEAD -->'."\n\r";
    }
  }

  enqueue_listtrac('script', $this->uri->segment(1));
  echo '<style type="text/css">.featured-title-container h2.text-center.section-title{width:580px;margin:0 auto}@media only screen and (max-width: 552px) and (min-width: 240px){.featured-title-container h2.text-center.section-title{width:100%;margin:0}}</style>';
});

hook('after_body_start_tag', function(){
  global $user;

  if($user->plan_id == 2 || (empty($user->plan_id) && !$user->trial)){
    $settings = $this->home_model->get_seo_settings($this->domain_user->agent_id);

    if(isset($settings->gtm_body) && $settings->gtm_body !== ''){
      echo '<!-- AS GTM BODY -->'."\n\r";
      print_r($settings->gtm_body);
      echo "\n\r".'<!-- ./ AS GTM BODY -->'."\n\r";
    }
  }
});

hook('before_body_end', function() {
  global $user;
  
  if($user->plan_id == 2 || (empty($user->plan_id) && !$user->trial)){
    $settings = $this->home_model->get_seo_settings($this->domain_user->agent_id);

    if(isset($settings->remarketing_tag) && $settings->remarketing_tag !== ''){
      echo '<!-- GOOGLE REMARKETING TAG -->'."\n\r";
      print_r($settings->remarketing_tag);
      echo "\n\r".'<!-- ./ GOOGLE REMARKETING TAG -->'."\n\r";
    }
  }

});

hook('custom-menu', function(){
  $html = '';
  $custom_menu = Modules::run('menu/get_custom_menu_data_raw');
  
  if(!empty($custom_menu)){
    $items = !empty($custom_menu['items'])? $custom_menu['items'] : array() ;
    $parents = !empty($custom_menu['parents'])? $custom_menu['parents'] : array() ;
    if(!empty($parents[0])){
      $total = count($parents[0]);
      $j = 0;
      for($i = 0; $i < $total; $i++){
        $item = $items[$parents[0][$i]];
        if(isset($item)){
          if(($i % 8) === 0){
            if($i === 0){
              $html .= "<nav class=\"custom-menu custom-menu-{$j}\">";
              $html .= '<ul class="menu">';
              $html .= "<li><a href=\"\" target=\"\">{$item['title']}</a></li>";
            }else if($i === ($total - 1)){
              $html .= "<li><a href=\"\" target=\"\">{$item['title']}</a></li>";
              $html .= '</ul>';
              $html .= '</nav>';
            }else{
              $html .= '</ul>';
              $html .= '</nav>';
              $html .= "<nav class=\"custom-menu custom-menu-{$j}\">";
              $html .= '<ul class="menu">';
              $html .= "<li><a href=\"\" target=\"\">{$item['title']}</a></li>";
            }
            $j++;
          }else if($i === ($total - 1)){
            $html .= "<li><a href=\"\" target=\"\">{$item['title']}</a></li>";
            $html .= '</ul>';
            $html .= '</nav>';
          }else{
            $html .= "<li><a href=\"\" target=\"\">{$item['title']}</a></li>";
          }
        }
      }
    }
  }
  echo $html;
});

filter('before-home-view', function($data){

  $dsl_featured_saved_search = Modules::run('dsl/getFeaturedAgentSavedSearch');
  $dsl_selected_saved_search = Modules::run('dsl/getSelectedAgentSavedSearch');
  $mysql_selected_saved_search = $this->home_model->get_selected_saved_search();
  $mysql_featured_saved_search = $this->home_model->get_featured_saved_search();

  //get new listings proxy for the new features to work properly
  $dsl = modules::load('dsl/dsl');
  $token = $this->home_model->getUserAccessToken($this->domain_user->agent_id);
  $access_token = array(
	'access_token' => $token->access_token
  );

  //check if dsl featured savedsearch status is failed
  if(isset($dsl_featured_saved_search['status']) &&  $dsl_featured_saved_search['status'] == "failed"){
    $is_saved_search_featured = $mysql_featured_saved_search;
  }elseif(!empty($dsl_featured_saved_search)){
    $is_saved_search_featured = $dsl_featured_saved_search[0] ;
  }else{
    $is_saved_search_featured = $mysql_featured_saved_search;
  }

   //check if dsl selected savedsearch status is failed
  if(isset($dsl_selected_saved_search['status']) &&  $dsl_selected_saved_search['status'] == "failed"){
    $is_saved_search_selected = $mysql_selected_saved_search;
  }elseif(!empty($dsl_selected_saved_search)){
    $is_saved_search_selected = $dsl_selected_saved_search ;
  }else{
    $is_saved_search_selected = $mysql_selected_saved_search;
  }

  $is_custom_home = $this->home_model->is_custom_home();
  $gsf_fields = $this->home_model->get_gsf_fields($this->domain_user->agent_id);
  $filterGeo = '';
	
	//set home view
	if($is_custom_home != "" || isset($is_saved_search_selected[0]) || isset($is_saved_search_featured) ){
	 	$data["page"] = "home_custom";
	} 
	else {
		$data["page"] = "home";
	}
  
  $featured_saved_search = $this->home_model->get_featured_saved_search_data($this->domain_user->id);
  if(!empty($featured_saved_search)) {
    $featured_saved_search_data = gzdecode($featured_saved_search->listings);
    $featured_saved_search_decoded = json_decode($featured_saved_search_data,true);
  }

  $data['socialmedia_link'] = Modules::run('agent_social_media/agent_social_media/linksInfo');
	$obj = Modules::load("mysql_properties/mysql_properties");
 
    if(!empty($data["token_checker"])){

      $filter_string = createFilterString($gsf_fields, "new_listings");
      $endpoint = "spark-listings?_limit=4&_expand=PrimaryPhoto&_filter=(".$filter_string.")&_orderby=-OnMarketDate";
      $results = $dsl->post_endpoint($endpoint, $access_token);
      $new_listings['results'] = json_decode($results);
      $data['new_listings'] = isset($new_listings['results']->data) ? $new_listings['results']->data : ""; 

    } else{

      $data['new_listings'] = $obj->get_properties($this->domain_user->id, "new");
    }

    if( $this->domain_user->agent_id == '20170814193510838719000000' ){
      $data['new_listings'] = $obj->get_properties($this->domain_user->id, "new");
    }

	//Active Listings Section
  $data['active_listings'] = $obj->get_properties($this->domain_user->id, "active");
  $data["itemCount"] = !empty($data['active_listings'])? count($data['active_listings']) :"";
    if($data["itemCount"] > 4) {
        $data["itemCount"] = 4;
    }
    $data["iCount"] = 0;
    $data["totalListingCount"] = 0;
    if(isset($data['active_listings']) && !empty($data['active_listings'])){
      foreach($data['active_listings'] as $feature) {
        if(isset($feature->StandardFields->ListingKey)){
            if(!empty($feature->StandardFields->ListingKey)){
                if($data["iCount"] < $data["itemCount"]) {
                    $data["totalListingCount"]++;
                }
                $data["iCount"]++;
            }
        }
      }
    }
  $home = Modules::load("home/home");
  $param['limit'] = 8;
  $data['sold_listings'] = "ajax_call"; #$home->getSoldListingsData(FALSE,$param);
	$data['office_listings'] = $obj->get_office($this->domain_user->id, "office");
	$data['nearby_listings'] = $obj->get_properties($this->domain_user->id, 'nearby');

	$data['account_info'] = $obj->get_account($this->domain_user->id, "account_info");
	$data['system_info'] = $obj->get_account($this->domain_user->id, "system_info");
	$data["active_title"] = $this->home_model->get_active_title();
	$data["new_title"] = $this->home_model->get_new_title();
	$data["nearby_title"] = $this->home_model->get_nearby_title();
	$data["office_title"] = $this->home_model->get_office_title();

	$data["is_active_selected"] = $this->home_model->get_active_selected();
	$data["is_nearby_selected"] = $this->home_model->get_nearby_selected();	
	$data["is_new_selected"] = $this->home_model->get_new_selected();
	$data["is_office_selected"] = $this->home_model->get_office_selected();
	$data["is_saved_search_selected"] = $is_saved_search_selected;
	
	$data["is_active_featured"] = $this->home_model->get_active_featured();
	$data["is_nearby_featured"] = $this->home_model->get_nearby_featured();
	$data["is_new_featured"] = $this->home_model->get_new_featured();
	$data["is_office_featured"] = $this->home_model->get_office_featured();
	$data["is_saved_search_featured"] = $is_saved_search_featured;
  
	//Selected and Featured Saved Search Section
  $data["saved_searches_selected"] = $is_saved_search_selected;
  $data["saved_searches_featured"] = isset($featured_saved_search_decoded) ? $featured_saved_search_decoded : "";
  
  $data['custom_menu_data'] = Modules::run('menu/get_custom_menu_data');
  
  $data['slider_photos'] = $this->home_model->getSliderPhotos();
  if(isset($data['slider_photos']) && !empty($data['slider_photos'])){

    $data['image'] = !empty($data['slider_photos'][0]->photo_url) ? getenv('AWS_S3_ASSETS')."uploads/slider/".$data['slider_photos'][0]->photo_url."" : "";

  } elseif(isset($data['active_listings']) && !empty($data['active_listings'])){

      $data['image'] = !empty($data['active_listings'][0]->StandardFields->Photos[0]->Uri1280) ? $data['active_listings'][0]->StandardFields->Photos[0]->Uri1280 : "";
  } elseif(isset($data['office_listings'])  && !empty($data['office_listings'])){

    $data['image'] = !empty($data['office_listings'][0]['Photos']['Uri1280']) ? $data['office_listings'][0]['Photos']['Uri1280'] : "";

  } elseif(isset($data['new_listings'])  && !empty($data['new_listings'])) {

    $data['image'] = !empty($data['new_listings'][0]->StandardFields->Photos[0]->Uri1280) ? $data['new_listings'][0]->StandardFields->Photos[0]->Uri1280 : "";
  }
  $data['agent_city'] = ($data['user_info']->city != 'Not Specified')? ucwords(str_replace('  ', '', $data['user_info']->city)).' ' : '';
  $data['system_info'] = $obj->get_account($this->domain_user->id, "system_info");
  
  $data["js"] = array("listings.js");


  return $data;
});

filter('before-buyer-view', function($data){
  
  $data['custom_menu_data'] = Modules::run('menu/get_custom_menu_data');
  
  return $data;
});

filter('before-mapped-search-view', function($data){
  
  $data["header"] = "header-page";
  $data["page"] = "advance_search_view/mapped-search";

  if ($data['browser'] != "Internet Explorer") {
    $data["js"] = array("selectize.js", "advanced_search.js", "jquery.swipebox.min.js");
  }
  $data["css"] = array("selectize.bootstrap3.css", "swipebox.min.css");
  
  return $data;
});

filter('before-sitemap-view', function($data){

  $obj = Modules::load("mysql_properties/mysql_properties");
  $data['system_info'] = $obj->get_account($this->domain_user->id, "system_info");

  $limit = 4;
  $selectFields = "Id,UnparsedAddress,UnparsedFirstLineAddress,PostalCode,ListingKey";

  $activelistings = Modules::run('dsl/getSparkProxyComplete', 'my/listings', "_limit=".$limit."&_filter=MlsStatus%20Eq%20'Active'&_select=".$selectFields."");

  if(!isset($activelistings->SparkQLErrors) && !empty($activelistings->Results)){
      $data['active_listings'] = $activelistings->Results;
  }

  $soldlistings = Modules::run('dsl/getSparkProxyComplete', 'my/listings', "_limit=".$limit."&_filter=MlsStatus%20Eq%20'Closed'&_select=".$selectFields."");
  if(!isset($soldlistings->SparkQLErrors) && !empty($soldlistings->Results)){
      $data['sold_listings'] = $soldlistings->Results;
  }
  $officelistings = Modules::run('dsl/getSparkProxyComplete', 'office/listings', "_limit=".$limit."&_filter=MlsStatus%20Eq%20'Active'&_select=".$selectFields."&_orderby=-OnMarketDate");
  if(!isset($officelistings->SparkQLErrors) && !empty($officelistings->Results)){
      $data['office_listings'] = $officelistings->Results;
  }

 if(!empty($data["token_checker"])){
    //get new listings proxy for the new features to work properly
    $dsl = modules::load('dsl/dsl');
    $token = $this->home_model->getUserAccessToken($this->domain_user->agent_id);
    $access_token = array(
      'access_token' => $token->access_token
    );

    //subtract 1 day from today's date
    $lastday = gmdate("Y-m-d\TH:i:s\Z",strtotime("-1 days"));
    $today =  gmdate("Y-m-d\TH:i:s\Z");
    //printA($lastday);exit;

    $filterStr = "(MlsStatus Ne 'Closed' Or MlsStatus Ne 'Sold') And (OnMarketDate Bt ".$lastday.",".$today.")";
    $filter_url = rawurlencode($filterStr);
    $filter_string = substr($filter_url, 3, -3);

    $endpoint = "spark-listings?_limit=".$limit."&_select=".$selectFields."&_filter=(".$filter_string.")";
    $results = $dsl->post_endpoint($endpoint, $access_token);
    $new_listings['results'] = json_decode($results);
    $data['new_listings'] = $new_listings['results']->data;

  } else{

    $data['new_listings'] = $obj->get_properties($this->domain_user->id, "new");
  }

  return $data;
});


/**
 * Filter function use in contact page to show map
 * base on the location coordinates or by the physical address
 * @var array $data
 * @return array
 */
$filter = function (array $data) {

	if( isset($data['user_info']->address) && !empty($data['user_info']->address) )
	{
		$data['address'] = $data['user_info']->address;
	}
	elseif ($data['account_info']->Mls === 'MLS BCS')
	{
		$data['address'] = $data['account_info']->Addresses[0]->City.', '.$data['account_info']->Addresses[0]->Region.' '.$data['account_info']->Addresses[0]->PostalCode;
	}
	elseif ( $data['account_info']->Mls === 'Vallarta-Nayarit MLS' )
	{
		$data['address'] = 'Col. Emiliano Zapata, Puerto Vallarta, JA 48380';
	}
	else
	{
		$data['address'] = isset($data['account_info']->Addresses[0]->Address) ? $data['account_info']->Addresses[0]->Address : '';
	}

	$data['location'] = [
		'latitude' => isset($data['user_info']->latitude) ? $data['user_info']->latitude : '',
		'longitude' => isset($data['user_info']->longitude) ? $data['user_info']->longitude : '',
	];

	return $data;
};

filter('before-contact-view', $filter);


if ( ! function_exists('createFilterString') )
{
	function createFilterString($gsf_fields = [], $filterType = null )
	{
		$filterGeo = '';

    if($filterType === "new_listings"){

      $lastday = gmdate("Y-m-d\TH:i:s\Z",strtotime("-1 days"));
      $today =  gmdate("Y-m-d\TH:i:s\Z");
      $filterStr = "(MlsStatus Ne 'Closed' Or MlsStatus Ne 'Sold') And (OnMarketDate Bt ".$lastday.",".$today.")";

    } else {
      $filterStr = "(MlsStatus Ne 'Closed','Sold','Leased','Pending','Rented')";
    }

		if ( isset($gsf_fields['gsf_standard_fields']) && !empty($gsf_fields['gsf_standard_fields']) )
		{
			$gsf_standard_fields = json_decode($gsf_fields['gsf_standard_fields']);

			if ( isset($gsf_standard_fields->PropertyClass) )
			{
				$filterStr .= ' And ( ';
				$len = count($gsf_standard_fields->PropertyClass);

				for ($i=0; $i < $len; $i++)
				{
					if ($i === 0)
					{
						$filterStr .= "PropertyClass Eq '*{$gsf_standard_fields->PropertyClass[$i]}*'";
					}
					else
					{
						$filterStr .= " Or PropertyClass Eq '*{$gsf_standard_fields->PropertyClass[$i]}*'";
					}
				}

				$filterStr .= ' )';
			}

			if( isset($gsf_standard_fields->City) )
			{
				$len = count($gsf_standard_fields->City);
				for ($i=0; $i < $len; $i++)
				{
					if ($filterGeo !== '')
					{
						$filterGeo.= " Or City Eq '".str_replace('_', ' ', $gsf_standard_fields->City[$i])."'";
					}
					else
					{
						$filterGeo.= " City Eq '".str_replace('_', ' ', $gsf_standard_fields->City[$i])."' ";
					}
				}
			}

			if ( isset($gsf_standard_fields->CountyOrParish) )
			{
				$len = count($gsf_standard_fields->CountyOrParish);
				for ($i=0; $i < $len; $i++)
				{
					if ($filterGeo !== '')
					{
						$filterGeo.= " Or CountyOrParish Eq '".str_replace('_', ' ', $gsf_standard_fields->CountyOrParish[$i])."'";
					}
					else
					{
						$filterGeo.= " CountyOrParish Eq '".str_replace('_', ' ', $gsf_standard_fields->CountyOrParish[$i])."' ";
					}
				}
			}

			if ( isset($gsf_standard_fields->PostalCode) )
			{
				$len = count($gsf_standard_fields->PostalCode);
				for ($i=0; $i < $len; $i++)
				{
					if ($filterGeo !== '')
					{
						$filterGeo.= " Or PostalCode Eq '{$gsf_standard_fields->PostalCode[$i]}'";
					}
					else
					{
						$filterGeo.= " PostalCode Eq '{$gsf_standard_fields->PostalCode[$i]}' ";
					}
				}
			}
		}

		if ( $filterGeo !== '' )
		{
			$filterStr .= " AND ({$filterGeo})";
		}

		if ( isset($gsf_fields['gsf_custom_fields']) && !empty($gsf_fields['gsf_custom_fields']))
		{
			$filterStr .= " And {$gsf_fields['gsf_custom_fields']}";
		}

		$filter_url = rawurlencode($filterStr);
		return substr($filter_url, 3, -3);
	}
}
