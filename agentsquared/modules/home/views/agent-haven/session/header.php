<!DOCTYPE html>
<html class="no-js" lang="">
<head itemscope itemtype="http://schema.org/Website">
    <meta charset="utf-8">
    <meta name="google-site-verification" content="CT_yXIl7JUphPwPV2rysqOFpuJprWjoIzya3DRDeyfY" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <?php // initialize SEO meta data for recycling
      $title = isset($title) ? $title : 'Home | ' . $user_info->first_name . ' ' . $user_info->last_name . ' | ' .$user_info->phone;
      $description = isset($description) ? $description  : $user_info->banner_tagline;
      $keywords = isset($keywords) ? $keywords: '';
      $url = (isset($canonical) && $canonical !== '') ? $canonical : current_url();
    ?>
    <?php 
        if(strpos( base_url(), 'agentsquared.com') !== false){
            echo '<meta name="robots" content="noindex,nofollow,noarchive,nodp"/>';
        }
    ?>

    <title itemprop="name"><?php echo $title;?></title>
    <meta itemprop="description" name ="description" content="<?php echo $description;?>"/>
    <meta itemprop="publisher" name="author" content="<?php echo $user_info->first_name . ' ' . $user_info->last_name;?>" />
    <meta name="keywords" content="<?php echo $keywords;?>" />
    <link rel="canonical" href="<?php echo $url;?>" />

    <!-- OG metas -->
    <meta property="og:title" content="<?= isset($title) ? $title : '' ?>" />
	<meta property="og:description" content="<?= isset($description) ? $description : '' ?>" />
    <meta property="og:url" content="<?= $url ?>" />
    <meta property="og:type" content="website" />
    <meta property="og:image" content="<?= isset($image) ? $image : '/header-photo/' ?>" />
    <meta property="og:image:width" content="1200" />
    <meta property="og:image:height" content="630" />
	<meta property="fb:app_id" content="<?php echo config_item('facebook_app_id'); ?>">

     <?php if(!empty($user_info->favicon)){

        $favicon = getenv('AWS_S3_ASSETS') . "uploads/favicon/".$user_info->favicon;
     } else {
        $favicon = "/assets/images/default-favicon.png";
     }?>

    <link rel="icon" type="image/x-icon"  href="<?php echo $favicon ?>"/>

    <!-- Stylesheets -->
    <!-- <link rel="stylesheet" type="text/css" href="/assets/css/agent-haven/home-compressed.css.php" /> -->
    <link rel="stylesheet" type="text/css" href="/assets/css/agent-haven/font.css?version=<?=AS_VERSION?>"/>
    <link rel="stylesheet" type="text/css" href="/assets/font-awesome/css/font-awesome.min.css?version=<?=AS_VERSION?>" />
    <link rel="stylesheet" type="text/css" href="/assets/bootstrap/css/bootstrap.min.css?version=<?=AS_VERSION?>" />
    <link rel="stylesheet" type="text/css" href="/assets/bootstrap/css/bootstrap-theme.min.css?version=<?=AS_VERSION?>" />
    <link rel="stylesheet" type="text/css" href="/assets/css/agent-haven/home-layout.css?version=<?=AS_VERSION?>" />
    <link rel="stylesheet" type="text/css" href="/assets/css/agent-haven/media.css?version=<?=AS_VERSION?>" />
    <link rel="stylesheet" type="text/css" href="/assets/fonts/fontello/css/fontello.css?version=<?=AS_VERSION?>" />
    <link rel="stylesheet" type="text/css" href="/assets/css/agent-haven/home-vegas.css?version=<?=AS_VERSION?>" />
    <link rel="stylesheet" type="text/css" href="/assets/css/agent-haven/selectize.bootstrap3.css?version=<?=AS_VERSION?>" />
    <link rel="stylesheet" type="text/css" href="/assets/css/agent-haven/owl.carousel.css?version=<?=AS_VERSION?>" />
    <script src="/assets/js/agent-haven/vendor/modernizr-2.8.3-respond-1.4.2.min.js?version=<?=AS_VERSION?>"></script>
    <script type="text/javascript">
        var base_url = "<?php echo base_url(); ?>";
        var searchIsSaved=false;
        var firstLaunch=false;
    </script>

    <!-- reCaptcha -->
    <script src='https://www.google.com/recaptcha/api.js'></script>
    
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=<?=getenv('GOOGLE_MAP_APIKEY'); ?>&libraries="></script>
    <script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
    <script type="text/javascript">
        function googleTranslateElementInit() {
            new google.translate.TranslateElement({pageLanguage: 'en'}, 'google_translate_element');
        }
    </script>
    <?php  get_ga(); ?>
    
    <script type="application/ld+json">
      {
        "@context": "http://schema.org",
        "@type": "WebSite",
        "url": "<?php echo base_url();?>",
        "name": "<?php echo $title;?>",
        "description": "<?php echo $description; ?>",
        "image": "<?php echo isset($image) ? $image : ""?>",
        "publisher": {
          "@type": "Thing",
          "name": "<?php echo $user_info->first_name . ' ' . $user_info->last_name;?>"
        },
        "about": {
          "@type": "Thing",
          "name": "<?php echo $description; ?>"
        }
      }
    </script>
    <?php add_hook('before_head_end_tag'); ?>
</head>
<body>
    <?php add_hook('after_body_start_tag'); ?>
    <!-- Modal Pricing -->
    <div class="modal fade" id="modalPricing" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <!-- <h4 class="modal-title" id="myModalLabel">IDX Site Free Trial Expired</h4> -->
                </div>
                <div class="modal-body">
                    <div class="row">
                        <h4 class="text-center">
                            <div class="col-md-12 col-sm-12">
                                <div class="price-item">
                                    <h4 class="price-title">IDX Website Free Trial Expired</h4>
                                    <div class="col-md-12">
                                        <div class="price-desc">
                                            Thanks for signing up for AgentSquared. We hope you have been enjoying your free trial.<br><br>
                                            Unfortunately, your 15 days free trial already expired.
                                            <br>
                                            <br>
                                            We'd love to keep you, and there is still time to complete and launch your own Agent site to the public. Simply visit your <a href="<?php echo AGENT_DASHBOARD_URL ?>login/auth/login">account dashboard</a> to subscribe.
                                            <br>
                                            <br>
                                            If you have any questions or feedback, just contact us through our AgentSquared <a href='https://www.agentsquared.com/support/'>support</a>.
                                        </div>
                                    </div>
                                    <!-- <a href="javascript:void(0)" data-type="monthly" data-price="<?php echo $this->config->item('price'); ?>" id="purchase-idx-monthly" class="price-text purchase-idx">Purchase</a> -->
                                </div>
                            </div>
                        </h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End of pricing modal -->

    <!-- Start Upgrade to Plan to IDX Modal -->
    <div id="agent-idx-website-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> -->
                    <div class="col-md-offset-5">
                        <div class="main-header-container">
                            <h3 class="main-header">Sell More Homes</h3>
                            <h4 class="main-header">Build Your Online Presence Today!</h4>  
                        </div>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="col-md-offset-5">
                        <div class="main-body">
                            <h4>Websites integrate with your MLS through a direct connection in FlexMLS. Buyers can search your website in your local market with real-time, instant updates.</h4>
                            <ul class="idx-modal-list">
                                <li>Capture Leads</li>
                                <li>Add Unlimited Pages</li>
                                <li>Add Saved Searches to any page</li>
                            </ul>
                            <div class="pricing-section clearfix">
                                <div class="pricing-header">
                                    <h4 class="text-center">PRICING PLANS</h4>
                                </div>
                                <div class="col-sm-offset-3 col-md-offset-3 col-md-6 col-sm-6 no-padding-left">
                                    <div class="large-monthly pricing">
                                        <h1>
                                        <span class="dollar">$</span>
                                            99
                                        <span class="permonth">/month</span>
                                        </h1>
                                        <p>Billed Monthly<!--  - <span class="discount">Save 20%</span> --></p>
                                        <div class="bottom-trial">
                                            <p>15 days FREE trial<!-- . $59 monthly until canceled. --></p>  
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <a href="http://www.sparkplatform.com/appstore/apps/instant-idx-website-powered-by-flexmls" class="btn btn-default btn-block" target="_blank">Click Here to Start Your FREE 15 Days Trial</a>  
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>
    </div>
    <!-- End of Upgrate to Plan to IDX Modal -->
    <div id="google_translate_element"></div>
    <nav class="navbar navbar-fixed-top main-navbar home-navbar" role="navigation">
        <div class="container">
            <div class="col-md-12">
                <p class="text-right registration-nav">
                <?php

                    if (isset($account_info->OfficeId) && $account_info->OfficeId == "20160426214504162264000000") {
                        $brokername = $account_info->Company;

                    } elseif (isset($account_info->Office) && !empty($account_info->Office)) {
                        $brokername = $account_info->Office;
                        
                    } else {
                        $brokername = "";
                    }
                    
                    if($this->session->userdata("customer_id")) { ?>
                        <span class="userlog">Welcome <?php echo ($this->session->userdata("customer_email")) ? $this->session->userdata("customer_email") : "User" ;?>!</span> 
                        <span class="nav-link">
                            <a href="javascript:void(0)" style="cursor: text;"><strong><?=$brokername;?></strong></a> | <a href="<?php echo site_url("customer-dashboard"); ?>">Dashboard</a> | <a href="#" class="customer_logout_btn" > Logout</a>
                        </span>
                <?php
                    } elseif(agent_customer_cookie()) { ?>
                        <span class="userlog">Welcome <?php echo ($this->session->userdata("customer_email")) ? $this->session->userdata("customer_email") : "User" ;?>!</span> 
                        <span class="nav-link">
                            <a href="javascript:void(0)" style="cursor: text;"><strong><?=$brokername;?></strong></a> | <a href="<?php echo site_url("customer-dashboard"); ?>">Dashboard</a> | <a href="#" class="customer_logout_btn" > Logout</a>
                        </span>
                <?php
                    } else { ?>
                        <a href="javascript:void(0)" style="cursor: text;"><strong><?=$brokername;?></strong></a> | 
                        <a href="#" type="button" class="modalogin" data-toggle="modal" data-target=".customer_login_signup">Login</a>  <span>|</span> 
                        <a href="#" type="button" class="modalsignup" data-toggle="modal" data-target=".customer_login_signup">SignUp</a> 
                <?php 
                    }
                ?>

                </p>
            </div>
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/">
                    <!-- Logo Area -->
                   <?php if(!empty($user_info->logo)){?>

                        <img src="<?php echo getenv('AWS_S3_ASSETS') . "uploads/logo/" . $user_info->logo?>" alt="Logo" width="200px"  height="65px">

                    <?php } else {

                        $default_mls_logo = array(
                           "20070913202326493241000000",//ARMLS
                           "20150128160104800461000000",//CPAR
                           "20130226165731231246000000",//SPACECOAST
                           "20180216183507684268000000",//GEPAR
                        );

                        if(isset($system_info->MlsId) && !empty($system_info->MlsId)){
                
                            if (in_array($system_info->MlsId, $default_mls_logo)) {?>
                                <img src="<?php echo getenv('AWS_S3_ASSETS') . "logo/" . $system_info->MlsId."-logo.png"?>" alt="Logo" width="200px"  height="65px"> 
                            <?php } else {?>
                                <img src="<?php echo getenv('AWS_S3_ASSETS') ?>logo/agentsquared-logo.png" alt="Logo" width="200px"  height="65px">
                           <?php }
                   } else {?>
                            <img src="<?php echo getenv('AWS_S3_ASSETS') ?>logo/agentsquared-logo.png" alt="Logo" width="200px"  height="65px">
                       <?php }
                    }?>
                </a>

                <?php 
                if (!empty($user_info->tag_line)) {
                    $tagline = htmlentities($user_info->tag_line);
                } else {

                    if ($account_info->MlsId == "20130226165731231246000000") {
                        $tagline = "The Voice For Real Estate in Brevard";
                    } else {
                        $tagline = "Your home away from home";
                    }
                    
                }
                ?>
                <p class="tagline"><?=$tagline;?></p>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <!-- Default menu Area -->  
                <?php if(isset($default_menu_data) && !empty($default_menu_data)){
                     echo $default_menu_data;
                }?>
            </div>
        </div>
    </nav>
    <?php
        $ci = ci();
         if(isIDXOwner() || $ci->input->get('modal_welcome')) { ?>
            <div class="backdrop" style="display:none;"></div>
            <div class="text-center pollSlider">
                <div class="note-container">
                    <div id="pollSlider-button">
                        <span class="fa fa-close"></span>
                        <p class="text-collapse">Agent Dashboard</p>
                    </div>
                    <h4><span><?=$account_info->FirstName; ?></span>, welcome to your new Instant IDX&trade; Website</h4>
                    <!-- <p>To start customizing your website, please login to your agent dashboard. </p> -->
                    <?php
                        if($ci->input->get('modal_welcome')) { ?>
                            <script type="text/javascript">firstLaunch=true;</script>
                            <a href="<?=AGENT_DASHBOARD_URL.'login/auth/agent_site_login?user_id='.$this->session->userdata('user_id').'&agent_id='.$this->session->userdata('agent_id').'&fresh=TRUE';?>" target="_blank" class="close-note">
                    <?php 
                        } else { ?>
                            <a href="<?=AGENT_DASHBOARD_URL.'login/auth/agent_site_login?user_id='.$this->session->userdata('user_id').'&agent_id='.$this->session->userdata('agent_id');?>" target="_blank" class="close-note">
                    <?php
                        }
                    ?>
                        Login to Agent Dashboard</a>
                        <h4>to customize your site.</h4>
                </div>
                <!-- <a href="#" class="close-note">Login to Agent Dashboard</a> -->
            </div>
        <?php
         }
    ?>

    <!-- Custom menu Area -->
    <?php if(isset($custom_menu_data) && !empty($custom_menu_data)){?>
        <div id="custom-navbar" class="sub-navbar-menu hide-menu">
            <div class="container">
                <div class="subnavbar-menu-btn">
                    <a href="#">More Pages <i class="fa fa-plus"></i></a>   
                </div>

                 <?php 
                    echo $custom_menu_data;
                    //add_hook('custom-menu');
                 ?>
                 
            </div>
        </div>
    <?php  }?>
