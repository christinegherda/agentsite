<!DOCTYPE html>
<html class="no-js" lang="">
<head itemscope itemtype="http://schema.org/Website">
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <?php 
      if(strpos( base_url(), 'agentsquared.com') !== false){
          echo '<meta name="robots" content="noindex,nofollow,noarchive,nodp"/>';
      }

    $url = current_url();

		$i = 1;
		$listtrac = false;
    	$agent_name = $user_info->first_name .' '.$user_info->last_name;
		$default = ' | '.$agent_name;
		$agent_photo_flex = isset($account_info->Images) ? $account_info->Images[0]->Uri : '';
		$agent_photo_upload = isset($user_info->agent_photo) ? getenv('AWS_S3_ASSETS')."uploads/photo/".$user_info->agent_photo : '';
		$agent_photo = !empty($agent_photo_upload) ? $agent_photo_upload : $agent_photo_flex;

		if($this->uri->segment(1) == 'home'){
			$i = 2;
		}

		$segment = $this->uri->segment($i);
		if($segment != "page" &&
			$segment != "listings" &&
			$segment != "property-details" &&
			$segment != "other-property-details"){

			$default_title = isset($page_data['title'])? $page_data['title']: '';
			$title = isset($title) ? $title : $default_title.$default;
			$description = isset($description) ? $description : $user_info->banner_tagline;
			$url = base_url().$segment;
			$image = isset($photos[0]['Uri1280']) ? $photos[0]['Uri1280'] : "";
		}

		if($segment == "page"){
			$title = isset($title)  ? $title : $page_data['title'].$default;
			$description = isset($description) ? $description : $user_info->banner_tagline;
			$url = base_url().'page/'.$page_data['slug'];
			$image = !empty($page_data['seo_image']) ? $page_data['seo_image'] : $agent_photo;
		}

		if($segment == "listings"){
			$search = (isset($_GET['Search']) && !empty($_GET['Search']))? ucwords($_GET['Search']) : 'Search Listings';
			$title = $search.$default;
			$description = $user_info->banner_tagline;

			$url = base_url().$segment;
			$image = '';
		}
		if($segment === "property-details" || $segment === "other-property-details"){
			if(!empty($property_standard_info["data"])){
				$details = $property_standard_info["data"];

				if(isset($details['ModificationTimestamp']) && $details['ModificationTimestamp'] != ''){
					$datetime = strtotime($details['ModificationTimestamp']);
					header('Last-Modified: ' . date('Y-m-d h:i:s A T', $datetime));
				}

				$address_full = isset($details["UnparsedAddress"]) ? $details["UnparsedAddress"] : "";
        		$title = $address_full.$default;

        		$listing_id = isset($details["ListingKey"]) ? $details["ListingKey"] : '';
        		$mls_id = isset($details["ListingId"]) ? $details["ListingId"] : '';
				$zipcode = isset($details["PostalCode"]) ? $details["PostalCode"] : '';

        		$description = 'Contact '.$agent_name.' to learn more about the property at '.$address_full.'. MLS #:'.$mls_id.'. Talk to '.$agent_name.' now!';
            $image = isset($photos) ? $photos : "";

				if($segment === "property-details"){
					$url = base_url().'property-details/'.$listing_id;
				}

				if($segment === "other-property-details") {
					$url = base_url().'other-property-details/'.$listing_id;
				}
				$listtrac = true;
			}
		}

	  if ( isset($canonical) && $canonical !== '')
	  {
		  $url = $canonical;
	  }
	?>
	
	<title itemprop="name"><?php echo isset($title) ? $title : ''; ?></title>
	<meta itemprop="description" name ="description" content="<?php echo isset($description)? $description:'';?>" />
	<meta itemprop="author" name="author" content="<?php echo $user_info->first_name . ' ' . $user_info->last_name;?>" />
	<link rel="canonical" href="<?= isset($url) ? $url : '' ?>" />

  <?php if($this->uri->segment(1) == 'search-results'){ ?>
    <meta name="robots" content="noindex,nofollow,noarchive,nodp" />
  <?php } ?>
	

	<!-- OG metas -->
	<meta property="og:title" content="<?= isset($title) ? $title : '' ?>" />
	<meta property="og:description" content="<?= isset($description) ? $description : '' ?>" />
	<meta property="og:url" content="<?= isset($url) ? $url : '' ?>" />
	<meta property="og:type" content="website" />
	<meta property="og:image" content="<?= isset($image) ? $image : '' ?>" />
	<meta property="og:image:width" content="1200" />
    <meta property="og:image:height" content="630" />
	<meta property="fb:app_id" content="<?php echo config_item('facebook_app_id'); ?>">


	 <!-- Twitter Card data -->
	<meta name="twitter:title" content="<?php echo isset($title)? $title:''; ?>" />
	<meta name="twitter:description" content="<?php echo isset($description)? $description:'';?>" />
	<meta name="twitter:url" content="<?php echo isset($url)? $url:'';?>" />
	<meta name="twitter:card" value="summary">
	<meta name="twitter:image" content="<?php echo isset($image)? $image:''; ?>" />
	
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
	
	<?php if(!empty($user_info->favicon)){

        $favicon = getenv('AWS_S3_ASSETS') . "uploads/favicon/".$user_info->favicon;

     } else {

        $favicon = "/assets/images/default-favicon.png";
     }?>

    <link rel="icon" type="image/x-icon"  href="<?php echo $favicon ?>"/>

	<!-- Stylesheet -->
	<link rel="stylesheet" type="text/css" href="/assets/css/agent-haven/font.css?version=<?=AS_VERSION?>" />
	<link rel="stylesheet" type="text/css" href="/assets/css/agent-haven/bootstrap-switch.min.css?version=<?=AS_VERSION?>" />
	<link rel="stylesheet" type="text/css" href="/assets/font-awesome/css/font-awesome.min.css?version=<?=AS_VERSION?>" />
	<link rel="stylesheet" type="text/css" href="/assets/bootstrap/css/bootstrap.min.css?version=<?=AS_VERSION?>" />
	<link rel="stylesheet" type="text/css" href="/assets/bootstrap/css/bootstrap-theme.min.css?version=<?=AS_VERSION?>" />
	<link rel="stylesheet" type="text/css" href="/assets/js/plugins/datepicker/css/datepicker.css?version=<?=AS_VERSION?>" />
	<link rel="stylesheet" type="text/css" href="/assets/css/agent-haven/home-layout.css?version=<?=AS_VERSION?>" />
	<link rel="stylesheet" type="text/css" href="/assets/css/agent-haven/media.css?version=<?=AS_VERSION?>" />
	<link rel="stylesheet" type="text/css" href="/assets/fonts/fontello/css/fontello.css?version=<?=AS_VERSION?>" />
	<link rel="stylesheet" type="text/css" href="/assets/css/agent-haven/home-vegas.css?version=<?=AS_VERSION?>" />
	<link rel="stylesheet" type="text/css" href="/assets/css/agent-haven/selectize.bootstrap3.css?version=<?=AS_VERSION?>" />
	<link rel="stylesheet" type="text/css" href="/assets/css/agent-haven/owl.carousel.css?version=<?=AS_VERSION?>" />
    <link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">

	<script src="/assets/js/agent-haven/vendor/modernizr-2.8.3-respond-1.4.2.min.js?version=<?=AS_VERSION?>"></script>
	<?php 
		if(isset($css)) {
			foreach($css as $key => $val ) {
	?>
	
	<link rel="stylesheet" href="/assets/css/agent-haven/<?php echo $val?>?version=<?=AS_VERSION?>" type="text/css" />
	<?php }} ?>
	<script type="text/javascript">var base_url = "<?php echo base_url(); ?>"; </script>

	<?php
		if($page == "advance_search") { ?>
			<!--<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyCJjsX64AbQb2_cDXl0cZ2geKwCWg2f9u0&libraries=geometry, places&callback=initShowMap" async defer></script>-->
			<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=<?=getenv('GOOGLE_MAP_APIKEY'); ?>&libraries=geometry"></script>	
	<?php
		} elseif($page == "contact" || $page == "property") { ?> 
			<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=<?=getenv('GOOGLE_MAP_APIKEY_FREE'); ?>&libraries=places"></script>	
	<?php
		} else { ?>
			<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=<?=getenv('GOOGLE_MAP_APIKEY'); ?>&libraries=drawing"></script>
	<?php
		} ?>
	<?php add_hook('before_head_end_tag');?>
	
  	<script type="text/javascript">
	  	var searchIsSaved=false;
	  	var firstLaunch=false;
  	</script>

    <!-- reCaptcha -->
    <script src='https://www.google.com/recaptcha/api.js'></script>

    <!-- Google Translate -->
    <script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
    <script type="text/javascript">
        function googleTranslateElementInit() {
            new google.translate.TranslateElement({pageLanguage: 'en'}, 'google_translate_element');
        }
    </script>
    <?php  get_ga(); ?>
</head>
<body>
	<?php
    add_hook('after_body_start_tag');
    enqueue_listtrac('pageview', $this->uri->segment(1), array(
      'listing_id' => isset($mls_id)? $mls_id : null,
      'zipcode' => isset($zipcode)? $zipcode : null,
    ));
  ?>
	
	<!-- Modal Pricing -->
	<div class="modal fade" id="modalPricing" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
						<!-- <h4 class="modal-title" id="myModalLabel">IDX Site Free Trial Expired</h4> -->
				</div>
				<div class="modal-body">
					<div class="row">
						<h4 class="text-center">
							<div class="col-md-12 col-sm-12">
								<div class="price-item">
									<h4 class="price-title">IDX Website Free Trial Expired</h4>
									<div class="col-md-12">
										<div class="price-desc">
												Thanks for signing up for AgentSquared. We hope you have been enjoying your free trial.<br><br>
												Unfortunately, your 15 days free trial already expired.
												<br>
												<br>
												We'd love to keep you, and there is still time to complete and launch your own Agent site to the public. Simply visit your <a href="<?php echo AGENT_DASHBOARD_URL ?>login/auth/login">account dashboard</a> to subscribe.
												<br>
												<br>
												If you have any questions or feedback, just contact us through our AgentSquared <a href='https://www.agentsquared.com/support/'>support</a>.
										</div>
									</div>
									<!-- <a href="javascript:void(0)" data-type="monthly" data-price="<?php echo $this->config->item('price'); ?>" id="purchase-idx-monthly" class="price-text purchase-idx">Purchase</a> -->
								</div>
							</div>
						</h4>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- End of pricing modal -->
	
	<!-- Start Upgrade to Plan to IDX Modal -->
    <div id="agent-idx-website-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> -->
                    <div class="col-md-offset-5">
                        <div class="main-header-container">
                            <h3 class="main-header">Sell More Homes</h3>
                            <h4 class="main-header">Build Your Online Presence Today!</h4>  
                        </div>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="col-md-offset-5">
                        <div class="main-body">
                            <h4>Websites integrate with your MLS through a direct connection in FlexMLS. Buyers can search your website in your local market with real-time, instant updates.</h4>
                            <ul class="idx-modal-list">
                                <li>Capture Leads</li>
                                <li>Add Unlimited Pages</li>
                                <li>Add Saved Searches to any page</li>
                            </ul>
                            <div class="pricing-section clearfix">
                                <div class="pricing-header">
                                    <h4 class="text-center">PRICING PLANS</h4>
                                </div>
                                <div class="col-sm-offset-3 col-md-offset-3 col-md-6 col-sm-6 no-padding-left">
                                    <div class="large-monthly pricing">
                                        <h1>
                                        <span class="dollar">$</span>
                                            99
                                        <span class="permonth">/month</span>
                                        </h1>
                                        <p>Billed Monthly<!--  - <span class="discount">Save 20%</span> --></p>
                                        <div class="bottom-trial">
                                            <p>15 days FREE trial<!-- . $59 monthly until canceled. --></p>  
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <a href="http://www.sparkplatform.com/appstore/apps/instant-idx-website-powered-by-flexmls" class="btn btn-default btn-block" target="_blank">Click Here to Start Your FREE 15 Days Trial</a>  
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>
    </div>
    <!-- End of Upgrate to Plan to IDX Modal -->
	<nav class="navbar navbar-fixed-top main-navbar page-navbar" role="navigation">
		<div class="container">
			<div id="google_translate_element"></div>
			<div class="col-md-12 header-top">
				<p class="text-right registration-nav">
					<?php
	                    if (isset($account_info->Office) && !empty($account_info->Office)) {
	                        $brokername = $account_info->Office;
	                    } else {
	                        $brokername = "";
	                    }

						if($this->session->userdata("customer_id")) {
					?>
					<span class="userlog" customer-id="<?php echo $this->session->userdata("customer_id"); ?>">Welcome <?php echo ($this->session->userdata("customer_email")) ? $this->session->userdata("customer_email") : "User" ;?>!</span> 
					<span class="nav-link">
						<?php if ($this->uri->segment(1) == "customer-dashboard"): ?>
							<?php if(isIDXOwner() || $this->input->get('modal_welcome')) { ?>
								<span class="agent-link-wrapper">
								<?php
	                                if($this->input->get('modal_welcome')) { ?>
	                                    <a href="<?=AGENT_DASHBOARD_URL.'login/auth/agent_site_login?user_id='.$this->session->userdata('user_id').'&agent_id='.$this->session->userdata('agent_id').'&fresh=TRUE';?>" target="_blank" class="close-note btn-agent-login">
	                            <?php 
	                                } else { ?>
	                                    <a href="<?=AGENT_DASHBOARD_URL.'login/auth/agent_site_login?user_id='.$this->session->userdata('user_id').'&agent_id='.$this->session->userdata('agent_id');?>" target="_blank" class="close-note btn-agent-login">
	                            <?php
	                                }
	                            ?>
	                                Login to Agent Dashboard</a>
								</span>
							<?php } ?>
						<?php endif ?>
						<span class="agent-broker">
							<a href="javascript:void(0)" style="cursor: text;"><strong><?=$brokername;?></strong></a>
						</span>
						|  <a href="<?php echo site_url("customer-dashboard"); ?>">Dashboard</a> | <a href="#" class="customer_logout_btn" > Logout</a>
					</span>
					<?php
						} elseif(agent_customer_cookie()) { ?>
							<span class="userlog">Welcome <?php echo ($this->session->userdata("customer_email")) ? $this->session->userdata("customer_email") : "User" ;?>!</span> 
							<span class="nav-link">
								<?php if ($this->uri->segment(1) == "customer-dashboard"): ?>
									<?php if(isIDXOwner()) { ?>
										<span class="agent-link-wrapper">
										<?php
			                                if($this->input->get('modal_welcome')) { ?>
			                                    <a href="<?=AGENT_DASHBOARD_URL.'login/auth/agent_site_login?user_id='.$this->session->userdata('user_id').'&agent_id='.$this->session->userdata('agent_id').'&fresh=TRUE';?>" target="_blank" class="close-note btn-agent-login">
			                            <?php 
			                                } else { ?>
			                                    <a href="<?=AGENT_DASHBOARD_URL.'login/auth/agent_site_login?user_id='.$this->session->userdata('user_id').'&agent_id='.$this->session->userdata('agent_id');?>" target="_blank" class="close-note btn-agent-login">
			                            <?php
			                                }
			                            ?>
			                                Login to Agent Dashboard</a>
										</span>
									<?php } ?>
								<?php endif ?>
								<span class="agent-broker">
									<a href="javascript:void(0)" style="cursor: text;"><strong><?=$brokername;?></strong></a>
								</span>
								|  <a href="<?php echo site_url("customer-dashboard"); ?>">Dashboard</a> | <a href="#" class="customer_logout_btn" > Logout</a>
							</span>
					<?php
						} else { ?>
							<a href="javascript:void(0)" style="cursor: text;"><strong><?=$brokername;?></strong></a> |  
							<a href="#" type="button" class="modalogin" data-toggle="modal" data-target=".customer_login_signup">Login</a> 
							<span>|</span> 
							<a href="#" type="button" class="modalsignup" data-toggle="modal" data-target=".customer_login_signup">SignUp</a>
					<?php
						}
					?>
				</p>
			</div>
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="/">
					
					 <!-- Logo Area -->
             <?php if(!empty($user_info->logo)){?>

                  <img src="<?php echo getenv('AWS_S3_ASSETS') . "uploads/logo/" . $user_info->logo?>" alt="Logo" width="200px"  height="65px">

              <?php } else {

                  $default_mls_logo = array(
                     "20070913202326493241000000",//ARMLS
                     "20150128160104800461000000",//CPAR
                     "20130226165731231246000000",//SPACECOAST
                     "20180216183507684268000000",//GEPAR
                  );

                  if(isset($system_info->MlsId) && !empty($system_info->MlsId)){
          
                      if (in_array($system_info->MlsId, $default_mls_logo)) {?>
                          <img src="<?php echo getenv('AWS_S3_ASSETS') . "logo/" . $system_info->MlsId."-logo.png"?>" alt="Logo" width="200px"  height="65px"> 
                      <?php } else {?>
                          <img src="<?php echo getenv('AWS_S3_ASSETS') ?>logo/agentsquared-logo.png" alt="Logo" width="200px"  height="65px">
                     <?php }
             } else {?>
                      <img src="<?php echo getenv('AWS_S3_ASSETS') ?>logo/agentsquared-logo.png" alt="Logo" width="200px"  height="65px">
                 <?php }
             }?>
				</a>
                <?php 
                if (!empty($user_info->tag_line)) {
                    $tagline = htmlentities($user_info->tag_line);
                } else {

                    if ($account_info->MlsId == "20130226165731231246000000") {
                        $tagline = "The Voice For Real Estate in Brevard";
                    } else {
                        $tagline = "Your home away from home";
                    }
                    
                }
                ?>
                <p class="tagline"><?=$tagline;?></p>
			</div>
			<div id="navbar" class="navbar-collapse collapse">
				<!-- Default menu Area -->  
				<?php if(isset($default_menu_data) && !empty($default_menu_data)){
					echo $default_menu_data;
				}?>
			</div>
		</div>
	</nav>
    <?php
         if(isIDXOwner() || $this->input->get('modal_welcome')) { ?>
            <div class="backdrop" style="display:none;"></div>
            <div class="text-center pollSlider">
                <div class="note-container">
                    <div id="pollSlider-button">
                        <span class="fa fa-close"></span>
                        <p class="text-collapse">Agent Dashboard</p>
                    </div>
                    <h4><span><?=$account_info->FirstName; ?></span>, welcome to your new Instant IDX&trade; Website</h4>
                    <!-- <p>To start customizing your website, please login to your agent dashboard. </p> -->
                    <?php
                        if($this->input->get('modal_welcome')) { ?>
                        	<script type="text/javascript">firstLaunch=true;</script>
                            <a href="<?=AGENT_DASHBOARD_URL.'login/auth/agent_site_login?user_id='.$this->session->userdata('user_id').'&agent_id='.$this->session->userdata('agent_id').'&fresh=TRUE';?>" target="_blank" class="close-note">
                    <?php 
                        } else { ?>
                            <a href="<?=AGENT_DASHBOARD_URL.'login/auth/agent_site_login?user_id='.$this->session->userdata('user_id').'&agent_id='.$this->session->userdata('agent_id');?>" target="_blank" class="close-note">
                    <?php
                        }
                    ?>
                        Login to Agent Dashboard</a>
                        <h4>to customize your site.</h4>
                </div>
                <!-- <a href="#" class="close-note">Login to Agent Dashboard</a> -->
            </div>
        <?php
         }
    ?>

	<!-- Custom menu Area -->
	<?php if(isset($custom_menu_data) && !empty($custom_menu_data)){?>
		<div id="custom-navbar" class="sub-navbar-menu hide-menu">
			<div class="container">
				<div class="subnavbar-menu-btn">
						<a href="#">More Pages <i class="fa fa-plus"></i></a>   
				</div>

				 <?php 
						echo $custom_menu_data;
				 ?>
				 
			</div>
		</div>
	<?php  }?>
