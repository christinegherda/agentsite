     <?php

        if(isset($socialmedia_link['facebook']) AND !empty($socialmedia_link['facebook'])) {
            $fblink = $socialmedia_link['facebook'];
        } else {
            $fblink = "https://www.facebook.com/AgentSquared/";
        }

        if(isset($socialmedia_link['twitter']) AND !empty($socialmedia_link['twitter'])) {
            $twittlink = $socialmedia_link['twitter'];
        } else {
            $twittlink = "https://twitter.com/agentsquared";
        }

         if(isset($socialmedia_link['instagram']) && !empty($socialmedia_link['instagram'])) {
             $instagramlink = $socialmedia_link['instagram'];
         } else {
             $instagramlink = "https://www.instagram.com/agentsquared";
         }

        if(isset($socialmedia_link['linkedin']) AND !empty($socialmedia_link['linkedin'])) {
            $lnlink = $socialmedia_link['linkedin'];
        } else {
            $lnlink = "https://www.linkedin.com/company/agentsquared";
        }

        if(isset($system_info->OfficeId) && $system_info->OfficeId == '20160426214504162264000000' ) {
            $office = $system_info->Company;
        } else {
            $office = $system_info->Office ? $system_info->Office : "";
        }

    ?>
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-7">
                    <div class="row">
                        <div class="col-md-5">
                            <div class="social-icons">
                                 <ul class="list-inline">
                                    <?php if($socialmedia_link['agent_email']) : ?><li><a href="mailto:<?php echo $socialmedia_link['agent_email']; ?>"><i class="fa fa-envelope"></i></a></li><?php endif; ?>
                                    <li><a href="<?php echo $fblink; ?>" target="_blank"><i class="fa fa-facebook-square"></i></i></a></li>
                                    <li><a href="<?php echo $twittlink; ?>" target="_blank"><i class="fa fa-twitter-square"></i></a></li>
                                    <li><a href="<?php echo $instagramlink; ?>" target="_blank" title="Instagram"><i class="fa fa-instagram"></i></a></li>
                                    <li><a href="<?php echo $lnlink; ?>" target="_blank"><i class="fa fa-linkedin-square"></i></a></li>
                                </ul>
                                <h4><small><?php echo $office; ?></small></h4>
                                <h4 class="mls-footer"><?php echo isset($system_info->Mls) ? $system_info->Mls : ""; ?></h4>
                                <a href="" data-toggle="modal" data-target="#Disclaimer"><h4 class="text-left">Disclaimer</h4></a>
                            </div>
                        </div>
                        <div class="col-md-7">
                            <?php if ($account_info->MlsId == "20180216183507684268000000" ): ?>
                                <div class="idx-disclosure-link">
                                    <a href="/tx-real-estate-commission-consumer-protection-notice">Texas Real Estate Commission Consumer Protection Notice</a><br>
                                    <a href="/texas-real-estate-commission-information-about-brokerage-services">Texas Real Estate Commission Information About Brokerage Services</a> 
                                </div>
                                
                            <?php endif ?>
                        </div>
                    </div>
                    <!-- add custom footer to http://rhondabutler.agentsquared.com/-SR-115 -->
                    <?php if ($this->session->userdata('user_id') === 12253): ?>
                        <div class="row">
                            <div class="col-md-12">
                                <p class="custom-footer">
                                    &copy; <?php echo date("Y"); ?> BHH Affiliates, LLC. An independently owned and operated franchisee of BHH Affiliates, LLC.
                                    Berkshire Hathaway HomeServices and the Berkshire Hathaway HomeServices symbol are registered service
                                    marks of HomeServices of America, Inc.® Equal Housing Opportunity.
                                </p>
                            </div>
                        </div>   
                    <?php endif ?>
                </div>
                <div class="col-md-5">
                    <div class="footer-terms">
                        <ul class="list-inline footer-modal-links">
                           <li><a href="<?php echo '/about/sitemap';?>">Sitemap</a></li>
                           <li><a href="" data-toggle="modal" data-target="#modalTerms">Terms of Use</a></li>
                           <?php // <li><a href="" data-toggle="modal" data-target="#modalFairHousing">Fair Housing Statement</a></li> ?>
                           <li><a href="/privacy-policy" >Privacy Policy</a></li>
                           <li class="hud-footer">
                               <div class="hud-logo">
                                   <a href="" data-toggle="modal" data-target="#modalFairHousing">
                                       <img src="/assets/images/equal-housing-logo-white.png" alt="">
                                   </a>
                               </div>
                           </li>
                        </ul>
                        <h4 class="footer-copyright">&copy; <?php echo date("Y"); ?> All Rights Reserved.</h4>
                        <p>
                            <a href="http://www.agentsquared.com/" target="_blank" class="brand-name">
                                <img src="/assets/images/dashboard/demo/agent-haven/logo.png" alt="Agentsquared Signup" class="footer-logo">
                            </a>
                        </p>
                    </div>
                </div>
                <a class="scrolltop">
                    <i class="fa fa-angle-up"></i>
                </a>

                <div class="modal fade legal-modal" id="Disclaimer" tabindex="-1" role="dialog">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content theme-modal">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">X</span></button>
                                <h3 class="text-center">Disclaimer Text</h3>
                            </div>
                            <div class="modal-body clearfix">
                                <p>
                                    <?php echo isset($system_info->Configuration[0]->IdxDisclaimerTextOnly) ? $system_info->Configuration[0]->IdxDisclaimerTextOnly : ""; ?>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- terms of use -->
                <div class="modal fade legal-modal" id="modalTerms" tabindex="-1" role="dialog">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content theme-modal">
                            <div class="modal-header">
                                <button type="button" class="close pull-right" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">X</span></button>
                                <h2 class="text-center">Terms of Use</h2>
                            </div>
                            <div class="modal-body clearfix">
                                <p>
                                 PLEASE READ THESE TERMS AND CONDITIONS OF USE ("SITE TERMS") CAREFULLY. YOUR USE OF THE SITE IS CONDITIONED UPON YOUR ACCEPTANCE OF THESE SITE TERMS WITHOUT MODIFICATION.
                                 BY ACCESSING OR USING THIS WEB SITE, YOU AGREE TO BE BOUND BY THESE SITE TERMS AND ALL TERMS INCORPORATED BY REFERENCE.
                                 IF YOU DO NOT AGREE TO THESE SITE TERMS, DO NOT USE THIS WEB SITE.
                                <p>
                                 The Web site you are accessing (the "Site") is hosted by AgentSquared.com on behalf of one of its real estate industry customers (the "Company").
                                 AgentSquared and Company are independent contractors and are not partners or otherwise engaged in any joint venture in delivering the Site.
                                 Notwithstanding that fact, these Site Terms apply to both Agent Squared and Company and as such, any reference to "we", "us" or "our" herein will apply
                                 equally to both Agent Squared and Company.
                                 These Site Terms apply exclusively to your access to, and use of, the Site so please read them carefully.
                                 These Site Terms do not alter in any way the terms or conditions of any other agreement you may have with Agent Squared, Company, or
                                 their respective subsidiaries or affiliates, for services, products or otherwise. We reserve the right to change or modify any of the Site
                                 Terms and the Site, at any time. If we decide to change our Site Terms, we will post a new version on the Site and update the date.
                                 Any changes or modifications will be effective immediately upon posting of the revisions on the Site, and you waive any right you may have to receive specific notice of such changes or modifications.
                                 Your use of the Site following the posting of changes or modifications to the Site Terms will constitute your acceptance of the revised Site Terms.
                                 Therefore, you should frequently review the Site Terms and applicable policies from time-to-time to understand the terms and conditions that apply to your use of the Site.
                                 If you do not agree to the amended terms, you must immediately stop using the Site.
                                </p>

                                <h4>Privacy</h4>
                                <p>
                                 We believe strongly in providing you notice of how we collect and use your data, including personally identifying information,
                                 collected from the Sites. We have adopted a Privacy Statement to which you should refer to fully understand how we collect and use
                                 personally identifying information.
                                </p>

                                <h4>Consent to Communications</h4>
                                <p>
                                 By filling out any forms on the Site or making any inquiries through the Site you acknowledge that we have an established business relationship and you expressly consent to being contacted us,
                                 whether by phone, mobile phone, email, mail or otherwise.
                                </p>

                                <h4>Property Listing Data</h4>
                                <p>
                                 Any real estate listing data provided to you in connection with the Site is not intended to be a representation of the complete Multiple Listing Service data for any of our MLS sources.
                                 We are not liable for and do not guarantee the accuracy of any listing data or other data or information found on the Site, and all such information should be independently verified. The
                                 information provided in connection with the Site is for the personal, non-commercial use of consumers and may not be
                                 used for any purpose other than to identify prospective properties consumers may be interested in purchasing.
                                 Some properties which appear for sale on this website may no longer be available because they are under contract, have sold or are no longer being offered for sale.
                                </p>

                                <h4>Use of Site; Limited License</h4>
                                <p>
                                 You are granted a limited, non-sublicensable license to access and use the Site and all content, data, information and materials included in the Site (the "Site Materials")
                                 solely for your personal use, subject to the terms and conditions set forth in these Site Terms. You may not use the Site or any Site Materials for commercial purposes. You agree that you will
                                 not modify, copy, distribute, resell, transmit, display, perform, reproduce, publish, license, create derivative works from, frame in another Web page, use on any other web site or service any of the Site Materials.
                                 You will not use the Site or any of the Site Materials other than for its intended purpose or in any way that is unlawful, or harms Agent Squared, Company and/or their suppliers.
                                </p>
                                <p>
                                 Without prejudice to the foregoing, you may not engage in the practices of "screen scraping," "database scraping," "data mining" or any other activity with the purpose of obtaining lists of users or other information
                                 from the Site or that uses web "bots" or similar data gathering or extraction methods. You may not obtain or attempt to obtain any materials or information through any means not
                                 intentionally made available or provided for through the Site.
                                </p>
                                <p>
                                 Any use of the Site or the Site Materials other than as specifically authorized herein is strictly prohibited and will terminate the license granted herein. Such unauthorized use may also violate applicable laws including without limitation copyright and
                                 trademark laws and applicable communications regulations and statutes. Unless explicitly stated herein, nothing in these Site Terms shall be construed as conferring any license to
                                 intellectual property rights, whether by estoppel, implication or otherwise. This license is revocable by us at any time.
                                </p>

                                <h4>Responsibility for Your Conduct and User Content</h4>
                                <p>
                                 You are solely liable for your conduct and for any information, text, photos, content, materials or messages that you upload, post or transmit to the Site (collectively the "User Content").
                                 You may not provide false or misleading information to the Site or submit information under false pretenses.
                                </p>
                                <p>
                                 Without limiting anything else in these Site Terms, we may immediately terminate your access to and use of any of the Sites if you violate any of the foregoing or
                                 if you provide false or misleading information or submit information under false pretenses.
                                </p>

                                <h4>Account Security</h4>
                                <p>
                                 Each person that is provided with a password and user ID to use the Site must agree to abide by these Site Terms and is responsible for all activity under such user ID.
                                 You are responsible for maintaining the confidentiality and security of any password connected with your account.
                                </p>

                                <h4>Submissions</h4>
                                <p>
                                 You agree that any materials, including but not limited to questions, comments, suggestions, ideas, plans, notes, drawings, original or creative materials or other information regarding Company or the Site, provided by you in the form of e-mail or
                                 submissions to us, or postings on the Sites, are non-confidential (subject to our Privacy Policy). We will own exclusive rights, including all intellectual property
                                 rights, and will be entitled to the unrestricted use of these materials for any purpose, commercial or otherwise, without acknowledgment or compensation to you.
                                </p>

                                <h4>Hyperlinks</h4>
                                <p>
                                 We make no claim or representation regarding, and accept no responsibility for, the quality, content, nature or reliability of third-party Web sites accessible by hyperlink from the Site, or Web sites linking to the Site.
                                 Such sites are not under our control and we are not responsible for the content of any linked site or any link contained in a linked site, or any review, changes or updates to such sites. We provide these links to you only as a convenience, and the inclusion of any
                                 link does not imply affiliation, endorsement or adoption by us of any site or any information contained therein. When you leave the Site, you should be aware that our terms and policies no longer govern.
                                 You should review the applicable terms and policies, including privacy and data gathering practices, of any site to which you navigate from the Site.
                                </p>

                                <h4>Third Party Products and Services; Third-Party Content</h4>
                                <p>
                                 We may run advertisements and promotions from third parties on the Site or may otherwise provide information about or links or referrals to third-party products or services on the Site. Your business dealings or correspondence with, or participation in promotions of,
                                 such third parties, and any terms, conditions, warranties or representations associated with such dealings or promotions are solely between you and such third party. We are not responsible or liable for any loss or damage of any sort incurred as the result of any
                                 such dealings or promotions or from any third party products or services, and you use such third party products and services at your own risk. We do not sponsor, endorse, recommend or approve any such third party who advertises their goods or services through the Site.
                                 You should investigate and use your independent judgment regarding the merits, quality and reputation of any individual, entity or information that you find on or through the Site. We do not represent or warrant that any third party who places advertisements on the Site is
                                 licensed, qualified, reputable or capable of performing any such service.
                                </p>
                                <p>
                                 We do not make any assurance as to the timeliness or accuracy of any third-party content or information provided on or linked to from the Site ("Third Party Content"). We do not monitor or have any control over any Third Party Content, and do not endorse or adopt any
                                 Third Party Content and can make no guarantee as to its accuracy or completeness. We will not update or review any Third Party Content, and if you choose to use such Third Party Content you do so at your own risk.
                                </p>

                                <h4>Disclaimer</h4>
                                <p>
                                 THE SITE AND THE SITE MATERIALS (INCLUDING ALL THIRD PARTY CONTENT), AND ALL LINKS, INFORMATION, MATERIALS, EVALUATIONS, RECOMMENDATIONS, SERVICES
                                 AND PRODUCTS PROVIDED ON OR THROUGH THE SITES ARE PROVIDED ON AN "AS IS" BASIS WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED. YOU EXPRESSLY
                                 AGREE THAT USE OF THE SITE AND THE MATERIALS IS AT YOUR SOLE RISK. COMPANY AND AGENT SQUARED AND THEIR RESPECTIVE SUPPLIERS DISCLAIM ALL WARRANTIES,
                                 EXPRESS OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, TITLE, AND NON-INFRINGEMENT
                                 AS TO THE SITE, THE SITE MATERIALS, LINKS, INFORMATION, MATERIALS, SERVICES AND PRODUCTS AVAILABLE ON OR THROUGH ON THE SITE.
                                 NEITHER COMPANY NOR AGENT SQUARED REPRESENTS OR WARRANTS THAT THE MATERIALS ARE ACCURATE, COMPLETE, RELIABLE, CURRENT, OR ERROR-FREE; OR IS
                                 RESPONSIBLE FOR TYPOGRAPHICAL ERRORS OR OMISSIONS RELATING TO PRICING, TEXT, OR PHOTOGRAPHY. WHILE WE ATTEMPT TO ENSURE YOUR ACCESS AND USE OF THE
                                 SITES IS SAFE, NEITHER COMPANY NOR AGENT SQUARED REPRESENTS OR WARRANTS THAT THE SITES OR ITS SERVER(S) ARE FREE OF VIRUSES OR OTHER HARMFUL COMPONENTS.
                                 AGENT SQUARED IS MERELY A SERVICE PROVIDER TO COMPANY, AND IN NO EVENT SHALL AGENT SQUARED BE LIABLE TO USER FOR ANY OF THE PRODUCTS, SERVICES, CONTENT
                                 OR INFORMATION PROVIDED THROUGH THE SITE OR OTHERWISE PROVIDED BY OR ON BEHALF OF COMPANY, AND AGENT SQUARED MAKES NO REPRESENTATION OR WARRANTY WITH
                                 RESPECT THERETO.
                                </p>

                                <h4>Limitation of Liability</h4>
                                <p>
                                 IN NO EVENT WILL COMPANY OR AGENT SQUARED OR ANY OF THEIR RESPECTIVE SUPPLIERS BE LIABLE FOR ANY DIRECT, SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES,
                                 OR ANY OTHER DAMAGES OF ANY KIND, INCLUDING BUT NOT LIMITED TO LOSS OF USE, LOSS OF PROFITS, OR LOSS OF DATA, WHETHER IN AN ACTION IN CONTRACT, TORT
                                 (INCLUDING BUT NOT LIMITED TO NEGLIGENCE), OR OTHERWISE, ARISING OUT OF OR IN ANY WAY CONNECTED WITH THE USE OF THE SITE OR THE SITE MATERIALS.
                                </p>

                                <h4>Termination; Refusal to Provide Services.</h4>
                                <p>
                                 We may terminate or suspend your access to the Site at any time, with or without cause, and with or without notice. Upon such termination or suspension,
                                 your right to use the Site will immediately cease. Furthermore, we reserve the right not to respond to any requests for information for any reason,
                                 or no reason.
                                </p>

                                <h4>Applicable Law and Venue</h4>
                                <p>
                                 These terms and conditions are governed by and construed in accordance with the laws of the State of California, applicable to agreements made and
                                 entirely to be performed within the State of California, without resort to its conflict of law provisions. You agree that any action at law or in equity
                                 arising out of or relating to these terms and conditions can be filed only in state or federal court located in San Diego, California, and you hereby
                                 irrevocably and unconditionally consent and submit to the exclusive jurisdiction of such courts over any suit, action or proceeding arising out of these
                                 terms and conditions.
                                </p>

                                <h4>General Terms</h4>
                                <p>
                                 If any part of the Site Terms is determined to be invalid or unenforceable pursuant to applicable law, then the invalid or unenforceable provision will
                                 be deemed superseded by a valid, enforceable provision that most closely matches the intent of the original provision and the remainder of the Site Terms
                                 will continue in effect. We may assign this Agreement, in whole or in part, at any time with or without notice to you. You may not assign this Agreement,
                                 or assign, transfer or sublicense your rights, if any, in the Sites. Except as expressly stated herein, the Site Terms constitute the entire agreement
                                 between you and us with respect to the Sites.
                                </p>

                                <h4>Copyright and Trademark</h4>
                                <p>
                                 Unless otherwise indicated in the Site, the Site Materials and the selection and arrangement thereof are the proprietary property of Agent Squared,
                                 Company or their suppliers and are protected by U.S. and international copyright laws. The Company name, and any Company products and services slogans
                                 or logos referenced herein are either trademarks or registered trademarks of Company in the United States and/or other countries. The names of actual
                                 third party companies and products mentioned in the Site may be the trademarks of their respective owners. Any rights not expressly granted herein are
                                 reserved.
                                </p>
                                <p>
                                 The parties have required that the Site Terms and all documents relating thereto be drawn up in English.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- fair housing modal -->
                <div class="modal fade legal-modal" id="modalFairHousing" tabindex="-1" role="dialog">
                   <div class="modal-dialog modal-lg">
                      <div class="modal-content theme-modal">
                         <div class="modal-header">
                           <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">X</span></button>
                            <h2 class="text-center">Fair Housing Statement</h2>
                         </div>
                         <div class="modal-body clearfix">
                            <p>
                               Federal and state fair housing laws were put into effect to create an even playing field for homebuyers in all areas of a real estate transaction.
                               These laws prohibit discrimination based on race, color, religion, sex, disability, familial status, and national origin.
                            </p>

                            <h4>Civil Rights Act of 1866</h4>
                            <p>
                               The Civil Rights Act of 1866 prohibits all racial discrimination in the sale or rental of property.
                            </p>

                            <h4>Fair Housing Act</h4>
                            <p>
                               The Fair Housing Act of 1968 makes fair housing a national policy. It prohibits discrimination in the sale, lease or rental of housing, or making housing
                               otherwise unavailable because of race, color, religion, sex, disability, familial status or national origin.
                            </p>

                            <h4>Americans with Disabilities Act</h4>
                            <p>
                               Title III of the Americans with Disabilities Act prohibits discrimination against persons with disabilities in commercial facilities and places of
                               public accommodation.
                            </p>

                            <h4>Equal Credit Opportunity Act</h4>
                            <p>
                               The Equal Credit Opportunity Act makes it unlawful to discriminate against anyone on a credit application due to race, color, religion, national
                               origin, sex, marital status, age or because all or part of an applicant's income comes from any public assistance program.
                            </p>

                            <h4>THE RESPONSIBILITIES</h4>
                            <p>
                               The home seller, the home seeker, and the real estate professional all have rights and responsibilities under the law.
                            </p>

                            <h4>For the Home Seller</h4>
                            <p>
                               You should know that as a home seller or landlord you are obligated not to discriminate in the sale, rental and financing of property on the basis of race,
                               color, religion, sex, disability, familial status, or national origin. You may not instruct the licensed broker or salesperson acting as your agent to convey
                               for you any limitations in the sale or rental, because the real estate professional is also obligated not to discriminate. Under the law, a home seller or
                               landlord cannot establish discriminatory terms or conditions in the purchase or rental; deny that housing is available or advertise that the property is
                               available only to persons of a certain race, color, religion, sex, disability, familial status, or national origin.
                            </p>

                            <h4>For the Home Seeker</h4>
                            <p>
                               You have the right to expect that housing will be available to you without discrimination or other limitations based on race, color, religion, sex,
                               disability, familial status, or national origin. This includes the right to expect:
                            </p>

                            <ul>
                               <li>Housing in your price range made available to you without discrimination</li>
                               <li>Equal professional service</li>
                               <li>The opportunity to consider a broad range of housing choices</li>
                               <li>No discriminatory limitations on communities or locations of housing</li>
                               <li>No discrimination in the financing, appraising or insuring of housing</li>
                               <li>Reasonable accommodations in rules, practices and procedures for persons with disabilities</li>
                               <li>Non-discriminatory terms and conditions for sale, rental, financing, or insuring of a dwelling</li>
                               <li>To be free from harassment</li>
                            </ul>

                            <h4>For the Real Estate Professional</h4>
                            <p>
                               Agents in a real estate transaction may not discriminate on the basis of race, color, religion, sex, disability, familial status or national origin.
                               They also may not follow such instructions from a home seller or landlord.
                            </p>

                            <h4>What To Do if You Feel the Law Has Been Violated</h4>
                            <p>
                               Discrimination complaints about housing may be filed with the nearest office of the U.S. Dept. of Housing and Urban Development (HUD) or by calling
                               HUD's toll-free numbers, <a href="tel:+1-800-669-9777">1-800-669-9777</a> (voice) or TTY (800) <a href="tel:+927-9275">927-9275</a> contact HUD on the Internet at <a href="http://www.hud.gov/offices/fheo/index.cfm">http://www.hud.gov/offices/fheo/index.cfm</a>
                            </p>

                         </div>
                      </div>
                   </div>
                </div>

                <!-- Capture Leads Modal modal -->
                <div class="modal fade legal-modal" id="modalPrivacy" tabindex="-1" role="dialog">
                   <div class="modal-dialog modal-lg">
                      <div class="modal-content theme-modal">
                         <div class="modal-header">
                           <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">X</span></button>
                           <h2 class="text-center">Privacy Policy</h2>
                         </div>
                         <div class="modal-body clearfix">
                            <p>
                               The website you are accessing (the "Site") is hosted by Agent Squared on behalf of one of its real estate industry customers (the "Company").
                               Agent Squared and Company are independent contractors and are not partners or otherwise engaged in any joint venture in delivering the Site.
                               Notwithstanding that fact, this Privacy Policy ("Policy") applies to both Agent Squared and Company and as such, any reference to "we", "us" or "our"
                               herein will apply equally to both Agent Squared and Company. This Policy explains how personal information is collected, used, and disclosed with
                               respect to your use of the Site, so you can make an informed decision about using the Site.
                            </p>

                            <p>
                               We reserve the right to change this Policy at any time. If we make any material changes to the Policy, we will post a new statement on the Site
                               and update the effective date set forth above. Therefore, we encourage you to check the effective date of the Policy whenever you visit the Site to
                               see if it has been updated since your last visit.
                            </p>

                            <h4>INFORMATION WE COLLECT</h4>
                            <ul>
                               <li>
                                  <b>Personal Information.</b> As used herein, the term "personal information" means information that specifically identifies an individual
                                  (such as a name, address, telephone number, mobile number, or e-mail address,), and information about that individual's location or activities or
                                  demographic information when such information is directly linked to personally identifiable information. Personal information does not include "aggregate"
                                  information, which is data we collect about the use of the Site or about a group or category of products, services or users, from which individual identities
                                  or other personal information has been removed. This Policy in no way restricts or limits our collection and use of aggregate information.
                               </li>
                               <li>
                                  <b>Collection.</b> Personal information may be collected in a number of ways when you visit the Site. We may collect the personal information that you voluntarily
                                  provide to us. For example, when you fill out a form, send us an email, make a request, complete a survey, register for certain services on the Site, or
                                  participate in certain activities on the Site, we may ask you to provide personal information such as your e-mail address, name, home or work address or
                                  telephone number. We may also collect demographic information, such as your ZIP code, age, gender, and preferences. Information we collect may be combined with
                                  information we obtained through other services we offer. We may collect information about your visit to the Site, including the pages you view, the links you
                                  click and other actions taken in connection with the Site and services. We may also collect certain standard information that your browser sends to every web
                                  site you visit, such as your IP address, browser type and language, access times and referring Web site addresses. If we provide you with various services related
                                  to home selling and home buying (e.g., estimates of current market value of homes, moving costs, mortgage or finance costs, current home listings, neighborhood
                                  information, etc.), we may collect additional information related to such services.
                               </li>
                               <li>
                                  <b>Cookies and Web Beacons.</b> We may automatically collect certain information through the use of "cookies" or web beacons. Cookies are small pieces of information
                                  or files that are stored on a user's computer by the Web server through a user's browser to enable the site to recognize users who have previously visited them
                                  and retain certain information such as customer preferences and history. We may use cookies to identify users, customize their experience on our site or to
                                  serve relevant ads. If we combine cookies with or link them to any of the personal information, we would treat this information as personal information.
                                  We do not use Web beacons, or similar technology on the Site. However, we may permit third parties to place cookies or Web beacons on the Site to monitor
                                  the effectiveness of advertising and for other lawful purposes. A Web beacon, also known as a "Web bug" or a "clear gif" is a small, graphic image on a Web page,
                                  Web-based document or in an e-mail message that is designed to allow the site owner or a third party to monitor who is visiting a site and user activity on the site.
                                  We do not use (or permit third parties to use) Web beacons to collect personally identifying information about you. If you wish to block, erase, or be warned of
                                  cookies, please refer to your browser instructions to learn about these functions. However, if a browser is set to not accept cookies or if a user rejects a cookie,
                                  some portions of the Site may not function properly. For example, you may not be able to sign in and may not be able to access certain Site features or services.
                               </li>
                               <li>
                                  <b>Third Party Cookies and Web Beacons; Use of Third Party Ad Networks.</b> We may also use third parties to serve ads on the Site.These third parties may place
                                  cookies, Web beacons or other devices on your computer to collect nonpersonal information, and information provided by these devices may be used, among other things,
                                  to deliver advertising targeted to your interests and to better understand the usage and visitation of the Site and the other sites tracked by these third parties.
                                  For example, we may allow other companies, called third-party ad servers or ad networks, to display advertisements on the Site. Some of these ad networks may place a
                                  persistent cookie on your computer in order to recognize your computer each time they send you an online advertisement. In this way, ad networks may compile information
                                  about where you, or others who are using your computer, saw their advertisements and determine which ads are clicked on. This information allows an ad network to
                                  deliver targeted advertisements that they believe will be of most interest to you. We do not have access to the cookies that may be placed by the third-party ad
                                  servers or ad networks. This Policy does not apply to, and we are not responsible for, cookies in third party ads, and we encourage you to check the privacy policies
                                  of advertisers and/or ad services to learn about their use of cookies and other technology.
                               </li>
                            </ul>

                            <h4>USE OF PERSONAL INFORMATION</h4>
                            <p>
                               We use personal information internally to serve users of the Site, to enhance and extend our relationship with Site users and to facilitate the use of, and
                               administration and operation of, the Site. For example, we may use personal information you provide to deliver services to you, to process your requests or
                               transactions, to provide you with information you request, to personalize content, features and advertising on the Site, to anticipate and resolve problems with
                               the Site, to respond to customer support inquiries, to inform you of other events, promotions, products or services offered by third parties that we think will
                               be of interest to you, to send you relevant survey invitations related to the Site and for our own internal operations, and for any purpose for which such
                               information was collected. We may, from time to time, contact you on behalf of external business partners about a particular offering that may be of interest to you.
                               In those cases, your personal information (e-mail, name, address, telephone number) is not transferred to the third party unless you have consented to such transfer.
                            </p>

                            <h4>DISCLOSURE OF PERSONAL INFORMATION</h4>
                            <p>
                               Except as set forth in this Policy, we do not disclose, sell, rent, loan, trade or lease any personal information collected on the Site to any third party.
                               We may provide aggregate information to third parties without restriction. We will disclose personal information in the following circumstances:
                            </p>
                            <ul>
                               <li>
                                  <b>Consent.</b> We may share personal information with third parties when you give us your express or implied consent to do so, such as when you fill out a form
                                  requesting certain information from a third party.
                               </li>
                               <li>
                                  <b>Internal Analysis and Operations; Service Providers.</b> We will use and disclose information about you for our own internal statistical, design or operational
                                  purposes, such as to estimate our audience size, measure aggregate traffic patterns, and understand demographic, user interest, purchasing and other trends among
                                  our users. We may outsource these tasks and disclose personal information about you to third parties, provided the use and disclosure of your personal information
                                  by these third parties is restricted to performance of such tasks. We may also share personal information with other third-party vendors, consultants, and service
                                  providers ("Service Providers") who are engaged by or working with us in connection with the operation of the Site or our business In some cases, the Service Provider
                                  may be directly collecting the information from you on our behalf. We require our third party vendors to keep such information confidential and to only use the
                                  information for the purpose it was provided. However, we are not responsible for the actions of Service Providers or other third parties, nor are we responsible
                                  for any additional information you provide directly to these Service Providers or other third parties.
                               </li>
                               <li>
                                  <b>Protection of Company and Others:</b> We may disclose personal information when we believe it is appropriate to comply with the law (e.g., a lawful subpoena,
                                  warrant or court order); to enforce or apply this Policy or our other policies or agreements; to initiate, render, bill, and collect for amounts owed to us;
                                  to respond to claims, to protect our or our users' rights, property or safety; or to protect us, our users or the public from fraudulent, abusive, harmful
                                  or unlawful activity or use of the Site; or if we reasonably believe that an emergency involving immediate danger of death or serious physical injury to any
                                  person requires disclosure of communications or justifies disclosure of records without delay.
                               </li>
                               <li>
                                  <b>Business Transfers.</b> In addition, information about users of the Site, including personally identifiable information provided by users, may be disclosed
                                  or transferred as part of, or during negotiations regarding, any merger, acquisition, debt financing, sale of company assets, as well as in the event of an
                                  insolvency, bankruptcy or receivership in which personally identifiable information could be transferred to third parties as one of the business assets of
                                  the company.
                               </li>
                               <li>
                                  <b>Co-Branded Services.</b> Some services available through the Site may be co-branded and offered in conjunction with other companies. If you register for or
                                  use such services, then those other companies may also receive any information collected in conjunction with the co-branded services.
                               </li>
                            </ul>

                            <h4>LINKS</h4>
                            <p>
                               Please be aware that we may provide links to third party Web sites from the Site as a service to its users and we are not responsible for the content or
                               information collection practices of those sites. We have no ability to control the privacy and data collection practices of such sites and the privacy
                               policies of such sites may differ from this Policy. Therefore, we encourage you to review and understand the privacy policies of such sites before providing
                               them with any information.
                            </p>

                            <h4>SECURITY</h4>
                            <p>
                               We use commercially reasonable measures to store and maintain personally identifying information, to protect it from loss, misuse, alternation or destruction
                               by any unauthorized party while it is under our control.
                            </p>

                            <h4>CHILDREN</h4>
                            <p>
                               We do not intend, and the Site is not designed, to collect personal information from children under the age of 13. If you are under 13 years old, you should
                               not provide information on the Site.
                            </p>

                            <h4>CHOICE AND ACCESS</h4>
                            <p>
                               You may unsubscribe at any time from receiving non-service related communications from us. Site users have the following options for accessing, changing and
                               deleting personal information previously provided, or opting out of receiving communications from Company or Agent Squared:
                            </p>
                            <ul>
                               <li>email us at support@agentsquared.com; or</li>
                               <li>call us at the following telephone number: 800-901-4428</li>
                            </ul>

                            <span>QUESTIONS AND CONCERNS</span><br>
                            <span>If you have any questions about this Policy, the practices applicable to the Site, or your dealings with the Site, please contact us at the following address:</span>
                            <br>
                            <span>Agent Squared</span>
                            <br/>
                            <span>1155 Camino Del Mar, #103</span>
                            <br/>
                            <span>Del Mar, CA 92014</span>
                         </div>
                      </div>
                   </div>
                </div>
                <div class="modal fade legal-modal customer_login_signup" tabindex="-1" role="dialog">
                    <div class="modal-dialog">
                        <div class="modal-content theme-modal">
                            <div class="modal-header">
                                <button type="button" class="close pull-right" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">X</span></button>
                            </div>
                            <div class="modal-body clearfix sign-in-modal">

                                <div class="content-signup">
                                    <div class="clearfix">
                                        <span class="sign-up-message"></span>
                                        <form class="form-signin signupForm" action="/home/signup" method="post">
                                            <h2 class="text-center">Sign Up</h2>
                                            <input type="hidden" name="listing_id" class="listing_id">
                                            <input type="hidden" name="property_name" class="property_name">
                                            <input type="hidden" name="property_type" class="property_type">
                                            <input type="hidden" name="popular_search" class="search_url">
                                            <input type="hidden" name="redirect_customer" class="redirect_customer">

                                            <div class="form-group">
                                                <label for="First Name">First Name</label>
                                                <input type="text" name="fname" class="form-control" maxlength="50" pattern="[a-zA-Z0-9-_.\s]+" title="Allowed characters: A-Z, 0-9, ., -, _" required>
                                            </div>
                                            <div class="form-group">
                                                <label for="Last Name">Last Name</label>
                                                <input type="text" name="lname" class="form-control" maxlength="50" pattern="[a-zA-Z0-9-_.\s]+" title="Allowed characters: A-Z, 0-9, ., -, _" required>
                                            </div>
                                            <div class="form-group mb-25px">
                                                <label for="Email">Email</label>
                                                <input type="email" name="email" id="email" class="form-control" required>
                                            </div>
                                            <div class="form-group mb-25px">
                                                <label for="Phone Number">Cell / Phone Number</label>
                                                <input type="text" name="phone_signup" class="form-control phone_number" required>
                                            </div>
                                            <div class="form-group mb-25px">
                                                <div class="g-recaptcha" data-sitekey="6Lf9eRsUAAAAAB5lP4Em8vm4L-c1m0gSyAKOTort"></div>
                                            </div>
                                            <div class="col-md-12 col-sm-12 text-center">
                                                <button class="btn btn-orange signup_sbmt_btn" name="submit" type="submit">Sign Up</button>
                                                <!-- <a href="javascript:void(0)" onclick="modal_fb_login();" class="fb_btn" id="fb-btn"><img style="width: 40%;" src="/assets/images/fb2.png"></a>    -->
                                            </div>
                                            <div class="col-md-12 col-sm-12">
                                                <div class="sign-in-account text-center">
                                                    <p>Already have an account?</p>
                                                    <!-- <p><a href="javascript:void(0)" class="showLoginTrigger">Click here to login</a></p> -->
                                                    <p><a href="#" class="modalogin" >Click here to login.</a></p>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>

                                <div class="content-signin">
                                    <?php if (isIDXOwner()) { ?>
                                    <div class="agent-login-dashboard">
                                        <div class="text-center">
                                            <h3>Hi <?=$account_info->FirstName; ?>!</h3>
                                            <p>Did you mean to login to your agent dashboard?</p>
                                            <?php
                                                    if($this->input->get('modal_welcome')) { ?>
                                                        <a href="<?=AGENT_DASHBOARD_URL.'login/auth/agent_site_login?user_id='.$this->session->userdata('user_id').'&agent_id='.$this->session->userdata('agent_id').'&fresh=TRUE';?>" target="_blank" class="close-note btn-agent-login">
                                                <?php
                                                    } else { ?>
                                                        <a href="<?=AGENT_DASHBOARD_URL.'login/auth/agent_site_login?user_id='.$this->session->userdata('user_id').'&agent_id='.$this->session->userdata('agent_id');?>" target="_blank" class="close-note btn-agent-login">
                                                <?php
                                                    }
                                                ?>
                                                Click here to login to your agent dashboard</a>
                                        </div>
                                    </div>
                                    <?php } ?>
                                    <div class="clearfix">
                                        <span class="sign-in-message col-md-12"></span>
                                        <form class="form-signin loginForm col-md-12" action="/home/login" method="post">
                                            <h2 class="text-center">Sign In</h2>
                                            <input type="hidden" name="listing_id" class="listing_id">
                                            <input type="hidden" name="property_name" class="property_name">
                                            <input type="hidden" name="property_type" class="property_type">
                                            <input type="hidden" name="popular_search" class="search_url">
                                            <input type="hidden" name="redirect_customer" class="redirect_customer">
                                            <input type="hidden" name="fromSearch" class="fromSearch">

                                            <div class="form-group mb-25px col-md-12">
                                                <label for="Email">Email</label>
                                                <input type="email" name="email" id="email" class="form-control" required>
                                            </div>
                                            <div class="form-group mb-25px col-md-12">
                                                <label for="Phone Number">Cell / Phone Number</label>
                                                <input type="text" name="phone" class="form-control phone_number" required>
                                            </div>
                                            <div class="form-group mb-25px text-center">
                                                <a href="#" id="showResetTrigger" class="modalreset">Reset my phone number</a>
                                            </div>
                                            <div class="col-md-12 col-sm-12 text-center">
                                                <button class="btn btn-orange login_sbmt_btn" name="submit" type="submit">Login to customer dashboard</button>
                                                <!-- <a href="javascript:void(0)" onclick="modal_fb_login();" class="fb_btn" id="fb-btn"><img style="width: 40%;" src="/assets/images/fb2.png"></a>    -->
                                            </div>
                                            <div class="col-md-12 col-sm-12">
                                                <div class="sign-in-account text-center">
                                                    <p>Don't have an account with us yet?</p>
                                                    <p><a href="#" class="modalsignup" >Click here to sign up.</a></p>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>

                                <div class="content-reset">
                                    <div class="clearfix">
                                        <span class="reset-message"></span>
                                        <form class="form-signin resetPhoneForm" action="/home/reset_phone_number" method="post">
                                            <h2 class="text-center">Reset Phone Number</h2>
                                            <div class="form-group mb-25px">
                                                <label for="Email">Email</label>
                                                <input type="email" name="email" id="email" class="form-control" required>
                                            </div>
                                            <div class="col-md-12 col-sm-12 text-center">
                                                <button class="btn btn-orange" name="submit" type="submit">Send Reset Email</button>
                                            </div>
                                            <div class="form-group mb-25px">
                                                <a href="#" class="modalogin">&laquo; Back to login</a>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="request-info-modal" class="modal fade" tabindex="-1" role="dialog">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title">Contact Us</h4>
                                <p>We look forward to helping you. Please add any additional comments below and we will be in touch with you shortly.</p>
                            </div>
                            <div class="modal-body">
                                <div class="show-mess" ></div><br/>
                                <form action="<?php echo site_url('home/customer/request_information'); ?>" method="POST" class="customer-request-info" id="customer-request-info" >
                                    <input type="hidden" name="property_id">
                                    <input type="hidden" name="property_address">
                                    <input type="hidden" name="property_type">

                                    <div class="form-group">
                                        <label for="first_name">First Name</label>
                                        <input type="first_name" name="first_name" class="form-control" id="first-name" value="<?= (isset($profile->first_name)) ? $profile->first_name : "" ; ?>" placeholder="First Name" maxlength="50" pattern="[a-zA-Z0-9-_.\s]+" title="Allowed characters: A-Z, 0-9, ., -, _" required>
                                    </div>

                                    <div class="form-group">
                                        <label for="last_name">Last Name</label>
                                        <input type="last_name" name="last_name" class="form-control" id="last-name" value="<?= (isset($profile->last_name)) ? $profile->last_name : "" ; ?>" placeholder="Last Name" maxlength="50" pattern="[a-zA-Z0-9-_.\s]+" title="Allowed characters: A-Z, 0-9, ., -, _" required>
                                    </div>

                                     <div class="form-group">
                                        <label for="email">Email</label>
                                        <input type="email" name="email" class="form-control" id="email" value="<?=(isset($profile->email)) ? $profile->email : "";?>" placeholder="Email" required <?=(isset($profile->email)) ? "readonly" : "";?>>
                                    </div>
                                    <div class="form-group">
                                        <label for="phone">Phone</label>
                                        <input type="phone" name="phone" class="form-control phone_number" id="phone" value="<?=(isset($profile->phone)) ? $profile->phone : "";?>" placeholder="Phone Number" required <?=(isset($profile->phone)) ? "readonly" : "" ; ?>>
                                    </div>
                                     <div class="form-group">
                                        <label for="comments">Comments, Questions, Special Requests?</label>
                                        <textarea rows="4" cols="50" name="comments" class="form-control" id="phone" placeholder="Comments, Questions, Special Requests?" required></textarea>
                                    </div>
                                    <!-- <?php //if(!$this->session->userdata("customer_id")) : ?>
                                        <?php //echo $recaptcha['widget'];?>
                                        <?php //echo $recaptcha['script'];?>
                                    <?php //endif ?> -->
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Submit <i class="submit-loader"></i></button>
                            </div>
                                </form>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.request-info-modal -->

                <div class="modal fade legal-modal" id="capture-leads-login" tabindex="-1" role="dialog">
                    <div class="modal-dialog">
                        <div class="modal-content theme-modal">
                            <div class="modal-header">
                                <button type="button" class="close pull-right capture-leads-login-btn close" data-dismiss="modal" aria-label="Close" style="display: none;"><span aria-hidden="true">X</span></button>
                            </div>
                            <div class="modal-body clearfix sign-in-modal">

                                <div class="content-leads-signup">
                                    <div class="clearfix">
                                        <span class="sign-up-message"></span>
                                        <form class="form-signin signupForm" action="/home/signup" method="post">
                                            <h2 class="text-center">Sign Up</h2>
                                            <input type="hidden" name="listing_id" class="listing_id" value="<?=(isset($property_id)) ? $property_id : '';?>">
                                            <input type="hidden" name="property_name" class="property_name" value="<?=(isset($property_name)) ? $property_name : '';?>">
                                            <input type="hidden" name="property_type" class="property_type" value="<?=(isset($isMyProperty) && $isMyProperty == TRUE) ? 'featured_property' : 'other_property';?>">
                                            <input type="hidden" name="popular_search" class="search_url">
                                            <input type="hidden" name="redirect_customer" class="redirect_customer">
                                            <input type="hidden" name="submission_type" class="submission_type">

                                            <div class="form-group">
                                                <label for="First Name">First Name</label>
                                                <input type="text" name="fname" class="form-control" maxlength="50" pattern="[a-zA-Z0-9-_.\s]+" title="Allowed characters: A-Z, 0-9, ., -, _" required>
                                            </div>
                                            <div class="form-group">
                                                <label for="Last Name">Last Name</label>
                                                <input type="text" name="lname" class="form-control" maxlength="50" pattern="[a-zA-Z0-9-_.\s]+" title="Allowed characters: A-Z, 0-9, ., -, _" required>
                                            </div>
                                            <div class="form-group mb-25px">
                                                <label for="Email">Email</label>
                                                <input type="email" name="email" id="email" class="form-control" required>
                                            </div>
                                            <div class="form-group mb-25px">
                                                <label for="Phone Number">Cell / Phone Number</label>
                                                <input type="text" name="phone_signup" class="form-control phone_number" required>
                                            </div>
                                            <div class="form-group mb-25px">
                                                <div class="g-recaptcha" data-sitekey="6Lf9eRsUAAAAAB5lP4Em8vm4L-c1m0gSyAKOTort"></div>
                                            </div>
                                            <div class="col-md-12 col-sm-12 text-center">
                                                <button class="btn btn-orange signup_sbmt_btn" name="submit" type="submit">Sign Up</button>
                                                <!-- <a href="javascript:void(0)" onclick="modal_fb_login();" class="fb_btn" id="fb-btn"><img style="width: 40%;" src="/assets/images/fb2.png"></a>    -->
                                            </div>
                                            <div class="col-md-12 col-sm-12">
                                              <div class="sign-in-account text-center">
                                                    <p>Already have an account?</p>
                                                    <!-- <p><a href="javascript:void(0)" class="showLoginTrigger">Click here to login</a></p> -->
                                                    <p><a href="#" class="leads-login">Click here to login.</a></p>
                                              </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>

                                <div class="content-leads-login">
                                    <div class="clearfix">
                                        <span class="sign-in-message"></span>
                                        <form class="form-signin loginForm" action="/home/login" method="post">
                                            <h2 class="text-center">Sign In</h2>
                                            <input type="hidden" name="listing_id" class="listing_id" value="<?=(isset($property_id)) ? $property_id : '';?>">
                                            <input type="hidden" name="property_name" class="property_name" value="<?=(isset($property_name)) ? $property_name : '';?>">
                                            <input type="hidden" name="property_type" class="property_type" value="<?=(isset($isMyProperty) && $isMyProperty == TRUE) ? 'featured_property' : 'other_property';?>">
                                            <input type="hidden" name="popular_search" class="search_url">
                                            <input type="hidden" name="redirect_customer" class="redirect_customer">
                                            <input type="hidden" name="submission_type" class="submission_type">

                                            <div class="form-group mb-25px">
                                                <label for="Email">Email</label>
                                                <input type="email" name="email" id="email" class="form-control" required>
                                            </div>
                                            <div class="form-group mb-25px">
                                                <label for="Phone Number">Cell / Phone Number</label>
                                                <input type="text" name="phone" class="form-control phone_number" required>
                                            </div>
                                            <div class="form-group mb-25px">
                                                <a href="#modalResetPhone" id="showResetTrigger" class="leads-resetpass">Need to reset your phone number?</a>
                                            </div>
                                            <div class="col-md-12 col-sm-12 text-center">
                                                <button class="btn btn-orange login_sbmt_btn" name="submit" type="submit">Sign In</button>
                                                <!-- <a href="javascript:void(0)" onclick="modal_fb_login();" class="fb_btn" id="fb-btn"><img style="width: 40%;" src="/assets/images/fb2.png"></a>    -->
                                            </div>
                                            <div class="col-md-12 col-sm-12">
                                                <div class="sign-in-account text-center">
                                                    <p>Don't have an account with us?</p>
                                                    <p><a href="#" class="leads-signup">Click here to sign up.</a></p>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>

                                <div class="content-leads-reset">
                                    <div class="clearfix">
                                        <span class="reset-message"></span>
                                        <form class="form-signin resetPhoneForm" action="/home/reset_phone_number" method="post">
                                            <h2 class="text-center">Reset Phone Number</h2>
                                            <div class="form-group mb-25px">
                                                <label for="Email">Email</label>
                                                <input type="email" name="email" id="email" class="form-control" required>
                                            </div>
                                            <div class="col-md-12 col-sm-12 text-center">
                                                <button class="btn btn-orange " name="submit" type="submit">Send Reset Email</button>
                                            </div>
                                            <div class="form-group mb-25px">
                                                <a href="#" class="leads-login">&laquo; Back to login</a>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Modal -->
                <div id="save_search_modal" class="modal fade modalNote" role="dialog">
                    <div class="modal-dialog">
                        <!-- Modal content-->
                        <div class="modal-content theme-modal">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title text-center"></h4>
                            </div>
                            <div class="modal-body">
                                <p>Saving your search will help you find your ideal house with ease. Do you want to save this search for future references?</p>
                            </div>
                            <div class="modal-footer">
                                <?php
                                    if(!$this->session->userdata('customer_id')) {

                                       if($this->uri->segment(1)== "search-results"){?>

                                            <button class="btn btn-primary saved_search save_search_modalogin" data-toggle='modal' data-target='.customer_login_signup' data-redirect-customer="search-results?<?=http_build_query($_GET).'&success';?>" title="Save Search" data-dismiss="modal">Save</button>

                                   <?php } else {?>

                                            <button class="btn btn-primary saved_search save_search_modalogin" data-toggle='modal' data-target='.customer_login_signup' data-redirect-customer="listings?Search=<?=$this->input->get('Search').'&success';?>" title="Save Search" data-dismiss="modal">Save</button>

                                   <?php }?>
                                <?php
                                    } else { ?>
                                        <!-- <a href="<?php echo site_url("home/home/save_search_properties?".http_build_query($_GET)); ?>" type="button" class="btn btn-primary follow-search">Save</a> -->
                                        <button type="button" class="btn btn-primary" id="save_search_properties_btn" data-save-url="<?='/home/save_search_properties?'.http_build_query($_GET);?>">Save</button>
                                <?php
                                    }
                                ?>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">No, Thanks</button>
                            </div>
                        </div>
                  </div>
                </div>
            </div>
        </div>
    </footer>

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js?version=<?=AS_VERSION?>"><\/script>')</script>
    <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.js"></script>

    <!-- <script type="text/javascript" src="/assets/js/agent-haven/compressed-min.js"></script> -->
    <!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script> -->
    <script type="text/javascript" src="/assets/bootstrap/js/bootstrap.min.js?version=<?=AS_VERSION?>"></script>
    <script type="text/javascript" src="/assets/js/agent-haven/vegas.js?version=<?=AS_VERSION?>"></script>
    <script type="text/javascript" src="/assets/js/agent-haven/selectize.js?version=<?=AS_VERSION?>"></script>
    <script type="text/javascript" src="/assets/js/agent-haven/owl.carousel.js?version=<?=AS_VERSION?>"></script>
    <script src="/assets/js/agent-haven/main.js?version=<?=AS_VERSION?>"></script>
    <!-- <script src="//cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.1.62/jquery.inputmask.bundle.js"></script> -->
    <?php
        if(isset($cdn)) :
            foreach($cdn as $key => $val) : ?>
                <script src="<?=$val;?>" type="text/javascript"></script>
    <?php   endforeach;
        endif;
    ?>
    <?php
        if(isset($js)) :
            foreach($js as $key => $val ) :  ?>
                <script src="/assets/js/agent-haven/<?php echo $val?>?version=<?=AS_VERSION?>" type="text/javascript"></script>
    <?php   endforeach;
        endif;
    ?>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/additional-methods.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.2/js/bootstrap-switch.min.js"></script>

    <?php if ($this->input->get('modal_welcome')==TRUE): ?>
      <script>
        $(document).ready(function() {
          $('#pollSlider-button').click();
        });
      </script>
    <?php endif ?>

    <?php
        if(isIDXOwner() && !$this->session->userdata('website_synced')) { ?>
            <script type="text/javascript">
                $.ajax({
                    url: '/agent/agent_syncer/',
                    type: 'GET',
                    dataType: 'json',
                    beforeSend: function () {
                        console.log('syncing....');
                    },
                    success: function(res) {
                        console.log(res);
                    }
                });
            </script>
    <?php
        }
    ?>
    <!-- Advance search search result page -->
    <?php
        if($this->input->get('Search')) { ?>
            <script type="text/javascript">
                $search_val = "<?php echo $this->input->get('Search'); ?>";
                $totalrows = "<?php echo ($this->input->get('totalrows')) ? $this->input->get('totalrows') : 0; ?>";
                $offset = "<?php echo ($this->input->get('offset')) ? $this->input->get('offset') : 0; ?>";
                $order_by = "<?php echo ($this->input->get('order')) ? $this->input->get('order') : ''; ?>"
            </script>
    <?php
        } else { ?>
            <script type="text/javascript">$search_val = ""</script>
    <?php
        }
        //if($page=="home" || $page =="home_custom") { ?>
            <!-- <script type="text/javascript">
                $(window).load(function() {
                    $('input.phone_number').inputmask({
                        mask: [{ "mask": "(###) ###-####"}],
                        greedy: false,
                        definitions: { '#': { validator: "[0-9]", cardinality: 1}}
                    });
                });
            </script> -->
    <?php
        //}

        $leads_config_json = (leads_capture_config()) ? json_encode(leads_capture_config()) : FALSE; ?>

        <script type="text/javascript">

            $(".customer_login_signup, #capture-leads-login").on('hidden.bs.modal', function() {
                $(this).find('form.loginForm')[0].reset(); //Empty all fields in the form upon closing the modal
                $(this).find('form.signupForm')[0].reset(); //Empty all fields in the form upon closing the modal

                $(".sign-up-message").html("");
                $(".sign-in-message").html("");
            });

            var myStorage = window.localStorage;

        </script>

    <?php
        if($leads_config_json != FALSE && !$this->session->userdata('customer_id')) { ?>
            <script type="text/javascript">
                var leads_config_set = true;
                var current_page = "<?php echo $this->uri->segment(1); ?>";
                var leads_config = JSON.parse('<?=$leads_config_json; ?>');
            </script>
    <?php
        } else { ?>
            <script type="text/javascript">
                var leads_config_set = false;
            </script>
    <?php
        }

        if($this->session->userdata('customer_id')) { ?>
            <script type="text/javascript">var isCustomLoggedin=true;</script>
    <?php
            if($this->session->userdata('save_properties')) {
                $prop_arr = array();
                foreach($this->session->userdata('save_properties') as $prop=>$val) {
                    array_push($prop_arr, $val['property_id']);
                }
                $prop_arr_obj = (object)$prop_arr;
                $save_prop_json = json_encode($prop_arr); ?>
                <script type="text/javascript">
                    var save_prop_json = JSON.parse('<?=$save_prop_json;?>');
                </script>
    <?php
            } else { ?>
                <script type="text/javascript">var save_prop_json=null;</script>
    <?php
            }
        } else { ?>
            <script type="text/javascript">var isCustomLoggedin=false;</script>
    <?php
        }

    ?>
    <script>

        $(document).ready(function() {

            $("a[href='#show-map'], a[href='#collapse-show-map']").click(function() {

                console.log("show map clicked");

                if($search_val!="") {
                    $dataString = {'Search':$search_val, 'offset':$offset, 'totalrows':$totalrows, 'order':$order_by}
                } else {
                    $dataString = $("#advance_search_form").serialize();
                }

                console.log($dataString);

                $.ajax({
                    url: '/home/show_map',
                    type: 'GET',
                    data: $dataString,
                    dataType: 'json',
                    beforeSend: function () {
                        console.log('show map');
                        $(".propmap-placeholder").show();
                        $(".propmap").hide();
                    },
                    success: function(obj) {

                        console.log(obj);

                       if(obj.success) {

                            $(".propmap").show();
                            $(".propmap-placeholder").hide();

                            var strProperties = '';
                            var geocoder;
                            var map;
                            var markers = [];

                            if(obj.properties.length > 0) {

                                map = new google.maps.Map(
                                    document.getElementById("show_map_canvas"), {
                                        center: new google.maps.LatLng(obj.properties[0].latitude, obj.properties[0].longitude),
                                        zoom: 10,
                                        scrollwheel: false,
                                        mapTypeId: google.maps.MapTypeId.ROADMAP
                                    }
                                );

                                for(var i = 0; i < obj.properties.length; i++) {

                                    var myLatLng = {lat: obj.properties[i].latitude, lng: obj.properties[i].longitude};

                                    markers[i] = new google.maps.Marker({
                                        position: myLatLng,
                                        map: map,
                                        icon: base_url+'assets/img/home/map-marker.png'
                                    });

                                    (function(marker, i) {

                                        google.maps.event.addListener(marker, 'click', function() {

                                            iw = new google.maps.InfoWindow({
                                                content: obj.properties[i].content_string,
                                                maxWidth: 275,
                                                position: myLatLng
                                            });

                                            iw.open(map, marker);

                                        });

                                        google.maps.event.addListener(marker, 'mouseover', function() {
                                            iw = new google.maps.InfoWindow({
                                              content: obj.properties[i].content_string,
                                              maxWidth: 275,
                                              position: myLatLng
                                            });
                                            iw.open(map, marker);
                                        });

                                        google.maps.event.addListener(marker, 'mouseout', function() {
                                            iw.close();
                                        });

                                    } (markers[i], i));

                                }

                            }

                        } else {

                            $(".propmap").show();
                            $(".propmap-placeholder").hide();
                            $("#show_map_canvas").html("<div class='alert alert-danger'><h1>No listings found! Nothing to show here.</h1></div>");

                        }
                    }
                });

                function numberWithCommas(x) {
                    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                }

            });
        });
    </script>

    <?php if($page == "advance_search"): ?>
        <script type="text/javascript">
            $(document).ready(function() {
                //$("select.form-control").selectize({});
                //refine search
                var $defaul_codes = document.getElementById( "defaultval-property-type2" );;
                property_type_code = $defaul_codes.options[$defaul_codes.selectedIndex].dataset.propertyclassCode;
                var codes = ["A","B","C","D","E","F"];
                codes.forEach(function(item){
                    if(item == property_type_code){
                        console.log(item);
                        $("."+property_type_code).css("display","block");
                        $("."+item).attr("name","PropertySubType");
                    }else{
                        $("."+item).css("display","none");
                        $("."+item).attr("name"," ");
                        //$("."+item).val("");
                    }
                });
            });
        </script>
    <?php endif ?>
    <!-- Modal Save This Search -->
    <?php

         if($this->uri->segment(1) == "search-results" || $this->uri->segment(1) == "listings" ) {

            if(!empty($featured)) {

                if(!empty(http_build_query($_GET))) {

                $saved_searches = FALSE;

                if(isset($saved_searches_db) && !empty($saved_searches_db)) :
                    foreach($saved_searches_db as $searches) :
                        if($searches->url == http_build_query($_GET)) :
                            $saved_searches = TRUE;
                            break;
                        endif;
                    endforeach;
                else :
                    $saved_searches = FALSE;
                endif;

                if(!$saved_searches) { ?>
                    <script type="text/javascript">

                        $(document).ready(function() {

                            setTimeout(function() {
                                $('#save_search_modal').modal();
                            }, 1000);

                        });

                        $('#save_search_properties_btn').on('click', function() {
                            $("input[name='redirect_customer']").val($(this).attr("data-redirect-customer"));
                            $("input[name='fromSearch']").val(1);
                            var b = $(this).attr("data-save-url");
                            $.ajax({
                                url: b,
                                type: "post",
                                dataType: "json",
                                success: function(a) {
                                    console.log(a);
                                    if(a.success) {
                                        $('#save_search_modal').modal('toggle');
                                        $(".follow-search").html("<i class='fa fa-save'></i> Saved Search").prop("disable", !0).addClass("saved-search").removeClass("follow-search").css("background-color", "#e24545");
                                        $(".saved-search").removeAttr("href");
                                    }
                                }
                            });
                        });

                    </script>
    <?php
                }
            }
        }
    }
    ?>
    <!-- Modal Pricing -->
    <?php
        if($this->input->get("is_trial") || $this->config->item('trial')) { ?>
            <script type="text/javascript">
            <?php
                if($this->config->item('left') >= 15) { ?>
                    $(document).ready(function() {
                        $('#modalPricing').modal('show');
                    });
    <?php
                } ?>
            </script>
    <?php
        }

        if(!$this->config->item('agent_idx_website')) { ?>
            <script type="text/javascript">
                $(document).ready(function() {
                    $('#agent-idx-website-modal').modal('show');
                });
            </script>
    <?php
        }
    ?>
    <!-- End of Modal Pricing -->

    <!-- custom-save-searches -->
    <?php if (!empty($saved_searches_selected)): ?>
        <script type="text/javascript">
             $(document).ready(function(){
                $(".owl-carousel-saved-search").owlCarousel({
                    loop:true,
                    margin:10,
                    nav:true,
                    items : <?php $savedItems = count($saved_searches_selected);
                            if ($savedItems < 4) {
                                echo $savedItems;
                            } else {
                                echo 4;
                            }
                        ?>,
                    navigation : true,
                    navigationText: ["<i class='fa fa-chevron-left'></i> <span>prev</span>","<span>next</span> <i class='fa fa-chevron-right'></i>"]
                });
            });
        </script>

    <?php endif ?>

    <!-- Sold listing -->
    <?php if (!empty($sold_listings)): ?>
        <script type="text/javascript">
            $(document).ready(function(){
                $(".sold-property-container").owlCarousel({
                        navigation : true,
                        pagination : true,
                        slideSpeed : 700,
                        paginationSpeed : 400,
                        navigationText: ["<i class='fa fa-chevron-left'></i> <span>prev</span>","<span>next</span> <i class='fa fa-chevron-right'></i>"],
                        items : 4,
                        itemsDesktop : [1199,3],
                        itemsDesktopSmall : [979,3]
                });

                var url = base_url + 'ajax_sold_listings';
                var data,listing_id,address,city,state_or_province,postal_city,url_rewrite,amount,photo,date_sold,$html,item_count,format_data = '';

                $.ajax({
                        url: url,
                        type: "get",
                        dataType: "json",
                        data: data,
                        beforeSend: function() {
                            $('.cd-control').removeClass('col-md-3');
                        },
                        success: function(result) {
                            if(result.length > 0){
                                $(".sold-property-container .owl-item").remove();
                                $.each(result,function(index, item){
                                    address = item.UnparseAddress.split('"').join('');
                                    url_rewrite = "/property-details/" + address.replace(/\s+/g, '-').split(/[ ,]+/).join('') +'-' +item.PostalCode.split('"').join('') + "/"+item.ListingKey.split('"').join('');
                                    city    = item.City.split('"').join('') + ','+item.StateOrProvince.split('"').join('')  + ' '+item.PostalCode.split('"').join('');
                                     amount = item.ClosePrice.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                     date_sold = item.CloseDate.split('"').join('');
                                     date_sold = new Date(date_sold);
                                     format_date = ("0" + (date_sold.getMonth()+1)).slice(-2) + '-' + ("0" + date_sold.getDate()).slice(-2) + '-' + date_sold.getFullYear();
                                     photo = item.PhotosURi;
                                     listing_id = item.ListingKey;

                                     if(index == 7){
                                        $html = '<div class="owl-item" style="width:191px;">'+
                                                    '<div class="other-listing-item last-item-sold">'+
                                                        '<div class="sold-banner">' +
                                                            '<img src="/assets/images/dashboard/demo/agent-haven/sold.png">'+
                                                        '</div>'+
                                                        '<div class="sold-image">'+
                                                            '<div class="last-item-viewall">' +
                                                                '<a href="/sold_listings" target="_blank">View All</a>'+
                                                            '</div>' +
                                                            '<a href="/sold_listings" target="_blank">' +
                                                            '<img src='+ photo + ' class="img-responsive">'+ 
                                                            '</a>' +
                                                        '</div>' +
                                                    '</div>'+
                                                  '</div>';

                                     }else{
                                         $html = '<div class="owl-item" style="width:191px;">'+
                                                    '<div class="other-listing-item">'+
                                                        '<div class="sold-banner">' +
                                                            '<img src="/assets/images/dashboard/demo/agent-haven/sold.png">'+
                                                        '</div>'+
                                                        '<div class="sold-image">'+
                                                            '<a href="'+ url_rewrite + '" target="_blank" >' +
                                                            '<img src='+ photo + ' class="img-responsive">'+ 
                                                            '</a>' +
                                                        '</div>' +
                                                        '<div class="sold-property-details">'+
                                                            '<p><a href="'+url_rewrite+'" target="_blank">'+ address + '</a></p>'+
                                                            '<p><i class="fa fa-map-marker"></i> '+ city+'</p>' +
                                                            '<p><i class="fa fa-usd"></i> '+amount +'</p>'+
                                                            '<p><i class="fa fa-calendar"></i> '+format_date + '</p>'+
                                                        '</div>'+
                                                    '</div>'+
                                                  '</div>';
                                        }
                                     $(".sold-property-container .owl-wrapper").append($html);
                                });
                            
                            }else{
                                $(".properties-sold").remove();
                            }

                            if(result.length < 5){
                                $(".sold-property-container .owl-controls").css('display','none');
                                $(".sold-property-container").owlCarousel({
                                    navigation : false
                                });
                            }
                        }
                    });
            });
        </script>
    <?php endif ?>

    <?php
        if($page == "property") { $id = $property_id; ?>
            <!-- AJAX Property Display -->
            <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.2/js/bootstrap-switch.min.js"></script>
            <script async src="https://static.addtoany.com/menu/page.js"></script>
            <script type="text/javascript">
                $(document).ready(function(){
                    $('#property-photos').load('<?=base_url()?>home/property/property_photos/<?=$id?>', function(){
                        var photosync1 = $("#photosync1");
                        var photosync2 = $("#photosync2");

                        photosync1.owlCarousel({
                            singleItem : true,
                            slideSpeed : 1000,
                            pagination:false,
                            afterAction : syncPosition,
                            responsiveRefreshRate : 200,
                        });

                        photosync2.owlCarousel({
                            items : 5,
                            itemsDesktop      : [1199,10],
                            itemsDesktopSmall     : [979,10],
                            itemsTablet       : [768,8],
                            itemsMobile       : [479,4],
                            navigation: true,
                            pagination:false,
                            navigationText: ["<i class='fa fa-chevron-left'></i> <span>prev</span>","<span>next</span> <i class='fa fa-chevron-right'></i>"],
                            responsiveRefreshRate : 100,
                            afterInit : function(el){
                              el.find(".owl-item").eq(0).addClass("synced");
                            }
                        });


                        function syncPosition(el) {

                            var current = this.currentItem;
                            $("#photosync2")
                                .find(".owl-item")
                                .removeClass("synced")
                                .eq(current)
                                .addClass("synced")

                            if($("#photosync2").data("owlCarousel") !== undefined) {
                              center(current)
                            }
                        }

                        $("#photosync2").on("click", ".owl-item", function(e){
                            e.preventDefault();
                            var number = $(this).data("owlItem");
                            photosync1.trigger("owl.goTo",number);
                        });

                        function center(number) {
                            var photosync2visible = photosync2.data("owlCarousel").owl.visibleItems;
                            var num = number;
                            var found = false;

                            for(var i in photosync2visible){
                                if(num === photosync2visible[i]){
                                    var found = true;
                                }
                            }

                            if(found===false){
                                if(num>photosync2visible[photosync2visible.length-1]){
                                    photosync2.trigger("owl.goTo", num - photosync2visible.length+2)
                                } else {
                                    if(num - 1 === -1){
                                        num = 0;
                                    }
                                    photosync2.trigger("owl.goTo", num);
                                }
                            } else if(num === photosync2visible[photosync2visible.length-1]){
                                photosync2.trigger("owl.goTo", photosync2visible[1])
                            } else if(num === photosync2visible[0]){
                                photosync2.trigger("owl.goTo", num-1)
                            }

                        }
                    });

                    <?php
	                /**
                     * Temp. Disable for displaying "Nearby Listings"
                     * in other property detail page (/other-property-details/)
                     */
                    if ( 'other-property-details' !== $this->uri->segment(1) ): ?>
                    $('#nearby-listings').load('<?=base_url()?>home/property/property_nearby/<?=$id?>', function(){
                        $(".sold-property-container").owlCarousel({
                            navigation : true,
                            pagination : true,
                            slideSpeed : 700,
                            paginationSpeed : 400,
                            navigationText: ["<i class='fa fa-chevron-left'></i> <span>prev</span>","<span>next</span> <i class='fa fa-chevron-right'></i>"],
                            items : 4,
                            itemsDesktop : [1199,3],
                            itemsDesktopSmall : [979,3]
                        });
                    });
                    <?php endif; /** End of nearby-listings in in other property detail page (/other-property-details/) */?>

                    $('#has-vtour').load('<?=base_url()?>home/property/property_virtual_tours/<?=$id?>', function(){

                             $(".virtualTour").click(function(){
                                var iframe = $(".virtualTours-iframe");
                                iframe.attr("src", iframe.data("src"));
                            });

                             $(".close-tour").click(function(){
                                var iframe = $(".virtualTours-iframe");
                                iframe.removeAttr("src");
                            });
                    });
                    $('#rooms').load('<?=base_url()?>home/property/property_rooms/<?=$id?>');

                });
            </script>
            <!-- End Of AJAX Property Display -->

            <!--Schedule Showing Datepicker -->
            <script src="/assets/js/plugins/datepicker/js/bootstrap-datepicker.js?version=<?=AS_VERSION?>"></script>
            <script type="text/javascript">
                $(document).ready(function(){
                    $( "#startdatepicker" ).datepicker({format: "yyyy-mm-dd"});
                    $( "#startdatepickermodal" ).datepicker({format: "yyyy-mm-dd", });
                });
            </script>
            <script type="text/javascript">
            $(document).ready(function(){

                $('#datepickersched').datepicker({
                    format: 'yyyy-mm-dd',
                    startDate: '-0d',

                });

            });

            $(document).ready(function(){

                $('.open-datetimepicker').click(function(event){
                    event.preventDefault();
                    $('#datepickersched').focus();
                });

            });
            </script>
    <?php
        }
    ?>

    <script type="text/javascript">
        $(window).ready(function() {
            'use strict';
            $(function() {
                $('#advance_search_form #submitFormSearch').click(function(e){
                    e.preventDefault();

                    var formFields = '';
                    $("#advance_search_form input[type='text'], #advance_search_form input[type='checkbox']:checked, #advance_search_form select, #advance_search_form textarea").each(function() {
                        if($(this).val() === "") {}
                        else {
                            if(formFields === "") {
                                formFields += $(this).attr('name') + '=' + $(this).val();
                            }
                            else {
                                formFields += '&' + $(this).attr('name') + '=' + $(this).val();
                            }
                        }
                    });
                    window.location.href = $("#advance_search_form").attr('action')+"?"+formFields;
                    // console.log(formFields);
                    // $("#advance_search_form").submit();
                    // console.log("Submit");
                });
                // Hide dropdown menu
                $( ".navbar-nav.main-nav" ).css( "display", "block" );
                $( ".navbar-nav.main-nav .has-dropdown .dropdown-menu" ).css( "display", "none" );

                // Hide custom menu
                $( ".sub-navbar-menu" ).css( "display", "block" );

                $('.slider-area').vegas({
                    slides: [
                        <?php
                        $count = 0;
                        $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
                        if(!empty($slider_photos)){
                            if(isset($slider_photos)){
                                foreach($slider_photos as $photo){
                                    if(isset($photo->photo_url)){
                                        if($photo->is_freemium == 1){
                                            $src = "https://d2svk5q5uf24n2.cloudfront.net/uploads/slider/".$photo->photo_url;
                                        }else{
                                           $src = "https://d2svk5q5uf24n2.cloudfront.net/upload/slider/".$photo->photo_url;
                                        }?>

                                         { src: "<?=$src?>"},

                                    <?php   }
                                    $count++;
                                }
                            }

                            } else{

                            if(isset($active_listings)) {
                                foreach($active_listings as $active) {
                                        if(isset($active->StandardFields->Photos[0]->Uri1024)) {
                                            if(!empty($active->StandardFields->Photos[0]->Uri1024)) {
                                                $old_url = $active->StandardFields->Photos[0]->Uri1024;
                                                $protocol1 = parse_url($old_url, PHP_URL_SCHEME);
                                                $protocol_replace = $protocol1."://";
                                                $new_url = str_replace($protocol_replace, $protocol, $old_url);
                                            ?>

                                            { src: "<?=$old_url;?>"},
                                            <?php
                                                }
                                            }
                                                //else {
                                                    ?>
                                            //{ src: "<?=base_url()?>assets/images/no-image-listing.jpg"},
                                                <?php
                                         //}
                                        $count++;
                                }
                                if($count < 1) { ?>
                                    { src: "/assets/images/for-sale-img.jpg" }
                         <?php  }
                            } else { ?>

                                     { src: "/assets/images/for-sale-img.jpg" }

                             <?php   } ?>

                         <?php   } ?>

                    ],
                    overlay : true
                });

            });
        });
    </script>

     <?php if($this->uri->segment(1) == "find-a-home" || $this->uri->segment(1) == "listings" || $this->uri->segment(1) == "search-results" || $this->uri->segment(1) == "property-details" || $this->uri->segment(1) == "other-property-details"  || $this->uri->segment(1) == "new_listings" || $this->uri->segment(1) == "nearby_listings" || $this->uri->segment(1) == "office_listings" || $this->uri->segment(1) == "saved_searches") { ?>
       <!-- Mortgage Validation    -->
        <script type="text/javascript">
            $( "#mortgageCalculator" ).validate({
              rules: {
                sale_price: {
                  required: true,
                  number: true
                },
                down_percent: {
                  required: true,
                  number: true
                },
                mortgage_interest_percent: {
                  required: true,
                  number: true
                },
                year_term: {
                  required: true,
                  number: true
                }
              }
            });
        </script>

    <?php } ?>


      <?php if(isset($_GET['is_fb_login'])){ ?>
        <script type="text/javascript">
           window.close();
           window.opener.location.reload(true);
        </script>
    <?php } ?>


      <?php if(isset($_GET['customer_login'])){ ?>
        <script type="text/javascript">
            $(document).ready(function() {
              $("#modalLogin").modal("show");
            });
        </script>
    <?php } ?>

    <script type="text/javascript">

        sbt_send_email = function( eve ){
            eve.preventDefault();
            var url = $(this).attr('action');
            var formData = $(this).serialize();// serializes the form's elements.
            //console.log(formData);
            $("#btn-send").text("Sending...");

            $('.save-loading').show();
            $.ajax({
                url: url,
                type: "POST",
                data: formData,
                dataType: "json",
                beforeSend: function(xhr) {
                  $("#btn-send").prop("disable", true);
                },
                success: function(data) {
                //var jqObj = jQuery(data); // You can get data returned from your ajax call here. ex. jqObj.find('.returned-data').html()

                  if( data.success )
                  {
                      $.msgBox({
                            title: "Success",
                            content: "Email has been successfully sent!",
                            type: "info"
                        });

                      location.reload();
                  }
                  else
                  {
                    $("#btn-send").text("Send");
                    $("#btn-send").prop("disable", false);
                      $.msgBox({
                            title: "Error",
                            content: "Failed to Send Email!",
                            type: "error",
                            buttons: [{ value: "Ok" }]
                        });
                  }
                },
            });
        };

        view_email_content = function (e){
            e.preventDefault();

            var message = $(this).data("message");
              $("#sent-email-content").val(message);

              $("#SentEmailView").modal("show");
        };

        $("#sbt-send-email").on("submit", sbt_send_email);
        $(".email-view").on("click", view_email_content );
    </script>

    <script> var modal_fb_url = <?php echo json_encode($this->facebook->customer_login_url()); ?>; </script>

    <?php
        if($this->uri->segment(1) == "customer-dashboard") {
            if($this->input->get('reset_password')) { ?>
                <script src="/assets/js/plugins/jquery.msgBox.js?version=<?=AS_VERSION?>"></script>
                <script>
                    $(document).ready(function() {
                        activaTab('settings');
                        function activaTab(tab) {
                            $('.nav-tabs a[href="#' + tab + '"]').tab('show');
                            $('#change-phone-modal').modal();
                        };
                    });
                </script>
    <?php   }
        }
    ?>
    <?php
        if($this->uri->segment(1) === "customer-dashboard") {  ?>
                <script>
                      (function() {
                          fakewaffle.responsiveTabs(['xs']);
                      })();
                </script>
    <?php   } ?>

    <script>
        $(document).ready(function() {
            $('a[href="#location"]').on('shown.bs.tab',function(e) {
                setTimeout(initialise, 500);
            });

            function initialise() {

                var location = {
                    lat: <?php echo isset($lat) && '' !== $lat ? $lat : 0 ?>,
                    lng: <?php echo isset($lon) && '' !== $lon ? $lon : 0 ?>
                };

                var myOptions = {
                    zoom:12,
                    scrollwheel: false,
                    center:location,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                };

                var map = new google.maps.Map(document.getElementById('gmap_canvas'), myOptions);

                map.setCenter(location);

                var marker = new google.maps.Marker({
                    map: map,
                    icon: "http://www.google.com/intl/en_us/mapfiles/ms/micons/red-dot.png",
                    position: location
                });

                var infowindow = new google.maps.InfoWindow({
                    content: '<strong><?=(isset($property_standard_info['data']['UnparsedFirstLineAddress'])) ? $property_standard_info['data']['UnparsedFirstLineAddress'] : '';?></strong>'
                });

                google.maps.event.addListener(marker, 'click', function() {
                    infowindow.open(map, marker);
                    map.setCenter(marker.getPosition());
                });

                infowindow.open(map, marker);
            }
        });
    </script>
    <?php if ($this->uri->segment(1) == "page"): ?>
        <script>
            var iframe = document.querySelector("iframe");

            jQuery(window).load(function(){
              jQuery('<script>').attr("src", iframe).appendTo("body");
            });
        </script>        
    <?php endif ?>
  <!-- added ManyChat applet to specific agentsite: eddytherealtyking.com -->
    <?php if ($this->session->userdata('user_id') === 12904): ?>
        <script src="//widget.manychat.com/2064033400547626.js" async="async"></script>   
    <?php endif ?>
    <?php add_hook('before_body_end'); ?>
</body>
</html>
