<!DOCTYPE html>
<html class="no-js" lang="">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <?php 
        if(strpos( base_url(), 'agentsquared.com') !== false){
            echo '<meta name="robots" content="noindex,nofollow,noarchive,nodp"/>';
        }
    ?>
    <title><?php echo (isset($title)) ? $title : 'AgentSquared Signup'; ?></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
    <link rel="icon" type="image/x-icon"  href="<?php if (isset($branding->favicon) AND !empty($branding->favicon)) {?><?php echo AGENT_DASHBOARD_URL . "assets/upload/favicon/$branding->favicon"?><?php } else { ?><?php echo AGENT_DASHBOARD_URL . "assets/images/default-favicon.png"?><?php } ?>"/>
    <link rel="apple-touch-icon" href="/assets/img/home/apple-touch-icon.png">

    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" type="text/css"/> -->
    <link rel="stylesheet" href="/assets/bootstrap/css/bootstrap.min.css?version=<?=AS_VERSION?>">
    <link rel="stylesheet" href="/assets/bootstrap/css/bootstrap-theme.min.css?version=<?=AS_VERSION?>" type="text/css"/>
    <link rel="stylesheet" href="/assets/css/landing_page.css?version=<?=AS_VERSION?>">
    <script src="/assets/js/agent-haven/vendor/modernizr-2.8.3-respond-1.4.2.min.js?version=<?=AS_VERSION?>"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="/assets/fonts/fontello/css/fontello.css?version=<?=AS_VERSION?>">
    <script type="text/javascript">var base_url = "<?php echo base_url(); ?>"; </script>
    <?php  get_ga(); ?>
    <?php add_hook('before_head_end_tag'); ?>
</head>
<body class="sign-up-page">
    <?php add_hook('after_body_start_tag'); ?>

