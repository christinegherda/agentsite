<div class="col-md-3 col-sm-3 invisible-xs">
   <div class="filter-tab">
        <div class="property-agent-info">
            <div class="agent-image">
                <?php
                    if( isset($user_info->agent_photo) AND !empty($user_info->agent_photo)) {

                            $agent_photo = getenv('AWS_S3_ASSETS') . "uploads/photo/".$user_info->agent_photo;
                        ?>

                        <img src="<?php echo $agent_photo ?>" alt="<?=isset($user_info->first_name) ? htmlentities($user_info->first_name) : $account_info->FirstName?> <?=isset($user_info->last_name) ? htmlentities($user_info->last_name) : $account_info->LastName?>"  class="img-thumbnail" width="400">
                <?php
                    } else {
                        if(isset($account_info->Images[0]->Uri) AND !empty($account_info->Images[0]->Uri)) {?>
                            <img src="<?=$account_info->Images[0]->Uri?>" alt="<?=isset($user_info->first_name) ? htmlentities($user_info->first_name) : $account_info->FirstName?> <?=isset($user_info->last_name) ? htmlentities($user_info->last_name) : $account_info->LastName?>" class="img-thumbnail" width="400">
                <?php   } else { ?>
                <img src="<?= base_url()?>assets/images/no-profile-img.gif" alt="No Profile Image">
                <?php
                        }
                    }
                ?>
            </div>
            <h4 class="agent-name">
                <?php echo isset($user_info->first_name) ? htmlentities($user_info->first_name) : $account_info->FirstName;?>
                <?php echo isset($user_info->last_name) ? htmlentities($user_info->last_name) : $account_info->LastName;?>
            </h4>

            <?php

                //Mobile phone
                if(isset($user_info->mobile) AND !empty($user_info->mobile)){?>
                    <p><i class="fa fa-2x fa-mobile" style="font-size: 1.5em;"></i> Mobile: <a href="tel:<?= $user_info->mobile; ?>"><?= $user_info->mobile; ?></a><br/></p>
            <?php
                } else {
                    if(isset($account_info->Phones )) {
                        foreach($account_info->Phones as $phone) {
                            if($phone->Type == 'Mobile') { ?>
                                <p><i class='fa fa-mobile'></i><?=$phone->Type ?> : <a href='tel:<?= $phone->Number; ?>'><?= $phone->Number;?></p>
            <?php
                            }
                        }
                    }
                }

                //Office Phone
                if(isset($user_info->phone) AND !empty($user_info->phone)){?>
                    <p><i class="fa fa-phone-square"></i> Office: <a href="tel:<?= $user_info->phone; ?>"><?= $user_info->phone; ?></a><br/>
            <?php
                } else {
                    if(isset($account_info->Phones)) {
                        foreach($account_info->Phones as $phone) {
                            if($phone->Type == 'Office'){ ?>
                                <p><i class='fa fa-phone-square'></i> <?=$phone->Type; ?> : <a href="tel:<?=$phone->Number; ?>"><?=$phone->Number; ?></a></p>
                <?php            }
                        }
                    }
                }

                //license number shows only when state is California
                if($account_info->Addresses[0]->Region == 'CA') {

                    if(isset($account_info->LicenseNumber) && !empty($account_info->LicenseNumber)){
                      $license_number = $account_info->LicenseNumber;
                      echo "<p><i class='fa fa-certificate'></i> License No.: CAL-BRE#".$license_number."</p>";
                    }
                }


                //FAX
                if(isset($account_info->Phones )) {
                    foreach($account_info->Phones as $phone) {
                        if($phone->Type == 'Fax'){ ?>
                            <p><i class='fa fa-fax'></i> <?=$phone->Type; ?> : <a href="tel:<?=$phone->Number; ?>"><?=$phone->Number; ?></a></p>
                <?php        }

                    }
                }
            ?>

            <p>
                <i class="fa fa-envelope"></i> Email: 
                <a href="<?= ($page=='contact') ? 'mailto:'.$user_info->email : base_url().'contact';?>" style="font-size: 13px;">
                    <?=$user_info->email;?>
                </a>
            </p>

            <?php if($page == "contact" || "testimonials") { ?>
                 <p>
                     
                    <?php if(isset($user_info->address) && !empty($user_info->address)){
                        echo "<i class='fa fa-map-marker'></i> Address: " .$user_info->address;
                    } elseif(isset($account_info->Addresses[0]->Address) && !empty($account_info->Addresses[0]->Address)){
                        echo "<i class='fa fa-map-marker'></i> Address: " .$account_info->Addresses[0]->Address;
                    }?>
                  
                </p>
            <?php }?>
        </div>
    </div>

    <?php
        if($page == "property") {
            
            if(isset($socialmedia_link['instagram']) && !empty($socialmedia_link['instagram'])) {
                $instagram_id = $socialmedia_link['instagram_id'];
            } else {
                $instagram_id = 'agentsquared';
            }
            ?>
            <div class="filter-tab">
                <h4 class="text-center">Share this property to:</h4>
                <hr>
                <!-- AddToAny BEGIN -->
                <div style="position: relative;display: inline-block;">
                    <div class="a2a_kit a2a_kit_size_32 a2a_default_style" style="float: left;width: 148px;">
                        <a class="a2a_button_facebook" href=""></a>
                        <a class="a2a_button_twitter" href=""></a>
                        <a class="a2a_button_linkedin" href=""></a>
                        <a class="a2a_button_email" href=""></a>
                    </div>
                    <div class="a2a_kit a2a_kit_size_32 a2a_default_style a2a_follow" style="float: left;width: 82px;">
                        <a class="a2a_button_instagram" data-a2a-follow="<?= $instagram_id ?>" title="Follow on Instagram"></a>
                    </div>
                </div>

                <!-- AddToAny END -->


<!--                 <ul class="list-inline property-social-share">
                    <li><span class='st_facebook_large' displayText='Facebook'></span></li>
                    <li><span class='st_twitter_large' displayText='Tweet'></span></li>
                    <li><span class='st_linkedin_large' displayText='LinkedIn'></span></li>
                    <li><span class='st_googleplus_large' displayText='Google +'></span></li>
                    <li><span class='st_email_large' displayText='Email'></span></li>
                </ul> -->
            </div>
    <?php
        }

        if($page == "contact") { 

            if($is_premium){?>

                <div class="filter-tab">
                    <h4>You can also contact me through these social media accounts</h4>
                    <hr>
                    <?php
                        if(isset($socialmedia_link['facebook']) AND !empty($socialmedia_link['facebook'])) {
                            $fblink = $socialmedia_link['facebook'];
                        } else {
                            $fblink = "https://www.facebook.com/AgentSquared/";
                        }

                        if(isset($socialmedia_link['twitter']) AND !empty($socialmedia_link['twitter'])) {
                            $twittlink = $socialmedia_link['twitter'];
                        } else {
                            $twittlink = "https://twitter.com/agentsquared";
                        }

                        if(isset($socialmedia_link['instagram']) && !empty($socialmedia_link['instagram'])) {
                            $instagramlink = $socialmedia_link['instagram'];
                        } else {
                            $instagramlink = "https://www.instagram.com/agentsquared";
                        }

                        if(isset($socialmedia_link['linkedin']) AND !empty($socialmedia_link['linkedin'])) {
                            $lnlink = $socialmedia_link['linkedin'];
                        } else {
                            $lnlink = "https://www.linkedin.com/company/agentsquared";
                        }
                    ?>
                    <ul class="list-inline property-social-share">
                        <?php if($socialmedia_link['agent_email']) : ?><li><a href="mailto:<?php echo $socialmedia_link['agent_email']; ?>"><i class="fa fa-envelope"></i></a></li><?php endif; ?>
                        <li><a href="<?php echo $fblink; ?>" target="_blank"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="<?php echo $twittlink; ?>" target="_blank"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="<?php echo $instagramlink; ?>" target="_blank" title="Instagram"><i class="fa fa-instagram"></i></a></li>
                        <li><a href="<?php echo $lnlink; ?>" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                    </ul>
                </div>
    <?php  }
        }

        if($page == "property") {
            if(isset($account_info->Mls) && $account_info->Mls == "MLS BCS") {
            
            } else { ?>
                <div class="filter-tab">
                    <div class="property-submit-question">
                        <h4 class="text-center">Mortgage Calculator</h4>
                        <hr>
                        <div id="mortgage_msg"></div>
                        <form  method="post" id="mortgageCalculator">
                            <div class="form-group">
                                <label for="sale_price">Price of Home ($<span id="sale_price_label"></span>)</label>
                                <input type="text" name="sale_price" id="sale_price" value="<?php echo $this->session->flashdata('mortgage_price_value'); ?>" maxlength="15" min="1" class="form-control" required>
                                <input type="hidden" name="calculator_url" value="<?php echo base_url();?>home/mortgage_calculator">
                            </div>
                            <div class="form-group">
                                <label for="down_percent">Downpayment (<span id="down_percent_label"></span>%)</label>
                                <input type="text" name="down_percent" id="down_percent" value="<?php echo $this->session->flashdata('mortgage_downpayment_value'); ?>" maxlength="4" min="1" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label for="mortgage_interest_percent">Annual Interest Rate (<span id="mortgage_interest_percent_label"></span>%)</label>
                                <input type="text" name="mortgage_interest_percent" id="mortgage_interest_percent" value="<?php echo $this->session->flashdata('mortgage_interest_rate_value'); ?>" maxlength="4" min="1" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label for="year_term">Loan Term (<span id="year_term_label"></span>  years)</label>
                                <input type="text" name="year_term" id="year_term" value="<?php echo $this->session->flashdata('mortgage_loan_terms_value'); ?>" maxlength="4" min="1" class="form-control" required>
                            </div>
                            <button class="btn btn-default btn-block submit-button" id="morg_calc_btn">Calculate <i style="display: none;" class="save-loading fa fa-spinner fa-spin"></i></button>
                            <div id="mortgage-result">
                                   <?php echo $this->session->flashdata('mortgage_monthly'); ?>
                                   <!-- <?php echo $this->session->flashdata('mortgage_monthly_interest'); ?>
                                   <?php echo $this->session->flashdata('mortgage_annual_interest'); ?>
                                   <?php echo $this->session->flashdata('mortgage_downpayment'); ?>
                                   <?php echo $this->session->flashdata('mortgage_amount_financed'); ?> -->
                            </div>
                        </form>
                    </div>
                </div>
    <?php 
            }
        }
    ?>
</div>
