<?php ob_start(); ?>
<script type="text/javascript">
	(function ($) {
		var script = {
			checkLocation: function () {
				var self = this;
				this.location.latitude = "<?= isset($location['latitude']) && '' !== $location['latitude'] ? $location['latitude'] : '' ?>";
				this.location.longitude = "<?= isset($location['longitude']) && '' !== $location['longitude'] ? $location['longitude'] : '' ?>";

				if ( '' === this.location.latitude || '' === this.location.longitude ) {
					console.log(`Location cannot be found, looking up geolocation from the supplied address "${self.address}"`);

					(function () {
						var geocoder = new google.maps.Geocoder();
						geocoder.geocode({ 'address': self.address }, function (results, status) {
							if ( status === google.maps.GeocoderStatus.OK ) {
								self.location.latitude = results[0].geometry.location.lat();
								self.location.longitude = results[0].geometry.location.lng();
							}
						})
					})();
				}

				return this.location;
			},
			init: function () {
				this.address = "<?= $address?>";
				this.location = {
					latitude: '',
					longitude: ''
				};
				this.checkLocation();

				var self = this;
				var buttonmap = document.getElementById('buttonmap');
				var map_canvas = document.getElementById('map_canvas');

				var showMap = function () {
					buttonmap.setAttribute('disabled', 'disabled');

					if ( self.location.latitude === '' || '' === self.location.longitude ) {
						buttonmap.style.display = 'none';
					}

					var map, marker,
						myOptions = {
						zoom: 16,
						scrollwheel: false,
						center: new google.maps.LatLng(self.location.latitude, self.location.longitude),
						mapTypeId: google.maps.MapTypeId.ROADMAP
					};

					map = new google.maps.Map(map_canvas, myOptions);

					// position of marker
					google.maps.event.addListenerOnce(map, 'idle', function() {
						this.getDiv().style.width = '100%';
						this.panBy(-(this.getDiv().offsetWidth/3),0);
						google.maps.event.trigger(this, 'resize');
					});

					marker = new google.maps.Marker({
						map: map,
						icon: '<?=$protocol;?>maps.google.com/mapfiles/ms/icons/red.png',
						position: new google.maps.LatLng(self.location.latitude, self.location.longitude),
						title: self.address,
						address: self.address
					});

					google.maps.event.addListener(marker, 'click', function() {
						map.setZoom(8);
						google.maps.event.addListenerOnce(map, 'idle', function() {
							this.getDiv().style.width = '100%';
							this.panBy(-(this.getDiv().offsetWidth/3),0);
							google.maps.event.trigger(this, 'resize');
						})
					});

					buttonmap.style.display = 'none';
				};
				buttonmap.addEventListener('click', showMap, false);
			}
		};

		$(document).ready(function () {
			script.init();
		})
	})(jQuery)
</script><?php
$script = ob_get_contents();
ob_clean();

hook('before_body_end', function () use ($script) {
	echo $script;
});
?>
<div class="page-content">
    <section class="property-detail-content">
        <div class="container">
            <div class="row">
            	<!-- Sidebar Area -->
    			<?php $this->load->view($theme."/session/sidebar"); ?>

				<div class="col-md-9 col-sm-9 agent-map">
					<div class="buyer-blurb">
                        <?=(isset($page_data) && !empty($page_data)) ? $page_data['content'] : "";?>
                    </div>
                    <div class="map-section-contact">
                    	<button type="button" id="buttonmap" class="btn submit-button mb-20" style="margin-left: 8px;">Show map location</button>
					    <div id="map_canvas" class="col-md-12 col-sm-12"></div>
					    <style type="text/css">
					      #map_canvas {
					        height: 850px;
					        width: 100%;
					        margin-top: 0px;
					        padding: 0px;
					      }
					    </style>
						<div class="col-md-12 col-sm-12 col-md-offset-0 property-details contac-form-wrapper">
						 	<h3>CONTACT ME  </h3>
						  	<div class="question-mess"></div><br/>
							<form action="<?php echo site_url('home/home/customer_questions'); ?>" method="POST" class="customer_questions" id="customer_questions" >
								<div class="form-group">
									<label for="">First Name</label>
									<input type="text" name="first_name" value="<?= (isset($profile->first_name)) ? $profile->first_name : "" ; ?>" class="form-control" placeholder="First Name" maxlength="50" pattern="[a-zA-Z0-9-_.\s]+" title="Allowed characters: A-Z, 0-9, ., -, _" required> 
								</div>
								<div class="form-group">
									<label for="">Last Name</label>
									<input type="text" name="last_name" value="<?= (isset($profile->last_name)) ? $profile->last_name : "" ; ?>" class="form-control" placeholder="Last Name" maxlength="50" pattern="[a-zA-Z0-9-_.\s]+" title="Allowed characters: A-Z, 0-9, ., -, _" required>
								</div>
								<div class="form-group">
								   	<label for="">Email</label>
								    <input type="email" name="email" value="<?= (isset($profile->email)) ? $profile->email : "" ; ?>" class="form-control" placeholder="Email" maxlength="50" required <?=(isset($profile->email)) ? "readonly" : "";?>>
								</div>
								<div class="form-group">
								   	<label for="">Phone</label>
								    <input type="phone" name="phone" value="<?= (isset($profile->phone)) ? $profile->phone : "" ; ?>" class="form-control phone_number" placeholder="Phone Number" required <?=(isset($profile->phone)) ? "readonly" : "";?>>
								</div>
								<div class="form-group">
									<label for="">Message</label>
									<textarea name="message" id="" value="" cols="30" rows="5" class="form-control" placeholder="Comments, Questions, Special Requests?" required></textarea>
								</div>
								<div class="form-group">
									<div class="g-recaptcha contact-captcha" data-sitekey="6Lf9eRsUAAAAAB5lP4Em8vm4L-c1m0gSyAKOTort"></div>
								</div>
								<button type="submit" class="btn btn-default btn-block submit-button submit-question-button">Submit</button>
							</form>
						</div>
                    </div>
				</div>
			</div>
		</div>
	</section>
</div>
