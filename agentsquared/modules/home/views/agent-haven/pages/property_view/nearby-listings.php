        <div class="sold-property-container">
        <?php 
            if(isset($nearby_listings['status']) && $nearby_listings['status'] == 200) {
                foreach($nearby_listings['data'] as $nearby) { ?>
                    <div class="other-listing-item">
                        <div class="other-related-items">
                        <?php if(isset( $nearby['Photos']['Uri300'])){?>
                             <a href="<?= base_url(); ?>other-property-details/<?=$nearby['StandardFields']['ListingKey'];?>">
                                <img src="<?php echo $nearby['Photos']['Uri300']; ?>" alt="<?= $nearby['StandardFields']['UnparsedFirstLineAddress'];?>" class="img-responsive">
                            </a>
                        <?php } else {?>
                            <a href="<?= base_url(); ?>other-property-details/<?=$nearby['StandardFields']['ListingKey'];?>">
                                <img src="<?=base_url()?>assets/images/image-not-available.jpg" alt="<?= $nearby['StandardFields']['UnparsedFirstLineAddress'];?>" class="img-responsive" style="width:100%;">
                            </a>
                       <?php }?>
                        </div>
                        <div class="other-related-description">
                            <div class="nearby-listing-type">
                               <?php echo $nearby['StandardFields']['PropertyClass']; ?>
                            </div>
                            <p><a href="<?= base_url(); ?>other-property-details/<?=$nearby['StandardFields']['ListingKey'];?>"><?php echo $nearby['StandardFields']["UnparsedFirstLineAddress"]; ?></a></p>
                            <p><i class="fa fa-map-marker"></i>
                                <?php

                                    if($account_info->Mls != "MLS BCS"){
                                        $mystring = $nearby['StandardFields']['City'];
                                        $postalcode = $nearby['StandardFields']['PostalCode'];
                                        $findme   = '*';
                                        $pos = strpos($mystring, $findme);
                                        $pos_code = strpos($postalcode, $findme);

                                        if($pos === false){
                                            if($pos_code === false){
                                                echo $nearby['StandardFields']['City'] . ", " . $nearby['StandardFields']['StateOrProvince'] . " " . $nearby['StandardFields']['PostalCode'];
                                            }else{
                                                 echo $nearby['StandardFields']['City'] . ", " . $nearby['StandardFields']['StateOrProvince'];   
                                            }
                                            
                                        }
                                        else{
                                            
                                            if($pos_code === false){
                                                echo $nearby['StandardFields']['PostalCity'] . ", " . $nearby['StandardFields']['StateOrProvince'] . " " . $nearby['StandardFields']['PostalCode'];
                                            }else{
                                                echo $nearby['StandardFields']['PostalCity'] . ", " . $nearby['StandardFields']['StateOrProvince'];
                                            }

                                        }
                                    }else{
                                        $mystring = $nearby['StandardFields']['City'];
                                        $StateOrProvince = $nearby['StandardFields']['StateOrProvince'];
                                        $findme   = '*';
                                        $pos = strpos($mystring, $findme);
                                        $pCode = strpos($StateOrProvince, $findme);

                                        if($pos === false && $pCode == false)
                                            echo $nearby['StandardFields']['City'];
                                        else
                                            echo $nearby['StandardFields']['PostalCity'] . ", " . $nearby['StandardFields']['StateOrProvince'];
                                    }
                                ?>
                                
                            </p>
                            <p><i class="fa fa-usd"></i><?php echo number_format($nearby['StandardFields']['CurrentPrice']); ?></p>
                        </div>
                    </div>
        <?php
                }
            } else { ?>
                <div class="alert alert-info text-center">No Nearby Listings Found!</div>
        <?php
            }
        ?>      
        </div>