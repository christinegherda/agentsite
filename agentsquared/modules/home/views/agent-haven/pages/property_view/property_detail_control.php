<?php
    if(!empty($property_standard_info["data"])){
    $detail = $property_standard_info["data"];
?>
<div class="property-title-wrapper">

    <h1 class="property-title"><?php echo $detail["UnparsedFirstLineAddress"]; ?>

    <?php if($account_info->Mls != "MLS BCS"){ ?>
        <span class="property-address">
        <?php $pos_code = strpos($detail["PostalCode"], "*"); ?>
        <?php echo $detail["City"]; ?>, <?php echo $detail["StateOrProvince"]; ?> <?php echo ($pos_code === false) ? $detail["PostalCode"] : ""; ?>
        
    </span></h1>
    <?php }else{ ?>
        <span class="property-address"><?php echo $detail["City"]; ?></span></h1>
    <?php } ?>

    <h5 class="property-bedsbathstotal">
        <ul>
           
            <?php if($detail["PropertyClass"] == "Residential" || $detail["PropertyClass"] == "Rental") { ?>

                <?php if(isset($detail["BedsTotal"]) && !empty($detail["BedsTotal"])){
                    if(($detail["BedsTotal"] != "********")){?>
                        <li><i class="fa fa-bed"></i> <?=$detail["BedsTotal"]?> Bed</li>
                    <?php } else{?>
                        <li><i class="fa fa-bed"></i> N/A</li>
                    <?php } ?>
                <?php  } else { ?>
                    <li><i class="fa fa-bed"></i> N/A</li>
                <?php } ?>

                <?php if($account_info->Mls != "MLS BCS"){ ?>
                          <?php if(isset($detail["BathsTotal"]) && !empty($detail["BathsTotal"])){
                            if(($detail["BathsTotal"] != "********")){ ?>
                                <li><i class="icon-toilet"></i> <?=$detail["BathsTotal"]?> Bath</li>
                          <?php } else if($detail["BathsFull"] != "********"){?>
                                <li><i class="icon-toilet"></i> <?=$detail["BathsFull"]?> Bath </li>
                          <?php } else{?>
                                <!-- <li><i class="icon-toilet"></i> N/A</li> -->
                          <?php } ?>

                     <?php  } else {?>
                       <li><i class="icon-toilet"></i> N/A</li>
                    <?php }?>
                <?php }else{ 
                    if(isset($detail["BathsFull"]) && !empty($detail["BathsFull"])){
                            if(($detail["BathsFull"] != "********")){?>
                                <li><i class="icon-toilet"></i> <?=$detail["BathsFull"]?> Full Baths <br /><i class="icon-toilet"></i><?=$detail["BathsHalf"]?> Half Baths</li>
                           <?php } else{?>
                                <!-- <li><i class="icon-toilet"></i> N/A</li> -->
                           <?php } ?>

                     <?php  } else {?>
                          <!--  <li><i class="icon-toilet"></i> N/A</li> -->
                    <?php }?>
                <?php }?>
            <?php }else if($detail["PropertyClass"] == "MultiFamily") {?>

                    <?php if($account_info->Mls != "MLS BCS"){ ?>
                         <?php if(isset($detail["BathsTotal"]) && !empty($detail["BathsTotal"])){
                            if(($detail["NumberOfUnitsTotal"] != "********")){?>
                                <li><i class="fa fa-university"></i> <?=$detail["NumberOfUnitsTotal"]?> No. of Units</li>
                           <?php } else{?>
                                <li><i class="fa fa-university"></i> N/A</li>
                           <?php } ?>

                     <?php  } else {?>
                       <li><i class="icon-toilet"></i> N/A</li>
                    <?php }?>
                <?php }else{ 
                    if(isset($detail["BathsFull"]) && !empty($detail["BathsFull"])){
                            if(($detail["BathsFull"] != "********")){?>
                                <li><i class="icon-toilet"></i> <?=$detail["BathsFull"]?> Full Baths <br /><i class="icon-toilet"></i><?=$detail["BathsHalf"]?> Half Baths</li>
                           <?php } else{?>
                                <li><i class="icon-toilet"></i> <?=$detail["NumberOfUnitsTotal"]?> No. of Units</li>
                           <?php } ?>

                     <?php  } else {?>
                          <!--  <li><i class="icon-toilet"></i> N/A</li> -->
                    <?php }?>
                <?php }?>
            <?php }?>
            <?php
                if(!empty($detail["BuildingAreaTotal"]) && ($detail["BuildingAreaTotal"] != "0")   && is_numeric($detail["BuildingAreaTotal"])) {?>

                    <li class="lot-item"><?=number_format($detail["BuildingAreaTotal"])?> sqft</li>

            <?php } elseif(!empty($detail["LotSizeArea"]) && ($detail["LotSizeArea"] != "0")   && is_numeric($detail["LotSizeArea"])) {

                    if(!empty($detail["LotSizeUnits"])  && ($detail["LotSizeUnits"] === "Acres") ){?>

                        <li class="lot-item">Lot Size: <?=number_format($detail["LotSizeArea"], 2, '.', ',')?> acres</li>

                    <?php } else { ?>

                        <li class="lot-item">Lot Size: <?=number_format($detail["LotSizeArea"])?> acres</li>

                    <?php } ?> 

             <?php } elseif(!empty($detail["LotSizeSquareFeet"]) && ($detail["LotSizeSquareFeet"] != "0")   && is_numeric($detail["LotSizeSquareFeet"])) {?>

                    <li class="lot-item">Lot Size: <?=number_format($detail["LotSizeSquareFeet"])?> sqft
                    </li> 


             <?php } elseif(!empty($detail["LotSizeAcres"]) && ($detail["LotSizeAcres"] != "0")   && is_numeric($detail["LotSizeAcres"])) {?>

                    <li class="lot-item">Lot Size: <?=number_format($detail["LotSizeAcres"],2 ,'.',',')?> acres</li>

            <?php } elseif(!empty($detail["LotSizeDimensions"]) && ($detail["LotSizeDimensions"] != "0")   && ($detail["LotSizeDimensions"] != "********")) {?>

                    <li class="lot-item">Lot Size: <?=$detail["LotSizeDimensions"]?></li>
            <?php } else {?>
                    <li class="lot-item">Lot Size: N/A</li>
            <?php } ?>
        </ul>
    </h5>

    <small><a  href="https://www.google.com/maps/dir//<?php echo urlencode($detail["UnparsedAddress"]); ?>" target="_blank"  style="color: #3498db;">Get Directions <span class="get-directions"><img src="<?=base_url()."assets/img/home/get-direction.png"?>" width="15px" height="15px" alt="Google Maps Direction"></span></a></small>
    <h2 class="property-info">
      <span class="property-type"><?php echo $detail["PropertyClass"]; ?></span>-<span class="p-status"><?php echo $detail["MlsStatus"]; ?></span>
      <span class="property-price"><?php echo '$ '.number_format($detail["CurrentPrice"]); ?></span>

    <?php if(isset($detail["OnMarketDate"]) && $detail["OnMarketDate"]!="********"){

          $onmarketdate = $detail["OnMarketDate"];
          $today = time();
          $onmarketdate= strtotime($onmarketdate);
          $hours =  round(abs($today-$onmarketdate)/60/60);

         if($hours <= "24"){?>
            <p class="prop-listing-date-detail">Listed <?=($hours == 0) ? 1 : $hours ?> <?=($hours == 1 || $hours == 0) ? "hour" :"hours" ?> ago!</p> 
         <?php }
     }?>
    </h2>
</div>

<div class="property-sold">
    <?php  if($detail["MlsStatus"] == 'Sold' || $detail["MlsStatus"] == 'Closed') { ?>
    <img src="<?php echo AGENT_DASHBOARD_URL ?>assets/images/SOLD.png" alt="Sold Status" class="img-responsive detail-sold-banner">
      <?php if(isset($detail["CloseDate"])){?>
          <p><i class="fa fa-calendar"></i> Sold Date: <?=date("m-d-Y", strtotime($detail["CloseDate"]))?></p>
      <?php  } ?>
    <?php } ?>
</div>

<div class="property-detail-wrapper">
    <?php if(strpos($mlsLogo, 'http') !== false) { ?>
        <p class="idx-logo" style="background-image: url('<?php echo $mlsLogo; ?>');"><img src="<?php echo $mlsLogo; ?>" alt="IDX Logo" class="mlsLogo"/></p>
    <?php } else { ?>
        <h4><?php echo $mlsName; ?></h4>
    <?php } ?>
    
    <?php if(isset($detail["ListingId"]) AND strpos($detail["ListingId"],'*') === false){?>
        <h2 style="margin: 0 0 5px;font-size: 18px;"><strong>MLS#:</strong> <small><?php echo $detail["ListingId"]; ?></small></h2>
    <?php }?>

    <p><strong>Courtesy of:</strong> 
        <small>
            <?php 
                $hasName=FALSE;
                if(isset($detail["ListAgentFirstName"]) && strpos($detail["ListAgentFirstName"], '*') === false && isset($detail["ListAgentLastName"]) && strpos($detail["ListAgentLastName"], '*') === false) {
                    $hasName=TRUE;
                    echo $detail["ListAgentFirstName"]." ".$detail["ListAgentLastName"];
                }
                if(isset($detail["ListOfficeName"]) && strpos($detail["ListOfficeName"], '*') === false) {
                    echo ($hasName) ?  ", ".$detail["ListOfficeName"] : $detail["ListOfficeName"];
                }
            ?>
        </small></p>


     <ul class="listing-icons list-inline">
        <li>
        <?php  
            $property_saved = FALSE;

            if(isset($_SESSION["save_properties"])) {
                foreach($_SESSION["save_properties"] as $key):
                    if($key['property_id'] == $property_id) : 
                        $property_saved = TRUE; 
                        break; 
                    endif;
                endforeach;
            }
        ?>
            <a href="#" data-action="<?=site_url('home/home/save_favorates_properties/'.$property_id);?>" data-pro-id="<?=$property_id?>" data-property-name="<?=$detail["UnparsedAddress"];?>" class="save-favorate <?=($property_saved) ? 'save-fave-property' : '';?>" data-title="Tooltip" data-trigger="hover" title="<?=($property_saved) ? "Saved Property" : "Save Property";?>">
                <span id="isSaveFavorate_<?=$property_id?>">
                    <?=($property_saved) ? "<i class='fa fa-2x fa-heart'></i>" : "<i class='fa fa-2x fa-heart-o'></i>";?>
                </span>
            </a>

            <p id="isSavedFavorate_<?=$property_id?>"><?php echo ($property_saved) ? "<small>Saved</small>" : "<small>Save</small>" ;?></p>
        </li>
        <li>
            <?php if(!isset($_SESSION["customer_id"])) {?>

                <p><a href='javascript:void(0)' class="sched-modal modalsignup" data-toggle='modal'  data-target='.customer_login_signup' data-pro-id="<?=$property_id?>" data-property-type="<?= ($isMyProperty) ? 'featured_property' : 'other_property'; ?>" data-title='Tooltip' data-trigger='hover' title='Schedule for Showing' ><i class='fa fa-2x fa-calendar'></i></a></p>
            
           <?php } else { ?>  

                <p><a href='#schedModal'  class='sched_modal' data-toggle='modal' data-title='Tooltip' data-trigger='hover' title='Schedule for Showing'><i class='fa fa-2x fa-calendar'></i></a></p>

             <?php } ?>

            <p><small>Showing</small></p>
        </li>
        <li>
            <p>
            <?php if(!isset($_SESSION['customer_id']) && $is_capture_leads) { ?>
                <a href="#" type="button" class="featured_listing modalogin" data-toggle="modal" data-target=".customer_login_signup" data-listingId="<?php echo str_replace(" ",  "-", $detail['ListingKey']); ?>" data-propertyType="<?= ($isMyProperty) ? 'featured_property' : 'other_property'; ?>" data-title='Tooltip' data-trigger='hover' title='Request Information'>
                <i class="fa  fa-2x fa-question-circle"></i></a>
            <?php } else { ?>
                <a href="#" type="button" class="request_info" data-toggle="modal" data-target="#request-info-modal" data-listingId="<?php echo str_replace(" ",  "-", $detail['ListingKey']); ?>" data-propertyType="<?= ($isMyProperty) ? 'featured_property' : 'other_property'; ?>" data-address="<?=$detail['UnparsedAddress'];?>" data-title='Tooltip' data-trigger='hover' title='Request Information'><i class="fa fa-2x fa-question-circle"></i></a>
            <?php }?>
            </p>
            <p><small>Info</small></p>
        </li>
        <!-- Insert Virtual tour -->
        <li>
            <div id="has-vtour"></div>
        </li>
    </ul>
</div>
<?php }?>
