<?php

if(!empty($photos)) {
    $photo = json_decode($photos);
    if(!isset($photo->response)) {
        if( isset($photo->data) && !empty($photo->data)){

            $photoD = $photo->data;
            $photoData = $photoD[0]->StandardFields->Photos;

            if(!empty($photoData)) {?>
                <div id="photosync1" class="owl-carousel photo-preview">
                    <?php for ($i = 0; $i < count($photoData); $i++) { ?>
                        <?php if (isset($property_standard_info["data"]) && !empty($property_standard_info["data"])) {
                            $altag = $property_standard_info["data"]["UnparsedFirstLineAddress"]; 
                        } else {
                            if (isset($photoData[$i]->Name) && !empty($photoData[$i]->Name)) {
                                $altag = $photoData[0]->Name;
                            } else {
                                $altag = "";
                            } 
                        } ?>
                        <div class="item"  style="background-image: url('<?php echo $photoData[$i]->Uri640; ?>');">
                            <img src="<?php echo $photoData[$i]->Uri640; ?>" alt="<?=$altag;?>" class=""> 
                        </div>
                    <?php } ?>
                </div>
                <div id="photosync2" class="owl-carousel owl-photo-thumbnail">
                    <?php for ($i = 0; $i < count($photoData); $i++) { ?>
                        <?php if (isset($property_standard_info["data"]) && !empty($property_standard_info["data"])) {
                            $altag = $property_standard_info["data"]["UnparsedFirstLineAddress"]; 
                        } else {
                            if (isset($photoData[$i]->Name) && !empty($photoData[$i]->Name)) {
                                $altag = $photoData[0]->Name;
                            } else {
                                $altag = "";
                            } 
                        } ?>
                        <div class="item"  style="background-image: url('<?php echo $photoData[$i]->Uri640; ?>');">
                            <img src="<?php echo $photoData[$i]->Uri640; ?>" alt="<?=$altag;?>" class=""> 
                        </div>
                    <?php } ?>
                </div>
            <?php
                } elseif(isset($photoData[0]->Uri640)) { ?>
                    <div class="no-image-available">
                        <img src="<?php echo $photoData[0]->Uri640; ?>" alt="<?=isset($photoData[0]->Name) ? $photoData[0]->Name : ""?>" class="">
                    </div>
            <?php
                } else { ?>
                    <div class="no-image-available">
                        <img src="<?=base_url()."assets/images/image-not-available.jpg"?>" alt="Image not Available" class="">
                    </div>
            <?php
                }
        } else{?>
             <p class="text-center">No Listings Data Available!</p>
        <?php }

    } else { ?>

        <p class="text-center">The resource is not available at the current API key's service level.</p>
<?php
    }
}else{?>
    <p class="text-center">The resource is not available at the current API key's service level.</p>
<?php }?>
