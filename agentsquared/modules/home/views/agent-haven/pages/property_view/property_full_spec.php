<div class="row">
<?php
if(!empty($property_standard_info["data"])){ ?>
    <!-- Schedule for Showing Modal -->
    <div id="schedModal" class="modal fade" data-backdrop="static">
        <div class="modal-dialog modal-virtual-tour">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">X</span></button>
                    <h4>Schedule For Showing</h4>
                </div>
                <div class="description sched-modal-section">
                    <!-- <div id="resultInsert" class="alert alert-success" style="display:none"></div> -->
                    <span id="resultInsert"></span>
                    <form action="<?php echo base_url("home/home/save_schedule_for_showing"); ?>" method="POST" class="sched_showing" >
                        <input type="hidden" name="property_id" value="<?php echo $property_standard_info["property_id"]; ?>"/>
                        <input type="hidden" name="property_name" value="<?php echo $property_standard_info["data"]["UnparsedFirstLineAddress"]; ?>"/>
                        <div class="row">
                            <div class="col-md-6 col-xs-6">
                                <label>Date schedule</label>
                                <div class="input-group date">
                                    <input type="text" name="date_scheduled" class="form-control" id="datepickersched" placeholder="Date schedule" required>
                                    <div id="datepickersched" class="input-group-addon">
                                        <i class="fa fa-calendar open-datetimepicker"></i>
                                    </div>
                                </div><br>
                            </div>
                        </div>
                        <textarea name="sched_for_showing" class="form-control" rows="10">I'd like to schedule a showing for <?php echo $property_standard_info["data"]["UnparsedFirstLineAddress"]; ?></textarea>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-default submit-button" id="sbt-showing" >Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<ul class="nav nav-tabs detail-tabs" role="tablist">
    <li class="view-full-details active" role="presentation" class="active"><a href="#property-detail" aria-controls="home" role="tab" data-toggle="tab">Property Details</a></li>
    <?php if(isset($account_info->Mls) && $account_info->Mls != "MLS BCS") { ?>
        <li role="presentation"><a href="#location" role="tab" data-toggle="tab">Map <br>&nbsp;</a></li>
    <?php }else{ ?>
        <li role="presentation"><a href="#location_new" role="tab" data-toggle="tab">Map <br>&nbsp;</a></li>
    <?php }?>
    <li class="view-full-spec" role="presentation"><a href="#fullspec" role="tab" data-toggle="tab">Full Specifications</a></li>
    <!-- <li role="presentation"><a href="#school" role="tab" data-toggle="tab">Nearby <br>&nbsp;</a></li> -->
</ul>

<div class="tab-content">
    <div role="tabpanel" class="tab-pane" id="getDirection">
        <div class="description property-detail-sections">
            <h4>Get Direction</h4>
            <div style='overflow:hidden;height:450px;width:100%;'>
                <input id="origin-input" class="controls" value="" type="text" placeholder="Enter an origin location">
                <input id="destination-input" value="<?php echo $property_standard_info["data"]["UnparsedFirstLineAddress"]; ?>" class="controls" type="text" placeholder="Enter a destination location">
                <div id="mode-selector" class="controls" style="display:none">
                    <input type="radio" name="type" id="changemode-walking" checked="checked">
                    <label for="changemode-walking">Walking</label>

                    <input type="radio" name="type" id="changemode-transit">
                    <label for="changemode-transit">Transit</label>

                    <input type="radio" name="type" id="changemode-driving">
                    <label for="changemode-driving">Driving</label>
                </div>

                <div id="map"></div>


                 <div id='gmap_directions' style='height:450px;width:100%;'></div>
                 <style>
                #gmap_directions img {
                         max-width: none!important;
                         background: none!important
                    }

                .controls {
                        margin-top: 10px;
                        border: 1px solid transparent;
                        border-radius: 2px 0 0 2px;
                        box-sizing: border-box;
                        -moz-box-sizing: border-box;
                        /*height: 32px;*/
                        outline: none;
                        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
                      }

                      #origin-input,
                      #destination-input {
                        background-color: #fff;
                        font-family: Roboto;
                        font-size: 15px;
                        font-weight: 300;
                        margin-left: 12px;
                        padding: 0 11px 0 13px;
                        text-overflow: ellipsis;
                        width: 200px;
                      }

                      #origin-input:focus,
                      #destination-input:focus {
                        border-color: #4d90fe;
                      }

                      #mode-selector {
                        color: #fff;
                        background-color: #4d90fe;
                        margin-left: 12px;
                        padding: 5px 11px 0px 11px;
                      }

                      #mode-selector label {
                        font-family: Roboto;
                        font-size: 13px;
                        font-weight: 300;
                        padding: 5px;
                      }
                </style>
            </div>
        </div>
    </div>

    <div role="tabpanel" class="tab-pane fade in active" id="property-detail">
        <div class="description property-detail-sections">
            <h4>Description</h4>
            <p>
                <?php echo $property_standard_info['data']['PublicRemarks']; ?>
                <?php echo isset($property_standard_info['data']['Supplement']) ? $property_standard_info['data']['Supplement'] : "" ?>
            </p>
        </div>
        <div class="specifications property-detail-sections">
            <h4>Specifications</h4>
            <div class="row">
                <?php
                    /*for($i=0; $i<3; $i++) { ?>
                        <div class="col-md-4 col-sm-12">
                            <ul class="property-detail-list">
                            <?php if(!empty($property_full_spec['groups'])){?>
                                <br/><h4 class='panel-title'><strong><?php echo $property_full_spec['groups'][$i]['group']; ?></strong></h4><br/>
                                <?php
                                    $count=0;
                                    foreach($property_full_spec['groups'][$i]['fields'] as $fields) {
                                        if(!empty($fields['Value'])){
                                            if($fields['Label'] && $fields['Value']) {
                                                if($count<5) { ?>
                                                    <?php if( isset($fields['Label']) && isset($fields['Value']) ) { ?>
                                                        <li>
                                                            <label><?php echo $fields['Label']; ?>: </label>
                                                            <!--<span><?php echo (!empty($fields['Value'])) ? $fields['Value'] : ""; ?></span>-->
                                                            <span>
                                                                <?php if( !is_array($fields['Value']) ) { ?>
                                                                    <span><?php echo (!empty($fields['Value'])) ? $fields['Value'] : ""; ?></span>
                                                                <?php } else { ?>
                                                                    <?php foreach( $fields['Value'] as $keyO => $valO ) { ?>
                                                                        <span><?php echo (!empty($keyO)) ? $keyO : ""; ?></span>
                                                                        <span><?php echo (!empty($valO)) ? $valO : ""; ?></span>
                                                                    <?php } ?>
                                                                <?php } ?>
                                                            </span>
                                                        </li>
                                                    <?php } ?>
                                    <?php
                                                }
                                                $count++;
                                            }
                                        }
                                    }
                                }
                                ?>
                            </ul>
                        </div>
                <?php
                    }*/
                ?>
                <?php
                    if(!empty($property_full_spec['data'])) {
                        $counter=1;
                        foreach ($property_full_spec['data']['Main'] as $key => $value) { ?>
                            <?php if($counter <= 3) { ?>
                                <div class="col-md-4 col-sm-12">
                                <?php foreach ($value as $key2 => $value2) {
                                     $count=0;
                                     ?>

                                     <ul class="property-detail-list">
                                            <br/><h4 class='panel-title'><strong><?php echo $key2; ?></strong></h4><br/>
                                            <?php
                                                foreach ($value2 as $key3 => $value3) {
                                                    foreach ($value3 as $key4 => $value4) {
                                                        if($count<5) { ?>
                                                            <li>
                                                                <label><?php echo $key4; ?>: </label>
                                                                <span><?php echo $value4; ?></span>
                                                            </li>

                                                        <?php } ?>
                                                <?php $count++;
                                                    }
                                                }
                                            ?>
                                        </ul>
                                <?php } $counter++ ?>
                                 </div>
                            <?php } ?>
                       <?php } ?>
                <?php } ?>
            </div>
        </div>
        <div class="view-full" role="presentation"><a href="#fullspec" role="tab" data-toggle="tab">View Full Specifications</a></div>
    </div>

    <div role="tabpanel" class="tab-pane" id="location">
        <div class="location-info property-detail-sections">
            <h4>Map View</h4>
            <div style='overflow:hidden;height:450px;width:100%;'>
                <div id='gmap_canvas' style='height:450px;width:100%;'></div>
                <style>
                    #gmap_canvas img {
                        max-width: none!important;
                        background: none!important
                    }
                </style>
            </div>            
        </div>
    </div>

    <div role="tabpanel" class="tab-pane" id="location_new">
        <div class="location-info property-detail-sections">
            <h4>Map View</h4>
            <div style='overflow:hidden;height:450px;width:100%;'>
                <div id='gmap_canvas_cabo' style='height:450px;width:100%;'></div>
                <style>
                    #gmap_canvas_cabo img {
                        max-width: none!important;
                        background: none!important
                    }
                </style>
            </div>
            
        </div>
    </div>

    <div role="tabpanel" class="tab-pane" id="fullspec">
        <div class="property-detail-sections">
            <ul class="property-detail-list">
                <div class="panel-group" id="accordion1">
                    <?php if(!empty($property_full_spec['data'])) {
                            $count=0;
                            $i=0;
                           foreach ($property_full_spec['data']['Main'] as $key => $value) {
                                foreach ($value as $key2 => $value2) {
                                    $count++;
                                    $i++;
                                        if($count>0) { ?>
                                            <div class="panel panel-default" style="border-radius: 0px !important;">
                                                <div class='panel-heading'>
                                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#<?= $i; ?>">
                                                        <h4 class='panel-title'><?php echo $key2; ?><i class="indicator glyphicon glyphicon-chevron-up <?php //if($i < 2) //echo 'glyphicon-chevron-up'; //else //echo 'glyphicon-chevron-down'; ?> pull-right"></i></h4>
                                                    </a>
                                                </div>
                                                <div id="<?=$i;?>" class="panel-collapse collapse in<?php //if($i < 2) //echo ' in'; ?>">
                                                    <div class="panel-body ">
                                                        <ul class='list-group property-detail-list'>
                                                            <?php
                                                                foreach ($value2 as $key3 => $value3) {
                                                                    foreach ($value3 as $key4 => $value4) { ?>
                                                                    <li class="list-group-item">
                                                                        <strong><?php echo $key4; ?> </strong>
                                                                        <span>
                                                                        	<?php if (isset($value4) && !empty($value4)): ?>
                                                                                <?php if (is_bool($value4) == true): ?>
                                                                                    <?php echo " "; ?>
                                                                                <?php elseif($key4 == "List Price/SqFt" && is_float($value4)): ?>
                                                                                    <?php echo ": $".$value4; ?>
                                                                                <?php elseif($value4 == "Y"): ?>
                                                                                    <?php echo ": Yes"; ?>
                                                                                <?php elseif($value4 == "N"): ?>
                                                                                    <?php echo ": No"; ?>
                                                                                <?php elseif($value4 == "0"): ?>
                                                                                    <?php echo ": $0"; ?>
                                                                                <?php else: ?>
                                                                                    <?php echo ": ".$value4; ?>
                                                                                <?php endif ?>
                                                                            <?php endif ?>
                                                                        	<?php //echo (isset($value4) && !empty($value4)) ? $value4 : 1; ?>
                                                                    	</span>
                                                                    </li>
                                                            <?php
                                                                    }
                                                                }
                                                            ?>

                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>
                                <?php } ?>
                           <?php } ?>

                    <?php } else { ?>

                    <?php } ?>
                </div>
            </ul>
            <div id="rooms"></div>
        </div>
    </div>

	<?php
	/**
	* Temp fix to minimize Google Geolocation API call
    * `DEV-2460`
	* Related file changes:
	 * - home/controller/Property.php line 180
	 * - home/views/session/footer.php line 1344
	 *
    <!-- Nearby Listings -->
    <div class="property-other-listings property-detail-sections">
        <h4>Nearby Listings</h4>
        <div id="nearby-listings">
            <div class="nearby-loader">
                <!-- <div class="preloader"></div> -->
            </div>
        </div>
    </div>
    *
    */ ?>

    <?php } ?>
</div>
