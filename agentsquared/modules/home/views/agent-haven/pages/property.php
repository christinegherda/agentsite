<section class="property-detail-content property-detail-section-nopadding">
	<div class="container">
		<div class="row"> 
			<?php $this->load->view($theme."/session/sidebar"); ?>
			<div class="col-md-3 col-sm-4 invisible-xs"></div>
			<div class="col-md-9 col-sm-9">
				<div class="property-details">
					<div class="property-detail-images">
						<div class="row">
							<div class="col-md-9">
								<?php
									//$this->load->view($theme.'/pages/property_view/photo-gallery');
								?>
								<div id="property-photos">
									<div class="placeholder-photos">
										<div class="preloader">Loading images</div>
									</div>
								</div>
							</div>
							<div class="col-md-3">
								<?php
									$this->load->view($theme.'/pages/property_view/property_detail_control');
								?>
							</div>
						</div>
					</div>
					<?php $this->load->view($theme.'/pages/property_view/property_full_spec'); ?>
				</div>
			</div> 
		</div> 
	</div> 