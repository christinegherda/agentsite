

        <div class="page-content">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="page-title">Blogs</h2>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-sm-8">
                    <ul class="blog-list">

                        <?php if( isset($blogs) && !empty($blogs) ) { 
                                foreach ($blogs as $blog) {?>  
                                        <li class="clearfix">
                                            <div class="blog-summary">
                                                <div class="col-md-2 col-sm-3">
                                                    <div class="date">
                                                        <h4><?php echo date("F", strtotime($blog->post_date)); ?></h4>
                                                        <h4><?php echo date("d", strtotime($blog->post_date)); ?></h4>
                                                    </div>    
                                                </div>
                                                <div class="col-md-10 col-sm-9">
                                                    <div class="summary">
                                                        <h4><a href="<?=base_url()?>post/<?php echo (str_replace(" ", "-", $blog->slug))?>"><?php echo ucwords($blog->title); ?></a></h4>
                                                        <p>
                                                            <?php echo ucwords(substr($blog->content,0,500)); ?>...
                                                        </p>
                                                        <a href="<?=base_url()?>post/<?php echo (str_replace(" ", "-", $blog->slug))?>">read more</a>
                                                    </div>    
                                                </div>
                                                
                                            </div>
                                        </li>

                            <?php } 

                             } else { ?>

                                <h2 class="text-center">No Blogs Created!</h2>

                        <?php }?>
                    </ul>

                    <div class="blog-pagination">
                        <?php if( $pagination ) : ?>
                            <div class="pagination-area pull-right">
                                <nav>
                                    <ul class="pagination">
                                        <?php echo $pagination; ?>
                                    </ul>
                                </nav>
                            </div>
                        <?php endif; ?>   
                    </div>
                </div>
                <div class="col-md-4 col-sm-4">

                 <?php if( isset($all_blog_archives) && !empty($all_blog_archives) ) {?>

                    <div class="filter-tab">
                        <h4 class="text-center">Archives</h4>
                        <hr>
                       <ul>
                            
                            <?php foreach ($all_blog_archives as $archive) {?>    
                                
                                 <li style="margin-left:20px;list-style-type:circle;margin-top:10px"><?php echo $archive->year?> </li>
                                 <li style="margin-left:20px">- <a href="<?=base_url()?>archives/<?=date("Y-m", strtotime($archive->post_date))?>"><?php echo $archive->month_name?></a> <span class="badge"><?php echo $archive->total?></span></li>

                            <?php } ?>
                        </ul>
                    </div>

               <?php  } ?>

                 <?php $this->load->view($theme.'/pages/page_view/page-agent-info'); ?>
            </div>
        </div>
    </div>

   