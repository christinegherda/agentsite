
    <div class="page-content">
        <section class="property-detail-content">
            <div class="container">
                <div class="row">
                     <!-- Sidebar Area -->
                    <?php $this->load->view($theme."/session/sidebar"); ?>
                    <div class="col-md-9 col-sm-9">
                        <?php 
                            if(isset($sold_listings) && !empty($sold_listings)) {?>

                                <section class="saved-search-area">
                                    <div class="search-listings">
                                        <?php
                                            foreach($sold_listings as $sold){?>
                                            
                                                <div class="col-md-4 col-sm-6">
                                                    <div style="min-height: 280px;" class="search-listing-item">
                                                     <div class="sold-banner"><img src="/assets/images/dashboard/demo/agent-haven/sold.png"></div>
                                                        <?php
                                                        $url_rewrite = url_title("{$sold["UnparseAddress"]} {$sold["PostalCode"]}");
                                                        $listing_id = str_replace('"', '', $sold["ListingKey"]);
                                                        $address = str_replace('"', '', $sold["UnparseAddress"]);
                                                        $postalcode = str_replace('"', '', $sold["PostalCode"]); 
                                                        $state_or_province = str_replace('"', '', $sold["StateOrProvince"]); 
                                                        $postal_city = str_replace('"', '', $sold["PostalCity"]);
                                                        $city = str_replace('"', '', $sold["City"]); 
                                                        $close_price = str_replace('"', '', $sold["ClosePrice"]); 
                                                        $close_date = str_replace('"', '', $sold["CloseDate"]); 

                                                        ?>
                                                        <div class="search-property-image">
                                                            <?php if(isset($sold["PhotosURi"])) { ?>
                                                                <a href="/property-details/<?php echo $url_rewrite ?>/<?php echo $listing_id; ?>"><img src=<?php echo $sold["PhotosURi"]; ?> alt="<?php echo $sold["UnparseAddress"]; ?>" class="img-responsive" style="width:100%;"></a>
                                                            <?php } else { ?>
                                                                <a href="/property-details/<?php echo $url_rewrite ?>/<?php echo $listing_id; ?>"><img src="/assets/images/image-not-available.jpg" alt="<?php echo $sold["UnparseAddress"]; ?>" class="img-responsive" style="width:100%;"></a>
                                                            <?php } ?>
                                                        </div>
                                                        <div class="search-listing-title">
                                                            <div class="col-md-12 col-xs-12">
                                                                <p class="search-property-title">
                                                                    <a class="listing-link" href="/property-details/<?php echo $url_rewrite ?>/<?php echo $listing_id; ?>">
                                                                        <?php echo $address; ?>
                                                                    </a>
                                                                </p>
                                                            </div>
                                                        </div>
                                                        <p><i class="fa fa-map-marker"></i>
                                                            <?php
                                                                if($account_info->Mls != "MLS BCS"){
                                                                    $mystring = str_replace('"', '', $sold["City"]); 
                                                                    $postalcode = str_replace('"', '', $sold["PostalCode"]); 
                                                                    $findme   = '*';
                                                                    $pos = strpos($mystring, $findme);
                                                                    $pos_code = strpos($postalcode, $findme);

                                                                    if($pos === false){
                                                                        if($pos_code === false){
                                                                            echo $city . ", " . $state_or_province . " " . $postalcode;
                                                                        }else{
                                                                            echo $city . ", " . $state_or_province;
                                                                        }
                                                                        
                                                                    } else {

                                                                        if($pos_code === false){
                                                                            echo $city . ", " . $state_or_province . " " . $postalcode;
                                                                        }else{
                                                                           echo $city . ", " . $state_or_province;     
                                                                        }
                                                                    }
                                                                
                                                                } else {
                                                                    $mystring = str_replace('"', '', $sold["City"]); 
                                                                    $StateOrProvince = $str_replace('"', '', $sold["StateOrProvince"]); 
                                                                    $findme   = '*';
                                                                    $pos = strpos($mystring, $findme);
                                                                    $pCode = strpos($StateOrProvince, $findme);

                                                                    if($pos === false && $pCode == false)
                                                                        echo $city;
                                                                    else
                                                                        echo $city . ", " . $state_or_province;
                                                                }
                                                            ?>

                                                        </p>
                                                        
                                                        <p><i class="fa fa-usd"></i> Price: <?php echo number_format($close_price);?></p>

                                                         <?php if(isset($sold["CloseDate"])){?>
                                                            <p><i class="fa fa-calendar"></i> <?php echo date("m-d-Y", strtotime($close_date)); ?></p>
                                                        <?php  } ?> 
                                                    </div>
                                                </div>

                                            <?php  }
                                            }?>
                                    </div>
                                </section>

                                 <?php
                                    if($total_sold_listings > 12) { ?>
                                            <div class="pagination-area">
                                             <?php echo $pagination;?>
                                    <?php } ?>
                    </div>
                </div>
            </div>
        </section>
    </div>
   
