  <?php
        $this->load->view($theme.'/pages/page_view/advance-search-form-custom');
    ?>
  <section class="property-main-content save-searches">
        <div class="container">
            <div class="property-list-tab">

              <!-- Nav tabs -->
              <!-- <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#property-list" aria-controls="property-list" role="tab" data-toggle="tab"><span class="fa fa-list-ul"></span> <span class="tab-text">Property List</span></a></li>
                <li role="presentation"><a href="#show-map" aria-controls="show-map" role="tab" data-toggle="tab"><span class="fa fa-map"></span> <span class="tab-text">Show Map</span></a></li>
                <li role="presentation" class="refine-search-tab"><a href="#refine-search" aria-controls="refine-search" role="tab" data-toggle="tab"><span class="fa fa-map-signs"></span> <span class="tab-text">Refine Your Search</span></a></li>
              </ul> -->

              <!-- Tab panes -->
              <div class="tab-content">

                <!-- PROPERTY LIST -->
                <div role="tabpanel" class="tab-pane active" id="property-list">
                  <div class="row">
                    <div class="col-md-7 col-sm-12">
                    <?php if($this->uri->segment(1) == "active_listings"){?>
                        <h1>Active Listings</h1>

                     <?php } elseif($this->uri->segment(1) == "office_listings"){?>
                        <h1>Office Listings</h1>

                     <?php } elseif($this->uri->segment(1) == "new_listings"){?>
                        <h1>New Listings</h1>

                        <?php } elseif($this->uri->segment(1) == "nearby_listings"){?>
                           <h1>Nearby Listings</h1>

                      <?php } elseif(isset($saved_search_name)){?>
                          <h1><?=$saved_search_name;?></h1>

                    <?php  } ?>
                        <?php if (isset($_GET) && !empty($_GET)): ?>
                          <div class="keywords-section">
                            <p>
                              <span class="keyword-text">Keywords:</span>
                            <?php foreach ($_GET as $k=>$v): ?>
                                <?php if (!empty($v)):
                                    $k = preg_replace('/(?<!\ )[A-Z]/', '$0', $k);
                                    if ($k != 'offset' && $k != 'totalrows') { ?>
                                            <?php
                                            if ($k === 'Roof') {
                                                foreach ($this->input->get('Roof') as $vRoof) {
                                                ?>
                                                    <span class="label label-gray"><?=strtoupper($k)?>: <?=strtoupper($vRoof)?></span>
                                                <?php
                                                }
                                            } elseif ($k == 'price_min' || $k == 'price_max' || $k == 'area_min' || $k == 'area_max') {
                                                $k = str_replace("_"," ",$k);
                                                ?>
                                                    <span class="label label-gray"><?=strtoupper($k)?>: <?=number_format($v)?></span>
                                                <?php
                                            } else {
                                                // echo "string";
                                                ?>
                                                    <span class="label label-gray"><?=strtoupper($k)?>: <?=($v)?></span>
                                                <?php
                                            } ?>
                                    <?php
                                    }
                                ?>
                                <?php endif ?>
                            <?php endforeach ?>
                            </p>
                          </div>
                        <?php endif ?>
                    </div>
                    <div class="invisible-xs">
                      <div class="col-md-2 col-sm-6 text-right no-padding">
                            <?php if($this->uri->segment(1) == "search-results" && !empty(http_build_query($_GET)) ){
                                $saved_searches = FALSE;
                                if(isset($saved_searches_db) && !empty($saved_searches_db)) :
                                    foreach($saved_searches_db as $searches) :
                                        if($searches->url == http_build_query($_GET)) :
                                            $saved_searches = TRUE;
                                            break;
                                        endif;
                                    endforeach;
                                else :
                                    $saved_searches = FALSE;
                                endif;
                            ?>

                          <?php if(!isset($_SESSION['customer_id'])) { ?>
                              <a href="javascript:void(0)" class="btn btn-light-green saved_search modalogin" data-toggle='modal' data-target='.customer_login_signup' data-redirect-customer="search-results?<?=http_build_query($_GET).'&success';?>"><i class="fa fa-plus"></i> <span class="">Save this Search</span>
                              </a>
                          <?php } else { ?>
                             <a href="<?php echo site_url("home/home/save_search_properties?".http_build_query($_GET)); ?>" type="button" id="moreFiltersToggle" class="<?=($saved_searches) ? "btn btn-danger follow-search-red" : "btn btn-light-green follow-search"?>">

                             <?php if($saved_searches){?>

                                  <i class="fa fa-save"></i> <span class="savethis-text">Saved Search</span>
                             <?php } else {?>

                                  <i class="fa fa-plus"></i> <span class="savethis-text">Save this Search</span>

                              <?php  }?> </a>

                              </a>
                          <?php }
                          }?>
                      </div>
                      <div class="col-md-3  col-sm-6">
                        <div class="form-inline form-sort">
                          <label for="">Sort By:</label>
                          <select name="sort-name" id="" class="form-control sort-by">
                            <option value="newest" <?=(isset($_GET["order"]) && ($_GET["order"] == "newest")) ? "selected" : ""; ?>>Newest</option>
                            <option value="highest" <?=(isset($_GET["order"]) && ($_GET["order"] == "highest")) ? "selected" : ""; ?>>Highest Price</option>
                            <option value="lowest" <?=(isset($_GET["order"]) && ($_GET["order"] == "lowest")) ? "selected" : ""; ?>>Lowest Price</option>
                            <option value="oldest" <?=(isset($_GET["order"]) && ($_GET["order"] == "oldest")) ? "selected" : ""; ?>>Oldest</option>
                          </select>
                        </div>
                      </div>
                    </div>
                    <div class="visible-xs">
                      <div class="col-xs-9">
                        <div class="form-inline form-sort">
                          <label for="">Sort By:</label>
                          <select name="sort-name" id="" class="form-control sort-by">
                            <option value="newest" <?=(isset($_GET["order"]) && ($_GET["order"] == "newest")) ? "selected" : ""; ?>>Newest</option>
                            <option value="highest" <?=(isset($_GET["order"]) && ($_GET["order"] == "highest")) ? "selected" : ""; ?>>Highest Price</option>
                            <option value="lowest" <?=(isset($_GET["order"]) && ($_GET["order"] == "lowest")) ? "selected" : ""; ?>>Lowest Price</option>                            
                            <option value="oldest" <?=(isset($_GET["order"]) && ($_GET["order"] == "oldest")) ? "selected" : ""; ?>>Oldest</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-xs-3">
                        <?php if($this->uri->segment(1) == "search-results" && !empty(http_build_query($_GET)) ){
                            $saved_searches = FALSE;
                            if(isset($saved_searches_db) && !empty($saved_searches_db)) :
                                foreach($saved_searches_db as $searches) :
                                    if($searches->url == http_build_query($_GET)) :
                                        $saved_searches = TRUE;
                                        break;
                                    endif;
                                endforeach;
                            else :
                                $saved_searches = FALSE;
                            endif;
                        ?>

                      <?php if(!isset($_SESSION['customer_id'])) { ?>
                          <a href="javascript:void(0)" class="btn btn-light-green saved_search modalogin" data-toggle='modal' data-target='.customer_login_signup' data-redirect-customer="search-results?<?=http_build_query($_GET).'&success';?>"><i class="fa fa-plus"></i> <span class="savethis-text">Save this Search</span>
                          </a>
                      <?php } else { ?>
                         <a href="<?php echo site_url("home/home/save_search_properties?".http_build_query($_GET)); ?>" type="button" id="moreFiltersToggle" class="<?=($saved_searches) ? "btn btn-danger follow-search-red" : "btn btn-light-green follow-search"?>">

                         <?php if($saved_searches){?>

                              <i class="fa fa-save"></i> <span class="savethis-text">Saved Search</span>
                         <?php } else {?>

                              <i class="fa fa-plus"></i> <span class="savethis-text">Save this Search</span>

                          <?php  }?> </a>

                          </a>
                      <?php }
                      }?>
                      </div>
                    </div>
                  </div>

                    <?php if (!isset($featured) || count($featured) == 0 || empty($featured)){ ?>
                        <div class="property-block property-empty">
                          <i class="fa fa-exclamation-triangle"></i>

                          <?php if($this->uri->segment(1) == "saved_searches"){?>

                            Saved Search not found!
                            
                          <?php  }elseif($this->uri->segment(1) == "active_listings"){?>

                            Active listings not found!

                          <?php }elseif($this->uri->segment(1) == "office_listings"){?>

                            Office listings not found!

                           <?php }elseif($this->uri->segment(1) == "new_listings"){?>

                            New listings not found!

                            <?php  }elseif($this->uri->segment(1) == "nearby_listings"){?>

                              Nearby listings not found!

                            <?php } ?>

                        </div>
                    <?php } else{ ?>
                        <?php
                            $offset = (isset($_GET['offset'])) ? $_GET['offset'] : "" ;
                            
                            if($page_size == 10){
                              $start_count = $current_page * $page_size - 9;
                            } else{
                              $start_count = 1;
                            }

                            $end_count = $current_page * $page_size;

                            if($end_count > $total_count) {
                              $end_count1 = $end_count - $total_count;
                              $end_count -= $end_count1;
                            }

                        ?>
                          <div class="search-count">
                            <p>
                              <span class="start-count"><?=$start_count;?></span> - <span class="end-count"><?=$end_count;?></span> of <span class="end-count"><?php echo !empty($total_count) ? number_format($total_count) :"" ?></span> Properties Found.
                              <!-- <?php echo !empty($offset) ? number_format($offset) : "" ?> <?php echo !empty($offset) ? "of" : "" ?> <?php echo !empty($total_count) ? number_format($total_count) : ""; ?> -->
                            </p>
                          </div>

                          <?php
                            $is_openhouse=false;
                            if(isset($_GET['listing_change_type'])) {
                                foreach($_GET['listing_change_type'] as $show_me) {
                                    $is_openhouse = ($show_me == "Open House") ? true : false;
                                }
                            }
                            if(!$is_openhouse) {
                                if(isset($_GET['Search'])) {
                                    $search_get = $_GET['Search'];
                                    $sg = strtolower($search_get);
                                    $is_openhouse = ($sg == 'open house') ? true : false;
                                }
                            }
                            foreach ($featured as $feature) { ?>

                              <?php  if(isset($feature['Photos']['Uri300'])){
                                    $listing_photo = $feature['Photos']['Uri300'];
                                  }elseif(isset($feature['StandardFields']['Photos'][0]['Uri300'])){
                                    $listing_photo = $feature['StandardFields']['Photos'][0]['Uri300'];
                                  }else{
                                    $listing_photo = base_url()."assets/images/image-not-available.jpg";
                               }?>

                                <div class="property-block">
                                  <div class="row">
                                    <div class="col-md-3 col-sm-6 col-xs-5 mobilepaddingright-0">
                                      <a href="<?=base_url('other-property-details/');?><?php echo str_replace(" ",  "-", $feature['StandardFields']['ListingKey']);?>" class="prop-img">
                                        <img src="<?=$listing_photo?>" alt="<?php echo $feature['StandardFields']["UnparsedFirstLineAddress"]; ?>" alt="" class="img-responsive">
                                      </a>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-5 mobilepaddingright-0">
                                      <div class="property-feature">
                                        <p class="propaddress">
                                            <a href="<?=base_url('other-property-details/');?><?php echo str_replace(" ",  "-", $feature['StandardFields']['ListingKey']);?>">
                                                <?php echo $feature['StandardFields']["UnparsedFirstLineAddress"]; ?>
                                            </a>
                                        </p>
                                        <p class="propcity"><?php echo str_replace($feature['StandardFields']["UnparsedFirstLineAddress"].',', "", $feature['StandardFields']["UnparsedAddress"]); ?></p>
                                        <div class="visible-xs">
                                          <p class="propprice"><sup>$</sup> <?=number_format($feature['StandardFields']['CurrentPrice'])?></p>
                                        </div>
                                        <ul class="prop-status-type">
                                        <?php if (isset( $feature['StandardFields']["MlsStatus"])): ?>
                                          <li>
                                            <p class="prop-status"><?php echo $feature['StandardFields']["MlsStatus"];?></p>
                                          </li>
                                        <?php endif ?>
                                        <?php if (isset( $feature['StandardFields']["PropertyClass"])): ?>
                                          <li>
                                            <p class="prop-type"><?php echo $feature['StandardFields']["PropertyClass"];?></p>
                                          </li>
                                        <?php endif ?>
                                        </ul>
                                        <ul class="prop-bed-bath-lot">
                                            <?php if ($feature['StandardFields']['BedsTotal'] && is_numeric($feature['StandardFields']['BedsTotal'])): ?>
                                              <li>
                                                <p>Bed</p>
                                                <p class="prop-number"><?=$feature['StandardFields']['BedsTotal']?></p>
                                              </li>
                                            <?php endif ?>
                                            <?php if ($feature['StandardFields']['BathsTotal'] && is_numeric($feature['StandardFields']['BathsTotal'])): ?>
                                              <li>
                                                <p>Bath</p>
                                                <p class="prop-number"><?=$feature['StandardFields']['BathsTotal']?></p>
                                              </li>
                                            <?php endif ?>
                                           <?php
                                            if(!empty($feature['StandardFields']['BuildingAreaTotal']) && ($feature['StandardFields']['BuildingAreaTotal'] != "0")   && is_numeric($feature['StandardFields']['BuildingAreaTotal'])) {?>

                                              <li>
                                                <p></p>
                                                <p class="prop-number"><?=number_format($feature['StandardFields']['BuildingAreaTotal'])?> sqft</p>
                                              </li>

                                            <?php } elseif(!empty($feature['StandardFields']['LotSizeArea']) && ($feature['StandardFields']['LotSizeArea'] != "0")   && is_numeric($feature['StandardFields']['LotSizeArea'])) {

                                                if(!empty($feature['StandardFields']['LotSizeUnits']) && ($feature['StandardFields']['LotSizeUnits']) === "Acres"){?>
                                                  <li>
                                                    <p>Lot Size:</p>
                                                    <p class="prop-number"><?=number_format($feature['StandardFields']['LotSizeArea'], 2, '.', ',' )?> acres</p>
                                                  </li>
                                                <?php } else {?>
                                                  <li>
                                                    <p>Lot Size:</p>
                                                    <p class="prop-number"><?=number_format($feature['StandardFields']['LotSizeArea'])?> acres</p>
                                                  </li>
                                                <?php }?>


                                            <?php } elseif(!empty($feature['StandardFields']['LotSizeSquareFeet']) && ($feature['StandardFields']['LotSizeSquareFeet'] != "0")   && is_numeric($feature['StandardFields']['LotSizeSquareFeet'])) {?>

                                                  <li>
                                                    <p>Lot Size:</p>
                                                    <p class="prop-number"><?=number_format($feature['StandardFields']['LotSizeSquareFeet'])?> sqft</p>
                                                  </li>
                                             <?php } elseif(!empty($feature['StandardFields']['LotSizeAcres']) && ($feature['StandardFields']['LotSizeAcres'] != "0")   && is_numeric($feature['StandardFields']['LotSizeAcres'])) {?>

                                                  <li>
                                                    <p>Lot Size:</p>
                                                    <p class="prop-number"><?=number_format($feature['StandardFields']['LotSizeAcres'],2 ,'.',',')?> acres</p>
                                                  </li>
                                            <?php } elseif(!empty($feature['StandardFields']['LotSizeDimensions']) && ($feature['StandardFields']['LotSizeDimensions'] != "0")   && ($feature['StandardFields']['LotSizeDimensions'] != "********")) {?>

                                                  <li>
                                                    <p>Lot Size:</p>
                                                    <p class="prop-number"><?=$feature['StandardFields']['LotSizeDimensions']?></p>
                                                  </li>
                                            <?php } ?>
                                        </ul>
                                      </div>
                                    </div>
                                    <div class="col-md-3 col-sm-12 col-xs-1 mobilepadding-0">
                                      <div class="prop-actions">
                                        <div class="invisible-xs">
                                          <p class="propprice"><sup>$</sup> <?=number_format($feature['StandardFields']['CurrentPrice'])?></p>
                                        </div>

                                        <?php
                                            $property_saved = FALSE;
                                            if(isset($_SESSION["save_properties"])) :
                                                foreach($_SESSION["save_properties"] as $key) :
                                                    if($key['property_id'] == $feature['StandardFields']['ListingKey']) :
                                                        $property_saved = TRUE;
                                                        break;
                                                    endif;
                                                endforeach;
                                            endif;
                                        ?>

                                        <?php if($this->uri->segment(1) == "office_listings"){
                                          $redirect = "office_listings?";

                                        } elseif($this->uri->segment(1) == "new_listings"){
                                          $redirect = "new_listings?";

                                        } elseif($this->uri->segment(1) == "nearby_listings"){
                                          $redirect = "nearby_listings?";

                                        } elseif($this->uri->segment(1) == "saved_searches"){
                                          $redirect = "saved_searches?";

                                        } else {
                                          $redirect = "search-results?";

                                        } ?>

                                        <p>
                                          <a href="#" data-action="<?=site_url('home/home/save_favorates_properties/'.$feature['StandardFields']['ListingKey']);?>" data-pro-id="<?=$feature['StandardFields']['ListingKey']?>" data-property-name="<?=$feature['StandardFields']['UnparsedAddress'];?>" class="save-favorate <?=($property_saved) ? 'saveproperty' : '';?>" data-title="Tooltip" data-trigger="hover" title="<?=($property_saved) ? "Saved Property" : "Save Property";?>">
                                            <span id="isSaveFavorate_<?=$feature['StandardFields']['ListingKey']?>" class="heart <?= ($property_saved) ? "yesheart" : "";?>"><?= ($property_saved) ? "<i class='fa fa-heart'></i>" : "<i class='fa fa-heart-o'></i>";?></span>
                                            <span class="hidethis-text">Save Property </span>
                                          </a>
                                        </p>

                                        <a href="#" type="button" class="requestprop request_info" data-toggle="modal" data-target="#request-info-modal" data-listingId="<?php echo str_replace(" ",  "-", $feature['StandardFields']['ListingKey']); ?>" data-propertyType="other_property" data-address="<?=$feature['StandardFields']["UnparsedAddress"];?>"><i class="fa fa-question-circle"></i>
                                          <span class="hidethis-text">Request Information</span>
                                        </a>

                                        <?php if($this->uri->segment(1) == "new_listings"){?>
                                         
                                              <?php if(isset($feature['StandardFields']["OnMarketDate"]) && $feature['StandardFields']["OnMarketDate"]!="********"){
                                                  $onmarketdate = $feature['StandardFields']["OnMarketDate"];
                                                  $today = time();
                                                  $onmarketdate= strtotime($onmarketdate);
                                                  $hours =  round(abs($today-$onmarketdate)/60/60);
                                              ?>
                                                  <p class="prop-listing-date-all">Listed <?=($hours == 0) ? 1 : $hours ?> <?=($hours == 1 || $hours == 0) ? "hour" :"hours" ?> ago!</p>
                                              <?php }?>
                                         
                                       <?php }?>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <?php
                            }
                          ?>
                    <?php } ?>
                  <?php if (isset($total_count)): ?>
                    <?php if ($total_count > 10): ?>
                      <div class="pagination-section">
                         <div class="row">
                            <div class="col-md-12">
                              <div class="text-center">
                                  <div class="pagination-area property-pagination">
                                      <?php echo $pagination;?>
                                  </div>
                              </div>
                            </div>
                          </div>
                      </div>
                    <?php endif ?>
                  <?php endif ?>

                </div>
                <!-- END OF PROPERTY LIST -->


                <!-- SHOW MAP -->
                <div role="tabpanel" class="tab-pane" id="show-map">
                  <div class="propmap-placeholder">
                      <div class="placeholder-map" style="background: #e1dfdb;position: relative;">
                          <div class="map"><p>Map Loading..</p></div>
                      </div>
                  </div>
                  <!-- <div class="propmap" style="display:none"> -->
                  <div class="propmap" style="display:none;">
                    <div id="show_map_canvas"></div>
                    <style type="text/css">
                      #show_map_canvas  {
                        height: 630px;
                        width: 100%;
                        margin-top: 0px;
                        padding: 0px;
                      }
                    </style>
                  </div>
                </div>
                <!-- END OF SHOW MAP -->


                <!-- REFINE SEARCH -->
                <div role="tabpanel" class="tab-pane" id="refine-search">
                    <form method="get" action="<?php echo base_url(); ?>search-results" id="advance_search_form">
                        <div class="col-md-6">
                            <div class="panel panel-default">
                                <div class="panel-heading">Address Information</div>
                                <div class="panel-body">
                                    <div class="form-group">
                                        <label for="">Street Number</label>
                                        <input type="text" class="form-control" value="<?=($this->input->get('StreetNumber')) ? $this->input->get('StreetNumber') : ""; ?>" name="StreetNumber">
                                        <input type="hidden" name="Search" value="<?=($this->input->get('Search')) ? $this->input->get('Search') : ''; ?>">
                                        <input type="hidden" name="offset" value="<?=($this->input->get('offset')) ? $this->input->get('offset') : ''; ?>">
                                        <input type="hidden" name="page" value="<?=($this->input->get('page')) ? $this->input->get('page') : '1'; ?>">
                                        <input type="hidden" name="totalrows" value="<?=($this->input->get('totalrows')) ? $this->input->get('totalrows') : ''; ?>">
                                        <input type="hidden" name="current_segment" value="<?=($this->uri->segment(1)) ? $this->uri->segment(1) : ''; ?>">
                                        <input type="hidden" name="search_id" value="<?=(isset($search_id) & !empty($search_id)) ? $search_id : ''; ?>">
                                        <!-- <input type="hidden" name="search_id" value="<?=($this->uri->segment(1) == 'saved_searches') ? $this->uri->segment(2) : ''; ?>"> -->
                                    </div>
                                    <div class="form-group">
                                        <label for="">Street Name</label>
                                        <input type="text" class="form-control" value="<?=($this->input->get('StreetName')) ? $this->input->get('StreetName') : ""; ?>" name="StreetName">
                                    </div>
                                    <div class="form-group">
                                        <?php
                                        $show_city = false;
                                            foreach($search_fields as $sf) {
                                                if($sf->field_name == 'City') {
                                                    $city_fields = $this->search_model->get_mls_data_search_fields_list_data($sf->id);
                                        ?>
                                        <label for="">City</label>

                                        <?php if($sf->field_haslist == '1'){?>

                                            <select class="form-control" name="City">
                                                <option value="">Select City</option>
                                                <?php
                                                        foreach($city_fields as $cf) {
                                                            if($cf->field_value != 'Select One') {
                                                ?>
                                                <option value="<?=$cf->field_value?>" <?=($this->input->get('City') && ($this->input->get('City') == $cf->field_name)) ? "selected" : ""; ?>><?=$cf->field_name?></option>
                                                <?php
                                                            }
                                                        }
                                                ?>
                                            </select>

                                         <?php } else {?>

                                            <input type="text" class="form-control" name="City" placeholder="">

                                         <?php }
                                                $show_city = true;
                                              }

                                            }
                                            if(!$show_city){?>

                                               <label for="">City</label>
                                                <input type="text" class="form-control" name="City" placeholder="">

                                          <?php  }
                                        ?>
                                    </div>
                                    <div class="form-group">
                                        <?php
                                        $show_zip = false;
                                            foreach($search_fields as $sf) {
                                                if($sf->field_name == 'PostalCode') {
                                                    $city_fields = $this->search_model->get_mls_data_search_fields_list_data($sf->id);
                                        ?>

                                        <label for="">Zip Code</label>

                                        <?php if($sf->field_haslist == '1'){?>

                                               <select class="form-control" name="PostalCode">
                                                  <option value="">Select Zip Code</option>
                                                  <?php
                                                          foreach($city_fields as $cf) {
                                                              if($cf->field_value != '00000') {
                                                  ?>
                                                  <option value="<?=$cf->field_value?>" <?=($this->input->get('PostalCode') && ($this->input->get('PostalCode') == $cf->field_name)) ? "selected" : ""; ?>><?=$cf->field_name?></option>
                                                  <?php
                                                              }
                                                          }
                                                  ?>
                                              </select>

                                         <?php } else {?>

                                                <input type="text" class="form-control" name="PostalCode" placeholder="">

                                         <?php }
                                                $show_zip = true;
                                              }

                                            }
                                             if(!$show_zip){?>

                                               <label for="">Zip Code</label>
                                                <input type="text" class="form-control" name="PostalCode" placeholder="">

                                          <?php  }
                                        ?>
                                    </div>
                                    <div class="form-group">
                                        <label for="">Subdivision</label>
                                        <input type="text" class="form-control" value="<?=($this->input->get('SubdivisionName')) ? $this->input->get('SubdivisionName') : "";?>" name="SubdivisionName">
                                    </div>
                                     <div class="form-group">
                                        <?php
                                        $show_county = false;
                                            foreach($search_fields as $sf) {
                                                if($sf->field_name == 'CountyOrParish') {

                                                    $search_fields_list = $this->search_model->get_mls_data_search_fields_list_data($sf->id);

                                        ?>
                                        <label for="">County</label>
                                          <?php if($sf->field_haslist == '1'){?>

                                                <select name="CountyOrParish" class="form-control" placeholder="County">
                                                  <option value="">Select County</option>
                                                  <?php
                                                      foreach($search_fields_list as $sfl) {
                                                          if($sfl->field_name != 'Select One') { ?>

                                                            <option value="<?=$sfl->field_value?>" <?=($this->input->get('CountyOrParish') && ($this->input->get('CountyOrParish') == $sfl->field_name)) ? "selected" : ""; ?>><?=$sfl->field_name?></option>

                                                    <?php } ?>


                                                        <?php
                                                      }
                                                  ?>
                                              </select>

                                          <?php } else {?>
                                              <input type="text" class="form-control" name="CountyOrParish" placeholder="">
                                          <?php }
                                                $show_county = true;
                                              }
                                            }
                                            if(!$show_county){?>

                                               <label for="">County</label>
                                                <input type="text" class="form-control" name="CountyOrParish" placeholder="">

                                          <?php  }
                                        ?>
                                    </div>
                                </div>
                            </div>
                           <div class="panel panel-default">
                                <div class="panel-heading">School</div>
                                <div class="panel-body">

                                 <?php
                                    $schoolShow = false;
                                    foreach($search_fields as $sf) {
                                        if($sf->field_name == 'ElementarySchool' || $sf->field_name == 'MiddleOrJuniorSchool' || $sf->field_name == 'HighSchool') {
                                            $schoolShow = true;
                                        }
                                    }
                                    if($schoolShow) {
                                ?>

                                    <div class="form-group">

                                        <?php
                                            foreach($search_fields as $sf) {
                                                if($sf->field_name == 'ElementarySchool') {

                                                    $search_fields_list = $this->search_model->get_mls_data_search_fields_list_data($sf->id);

                                        ?>
                                        <label for="">Elementary School</label>
                                        <select name="ElementarySchool" class="form-control" placeholder="Elementary School">
                                            <option value="">Select One</option>
                                            <?php
                                                foreach($search_fields_list as $sfl) {
                                                    if($sfl->field_name != 'Select One') {
                                            ?>
                                            <option value="<?=$sfl->field_value?>" <?=($this->input->get('ElementarySchool') && ($this->input->get('ElementarySchool') == $sfl->field_name)) ? "selected" : ""; ?>><?=$sfl->field_name?></option>
                                            <?php
                                                    }
                                            ?>

                                            <?php
                                                }
                                            ?>
                                        </select>
                                        <?php
                                                }
                                            }
                                        ?>
                                    </div>
                                    <div class="form-group">

                                        <?php
                                            foreach($search_fields as $sf) {
                                                if($sf->field_name == 'MiddleOrJuniorSchool') {

                                                    $search_fields_list = $this->search_model->get_mls_data_search_fields_list_data($sf->id);

                                        ?>
                                        <label for="">Middle or Junior School</label>
                                        <select name="MiddleOrJuniorSchool" class="form-control" placeholder="Middle or Junior School">
                                            <option value="">Select One</option>
                                            <?php
                                                foreach($search_fields_list as $sfl) {
                                                    if($sfl->field_name != 'Select One') {
                                            ?>
                                            <option value="<?=$sfl->field_value?>" <?=($this->input->get('MiddleOrJuniorSchool') && ($this->input->get('MiddleOrJuniorSchool') == $sfl->field_name)) ? "selected" : ""; ?>><?=$sfl->field_name?></option>
                                            <?php
                                                    }
                                            ?>

                                            <?php
                                                }
                                            ?>
                                        </select>
                                        <?php
                                                }
                                            }
                                        ?>
                                    </div>
                                    <div class="form-group">

                                        <?php
                                            foreach($search_fields as $sf) {
                                                if($sf->field_name == 'HighSchool') {

                                                    $search_fields_list = $this->search_model->get_mls_data_search_fields_list_data($sf->id);

                                        ?>
                                        <label for="">High School</label>
                                        <select name="HighSchool" class="form-control" placeholder="High School">
                                            <option value="">Select One</option>
                                            <?php
                                                foreach($search_fields_list as $sfl) {
                                                    if($sfl->field_name != 'Select One') {
                                            ?>
                                            <option value="<?=$sfl->field_value?>" <?=($this->input->get('HighSchool') && ($this->input->get('HighSchool') == $sfl->field_name)) ? "selected" : ""; ?>><?=$sfl->field_name?></option>
                                            <?php
                                                    }
                                            ?>

                                            <?php
                                                }
                                            ?>
                                        </select>
                                        <?php
                                                }
                                            }?>


                                          </div>

                                          <?php  } else {?>

                                              <div class="form-group">
                                                  <label for="">Elementary School</label>
                                                  <input type="text" class="form-control" name="ElementarySchool" placeholder="">
                                              </div>

                                               <div class="form-group">
                                                  <label for="">Middle Or JuniorSchool</label>
                                                  <input type="text" class="form-control" name="MiddleOrJuniorSchool" placeholder="">
                                              </div>

                                              <div class="form-group">
                                                  <label for="">High School</label>
                                                  <input type="text" class="form-control" name="HighSchool" placeholder="">
                                              </div>

                                         <?php }
                                        ?>

                                </div>
                            </div>
                        </div><!-- -->
                        <div class="col-md-6">
                            <div class="panel panel-default">
                                <div class="panel-heading">General Property Description</div>
                                <div class="panel-body">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <?php
                                            $show_property_type = false;
                                                foreach($search_fields as $sf) {
                                                    if($sf->field_name == 'PropertyClass') {

                                                        $search_fields_list = $this->search_model->get_mls_data_search_fields_list_data($sf->id);

                                            ?>
                                            <label for="">Property Type</label>

                                          <?php if($sf->field_haslist == '1'){?>

                                             <select id="defaultval-property-type2" name="PropertyClass" class="form-control defaultval-property-type" placeholder="<?=$sf->field_label?>">
                                                  <option value="">Select One</option>
                                                  <?php
                                                          foreach($search_fields_list as $sfl) {
                                                              if($sfl->field_name != 'Select One') {
                                                                $decoded_mlscode = json_decode($sfl->field_applies_to_json);
                                                              ?>

                                                                  <option value="<?=$sfl->field_value?>" <?=($this->input->get('PropertyClass') && ($this->input->get('PropertyClass') == $sfl->field_name)) ? "selected" : ""; ?> data-propertyclass-code='<?=$decoded_mlscode[0]?>' ><?=$sfl->field_name?></option>

                                                                <?php } ?>

                                                              <?php
                                                          }
                                                  ?>
                                              </select>
                                          <?php } else {?>

                                              <input type="text" class="form-control" name="PropertyClass" placeholder="">
                                          <?php }
                                                  $show_property_type = true;
                                                 }

                                                }

                                                if(!$show_property_type){?>
                                                  <label for="">Property Type</label>
                                                  <input type="text" class="form-control" name="PropertyClass" placeholder="">

                                          <?php  }
                                            ?>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                         <div class="form-group subtype-class" style="display:none" >
                                            <?php
                                                $show_property_subtype = false;
                                                $field_lists = array("A","B","C","D","E","F");
                                                foreach($search_fields as $sf) {
                                                    if($sf->field_name == 'PropertySubType') {

                                                        $search_fields_list = $this->search_model->get_mls_data_search_fields_list_data($sf->id);

                                            ?>
                                            <label for="">Property Sub Type</label>

                                             <?php if($sf->field_haslist == '1'){?>

                                                <?php foreach($field_lists as $field) { ?>
                                                <!-- <div class="<?=$field;?>" style="display:none"> -->
                                                    <select style="display:none" name="PropertySubType" class="form-control <?=$field;?> set-property-sub" placeholder="<?=$sf->field_label?>">
                                                      <option value="">-- Select --</option>
                                                      <?php
                                                        foreach($search_fields_list as $sfl) {
                                                          if($sfl->field_name != 'Select One') {
                                                            $array_fields_lists = json_decode($sfl->field_applies_to_json);
                                                      ?>
                                                          <?php if( in_array($field, $array_fields_lists) ){ ?>
                                                            <option value="<?=$sfl->field_value?>" <?=($this->input->get('PropertySubType') && ($this->input->get('PropertySubType') == $sfl->field_name)) ? "selected" : ""; ?>><?=$sfl->field_name?></option>
                                                          <?php } ?>

                                                      <?php
                                                          }
                                                        }
                                                      ?>
                                                    </select>
                                                  <!-- </div> -->
                                                <?php } ?>

                                                <!-- <select name="PropertySubType" class="form-control" placeholder="<?=$sf->field_label?>">
                                                    <?php
                                                        foreach($search_fields_list as $sfl) {
                                                          $array_fields_lists = json_decode($sfl->field_applies_to_json);
                                                            if($sfl->field_name == 'Select One') {?>

                                                              <option value=""><?=$sfl->field_name?></option>

                                                              <?php } else {?>
                                                                 <option value="<?=$sfl->field_value?>" <?=($this->input->get('PropertySubType') && ($this->input->get('PropertySubType') == $sfl->field_name)) ? "selected" : ""; ?>><?=$sfl->field_name?></option>
                                                              <?php
                                                            }
                                                        }
                                                    ?>
                                                </select> -->

                                             <?php } else {?>

                                                <input type="text" class="form-control" name="PropertySubType" placeholder="">
                                              <?php }
                                                      $show_property_subtype = true;
                                                    }

                                                }
                                                 if(!$show_property_subtype){?>
                                                  <label for="">Property Sub Type</label>
                                                  <input type="text" class="form-control" name="PropertySubType" placeholder="">

                                          <?php  }
                                            ?>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Minimum Price</label>
                                            <div class="input-group">
                                                <span class="input-group-addon">$</span>
                                                <input type="text" name="price_min" class="form-control input-number" maxlength="11" value="<?=($this->input->get('price_min')) ? $this->input->get('price_min') : "";?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Maximum Price</label>
                                            <div class="input-group">
                                                <span class="input-group-addon">$</span>
                                                <input type="text" name="price_max" class="form-control input-number" maxlength="11" value="<?=($this->input->get('price_max')) ? $this->input->get('price_max') : "";?>">
                                            </div>
                                        </div>
                                    </div>
                                    <!-- <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Min. Lot Size</label>
                                            <div class="input-group">
                                                <input type="text" name="area_min" class="form-control input-number" maxlength="11" value="<?=($this->input->get('area_min')) ? $this->input->get('area_min') : "";?>">
                                                <span class="input-group-addon">Sq.Ft.</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Max. Lot Size</label>
                                            <div class="input-group">
                                                <input type="text" name="area_max" class="form-control input-number" maxlength="11" value="<?=($this->input->get('area_max')) ? $this->input->get('area_max') : "";?>">
                                                <span class="input-group-addon">Sq.Ft.</span>
                                            </div>
                                        </div>
                                    </div> -->

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Min. House Sq Ft</label>
                                            <div class="input-group">
                                                <input type="text" name="house_min" class="form-control input-number" maxlength="11" value="<?=($this->input->get('house_min')) ? $this->input->get('house_min') : "";?>">
                                                <span class="input-group-addon">Sq.Ft.</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Max. House Sq Ft</label>
                                            <div class="input-group">
                                                <input type="text" name="house_max" class="form-control input-number" maxlength="11" value="<?=($this->input->get('house_max')) ? $this->input->get('house_max') : "";?>">
                                                <span class="input-group-addon">Sq.Ft.</span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Bedrooms</label>
                                            <input type="text" name="BedsTotal" class="form-control input-number" value="<?=($this->input->get('BedsTotal')) ? $this->input->get('BedsTotal') : "";?>" min="0" maxlength="4">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Bathrooms</label>
                                            <input type="text" name="BathsTotal" class="form-control input-number" value="<?=($this->input->get('BathsTotal')) ? $this->input->get('BathsTotal') : "";?>" min="0" maxlength="4">
                                        </div>
                                    </div>



                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for=""># of Interior Levels</label>
                                            <input type="text" name="BathsFull" class="form-control input-number" value="<?=($this->input->get('BathsFull')) ? $this->input->get('BathsFull') : "";?>" min="0" maxlength="4">
                                        </div>
                                    </div>
                                    <?php
                                        if($account_info->MlsId == "20070913202326493241000000") {
                                    ?>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Pool</label>
                                            <select class="form-control" name="Pool">
                                                <option value="">Pool</option>
                                                <option value="Both Private & Community"  <?=($this->input->get('Pool') && ($this->input->get('Pool') == "Both Private & Community")) ? "selected" : ""; ?>>Both Private & Community</option>
                                                <option value="Community Only" <?=($this->input->get('Pool') && ($this->input->get('Pool') == "Community Only")) ? "selected" : ""; ?>>Community Only</option>
                                                <option value="None" <?=($this->input->get('Pool') && ($this->input->get('Pool') == "None")) ? "selected" : ""; ?>>None</option>
                                                <option value="Private Only" <?=($this->input->get('Pool') && ($this->input->get('Pool') == "Private Only")) ? "selected" : ""; ?>>Private Only</option>
                                            </select>
                                        </div>
                                    </div>
                                    <?php
                                        }
                                    ?>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <?php
                                            $show_mlsStatus = false;
                                                foreach($search_fields as $sf) {
                                                    if($sf->field_name == 'MlsStatus') {

                                                        $search_fields_list = $this->search_model->get_mls_data_search_fields_list_data($sf->id);

                                            ?>
                                            <label for="">MLS Status</label>

                                            <?php if($sf->field_haslist == '1'){?>

                                                <select name="MlsStatus" class="form-control defaultval-mls-status" placeholder="<?=$sf->field_label?>">
                                                    <?php
                                                        foreach($search_fields_list as $sfl) {
                                                            if($sfl->field_name == 'Select One') {?>

                                                                <option value=""><?=$sfl->field_name?></option>

                                                            <?php } else {?>

                                                                <option value="<?=$sfl->field_value?>" <?=($this->input->get('MlsStatus') && ($this->input->get('MlsStatus') == $sfl->field_name)) ? "selected" : ""; ?>><?=$sfl->field_name?></option>
                                                    <?php
                                                                }
                                                            }
                                                    ?>
                                                </select>

                                            <?php } else { ?>

                                            <input type="text" class="form-control" name="MlsStatus" placeholder="">

                                            <?php }
                                                    $show_mlsStatus = true;
                                                    }

                                                }

                                                if(!$show_mlsStatus){?>
                                                  <label for="">Mls Status</label>
                                                  <input type="text" class="form-control" name="MlsStatus" placeholder="">

                                          <?php  }
                                            ?>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Year Built</label>
                                            <input type="text" class="form-control input-number" value="<?=($this->input->get('YearBuilt')) ? $this->input->get('YearBuilt') : "";?>" name="YearBuilt" min="0" maxlength="4">
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">Visual Options</div>
                                <div class="panel-body">
                                    <div class="col-md-6">
                                        <label class="checkbox-inline">
                                            <input type="checkbox" name="VirtualToursCount" value="1" <?=($this->input->get('VirtualToursCount') && ($this->input->get('VirtualToursCount') == 1)) ? "checked" : ""; ?>> Virtual Tour
                                        </label>
                                    </div>
                                    <div class="col-md-6">
                                        <label class="checkbox-inline">
                                            <input type="checkbox" name="VideosCount" value="1" <?=($this->input->get('VideosCount') && ($this->input->get('VideosCount') == 1)) ? "checked" : ""; ?>> Videos
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                            if($account_info->MlsId == "20070913202326493241000000") {
                        ?>
                        <hr>
                        <div class="col-md-6">
                            <div class="panel panel-default">
                                <div class="panel-heading">Roof</div>
                                <div class="panel-body">
                                    <div class="col-md-6">
                                        <label class="checkbox-inline">
                                            <input type="checkbox" name="Roof[]" value="All Tile"
                                            <?php if ($this->input->get('Roof') && !empty($this->input->get('Roof'))): ?>
                                                <?php foreach ($this->input->get('Roof') as $rf) {
                                                    $roof = $rf;
                                                    if ($rf == "All Tile") {
                                                        echo "checked";
                                                    }
                                                } ?>
                                            <?php endif ?>
                                            >All Tile
                                        </label>
                                    </div>
                                    <div class="col-md-6">
                                        <label class="checkbox-inline">
                                            <input type="checkbox" name="Roof[]" value="Built-Up"
                                            <?php if ($this->input->get('Roof') && !empty($this->input->get('Roof'))): ?>
                                                <?php foreach ($this->input->get('Roof') as $rf) {
                                                    $roof = $rf;
                                                    if ($rf == "Built-Up") {
                                                        echo "checked";
                                                    }
                                                } ?>
                                            <?php endif ?>
                                            >Built-Up
                                        </label>
                                    </div>
                                    <div class="col-md-6">
                                        <label class="checkbox-inline">
                                            <input type="checkbox" name="Roof[]" value="Comp Shingle"
                                            <?php if ($this->input->get('Roof') && !empty($this->input->get('Roof'))): ?>
                                                <?php foreach ($this->input->get('Roof') as $rf) {
                                                    $roof = $rf;
                                                    if ($rf == "Comp Shingle") {
                                                        echo "checked";
                                                    }
                                                } ?>
                                            <?php endif ?>
                                            >Comp Shingle
                                        </label>
                                    </div>
                                    <div class="col-md-6">
                                        <label class="checkbox-inline">
                                            <input type="checkbox" name="Roof[]" value="Concrete"
                                            <?php if ($this->input->get('Roof') && !empty($this->input->get('Roof'))): ?>
                                                <?php foreach ($this->input->get('Roof') as $rf) {
                                                    $roof = $rf;
                                                    if ($rf == "Concrete") {
                                                        echo "checked";
                                                    }
                                                } ?>
                                            <?php endif ?>
                                            >Concrete
                                        </label>
                                    </div>
                                    <div class="col-md-6">
                                        <label class="checkbox-inline">
                                            <input type="checkbox" name="Roof[]" value="Foam"
                                            <?php if ($this->input->get('Roof') && !empty($this->input->get('Roof'))): ?>
                                                <?php foreach ($this->input->get('Roof') as $rf) {
                                                    $roof = $rf;
                                                    if ($rf == "Foam") {
                                                        echo "checked";
                                                    }
                                                } ?>
                                            <?php endif ?>
                                            >Foam
                                        </label>
                                    </div>
                                    <div class="col-md-6">
                                        <label class="checkbox-inline">
                                            <input type="checkbox" name="Roof[]" value="Metal"
                                            <?php if ($this->input->get('Roof') && !empty($this->input->get('Roof'))): ?>
                                                <?php foreach ($this->input->get('Roof') as $rf) {
                                                    $roof = $rf;
                                                    if ($rf == "Metal") {
                                                        echo "checked";
                                                    }
                                                } ?>
                                            <?php endif ?>
                                            >Metal
                                        </label>
                                    </div>
                                    <div class="col-md-6">
                                        <label class="checkbox-inline">
                                            <input type="checkbox" name="Roof[]" value="Other (See Remarks)"
                                            <?php if ($this->input->get('Roof') && !empty($this->input->get('Roof'))): ?>
                                                <?php foreach ($this->input->get('Roof') as $rf) {
                                                    $roof = $rf;
                                                    if ($rf == "Other (See Remarks)") {
                                                        echo "checked";
                                                    }
                                                } ?>
                                            <?php endif ?>
                                            >Other (See Remarks)
                                        </label>
                                    </div>
                                    <div class="col-md-6">
                                        <label class="checkbox-inline">
                                            <input type="checkbox" name="Roof[]" value="Partial Tile"
                                            <?php if ($this->input->get('Roof') && !empty($this->input->get('Roof'))): ?>
                                                <?php foreach ($this->input->get('Roof') as $rf) {
                                                    $roof = $rf;
                                                    if ($rf == "Partial Tile") {
                                                        echo "checked";
                                                    }
                                                } ?>
                                            <?php endif ?>
                                            >Partial Tile
                                        </label>
                                    </div>
                                    <div class="col-md-6">
                                        <label class="checkbox-inline">
                                            <input type="checkbox" name="Roof[]" value="Reflective Coating"
                                            <?php if ($this->input->get('Roof') && !empty($this->input->get('Roof'))): ?>
                                                <?php foreach ($this->input->get('Roof') as $rf) {
                                                    $roof = $rf;
                                                    if ($rf == "Reflective Coating") {
                                                        echo "checked";
                                                    }
                                                } ?>
                                            <?php endif ?>
                                            >Reflective Coating
                                        </label>
                                    </div>
                                    <div class="col-md-6">
                                        <label class="checkbox-inline">
                                            <input type="checkbox" name="Roof[]" value="Rock"
                                            <?php if ($this->input->get('Roof') && !empty($this->input->get('Roof'))): ?>
                                                <?php foreach ($this->input->get('Roof') as $rf) {
                                                    $roof = $rf;
                                                    if ($rf == "Rock") {
                                                        echo "checked";
                                                    }
                                                } ?>
                                            <?php endif ?>
                                            >Rock
                                        </label>
                                    </div>
                                    <div class="col-md-6">
                                        <label class="checkbox-inline">
                                            <input type="checkbox" name="Roof[]" value="Shake"
                                            <?php if ($this->input->get('Roof') && !empty($this->input->get('Roof'))): ?>
                                                <?php foreach ($this->input->get('Roof') as $rf) {
                                                    $roof = $rf;
                                                    if ($rf == "Shake") {
                                                        echo "checked";
                                                    }
                                                } ?>
                                            <?php endif ?>
                                            >Shake
                                        </label>
                                    </div>
                                    <div class="col-md-6">
                                        <label class="checkbox-inline">
                                            <input type="checkbox" name="Roof[]" value="Sub Tile Ventilation"
                                            <?php if ($this->input->get('Roof') && !empty($this->input->get('Roof'))): ?>
                                                <?php foreach ($this->input->get('Roof') as $rf) {
                                                    $roof = $rf;
                                                    if ($rf == "Sub Tile Ventilation") {
                                                        echo "checked";
                                                    }
                                                } ?>
                                            <?php endif ?>
                                            >Sub Tile Ventilation
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                            }
                        ?>
                        <div class="col-md-6">
                            <div class="panel panel-default">
                                <div class="panel-heading">Parking Space</div>
                                <div class="panel-body">
                                    <div class="form-group">
                                        <label for="">Garage Spaces</label>
                                        <input type="text" class="form-control input-number" value="<?=($this->input->get('GarageSpaces')) ? $this->input->get('GarageSpaces') : ""; ?>" name="GarageSpaces" min="0" maxlength="4">
                                    </div>
                                    <div class="form-group">
                                        <label for="">Carport Spaces</label>
                                        <input type="text" class="form-control input-number" value="<?=($this->input->get('CarportSpaces')) ? $this->input->get('CarportSpaces') : "";?>" name="CarportSpaces"  min="0" maxlength="4">
                                    </div>
                                    <div class="form-group">
                                        <label for="">Slab Parking Spaces</label>
                                        <input type="text" class="form-control input-number" value="<?=($this->input->get('Slab_Parking_Spaces')) ? $this->input->get('Slab_Parking_Spaces') : "";?>" name="Slab Parking Spaces" min="0" maxlength="4">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-12 text-right">
                                <button type="reset" class="btn btn-primary button-reset" style="background-color:#c50000">Clear</button>
                                <button type="button" class="btn btn-primary" id="submitFormSearch">Search</button>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- END OF REFINE SEARCH -->
              </div>

            </div>
        </div>
    </section>
