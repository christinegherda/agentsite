
<section class="page-content" >
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="customer-dashboard">
                    <ul class="nav nav-tabs responsive" role="tablist">
                        <li role="presentation" class="active"><a href="#properties" aria-controls="home" role="tab" data-toggle="tab">Favorites </a></li>
                        <li role="presentation"><a href="#searches" aria-controls="home" role="tab" data-toggle="tab">Searches </a></li>
                        <li role="presentation"><a href="#showings" aria-controls="home" role="tab" data-toggle="tab">Schedule Showings </a></li>
                        <li role="presentation"><a href="#settings" aria-controls="home" role="tab" data-toggle="tab">Profile / Settings</a></li>
                    </ul>

                      <!-- Tab panes -->
                    <div class="tab-content responsive">
                        <div role="tabpanel" class="tab-pane active" id="properties">

                           <?php

                              $current_page = $offset/$limit+1;

                              if($limit == 5){
                                $start_count = $current_page * $limit - 4;
                              } else{
                                $start_count = 1;
                              }

                              $end_count = $current_page * $limit;

                              if($end_count > $total_save_properties) {
                                $end_count1 = $end_count - $total_save_properties;
                                $end_count -= $end_count1;
                              }
                           ?>

                            <div class="propCount">
                                <h3>My Saved Favorites (<?=$start_count;?>-<?=$end_count;?> of <?php echo !empty($total_save_properties) ? number_format($total_save_properties) :"" ?>)
                                </h3>
                            </div>

                            <br>
                            <div class="display-alert">
                                <?php echo $this->session->flashdata('msg'); ?>
                            </div>
                            <div class="row">
                              
                            <div id="customer-favorites" class="col-md-12">
                            <?php
                                if(!empty($token_checker)){
                                 if( !empty($save_properties) ){
                                    foreach ($save_properties as $property) { 
                                      if(isset($property->properties_details["ListingKey"]) && !empty($property->properties_details["ListingKey"])) {
                                      ?> 
                                      <div class="property-row saved-favorites" id="<?=$property->cpid?>">
                                        <div class="row">
                                          <div class="col-md-3 col-sm-3 col-xs-12">
                                            <div class="property-img">
                                              <a href="<?php echo site_url("other-property-details/".$property->property_id); ?>" target="_blank" data-original-title="" title=""><img src="<?php echo isset($property->property_photos[0]["StandardFields"]["Photos"][0]["Uri300"]) ? $property->property_photos[0]["StandardFields"]["Photos"][0]["Uri300"] : ""?>">
                                              </a>
                                            </div>
                                          </div>
                                          <div class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="property-detail-item">
                                              <p class="title"><a href="<?php echo site_url("other-property-details/".$property->property_id); ?>"><?php echo isset($property->properties_details["UnparsedAddress"]) ? $property->properties_details["UnparsedAddress"] :"" ?></a></p>
                                              <p class="description">
                                                <?php echo isset($property->properties_details["PublicRemarks"]) ? str_limit($property->properties_details["PublicRemarks"],150) : "" ?>... </p>
                                              <ul class="list-icon">
                                                  <?php if(isset($property->properties_details["BedsTotal"]) && !empty($property->properties_details["BedsTotal"])){
                                                          if(($property->properties_details["BedsTotal"] != "********")){?>
                                                          <li><i class="fa fa-bed"></i> <?=$property->properties_details["BedsTotal"]?> Bed</li>
                                                  <?php }
                                                  }?>
                                                  <?php if(isset($property->properties_details["BathsTotal"]) && !empty($property->properties_details["BathsTotal"])){
                                                          if(($property->properties_details["BathsTotal"] != "********")){?>
                                                          <li><i class="icon-toilet"></i> <?=$property->properties_details["BathsTotal"]?> Bath</li>
                                                  <?php }
                                                  }?>
                                                  <?php if(isset($property->properties_details["BuildingAreaTotal"]) && !empty($property->properties_details["BuildingAreaTotal"])){
                                                          if(($property->properties_details["BuildingAreaTotal"] != "********")){?>
                                                          <li><?=number_format($property->properties_details["BuildingAreaTotal"])?> sqft.</li> 
                                                  <?php }
                                                  }?>
                                              </ul>
                                            </div>
                                          </div>
                                          <div class="col-md-3 col-sm-3 col-xs-12">
                                              <p class="favorites-price">$<?php echo isset($property->properties_details["CurrentPrice"]) ? number_format($property->properties_details["CurrentPrice"]) : "" ?></p>
                                              <p class="favorites-request-info">
                                                   <a href="#" type="button" class="request_info" data-toggle="modal" data-target="#request-info-modal" data-listingId="<?php echo str_replace(" ",  "-", $property->properties_details["ListingKey"]); ?>" data-propertyType="other_property" data-address="<?=$property->properties_details["UnparsedAddress"];?>" data-title='Tooltip' data-trigger='hover' title='Request Information'><i class="fa fa-question-circle"></i> Request Information</a>
                                              </p>
                                              <p class="favorites-remove">
                                                  <a href="<?php echo site_url('home/customer/delete_customer_property/'); ?>" data-id="<?=$property->cpid?>" data-property-id="<?=$property->property_id?>" class="delete-save-property"><i class="fa fa-trash"></i> Remove Favorites</a>
                                              </p>                                          
                                              <?php 

                                                $prop_id = $property->properties_details["ListingKey"];
                                                $data = Modules::run('customer/get_customer_notes',$prop_id);
                                                //printA($data);
                                              ?>
                                              <?php if( !empty($data) && $data->property_id == $prop_id){?>
                                                  <p class="view-note">
                                                      <a href="#" data-toggle="modal" data-target="#customer-notes-viewmodal<?=$property->properties_details["ListingKey"]?>"><i class="fa fa-search"></i> View Note</a>
                                                  </p>

                                                 <?php } else {?>
                                                      <p class="add-note">
                                                          <a href="#" data-toggle="modal" data-target="#customer-notes-modal<?=$property->properties_details["ListingKey"]?>" data-property-id="<?=$property->properties_details["ListingKey"]?>"><i class="fa fa-plus-square"></i> Add Note</a>
                                                      </p>

                                             <?php }?>
                                          </div>
                                        </div>
                                      </div>

                                      <!-- //add note modal -->
                                      <div id="customer-notes-modal<?=$property->properties_details["ListingKey"]?>" class="modal fade modalNote" role="dialog">
                                        <div class="modal-dialog">
                                          <!-- Modal content-->
                                          <div class="modal-content">
                                            <div class="modal-header">
                                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                                              <h4 class="modal-title text-center">Add a note</h4>
                                            </div>
                                            <form action="<?php echo site_url('home/customer/customer_notes'); ?>" method="POST" class="customer-notes" id="customer-notes">
                                              <div class="modal-body">
                                                  <div class="show-mess" ></div><br/>
                                                  <input type="hidden" name="property_id" class="property_id" value="<?=$property->properties_details["ListingKey"]?>">
                                                  <div class="form-group">
                                                      <label>Note</label>
                                                      <textarea class="form-control" rows="7" name="notes" required></textarea>
                                                  </div>
                                              </div>
                                              <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-primary">Submit</button>
                                              </div>
                                            </form>
                                          </div>
                                        </div>
                                      </div> <!-- customer-notes-modal -->

                                       <!-- //view note modal -->
                                      <div id="customer-notes-viewmodal<?=$property->properties_details["ListingKey"]?>" class="modal fade modalNote" role="dialog">
                                        <div class="modal-dialog">
                                          <!-- Modal content-->
                                          <div class="modal-content">
                                            <div class="modal-header">
                                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                                              <h4 class="modal-title text-center">View note</h4>
                                            </div>
                                            <form action="<?php echo site_url('home/customer/customer_notes');?>" method="POST" class="customer-notes" id="customer-notes">
                                              <div class="modal-body">
                                               <div class="show-mess" ></div><br/>
                                               <input type="hidden" name="property_id" class="property_id" value="<?=$property->properties_details["ListingKey"]?>">
                                               <?php 
                                                  $prop_id = $property->properties_details["ListingKey"];
                                                  $data = Modules::run('customer/get_customer_notes',$prop_id);
                                                  //printA($data);exit;
                                                ?>
                                                <div class="form-group">
                                                  <label>Note</label>
                                                  <textarea class="form-control" rows="7" name="notes" required><?php echo isset($data->notes) ? $data->notes : ""?></textarea>
                                                </div>
                                              </div>
                                              <div class="modal-footer">
                                                <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
                                                <button type="submit" class="btn btn-primary">Update</button>
                                                <button type="button" class="btn btn-danger delete_note_btn" data-property_id="<?=$property->properties_details["ListingKey"]?>">Delete</button>
                                              </div>
                                            </form>
                                          </div>
                                        </div>
                                      </div> <!-- customer-notes-modal -->
                                    <?php }
                                    } ?>
                               <?php } else {?>
                                    <p class="text-center"><i>No saved properties records!</i></p>
                              <?php } ?>
                            <?php } else {?>
                                      <p class="text-center"><i>Saved Properties not available!</i></p>
                            <?php } ?>
                              <!--  -->
                              </div>
                              <div class="col-md-12">
                                  <?php if( isset($pagination) ) : ?>
                                      <div class="pagination-area pull-right">
                                          <nav>
                                              <ul class="pagination">
                                                  <?php echo $pagination; ?>
                                              </ul>
                                          </nav>
                                      </div>
                                  <?php endif; ?>
                              </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="searches">
                          <div class="saveSearch"><h3>My Saved Searches ( <?php echo isset($save_searches) ? count($save_searches) : 0; ?> )</h3></div>
                          <br>
                          <table class="table table-striped table-responsive">
                            <thead>
                                <tr>
                                    <td width="20%">Searched Links</td>
                                    <td>Date Saved</td>
                                    <td>Query Information</td>
                                    <td>Email Settings</td>
                                    <td align="center">Delete</td>
                                </tr>
                            </thead>
                            <tbody>
                              <?php 
                                if(!empty($save_searches)) : 

                                  foreach ($save_searches as $searches) : ?>
                                    <tr id="search_<?=$searches->csid?>">
                                      <td><a href='<?php echo base_url()."search-results?".$searches->url;?>' target="_blank">Run this search >></a></td>
                                      <td><?php echo date("m-d-Y", strtotime($searches->date_created))?></td>
                                      <td>
                                        <?php
                                          foreach($searches->details as $key) {

                                            $count=0;

                                            foreach($key as $k=>$val) {

                                              echo ucfirst($k)." = ";

                                              if(is_array($val)) {

                                                echo "(";

                                                $c=0;

                                                foreach($val as $v) {
                                                  $c++;
                                                  echo $v;

                                                  if(count($val) > 1) {
                                                    if($c<count($val)) {
                                                      echo ", ";
                                                    }
                                                  }

                                                }

                                                echo ") ";

                                              } else {
                                                $count++;
                                                echo ucfirst($val);

                                                if(count($key) > 1) {
                                                  if($count < count($key)) {
                                                    echo ", ";
                                                  }
                                                }
                                                
                                              }
                                            }
                                          }
                                        ?>
                                      </td>
                                      <td>On</td>
                                      <td  align="center"><a href="<?php echo site_url('home/customer/delete_customer_search/'); ?>" data-id="<?=$searches->csid?>" class="delete-save-searches" ><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Delete</a></td>

                                    </tr>

                              <?php endforeach; else : ?>
                                    <tr class="center"><td colspan="10"><i>no saved searches records!</i></td></tr>
                              <?php endif; ?>
                            </tbody>
                          </table>
                        </div>
                         <div role="tabpanel" class="tab-pane" id="showings">
                             <div class="schedShow"><h3>My Schedule Showings ( <?php echo isset($schedule_showings) ? count($schedule_showings) : 0; ?> )</h3></div>
                            <br>
                            <table class="table table-striped table-responsive">
                                <thead>
                                    <tr>
                                        <td>Property</td>
                                        <td>Schedule Date</td>
                                        <td>Date Created</td>
                                        <td>Action</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if( !empty($schedule_showings) ) { 
                                        foreach ($schedule_showings as $schedule) {?>  
                                            <tr id="showing_<?=$schedule->cssid?>">
                                                <td class="text-center"><a href="<?php echo base_url()?>other-property-details/<?=$schedule->property_id?>" target="_blank"><?php echo $schedule->property_name?></td>
                                                <td class="text-center"><?php echo date("m-d-Y", strtotime($schedule->date_scheduled))?></td>
                                                <td class="text-center"><?php echo date("m-d-Y", strtotime($schedule->created))?></td>
                                                
                                                <td class="text-center">
                                                    <a href="#" data-toggle="modal" data-target="#customer-sched-modal<?=$schedule->property_id?>" data-title="Tooltip" data-trigger="hover" title="View Message" class="view-schedule-btn"><i class="fa fa-search"></i></a>

                                                    <a href="<?php echo site_url('home/customer/delete_customer_showing/'); ?>" data-id="<?=$schedule->cssid?>" class="delete-customer-showing" data-title="Tooltip" data-trigger="hover" title="Delete Showing"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
                                                  
                                                </td>
                                            </tr>

                                             <!-- //Sched Showing note modal -->
                                            <div id="customer-sched-modal<?=$schedule->property_id?>" class="modal fade modalNote" role="dialog">
                                              <div class="modal-dialog">
                                                <!-- Modal content-->
                                                <div class="modal-content">
                                                  <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h4 class="modal-title text-center">Schedule Showing Message</h4>
                                                  </div>
                                                    <div class="modal-body">
                                                        <textarea readonly class="form-control" rows="7"><?= $schedule->message?></textarea>
                                                    </div>
                                                    <div class="modal-footer">
                                                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                    </div>
                                                </div>
                                              </div>
                                            </div> <!-- customer-sched-modal -->

                                        <?php }?>

                                    <?php } else {?>

                                        <tr class="center"><td colspan="10"><i>no schedule showing records!</i></td></tr>

                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="settings">
                            <div class="alert-flash display-status alert alert-success" style="display:none;text-align:center;margin-top: 10px;background-image: none;border-radius: 0;">Email Schedule Updated!</div>
                            
                            <div class="box-setting">
                                <h3>My Profile </h3>
                                <hr>
                                <p>
                                    Account/Email :  <?= (isset($profile->email)) ? $profile->email : $this->session->userdata("customer_email"); ; ?> (<a href="#" class="change-phone" data-toggle="modal" data-target="#change-phone-modal" data-phone-num="<?=$profile->phone;?>">change phone</a>)
                                </p>
                                <p class="text-right">
                                  <a href="#" type="button" class="btn btn-primary" id="customer-edit-profile" data-toggle="modal" data-target="#customer-profile-modal"><span class="fa fa-edit"></span>  <span aria-hidden="true">Edit Profile</span></a>
                                </p>
                            </div>  

                            <div class="box-setting">
                                <h3>Email Preferences </h3>
                                <hr>
                                <div class="row">
                                    <div class="col-md-6">
                                        <p>Email Listings Updates: <input type="checkbox" name="email_updates"></p>
                                    </div>
                                    <div class="col-md-6 mobile-ipad-padding-0">
                                        <div class="col-md-4 mobilepadding-0">
                                            <label for="Select Frequency">Select Frequency: </label>
                                        </div>
                                        <div class="col-md-6 mobilepadding-0">
                                            <select name="email_preference" class="form-control">
                                                <option value="daily">Daily</option>
                                                <option value="weekly">Weekly</option>
                                                <option value="monthly">Monthly</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>  
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div id="request-info-modal" class="modal fade" tabindex="-1" role="dialog">
<div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Contact Us</h4>
        <p>We look forward to helping you. Please add any additional comments below and we will be in touch with you shortly.</p>
      </div>
      <div class="modal-body">
            <div class="show-mess" ></div><br/>
            <form action="<?php echo site_url('home/customer/request_information'); ?>" method="POST" class="customer-request-info" id="customer-request-info" >
                <input type="hidden" name="property_id">
                <input type="hidden" name="property_address">
                <input type="hidden" name="property_type">
                <div class="form-group">
                    <label for="first_name">First Name</label>
                    <input type="first_name" name="first_name" class="form-control" id="first-name" value="<?= (isset($profile->first_name)) ? $profile->first_name : "" ; ?>" placeholder="First Name" required>
                </div> 

                <div class="form-group">
                    <label for="last_name">Last Name</label>
                    <input type="last_name" name="last_name" class="form-control" id="last-name" value="<?= (isset($profile->last_name)) ? $profile->last_name : "" ; ?>" placeholder="Last Name" required>
                </div>

                 <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" name="email" class="form-control" id="email" value="<?= (isset($profile->email)) ? $profile->email : "" ; ?>" placeholder="Email" required>
                </div> 
                <div class="form-group">
                    <label for="phone">Phone</label>
                    <input type="phone" name="phone" class="form-control phone_number" id="phone" value="<?= (isset($profile->phone)) ? $profile->phone : "" ; ?>" placeholder="Phone Number" required>
                </div>
                 <div class="form-group">
                    <label for="comments">Comments, Questions, Special Requests?</label>
                    <textarea rows="4" cols="50" name="comments" class="form-control" id="phone" placeholder="Comments, Questions, Special Requests" required></textarea>
                </div>                     
            
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Submit  <i class="submit-loader"></i></button>
      </div>
       </form>
    </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="change-phone-modal" class="modal fade" tabindex="-1" role="dialog">
<div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Change Phone Number</h4>
      </div>
      <div class="modal-body">
        <div class="show-mess" ></div><br/>
        <form action="<?php echo site_url('home/customer/change_phone'); ?>" method="POST" class="customer-change-phone" id="customer-change-phone" >
            <div class="form-group">
                <label for="phone">New Phone Number</label>
                <input type="phone" name="new_phone" class="form-control phone_number" id="new-phone" placeholder="New Phone Number">
            </div>       
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
       </form>
    </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="customer-profile-modal" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Edit Profile</h4>
      </div>
      <div class="modal-body">
        <div class="show-mess" ></div><br/>
        <?php $profile_url = (isset($profile->id)) ? "home/customer/update_profile/".$profile->id : "home/customer/update_profile"; ?>
        <form action="<?php echo base_url().$profile_url; ?>" method="post" class="customer-profile" id="customer-profile" >
          <div class="form-group">
              <label for="exampleInputEmail1">First Name</label>
              <input type="text" name="profile[first_name]" value="<?= (isset($profile->first_name)) ? $profile->first_name : "" ; ?>" class="form-control" id="fname" placeholder="First Name" required >
          </div> 

          <div class="form-group">
              <label for="exampleInputEmail1">Last Name</label>
              <input type="text" name="profile[last_name]" value="<?= (isset($profile->last_name)) ? $profile->last_name : "" ; ?>" class="form-control" id="lname" placeholder="Last Name" required>
          </div>

          <div class="form-group">
              <label for="exampleInputEmail1">Email Address</label>
              <input type="email" name="profile[email]" value="<?= (isset($profile->email)) ? $profile->email : $this->session->userdata("customer_email"); ; ?>" class="form-control" id="email" placeholder="Email Address" required>
          </div>
          
          <div class="form-group">
              <label for="exampleInputEmail1">Phone</label>
              <input type="text" name="profile[phone]" value="<?= (isset($profile->phone)) ? $profile->phone : "" ; ?>" class="form-control phone_number" id="phone" placeholder="Phone Number" required>
          </div>
          <!-- <div class="form-group">
              <label for="exampleInputEmail1">Mobile</label>
              <input type="text" name="profile[mobile]" value="<?= (isset($profile->mobile)) ? $profile->mobile : "" ; ?>" class="form-control" id="mobile" placeholder="Mobile Number" required>
          </div>

          <div class="form-group">
              <label for="exampleInputEmail1">State</label>
              <input type="text" name="profile[state]" value="<?= (isset($profile->state)) ? $profile->state : "" ; ?>" class="form-control" id="state" placeholder="State" required>
          </div>

          <div class="form-group">
              <label for="exampleInputEmail1">City</label>
              <input type="text" name="profile[city]" value="<?= (isset($profile->city)) ? $profile->city : "" ; ?>" class="form-control" id="city" placeholder="City" required>
          </div>

          <div class="form-group">
              <label for="exampleInputEmail1">Address</label>
              <input type="text" name="profile[address]" value="<?= (isset($profile->address)) ? $profile->address : "" ; ?>" class="form-control" id="address" placeholder="Address" required>
          </div> -->
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary edit-profile-btn" data-loading-text="Loading...">Submit</button>
      </div>
       </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->