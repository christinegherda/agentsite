
    <section class="saved-search-area">
        <div class="search-listings">
            <?php
                $count = 0;

                foreach($saved_searches['data'] as $saved){

                    if($count < 6) { ?>
                
                    <div class="col-md-4 col-sm-6">
                        <div class="search-listing-item">
                            <div class="search-property-image <?php if($saved['StandardFields']['PropertyClass'] == 'Land' || $saved['StandardFields']['PropertyClass'] == 'MultiFamily'){ echo "property-image-land";} ?>">
                                <?php if(isset($saved['Photos']['Uri300'])) { ?>
                                    <a href="<?= base_url();?>other-property-details/<?= $saved['StandardFields']['ListingKey']; ?>"><img src="<?=$saved['Photos']['Uri300']?>" alt="<?php echo $saved['StandardFields']['UnparsedFirstLineAddress']; ?>" class="img-responsive" style="width:100%;"></a>
                                <?php } else { ?>
                                    <a href="<?= base_url();?>other-property-details/<?= $saved['StandardFields']['ListingKey']; ?>"><img src="<?=base_url()?>assets/images/image-not-available.jpg" alt="<?php echo $saved['StandardFields']['UnparsedFirstLineAddress']; ?>" class="img-responsive" style="width:100%;"></a>
                                <?php } ?>
                            </div>
                            <div class="property-listing-status">
                                <?php echo $saved['StandardFields']['MlsStatus'];?> 
                            </div>
                            <div class="search-listing-type">
                                <?=$saved['StandardFields']['PropertyClass'];?> 
                            </div>
                            <div class="search-listing-title">
                                <div class="col-md-9 col-xs-9 padding-0">
                                    <p class="search-property-title">
                                        <a class="listing-link" href="<?= base_url();?>other-property-details/<?= $saved['StandardFields']['ListingKey']; ?>">
                                            <?php echo $saved['StandardFields']['UnparsedFirstLineAddress']; ?>
                                        </a>
                                    </p>
                                </div>
                                <div class="col-md-3 col-xs-3 padding-0">
                                    <p class="property-action">
                                        <?php  
                                            $property_saved = FALSE; 
                                            if(isset($_SESSION["save_properties"])) :
                                                foreach($_SESSION["save_properties"] as $key) :
                                                    if($key['property_id'] == $saved['StandardFields']['ListingKey']) : 
                                                        $property_saved = TRUE; 
                                                        break; 
                                                    endif;
                                                endforeach;
                                            else : 
                                                $property_saved = FALSE; 
                                            endif;
                                        ?>

                                        <a href="#" data-action="<?=site_url('home/home/save_favorates_properties/'.$saved['StandardFields']['ListingKey']);?>" data-pro-id="<?=$saved['StandardFields']['ListingKey']?>" data-property-name="<?=$saved['StandardFields']['UnparsedAddress'];?>" class="save-favorate <?=($property_saved) ? 'saveproperty' : '';?>" data-title="Tooltip" data-trigger="hover" title="<?=($property_saved) ? "Saved Property" : "Save Property";?>">
                                            <span id="isSaveFavorate_<?=$saved['StandardFields']['ListingKey']?>">
                                                <?= ($property_saved) ? "<i class='fa fa-heart'></i>" : "<i class='fa fa-heart-o'></i>";?>
                                            </span>
                                        </a>
                                        
                                        <a href="#" type="button" class="request_info" data-toggle="modal" data-target="#request-info-modal" data-listingId="<?php echo str_replace(" ",  "-", $saved['StandardFields']['ListingKey']); ?>" data-propertyType="other_property" data-address="<?=$saved['StandardFields']["UnparsedAddress"];?>" title="Request Information"><span class="fa fa-question-circle"></span>
                                        </a>                                        
                                        
                                    </p>
                                </div>  
                            </div>
                            <p class="search-property-location"><i class="fa fa-map-marker"></i> 
                                <?php
                                    if($account_info->Mls != "MLS BCS"){
                                            $mystring = $saved['StandardFields']['City'];
                                            $findme   = '*';
                                            $pos = strpos($mystring, $findme);

                                        if($pos === false)
                                            echo $saved['StandardFields']['City'] . ", " . $saved['StandardFields']['StateOrProvince'] . " " . $saved['StandardFields']['PostalCode'];
                                            else
                                            echo $saved['StandardFields']['PostalCity'] . ", " . $saved['StandardFields']['StateOrProvince'] . " " . $saved['StandardFields']['PostalCode'];

                                    }else{
                                        $mystring = $saved["StandardFields"]["City"];
                                        $StateOrProvince = $saved["StandardFields"]["StateOrProvince"];
                                        $findme   = '*';
                                        $pos = strpos($mystring, $findme);
                                        $pCode = strpos($StateOrProvince, $findme);

                                        if($pos === false && $pCode == false)
                                            echo $saved["StandardFields"]["City"];
                                        else
                                            echo $saved["StandardFields"]["PostalCity"] . ", " . $saved["StandardFields"]["StateOrProvince"];
                                    }
                                ?>
                            </p>
                            <p><i class="fa fa-usd"></i> <?=number_format($saved['StandardFields']['CurrentPrice']);?></p>
                            <ul class="list-inline search-property-specs">
                                <?php
                                   if($saved['StandardFields']['BedsTotal'] && is_numeric($saved['StandardFields']['BedsTotal'])) {?>

                                    <li><i class="fa fa-bed"></i> <?=$saved['StandardFields']['BedsTotal']?> Bed</li>

                                <?php } elseif(empty($saved['StandardFields']['BedsTotal'])){?>

                                    <li><i class="fa fa-bed"></i> N/A</li>

                               <?php }?>

                               <?php
                                   if($saved['StandardFields']['BathsTotal'] && is_numeric($saved['StandardFields']['BathsTotal'])) {?>

                                    <li><i class="icon-toilet"></i> <?=$saved['StandardFields']['BathsTotal']?> Bath</li>

                                <?php } elseif(empty($saved['StandardFields']['BathsTotal'])){?>

                                    <li><i class="icon-toilet"></i> N/A</li>

                               <?php }?>

                                <?php
                                if(!empty($saved['StandardFields']['BuildingAreaTotal']) && ($saved['StandardFields']['BuildingAreaTotal'] != "0")   && is_numeric($saved['StandardFields']['BuildingAreaTotal'])) {?>

                                <li class="lot-item"><?=number_format($saved['StandardFields']['BuildingAreaTotal'])?> sqft</li>

                             <?php } elseif(!empty($saved['StandardFields']['LotSizeArea']) && ($saved['StandardFields']['LotSizeArea'] != "0")   && is_numeric($saved['StandardFields']['LotSizeArea'])) {

                                if(!empty($saved['StandardFields']['LotSizeUnits']) && ($saved['StandardFields']['LotSizeUnits']) === "Acres"){?>
                                    <li>Lot Size: <?=number_format($saved['StandardFields']['LotSizeArea'], 2, '.', ',' )?> acres</li>

                                <?php } else {?>

                                    <li>Lot Size: <?=number_format($saved['StandardFields']['LotSizeArea'])?> acres</li>

                                <?php }?>

                            <?php } elseif(!empty($saved['StandardFields']['LotSizeSquareFeet']) && ($saved['StandardFields']['LotSizeSquareFeet'] != "0")   && is_numeric($saved['StandardFields']['LotSizeSquareFeet'])) {?>

                                    <li>Lot Size: <?=number_format($saved['StandardFields']['LotSizeSquareFeet'])?> sqft</li> 

                             <?php } elseif(!empty($saved['StandardFields']['LotSizeAcres']) && ($saved['StandardFields']['LotSizeAcres'] != "0")   && is_numeric($saved['StandardFields']['LotSizeAcres'])) {?>

                                    <li>Lot Size: <?=number_format($saved['StandardFields']['LotSizeAcres'],2 ,'.',',')?> acres</li>

                             <?php } elseif(!empty($saved['StandardFields']['LotSizeDimensions']) && ($saved['StandardFields']['LotSizeDimensions'] != "0")   && ($saved['StandardFields']['LotSizeDimensions'] != "********")) {?>

                                   <li class="lot-item">Lot Size: <?=$saved['StandardFields']['LotSizeDimensions']?></li>
                            <?php } else {?>
                                    <li class="lot-item">Lot Size: N/A</li>
                            <?php } ?>
                            </ul>
                        </div>
                    </div>

                <?php  } $count++; 

                }

                if($count < 1) { ?>
                    <div class="col-md-12 col-sm-6 bg-danger text-center" style="padding: 20px;">
                        <h4>No Property Listings!</h4>
                    </div>

                <?php }?>
        </div>

                <?php if(isset($saved_searches['data']) AND !empty($saved_searches['data'])){
                        if (count($saved_searches['data']) > 5){?>
                         <div class="col-md-12 col-sm-12 text-center">
                            <div class="col-md-12 line-container">
                                    <p class="submit-line"></p>    
                            </div>

                              <a href="<?php echo base_url()?>saved_searches/<?php echo $search_id?>" target="_blank" class="btn btn-default submit-button">View All</a>  
                             
                        </div>
                        
            <?php       }
                    } 
            ?>
    </section>