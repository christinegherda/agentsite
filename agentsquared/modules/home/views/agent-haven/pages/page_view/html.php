<?php
  $sitemap = json_decode(isset( $settings->html_sitemap )? $settings->html_sitemap : '{}');
?>
<link href="<?php echo base_url().'assets/font-awesome/css/font-awesome.css';?>" rel="stylesheet" />
<section class="html-sitemap">
  <div class="wrapper">
    <div class="col-md-6">
      <?php if(isset($sitemap->main_pages)){ if($sitemap->main_pages){ ?>
        <ul>
          <li><a href="/">Home</a></li>
          <?php foreach($pages as $page){?>
            <?php if($page->menu_type == 'default_menu' && $page->is_menu == '1' && $page->slug != 'home'){?>
              <?php if ($plan_id == 1) { ?>
                <?php if ($page->slug != 'seller' && $page->slug != 'buyer'): ?>
                  <li><a href="<?php echo base_url().$page->slug;?>"><?php echo $page->title;?></a></li>
                <?php endif ?>
              <?php } else { ?>
                <li><a href="<?php echo base_url().$page->slug;?>"><?php echo $page->title;?></a></li>
              <?php }?>
            <?php }?>
          <?php }?>
        </ul>
      <?php }}else{ ?>
        <ul>
          <li><a href="/">Home</a></li>
          <?php foreach($pages as $page){?>
            <?php if($page->menu_type == 'default_menu' && $page->is_menu == '1' && $page->slug != 'home'){?>
              <?php if ($plan_id == 1) { ?>
                <?php if ($page->slug != 'seller' && $page->slug != 'buyer'): ?>
                  <li><a href="<?php echo base_url().$page->slug;?>"><?php echo $page->title;?></a></li>
                <?php endif ?>
              <?php } else { ?>
                <li><a href="<?php echo base_url().$page->slug;?>"><?php echo $page->title;?></a></li>
              <?php }?>
            <?php }?>
          <?php }?>
        </ul>
      <?php } ?>
      <?php if(isset($sitemap->custom_menu)){ ?>
        <?php if($sitemap->custom_menu){ ?>
          <ul>
            <?php $allpages = $pages;?>
            <?php foreach($pages as $page){?>
              <?php if($page->parent_menu_id == '0'  && $page->menu_type == 'custom_menu' && $page->is_menu == '1' && $page->page_type == 'page'){?>
            <li>
              <a href="<?php echo base_url().'page/'.$page->slug;?>"><?php echo $page->title;?></a>
              <?php 
                $subpages = array();
                foreach($allpages as $innerpage){
                  if($page->id == $innerpage->parent_menu_id && $innerpage->parent_menu_id != '0' && $innerpage->menu_type == 'custom_menu' && $innerpage->is_menu == '1' && $innerpage->page_type == 'page'){
                    array_push($subpages, $innerpage);
                  }
                }
              ?>
              <?php if(!empty($subpages)){?>
              <ul>
                <?php foreach($subpages as $subpage){?>
                <li><a href="<?php echo base_url().'page/'.$subpage->slug;?>"><?php echo $subpage->title;?></a></li>
                <?php }?>
              </ul>
              <?php }?>
            </li>
              <?php }?>
            <?php }?>
          </ul>
        <?php } ?>
      <?php }else{ ?>
        <ul>
          <?php $allpages = $pages;?>
          <?php foreach($pages as $page){?>
            <?php if($page->parent_menu_id == '0'  && $page->menu_type == 'custom_menu' && $page->is_menu == '1' && $page->page_type == 'page'){?>
          <li>
            <a href="<?php echo base_url().'page/'.$page->slug;?>"><?php echo $page->title;?></a>
            <?php 
              $subpages = array();
              foreach($allpages as $innerpage){
                if($page->id == $innerpage->parent_menu_id && $innerpage->parent_menu_id != '0' && $innerpage->menu_type == 'custom_menu' && $innerpage->is_menu == '1' && $innerpage->page_type == 'page'){
                  array_push($subpages, $innerpage);
                }
              }
            ?>
            <?php if(!empty($subpages)){?>
            <ul>
              <?php foreach($subpages as $subpage){?>
              <li><a href="<?php echo base_url().'page/'.$subpage->slug;?>"><?php echo $subpage->title;?></a></li>
              <?php }?>
            </ul>
            <?php }?>
          </li>
            <?php }?>
          <?php }?>
        </ul>
      <?php } ?>
    </div>
    <div class="col-md-6">
      <?php if(isset($sitemap->active)){?>
        <?php if($sitemap->active && isset($active_listings) && !empty($active_listings)){ ?>
          <h4>Active Listings</h4>
          <ul>
            <?php foreach($active_listings as $active){
            $url_rewrite = url_title("{$active->StandardFields->UnparsedFirstLineAddress} {$active->StandardFields->PostalCode}");
            ?>
            <li><a href="<?php echo base_url().'property-details/'.$url_rewrite.'/'.$active->Id;?>"><?php echo $active->StandardFields->UnparsedAddress;?></a></li>
            <?php }?>
          </ul>
        <?php } ?>
      <?php }else{ ?>
        <?php if(isset($active_listings) && !empty($active_listings)){ ?>
          <h4>Active Listings</h4>
          <ul>
            <?php foreach($active_listings as $active){
            $url_rewrite = url_title("{$active->StandardFields->UnparsedFirstLineAddress} {$active->StandardFields->PostalCode}");
            ?>
            <li><a href="<?php echo base_url().'property-details/'.$url_rewrite.'/'.$active->Id;?>"><?php echo $active->StandardFields->UnparsedAddress;?></a></li>
            <?php }?>
          </ul>
        <?php } ?>
      <?php } ?>
      <?php if(isset($sitemap->office)){?>
        <?php if($sitemap->office && isset($office_listings) && !empty($office_listings)){ ?>
          <h4>Office Listings</h4>
          <ul>
            <?php foreach($office_listings as $office){?>
            <li><a href="<?php echo base_url().'other-property-details/'.$office->Id;?>"><?php echo $office->StandardFields->UnparsedAddress;?></a></li>
            <?php }?>
          </ul>
        <?php } ?>
      <?php }else{ ?>
        <?php if(isset($office_listings) && !empty($office_listings)){ ?>
          <h4>Office Listings</h4>
          <ul>
            <?php foreach($office_listings as $office){?>
            <li><a href="<?php echo base_url().'other-property-details/'.$office->Id;?>"><?php echo $office->StandardFields->UnparsedAddress;?></a></li>
            <?php }?>
          </ul>
        <?php } ?>
      <?php } ?>
      <?php if(isset($sitemap->sold)){?>
        <?php if($sitemap->sold && isset($sold_listings) && !empty($sold_listings)) { ?>
          <h4>Sold Listings</h4>
          <ul>
            <?php foreach($sold_listings as $sold){
            $url_rewrite = url_title("{$sold->StandardFields->UnparsedFirstLineAddress} {$sold->StandardFields->PostalCode}");
            ?>
            <li><a href="<?php echo base_url().'property-details/'.$url_rewrite.'/'.$sold->Id;?>"><?php echo $sold->StandardFields->UnparsedAddress;?></a></li>
            <?php }?>
          </ul>
        <?php } ?>
      <?php }else{ ?>
        <?php if(isset($sold_listings) && !empty($sold_listings)){ ?>
          <h4>Sold Listings</h4>
          <ul>
            <?php foreach($sold_listings as $sold){
            $url_rewrite = url_title("{$sold->StandardFields->UnparsedFirstLineAddress} {$sold->StandardFields->PostalCode}");
            ?>
            <li><a href="<?php echo base_url().'property-details/'.$url_rewrite.'/'.$sold->Id;?>"><?php echo $sold->StandardFields->UnparsedAddress;?></a></li>
            <?php }?>
          </ul>
        <?php } ?>
      <?php } ?>
      <?php if(isset($sitemap->new)){?>
        <?php if($sitemap->new && isset($new_listings) && !empty($new_listings)){ ?>
          <h4>New Listings</h4>
          <ul>
            <?php foreach($new_listings as $new){?>
            <li><a href="<?php echo base_url().'other-property-details/'.$new->StandardFields->ListingKey;?>"><?php echo $new->StandardFields->UnparsedAddress;?></a></li>
            <?php }?>
          </ul>
        <?php } ?>
      <?php }else{ ?>
        <?php if(isset($new_listings) && !empty($new_listings)){ ?>
          <h4>New Listings</h4>
          <ul>
            <?php foreach($new_listings as $new){?>
            <li><a href="<?php echo base_url().'other-property-details/'.$new->StandardFields->ListingKey;?>"><?php echo $new->StandardFields->UnparsedAddress;?></a></li>
            <?php }?>
          </ul>
        <?php } ?>
      <?php } ?>
    </div>
  </div>
</section>
