<section class="advance-search-area">
            <form method="get" action="<?php echo base_url(); ?>listings" class="advance-search-form">
                <div class="visible-xs">
                    <!-- <button class="btn btn-default submit-button btn-show-filter">Show Filters</button> -->
                    <a href="javascript:void(0);" class="btn btn-default submit-button btn-show-filter"><i class="fa fa-search"></i> Show Filters</a>
                </div>
                <nav class="navbar  navbar-search">
                    <div class="container">
                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <div class="navbar-form navbar-left nav-search-box" role="search">
                                <div class="form-group">
                                    <input type="text" class="form-control input-search" placeholder="Search for..." name="Search" maxlength="100" <?php
                                            if(isset($_GET['Search']) && $_GET['Search']) {
                                                echo "value='".$_GET['Search']."'";
                                            }
                                        ?>>
                                    <input type="hidden" name="Search" value="<?=($this->input->get('Search')) ? $this->input->get('Search') : ''; ?>">
                                    <input type="hidden" name="offset" value="<?=($this->input->get('offset')) ? $this->input->get('offset') : ''; ?>">
                                    <input type="hidden" name="page" value="<?=($this->input->get('page')) ? $this->input->get('page') : '1'; ?>">
                                    <input type="hidden" name="totalrows" value="<?=($this->input->get('totalrows')) ? $this->input->get('totalrows') : ''; ?>">
                                    <input type="hidden" name="current_segment" value="<?=($this->uri->segment(1)) ? $this->uri->segment(1) : ''; ?>">
                                    <input type="hidden" name="search_id" value="<?=($this->uri->segment(1) == 'saved_searches_new') ? $this->uri->segment(2) : ''; ?>">
                                </div>
                            </div>
                            <ul class="nav navbar-nav price-input">
                                <li class="dropdown">
                                    <a href="#"><i class="fa fa-usd" aria-hidden="true"></i> <span class="search-label">Price</span> (<span id="price_label"><?php
                                        if(isset($_GET['min_price']) && $_GET['min_price']) {
                                            if(isset($_GET['max_price']) && $_GET['max_price']) {
                                                echo number_format($_GET['min_price'])." - ".number_format($_GET['max_price']);
                                            }
                                            else {
                                                echo number_format($_GET['min_price']);
                                            }
                                        }
                                        if(isset($_GET['max_price']) && $_GET['max_price'] && !(isset($_GET['min_price']) || $_GET['min_price'])) { 
                                            echo number_format($_GET['max_price']);
                                        }
                                        if(
                                            !(isset($_GET['min_price']) && $_GET['min_price']) && !(isset($_GET['max_price']) && $_GET['max_price'])
                                        ) {
                                            echo "Any";
                                        }

                                    ?></span>)<span class="caret"></span></a>
                                    <div class="dropdown-menu">
                                        <div class="col-md-12">
                                            <label>From</label>
                                            <input type="text" class="form-control" name="min_price" id="min_price" maxlength="12" <?php if(isset($_GET['min_price']) && $_GET['min_price']) { echo "value='".$_GET['min_price']."'"; } ?>>
                                            
                                            <label>To</label>
                                            <input type="text" class="form-control" name="max_price" id="max_price" maxlength="12" <?php if(isset($_GET['max_price']) && $_GET['max_price']) { echo "value='".$_GET['max_price']."'"; } ?>>
                                            <a href="" class="dismiss-dropdown" style="width:100%;display: block;text-align: center;padding: 5px;"><small>Dismiss</small></a>                                            
                                        </div>
                                    </div>
                                </li>
                            </ul>
                            <ul class="nav navbar-nav">
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="fa fa-bed" aria-hidden="true"></i>  <span class="search-label">Beds</span> (<span id="bedroom_label"><?php if(isset($_GET['bedroom']) && $_GET['bedroom'] ) { echo $_GET['bedroom']."+"; } else { echo "Any";} ?></span>)<span class="caret"></span>
                                    </a>
                                    <input type="hidden" name="bedroom" id="bedroom" <?php if(isset($_GET['bedroom']) && $_GET['bedroom'] ) { echo "value='".$_GET['bedroom']."'"; } ?>>
                                    <ul class="dropdown-menu">
                                        <li><a href="" onclick="setFieldValue('bedroom', 'Any')">Any</a></li>
                                        <li><a href="" onclick="setFieldValue('bedroom', '1')">1+</a></li>
                                        <li><a href="" onclick="setFieldValue('bedroom', '2')">2+</a></li>
                                        <li><a href="" onclick="setFieldValue('bedroom', '3')">3+</a></li>
                                        <li><a href="" onclick="setFieldValue('bedroom', '4')">4+</a></li>
                                        <li><a href="" onclick="setFieldValue('bedroom', '5')">5+</a></li>
                                        <li><a href="" onclick="setFieldValue('bedroom', '6')">6+</a></li>
                                        <li><a href="" onclick="setFieldValue('bedroom', '7')">7+</a></li>
                                        <li><a href="" onclick="setFieldValue('bedroom', '8')">8+</a></li>
                                    </ul>
                                </li>
                            </ul>
                            <ul class="nav navbar-nav">
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="icon-toilet" aria-hidden="true"></i> <span class="search-label">Baths</span> (<span id="bathroom_label"><?php if(isset($_GET['bathroom']) && $_GET['bathroom'] ) { echo $_GET['bathroom']."+"; } else { echo "Any";} ?></span>)<span class="caret"></span>
                                    </a>
                                    <input type="hidden" name="bathroom" id="bathroom" <?php if(isset($_GET['bathroom']) && $_GET['bathroom'] ) { echo "value='".$_GET['bathroom']."'"; } ?>>
                                    <ul class="dropdown-menu">
                                        <li><a href="" onclick="setFieldValue('bathroom', 'Any')">Any</a></li>
                                        <li><a href="" onclick="setFieldValue('bathroom', '1')">1+</a></li>
                                        <li><a href="" onclick="setFieldValue('bathroom', '2')">2+</a></li>
                                        <li><a href="" onclick="setFieldValue('bathroom', '3')">3+</a></li>
                                        <li><a href="" onclick="setFieldValue('bathroom', '4')">4+</a></li>
                                        <li><a href="" onclick="setFieldValue('bathroom', '5')">5+</a></li>
                                        <li><a href="" onclick="setFieldValue('bathroom', '6')">6+</a></li>
                                        <li><a href="" onclick="setFieldValue('bathroom', '7')">7+</a></li>
                                        <li><a href="" onclick="setFieldValue('bathroom', '8')">8+</a></li>
                                    </ul>
                                </li>
                            </ul>
                            <ul class="nav navbar-nav more-filters">
                                <li class="dropdown">
                                    <?php
                                        if(isset($system_info) && $system_info->MlsId == '20070913202326493241000000') {
                                    ?>
                                    <a href="<?=base_url();?>advanced-search" ></i> Advance Search <span class="search-label">(Show)</span></span></a>
                                    <?php
                                        }
                                        else {
                                    ?>
                                    <a href="javascript:void(0);" id="moreFiltersToggle" class="moreFiltersToggle"></i> Advance Search <span class="search-label">(Show)</span></span></a>
                                    <?php } ?>
                                </li>
                            </ul>
                            <ul class="nav navbar-nav submit-filters">
                                <li class="dropdown">
                                    <button type="submit"  class="submit-button submit-general"> <i class="fa fa-search"></i> <span class="search-label">Search</span> </button>
                                </li>
                            </ul>
                            <ul class="nav navbar-nav pull-right show-map-area">
                                <li class="dropdown">
                                    <a href="#" class="view-map-button"><i class="fa fa-map"></i> <span class="show-map-label">Show map</span> </a>
                                </li>

                                         <?php  
                                            $saved_searches = FALSE; 
                                            if(isset($saved_searches_db) && !empty($saved_searches_db)) :
                                                foreach($saved_searches_db as $searches) :
                                                    if($searches->url == http_build_query($_GET)) : 
                                                        $saved_searches = TRUE; 
                                                        break; 
                                                    endif;
                                                endforeach;
                                            else : 
                                                $saved_searches = FALSE; 
                                            endif;
                                        ?>

                                <?php  if($this->uri->segment(1) == 'listings'){?>
                                    <li>
                                     <?php if(!isset($_SESSION['customer_id'])) { ?>
                                            <a href="javascript:void(0)" class="saved_search modalogin" data-toggle='modal' data-target='.customer_login_signup' data-redirect-customer="listings?Search=<?=$this->input->get('Search').'&success';?>" title="Save Search"><i class="fa fa-undo"></i> <span class="search-label">Save Search</span>
                                            </a>
                                        <?php } else { ?>    
                                           <a href="<?php echo site_url("home/home/save_search_properties?".http_build_query($_GET)); ?>" type="button" id="moreFiltersToggle" class="<?=($saved_searches) ? "follow-search-red" : "follow-search"?>"> 

                                           <?php if($saved_searches){?>

                                                <i class="fa fa-save"></i> <span class="search-label">Saved Search</span>
                                           <?php } else {?>

                                            <i class="fa fa-undo"></i> <span class="search-label">Save Search</span>

                                               <?php  }?> </a>
                                            </a>
                                        <?php } ?>
                                    </li>
                               <?php }?>
                                   
                            </ul>
                        </div><!-- /.navbar-collapse -->
                    </div> 
                </nav>
                <div class="container" id="more-filters-area" style="display:none;">
                    <div class="row more-filter">
                        <div class="container">
                       <!--  <div class="col-md-12">
                            <div class="alert alert-danger">
                                <p><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> This section is still under development. Click <a href="" class="moreFiltersToggle">here</a> to hide this area</p>
                            </div>
                        </div> -->
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <div class="form-group" id="property_type_container">
                                <h3>Property Types</h3>
                                <hr>
                                <?php 
                                    if($property_types) {
                                        foreach($property_types as $type=>$val) { ?>
                                            <div class="checkbox"><label><input name="property_type[]" value="<?=$val["PropertyClass"];?>" type="checkbox" id="<?=$val["MlsCode"];?>"><?=$val["MlsName"];?></label></div>
                                <?php   } 
                                    }
                                ?>
                            </div>
                            <div class="form-group" id="property_sub_type_container" style="display:none;">
                                <h3>Property Sub Types</h3>
                                <?php 
                                    if($property_subtypes) { ?>
                                        <div id="type_a" style="display:none;"> <?php
                                            foreach($property_subtypes as $subtype) {
                                                if($subtype['Name'] != 'Select One') {
                                                    foreach($subtype["AppliesTo"] as $st) {
                                                        if($st=="A") { ?>
                                                            <div class="checkbox"><label><input name="a_type[]" value="<?=$subtype['Name']?>" type="checkbox"><?=$subtype['Name']?></label></div> <?php
                                                        }
                                                    }
                                                }
                                            } ?>
                                        </div> 
                                        <div id="type_b" style="display:none;"> <?php
                                            foreach($property_subtypes as $subtype) {
                                                if($subtype['Name'] != 'Select One') {
                                                    foreach($subtype["AppliesTo"] as $st) { 
                                                        if($st=="B") { ?>
                                                            <div class="checkbox"><label><input name="b_type[]" value="<?=$subtype['Name']?>" type="checkbox"><?=$subtype['Name']?></label></div> <?php
                                                        }
                                                    }
                                                }
                                            } ?>
                                        </div>
                                        <div id="type_c" style="display:none;"> <?php
                                            foreach($property_subtypes as $subtype) {
                                                if($subtype['Name'] != 'Select One') {
                                                    foreach($subtype["AppliesTo"] as $st) { 
                                                        if($st=="C") { ?>
                                                            <div class="checkbox"><label><input name="c_type[]" value="<?=$subtype['Name']?>" type="checkbox"><?=$subtype['Name']?></label></div> <?php
                                                        }
                                                    }
                                                }
                                            } ?>
                                        </div>
                                        <div id="type_d" style="display:none;"> <?php
                                            foreach($property_subtypes as $subtype) {
                                                if($subtype['Name'] != 'Select One') {
                                                    foreach($subtype["AppliesTo"] as $st) { 
                                                        if($st=="D") { ?>
                                                            <div class="checkbox"><label><input name="d_type[]" value="<?=$subtype['Name']?>" type="checkbox"><?=$subtype['Name']?></label></div> <?php
                                                        }
                                                    }
                                                }
                                            } ?>
                                        </div>
                                        <div id="type_e" style="display:none;"> <?php
                                            foreach($property_subtypes as $subtype) {
                                                if($subtype['Name'] != 'Select One') {
                                                    foreach($subtype["AppliesTo"] as $st) { 
                                                        if($st=="E") { ?>
                                                            <div class="checkbox"><label><input name="e_type[]" value="<?=$subtype['Name']?>" type="checkbox"><?=$subtype['Name']?></label></div> <?php
                                                        }
                                                    }
                                                }
                                            } ?>
                                        </div>
                                        <div id="type_f" style="display:none;"> <?php
                                            foreach($property_subtypes as $subtype) {
                                                if($subtype['Name'] != 'Select One') {
                                                    foreach($subtype["AppliesTo"] as $st) { 
                                                        if($st=="F") { ?>
                                                            <div class="checkbox"><label><input name="f_type[]" value="<?=$subtype['Name']?>" type="checkbox"><?=$subtype['Name']?></label></div> <?php
                                                        }
                                                    }
                                                }
                                            } ?>
                                        </div>
                                <?php
                                    }
                                ?>

                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <h3>Show Me</h3>
                            <hr>
                            <div class="form-group">
                                <div class="checkbox"><label><input name="listing_change_type[]" value="New Listing" type="checkbox"> New Listings</label></div>
                                <div class="checkbox"><label><input name="listing_change_type[]" value="New Construction" type="checkbox"> New Construction</label></div>
                                <div class="checkbox"><label><input name="listing_change_type[]" value="Open House" type="checkbox"> Open Houses</label></div>
                                <div class="checkbox"><label><input name="listing_change_type[]" value="Price Reduced" type="checkbox"> Price Reduced</label></div>
                            </div>
                            
                            <h3>Recently Sold</h3>
                            <hr>
                            <div class="form-group">
                                <?php 
                                    if($property_types) {
                                        foreach($property_types as $type=>$val) { ?>
                                            <div class="checkbox"><label><input name="recently_sold[]" value="<?=$val["PropertyClass"];?>" type="checkbox"><?=$val["MlsName"];?></label></div>
                                <?php   } 
                                    }
                                ?>
                            </div>
                            
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <h3>Features</h3>
                            <hr>
                            <div class="form-group">
                                <select name="house_size" class="form-control">
                                    <option value="0">Any House Size</option>
                                    <?php $counter = 600; for($i=0; $i<20; $i++) { ?>
                                        <option value="<?php echo $counter; ?>"><?php echo $counter."+ sq ft"; ?></option>
                                    <?php if($counter < 2000) $counter += 200; elseif($counter < 3000) $counter += 250; elseif($counter < 4000) $counter += 500; else $counter += 1000; } ?>
                                </select> 
                            </div>
                            <div class="form-group">
                                <select name="lot_size"  class="form-control">
                                    <option value="0">Any Lot Size</option>
                                    <option value="21780">1/2+ acre</option>
                                    <option value="43560">1+ acre</option>
                                    <option value="87120">2+ acres</option>
                                    <option value="217800">5+ acres</option>
                                    <option value="435600">10+ acre</option>
                                    <option value="871200">20+ acre</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <select name="house_age" class="form-control">
                                    <option value="0">Any House Age</option>
                                    <option value="fiveyears">0-5 Years</option>
                                    <option value="tenyears">0-10 Years</option>
                                    <option value="fifthteenyears">0-15 Years</option>
                                    <option value="twentyyears">0-20 Years</option>
                                    <option value="fiftyyears">0-50 Years</option>
                                    <option value="fiftyyearsplus">51+ Years</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <div class="checkbox"><label><input type="checkbox" name="features[]" value="garage"> 2+ Car Garage</label></div>
                                <div class="checkbox"><label><input type="checkbox" name="features[]" value="pool"> Swimming Pool</label></div>
                                <div class="checkbox"><label><input type="checkbox" name="features[]" value="view"> View</label></div>
                                <div class="checkbox"><label><input type="checkbox" name="features[]" value="waterfront"> Waterfront</label></div>
                                <div class="checkbox"><label><input type="checkbox" name="features[]" value="Cul-De-Sac"> Cul-De-Sac</label></div>
                                <div class="checkbox"><label><input type="checkbox" name="features[]" value="golf"> Golf Course View</label></div>
                                <div><a href="javacript:;" data-toggle="modal" data-target="#features_modal" class="viewallfeatures">View all features</a></div>
                                <style>
                                    .modal-backdrop.in {
                                        z-index: 0;
                                    }
                                    .modal-open .modal {
                                        overflow-x: hidden;
                                        overflow-y: auto;
                                    }
                                    .show-me-text {
                                        color: inherit !important;
                                    }
                                </style>
                                <div class="modal fade" id="features_modal" style="margin-top:198px; color:#000000;">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header text-center">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title">Features</h4>
                                            </div>
                                            <div class="modal-body" style="color:#000000;">
                                                <div class="row">
                                                    <div class="col-md-3 col-sm-3">
                                                        <h4>Inside</h4>
                                                        <div class="form-group">
                                                            <div class="checkbox"><label class="show-me-text"><input type="checkbox" name="features[]" value="floors"> Hardwood Floors</label></div>
                                                            <div class="checkbox"><label class="show-me-text"><input type="checkbox" name="features[]" value="fireplace"> Fireplace</label></div>
                                                            <div class="checkbox"><label class="show-me-text"><input type="checkbox" name="features[]" value="energy"> Energy Efficient</label></div>
                                                            <div class="checkbox"><label class="show-me-text"><input type="checkbox" name="features[]" value="features"> Disability Features</label></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-3">
                                                        <h4>Heating/Cooling</h4>
                                                        <div class="form-group">
                                                            <div class="checkbox"><label class="show-me-text"><input type="checkbox" name="features[]" value="air"> Central Air</label></div>
                                                            <div class="checkbox"><label class="show-me-text"><input type="checkbox" name="features[]" value="heat"> Central Heat</label></div>
                                                            <div class="checkbox"><label class="show-me-text"><input type="checkbox" name="features[]" value="air"> Forced Air</label></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-3">
                                                        <h4>Community</h4>
                                                        <div class="form-group">
                                                            <div class="checkbox"><label class="show-me-text"><input type="checkbox" name="features[]" value="recreation"> Recreation Facilities</label></div>
                                                            <div class="checkbox"><label class="show-me-text"><input type="checkbox" name="features[]" value="security"> Comm Security Features</label></div>
                                                            <div class="checkbox"><label class="show-me-text"><input type="checkbox" name="features[]" value="pool"> Comm Swimming Pool(s)</label></div>
                                                            <div class="checkbox"><label class="show-me-text"><input type="checkbox" name="features[]" value="boat" > Comm Boat Facilities</label></div>
                                                            <div class="checkbox"><label class="show-me-text"><input type="checkbox" name="features[]" value="clubhouse"> Comm Clubhouse(s)</label></div>
                                                            <div class="checkbox"><label class="show-me-text"><input type="checkbox" name="features[]" value="horse" > Comm Horse Facilities</label></div>
                                                            <div class="checkbox"><label class="show-me-text"><input type="checkbox" name="features[]" value="tennis"> Comm Tennis Court(s)</label></div>
                                                            <div class="checkbox"><label class="show-me-text"><input type="checkbox" name="features[]" value="park"> Comm Park(s)</label></div>
                                                            <div class="checkbox"><label class="show-me-text"><input type="checkbox" name="features[]" value="golf"> Comm Golf</label></div>
                                                            <div class="checkbox"><label class="show-me-text"><input type="checkbox" name="features[]" value="senior"> Senior Comm</label></div>
                                                            <div class="checkbox"><label class="show-me-text"><input type="checkbox" name="features[]" value="spa/hot tub"> Comm Spa/Hot Tub(s)</label></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-3">
                                                        <h4>Outdoor Amenities</h4>
                                                        <div class="form-group">
                                                            <div class="checkbox"><label class="show-me-text"><input type="checkbox" name="features[]" value="pool"> Swimming Pool</label></div>
                                                           <!--  <div class="checkbox"><label class="show-me-text"><input type="checkbox" name="features[]" value="boat"> RV/Boat Parking</label></div> -->
                                                            <div class="checkbox"><label class="show-me-text"><input type="checkbox" name="features[]" value="spa/hot tub"> Spa/Hot Tub</label></div>
                                                            <div class="checkbox"><label class="show-me-text"><input type="checkbox" name="features[]" value="horse"> Horse Facilities</label></div>
                                                            <div class="checkbox"><label class="show-me-text"><input type="checkbox" name="features[]" value="tennis"> Tennis Courts</label></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-3 col-sm-3">
                                                        <h4>Rooms</h4>
                                                        <div class="form-group">
                                                            <div class="checkbox"><label class="show-me-text"><input type="checkbox" name="features[]" value="dining"> Dining</label></div>
                                                            <div class="checkbox"><label class="show-me-text"><input type="checkbox" name="features[]" value="laundry"> Laundry</label></div>
                                                            <div class="checkbox"><label class="show-me-text"><input type="checkbox" name="features[]" value="family"> Family</label></div>
                                                            <div class="checkbox"><label class="show-me-text"><input type="checkbox" name="features[]" value="basement"> Basement</label></div>
                                                            <div class="checkbox"><label class="show-me-text"><input type="checkbox" name="features[]" value="office"> Den/Office</label></div>
                                                            <div class="checkbox"><label class="show-me-text"><input type="checkbox" name="features[]" value="games"> Games</label></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-3">
                                                        <h4>Lot</h4>
                                                        <div class="form-group">
                                                            <div class="checkbox"><label class="show-me-text"><input type="checkbox" name="features[]" value="lot"> Corner Lot</label></div>
                                                            <div class="checkbox"><label class="show-me-text"><input type="checkbox" name="features[]" value="cul-de-sac"> Cul-De-Sac</label></div>
                                                            <div class="checkbox"><label class="show-me-text"><input type="checkbox" name="features[]" value="waterfront"> WaterFront</label></div>
                                                            <div class="checkbox"><label class="show-me-text"><input type="checkbox" name="features[]" value="view"> Water View</label></div>
                                                            <div class="checkbox"><label class="show-me-text"><input type="checkbox" name="features[]" value="golf"> Golf Course Lot/Frontage</label></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-3">
                                                        <h4>Parking</h4>
                                                        <div class="form-group">
                                                            <div class="checkbox"><label class="show-me-text"><input type="checkbox" name="features[]" value='garage'> Garage 1+</label></div>
                                                            <div class="checkbox"><label class="show-me-text"><input type="checkbox" name="features[]" value='garage'> Garage 2+</label></div>
                                                           <!--  <div class="checkbox"><label class="show-me-text"><input type="checkbox" name="features[]" value='spa'> Spa/Hot Tub</label></div> -->
                                                            <div class="checkbox"><label class="show-me-text"><input type="checkbox" name="features[]" value="garage"> Garage 3+</label></div>
                                                            <div class="checkbox"><label class="show-me-text"><input type="checkbox" name="features[]" value="carport"> Carport</label></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-3">
                                                        <h4>Stories</h4>
                                                        <div class="form-group">
                                                            <div class="checkbox"><label class="show-me-text"><input type="checkbox" name="features[]" value='single'> Single Story</label></div>
                                                            <div class="checkbox"><label class="show-me-text"><input type="checkbox" name="features[]" value='multi'> Multi</label></div>
                                                        </div>
                                                    </div> 
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-success submit-button" data-dismiss="modal">Apply</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <input type="submit" class="btn btn-default submit-button btn-lg">
                            <p>&nbsp;</p>
                        </div>
                    </div>
                    </div>
                </div>
            </form>
            <div class="visible-xs">
                <a href="#" class="view-map-button submit-button"><i class="fa fa-map"></i> <span class="search-label">Show map</span> </a>
                <button class="btn btn-default submit-button filter-search-button">
                     <i class="fa fa-filter"></i> More Options
                 </button>
                <!-- <?php if( isset($_GET) AND !empty($_GET)) : ?>
                    <a href="<?php echo site_url("home/home/save_search_properties?".http_build_query($_GET)); ?>" type="button" id="moreFiltersToggle" class="follow-search submit-button"><i class="fa fa-undo"></i> <span class="search-label">Save Search</span>  </a>
                <?php endif;?> -->

                <?php  if($this->uri->segment(1) == 'listings'){?>
                    <li>
                     <?php if(!isset($_SESSION['customer_id'])) { ?>
                            <a href="javascript:void(0)" class="follow-search" id="follow_search modalogin" data-toggle='modal' data-target='.customer_login_signup' data-redirect-customer="listings?Search=<?=$this->input->get('Search').'&success';?>" title="Save Search"><i class="fa fa-undo"></i> <span class="search-label">Save Search</span>
                            </a>
                        <?php } else { ?>    
                           <a href="<?php echo site_url("home/home/save_search_properties?".http_build_query($_GET)); ?>" type="button" id="moreFiltersToggle" class="<?=($saved_searches) ? "follow-search-red" : "follow-search"?>"><i class="fa fa-undo"></i> <span class="search-label">Save Search</span> </a>
                            </a>
                        <?php } ?>
                    </li>
               <?php }?>
            </div>
    </section>