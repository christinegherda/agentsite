
    <div class="page-content">
        <section class="property-detail-content">
            <div class="container">
                <div class="row">
                    <?php $this->load->view($theme."/session/sidebar");?>
                    <div class="col-md-9 col-sm-9 content-panel">
                        <span id="ret-message" class="col-md-12 col-sm-12 col-xs-12" style="margin-top: 10px;"></span>

                        <!-- Customize testimonial page for mariamuriello.com user_id:1037 -->
                        <?php if($this->session->userdata('user_id') == "1037"){?>

                              <div class="realsatisfied_widget" param_vanity_key="Maria-Muriello-CRS-GRI-SRES" param_type="" param_config="Red & Square:50:Testimonials"></div>

                              <script src="https://secure.realsatisfied.com/js/app/widget.js" type="text/javascript"></script>

                       <?php }?>
                        
                        <h2>Testimonials</h2>
                        <div class="rating-summary">
                            <div class="average-rating" data-average-reviews="<?=$average_reviews['average'];?>"></div>
                            <p>Based on <?=$average_reviews['total']?> reviews.</p>
                            <button class="btn btn-default write-review-button">Write a review</button>
                            <p class="clearfix"></p>
                            <br>
                            <div class="review-area">
                                <form action="<?php echo base_url();?>home/add_testimonial" id="review_form">
                                    <p>Rate my service</p>
                                    <div class="new"></div>
                                    <br>
                                    <div class="form-group">
                                      <input type="hidden" name="customer_id" value="<?=$this->session->userdata('contact_id');?>">
                                      <input type="hidden" name="redirect-customer" value="testimonials?success">
                                      <input type="hidden" name="rating" id="rating">
                                      <input type="text" name="subject" class="form-control" placeholder="Enter your subject here." required>
                                    </div>
                                    <div class="form-group">
                                      <textarea name="message" id="testimonial-message" cols="10" rows="10" class="form-control" placeholder="Enter your testimonial here." required></textarea>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-default" id="submit_review">Submit</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <hr>
                        <div class="col-md-8 col-xs-7 no-padding-left">
                            <h4 class="text-left">Customer Reviews</h4>
                        </div>
                        <div class="col-md-4 col-xs-5 no-padding">
                        <?php 
                            if($reviews) { ?>
                                <form action="#" id="sort_reviews_form" method="GET">
                                    <select name="reviews_order" id="reviews_order" class="form-control">
                                        <option>Sort By</option>
                                        <option value="highest">Highest</option>
                                        <option value="lowest">Lowest</option>
                                        <option value="newest">Newest</option>
                                        <option value="oldest">Oldest</option>
                                    </select>
                                </form>
                        <?php
                            }
                        ?>
                        </div>
                        <p class="clearfix"></p>
                        <div class="customer-reviews">
                            <?php 
                                if($reviews) {
                                    foreach($reviews as $review) { ?>
                                        <ul>
                                            <li>
                                                <div class="ratefixed" data-rate="<?=$review->rating?>"></div>
                                                <p><strong><?=$review->subject;?></strong></p>
                                                <p>by: <?=($review->customer_id == 0 ? $review->name : $review->first_name." ".$review->last_name);?></p>
                                                <p><?=$review->message;?></p>
                                            </li>
                                        </ul>
                            <?php
                                    }
                                }
                            ?>
                        </div>
                        <ul class="pagination pagination-list">
                            <?php echo $pagination;?>                        
                        </ul>
                    </div>
                </div>
            </div>
        </section>
    </div>


    