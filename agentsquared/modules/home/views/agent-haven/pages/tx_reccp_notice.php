<div class="page-content">
  <div class="container">
    <div class="tx-wrapper text-center ">
      <div class="tx-notice">
        <p>THE TEXAS REAL ESTATE COMMISSION (TREC) REGULATES <br>
        REAL ESTATE BROKERS AND SALES AGENTS, REAL ESTATE INSPECTORS,<br>
        HOME WARRANTY COMPANIES, EASEMENT AND RIGHT-OF-WAY AGENTS<br>
        AND TIMESHARE INTEREST PROVIDERS </p>
        <p>
          YOU CAN FIND MORE INFORMATION AND <br>
          CHECK THE STATUS OF A LICENSE HOLDER AT
        </p>
        <h2><a href="http://www.trec.texas.gov/">WWW.TREC.TEXAS.GOV</a></h2>
        <hr>
        <p>
          YOU CAN SEND A COMPLAINT AGAINST A LICENSE HOLDER TO TREC <br>
          A COMPLAINT FORM IS AVAILABLE ON THE TREC WEBSITE<br>
        </p>
        <hr>
        <p>
          TREC ADMINISTERS TWO RECOVERY FUNDS WHICH MAY BE USED TO  <br>
          SATISFY A CIVIL COURT JUDGMENT AGAINST A BROKER, SALES AGENT, <br>
          REAL ESTATE INSPECTOR, OR EASEMENT OR RIGHT-OF-WAY AGENT, <br>
          IF CERTAIN REQUIREMENTS ARE MET <br>
        </p>
        <hr>
        <p>
          
          IF YOU HAVE QUESTIONS OR ISSUES ABOUT THE ACTIVITIES OF <br>
          A LICENSE HOLDER, THE COMPLAINT PROCESS OR THE <br>
          RECOVERY FUNDS, PLEASE VISIT THE WEBSITE OR CONTACT TREC AT <br>
        </p>

        <div class="row">
          <div class="col-md-3 col-md-offset-2">
            <img src="/assets/images/tx-trec-logo.png" alt="">
          </div>
          <div class="col-md-6">
            <h3>TEXAS REAL ESTATE COMMISSION</h3>
            <h3>P.O. BOX 12188</h3>
            <h3>AUSTIN, TEXAS 78711-2188</h3>
            <h3>(512) 936-3000</h3>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>