	<?php
        $this->load->view($theme.'/pages/page_view/advance-search-form');  
    ?> 
        <section class="property-map">

            <?php if(isset($account_info->Mls) && $account_info->Mls != "MLS BCS"  && $account_info->Mls != "Vallarta-Nayarit MLS") { ?>

                <div id="map_canvas" style="border: 2px solid #3872ac;"></div>
                <style type="text/css">
                    html,
                    body,
                    #map_canvas  {
                        height: 600px;
                        width: 100%;
                        margin-top: 0px;
                        padding: 0px;
                    }
                </style>
            <?php }else{ ?>

                <div id="map_canvas_bcs" style="border: 2px solid #3872ac;"></div>
                <style type="text/css">
                    html,
                    body,
                    #map_canvas_bcs  {                  
                        height: 600px;
                        width: 100%;
                        margin-top: 0px;
                        padding: 0px;
                    }
                </style>
            <?php }?>
        <script type="text/javascript">
            
                    function initialize() {
                                map = new google.maps.Map(
                                    document.getElementById("map_canvas"), {

                                      center: new google.maps.LatLng(37.387474,-122.05754339999999),
                                       zoom: 15,
                                       scrollwheel: false,
                                       mapTypeId: google.maps.MapTypeId.ROADMAP
                                     });

                                geocoder = new google.maps.Geocoder();

                            <?php 
                                if(isset($featured) && $featured) {
                                    foreach($featured as $feature) {
                            ?>
                                    var l1 = "<?php echo $feature['StandardFields']['UnparsedFirstLineAddress']?>";
                                    var l2 = "<?php echo $feature['StandardFields']['UnparsedAddress']?>";
                                    var l3 = base_url + "other-property-details/<?php echo $feature['StandardFields']['ListingKey']?>";
                                    var l4 = "<?= (isset($feature['Photos']['Uri300'])) ? $feature['Photos']['Uri300'] : ""; ?>";

                                    var locations = [
                                        [l1, l2, l3, l4]
                                    ];

                                    geocodeAddress(locations);

                             <?php } } ?>

                     }

                            var geocoder;
                            var map;
                            var bounds = new google.maps.LatLngBounds();

                            function geocodeAddress(locations, i) {
                                var title = locations[0][0];
                                var address = locations[0][1];
                                var url = locations[0][2];
                                var img = locations[0][3];

                                geocoder.geocode({
                                  'address': locations[0][1]
                                },

                                function(results, status) {
                                    if (status == google.maps.GeocoderStatus.OK) {
                                        var marker = new google.maps.Marker({
                                              icon: '<?=$protocol;?>maps.google.com/mapfiles/ms/icons/red.png',
                                              map: map,
                                              position: results[0].geometry.location,
                                              title: title,
                                              animation: google.maps.Animation.DROP,
                                              address: address,
                                              url: url
                                        })
                                        //infoWindow(marker, map, title, address, url, img);

                                        google.maps.event.addListener(marker, 'mouseover', function() {
                                            var html = "<div style='position:relative'> <br><img src="+img+" alt="+address+" width='205'  height='100'><br>" + address + "<br><a href='" + url + "' target='_blank' >View Property</a></p></div>";
                                            iw = new google.maps.InfoWindow({
                                              content: html,
                                              maxWidth: 205,
                                              //position: "left"
                                            });
                                            iw.open(map, marker);

                                        });

                                        google.maps.event.addListener(marker, 'click', function() {
                                            var html = "<div style='position:relative'> <br><img src="+img+" alt="+address+" width='205'  height='100'><br>" + address + "<br><a href='" + url + "' target='_blank' >View Property</a></p></div>";
                                            iw = new google.maps.InfoWindow({
                                              content: html,
                                              maxWidth: 205,
                                              //position: "left"
                                            });
                                            iw.open(map, marker);

                                        });
              

                                         google.maps.event.addListener(marker, 'mouseout', function() {
                                            iw.close();
                                        });

                                        bounds.extend(marker.getPosition());
                                        map.fitBounds(bounds);

                                    } else if (status == google.maps.GeocoderStatus.OVER_QUERY_LIMIT) { 

                                        wait = true;

                                        setTimeout(function() {
                                            geocodeAddress(locations, i);
                                        }, 1000 );

                                       // setTimeout("wait = true", 10000);
                                        
                                    } else {
                                       // console.log( "Geocode was not successful for the following reason: " + status );

                                        //alert("Geocode was not successful for the following reason: " + status);
                                    }
                                 
                                });
                            }

                        function initialize_bcs() {
                                
                            map = new google.maps.Map(
                                document.getElementById("map_canvas_bcs"), {

                                   center: new google.maps.LatLng(37.387474,-122.05754339999999),
                                   zoom: 4,
                                   scrollwheel: false,
                                   mapTypeId: google.maps.MapTypeId.ROADMAP
                                });

                                geocoder = new google.maps.Geocoder();
                            
                            <?php 
                                if(isset($featured) && $featured) {
                                    $count = 1;
                                    foreach($featured as $feature) {
                                        if( $feature['StandardFields']['MlsId'] == "20100129000321508794000000" || $feature['StandardFields']['MlsId'] == "20100129000321508794000000")
                                        {
                                            continue;
                                        }
                                        $latitude = ($feature['StandardFields']['Latitude']) ? $feature['StandardFields']['Latitude']  : "";
                                        $longitude = ($feature["StandardFields"]["Longitude"]) ? $feature["StandardFields"]["Longitude"] :"";

                                        if($latitude != "********" && $longitude != "********"){

                                            $lat = ($feature["StandardFields"]["Latitude"]) ? $feature["StandardFields"]["Latitude"]  : "";
                                            $lon = ($feature["StandardFields"]["Longitude"]) ? $feature["StandardFields"]["Longitude"] :"";

                                        } else {

                                            $latlon = Modules::run('home/getlatlon', $feature["StandardFields"]["UnparsedAddress"]);

                                            if($latlon){
                                                $lat = ($latlon["lat"]) ? $latlon["lat"] : "";
                                                $lon = ($latlon["lon"]) ? $latlon["lon"] : "";    
                                            }
                                            continue;

                                        }
                           ?>
                                    var l1 = "<?php echo $feature['StandardFields']['UnparsedFirstLineAddress']?>";
                                    var l2 = "<?php echo $feature['StandardFields']['UnparsedAddress']?>";
                                    var l3 = base_url + "other-property-details/<?php echo $feature['StandardFields']['ListingKey']?>";
                                    var l4 = "<?= (isset($feature['Photos']['Uri300'])) ? $feature['Photos']['Uri300'] : ""; ?>";
                                    var l5 = <?= $lat; ?>;
                                    var l6 = <?= $lon; ?>;

                                    var locations = [
                                        [l1, l2, l3, l4, l5, l6]
                                    ];

                                    var c = "<?php echo $count; ?>";
                                    geocodeAddressBcs(locations);
                                    //geocodeAddressBcs2(locations, c);

                             <?php } $count++; } ?>

                        }

                            var geocoder;
                            //var geocoder = new google.maps.Geocoder();
                            var map;
                            var bounds_bcs = new google.maps.LatLngBounds();

                            function geocodeAddressBcs2(locations, loc_map) {
                                var title = locations[0][0];
                                var address = locations[0][1];
                                var url = locations[0][2];
                                var img = locations[0][3];
                                var lat_1 = locations[0][4];
                                var lon_1 = locations[0][5];

                                //console.log( results );
                                        var map_location = {lat: lat_1, lng: lon_1};
                                        console.log(map_location);
                                        loc_map = new google.maps.Map(document.getElementById('map_canvas_bcs'), {
                                              center: map_location,
                                              zoom: 12,
                                              scrollwheel: false,
                                              mapTypeId: google.maps.MapTypeId.ROADMAP
                                        });

                                        var marker =  new google.maps.Marker({
                                            icon: '<?=$protocol;?>maps.google.com/mapfiles/ms/icons/red.png',
                                            position: map_location,
                                            map: loc_map,
                                            title: title,
                                            animation: google.maps.Animation.DROP,
                                            address: address,
                                            url: url
                                        });

                                        google.maps.event.addListener(marker, 'mouseover', function() {
                                            var html = "<div style='position:relative'> <br><img src="+img+" alt="+address+" width='205'  height='100'><br>" + address + "<br><a href='" + url + "' target='_blank' >View Property</a></p></div>";
                                            iw = new google.maps.InfoWindow({
                                              content: html,
                                              maxWidth: 205,
                                              position: "left"
                                            });
                                            iw.open(map, marker);

                                        });

                                        google.maps.event.addListener(marker, 'click', function() {
                                            var html = "<div style='position:relative'> <br><img src="+img+" alt="+address+" width='205'  height='100'><br>" + address + "<br><a href='" + url + "' target='_blank' >View Property</a></p></div>";
                                            iw = new google.maps.InfoWindow({
                                              content: html,
                                              maxWidth: 205,
                                              position: "left"
                                            });
                                            iw.open(map, marker);

                                        });
              

                                        google.maps.event.addListener(marker, 'mouseout', function() {
                                            iw.close();
                                        });

                                        bounds_bcs.extend(marker.getPosition());
                                        map.fitBounds(bounds_bcs);

                            }

                            function geocodeAddressBcs(locations, i) {

                                var title = locations[0][0];
                                var address = locations[0][1];
                                var url = locations[0][2];
                                var img = locations[0][3];
                                var lat_1 = locations[0][4];
                                var lon_1 = locations[0][5];
                                console.log(lat_1);
                                console.log(lon_1);

                                geocoder.geocode({
                                  //'address': locations[0][1],
                                  "location": {
                                    "lat": lat_1,
                                    "lng": lon_1,
                                  },
                                  "country": "US",
                                  //"components" : "administrative_area:CA"
                                },function(results, status) {
                                    if (status == google.maps.GeocoderStatus.OK) { 
                                        console.log( results );
                                        var map_location = {lat: lat_1, lng: lon_1};
                                        console.log(map_location);
                                        // loc_map = new google.maps.Map(document.getElementById('map_canvas_bcs'), {
                                        //       center: map_location,
                                        //       zoom: 12,
                                        //       scrollwheel: false,
                                        //       mapTypeId: google.maps.MapTypeId.ROADMAP
                                        // });

                                        // var marker =  new google.maps.Marker({
                                        //     icon: '<?=$protocol;?>maps.google.com/mapfiles/ms/icons/red.png',
                                        //     position: map_location,
                                        //     map: loc_map,
                                        //     title: title,
                                        //     animation: google.maps.Animation.DROP,
                                        //     address: address,
                                        //     url: url
                                        // });
                                        var marker = new google.maps.Marker({
                                              icon: '<?=$protocol;?>maps.google.com/mapfiles/ms/icons/red.png',
                                              map: map,
                                              position: results[0].geometry.location,
                                              title: title,
                                              animation: google.maps.Animation.DROP,
                                              address: address,
                                              url: url
                                        })

                                        //infoWindow(marker, map, title, address, url, img);
                                        google.maps.event.addListener(marker, 'mouseover', function() {
                                            var html = "<div style='position:relative'> <br><img src="+img+" alt="+address+" width='205'  height='100'><br>" + address + "<br><a href='" + url + "' target='_blank' >View Property</a></p></div>";
                                            iw = new google.maps.InfoWindow({
                                              content: html,
                                              maxWidth: 205,
                                              position: "left"
                                            });
                                            iw.open(map, marker);

                                        });

                                        google.maps.event.addListener(marker, 'click', function() {
                                            var html = "<div style='position:relative'> <br><img src="+img+" alt="+address+" width='205'  height='100'><br>" + address + "<br><a href='" + url + "' target='_blank' >View Property</a></p></div>";
                                            iw = new google.maps.InfoWindow({
                                              content: html,
                                              maxWidth: 205,
                                              position: "left"
                                            });
                                            iw.open(map, marker);

                                        });
              

                                        google.maps.event.addListener(marker, 'mouseout', function() {
                                            iw.close();
                                        });
                                        bounds_bcs.extend(marker.getPosition());
                                        map.fitBounds(bounds_bcs);

                                    }else if (status == google.maps.GeocoderStatus.OVER_QUERY_LIMIT) { 

                                        wait = true;

                                        setTimeout(function() {
                                            geocodeAddress(locations, i);
                                        }, 1000 );

                                       // setTimeout("wait = true", 10000);
                                        
                                    } else {
                                        console.log( "Geocode was not successful for the following reason: " + status );
                                        //alert("Geocode was not successful for the following reason: " + status);
                                    }
                                });


                                //infowindow = new google.maps.InfoWindow();
                                //var service = new google.maps.places.PlacesService(loc_map);

                            }

                    <?php if(isset($account_info->Mls) && $account_info->Mls != "MLS BCS" && $account_info->Mls != "Vallarta-Nayarit MLS") { ?>            
                        google.maps.event.addDomListener(window, "load", initialize);
                    <?php }else{ ?>
                        google.maps.event.addDomListener(window, "load", initialize_bcs);

                    <?php }?>

                    function infoWindow(marker, map, title, address, url,img) {
                          google.maps.event.addListener(marker, 'mouseover', function() {
                            var html = "<div><h3>" + title + "</h3><p>" + address + "<br><img src="+img+" alt="+address+" width='200'  height='150'><br><a href='" + url + "' target='_blank' >View Property</a></p></div>";
                            iw = new google.maps.InfoWindow({
                              content: html,
                              maxWidth: 350
                            });
                            iw.open(map, marker);

                          });

                          google.maps.event.addListener(marker, 'mouseout', function() {
                                
                                new google.maps.InfoWindow.close(map, marker);
                            });

                    }

                    
            </script>        
           
        </section>
	<section class="property-main-content">
        <div class="container">
            <div class="row">
                
                <div class="col-md-3 col-sm-3">
                     <?php 
                            if($account_info->Mls != "MLS BCS"){
                                $this->load->view($theme.'/pages/findahome_view/mortgage-calc');
                            }else{
                                $this->load->view($theme.'/pages/findahome_view/customer-question');
                            }
                            
                        ?>
                </div>
                
                <div class="col-md-9 col-sm-9">
                  <?php if(isset($h1)){?>
                  
                  <h1><?php echo $h1;?></h1>
                  <?php }?>
                  
                	<div class="search-listings">
                        <?php
                        if(!isset($featured) || count($featured) == 0 || empty($featured)) { ?>
                            <div class="nosold-property">
                              <i class="fa fa-exclamation-triangle"></i>
                              Results not found. Please search again.
                            </div>
                        <?php
                        } else { ?>
                            <div class="col-md-12 col-sm-12 mobilepadding-0">
                                <p class="search-for">
                                    <strong>You searched for:</strong>

                                    <?php 
                                        if(isset($_GET['Search'])) {

                                            $search = ($_GET['Search']) ? $_GET['Search'].", " : "" ;
                                            
                                            if(!empty($_GET['bedroom'])) {
                                                $bed = ($_GET['bedroom'] != 1 ) ? "Beds" : "Bed";
                                            } else {
                                                $bed = "Bed";
                                            }

                                             if(!empty($_GET['bathroom'])) {
                                                $bath = ($_GET['bathroom'] != 1) ? "Baths" : "Bath";
                                            } else {
                                                $bath = "Bath";
                                            }

                                            $bedroom = !empty($_GET['bedroom']) ? $_GET['bedroom'] : 0;
                                            $bathroom = !empty($_GET['bathroom']) ? $_GET['bathroom'] : 0;

                                            $min_price = !empty($_GET['min_price']) ? $_GET['min_price'] : 0;
                                            $max_price = !empty($_GET['max_price']) ? $_GET['max_price'] : 0;

                                            $property_type = !empty($_GET['property_type']) ? ' , '.'Property Type('.$_GET['property_type'][0].')' : "" ;
                                            $property_subtype = !empty($_GET['a_type']) ? ' , '.'Property Sub Type('.$_GET['a_type'][0].')' : "" ;

                                            $house_size = !empty($_GET['house_size']) ? ' , '.'House size('.number_format($_GET['house_size']).' sqft)' : "" ;
                                            $lot_size = !empty($_GET['lot_size']) ? ' , '.'Lot size('.number_format($_GET['lot_size']).' sqft)' : "";

                                            if(!empty($_GET['listing_change_type'])){
                                                foreach($_GET['listing_change_type'] as $key=>$val){
                                                    $change_type[] = $val;
                                                }
                                            }

                                            if(!empty($change_type)){
                                                $listing_change = implode (", ", $change_type);
                                            } else {
                                               $listing_change = ""; 
                                            }
                                            
                                            $listing_change_type = !empty($listing_change) ? ' , '.'Listing Change Type('.$listing_change.')' : "";

                                            if(!empty($_GET['recently_sold'])){
                                                foreach($_GET['recently_sold'] as $key=>$val){
                                                    $recent_sold[] = $val;
                                                }
                                            }

                                            if(!empty($recent_sold)){
                                                $recentsold = implode (", ", $recent_sold);
                                            } else {
                                               $recentsold = ""; 
                                            }
                                            
                                            $recently_sold = !empty($recentsold) ? ' , '.'Recently Sold('.$recentsold.')' : "";

                                            if( isset($_GET['house_age']) && !empty($_GET['house_age'])){

                                                if($_GET['house_age'] == "fiveyears"){
                                                    $houseage = "5 years";
                                                } elseif($_GET['house_age'] == "tenyears"){
                                                     $houseage = "10 years";
                                                }
                                                elseif($_GET['house_age'] == "fifthteenyears"){
                                                     $houseage = "15 years";
                                                }
                                                elseif($_GET['house_age'] == "twentyyears"){
                                                     $houseage = "20 years";
                                                }
                                                elseif($_GET['house_age'] == "fiftyyears"){
                                                     $houseage = "50 years";
                                                }elseif($_GET['house_age'] == "fiftyyearsplus"){
                                                     $houseage = "50+ years";
                                                } else{
                                                    $houseage = "";
                                                }
    
                                            } else{
                                               $houseage = ""; 
                                            }

                                            $house_age = (isset($_GET['house_age']) && $_GET['house_age'] != "Any House Age") ? ' , '.'House age('.$houseage.')' : "" ;

                                            if(!empty($_GET['features'])){
                                                foreach($_GET['features'] as $key=>$val){
                                                    $search_features[] = $val;
                                                }
                                            }

                                            if(!empty($search_features)){
                                                $all_features = implode (", ", $search_features);
                                            } else {
                                               $all_features = ""; 
                                            }
                                            
                                            $features = !empty($all_features) ? ' , '.'Features('.$all_features.').' : "";
                                            
                                            echo ucfirst($search) ."Price (".number_format($min_price)."-".number_format($max_price)."), ".$bedroom ." ".$bed.", ".$bathroom ." ".$bath." ".$property_type.$property_subtype.$listing_change_type.$recently_sold.$house_size.$lot_size.$house_age.$features;
                                        } 
                                    ?>
                                </p>
                                    <?php 
                                        $offset = (isset($_GET['offset'])) ? $_GET['offset'] : "" ; 
                                    ?>

                                    <h4><?php echo !empty($total_count) ? number_format($total_count) :"" ?> Properties Found. <!-- Showing  <?php echo !empty($offset) ? number_format($offset) : "" ?> <?php echo !empty($offset) ? "of" : "" ?> <?php echo !empty($total_count) ? number_format($total_count) : ""; ?> --></h4>
                                <br/>
                            </div>
                        <?php
                            $is_openhouse=false;
                            if(isset($_GET['listing_change_type'])) {
                                foreach($_GET['listing_change_type'] as $show_me) {
                                    $is_openhouse = ($show_me == "Open House") ? true : false;
                                }
                            }
                            if(!$is_openhouse) {
                                if(isset($_GET['Search'])) {
                                    $search_get = $_GET['Search'];
                                    $sg = strtolower($search_get);
                                    $is_openhouse = ($sg == 'open house') ? true : false;
                                }
                            }

                            foreach($featured as $feature) {
                                if(isset($feature['Photos']['Uri300'])) { ?>
                                    <div class="col-md-4 col-sm-6 mobilepadding-0">
                                        <div class="search-listing-item">
                                            <div class="search-property-image">
                                                <a href="<?=base_url('other-property-details/');?><?php echo str_replace(" ",  "-", $feature['StandardFields']['ListingKey']);?>" title="">
                                                    <img src="<?= $feature['Photos']['Uri300']?>" alt="<?php echo $feature['StandardFields']["UnparsedFirstLineAddress"]; ?>" class="img-responsive">
                                                </a>
                                            </div>
                                            <div style="top:15px;" class="property-listing-status">
                                                <?php
                                                    if(isset( $feature['StandardFields']["MlsStatus"])){?>
                                                        <?php echo $feature['StandardFields']["MlsStatus"];?>
                                                <?php  } ?>
                                            </div>
                                            <div class="search-listing-type">
                                                <?php
                                                    if(isset( $feature['StandardFields']["PropertyClass"])){?>
                                                        <?php echo $feature['StandardFields']["PropertyClass"];?>
                                                <?php  } ?>
                                            </div>
                                            <div class="search-listing-title">
                                                <div class="col-md-9 col-sm-9 col-xs-9">
                                                    <p class="search-property-title">
                                                        <a href="<?=base_url('other-property-details/');?><?php echo str_replace(" ",  "-", $feature['StandardFields']['ListingKey']);?>">
                                                            <?php echo $feature['StandardFields']["UnparsedFirstLineAddress"]; ?>
                                                        </a>
                                                    </p>
                                                </div>
                                                <div class="col-md-3 col-sm-3 col-xs-3 padding-0">
                                                    <p class="property-action">
                                                        <?php  
                                                            $property_saved = FALSE; 
                                                            if(isset($_SESSION["save_properties"])) :
                                                                foreach($_SESSION["save_properties"] as $key) :
                                                                    if($key['property_id'] == $feature['StandardFields']['ListingKey']) : 
                                                                        $property_saved = TRUE; 
                                                                        break; 
                                                                    endif;
                                                                endforeach;
                                                            else : 
                                                                $property_saved = FALSE; 
                                                            endif;
                                                        ?>

                                                        <?php if(!isset($_SESSION['customer_id'])) { ?>
                                                            <a href="javascript:void(0)" class="search_hearty modalogin" data-toggle='modal' data-target='.customer_login_signup' data-redirect-customer="listings?Search=<?=$this->input->get('Search').'&success';?>" title="Save Property" >
                                                                <span id="isSaveFavorate_<?=$feature['StandardFields']['ListingKey']?>"><?= ($property_saved) ? "<i class='fa fa-heart'></i>" : "<i class='fa fa-heart-o'></i>";?></span>
                                                            </a>
                                                        <?php } else { ?>    
                                                            <a href="javascript:void(0)" data-pro-id="<?=$feature['StandardFields']['ListingKey']?>" class="save-favorate" title="Save Property" >
                                                                <span id="isSaveFavorate_<?=$feature['StandardFields']['ListingKey']?>"><?= ($property_saved) ? "<i class='fa fa-heart'></i>" : "<i class='fa fa-heart-o'></i>";?></span>
                                                            </a>
                                                        <?php } ?>

                                                        <?php if (!isset($_SESSION['customer_id']) && $is_capture_leads): ?>
                                                            <a href="#" type="button" class="request_info_search modalogin" data-toggle="modal" data-target=".customer_login_signup" data-redirect-customer="listings?Search=<?=$this->input->get('Search').'&success';?>" title="Request Information">
                                                                <span class="fa fa-question-circle"></span>
                                                            </a>
                                                        <?php else: ?>
                                                            <a href="#" type="button" class="request_info" data-toggle="modal" data-target="#request-info-modal" data-listingId="<?php echo str_replace(" ",  "-", $feature['StandardFields']['ListingKey']); ?>" data-propertyType="other_property" data-address="<?=$feature['StandardFields']["UnparsedAddress"];?>" title="Request Information"><span class="fa fa-question-circle"></span>
                                                            </a>
                                                        <?php endif ?>
                                                    </p>
                                                    
                                                </div>
                                            </div>

                                            <p><i class="fa fa-map-marker"></i><?php echo str_replace($feature['StandardFields']["UnparsedFirstLineAddress"].',', "", $feature['StandardFields']["UnparsedAddress"]); ?></p>
                                            <p class="search-property-location"><i class="fa fa-usd"></i> <?=number_format($feature['StandardFields']['CurrentPrice'])?></p>
                                            <ul class="list-inline search-property-specs">
                                            <?php
                                              
                                                if($feature['StandardFields']['BedsTotal'] && is_numeric($feature['StandardFields']['BedsTotal'])) { ?>
                                                    <li><i class="fa fa-bed"></i> <?=$feature['StandardFields']['BedsTotal']?> Bed</li>
                                            <?php }
                                                if($feature['StandardFields']['BathsTotal'] && is_numeric($feature['StandardFields']['BathsTotal'])) { ?>
                                                    <li><i class="icon-toilet"></i> <?=$feature['StandardFields']['BathsTotal']?> Bath</li>
                                            <?php }?>

                                           <?php
                                            if(!empty($feature['StandardFields']['BuildingAreaTotal']) && ($feature['StandardFields']['BuildingAreaTotal'] != "0")   && is_numeric($feature['StandardFields']['BuildingAreaTotal'])) {?>

                                                    <li><?=number_format($feature['StandardFields']['BuildingAreaTotal'])?> sqft</li>

                                            <?php } elseif(!empty($feature['StandardFields']['LotSizeArea']) && ($feature['StandardFields']['LotSizeArea'] != "0")   && is_numeric($feature['StandardFields']['LotSizeArea'])) {

                                                if(!empty($feature['StandardFields']['LotSizeUnits']) && ($feature['StandardFields']['LotSizeUnits']) === "Acres"){?>
                                                    <li>Lot size: <?=number_format($feature['StandardFields']['LotSizeArea'], 2, '.', ',' )?> acres</li>

                                                <?php } else {?>

                                                    <li>Lot size: <?=number_format($feature['StandardFields']['LotSizeArea'])?> acres</li>

                                                <?php }?>


                                            <?php } elseif(!empty($feature['StandardFields']['LotSizeSquareFeet']) && ($feature['StandardFields']['LotSizeSquareFeet'] != "0")   && is_numeric($feature['StandardFields']['LotSizeSquareFeet'])) {?>

                                                    <li>Lot size: <?=number_format($feature['StandardFields']['LotSizeSquareFeet'])?> sqft</li> 

                                             <?php } elseif(!empty($feature['StandardFields']['LotSizeAcres']) && ($feature['StandardFields']['LotSizeAcres'] != "0")   && is_numeric($feature['StandardFields']['LotSizeAcres'])) {?>

                                                    <li>Lot size: <?=number_format($feature['StandardFields']['LotSizeAcres'],2 ,'.',',')?> acres</li>

                                            <?php } elseif(!empty($feature['StandardFields']['LotSizeDimensions']) && ($feature['StandardFields']['LotSizeDimensions'] != "0")   && ($feature['StandardFields']['LotSizeDimensions'] != "********")) {?>

                                                <li class="lot-item">Lot size:<?=$feature['StandardFields']['LotSizeDimensions']?></li>
                                            <?php } ?>
                                            </ul>

                                            <?php 
                                                if($is_openhouse) { ?>
                                                    <style type="text/css">
                                                        .search-listing-item {
                                                            min-height: 410px;
                                                        }
                                                    </style>
                                            <?php
                                                    $pos = strpos(strtoupper($feature['StandardFields']['PublicRemarks']), 'OPEN HOUSE');
                                                    $ret = substr($feature['StandardFields']['PublicRemarks'], $pos, 60); ?>
                                                    <p style="color:red;"><?php echo $ret; ?>....
                                                        <a href="<?=base_url('other-property-details/');?><?php echo str_replace(" ",  "-", $feature['StandardFields']['ListingKey']);?>" target="_blank"> Read More</a>
                                                    </p>
                                            <?php } ?>
                                        </div>
                                    </div>
                                <?php 
                                }
                            }
                        }
                    ?>
            </div>
            <?php
                if(isset($total_count)) {
                    if($total_count > 25) { ?>
                        <div class="pagination-area">
                            <?php echo $pagination;?>
                        </div>
                    <?php
                    }
                }
            ?>
        </div>
    </section>