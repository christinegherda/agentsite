<?php if (!empty($is_saved_search_selected)): ?>
<div class="save-search-slider" style="margin-bottom: 20px;">
  <div class="container">
  <h2 class="other-listing-title text-center"><?= isset($saved_search_title["option_title"]) ? $saved_search_title["option_title"] :"Recommended Listings"?></h2>
      <!-- Set up your HTML -->
      <!-- <h2 class="other-listing-title">Recommended Listings</h2> -->
      <div class="owl-carousel-saved-search">
      <?php 
        $count_slider = count($slider_photos);
        $count_active = count($active_listings);
        $count_office = count($office_listings);
        $count_new = count($new_listings);

          if (!empty($slider_photos) && $count_slider >= 4 ) {
            foreach ($slider_photos as $photo) { 
              $bg_img[] = getenv('AWS_S3_ASSETS') . "uploads/slider/".$photo->photo_url;
            }
          } elseif (!empty($active_listings) && $count_active >= 4 ) {
              foreach ($active_listings as $active_photo) {
                $bg_img[] = isset($active_photo->StandardFields->Photos[0]->Uri300) ? $active_photo->StandardFields->Photos[0]->Uri300 : "";
              }
          } elseif (!empty($office_listings) && $count_office >= 4) {
              foreach ($office_listings as $office_photo) {
                  $bg_img[] = isset($office_photo['Photos']['Uri300']) ? $office_photo['Photos']['Uri300'] : "";
              }
          } elseif (!empty($new_listings) && $count_new >= 4) {
              foreach ($new_listings as $new_photo) {
                $bg_img[] = isset($new_photo->Photos->Uri300) ? $new_photo->Photos->Uri300 : "";
              }
          } else {
                $bg_img[] = "";
          }
      ?>

        <?php foreach ($is_saved_search_selected as $key => $saved): ?>
          <?php if ($key == 4): ?>
              <div class="saved-item viewall-list">
                <div class="save-image" style="background-image: url(<?=isset($nearby_listings[0]->Photos->Uri300) ? $nearby_listings[0]->Photos->Uri300: $bg_img[1] ;?>); background-size: 100%;"></div>
                <div class="overlay">
                 <a href="<?php echo base_url()?>all_saved_searches" target="_blank">View All <br> Saved Search</a> 
                </div>
              </div>
          <?php else: ?>
                <?php if ($key < 4): ?>
                  <?php $img = $bg_img[$key]; ?>
                    <div class="saved-item">
                      <div class="save-image" style="background-image: url(<?=$img?>); background-size: 100%;"></div>
                      <div class="saved-caption">
                            <a href="<?php echo base_url()?>saved_searches/<?php echo $saved['Id'];?>" target="_blank"> View All</a>
                       
                      </div>
                      <p class="searched-title">
                        <span><i class="fa fa-tasks"></i> <br> <?=$saved['Name']?></span>
                      </p>
                    </div>
                <?php endif ?>
          <?php endif ?>
        <?php endforeach ?>
      </div>
    </div>
</div>
<?php endif ?>
