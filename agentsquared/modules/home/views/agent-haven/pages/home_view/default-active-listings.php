        

        <section class="active-office-listing-area featured-listing-area">
            <div class="container">
                <div class="row">
                    <div class="featured-title-container">
                        <div class="trapezoid"></div>
                        <h2 class="text-center section-title">Featured <?php echo isset($agent_city)? $agent_city . ' ' : '';?>Properties</h2>
                    </div>
                     <div class="container-col">
                        <div class="featured-list">
                         <?php
                        //active_listings
                        if(isset($active_listings) && !empty($active_listings)) {
                            if(isset($active_listings) && !empty($active_listings)){
                                $countFeatured = count($active_listings);

                                if($countFeatured >= 4){?>

                                    <p class="featured-viewall"><a href="<?php echo base_url()?>active_listings" target="_blank">View All</a></p> 
                                <?php }
                            }

                            $itemCount = count($active_listings);
                            if($itemCount > 4) {
                                $itemCount = 4;
                            }
                            $iCount = 0;
                            $totalListingCount = 0;
                            foreach($active_listings as $feature) {
                                if(isset($feature->StandardFields->ListingKey)){
                                    if(!empty($feature->StandardFields->ListingKey)){
                                        if($iCount < $itemCount) {
                                            $totalListingCount++;
                                        }
                                        $iCount++;
                                    }
                                }
                            }

                             $this->load->view($theme."/pages/home_view/active-listings-section");

                            //If active_listings count = 0, add 4 office_listings
                            if($iCount == 0){
                                if(isset($office_listings) && !empty($office_listings)){

                                    $this->load->view($theme."/pages/home_view/office-listings-section");
                                }

                            // if active_listings count = 1, add 3 office_listings
                             } elseif($iCount == 1){

                                    if(!empty($office_listings[0]['StandardFields']['ListAgentId']) && !empty($active_listings[0]->StandardFields->ListAgentId)){

                                        if($office_listings[0]['StandardFields']['ListAgentId'] == $active_listings[0]->StandardFields->ListAgentId){

                                                $count2 = 0;
                                                //add 3 Nearby Listings if Office listing Id is === Active listing Id
                                                if(isset($nearby_listings) && !empty($nearby_listings)){

                                                    $this->load->view($theme."/pages/home_view/nearby-listings-section"); 
                                                }
                                        //add 3 office_listings if office_listings != active_listings
                                        } else {

                                            if(isset($office_listings) && !empty($office_listings)){

                                               $this->load->view($theme."/pages/home_view/office-listings-section");
                                            }
                                        }

                                    //add 3 office_listings if nearby_listings is empty
                                    } else {

                                        if(isset($office_listings) && !empty($office_listings)){

                                            $this->load->view($theme."/pages/home_view/office-listings-section");
                                        } else {
                                        //add 2 new_listings
                                             if(isset($new_listings) && !empty($new_listings)){

                                                $this->load->view($theme."/pages/home_view/new-listings-section");  
                                            }
                                        }
                                    }

                            //add 2 office_listings
                            } elseif($iCount == 2){

                                if(!empty($office_listings[0]['StandardFields']['ListAgentId']) && !empty($active_listings[0]->StandardFields->ListAgentId)){
                                        if($office_listings[0]['StandardFields']['ListAgentId'] == $active_listings[0]->StandardFields->ListAgentId){

                                            $count3 = 0;
                                            //add 2 nearby_listings if office_listings Id is === active_listings Id
                                            if(isset($nearby_listings) && !empty($nearby_listings)){

                                                $this->load->view($theme."/pages/home_view/nearby-listings-section");
                                            }
                                        //add 2 office_listings if office_listings != active_listings
                                        } else {

                                            if(isset($office_listings) && !empty($office_listings)){

                                                $this->load->view($theme."/pages/home_view/office-listings-section");
                                            }
                                        }

                                // add 2 office_listings if nearby_listings is empty
                                } else {
                                    if(isset($office_listings) && !empty($office_listings)){

                                            $this->load->view($theme."/pages/home_view/office-listings-section");
                                    } else {

                                        //add 2 new_listings
                                         if(isset($new_listings) && !empty($new_listings)){

                                            $this->load->view($theme."/pages/home_view/new-listings-section");
                                        }
                                    }
                                }


                            // add 1 office_listings
                            } elseif($iCount == 3){

                                if(!empty($office_listings[0]['StandardFields']['ListAgentId']) && !empty($active_listings[0]->StandardFields->ListAgentId)){
                                        if($office_listings[0]['StandardFields']['ListAgentId'] == $active_listings[0]->StandardFields->ListAgentId){

                                                $count4 = 0;
                                                //add 1 nearby_listings if office_listings Id is === active_listings Id
                                                if(isset($nearby_listings) && !empty($nearby_listings)){

                                                    $this->load->view($theme."/pages/home_view/nearby-listings-section");
                                                }
                                        //add  1 office_listings if office_listingsId !=  active_listings Id
                                        } else {
                                            if(isset($office_listings) && !empty($office_listings)){

                                                $this->load->view($theme."/pages/home_view/office-listings-section");
                                            }
                                        }

                                // add  1 office_listings if nearby_listings is empty
                                } else {
                                    if(isset($office_listings) && !empty($office_listings)){

                                        $$this->load->view($theme."/pages/home_view/office-listings-section");
                                    } else {
                                        //add 1 new_listings
                                         if(isset($new_listings) && !empty($new_listings)){

                                            $this->load->view($theme."/pages/home_view/new-listings-section");
                                        }
                                    }
                                }
                        }
                        //Add 4 office_listings if NO active_listings
                         } else {
                            if(isset($office_listings) && !empty($office_listings)){
                               $this->load->view($theme."/pages/home_view/office-listings-section");
                            }

                          }?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
