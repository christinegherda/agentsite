
        <div class="col-md-3 col-sm-4 nopadding">
            <div class="agent-image">
               <?php if( isset($user_info->agent_photo) && !empty($user_info->agent_photo)) {
                
                        $agent_photo = getenv('AWS_S3_ASSETS') . "uploads/photo/".$user_info->agent_photo;
                    ?>
                     <img itemprop="image" src="<?php echo $agent_photo?>" alt="<?=isset($user_info->first_name) ? htmlentities($user_info->first_name) : $account_info->FirstName?> <?=isset($user_info->last_name) ? htmlentities($user_info->last_name) : $account_info->LastName?>" class="img-thumbnail" width="300">
                <?php } else { ?>
                    <?php if (isset($account_info->Images[0]->Uri) && !empty($account_info->Images[0]->Uri)){?>
                        <img itemprop="image" src="<?=$account_info->Images[0]->Uri?>" alt="<?=isset($user_info->first_name) ? htmlentities($user_info->first_name) : $account_info->FirstName?> <?=isset($user_info->last_name) ? htmlentities($user_info->last_name) : $account_info->LastName?>" class="img-thumbnail" width="300">
                     <?php } else { ?>
                        <img itemprop="image" src="<?= base_url()?>assets/images/no-profile-img.gif" alt="No Profile Image">
                     <?php } ?>
                <?php } ?>
            </div>
        </div>
        <div class="col-md-9 col-sm-8 nopadding">

           <?php
                //first_name and last_name
                if(isset($user_info->first_name) && !empty($user_info->last_name)) {
                    $first_name = $user_info->first_name;
                    $last_name = $user_info->last_name;
                }elseif(isset($account_info->FirstName) && !empty($account_info->LastName)){
                    $first_name = $account_info->FirstName;
                    $last_name = $account_info->LastName;
                }
                //mobile_number
                if (isset($user_info->mobile) && !empty($user_info->mobile)) {
                    $mobile_number = $user_info->mobile;
                    
                } elseif(isset($account_info->Phones)){
                      foreach($account_info->Phones as $phone){
                          if($phone->Name == "Mobile"){
                              $mobile_number = $phone->Number;
                          }
                      }
                  }
                  //phone_number
                  if (isset($user_info->phone) && !empty($user_info->phone)) {
                      $phone_number = $user_info->phone;
                  } elseif(isset($account_info->Phones[0]->Number) && !empty($account_info->Phones[0]->Number) && ($account_info->Phones[0]->Number !== "********")) {
                       $phone_number = $account_info->Phones[0]->Number;
                  }
                  //address
                   if (isset($user_info->address) && !empty($user_info->address)) {
                      $address = $user_info->address;
                   } elseif(isset($account_info->Addresses[0]->Address) && !empty($account_info->Addresses[0]->Address) && ($account_info->Addresses[0]->Address !== "********")){
                      $address = $$account_info->Addresses[0]->Address;
                  }
                  //email
                  if (isset($user_info->email) && !empty($user_info->email)) {
                      $email = $user_info->email;
                  } elseif(isset($account_info->Emails[0]->Address) && !empty($account_info->Emails[0]->Address) && ($account_info->Emails[0]->Address !== "********")) {
                      $email = $account_info->Emails[0]->Addres;
                  }
                   //liscence number
                  if(isset($account_info->LicenseNumber) && !empty($account_info->LicenseNumber)){
                      $license_number = $account_info->LicenseNumber;
                  }
                  //broker
                  if(isset($account_info->OfficeId) && $account_info->OfficeId === '20160426214504162264000000') {
                       $broker = $account_info->Company;
                  } elseif(isset($account_info->Office) && !empty($account_info->Office) && ($account_info->Office !== "********")) {
                       $broker = $account_info->Office;
                  }
                  //fax number
                  if(isset($account_info->Phones)){
                      foreach($account_info->Phones as $phone){
                          if($phone->Name == "Fax"){
                            $fax_number = $phone->Number;
                          }
                      }
                  }
              ?>

               <h4 itemprop="name" class="agent-name">
                    <?php echo $first_name; ?>
                    <?php echo $last_name; ?>
                </h4>
                <div class="about-agent mobile-ipad-padding-0" style="display: inline-block; position: relative; width: 100%; margin-bottom: 20px;">
                    <?php
                        if (isset($user_info->about_agent) && !empty($user_info->about_agent)) {

                            $img = $user_info->about_agent;
                            $findImg   = 'data-filename';
                            $aboutImg = strpos($img, $findImg);

                            $about_len = strlen(strip_tags($user_info->about_agent));
                            $about_data = $user_info->about_agent;

                             echo ($aboutImg !== false) ? $user_info->about_agent : (($about_len > 400) ? html_cut($about_data,400).'... <a href="./about">Read more</a>' :  $user_info->about_agent);
                        }
                    ?>
                </div>
                <ul class="agent-detail" style="display: inline-block; position: relative; width: 100%;">
                    <?php

                      //hide McDonald Realty Agent name
                      if(isset($account_info->Id) && $account_info->Id != "20050122025315688539000000"){?>
                         <li>
                              <i class='fa fa-user'></i>
                               <?php echo $first_name." ".$last_name; ?> <!-- <?php echo isset($license_number) ? ', '.$license_number : '' ?> -->
                          </li>

                         <?php 
                      }
                      ////hide McDonald Realty Mobile Number
                      if(isset($account_info->Id) && $account_info->Id != "20050122025315688539000000"){
                        if(isset($phone_number)){
                          echo "<li><i class='fa fa-phone'></i> Office: <a href='tel:".$phone_number."'>".$phone_number."</a></li>";
                        } 
                      }
                      if(isset($mobile_number)){
                          echo "<li><i class='fa fa-mobile'></i> Mobile: <a href='tel:".$mobile_number."'><span itemprop=\"telephone\">".$mobile_number."</span></a></li>";
                      }
                       if(isset($address)){
                          echo "<li><i class='fa fa-map-marker'></i>  <span itemprop=\"address\">".$address."</span></li>";
                       }
                       if(isset($email)){
                         echo "<li><i class='fa fa-envelope'></i> <a href='mailto:".$email."'>".$email."</a></li>";
                       }
                       if(isset($license_number)){
                          if($account_info->Addresses[0]->Region == 'CA') {
                            echo "<li><i class='fa fa-certificate'></i> License Number: CAL-BRE#".$license_number."</li>";
                          } else {
                            echo "<li><i class='fa fa-certificate'></i> License Number: ".$license_number."</li>";
                          }
                       }
                       if(isset($broker)){
                        echo "<li><i class='fa fa-university'></i> Brokerage Name: ".$broker."</li>";
                       }
                       if(isset($fax_number)){
                        echo "<li><i class='fa fa-fax'></i> Fax Number: ".$fax_number ."</li>";
                       }
                       
                    ?>
                </ul>
        </div>
