<?php if(isset($nearby_listings) && !empty($nearby_listings)) {?>
	<section class="<?=(isset($is_nearby_featured)) ? "featured-listing-area" : "new-listing-area"?>">
		<div class="container">
			<div class="row">
				<div class="featured-title-container">

					<?php if($is_nearby_featured){?>
						<div class="trapezoid"></div>
					<?php }?>

					<h2 class="<?=(isset($is_nearby_featured)) ? "section-title" : ""?> other-listing-title text-center"><?php echo $nearby_title['option_title'];?></h2>
				</div>
				<?php

				$totalNL = count($nearby_listings);

				if ($totalNL == 3) {
					$container_class = "col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1";
					$col_class = "col-md-4 col-sm-4";
				} elseif ($totalNL == 2) {
					$container_class = "col-md-9 col-md-offset-3 col-sm-9 col-sm-offset-3";
					$col_class = "col-md-4 col-sm-4";
				}elseif ($totalNL == 1) {
					$container_class = "col-md-8 col-md-offset-4 col-sm-8 col-sm-offset-4";
					$col_class = "col-md-6 col-sm-6 listing-landscape";
				} else {
					$container_class = "col-md-12 col-sm-12";
					$col_class = "col-md-3 col-sm-3";
				}

				?>
				<div class="<?php echo $container_class; ?>">
					<div class="featured-list">
						<?php
						$limit = 4;
						if ( count($nearby_listings) >= $limit ) { ?>
							<p class="featured-viewall"><a href="<?php echo base_url()?>nearby_listings" target="_blank">View All</a></p><?php
						}

						$count = 0;

						foreach($nearby_listings as $nearby){

							if($count < 4) { ?>
								<div class="<?php echo $col_class; ?> featured-list-item">
									<div class="property-image <?php if($nearby->StandardFields->PropertyClass == 'Land' || $nearby->StandardFields->PropertyClass == 'MultiFamily'){ echo "property-image-land";} ?>">
										<?php
										if(!empty($token_checker)){?>
										<a href="<?= base_url();?>other-property-details/<?=$nearby->StandardFields->ListingKey;?>">
											<?php }
											if(isset($nearby->StandardFields->Photos[0]->Uri300)) { ?>
												<img src="<?=$nearby->StandardFields->Photos[0]->Uri300;?>" alt="<?= $nearby->StandardFields->UnparsedFirstLineAddress;?>" class="img-responsive" style="width:100%;">
											<?php } elseif(isset($nearby->Photos->Uri300)) { ?>
												<img src="<?=$nearby->Photos->Uri300;?>" alt="<?= $nearby->StandardFields->UnparsedFirstLineAddress;?>" class="img-responsive" style="width:100%;">
											<?php } else{?>
												<img src="<?=base_url()?>assets/images/image-not-available.jpg" alt="<?= $nearby->StandardFields->UnparsedFirstLineAddress;?>" class="img-responsive" style="width:100%;">
											<?php  } ?>
										</a>
									</div>
									<div class="property-listing-status">
										<?php echo $nearby->StandardFields->MlsStatus;?>
									</div>
									<div class="property-listing-price">
										<div class="property-listing-type">
											<?=$nearby->StandardFields->PropertyClass;?>
										</div>
										$<?=number_format($nearby->StandardFields->CurrentPrice);?>
									</div>
									<div class="property-quick-icons">
										<ul class="list-inline">

											<?php if(isset($nearby->StandardFields->BedsTotal) && !empty($nearby->StandardFields->BedsTotal)){
												if(($nearby->StandardFields->BedsTotal != "********")){?>
													<li><i class="fa fa-bed"></i> <?=$nearby->StandardFields->BedsTotal?> Bed</li>
												<?php } else{?>
													<li><i class="fa fa-bed"></i> N/A</li>
												<?php } ?>

											<?php  } else {?>
												<li><i class="fa fa-bed"></i> N/A</li>
											<?php }?>

											<?php if($account_info->Mls != "MLS BCS"){ ?>
												<?php if(isset($nearby->StandardFields->BathsTotal) && !empty($nearby->StandardFields->BathsTotal)){
													if(($nearby->StandardFields->BathsTotal != "********")){?>
														<li><i class="icon-toilet"></i> <?=$nearby->StandardFields->BathsTotal?> Bath</li>
													<?php }else if(isset($nearby->StandardFields->BathsFull) && $nearby->StandardFields->BathsFull != "********"){ ?>
														<li><i class="icon-toilet"></i> <?=$nearby->StandardFields->BathsFull?> Bath</li>
													<?php } else{?>
														<li><i class="icon-toilet"></i> N/A</li>
													<?php } ?>
												<?php }else if(isset($nearby->StandardFields->BathsFull) && $nearby->StandardFields->BathsFull != "********" ){ ?>
													<li><i class="icon-toilet"></i> <?=$nearby->StandardFields->BathsFull?> Bath</li>

												<?php  } else {?>
													<li><i class="icon-toilet"></i> N/A</li>
												<?php }?>

											<?php }else{
												if(isset($nearby->StandardFields->BathsFull) && !empty($nearby->StandardFields->BathsFull)){
													if(($nearby->StandardFields->BathsFull != "********")){?>
														<li><i class="icon-toilet"></i> <?=$nearby->StandardFields->BathsFull?> Full Baths <?=$nearby->StandardFields->BathsHalf?> Half Baths</li>
													<?php } else{?>
														<!-- <li><i class="icon-toilet"></i> N/A</li> -->
													<?php } ?>

												<?php  } else {?>
													<!--  <li><i class="icon-toilet"></i> N/A</li> -->
												<?php }?>
											<?php }?>

											<?php
											if(!empty($nearby->StandardFields->BuildingAreaTotal) && ($nearby->StandardFields->BuildingAreaTotal != "0")   && is_numeric($nearby->StandardFields->BuildingAreaTotal)) {?>

												<li class="lot-item"><?=number_format($nearby->StandardFields->BuildingAreaTotal)?> sqft</li>

											<?php } elseif(!empty($nearby->StandardFields->LotSizeArea) && ($nearby->StandardFields->LotSizeArea != "0")   && is_numeric($nearby->StandardFields->LotSizeArea)) {


												if(!empty($nearby->StandardFields->LotSizeUnits) && ($nearby->StandardFields->LotSizeUnits) === "Acres"){?>

													<li class="lot-item"><?=number_format($nearby->StandardFields->LotSizeArea, 2, '.', ',' )?> acres</li>

												<?php } else {?>

													<li class="lot-item"><?=number_format($nearby->StandardFields->LotSizeArea)?> acres</li>

												<?php }?>

											<?php } elseif(!empty($nearby->StandardFields->LotSizeSquareFeet) && ($nearby->StandardFields->LotSizeSquareFeet != "0")   && is_numeric($nearby->StandardFields->LotSizeSquareFeet)) {?>

												<li class="lot-item"><?=number_format($nearby->StandardFields->LotSizeSquareFeet)?> sqft</li>


											<?php } elseif(!empty($nearby->StandardFields->LotSizeAcres) && ($nearby->StandardFields->LotSizeAcres != "0")   && is_numeric($nearby->StandardFields->LotSizeAcres)) {?>

												<li class="lot-item"><?=number_format($nearby->StandardFields->LotSizeAcres,2 ,'.',',')?> acres</li>

											<?php } elseif(!empty($nearby->StandardFields->LotSizeDimensions) && ($nearby->StandardFields->LotSizeDimensions != "0")   && ($nearby->StandardFields->LotSizeDimensions != "********")) {?>

												<li class="lot-item"><?=$nearby->StandardFields->LotSizeDimensions?></li>
											<?php } else {?>
												<li class="lot-item">N/A</li>
											<?php } ?>
										</ul>
									</div>
									<div class="property-listing-description">
										<p>
											<?php if(!empty($token_checker)){?>
											<a class="listing-link" href="<?= base_url();?>other-property-details/<?=$nearby->StandardFields->ListingKey;?>">
												<?php }?>
												<b><?php echo $nearby->StandardFields->UnparsedFirstLineAddress; ?></b>
											</a>
										</p>
										<p>
											<?php
											if($account_info->Mls != "MLS BCS"){
												$mystring = $nearby->StandardFields->City;
												$postalcode = $nearby->StandardFields->PostalCode;
												$findme   = '*';
												$pos = strpos($mystring, $findme);
												$pos_code = strpos($postalcode, $findme);

												if($pos === false){
													if($pos_code === false){
														echo $nearby->StandardFields->City . ", " . $nearby->StandardFields->StateOrProvince . " " . $nearby->StandardFields->PostalCode;
													}else{
														echo $nearby->StandardFields->City . ", " . $nearby->StandardFields->StateOrProvince;
													}

												}
												else{
													if($pos_code === false){
														echo $nearby->StandardFields->PostalCity . ", " . $nearby->StandardFields->StateOrProvince . " " . $nearby->StandardFields->PostalCode;
													}else{
														echo $nearby->StandardFields->PostalCity . ", " . $nearby->StandardFields->StateOrProvince;
													}

												}
											}else{
												$mystring = $nearby->StandardFields->City;
												$StateOrProvince = $nearby->StandardFields->StateOrProvince;
												$findme   = '*';
												$pos = strpos($mystring, $findme);
												$pCode = strpos($StateOrProvince, $findme);

												if($pos === false && $pCode == false)
													echo $nearby->StandardFields->City;
												else
													echo $nearby->StandardFields->PostalCity . ", " . $nearby->StandardFields->StateOrProvince;
											}

											?>
										</p>
									</div>
								</div>

							<?php  } $count++;
						}
						?>
					</div>

				</div>
			</div>
		</div>
	</section>
<?php } ?>