
    <section class="filters">
        <div class="container">
            <div class="row">
                <div class="col-md-12">

                    <form action="<?php echo base_url(); ?>search-results" method="get" class="filter-property" id="basic_search_form">
                        <div class="filter-tabs">
                            <h1><?= (!empty($user_info->banner_tagline)) ? htmlentities($user_info->banner_tagline) : "Everyone needs a place to call HOME"; ?></h1>
                        </div>
                        <div class="input-group">
                            <input type="text" class="form-control home-search" name="Search" placeholder="Address, City, Zip Code" maxlength="100" value="<?=$this->input->get('Search');?>" id="filter_search_field" required>
                            <span class="input-group-btn">
                                <button class="btn btn-default submit-button">Search</button>
                            </span>
                        </div>
                        <div class="help-info">
                            <div class="arrow-up"></div>
                            <p>Examples:</p>
                            <ul>
                                <?php
                                    if(isset($new_listings)) { ?>
                                        <?php if($account_info->MlsId != "MLS BCS") { ?>
                                            <li>
                                                <div class="col-md-4 col-sm-4 col-xs-5">
                                                    <p>City</p>
                                                </div>
                                                <div class="col-md-8 col-sm-8 col-xs-7">
                                                    <p>
                                                        <?php
                                                            echo isset($new_listings[0]->StandardFields->City) ? $new_listings[0]->StandardFields->City . " ": "" ;
                                                        ?>
                                                    </p>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="col-md-4 col-sm-4 col-xs-5">
                                                    <p>Address</p>
                                                </div>
                                                <div class="col-md-8 col-sm-8 col-xs-7">
                                                    <p><?php echo isset($new_listings[0]->StandardFields->UnparsedAddress) ? $new_listings[0]->StandardFields->UnparsedAddress : "" ; ?></p>
                                                </div>
                                            </li>

                                        <?php } else{ ?>
                                            <li>
                                                <div class="col-md-4 col-sm-4 col-xs-5">
                                                    <p>City, State</p>
                                                </div>
                                                <div class="col-md-8 col-sm-8 col-xs-7">
                                                    <p>
                                                        <?php
                                                            echo isset($new_listings[0]->StandardFields->City) ? $new_listings[0]->StandardFields->City . ", ": "" ;
                                                            echo isset($new_listings[0]->StandardFields->StateOrProvince) ? $new_listings[0]->StandardFields->StateOrProvince : "" ;
                                                        ?>
                                                    </p>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="col-md-4 col-sm-4 col-xs-5">
                                                    <p>Zip Code</p>
                                                </div>
                                                <div class="col-md-8 col-sm-8 col-xs-7">
                                                    <p><?php echo isset($new_listings[0]->StandardFields->PostalCode) ? $new_listings[0]->StandardFields->PostalCode : "" ; ?></p>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="col-md-4 col-sm-4 col-xs-5">
                                                    <p>Address</p>
                                                </div>
                                                <div class="col-md-8 col-sm-8 col-xs-7">
                                                    <p><?php echo isset($new_listings[0]->StandardFields->UnparsedAddress) ? $new_listings[0]->StandardFields->UnparsedAddress : "" ; ?></p>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="col-md-4 col-sm-4 col-xs-5">
                                                    <p>County</p>
                                                </div>
                                                <div class="col-md-8 col-sm-8 col-xs-7">
                                                    <p><?php echo isset($new_listings[0]->StandardFields->CountyOrParish) ? $new_listings[0]->StandardFields->CountyOrParish : "" ; ?></p>
                                                </div>
                                            </li>
                                        <?php } ?>
                                        <li>
                                            <div class="col-md-4 col-sm-4 col-xs-5">
                                                <p>Street Address</p>
                                            </div>
                                            <div class="col-md-8 col-sm-8 col-xs-7">
                                                <p><?php echo isset($new_listings[0]->StandardFields->UnparsedFirstLineAddress) ? $new_listings[0]->StandardFields->UnparsedFirstLineAddress : "" ; ?></p>
                                            </div>
                                        </li>
                                        <!-- <li>
                                            <div class="col-md-4 col-sm-4 col-xs-5">
                                                <p>Listing ID</p>
                                            </div>
                                            <div class="col-md-8 col-sm-8 col-xs-7">
                                                <p><?php echo isset($new_listings[0]->StandardFields->ListingKey) ? $new_listings[0]->StandardFields->ListingKey : "" ; ?></p>
                                            </div>
                                        </li> -->
                                       <!--  <li>
                                            <div class="col-md-4 col-sm-4 col-xs-5">
                                                <p>MLS ID</p>
                                            </div>
                                            <div class="col-md-8 col-sm-8 col-xs-7">
                                                <p><?php echo isset($new_listings[0]->StandardFields->MlsId ) ? $new_listings[0]->StandardFields->MlsId : "" ; ?></p>
                                            </div>
                                        </li> -->
                                <?php
                                    }
                                ?>
                            </ul>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
