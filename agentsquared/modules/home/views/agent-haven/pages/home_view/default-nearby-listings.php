<?php if (isset($nearby_listings) && !empty($nearby_listings)): ?>
	<div class="col-md-6 col-sm-12">
		<div class="nearby-listings-area">
			<h2 class="other-listing-title"> <?php echo isset($nearby_title['option_title']) ? $nearby_title['option_title'] : "Nearby Listings" ;?>
				<?php
				$limit = 4;
				if ( count($nearby_listings) >= $limit ): ?>
					<span class="viewall-newlisting">
					<a href="<?php base_url()?>nearby_listings" target="_blank" data-original-title="" title="">View All</a>
					</span><?php
				endif;
				?></h2>
			<ul><?php
				foreach($nearby_listings as $index => $nearby):
					if ($index < $limit): ?>
						<li>
						<div class="col-md-4 col-sm-4 col-xs-4 no-padding-left">
							<div class="nearby-image">
								<?php if(!empty($token_checker)){?>
								<a href="<?= base_url();?>other-property-details/<?=$nearby->StandardFields->ListingKey;?>">
									<?php }
									if(isset($nearby->StandardFields->Photos[0]->Uri300)) { ?>
										<img src="<?=$nearby->StandardFields->Photos[0]->Uri300;?>" alt="<?= $nearby->StandardFields->UnparsedFirstLineAddress;?>" class="img-responsive" style="width:100%;">
									<?php } elseif(isset($nearby->Photos->Uri300)) { ?>
										<img src="<?=$nearby->Photos->Uri300;?>" alt="<?= $nearby->StandardFields->UnparsedFirstLineAddress;?>" class="img-responsive" style="width:100%;">
									<?php } else{?>
										<img src="<?=base_url()?>assets/images/image-not-available.jpg" alt="<?= $nearby->StandardFields->UnparsedFirstLineAddress;?>" class="img-responsive" style="width:100%;">
									<?php  } ?>
								</a>
							</div>
							<div class="property-listing-status">
								<?php echo $nearby->StandardFields->MlsStatus;?>
							</div>
							<div itemprop="offers" itemscope="" itemtype="http://schema.org/Offer" class="visible-xs-block property-listing-price">
								<div class="property-listing-type"><?= $nearby->StandardFields->PropertyClass ?></div>
								<span itemprop="priceCurrency" content="USD">$</span>
								<span itemprop="price" content="<?= $nearby->StandardFields->CurrentPrice ?>"><?=number_format($nearby->StandardFields->CurrentPrice);?></span>
							</div>
						</div>
						<div class="nearby-description clearfix">
							<div class="col-md-8 col-sm-8 col-xs-8">
								<h4>
									<?php if(!empty($token_checker)){?>
									<a href="<?= base_url();?>other-property-details/<?=$nearby->StandardFields->ListingKey;?>">
										<?php }?>
										<b><?php echo $nearby->StandardFields->UnparsedFirstLineAddress; ?></b>
									</a>
								</h4>
								<div itemprop="offers" itemscope="" itemtype="http://schema.org/Offer" class="visible-lg-block visible-md-block visible-sm-block property-listing-price">
									<div class="property-listing-type"><?= $nearby->StandardFields->PropertyClass ?></div>
									<span itemprop="priceCurrency" content="USD">$</span>
									<span itemprop="price" content="<?= $nearby->StandardFields->CurrentPrice ?>"><?=number_format($nearby->StandardFields->CurrentPrice);?></span>
								</div>
								<p>

									<?php

									if($account_info->Mls != "MLS BCS"){
										$mystring = $nearby->StandardFields->City;
										$postalcode = $nearby->StandardFields->PostalCode;
										$findme   = '*';
										$pos = strpos($mystring, $findme);
										$pos_code = strpos($postalcode, $findme);

										if($pos === false){
											if($pos_code === false){
												echo $nearby->StandardFields->City . ", " . $nearby->StandardFields->StateOrProvince . " " . $nearby->StandardFields->PostalCode;
											}else{
												echo $nearby->StandardFields->City . ", " . $nearby->StandardFields->StateOrProvince;
											}

										}
										else{
											if($pos_code === false){
												echo $nearby->StandardFields->PostalCity . ", " . $nearby->StandardFields->StateOrProvince . " " . $nearby->StandardFields->PostalCode;
											}else{
												echo $nearby->StandardFields->PostalCity . ", " . $nearby->StandardFields->StateOrProvince;
											}

										}
									}else{
										$mystring = $nearby->StandardFields->City;
										$StateOrProvince = $nearby->StandardFields->StateOrProvince;
										$findme   = '*';
										$pos = strpos($mystring, $findme);
										$pCode = strpos($StateOrProvince, $findme);

										if($pos === false && $pCode == false)
											echo $nearby->StandardFields->City;
										else
											echo $nearby->StandardFields->PostalCity . ", " . $nearby->StandardFields->StateOrProvince;
									}

									?>
								</p>
								<ul class="list-inline">

									<?php if(isset($nearby->StandardFields->BedsTotal) && !empty($nearby->StandardFields->BedsTotal)){
										if(($nearby->StandardFields->BedsTotal != "********")){?>
											<li><i class="fa fa-bed"></i> <?=$nearby->StandardFields->BedsTotal?> Bed</li>
										<?php } else{?>
											<li><i class="fa fa-bed"></i> N/A</li>
										<?php } ?>

									<?php  } else {?>
										<li><i class="fa fa-bed"></i> N/A</li>
									<?php }?>

									<?php if($account_info->Mls != "MLS BCS"){ ?>

										<?php if(isset($nearby->StandardFields->BathsTotal) && !empty($nearby->StandardFields->BathsTotal)){
											if(($nearby->StandardFields->BathsTotal != "********")){?>
												<li><i class="icon-toilet"></i> <?=$nearby->StandardFields->BathsTotal?> Bath</li>
											<?php }else if(isset($nearby->StandardFields->BathsFull) && $nearby->StandardFields->BathsFull != "********"){ ?>
												<li><i class="icon-toilet"></i> <?=$nearby->StandardFields->BathsFull?> Bath</li>
											<?php } else{?>
												<li><i class="icon-toilet"></i> N/A</li>
											<?php } ?>

										<?php }else if(isset($nearby->StandardFields->BathsFull) && $nearby->StandardFields->BathsFull != "********"){ ?>
											<li><i class="icon-toilet"></i> <?=$nearby->StandardFields->BathsFull?> Bath</li>

										<?php  } else {?>
											<li><i class="icon-toilet"></i> N/A</li>
										<?php }?>
									<?php }else{
										if(isset($nearby->StandardFields->BathsFull) && !empty($nearby->StandardFields->BathsFull)){
											if(($nearby->StandardFields->BathsFull != "********")){?>
												<li><i class="icon-toilet"></i> <?=$nearby->StandardFields->BathsFull?> Full Baths <?=$nearby->StandardFields->BathsHalf?> Half Baths</li>
											<?php } else{?>
												<!-- <li><i class="icon-toilet"></i> N/A</li> -->
											<?php } ?>

										<?php  } else {?>
											<!--  <li><i class="icon-toilet"></i> N/A</li> -->
										<?php }?>
									<?php }?>
									<?php
									if(!empty($nearby->StandardFields->BuildingAreaTotal) && ($nearby->StandardFields->BuildingAreaTotal != "0")   && is_numeric($nearby->StandardFields->BuildingAreaTotal)) {?>

										<li class="lot-item"><?=number_format($nearby->StandardFields->BuildingAreaTotal)?> sqft</li>

									<?php } elseif(!empty($nearby->StandardFields->LotSizeArea) && ($nearby->StandardFields->LotSizeArea != "0")   && is_numeric($nearby->StandardFields->LotSizeArea)) {


										if(!empty($nearby->StandardFields->LotSizeUnits) && ($nearby->StandardFields->LotSizeUnits) === "Acres"){?>

											<li class="lot-item"><?=number_format($nearby->StandardFields->LotSizeArea, 2, '.', ',' )?> acres</li>

										<?php } else {?>

											<li class="lot-item"><?=number_format($nearby->StandardFields->LotSizeArea)?> acres</li>

										<?php }?>

									<?php } elseif(!empty($nearby->StandardFields->LotSizeSquareFeet) && ($nearby->StandardFields->LotSizeSquareFeet != "0")   && is_numeric($nearby->StandardFields->LotSizeSquareFeet)) {?>

										<li class="lot-item"><?=number_format($nearby->StandardFields->LotSizeSquareFeet)?> sqft</li>


									<?php } elseif(!empty($nearby->StandardFields->LotSizeAcres) && ($nearby->StandardFields->LotSizeAcres != "0")   && is_numeric($nearby->StandardFields->LotSizeAcres)) {?>

										<li class="lot-item"><?=number_format($nearby->StandardFields->LotSizeAcres,2 ,'.',',')?> acres</li>

									<?php } elseif(!empty($nearby->StandardFields->LotSizeDimensions) && ($nearby->StandardFields->LotSizeDimensions != "0")   && ($nearby->StandardFields->LotSizeDimensions != "********")) {?>

										<li class="lot-item"><?=$nearby->StandardFields->LotSizeDimensions?></li>
									<?php } else {?>
										<li class="lot-item">N/A</li>
									<?php } ?>
								</ul>
							</div>
						</div>
						</li><?php
					endif;
				endforeach; ?>
			</ul>
		</div>
	</div>
<?php endif ?>