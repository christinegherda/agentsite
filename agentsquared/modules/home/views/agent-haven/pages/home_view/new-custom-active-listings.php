        
<?php if(isset($active_listings) && !empty($active_listings)) {?>

      <section class="active-office-listing-area <?=(isset($is_active_featured)) ? "featured-listing-area" : "new-listing-area"?>">
            <div class="container">
                <div class="featured-title-container">

                    <?php if($is_active_featured){?>
                        <div class="trapezoid"></div>
                    <?php }?>
                    <h2 class="text-center other-listing-title <?=(isset($is_active_featured)) ? "section-title" : ""?>"><span><?php echo $active_title['option_title'];?></span></h2>
                    
                </div>

                 <?php 
                    $totalOL = count($active_listings);

                    if ($totalOL == 3) {
                        $container_class = "col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1";
                        $col_class = "col-md-4 col-sm-4";
                    } elseif ($totalOL == 2) {
                        $container_class = "col-md-12 col-sm-12";
                        $col_class = "col-md-6 col-sm-6 listing-landscape";
                    }elseif ($totalOL == 1) {
                        $container_class = "col-md-8 col-md-offset-2 col-sm-12";
                        $col_class = "col-md-12 col-sm-12 listing-single-lanscape";
                    } else {
                        $container_class = "col-md-12 col-sm-12";
                        $col_class = "col-md-3 col-sm-3";
                    }
                ?>
                <div class="row">
                   <div class="<?php echo $container_class;?>">
                        <div class="featured-list">

                             <?php
                            if(isset($active_listings) && !empty($active_listings)) {

                                    $countFeatured = count($active_listings);
                                    $active_count = isset($active_listings_count) ? $active_listings_count : ""; 

                                    if($countFeatured >= 4 || $active_count >=4 ){?>

                                        <p class="featured-viewall"><a href="<?php echo base_url()?>active_listings" target="_blank">View All</a></p> 
                                    <?php } ?>

                                    <?php if ($totalOL == 1 || $totalOL == 2): ?>

                                        <?php foreach ($active_listings as $feature): ?>
                                            <!-- <div class="row"> -->
                                              <div class="<?=$col_class; ?>">
                                                <div class="property-list-view">
                                                  <div class="col-md-5 col-sm-5">
                                                      <div class="property-status">
                                                          <?=$feature->StandardFields->PropertyClass;?>
                                                      </div>
                                                      <?php
                                                        $url_rewrite = url_title("{$feature->StandardFields->UnparsedFirstLineAddress} {$feature->StandardFields->PostalCode}");
                                                        ?>
                                                        <?php if(!empty($token_checker)) { ?>
                                                                <a href="<?= base_url();?>property-details/<?php echo $url_rewrite ?>/<?= $feature->StandardFields->ListingKey; ?>" class="image">
                                                        <?php }
                                                            if(isset($feature->StandardFields->Photos[0]->Uri300)) { ?>
                                                                <img src="<?=$feature->StandardFields->Photos[0]->Uri300?>" alt="<?php echo $feature->StandardFields->UnparsedFirstLineAddress; ?>" class="img-responsive" style="width:100%;">
                                                        <?php }
                                                            else if(isset($feature->Photos->Uri300)) { ?>
                                                                <img src="<?=$feature->Photos->Uri300?>" alt="<?php echo $feature->StandardFields->UnparsedFirstLineAddress; ?>" class="img-responsive" style="width:100%;">
                                                        <?php }
                                                            else { ?>
                                                                <img src="<?=base_url()?>assets/images/image-not-available.jpg" alt="<?php echo $feature->StandardFields->UnparsedFirstLineAddress; ?>" class="img-responsive" style="width:100%;">
                                                        <?php } ?>
                                                            </a>
                                                  </div>
                                                  <div class="col-md-7 col-sm-7">
                                                      <div class="description">
                                                        <h3><?php
                                                            $url_rewrite = url_title("{$feature->StandardFields->UnparsedFirstLineAddress} {$feature->StandardFields->PostalCode}");
                                                            ?>
                                                         <?php if(!empty($token_checker)) { ?>
                                                            <a class="listing-link" href="<?= base_url();?>property-details/<?php echo $url_rewrite ?>/<?=$feature->StandardFields->ListingKey;?>">
                                                                <b><?php echo $feature->StandardFields->UnparsedFirstLineAddress; ?></b>
                                                            </a>
                                                        <?php } ?>

                                                        </h3>
                                                        <p>
                                                           <?php
                                                            if($account_info->Mls != "MLS BCS"){
                                                                $mystring = $feature->StandardFields->City;
                                                                $postalcode = $feature->StandardFields->PostalCode;
                                                                $findme   = '*';
                                                                $pos = strpos($mystring, $findme);
                                                                $pos_code = strpos($postalcode, $findme);


                                                                if($pos === false){
                                                                    if($pos_code === false){
                                                                        echo $feature->StandardFields->City . ", " . $feature->StandardFields->StateOrProvince . " " . $feature->StandardFields->PostalCode;
                                                                    }else{
                                                                        echo $feature->StandardFields->City . ", " . $feature->StandardFields->StateOrProvince;
                                                                    }

                                                                }
                                                                else{
                                                                    if($pos_code === false){
                                                                        echo $feature->StandardFields->PostalCity . ", " . $feature->StandardFields->StateOrProvince . " " . $feature->StandardFields->PostalCode;
                                                                    }else{
                                                                       echo $feature->StandardFields->PostalCity . ", " . $feature->StandardFields->StateOrProvince;
                                                                    }

                                                                }

                                                            }else{
                                                                $mystring = $feature->StandardFields->City;
                                                                $StateOrProvince = $feature->StandardFields->StateOrProvince;
                                                                $findme   = '*';
                                                                $pos = strpos($mystring, $findme);
                                                                $pCode = strpos($StateOrProvince, $findme);

                                                                if($pos === false && $pCode == false)
                                                                    echo $feature->StandardFields->City;
                                                                else
                                                                    echo $feature->StandardFields->PostalCity . ", " . $feature->StandardFields->StateOrProvince;
                                                            }
                                                            ?>   

                                                        </p>
                                                      </div>
                                                      <div class="price">
                                                          $ <?=number_format($feature->StandardFields->CurrentPrice);?>
                                                      </div>
                                                      <ul class="prop-details">
                                                        <?php if(isset($feature->StandardFields->BedsTotal) && !empty($feature->StandardFields->BedsTotal)){
                                                                if(($feature->StandardFields->BedsTotal != "********")){?>
                                                                    <li><i class="fa fa-bed"></i> <?=$feature->StandardFields->BedsTotal?> Bed</li>
                                                               <?php } else{?>
                                                                    <li><i class="fa fa-bed"></i> N/A</li>
                                                               <?php } ?>

                                                         <?php  } else {?>
                                                                <li><i class="fa fa-bed"></i> N/A</li>
                                                        <?php }?>
                                                        <?php if($account_info->Mls != "MLS BCS"){ ?>
                                                            <?php if(isset($feature->StandardFields->BathsTotal) && !empty($feature->StandardFields->BathsTotal)){
                                                                    if(($feature->StandardFields->BathsTotal != "********")){?>
                                                                        <li><i class="icon-toilet"></i> <?=$feature->StandardFields->BathsTotal?> Bath</li>

                                                                    <?php }else if(isset($feature->StandardFields->BathsFull) && $feature->StandardFields->BathsFull != "********"){ ?>
                                                                    <li><i class="icon-toilet"></i> <?=$feature->StandardFields->BathsFull?> Bath</li>

                                                                   <?php } else{?>
                                                                        <li><i class="icon-toilet"></i> N/A</li>
                                                                   <?php } ?>
                                                                   
                                                            <?php }else if(isset($feature->StandardFields->BathsFull) && $feature->StandardFields->BathsFull != "********"){ ?>
                                                                    <li><i class="icon-toilet"></i> <?=$feature->StandardFields->BathsFull?> Bath</li>

                                                             <?php  } else {?>
                                                                   <li><i class="icon-toilet"></i> N/A</li>
                                                            <?php }?>
                                                        <?php }else{
                                                            if(isset($feature->StandardFields->BathsFull) && !empty($feature->StandardFields->BathsFull)){
                                                                    if(($feature->StandardFields->BathsFull != "********")){?>
                                                                        <li><i class="icon-toilet"></i> <?=$feature->StandardFields->BathsFull?> BathsFull <?=$feature->StandardFields->BathsHalf?> BathsHalf</li>
                                                                   <?php } else{?>
                                                                        <!-- <li><i class="icon-toilet"></i> N/A</li> -->
                                                                   <?php } ?>

                                                             <?php  } else {?>
                                                                  <!--  <li><i class="icon-toilet"></i> N/A</li> -->
                                                            <?php }?>
                                                        <?php }?>
                                                         <?php
                                                            if(!empty($feature->StandardFields->BuildingAreaTotal) && ($feature->StandardFields->BuildingAreaTotal != "0")   && is_numeric($feature->StandardFields->BuildingAreaTotal)) {?>

                                                                <li class="lot-item"><?=number_format($feature->StandardFields->BuildingAreaTotal)?> sqft</li>

                                                        <?php } elseif(!empty($feature->StandardFields->LotSizeArea) && ($feature->StandardFields->LotSizeArea != "0")   && is_numeric($feature->StandardFields->LotSizeArea)) {


                                                                if(!empty($feature->StandardFields->LotSizeUnits)  && ($feature->StandardFields->LotSizeUnits === "Acres") ){?>

                                                                    <li class="lot-item"><?=number_format($feature->StandardFields->LotSizeArea, 2, '.', ',')?> acres</li>

                                                                <?php } else { ?>

                                                                    <li class="lot-item"><?=number_format($feature->StandardFields->LotSizeArea)?> acres</li>

                                                                <?php } ?>

                                                          <?php } elseif(!empty($feature->StandardFields->LotSizeSquareFeet) && ($feature->StandardFields->LotSizeSquareFeet != "0")   && is_numeric($feature->StandardFields->LotSizeSquareFeet)) {?>

                                                                <li class="lot-item"><?=number_format($feature->StandardFields->LotSizeSquareFeet)?> 
                                                                sqft</li>

                                                         <?php } elseif(!empty($feature->StandardFields->LotSizeAcres) && ($feature->StandardFields->LotSizeAcres != "0")   && is_numeric($feature->StandardFields->LotSizeAcres)) {?>

                                                                <li class="lot-item"><?=number_format($feature->StandardFields->LotSizeAcres,2 ,'.',',')?> acres</li>

                                                        <?php } elseif(!empty($feature->StandardFields->LotSizeDimensions) && ($feature->StandardFields->LotSizeDimensions != "0")   && ($feature->StandardFields->LotSizeDimensions != "********")) {?>

                                                                <li class="lot-item"><?=$feature->StandardFields->LotSizeDimensions?></li>
                                                        <?php } else {?>
                                                                <li class="lot-item">N/A</li>
                                                        <?php } ?>
                                                      </ul>
                                                  </div>
                                                </div>
                                              </div>
                                            <!-- </div> -->
                                        <?php endforeach ?>
                                    <?php else: ?>
                                        <?php foreach($active_listings as $feature) {?>

                                          <div class="<?php echo $col_class; ?>  featured-list-item">
                                                <div class="property-image <?php if($feature->StandardFields->PropertyClass == 'Land' || $feature->StandardFields->PropertyClass == 'MultiFamily'){ echo "property-image-land";} ?>">
                                                    <?php
                                                    $url_rewrite = url_title("{$feature->StandardFields->UnparsedFirstLineAddress} {$feature->StandardFields->PostalCode}");
                                                    ?>
                                                    <?php if(!empty($token_checker)) { ?>
                                                            <a href="<?= base_url();?>property-details/<?php echo $url_rewrite ?>/<?= $feature->StandardFields->ListingKey; ?>">
                                                    <?php }
                                                        if(isset($feature->StandardFields->Photos[0]->Uri300)) { ?>
                                                            <img src="<?=$feature->StandardFields->Photos[0]->Uri300?>" alt="<?php echo $feature->StandardFields->UnparsedFirstLineAddress; ?>" class="img-responsive" style="width:100%;">
                                                    <?php }
                                                        else if(isset($feature->Photos->Uri300)) { ?>
                                                            <img src="<?=$feature->Photos->Uri300?>" alt="<?php echo $feature->StandardFields->UnparsedFirstLineAddress; ?>" class="img-responsive" style="width:100%;">
                                                    <?php }
                                                        else { ?>
                                                            <img src="<?=base_url()?>assets/images/image-not-available.jpg" alt="<?php echo $feature->StandardFields->UnparsedFirstLineAddress; ?>" class="img-responsive" style="width:100%;">
                                                    <?php } ?>
                                                        </a>
                                                </div>
                                                <div class="property-listing-status">
                                                    <?php echo $feature->StandardFields->MlsStatus;?>
                                                </div>
                                                <div class="property-listing-price">
                                                    <div class="property-listing-type">
                                                        <?=$feature->StandardFields->PropertyClass;?>
                                                    </div>
                                                    $<?=number_format($feature->StandardFields->CurrentPrice);?>
                                                </div>
                                                <div class="property-quick-icons">
                                                     <ul class="list-inline">
                                                        <?php if(isset($feature->StandardFields->BedsTotal) && !empty($feature->StandardFields->BedsTotal)){
                                                                if(($feature->StandardFields->BedsTotal != "********")){?>
                                                                    <li><i class="fa fa-bed"></i> <?=$feature->StandardFields->BedsTotal?> Bed</li>
                                                               <?php } else{?>
                                                                    <li><i class="fa fa-bed"></i> N/A</li>
                                                               <?php } ?>

                                                         <?php  } else {?>
                                                                <li><i class="fa fa-bed"></i> N/A</li>
                                                        <?php }?>
                                                        <?php if($account_info->Mls != "MLS BCS"){ ?>
                                                            <?php if(isset($feature->StandardFields->BathsTotal) && !empty($feature->StandardFields->BathsTotal)){
                                                                    if(($feature->StandardFields->BathsTotal != "********")){?>
                                                                        <li><i class="icon-toilet"></i> <?=$feature->StandardFields->BathsTotal?> Bath</li>

                                                                    <?php }else if(isset($feature->StandardFields->BathsFull) && $feature->StandardFields->BathsFull != "********"){ ?>
                                                                    <li><i class="icon-toilet"></i> <?=$feature->StandardFields->BathsFull?> Bath</li>

                                                                   <?php } else{?>
                                                                        <li><i class="icon-toilet"></i> N/A</li>
                                                                   <?php } ?>
                                                                   
                                                            <?php }else if(isset($feature->StandardFields->BathsFull) && $feature->StandardFields->BathsFull != "********"){ ?>
                                                                    <li><i class="icon-toilet"></i> <?=$feature->StandardFields->BathsFull?> Bath</li>

                                                             <?php  } else {?>
                                                                   <li><i class="icon-toilet"></i> N/A</li>
                                                            <?php }?>
                                                        <?php }else{
                                                            if(isset($feature->StandardFields->BathsFull) && !empty($feature->StandardFields->BathsFull)){
                                                                    if(($feature->StandardFields->BathsFull != "********")){?>
                                                                        <li><i class="icon-toilet"></i> <?=$feature->StandardFields->BathsFull?> BathsFull <?=$feature->StandardFields->BathsHalf?> BathsHalf</li>
                                                                   <?php } else{?>
                                                                        <!-- <li><i class="icon-toilet"></i> N/A</li> -->
                                                                   <?php } ?>

                                                             <?php  } else {?>
                                                                  <!--  <li><i class="icon-toilet"></i> N/A</li> -->
                                                            <?php }?>
                                                        <?php }?>
                                                         <?php
                                                            if(!empty($feature->StandardFields->BuildingAreaTotal) && ($feature->StandardFields->BuildingAreaTotal != "0")   && is_numeric($feature->StandardFields->BuildingAreaTotal)) {?>

                                                                <li class="lot-item"><?=number_format($feature->StandardFields->BuildingAreaTotal)?> sqft</li>

                                                        <?php } elseif(!empty($feature->StandardFields->LotSizeArea) && ($feature->StandardFields->LotSizeArea != "0")   && is_numeric($feature->StandardFields->LotSizeArea)) {


                                                                if(!empty($feature->StandardFields->LotSizeUnits)  && ($feature->StandardFields->LotSizeUnits === "Acres") ){?>

                                                                    <li class="lot-item"><?=number_format($feature->StandardFields->LotSizeArea, 2, '.', ',')?></li>

                                                                <?php } else { ?>

                                                                    <li class="lot-item"><?=number_format($feature->StandardFields->LotSizeArea)?></li>

                                                                <?php } ?>

                                                          <?php } elseif(!empty($feature->StandardFields->LotSizeSquareFeet) && ($feature->StandardFields->LotSizeSquareFeet != "0")   && is_numeric($feature->StandardFields->LotSizeSquareFeet)) {?>

                                                                <li class="lot-item"><?=number_format($feature->StandardFields->LotSizeSquareFeet)?>
                                                                </li>

                                                         <?php } elseif(!empty($feature->StandardFields->LotSizeAcres) && ($feature->StandardFields->LotSizeAcres != "0")   && is_numeric($feature->StandardFields->LotSizeAcres)) {?>

                                                                <li class="lot-item"><?=number_format($feature->StandardFields->LotSizeAcres,2 ,'.',',')?></li>

                                                        <?php } elseif(!empty($feature->StandardFields->LotSizeDimensions) && ($feature->StandardFields->LotSizeDimensions != "0")   && ($feature->StandardFields->LotSizeDimensions != "********")) {?>

                                                                <li class="lot-item"><?=$feature->StandardFields->LotSizeDimensions?></li>
                                                        <?php } else {?>
                                                                <li class="lot-item">N/A</li>
                                                        <?php } ?>
                                                    </ul>
                                                </div>
                                                <div class="property-listing-description">
                                                    <?php
                                                    $url_rewrite = url_title("{$feature->StandardFields->UnparsedFirstLineAddress} {$feature->StandardFields->PostalCode}");
                                                    ?>
                                                    <p>
                                                        <?php if(!empty($token_checker)) { ?>
                                                            <a class="listing-link" href="<?= base_url();?>property-details/<?php echo $url_rewrite ?>/<?=$feature->StandardFields->ListingKey;?>">
                                                                <b><?php echo $feature->StandardFields->UnparsedFirstLineAddress; ?></b>
                                                            </a>
                                                        <?php } ?>
                                                    </p>
                                                    <p>

                                                   <?php
                                                    if($account_info->Mls != "MLS BCS"){
                                                        $mystring = $feature->StandardFields->City;
                                                        $postalcode = $feature->StandardFields->PostalCode;
                                                        $findme   = '*';
                                                        $pos = strpos($mystring, $findme);
                                                        $pos_code = strpos($postalcode, $findme);


                                                        if($pos === false){
                                                            if($pos_code === false){
                                                                echo $feature->StandardFields->City . ", " . $feature->StandardFields->StateOrProvince . " " . $feature->StandardFields->PostalCode;
                                                            }else{
                                                                echo $feature->StandardFields->City . ", " . $feature->StandardFields->StateOrProvince;
                                                            }

                                                        }
                                                        else{
                                                            if($pos_code === false){
                                                                echo $feature->StandardFields->PostalCity . ", " . $feature->StandardFields->StateOrProvince . " " . $feature->StandardFields->PostalCode;
                                                            }else{
                                                               echo $feature->StandardFields->PostalCity . ", " . $feature->StandardFields->StateOrProvince;
                                                            }

                                                        }

                                                    }else{
                                                        $mystring = $feature->StandardFields->City;
                                                        $StateOrProvince = $feature->StandardFields->StateOrProvince;
                                                        $findme   = '*';
                                                        $pos = strpos($mystring, $findme);
                                                        $pCode = strpos($StateOrProvince, $findme);

                                                        if($pos === false && $pCode == false)
                                                            echo $feature->StandardFields->City;
                                                        else
                                                            echo $feature->StandardFields->PostalCity . ", " . $feature->StandardFields->StateOrProvince;
                                                    }
                                                    ?>

                                                    </p>
                                                </div>
                                           </div>
                                    <?php } ?>

                                    <?php endif ?>
                                    
                              <?php }?>


                        </div>
                    </div>
                </div>
            </div>
        </section>

 <?php } ?>
