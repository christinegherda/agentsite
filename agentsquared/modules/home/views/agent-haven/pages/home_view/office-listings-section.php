            
   
    <!-- OFFICE LISTINGS SECTION -->
    <?php 

        $count = 0;

        if($iCount == 0){
            $counter = 4;
        }elseif($iCount == 1){
            $counter =  3;
        }elseif($iCount == 2){
            $counter =  2;
        }elseif($iCount == 3){
            $counter =  1;
        }else{
            $counter = 4;
        }
       
        foreach($office_listings as $office){

            if($count++ >= $counter) break;?>
                 <div class="col-md-3 col-sm-3  featured-list-item office-<?=$counter?>">
                    <div class="property-image <?php if($office['StandardFields']['PropertyClass'] == 'Land' || $office['StandardFields']['PropertyClass'] == 'MultiFamily'){ echo "property-image-land";} ?>">
                        <?php if(!empty($token_checker)){?>
                                <a class="listing-link"  href="<?= base_url();?>other-property-details/<?= $office['StandardFields']['ListingKey'];?>">
                        <?php  }
                            if(isset($office['Photos']['Uri300'])) { ?>
                                <img src="<?=$office['Photos']['Uri300'];?>" alt="<?= $office['StandardFields']['UnparsedFirstLineAddress'];?>" class="img-responsive" style="width:100%;">
                        <?php } else { ?>
                                <img src="<?=base_url()?>assets/images/image-not-available.jpg" alt="<?=$office['StandardFields']['UnparsedFirstLineAddress']; ?>" class="img-responsive" style="width:100%;">
                        <?php } ?>
                        </a>
                    </div>
                    <div class="property-listing-status">
                         <?php echo $office['StandardFields']['MlsStatus'];?>
                    </div>
                    <div class="property-listing-price">
                        <div class="property-listing-type">
                            <?=$office['StandardFields']['PropertyClass'];?>
                        </div>
                        $<?=number_format($office['StandardFields']['CurrentPrice']);?>
                    </div>
                    <div class="property-quick-icons">
                        <ul class="list-inline">
                           <?php if(isset($office['StandardFields']['BedsTotal']) && !empty($office['StandardFields']['BedsTotal'])){
                                    if(($office['StandardFields']['BedsTotal'] != "********")){?>
                                        <li><i class="fa fa-bed"></i> <?=$office['StandardFields']['BedsTotal']?> Bed</li>
                                   <?php } else{?>
                                        <li><i class="fa fa-bed"></i> N/A</li>
                                   <?php } ?>

                             <?php  } else {?>
                                    <li><i class="fa fa-bed"></i> N/A</li>
                            <?php }?>

                            <?php if($account_info->Mls != "MLS BCS"){ ?>
                                <?php if(isset($office['StandardFields']['BathsTotal']) && !empty($office['StandardFields']['BathsTotal'])){
                                        if(($office['StandardFields']['BathsTotal'] != "********")){?>
                                            <li><i class="icon-toilet"></i> <?=$office['StandardFields']['BathsTotal']?> Bath</li>
                                        <?php }else if(isset($office['StandardFields']['BathsFull']) && !empty($office['StandardFields']['BathsFull'])){ ?>
                                                    <li><i class="icon-toilet"></i> <?=$office['StandardFields']['BathsFull']?> Bath</li>
                                       <?php } else{?>
                                            <li><i class="icon-toilet"></i> N/A</li>
                                       <?php } ?>
                                <?php }else if(isset($office['StandardFields']['BathsFull']) && $office['StandardFields']['BathsFull'] != "********" ){ ?>
                                        <li><i class="icon-toilet"></i> <?=$office['StandardFields']['BathsFull']?> Bath</li>

                                 <?php  } else {?>
                                       <li><i class="icon-toilet"></i> N/A</li>
                                <?php }?>
                            <?php }else{
                                if(isset($office['StandardFields']['BathsFull']) && !empty($office['StandardFields']['BathsFull'])){
                                        if(($office['StandardFields']['BathsFull'] != "********")){?>
                                            <li><i class="icon-toilet"></i> <?=$office['StandardFields']['BathsFull']?> BathsFull <?=$office['StandardFields']['BathsHalf'];?> BathsHalf</li>
                                       <?php } else{?>
                                            <!-- <li><i class="icon-toilet"></i> N/A</li> -->
                                       <?php } ?>

                                 <?php  } else {?>
                                      <!--  <li><i class="icon-toilet"></i> N/A</li> -->
                                <?php }?>
                            <?php }?>

                           <?php
                            if(!empty($office['StandardFields']['BuildingAreaTotal']) && ($office['StandardFields']['BuildingAreaTotal'] != "0")   && is_numeric($office['StandardFields']['BuildingAreaTotal'])) {?>

                                    <li class="lot-item"><?=number_format($office['StandardFields']['BuildingAreaTotal'])?> sqft </li>

                        <?php } elseif(!empty($office['StandardFields']['LotSizeArea']) && ($office['StandardFields']['LotSizeArea'] != "0")   && is_numeric($office['StandardFields']['LotSizeArea'])) {

                               if(!empty($office['StandardFields']['LotSizeUnits']) && ($office['StandardFields']['LotSizeUnits']) === "Acres"){?>

                                    <li class="lot-item"><?=number_format($office['StandardFields']['LotSizeArea'], 2, '.', ',' )?> acres</li>

                                <?php } else {?>

                                    <li class="lot-item"><?=number_format($office['StandardFields']['LotSizeArea'])?> acres</li>

                                <?php }?>

                        <?php } elseif(!empty($office['StandardFields']['LotSizeSquareFeet']) && ($office['StandardFields']['LotSizeSquareFeet'] != "0")   && is_numeric($office['StandardFields']['LotSizeSquareFeet'])) {?>

                                <li class="lot-item"><?=number_format($office['StandardFields']['LotSizeSquareFeet'])?> sqft</li>

                         <?php } elseif(!empty($office['StandardFields']['LotSizeAcres']) && ($office['StandardFields']['LotSizeAcres'] != "0")   && is_numeric($office['StandardFields']['LotSizeAcres'])) {?>

                                <li class="lot-item"><?=number_format($office['StandardFields']['LotSizeAcres'],2 ,'.',',')?> acres</li>

                         <?php } elseif(!empty($office['StandardFields']['LotSizeDimensions']) && ($office['StandardFields']['LotSizeDimensions'] != "0")   && ($office['StandardFields']['LotSizeDimensions'] != "********")) {?>

                                <li class="lot-item"><?=$office['StandardFields']['LotSizeDimensions']?></li>
                        <?php } else {?>
                                <li class="lot-item">N/A</li>
                        <?php } ?>
                        </ul>
                    </div>
                    <div class="property-listing-description">
                        <p>
                            <?php if(!empty($token_checker)) { ?>
                                <a class="listing-link"  href="<?= base_url();?>other-property-details/<?= $office['StandardFields']['ListingKey'];?>">
                                    <b><?php echo $office['StandardFields']['UnparsedFirstLineAddress']; ?></b>
                                </a>
                            <?php } ?>
                        </p>
                        <p>
                       <?php
                            if($account_info->Mls != "MLS BCS"){
                                $mystring = $office['StandardFields']['City'];
                                $findme   = '*';
                                $pos = strpos($mystring, $findme);

                                if($pos === false)
                                    echo $office['StandardFields']['City'] . ", " . $office['StandardFields']['StateOrProvince'] . " " . $office['StandardFields']['PostalCode'];
                                else
                                    echo $office['StandardFields']['PostalCity'] . ", " . $office['StandardFields']['StateOrProvince'] . " " . $office['StandardFields']['PostalCode'];
                            }else{
                                $mystring = $office['StandardFields']['City'];
                                $StateOrProvince = $office['StandardFields']['StateOrProvince'];
                                $findme   = '*';
                                $pos = strpos($mystring, $findme);
                                $pCode = strpos($StateOrProvince, $findme);

                                if($pos === false && $pCode == false)
                                    echo $office['StandardFields']['City'];
                                else
                                    echo  $office['StandardFields']['PostalCity'] . ", " . $office['StandardFields']['StateOrProvince'];
                            }
                        ?>
                        </p>
                    </div>
            </div>  
        <?php }
        $count++;?>

   