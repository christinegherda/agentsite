
    <div class="col-md-4 col-sm-12 mobilepadding-0">
        <div class="mortage-calculator">
            <h3 class="text-center">CONTACT AGENT</h3>

            <div class="question-mess" ></div>  <br/>
            <form action="<?php echo site_url('home/home/customer_questions'); ?>" method="POST" class="customer_questions" id="customer_questions" >
                <div class="form-group">
                    <input type="text" name="first_name" value="<?= (isset($profile->first_name)) ? $profile->first_name : "" ; ?>" class="form-control" placeholder="First Name" maxlength="50" pattern="[a-zA-Z0-9-_.\s]+" title="Allowed characters: A-Z, 0-9, ., -, _" required>
                </div>
                <div class="form-group">
                    <input type="text" name="last_name" value="<?= (isset($profile->last_name)) ? $profile->last_name : "" ; ?>" class="form-control" placeholder="Last Name" maxlength="50" pattern="[a-zA-Z0-9-_.\s]+" title="Allowed characters: A-Z, 0-9, ., -, _" required>
                </div>
                <div class="form-group">
                    <input type="email" name="email" value="<?= (isset($profile->email)) ? $profile->email : "" ; ?>" class="form-control" placeholder="Email" maxlength="50" required <?=(isset($profile->email)) ? "readonly" : "";?>>
                </div>
                 <div class="form-group">
                    <input type="phone" name="phone" value="<?= (isset($profile->phone)) ? $profile->phone : "" ; ?>" class="form-control phone_number" placeholder="Phone Number" required <?= (isset($profile->phone)) ? "readonly" : "" ; ?>>
                </div>
                <div class="form-group">
                    <textarea name="message" id="" value="" cols="30" rows="10" class="form-control" placeholder="Comments, Questions, Special Requests?" required></textarea>
                </div>
                <div class="form-group">
                    <div class="g-recaptcha" data-sitekey="6Lf9eRsUAAAAAB5lP4Em8vm4L-c1m0gSyAKOTort"></div>
                </div>
                <button type="submit" class="btn btn-default btn-block submit-button submit-question-button" style="text-transform:uppercase;">Submit</button>
            </form>
        </div>
    </div>
