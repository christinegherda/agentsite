    <?php if(isset($new_listings) && !empty($new_listings)) {?>
        <section class=" <?=(isset($is_new_featured)) ? "featured-listing-area" : "new-listing-area"?>">
            <div class="container">
                <div class="row">
                    <div class="featured-title-container">

                        <?php if($is_new_featured){?>
                            <div class="trapezoid"></div>
                        <?php }?>

                        <h2 class="<?=(isset($is_new_featured)) ? "section-title" : ""?> other-listing-title text-center"><span><?php echo $new_title['option_title'];?></span></h2>

                         <?php if(isset($new_listings) && !empty($new_listings)){
                                if(count($new_listings) == 4){?>
                                    <p class="viewall">
                                        <a href="<?php base_url()?>new_listings" target="_blank">View All</a> 
                                    </p>
                        <?php  }
                                   
                            }?>
                    </div>
                 <?php 
                    $totalNL = count($new_listings);

                    if ($totalNL == 3) {
                        $container_class = "col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1";
                        $col_class = "col-md-4 col-sm-4";
                    } elseif ($totalNL == 2) {
                        $container_class = "col-md-12 col-sm-12";
                        $col_class = "col-md-6 col-sm-6 listing-landscape";
                    }elseif ($totalNL == 1) {
                        $container_class = "col-md-8 col-md-offset-2 col-sm-12";
                        $col_class = "col-md-12 col-sm-12 listing-single-lanscape";
                    } else {
                        $container_class = "col-md-12 col-sm-12";
                        $col_class = "col-md-3 col-sm-3";
                    }
                ?>
                    <div class="<?php echo $container_class; ?>">
                        <div class="featured-list">
                        <?php if ($totalNL == 1 || $totalNL == 2): ?>

                            <?php foreach ($new_listings as $new_listing): ?>
                                <!-- <div class="row"> -->
                                  <div class="<?=$col_class; ?>">
                                    <div class="property-list-view">
                                      <div class="col-md-5 col-sm-5">
                                          <div class="property-status">
                                              <?=$new_listing->StandardFields->PropertyClass;?>
                                          </div>
                                          <?php
                                            $url_rewrite = url_title("{$new_listing->StandardFields->UnparsedFirstLineAddress} {$new_listing->StandardFields->PostalCode}");
                                            ?>
                                            <?php if(!empty($token_checker)) { ?>
                                                    <a href="<?= base_url();?>property-details/<?php echo $url_rewrite ?>/<?= $new_listing->StandardFields->ListingKey; ?>" class="image">
                                            <?php }
                                                if(isset($new_listing->Photos->Uri300)) { ?>
                                                    <img src="<?=$new_listing->Photos->Uri300?>" alt="<?php echo $new_listing->StandardFields->UnparsedFirstLineAddress; ?>" class="img-responsive" style="width:100%; ">
                                            <?php }
                                                else if(isset($new_listing->Photos->Uri300 )) { ?>
                                                    <img src="<?=$new_listing->Photos->Uri300; ?>" alt="<?php echo $new_listing->StandardFields->UnparsedFirstLineAddress; ?>" class="img-responsive" style="width:100%;">
                                            <?php }
                                                else { ?>
                                                    <img src="<?=base_url()?>assets/images/image-not-available.jpg" alt="<?php echo $new_listing->StandardFields->UnparsedFirstLineAddress; ?>" class="img-responsive" style="width:100%;">
                                            <?php } ?>
                                                </a>
                                      </div>
                                      <div class="col-md-7 col-sm-7">
                                          <div class="description">
                                            <h3><?php
                                                $url_rewrite = url_title("{$new_listing->StandardFields->UnparsedFirstLineAddress} {$new_listing->StandardFields->PostalCode}");
                                                ?>
                                             <?php if(!empty($token_checker)) { ?>
                                                <a class="listing-link" href="<?= base_url();?>property-details/<?php echo $url_rewrite ?>/<?=$new_listing->StandardFields->ListingKey;?>">
                                                    <b><?php echo $new_listing->StandardFields->UnparsedFirstLineAddress; ?></b>
                                                </a>
                                            <?php } ?>

                                            </h3>
                                            <p>
                                               <?php
                                                if($account_info->Mls != "MLS BCS"){
                                                    $mystring = $new_listing->StandardFields->City;
                                                    $postalcode = $new_listing->StandardFields->PostalCode;
                                                    $findme   = '*';
                                                    $pos = strpos($mystring, $findme);
                                                    $pos_code = strpos($postalcode, $findme);


                                                    if($pos === false){
                                                        if($pos_code === false){
                                                            echo $new_listing->StandardFields->City . ", " . $new_listing->StandardFields->StateOrProvince . " " . $new_listing->StandardFields->PostalCode;
                                                        }else{
                                                            echo $new_listing->StandardFields->City . ", " . $new_listing->StandardFields->StateOrProvince;
                                                        }

                                                    }
                                                    else{
                                                        if($pos_code === false){
                                                            echo $new_listing->StandardFields->PostalCity . ", " . $new_listing->StandardFields->StateOrProvince . " " . $new_listing->StandardFields->PostalCode;
                                                        }else{
                                                           echo $new_listing->StandardFields->PostalCity . ", " . $new_listing->StandardFields->StateOrProvince;
                                                        }

                                                    }

                                                }else{
                                                    $mystring = $new_listing->StandardFields->City;
                                                    $StateOrProvince = $new_listing->StandardFields->StateOrProvince;
                                                    $findme   = '*';
                                                    $pos = strpos($mystring, $findme);
                                                    $pCode = strpos($StateOrProvince, $findme);

                                                    if($pos === false && $pCode == false)
                                                        echo $new_listing->StandardFields->City;
                                                    else
                                                        echo $new_listing->StandardFields->PostalCity . ", " . $new_listing->StandardFields->StateOrProvince;
                                                }
                                                ?>   

                                            </p>
                                          </div>
                                          <div class="price">
                                              $ <?=number_format($new_listing->StandardFields->CurrentPrice);?>
                                          </div>
                                          <ul class="prop-details">
                                            <?php if(isset($new_listing->StandardFields->BedsTotal) && !empty($new_listing->StandardFields->BedsTotal)){
                                                    if(($new_listing->StandardFields->BedsTotal != "********")){?>
                                                        <li><i class="fa fa-bed"></i> <?=$new_listing->StandardFields->BedsTotal?> Bed</li>
                                                   <?php } else{?>
                                                        <li><i class="fa fa-bed"></i> N/A</li>
                                                   <?php } ?>

                                             <?php  } else {?>
                                                    <li><i class="fa fa-bed"></i> N/A</li>
                                            <?php }?>
                                            <?php if($account_info->Mls != "MLS BCS"){ ?>
                                                <?php if(isset($new_listing->StandardFields->BathsTotal) && !empty($new_listing->StandardFields->BathsTotal)){
                                                        if(($new_listing->StandardFields->BathsTotal != "********")){?>
                                                            <li><i class="icon-toilet"></i> <?=$new_listing->StandardFields->BathsTotal?> Bath</li>

                                                        <?php }else if(isset($new_listing->StandardFields->BathsFull) && $new_listing->StandardFields->BathsFull != "********"){ ?>
                                                        <li><i class="icon-toilet"></i> <?=$new_listing->StandardFields->BathsFull?> Bath</li>

                                                       <?php } else{?>
                                                            <li><i class="icon-toilet"></i> N/A</li>
                                                       <?php } ?>
                                                       
                                                <?php }else if(isset($new_listing->StandardFields->BathsFull) && $new_listing->StandardFields->BathsFull != "********"){ ?>
                                                        <li><i class="icon-toilet"></i> <?=$new_listing->StandardFields->BathsFull?> Bath</li>

                                                 <?php  } else {?>
                                                       <li><i class="icon-toilet"></i> N/A</li>
                                                <?php }?>
                                            <?php }else{
                                                if(isset($new_listing->StandardFields->BathsFull) && !empty($new_listing->StandardFields->BathsFull)){
                                                        if(($new_listing->StandardFields->BathsFull != "********")){?>
                                                            <li><i class="icon-toilet"></i> <?=$new_listing->StandardFields->BathsFull?> BathsFull <?=$new_listing->StandardFields->BathsHalf?> BathsHalf</li>
                                                       <?php } else{?>
                                                            <!-- <li><i class="icon-toilet"></i> N/A</li> -->
                                                       <?php } ?>

                                                 <?php  } else {?>
                                                      <!--  <li><i class="icon-toilet"></i> N/A</li> -->
                                                <?php }?>
                                            <?php }?>
                                             <?php
                                                if(!empty($new_listing->StandardFields->BuildingAreaTotal) && ($new_listing->StandardFields->BuildingAreaTotal != "0")   && is_numeric($new_listing->StandardFields->BuildingAreaTotal)) {?>

                                                    <li class="lot-item"><?=number_format($new_listing->StandardFields->BuildingAreaTotal)?> sqft</li>

                                            <?php } elseif(!empty($new_listing->StandardFields->LotSizeArea) && ($new_listing->StandardFields->LotSizeArea != "0")   && is_numeric($new_listing->StandardFields->LotSizeArea)) {


                                                    if(!empty($new_listing->StandardFields->LotSizeUnits)  && ($new_listing->StandardFields->LotSizeUnits === "Acres") ){?>

                                                        <li class="lot-item"><?=number_format($new_listing->StandardFields->LotSizeArea, 2, '.', ',')?> acres</li>

                                                    <?php } else { ?>

                                                        <li class="lot-item"><?=number_format($new_listing->StandardFields->LotSizeArea)?> acres</li>

                                                    <?php } ?>

                                              <?php } elseif(!empty($new_listing->StandardFields->LotSizeSquareFeet) && ($new_listing->StandardFields->LotSizeSquareFeet != "0")   && is_numeric($new_listing->StandardFields->LotSizeSquareFeet)) {?>

                                                    <li class="lot-item"><?=number_format($new_listing->StandardFields->LotSizeSquareFeet)?> 
                                                    sqft</li>

                                             <?php } elseif(!empty($new_listing->StandardFields->LotSizeAcres) && ($new_listing->StandardFields->LotSizeAcres != "0")   && is_numeric($new_listing->StandardFields->LotSizeAcres)) {?>

                                                    <li class="lot-item"><?=number_format($new_listing->StandardFields->LotSizeAcres,2 ,'.',',')?> acres</li>

                                            <?php } elseif(!empty($new_listing->StandardFields->LotSizeDimensions) && ($new_listing->StandardFields->LotSizeDimensions != "0")   && ($new_listing->StandardFields->LotSizeDimensions != "********")) {?>

                                                    <li class="lot-item"><?=$new_listing->StandardFields->LotSizeDimensions?></li>
                                            <?php } else {?>
                                                    <li class="lot-item">N/A</li>
                                            <?php } ?>
                                          </ul>
                                      </div>
                                    </div>
                                  </div>
                                <!-- </div> -->
                            <?php endforeach ?>
                        <?php else: ?>

                          <?php

                                $count = 0;
                               foreach($new_listings as $new_listing){

    
                                    if($count < 4) { ?>
                                <div class="<?php echo $col_class; ?> featured-list-item">
                                    <div class="property-image <?php if($new_listing->StandardFields->PropertyClass == 'Land' || $new_listing->StandardFields->PropertyClass == 'MultiFamily'){ echo "property-image-land";} ?>">
                                        <?php 
                                            if(!empty($token_checker)){?>
                                                <a href="<?= base_url();?>other-property-details/<?=$new_listing->StandardFields->ListingKey;?>">
                                        <?php }
                                            if(isset($new_listing->Photos->Uri300)) { ?>
                                                <img src="<?=$new_listing->Photos->Uri300?>" alt="<?php echo $new_listing->StandardFields->UnparsedFirstLineAddress; ?>" class="img-responsive" style="width:100%;">
                                        <?php }
                                            else { ?>
                                                <img src="<?=base_url()?>assets/images/image-not-available.jpg" alt="<?php echo $new_listing->StandardFields->UnparsedFirstLineAddress; ?>" class="img-responsive" style="width:100%;">
                                        <?php } ?>
                                        </a>
                                    </div>
                                    <div class="property-listing-status">
                                        <?php echo $new_listing->StandardFields->MlsStatus;?>
                                    </div>

                                     <?php if(!empty($token_checker)){?>
                                         <div class="<?=isset($new_listing->StandardFields->OnMarketDate) ? "property-listing-date" : "" ?>">
                                            <?php if(isset($new_listing->StandardFields->OnMarketDate) && $new_listing->StandardFields->OnMarketDate!="********"){
                                                $onmarketdate = $new_listing->StandardFields->OnMarketDate;
                                                $today = time();
                                                $onmarketdate= strtotime($onmarketdate);
                                                $hours =  round(abs($today-$onmarketdate)/60/60);
                                            ?>
                                                Listed <?=($hours == 0) ? 1 : $hours ?> <?=($hours == 1 || $hours == 0) ? "hour" :"hours" ?> ago!
                                            <?php }?>
                                        </div>
                                    <?php }?>
                                    
                                    <div class="property-listing-price">
                                        <div class="property-listing-type">
                                            <?=$new_listing->StandardFields->PropertyClass;?>
                                        </div>
                                        $<?=number_format($new_listing->StandardFields->CurrentPrice);?>
                                    </div>
                                    <div class="property-quick-icons">
                                        <ul class="list-inline">

                                        <?php if(isset($new_listing->StandardFields->BedsTotal) && !empty($new_listing->StandardFields->BedsTotal)){
                                                if(($new_listing->StandardFields->BedsTotal != "********")){?>
                                                    <li><i class="fa fa-bed"></i> <?=$new_listing->StandardFields->BedsTotal?> Bed</li>
                                               <?php } else{?>
                                                    <li><i class="fa fa-bed"></i> N/A</li>
                                               <?php } ?>

                                         <?php  } else {?>
                                                <li><i class="fa fa-bed"></i> N/A</li>
                                        <?php }?>

                                        <?php if($account_info->Mls != "MLS BCS"){ ?>
                                            <?php if(isset($new_listing->StandardFields->BathsTotal) && !empty($new_listing->StandardFields->BathsTotal)){
                                                    if(($new_listing->StandardFields->BathsTotal != "********")){?>
                                                        <li><i class="icon-toilet"></i> <?=$new_listing->StandardFields->BathsTotal?> Bath</li>
                                                    <?php }else if(isset($new_listing->StandardFields->BathsFull) && $new_listing->StandardFields->BathsFull != "********"){ ?>
                                                        <li><i class="icon-toilet"></i> <?=$new_listing->StandardFields->BathsFull?> Bath</li>
                                                   <?php } else{?>
                                                        <li><i class="icon-toilet"></i> N/A</li>
                                                   <?php } ?>
                                             <?php }else if(isset($new_listing->StandardFields->BathsFull) && $new_listing->StandardFields->BathsFull != "********" ){ ?>
                                                    <li><i class="icon-toilet"></i> <?=$new_listing->StandardFields->BathsFull?> Bath</li>

                                             <?php  } else {?>
                                                   <li><i class="icon-toilet"></i> N/A</li>
                                            <?php }?>
                                        <?php }else{ 
                                            if(isset($new_listing->StandardFields->BathsFull) && !empty($new_listing->StandardFields->BathsFull)){
                                                    if(($new_listing->StandardFields->BathsFull != "********")){?>
                                                        <li><i class="icon-toilet"></i> <?=$new_listing->StandardFields->BathsFull?> Full Baths <?=$new_listing->StandardFields->BathsHalf?> Half Baths</li>
                                                   <?php } else{?>
                                                        <!-- <li><i class="icon-toilet"></i> N/A</li> -->
                                                   <?php } ?>

                                             <?php  } else {?>
                                                  <!--  <li><i class="icon-toilet"></i> N/A</li> -->
                                            <?php }?>
                                        <?php }?>
                                        
                                        <?php
                                        if(!empty($new_listing->StandardFields->BuildingAreaTotal) && ($new_listing->StandardFields->BuildingAreaTotal != "0")   && is_numeric($new_listing->StandardFields->BuildingAreaTotal)) {?>

                                            <li class="lot-item"><?=number_format($new_listing->StandardFields->BuildingAreaTotal)?> sqft </li>

                                        <?php } elseif(!empty($new_listing->StandardFields->LotSizeArea) && ($new_listing->StandardFields->LotSizeArea != "0")   && is_numeric($new_listing->StandardFields->LotSizeArea)) {
                                               
                                            if(!empty($new_listing->StandardFields->LotSizeUnits) && ($new_listing->StandardFields->LotSizeUnits) === "Acres"){?>

                                                <li class="lot-item"><?=number_format($new_listing->StandardFields->LotSizeArea, 2, '.', ',' )?> acres</li>

                                            <?php } else {?>

                                                <li class="lot-item"><?=number_format($new_listing->StandardFields->LotSizeArea)?> acres</li>

                                            <?php }?>

                                        <?php } elseif(!empty($new_listing->StandardFields->LotSizeSquareFeet) && ($new_listing->StandardFields->LotSizeSquareFeet != "0")   && is_numeric($new_listing->StandardFields->LotSizeSquareFeet)) {?>

                                                <li class="lot-item"><?=number_format($new_listing->StandardFields->LotSizeSquareFeet)?> sqft</li> 

                                         <?php } elseif(!empty($new_listing->StandardFields->LotSizeAcres) && ($new_listing->StandardFields->LotSizeAcres != "0")   && is_numeric($new_listing->StandardFields->LotSizeAcres)) {?>

                                                <li class="lot-item"><?=number_format($new_listing->StandardFields->LotSizeAcres,2 ,'.',',')?> acres</li>
                                                
                                         <?php } elseif(!empty($new_listing->StandardFields->LotSizeDimensions) && ($new_listing->StandardFields->LotSizeDimensions != "0")   && ($new_listing->StandardFields->LotSizeDimensions != "********")) {?>

                                                        <li class="lot-item"><?=$new_listing->StandardFields->LotSizeDimensions?></li>
                                        <?php } else {?>
                                                        <li class="lot-item">N/A</li>
                                        <?php } ?>
                                        </ul>
                                    </div>
                                    <div class="property-listing-description">
                                        <p>
                                            <?php if(!empty($token_checker)){?>
                                                <a class="listing-link" href="<?= base_url();?>other-property-details/<?= $new_listing->StandardFields->ListingKey; ?>">
                                            <?php }?>
                                                <b><?php echo $new_listing->StandardFields->UnparsedFirstLineAddress; ?></b>
                                            </a>
                                        </p>
                                        <p>
                                          <?php
                                            if($account_info->Mls != "MLS BCS"){
                                                $mystring = $new_listing->StandardFields->City;
                                                $postalcode = $new_listing->StandardFields->PostalCode;
                                                $findme   = '*';
                                                $pos = strpos($mystring, $findme);
                                                $pos_code = strpos($postalcode, $findme);

                                                if($pos === false){
                                                    if($pos_code === false){
                                                        echo $new_listing->StandardFields->City . ", " . $new_listing->StandardFields->StateOrProvince . " " . $new_listing->StandardFields->PostalCode;
                                                    }else{
                                                        echo $new_listing->StandardFields->City . ", " . $new_listing->StandardFields->StateOrProvince;
                                                    }
                                                    
                                                }
                                                else{
                                                    if($pos_code === false){
                                                        echo $new_listing->StandardFields->PostalCity . ", " . $new_listing->StandardFields->StateOrProvince . " " . $new_listing->StandardFields->PostalCode;
                                                    }else{
                                                       echo $new_listing->StandardFields->PostalCity . ", " . $new_listing->StandardFields->StateOrProvince;     
                                                    }
                                                    
                                                }

                                            }else{
                                                $mystring = $new_listing->StandardFields->City;
                                                $StateOrProvince = $new_listing->StandardFields->StateOrProvince;
                                                $findme   = '*';
                                                $pos = strpos($mystring, $findme);
                                                $pCode = strpos($StateOrProvince, $findme);

                                                if($pos === false && $pCode == false)
                                                    echo $new_listing->StandardFields->City;
                                                else
                                                    echo $new_listing->StandardFields->PostalCity . ", " . $new_listing->StandardFields->StateOrProvince;
                                            }
                                            ?>
                                        </p>

                                    </div>
                                </div>

                            <?php  } $count++;

                                 } 
                            ?> 
                        <?php endif ?>
                        </div>

                    </div>
                </div>
            </div>
        </section>
    <?php } ?>
