<?php if($sold_listings_option->show_sold_listings){
        if (isset($sold_listings)){?>
            <div class="col-md-12 properties-sold">
                <h4> <?= isset($sold_listings_title["option_title"]) ? $sold_listings_title["option_title"] :"Recent Properties Sold"?></h4>
                <div class="sold-property-container" style="max-height: 220px; overflow: hidden;">
                    <?php
                         $total_sold_count =  !empty($total_sold_listings) ? $total_sold_listings :"";
                         $total_sold = ($total_sold_count < 8) ? $total_sold_count : 8;
                     ?>
                    <?php for($i = 0; $i < $total_sold; $i++) { ?>
                        <div class="other-listing-item last-item-sold col-md-3 cd-control" >
                            <div class="sold-banner"><img src="/assets/images/dashboard/demo/agent-haven/sold.png"></div>
                            <div class="panel panel-default panel-placeholder sold-image">
                                <div class='panel-body'>
                                    <div class='col-md-12 col-xs-12 no-padding'>
                                        <div class="placeholder-img"></div>
                                    </div>
                                    <div class='col-md-12 col-xs-12 no-padding'>
                                        <p class="placeholder-details"></p>
                                        <p class="placeholder-details"></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                    </div>
                
            </div>
       <?php } ?>
<?php } ?>