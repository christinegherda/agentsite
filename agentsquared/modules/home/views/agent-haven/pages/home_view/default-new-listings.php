    <?php if (isset($new_listings) && !empty($new_listings)){?>
        <div class="col-md-6 col-sm-12">
            <div class="new-listings-area">
                <h2 class="other-listing-title"><?php echo isset($new_title['option_title']) ? $new_title['option_title'] : "New Listings" ;?>
                    <span class="viewall-newlisting">

                        <?php if(isset($new_listings) && !empty($new_listings)){
                                if(count($new_listings) == 4){?>
                                    <a href="<?php base_url()?>new_listings" target="_blank" data-original-title="" title="">View All</a> 
                        <?php  }     
                            }?>
                    </span>
                </h2>

                     <?php 

                        $count = 0;
                        
                        foreach($new_listings as $new_listing) {

                            if($count < 4) { ?>

                            <div itemscope itemtype="http://schema.org/Product" class="col-md-6 col-sm-6 featured-list-item">

                            <?php
                                if(isset($new_listing->Photos->Uri300) && !empty($new_listing->Photos->Uri300)) {
                                        $photo = $new_listing->Photos->Uri300;
                                }
                                $defaultPhoto = base_url('assets/images/image-not-available.jpg');
                            ?>
                            <div class="property-image <?php if($new_listing->StandardFields->PropertyClass == 'Land' || $new_listing->StandardFields->PropertyClass == 'MultiFamily'){ echo "property-image-land";} ?>">
                                <?php 
                                    if(!empty($token_checker)) { ?>
                                    <a itemprop="url" href="<?= base_url();?>other-property-details/<?=$new_listing->StandardFields->ListingKey;?>">
                                <?php } 
                                    if(isset($new_listing->Photos->Uri300)) { ?>
                                        <img itemprop="image" src="<?= (@getimagesize($photo)) ? $photo : $defaultPhoto ?>" alt="<?php echo $new_listing->StandardFields->UnparsedFirstLineAddress; ?>" class="img-responsive" style="width:100%;">
                                <?php
                                    } else { ?>
                                        <img itemprop="image" src="<?=base_url()?>assets/images/image-not-available.jpg" alt="<?php echo $new_listing->StandardFields->UnparsedFirstLineAddress; ?>" class="img-responsive" style="width:100%;">
                                <?php } ?>
                                    </a>
                                <div itemprop="name" class="property-listing-description">
                                    <p>
                                    <?php if(!empty($token_checker)){?>
                                        <a class="listing-link" href="<?= base_url();?>other-property-details/<?=$new_listing->StandardFields->ListingKey;?>">
                                    <?php  }?>
                                        <b><?php echo $new_listing->StandardFields->UnparsedFirstLineAddress; ?></b>
                                    </a>
                                    </p>
                                    <p> 
                                        <?php

                                            if($account_info->Mls != "MLS BCS"){
                                                $mystring = $new_listing->StandardFields->City;
                                                $postalcode = $new_listing->StandardFields->PostalCode;
                                                $findme   = '*';
                                                $pos = strpos($mystring, $findme);
                                                $pos_code = strpos($postalcode, $findme);
                                                if($pos === false){
                                                    if($pos_code === false){
                                                        echo $new_listing->StandardFields->City . ", " . $new_listing->StandardFields->StateOrProvince . " " . $new_listing->StandardFields->PostalCode;
                                                    }else{
                                                        echo $new_listing->StandardFields->City . ", " . $new_listing->StandardFields->StateOrProvince;
                                                    }
                                                    
                                                }
                                                else{
                                                    if($pos_code === false){
                                                        echo $new_listing->StandardFields->PostalCity . ", " . $new_listing->StandardFields->StateOrProvince . " " . $new_listing->StandardFields->PostalCode;
                                                    }else{
                                                       echo $new_listing->StandardFields->PostalCity . ", " . $new_listing->StandardFields->StateOrProvince;     
                                                    }
                                                    
                                                }
                                            }else{
                                                $mystring = $new_listing->StandardFields->City;
                                                $StateOrProvince = $new_listing->StandardFields->StateOrProvince;
                                                $findme   = '*';
                                                $pos = strpos($mystring, $findme);
                                                $pCode = strpos($StateOrProvince, $findme);

                                                if($pos === false && $pCode == false)
                                                    echo $new_listing->StandardFields->City;
                                                else
                                                    echo $new_listing->StandardFields->PostalCity . ", " . $new_listing->StandardFields->StateOrProvince;
                                            }

                                        ?>
                                    </p>
                                </div>
                            </div>
                            <div class="property-listing-status">
                                <?php echo $new_listing->StandardFields->MlsStatus;?>                               
                            </div>

                            <?php if(!empty($token_checker)){?>
                                 <div class="<?=isset($new_listing->StandardFields->OnMarketDate) ? "property-listing-date" : "" ?>">
                                    <?php if(isset($new_listing->StandardFields->OnMarketDate) && $new_listing->StandardFields->OnMarketDate!="********"){
                                        $onmarketdate = $new_listing->StandardFields->OnMarketDate;
                                        $today = time();
                                        $onmarketdate= strtotime($onmarketdate);
                                        $hours =  round(abs($today-$onmarketdate)/60/60);
                                    ?>
                                        Listed <?=($hours == 0) ? 1 : $hours ?> <?=($hours == 1 || $hours == 0) ? "hour" :"hours" ?> ago!
                                    <?php }?>
                                </div>
                            <?php }?>
                            
                            <div itemprop="offers" itemscope itemtype="http://schema.org/Offer" class="property-listing-price">
                                <div class="property-listing-type">
                                    <?=$new_listing->StandardFields->PropertyClass;?>
                                </div>
                                <span itemprop="priceCurrency" content="USD">$</span><span itemprop="price" content="<?=str_replace(',','',number_format($new_listing->StandardFields->CurrentPrice));?>"><?=number_format($new_listing->StandardFields->CurrentPrice);?></span>
                            </div>
                            <div class="property-quick-icons">
                                <ul class="list-inline">
                                    <?php if(isset($new_listing->StandardFields->BedsTotal) && !empty($new_listing->StandardFields->BedsTotal)){
                                            if(($new_listing->StandardFields->BedsTotal != "********")){?>
                                                <li><i class="fa fa-bed"></i> <?=$new_listing->StandardFields->BedsTotal?> Bed</li>
                                           <?php } else{?>
                                                <li><i class="fa fa-bed"></i> N/A</li>
                                           <?php } ?>

                                     <?php  } else {?>
                                            <li><i class="fa fa-bed"></i> N/A</li>
                                    <?php }?>
                                    <?php
                                        if($account_info->Mls != "MLS BCS"){
                                             if(isset($new_listing->StandardFields->BathsTotal) && !empty($new_listing->StandardFields->BathsTotal)){
                                                if(($new_listing->StandardFields->BathsTotal != "********")){?>
                                                    <li><i class="icon-toilet"></i> <?=$new_listing->StandardFields->BathsTotal?> Bath</li>
                                                <?php }else if(isset($new_listing->StandardFields->BathsFull) && $new_listing->StandardFields->BathsFull != "********"){ ?>
                                                    <li><i class="icon-toilet"></i> <?=$new_listing->StandardFields->BathsFull?> Bath</li>
                                               <?php } else{?>
                                                    <li><i class="icon-toilet"></i> N/A</li>
                                               <?php } ?>
                                               
                                         <?php }else if(isset($new_listing->StandardFields->BathsFull) && $new_listing->StandardFields->BathsFull != "********"){ ?>
                                                    <li><i class="icon-toilet"></i> <?=$new_listing->StandardFields->BathsFull?> Bath</li>

                                         <?php  } else {?>
                                               <li><i class="icon-toilet"></i> N/A</li>
                                        <?php }?>

                                    <?php }else{ 
                                        if(isset($new_listing->StandardFields->BathsFull) && !empty($new_listing->StandardFields->BathsFull)){
                                                if(($new_listing->StandardFields->BathsFull != "********")){?>
                                                    <li><i class="icon-toilet"></i> <?=$new_listing->StandardFields->BathsFull?> Full Baths <?=$new_listing->StandardFields->BathsHalf?> Half Baths</li>
                                               <?php } else{?>
                                                    <!-- <li><i class="icon-toilet"></i> N/A</li> -->
                                               <?php } ?>

                                         <?php  } else {?>
                                              <!--  <li><i class="icon-toilet"></i> N/A</li> -->
                                        <?php }?>
                                    <?php }?>

                                   <?php
                                    if(!empty($new_listing->StandardFields->BuildingAreaTotal) && ($new_listing->StandardFields->BuildingAreaTotal != "0")   && is_numeric($new_listing->StandardFields->BuildingAreaTotal)) {?>

                                        <li class="lot-item"><?=number_format($new_listing->StandardFields->BuildingAreaTotal)?> sqft </li>

                                    <?php } elseif(!empty($new_listing->StandardFields->LotSizeArea) && ($new_listing->StandardFields->LotSizeArea != "0")   && is_numeric($new_listing->StandardFields->LotSizeArea)) {
                                           
                                        if(!empty($new_listing->StandardFields->LotSizeUnits) && ($new_listing->StandardFields->LotSizeUnits) === "Acres"){?>

                                            <li class="lot-item"><?=number_format($new_listing->StandardFields->LotSizeArea, 2, '.', ',' )?> acres</li>

                                        <?php } else {?>

                                            <li class="lot-item"><?=number_format($new_listing->StandardFields->LotSizeArea)?> acres</li>

                                        <?php }?>

                                    <?php } elseif(!empty($new_listing->StandardFields->LotSizeSquareFeet) && ($new_listing->StandardFields->LotSizeSquareFeet != "0")   && is_numeric($new_listing->StandardFields->LotSizeSquareFeet)) {?>

                                            <li class="lot-item"><?=number_format($new_listing->StandardFields->LotSizeSquareFeet)?> sqft</li> 

                                     <?php } elseif(!empty($new_listing->StandardFields->LotSizeAcres) && ($new_listing->StandardFields->LotSizeAcres != "0")   && is_numeric($new_listing->StandardFields->LotSizeAcres)) {?>

                                            <li class="lot-item"><?=number_format($new_listing->StandardFields->LotSizeAcres,2 ,'.',',')?> acres</li>
                                            
                                     <?php } elseif(!empty($new_listing->StandardFields->LotSizeDimensions) && ($new_listing->StandardFields->LotSizeDimensions != "0")   && ($new_listing->StandardFields->LotSizeDimensions != "********")) {?>

                                                    <li class="lot-item"><?=$new_listing->StandardFields->LotSizeDimensions?></li>
                                    <?php } else {?>
                                                    <li class="lot-item">N/A</li>
                                    <?php } ?>
                                </ul>
                            </div>
                        </div>
                             <?php  } $count++;

                             } 
                        ?> 
            </div>
        </div>
    <?php } ?>
