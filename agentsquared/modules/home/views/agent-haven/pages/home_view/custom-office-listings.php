
    <?php if(isset($office_listings) && !empty($office_listings)) {?>

        <section class="<?=(isset($is_office_featured)) ? "featured-listing-area" : "new-listing-area"?>">
            <div class="container">
                <div class="row">
                    <div class="featured-title-container">

                        <?php if($is_office_featured){?>
                            <div class="trapezoid"></div>
                        <?php }?>

                        <h2 class="<?=(isset($is_office_featured)) ? "section-title" : ""?> other-listing-title text-center"><?php echo $office_title['option_title'];?></h2>
                    </div>

                 <?php 
                    $totalOL = count($office_listings);

                    if ($totalOL == 3) {
                        $container_class = "col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1";
                        $col_class = "col-md-4 col-sm-4";
                    } elseif ($totalOL == 2) {
                        $container_class = "col-md-12 col-sm-12";
                        $col_class = "col-md-6 col-sm-6 listing-landscape";
                    }elseif ($totalOL == 1) {
                        $container_class = "col-md-8 col-md-offset-2 col-sm-12";
                        $col_class = "col-md-12 col-sm-12 listing-single-lanscape";
                    } else {
                        $container_class = "col-md-12 col-sm-12";
                        $col_class = "col-md-3 col-sm-3";
                    }
                ?>
                    <div class="<?php echo $container_class; ?>">
                        <div class="featured-list">
                          <?php

                                $countOffice = count($office_listings);

                                if($countOffice >= 4){?>

                                     <p class="featured-viewall"><a href="<?php echo base_url()?>office_listings" target="_blank">View All</a></p>

                                <?php }?>
                        
                                <!-- list view -->
                                <?php if ($totalOL == 1 || $totalOL == 2): ?> 

                                    <?php foreach ($office_listings as $office): ?>
                                        <!-- <div class="row"> -->
                                          <div class="<?=$col_class; ?>">
                                            <div class="property-list-view">
                                              <div class="col-md-5 col-sm-5">
                                                  <div class="property-status">
                                                      <?=$office['StandardFields']['PropertyClass'];?>
                                                  </div>
                                                  <?php
                                                    $url_rewrite = url_title("{$office['StandardFields']['UnparsedFirstLineAddress']} {$office['StandardFields']['PostalCode']}");
                                                    ?>
                                                    <?php if(!empty($token_checker)) { ?>
                                                            <a href="<?= base_url();?>property-details/<?php echo $url_rewrite ?>/<?= $office['StandardFields']['ListingKey']; ?>" class="image">
                                                    <?php }
                                                        if(isset($office['Photos']['Uri300'])) { ?>
                                                            <img src="<?=$office['Photos']['Uri300']?>" alt="<?php echo $office['StandardFields']['UnparsedFirstLineAddress']; ?>" class="img-responsive" style="width:100%; ">
                                                    <?php }
                                                        else if(isset($office['Photos']['Uri300'] )) { ?>
                                                            <img src="<?=$office['Photos']['Uri300']; ?>" alt="<?php echo $office['StandardFields']['UnparsedFirstLineAddress']; ?>" class="img-responsive" style="width:100%;">
                                                    <?php }
                                                        else { ?>
                                                            <img src="<?=base_url()?>assets/images/image-not-available.jpg" alt="<?php echo $office['StandardFields']['UnparsedFirstLineAddress']; ?>" class="img-responsive" style="width:100%;">
                                                    <?php } ?>
                                                        </a>
                                              </div>
                                              <div class="col-md-7 col-sm-7">
                                                  <div class="description">
                                                    <h3><?php
                                                        $url_rewrite = url_title("{$office['StandardFields']['UnparsedFirstLineAddress']} {$office['StandardFields']['PostalCode']}");
                                                        ?>
                                                     <?php if(!empty($token_checker)) { ?>
                                                        <a class="listing-link" href="<?= base_url();?>property-details/<?php echo $url_rewrite ?>/<?=$office['StandardFields']['ListingKey'];?>">
                                                            <b><?php echo $office['StandardFields']['UnparsedFirstLineAddress']; ?></b>
                                                        </a>
                                                    <?php } ?>

                                                    </h3>
                                                    <p>
                                                       <?php
                                                        if($account_info->Mls != "MLS BCS"){
                                                            $mystring = $office['StandardFields']['City'];
                                                            $postalcode = $office['StandardFields']['PostalCode'];
                                                            $findme   = '*';
                                                            $pos = strpos($mystring, $findme);
                                                            $pos_code = strpos($postalcode, $findme);


                                                            if($pos === false){
                                                                if($pos_code === false){
                                                                    echo $office['StandardFields']['City'] . ", " . $office['StandardFields']['StateOrProvince'] . " " . $office['StandardFields']['PostalCode'];
                                                                }else{
                                                                    echo $office['StandardFields']['City'] . ", " . $office['StandardFields']['StateOrProvince'];
                                                                }

                                                            }
                                                            else{
                                                                if($pos_code === false){
                                                                    echo $office['StandardFields']['PostalCity'] . ", " . $office['StandardFields']['StateOrProvince'] . " " . $office['StandardFields']['PostalCode'];
                                                                }else{
                                                                   echo $office['StandardFields']['PostalCity'] . ", " . $office['StandardFields']['StateOrProvince'];
                                                                }

                                                            }

                                                        }else{
                                                            $mystring = $office['StandardFields']['City'];
                                                            $StateOrProvince = $office['StandardFields']['StateOrProvince'];
                                                            $findme   = '*';
                                                            $pos = strpos($mystring, $findme);
                                                            $pCode = strpos($StateOrProvince, $findme);

                                                            if($pos === false && $pCode == false)
                                                                echo $office['StandardFields']['City'];
                                                            else
                                                                echo $office['StandardFields']['PostalCity'] . ", " . $office['StandardFields']['StateOrProvince'];
                                                        }
                                                        ?>   

                                                    </p>
                                                  </div>
                                                  <div class="price">
                                                      $ <?=number_format($office['StandardFields']['CurrentPrice']);?>
                                                  </div>
                                                  <ul class="prop-details">
                                                    <?php if(isset($office['StandardFields']['BedsTotal']) && !empty($office['StandardFields']['BedsTotal'])){
                                                            if(($office['StandardFields']['BedsTotal'] != "********")){?>
                                                                <li><i class="fa fa-bed"></i> <?=$office['StandardFields']['BedsTotal']?> Bed</li>
                                                           <?php } else{?>
                                                                <li><i class="fa fa-bed"></i> N/A</li>
                                                           <?php } ?>

                                                     <?php  } else {?>
                                                            <li><i class="fa fa-bed"></i> N/A</li>
                                                    <?php }?>
                                                    <?php if($account_info->Mls != "MLS BCS"){ ?>
                                                        <?php if(isset($office['StandardFields']['BathsTotal']) && !empty($office['StandardFields']['BathsTotal'])){
                                                                if(($office['StandardFields']['BathsTotal'] != "********")){?>
                                                                    <li><i class="icon-toilet"></i> <?=$office['StandardFields']['BathsTotal']?> Bath</li>

                                                                <?php }else if(isset($office['StandardFields']['BathsFull']) && $office['StandardFields']['BathsFull'] != "********"){ ?>
                                                                <li><i class="icon-toilet"></i> <?=$office['StandardFields']['BathsFull']?> Bath</li>

                                                               <?php } else{?>
                                                                    <li><i class="icon-toilet"></i> N/A</li>
                                                               <?php } ?>
                                                               
                                                        <?php }else if(isset($office['StandardFields']['BathsFull']) && $office['StandardFields']['BathsFull'] != "********"){ ?>
                                                                <li><i class="icon-toilet"></i> <?=$office['StandardFields']['BathsFull']?> Bath</li>

                                                         <?php  } else {?>
                                                               <li><i class="icon-toilet"></i> N/A</li>
                                                        <?php }?>
                                                    <?php }else{
                                                        if(isset($office['StandardFields']['BathsFull']) && !empty($office['StandardFields']['BathsFull'])){
                                                                if(($office['StandardFields']['BathsFull'] != "********")){?>
                                                                    <li><i class="icon-toilet"></i> <?=$office['StandardFields']['BathsFull']?> BathsFull <?=$office['StandardFields']['BathsHalf']?> BathsHalf</li>
                                                               <?php } else{?>
                                                                    <!-- <li><i class="icon-toilet"></i> N/A</li> -->
                                                               <?php } ?>

                                                         <?php  } else {?>
                                                              <!--  <li><i class="icon-toilet"></i> N/A</li> -->
                                                        <?php }?>
                                                    <?php }?>
                                                     <?php
                                                        if(!empty($office['StandardFields']['BuildingAreaTotal']) && ($office['StandardFields']['BuildingAreaTotal'] != "0")   && is_numeric($office['StandardFields']['BuildingAreaTotal'])) {?>

                                                            <li class="lot-item"><?=number_format($office['StandardFields']['BuildingAreaTotal'])?> sqft</li>

                                                    <?php } elseif(!empty($office['StandardFields']['LotSizeArea']) && ($office['StandardFields']['LotSizeArea'] != "0")   && is_numeric($office['StandardFields']['LotSizeArea'])) {


                                                            if(!empty($office['StandardFields']['LotSizeUnits'])  && ($office['StandardFields']['LotSizeUnits'] === "Acres") ){?>

                                                                <li class="lot-item"><?=number_format($office['StandardFields']['LotSizeArea'], 2, '.', ',')?> acres</li>

                                                            <?php } else { ?>

                                                                <li class="lot-item"><?=number_format($office['StandardFields']['LotSizeArea'])?> acres</li>

                                                            <?php } ?>

                                                      <?php } elseif(!empty($office['StandardFields']['LotSizeSquareFeet']) && ($office['StandardFields']['LotSizeSquareFeet'] != "0")   && is_numeric($office['StandardFields']['LotSizeSquareFeet'])) {?>

                                                            <li class="lot-item"><?=number_format($office['StandardFields']['LotSizeSquareFeet'])?> 
                                                            sqft</li>

                                                     <?php } elseif(!empty($office['StandardFields']['LotSizeAcres']) && ($office['StandardFields']['LotSizeAcres'] != "0")   && is_numeric($office['StandardFields']['LotSizeAcres'])) {?>

                                                            <li class="lot-item"><?=number_format($office['StandardFields']['LotSizeAcres'],2 ,'.',',')?> acres</li>

                                                    <?php } elseif(!empty($office['StandardFields']['LotSizeDimensions']) && ($office['StandardFields']['LotSizeDimensions'] != "0")   && ($office['StandardFields']['LotSizeDimensions'] != "********")) {?>

                                                            <li class="lot-item"><?=$office['StandardFields']['LotSizeDimensions']?></li>
                                                    <?php } else {?>
                                                            <li class="lot-item">N/A</li>
                                                    <?php } ?>
                                                  </ul>
                                              </div>
                                            </div>
                                          </div>
                                        <!-- </div> -->
                                    <?php endforeach ?>


                                <!-- grid view -->
                                <?php else: ?>

                                <?php
                                $count = 0;

                                    foreach( $office_listings as $office){

                                        if($count < 4) { ?>
                                        <div class="<?php echo $col_class; ?> featured-list-item">
                                            <div class="property-image <?php if($office['StandardFields']['PropertyClass'] == 'Land' || $office['StandardFields']['PropertyClass'] == 'MultiFamily'){ echo "property-image-land";} ?>">
                                                <?php if(!empty($token_checker)){?>
                                                        <a href="<?= base_url();?>other-property-details/<?= $office['StandardFields']['ListingKey']; ?>">
                                                <?php }
                                                    if(isset($office['Photos']['Uri300'])) { ?>
                                                        <img src="<?=$office['Photos']['Uri300']?>" alt="<?php echo $office['StandardFields']['UnparsedFirstLineAddress']; ?>" class="img-responsive" style="width:100%;">
                                                <?php } else { ?>
                                                        <img src="<?=base_url()?>assets/images/image-not-available.jpg" alt="<?php echo $office['StandardFields']['UnparsedFirstLineAddress']; ?>" class="img-responsive" style="width:100%;">
                                                <?php } ?>
                                                </a>
                                            </div>
                                            <div class="property-listing-status">
                                                <?php echo $office['StandardFields']['MlsStatus'];?>
                                            </div>
                                            <div class="property-listing-price">
                                                <div class="property-listing-type">
                                                    <?=$office['StandardFields']['PropertyClass'];?>
                                                </div>
                                                $<?=number_format($office['StandardFields']['CurrentPrice']);?>
                                            </div>
                                            <div class="property-quick-icons">
                                                <ul class="list-inline">

                                                    <?php if(isset($office['StandardFields']['BedsTotal']) && !empty($office['StandardFields']['BedsTotal'])){
                                                            if(($office['StandardFields']['BedsTotal'] != "********")){?>
                                                                <li><i class="fa fa-bed"></i> <?=$office['StandardFields']['BedsTotal']?> Bed</li>
                                                           <?php } else{?>
                                                                <li><i class="fa fa-bed"></i> N/A</li>
                                                           <?php } ?>

                                                     <?php  } else {?>
                                                            <li><i class="fa fa-bed"></i> N/A</li>
                                                    <?php }?>

                                                    <?php if($account_info->Mls != "MLS BCS"){ ?>

                                                        <?php if(isset($office['StandardFields']['BathsTotal']) && !empty($office['StandardFields']['BathsTotal'])){
                                                                if(($office['StandardFields']['BathsTotal'] != "********")){?>
                                                                    <li><i class="icon-toilet"></i> <?=$office['StandardFields']['BathsTotal']?> Bath</li>
                                                                <?php }else if(isset($office['StandardFields']['BathsFull']) && $office['StandardFields']['BathsFull'] != "********"){ ?>
                                                                     <li><i class="icon-toilet"></i> <?=$office['StandardFields']['BathsFull']?> Bath </li>
                                                               <?php } else{?>
                                                                    <li><i class="icon-toilet"></i> N/A</li>
                                                               <?php } ?>

                                                         <?php }else if(isset($office['StandardFields']['BathsFull']) && $office['StandardFields']['BathsFull'] != "********"){ ?>
                                                             <li><i class="icon-toilet"></i> <?=$office['StandardFields']['BathsFull']?> Bath </li>

                                                         <?php  } else {?>
                                                               <li><i class="icon-toilet"></i> N/A</li>
                                                        <?php }?>

                                                    <?php }else{
                                                        if(isset($office['StandardFields']['BathsFull']) && !empty($office['StandardFields']['BathsFull'])){
                                                                if(($office['StandardFields']['BathsFull'] != "********")){?>
                                                                    <li><i class="icon-toilet"></i> <?=$office['StandardFields']['BathsFull']?> Full Baths <?=$office['StandardFields']['BathsHalf']?> Half Baths</li>
                                                               <?php } else{?>
                                                                    <!-- <li><i class="icon-toilet"></i> N/A</li> -->
                                                               <?php } ?>

                                                         <?php  } else {?>
                                                              <!--  <li><i class="icon-toilet"></i> N/A</li> -->
                                                        <?php }?>
                                                    <?php }?>

                                                   <?php
                                                    if(!empty($office['StandardFields']['BuildingAreaTotal']) && ($office['StandardFields']['BuildingAreaTotal'] != "0")   && is_numeric($office['StandardFields']['BuildingAreaTotal'])) {?>

                                                            <li class="lot-item"><?=number_format($office['StandardFields']['BuildingAreaTotal'])?> sqft </li>

                                                <?php } elseif(!empty($office['StandardFields']['LotSizeArea']) && ($office['StandardFields']['LotSizeArea'] != "0")   && is_numeric($office['StandardFields']['LotSizeArea'])) {

                                                       if(!empty($office['StandardFields']['LotSizeUnits']) && ($office['StandardFields']['LotSizeUnits']) === "Acres"){?>

                                                            <li class="lot-item"><?=number_format($office['StandardFields']['LotSizeArea'], 2, '.', ',' )?> acres</li>

                                                        <?php } else {?>

                                                            <li class="lot-item"><?=number_format($office['StandardFields']['LotSizeArea'])?> acres</li>

                                                        <?php }?>

                                                <?php } elseif(!empty($office['StandardFields']['LotSizeSquareFeet']) && ($office['StandardFields']['LotSizeSquareFeet'] != "0")   && is_numeric($office['StandardFields']['LotSizeSquareFeet'])) {?>

                                                        <li class="lot-item"><?=number_format($office['StandardFields']['LotSizeSquareFeet'])?> sqft</li>

                                                 <?php } elseif(!empty($office['StandardFields']['LotSizeAcres']) && ($office['StandardFields']['LotSizeAcres'] != "0")   && is_numeric($office['StandardFields']['LotSizeAcres'])) {?>

                                                        <li class="lot-item"><?=number_format($office['StandardFields']['LotSizeAcres'],2 ,'.',',')?> acres</li>

                                                 <?php } elseif(!empty($office['StandardFields']['LotSizeDimensions']) && ($office['StandardFields']['LotSizeDimensions'] != "0")   && ($office['StandardFields']['LotSizeDimensions'] != "********")) {?>

                                                        <li class="lot-item"><?=$office['StandardFields']['LotSizeDimensions']?></li>
                                                <?php } else {?>
                                                        <li class="lot-item">N/A</li>
                                                <?php } ?>
                                                </ul>
                                            </div>
                                            <div class="property-listing-description">
                                                <p>
                                                  <?php if(!empty($token_checker)) { ?>
                                                      <a href="<?= base_url();?>other-property-details/<?= $office['StandardFields']['ListingKey']; ?>">
                                                          <b><?php echo $office['StandardFields']['UnparsedFirstLineAddress']; ?></b>
                                                      </a>
                                                    <?php }?>
                                                </p>
                                                <p>
                                                  <?php
                                                        if($account_info->Mls != "MLS BCS"){
                                                            $mystring = $office['StandardFields']['City'];
                                                            $postalcode = $office['StandardFields']['PostalCode'];
                                                            $findme   = '*';
                                                            $pos = strpos($mystring, $findme);
                                                            $pos_code = strpos($postalcode, $findme);

                                                            if($pos === false){
                                                                if($pos_code === false){
                                                                    echo $office['StandardFields']['City'] . ", " . $office['StandardFields']['StateOrProvince'] . " " . $office['StandardFields']['PostalCode'];
                                                                }else{
                                                                     echo $office['StandardFields']['City'] . ", " . $office['StandardFields']['StateOrProvince'];
                                                                }

                                                            }
                                                            else{

                                                                if($pos_code === false){
                                                                    echo $office['StandardFields']['PostalCity'] . ", " . $office['StandardFields']['StateOrProvince'] . " " . $office['StandardFields']['PostalCode'];
                                                                }else{
                                                                    echo $office['StandardFields']['PostalCity'] . ", " . $office['StandardFields']['StateOrProvince'];
                                                                }
                                                            }
                                                        }else{
                                                            $mystring = $office['StandardFields']['City'];
                                                            $StateOrProvince = $office['StandardFields']['StateOrProvince'];
                                                            $findme   = '*';
                                                            $pos = strpos($mystring, $findme);
                                                            $pCode = strpos($StateOrProvince, $findme);

                                                            if($pos === false && $pCode == false)
                                                                echo $office['StandardFields']['City'];
                                                            else
                                                                echo $office['StandardFields']['PostalCity'] . ", " . $office['StandardFields']['StateOrProvince'];
                                                        }

                                                    ?>
                                                </p>
                                            </div>
                                        </div>

                                        <?php  } $count++;
                                    } ?>

                                <?php endif ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    <?php } ?>
