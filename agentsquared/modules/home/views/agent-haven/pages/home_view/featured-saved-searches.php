
    <?php if(isset($saved_searches_featured) && !empty($saved_searches_featured)) {?>

        <section class="save-search-slider <?=(isset($is_saved_search_featured)) ? "featured-listing-area" : "new-listing-area"?>">
            <div class="container">
                <div class="row">
                    <div class="featured-title-container">

                        <?php if($is_saved_search_featured){?>
                            <div class="trapezoid"></div>
                        <?php }?>

                        <h2 class="<?=(isset($is_saved_search_featured)) ? "section-title" : ""?> other-listing-title text-center"><span><?php echo $is_saved_search_featured['Name'];?></span></h2>
                       
                    </div>

                    <?php 
                        $total = count($saved_searches_featured);

                        if ($total == 3) {
                            $container_class = "col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1";
                            $col_class = "col-md-4 col-sm-4";
                        } elseif ($total == 2) {
                            $container_class = "col-md-12 col-sm-12";
                            $col_class = "col-md-6 col-sm-6 listing-landscape";
                        }elseif ($total == 1) {
                            $container_class = "col-md-8 col-md-offset-2 col-sm-12";
                            $col_class = "col-md-12 col-sm-12 listing-single-lanscape";
                        } else {
                            $container_class = "col-md-12 col-sm-12";
                            $col_class = "col-md-3 col-sm-3";
                        }
                    ?>
                    <div class="<?php echo $container_class; ?>">
                        <div class="featured-list">

                         <?php 
                                if(isset($saved_searches_featured) && !empty($saved_searches_featured)){
                                    if(count($saved_searches_featured) > 4) {?>
                                    <p class="featured-viewall">
                                        <a href="<?php echo base_url()?>saved_searches/<?php echo $is_saved_search_featured['Id'];?>"  target="_blank" >View All</a>
                                    </p>
                            <?php   }
                                }
                        ?>

                        <?php if ($total == 1 || $total == 2): ?>
                            <?php foreach ($saved_searches_featured as $saved_featured): ?>
                                <!-- <div class="row"> -->
                                  <div class="<?=$col_class; ?>">
                                    <div class="property-list-view">
                                      <div class="col-md-5 col-sm-5">
                                          <div class="property-status">
                                              <?=$saved_featured['StandardFields']['PropertyClass'];?>
                                          </div>
                                          <?php
                                            $url_rewrite = url_title("{$saved_featured['StandardFields']['UnparsedFirstLineAddress']} {$saved_featured['StandardFields']['PostalCode']}");
                                            ?>
                                            <?php if(!empty($token_checker)) { ?>
                                                    <a href="<?= base_url();?>property-details/<?php echo $url_rewrite ?>/<?= $saved_featured['StandardFields']['ListingKey']; ?>" class="image">
                                            <?php }
                                                if(isset($saved_featured['Photos']['Uri300'])) { ?>
                                                    <img src="<?=$saved_featured['Photos']['Uri300']?>" alt="<?php echo $saved_featured['StandardFields']['UnparsedFirstLineAddress']; ?>" class="img-responsive" style="width:100%; ">
                                            <?php }
                                                else if(isset($saved_featured['Photos']['Uri300'] )) { ?>
                                                    <img src="<?=$saved_featured['Photos']['Uri300']; ?>" alt="<?php echo $saved_featured['StandardFields']['UnparsedFirstLineAddress']; ?>" class="img-responsive" style="width:100%;">
                                            <?php }
                                                else { ?>
                                                    <img src="<?=base_url()?>assets/images/image-not-available.jpg" alt="<?php echo $saved_featured['StandardFields']['UnparsedFirstLineAddress']; ?>" class="img-responsive" style="width:100%;">
                                            <?php } ?>
                                                </a>
                                      </div>
                                      <div class="col-md-7 col-sm-7">
                                          <div class="description">
                                            <h3><?php
                                                $url_rewrite = url_title("{$saved_featured['StandardFields']['UnparsedFirstLineAddress']} {$saved_featured['StandardFields']['PostalCode']}");
                                                ?>
                                             <?php if(!empty($token_checker)) { ?>
                                                <a class="listing-link" href="<?= base_url();?>property-details/<?php echo $url_rewrite ?>/<?=$saved_featured['StandardFields']['ListingKey'];?>">
                                                    <b><?php echo $saved_featured['StandardFields']['UnparsedFirstLineAddress']; ?></b>
                                                </a>
                                            <?php } ?>

                                            </h3>
                                            <p>
                                               <?php
                                                if($account_info->Mls != "MLS BCS"){
                                                    $mystring = $saved_featured['StandardFields']['City'];
                                                    $postalcode = $saved_featured['StandardFields']['PostalCode'];
                                                    $findme   = '*';
                                                    $pos = strpos($mystring, $findme);
                                                    $pos_code = strpos($postalcode, $findme);


                                                    if($pos === false){
                                                        if($pos_code === false){
                                                            echo $saved_featured['StandardFields']['City'] . ", " . $saved_featured['StandardFields']['StateOrProvince'] . " " . $saved_featured['StandardFields']['PostalCode'];
                                                        }else{
                                                            echo $saved_featured['StandardFields']['City'] . ", " . $saved_featured['StandardFields']['StateOrProvince'];
                                                        }

                                                    }
                                                    else{
                                                        if($pos_code === false){
                                                            echo $saved_featured['StandardFields']['PostalCity'] . ", " . $saved_featured['StandardFields']['StateOrProvince'] . " " . $saved_featured['StandardFields']['PostalCode'];
                                                        }else{
                                                           echo $saved_featured['StandardFields']['PostalCity'] . ", " . $saved_featured['StandardFields']['StateOrProvince'];
                                                        }

                                                    }

                                                }else{
                                                    $mystring = $saved_featured['StandardFields']['City'];
                                                    $StateOrProvince = $saved_featured['StandardFields']['StateOrProvince'];
                                                    $findme   = '*';
                                                    $pos = strpos($mystring, $findme);
                                                    $pCode = strpos($StateOrProvince, $findme);

                                                    if($pos === false && $pCode == false)
                                                        echo $saved_featured['StandardFields']['City'];
                                                    else
                                                        echo $saved_featured['StandardFields']['PostalCity'] . ", " . $saved_featured['StandardFields']['StateOrProvince'];
                                                }
                                                ?>   

                                            </p>
                                          </div>
                                          <div class="price">
                                              $ <?=number_format($saved_featured['StandardFields']['CurrentPrice']);?>
                                          </div>
                                          <ul class="prop-details">
                                            <?php if(isset($saved_featured['StandardFields']['BedsTotal']) && !empty($saved_featured['StandardFields']['BedsTotal'])){
                                                    if(($saved_featured['StandardFields']['BedsTotal'] != "********")){?>
                                                        <li><i class="fa fa-bed"></i> <?=$saved_featured['StandardFields']['BedsTotal']?> Bed</li>
                                                   <?php } else{?>
                                                        <li><i class="fa fa-bed"></i> N/A</li>
                                                   <?php } ?>

                                             <?php  } else {?>
                                                    <li><i class="fa fa-bed"></i> N/A</li>
                                            <?php }?>
                                            <?php if($account_info->Mls != "MLS BCS"){ ?>
                                                <?php if(isset($saved_featured['StandardFields']['BathsTotal']) && !empty($saved_featured['StandardFields']['BathsTotal'])){
                                                        if(($saved_featured['StandardFields']['BathsTotal'] != "********")){?>
                                                            <li><i class="icon-toilet"></i> <?=$saved_featured['StandardFields']['BathsTotal']?> Bath</li>

                                                        <?php }else if(isset($saved_featured['StandardFields']['BathsFull']) && $saved_featured['StandardFields']['BathsFull'] != "********"){ ?>
                                                        <li><i class="icon-toilet"></i> <?=$saved_featured['StandardFields']['BathsFull']?> Bath</li>

                                                       <?php } else{?>
                                                            <li><i class="icon-toilet"></i> N/A</li>
                                                       <?php } ?>
                                                       
                                                <?php }else if(isset($saved_featured['StandardFields']['BathsFull']) && $saved_featured['StandardFields']['BathsFull'] != "********"){ ?>
                                                        <li><i class="icon-toilet"></i> <?=$saved_featured['StandardFields']['BathsFull']?> Bath</li>

                                                 <?php  } else {?>
                                                       <li><i class="icon-toilet"></i> N/A</li>
                                                <?php }?>
                                            <?php }else{
                                                if(isset($saved_featured['StandardFields']['BathsFull']) && !empty($saved_featured['StandardFields']['BathsFull'])){
                                                        if(($saved_featured['StandardFields']['BathsFull'] != "********")){?>
                                                            <li><i class="icon-toilet"></i> <?=$saved_featured['StandardFields']['BathsFull']?> BathsFull <?=$saved_featured['StandardFields']['BathsHalf']?> BathsHalf</li>
                                                       <?php } else{?>
                                                            <!-- <li><i class="icon-toilet"></i> N/A</li> -->
                                                       <?php } ?>

                                                 <?php  } else {?>
                                                      <!--  <li><i class="icon-toilet"></i> N/A</li> -->
                                                <?php }?>
                                            <?php }?>
                                             <?php
                                                if(!empty($saved_featured['StandardFields']['BuildingAreaTotal']) && ($saved_featured['StandardFields']['BuildingAreaTotal'] != "0")   && is_numeric($saved_featured['StandardFields']['BuildingAreaTotal'])) {?>

                                                    <li class="lot-item"><?=number_format($saved_featured['StandardFields']['BuildingAreaTotal'])?> sqft</li>

                                            <?php } elseif(!empty($saved_featured['StandardFields']['LotSizeArea']) && ($saved_featured['StandardFields']['LotSizeArea'] != "0")   && is_numeric($saved_featured['StandardFields']['LotSizeArea'])) {


                                                    if(!empty($saved_featured['StandardFields']['LotSizeUnits'])  && ($saved_featured['StandardFields']['LotSizeUnits'] === "Acres") ){?>

                                                        <li class="lot-item"><?=number_format($saved_featured['StandardFields']['LotSizeArea'], 2, '.', ',')?> acres</li>

                                                    <?php } else { ?>

                                                        <li class="lot-item"><?=number_format($saved_featured['StandardFields']['LotSizeArea'])?> acres</li>

                                                    <?php } ?>

                                              <?php } elseif(!empty($saved_featured['StandardFields']['LotSizeSquareFeet']) && ($saved_featured['StandardFields']['LotSizeSquareFeet'] != "0")   && is_numeric($saved_featured['StandardFields']['LotSizeSquareFeet'])) {?>

                                                    <li class="lot-item"><?=number_format($saved_featured['StandardFields']['LotSizeSquareFeet'])?> 
                                                    sqft</li>

                                             <?php } elseif(!empty($saved_featured['StandardFields']['LotSizeAcres']) && ($saved_featured['StandardFields']['LotSizeAcres'] != "0")   && is_numeric($saved_featured['StandardFields']['LotSizeAcres'])) {?>

                                                    <li class="lot-item"><?=number_format($saved_featured['StandardFields']['LotSizeAcres'],2 ,'.',',')?> acres</li>

                                            <?php } elseif(!empty($saved_featured['StandardFields']['LotSizeDimensions']) && ($saved_featured['StandardFields']['LotSizeDimensions'] != "0")   && ($saved_featured['StandardFields']['LotSizeDimensions'] != "********")) {?>

                                                    <li class="lot-item"><?=$saved_featured['StandardFields']['LotSizeDimensions']?></li>
                                            <?php } else {?>
                                                    <li class="lot-item">N/A</li>
                                            <?php } ?>
                                          </ul>
                                      </div>
                                    </div>
                                  </div>
                                <!-- </div> -->
                            <?php endforeach ?>

                        <?php else: ?>
                            <?php

                                $count = 0;
                                foreach($saved_searches_featured as $saved_featured){
        
                                    if($count < 4) { ?>
                                        <div data-date="<?php echo $saved_featured['StandardFields']['OnMarketDate']; ?>" class="<?php echo $col_class; ?> featured-list-item">
                                            <div class="property-image <?php if($saved_featured['StandardFields']['PropertyClass'] == 'Land' || $saved_featured['StandardFields']['PropertyClass'] == 'MultiFamily'){ echo "property-image-land";} ?>">
                                                 <?php if(!empty($token_checker)) { ?><a href="<?= base_url();?>other-property-details/<?= $saved_featured['StandardFields']['ListingKey']; ?>"><?php } ?>
                                                <?php if(isset($saved_featured['Photos']['Uri300'])) { ?>
                                                    <img src="<?=$saved_featured['Photos']['Uri300']?>" alt="<?php echo $saved_featured['StandardFields']['UnparsedFirstLineAddress']; ?>" class="img-responsive" style="width:100%;">
                                                <?php } else { ?>
                                                    <img src="<?=base_url()?>assets/images/image-not-available.jpg" alt="<?php echo $saved_featured['StandardFields']['UnparsedFirstLineAddress']; ?>" class="img-responsive" style="width:100%;">
                                                <?php } ?>
                                                </a>
                                            </div>
                                            <div class="property-listing-status">
                                                <?php echo $saved_featured['StandardFields']['MlsStatus'];?>
                                            </div>
                                            <div class="property-listing-price">
                                                <div class="property-listing-type">
                                                    <?=$saved_featured['StandardFields']['PropertyClass'];?>
                                                </div>
                                                $<?=number_format($saved_featured['StandardFields']['CurrentPrice']);?>
                                            </div>
                                            <div class="property-quick-icons">
                                                <ul class="list-inline">
                                                    <?php if (array_key_exists('BedsTotal', $saved_featured['StandardFields'])): ?>
                                                        <?php if ($saved_featured['StandardFields']['BedsTotal'] && is_numeric($saved_featured['StandardFields']['BedsTotal']) || strpos($saved_featured['StandardFields']['BedsTotal'], "*")): ?>
                                                            <li><i class="fa fa-bed"></i> <?=$saved_featured['StandardFields']['BedsTotal']?> Bed</li>
                                                        <?php elseif (empty($saved_featured['StandardFields']['BedsTotal'])): ?>
                                                            <li><i class="fa fa-bed"></i> N/A</li>
                                                        <?php else: ?>
                                                            <li><i class="fa fa-bed"></i> N/A</li>
                                                        <?php endif ?>
                                                    <?php else: ?>
                                                        <li><i class="fa fa-bed"></i> N/A</li>
                                                    <?php endif ?>

                                                    <?php if (array_key_exists('BathsTotal', $saved_featured['StandardFields'])): ?>
                                                        <?php if ($saved_featured['StandardFields']['BathsTotal'] && is_numeric($saved_featured['StandardFields']['BathsTotal']) ||  strpos($saved_featured['StandardFields']['BathsTotal'], "*")): ?>
                                                            <li><i class="icon-toilet"></i> <?=$saved_featured['StandardFields']['BathsTotal']?> Bath</li>
                                                        <?php elseif (empty($saved_featured['StandardFields']['BathsTotal'])): ?>
                                                            <li><i class="icon-toilet"></i> N/A</li>
                                                        <?php else: ?>
                                                            <li><i class="icon-toilet"></i> N/A</li>
                                                        <?php endif ?>
                                                    <?php else: ?>
                                                        <li><i class="icon-toilet"></i> N/A</li>
                                                    <?php endif ?>

                                               <?php
                                               if(!empty($saved_featured['StandardFields']['BuildingAreaTotal']) && ($saved_featured['StandardFields']['BuildingAreaTotal'] != "0")   && is_numeric($saved_featured['StandardFields']['BuildingAreaTotal'])) {?>

                                                        <li class="lot-item"><?=number_format($saved_featured['StandardFields']['BuildingAreaTotal'])?> sqft </li>
                                                <?php } elseif(!empty($saved_featured['StandardFields']['LotSizeArea']) && ($saved_featured['StandardFields']['LotSizeArea'] != "0")   && is_numeric($saved_featured['StandardFields']['LotSizeArea'])) {

                                                    if(!empty($saved_featured['StandardFields']['LotSizeUnits']) && ($saved_featured['StandardFields']['LotSizeUnits']) === "Acres"){?>
                                                        <li class="lot-item"><?=number_format($saved_featured['StandardFields']['LotSizeArea'], 2, '.', ',' )?> acres</li>

                                                    <?php } else {?>

                                                        <li class="lot-item"><?=number_format($saved_featured['StandardFields']['LotSizeArea'])?> acres</li>

                                                    <?php }?>

                                                <?php } elseif(!empty($saved_featured['StandardFields']['LotSizeSquareFeet']) && ($saved_featured['StandardFields']['LotSizeSquareFeet'] != "0")   && is_numeric($saved_featured['StandardFields']['LotSizeSquareFeet'])) {?>

                                                        <li class="lot-item"><?=number_format($saved_featured['StandardFields']['LotSizeSquareFeet'])?> sqft</li> 

                                                 <?php } elseif(!empty($saved_featured['StandardFields']['LotSizeAcres']) && ($saved_featured['StandardFields']['LotSizeAcres'] != "0")   && is_numeric($saved_featured['StandardFields']['LotSizeAcres'])) {?>

                                                        <li class="lot-item"><?=number_format($saved_featured['StandardFields']['LotSizeAcres'],2 ,'.',',')?> acres</li>
                                                <?php } elseif(!empty($saved_featured['StandardFields']['LotSizeDimensions']) && ($saved_featured['StandardFields']['LotSizeDimensions'] != "0")   && ($saved_featured['StandardFields']['LotSizeDimensions'] != "********")) {?>

                                                    <li class="lot-item"><?=$saved_featured['StandardFields']['LotSizeDimensions']?></li>
                                                <?php } else {?>
                                                        <li class="lot-item">N/A</li>
                                                <?php } ?>
                                                </ul>
                                            </div>
                                            <div class="property-listing-description">
                                                <p>
                                                    <?php if(!empty($token_checker)) { ?>
                                                        <a class="listing-link" href="<?= base_url();?>other-property-details/<?= $saved_featured['StandardFields']['ListingKey']; ?>">
                                                            <b><?php echo $saved_featured['StandardFields']['UnparsedFirstLineAddress']; ?></b>
                                                        </a>
                                                    <?php }?>
                                                </p>
                                                <p>
                                                  <?php
                                                        $mystring = $saved_featured['StandardFields']['City'];
                                                        $findme   = '*';
                                                        $pos = strpos($mystring, $findme);

                                                    if($pos === false)
                                                        echo $saved_featured['StandardFields']['City'] . ", " . $saved_featured['StandardFields']['StateOrProvince'] . " " . $saved_featured['StandardFields']['PostalCode'];
                                                        else
                                                        echo $saved_featured['StandardFields']['PostalCity'] . ", " . $saved_featured['StandardFields']['StateOrProvince'] . " " . $saved_featured['StandardFields']['PostalCode'];
                                                    ?>
                                                </p>    
                                            </div>
                                        </div>

                            <?php  } $count++;

                                 }
                            ?> 
                        <?php endif ?>

                        </div>

                    </div>
                </div>
            </div>
        </section>
    <?php } ?>