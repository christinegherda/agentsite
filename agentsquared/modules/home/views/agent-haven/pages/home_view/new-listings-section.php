        

    <!-- NEW LISTINGS SECTION -->
    <?php 

    $count = 0;

    if($iCount == 0){
            $counter = 4;
    }elseif($iCount == 1){
        $counter =  3;
    }elseif($iCount == 2){
        $counter =  2;
    }elseif($iCount == 3){
        $counter =  1;
    }else{
        $counter = 4;
    }

    foreach($new_listings as $new_listing){

        if($count++ >= $counter) break;

            if(!empty($new_listing->StandardFields->Photos)){
             $photo = $new_listing->StandardFields->Photos[0]->Uri300 ;
            }
            else if(!empty(isset($new_listing->Photos->Uri300))) {
                 $photo = $new_listing->Photos->Uri300 ;
            }
            $defaultPhoto = base_url('assets/images/image-not-available.jpg');
        ?>
            <div class="col-md-3 col-sm-3  featured-list-item new-<?=$counter?>">
                <div class="property-image <?php if($new_listing->StandardFields->PropertyClass == 'Land' || $new_listing->StandardFields->PropertyClass == 'MultiFamily'){ echo "property-image-land";} ?>">
                    <?php if(!empty($token_checker)){?>
                        <a class="listing-link"  href="<?= base_url();?>other-property-details/<?= $new_listing->StandardFields->ListingKey;?>">
                    <?php }?>
                    <?php if(isset($new_listing->StandardFields->Photos[0]->Uri300) || isset($new_listing->Photos->Uri300)) { ?>
                        <img src="<?= (@getimagesize($photo)) ? $photo : $defaultPhoto ?>" alt="<?= $new_listing->StandardFields->UnparsedFirstLineAddress;?>" class="img-responsive" style="width:100%;">
                    <?php } else { ?>
                        <img src="<?=base_url()?>assets/images/image-not-available.jpg" alt="<?=$new_listing->StandardFields->UnparsedFirstLineAddress; ?>" class="img-responsive" style="width:100%;">
                    <?php } ?>
                    </a>
                </div>
                <div class="property-listing-status">
                     <?php echo $new_listing->StandardFields->MlsStatus;?>
                </div>
                <div class="property-listing-price">
                    <div class="property-listing-type">
                        <?=$new_listing->StandardFields->PropertyClass;?>
                    </div>
                    $<?=number_format($new_listing->StandardFields->CurrentPrice);?>
                </div>
                <div class="property-quick-icons">
                    <ul class="list-inline">
                        <?php if(isset($new_listing->StandardFields->BedsTotal) && !empty($new_listing->StandardFields->BedsTotal)){
                                if(($new_listing->StandardFields->BedsTotal != "********")){?>
                                    <li><i class="fa fa-bed"></i> <?=$new_listing->StandardFields->BedsTotal?> Bed</li>
                               <?php } else{?>
                                    <li><i class="fa fa-bed"></i> N/A</li>
                               <?php } ?>

                         <?php  } else {?>
                                <li><i class="fa fa-bed"></i> N/A</li>
                        <?php }?>
                        <?php if($account_info->Mls != "MLS BCS"){ ?>
                            <?php if(isset($new_listing->StandardFields->BathsTotal) && !empty($new_listing->StandardFields->BathsTotal)){
                                    if(($new_listing->StandardFields->BathsTotal != "********")){?>
                                        <li><i class="icon-toilet"></i> <?=$new_listing->StandardFields->BathsTotal?> Bath</li>
                                    <?php }else if(isset($new_listing->StandardFields->BathsFull) && $new_listing->StandardFields->BathsFull != "********"){ ?>
                                            <li><i class="icon-toilet"></i> <?=$new_listing->StandardFields->BathsFull?> Bath</li>
                                   <?php } else{?>
                                        <li><i class="icon-toilet"></i> N/A</li>
                                   <?php } ?>

                            <?php }else if(isset($new_listing->StandardFields->BathsFull) && $new_listing->StandardFields->BathsFull != "********" ){ ?>
                                        <li><i class="icon-toilet"></i> <?=$new_listing->StandardFields->BathsFull?> Bath</li>

                             <?php  } else {?>
                                   <li><i class="icon-toilet"></i> N/A</li>
                            <?php }?>
                        <?php }else{
                            if(isset($new_listing->StandardFields->BathsFull) && !empty($new_listing->StandardFields->BathsFull)){
                                    if(($new_listing->StandardFields->BathsFull != "********")){?>
                                        <li><i class="icon-toilet"></i> <?=$new_listing->StandardFields->BathsFull?> BathsFull <?=$new_listing->StandardFields->BathsHalf?> BathsHalf</li>
                                   <?php } else{?>
                                        <!-- <li><i class="icon-toilet"></i> N/A</li> -->
                                   <?php } ?>

                             <?php  } else {?>
                                  <!--  <li><i class="icon-toilet"></i> N/A</li> -->
                            <?php }?>
                        <?php }?>
                        <?php
                            if(!empty($new_listing->StandardFields->BuildingAreaTotal) && ($new_listing->StandardFields->BuildingAreaTotal != "0")   && is_numeric($new_listing->StandardFields->BuildingAreaTotal)) {?>

                                <li class="lot-item"><?=number_format($new_listing->StandardFields->BuildingAreaTotal)?> sqft </li>

                            <?php } elseif(!empty($new_listing->StandardFields->LotSizeArea) && ($new_listing->StandardFields->LotSizeArea != "0")   && is_numeric($new_listing->StandardFields->LotSizeArea)) {

                                if(!empty($new_listing->StandardFields->LotSizeUnits) && ($new_listing->StandardFields->LotSizeUnits) === "Acres"){?>

                                    <li class="lot-item"><?=number_format($new_listing->StandardFields->LotSizeArea, 2, '.', ',' )?> acres</li>

                                <?php } else {?>

                                    <li class="lot-item"><?=number_format($new_listing->StandardFields->LotSizeArea)?> acres</li>

                                <?php }?>

                            <?php } elseif(!empty($new_listing->StandardFields->LotSizeSquareFeet) && ($new_listing->StandardFields->LotSizeSquareFeet != "0")   && is_numeric($new_listing->StandardFields->LotSizeSquareFeet)) {?>

                                    <li class="lot-item"><?=number_format($new_listing->StandardFields->LotSizeSquareFeet)?> sqft</li>

                             <?php } elseif(!empty($new_listing->StandardFields->LotSizeAcres) && ($new_listing->StandardFields->LotSizeAcres != "0")   && is_numeric($new_listing->StandardFields->LotSizeAcres)) {?>

                                    <li class="lot-item"><?=number_format($new_listing->StandardFields->LotSizeAcres,2 ,'.',',')?> acres</li>

                             <?php } elseif(!empty($new_listing->StandardFields->LotSizeDimensions) && ($new_listing->StandardFields->LotSizeDimensions != "0")   && ($new_listing->StandardFields->LotSizeDimensions != "********")) {?>

                                            <li class="lot-item"><?=$new_listing->StandardFields->LotSizeDimensions?></li>
                            <?php } else {?>
                                            <li class="lot-item">N/A</li>
                            <?php } ?>
                    </ul>
                </div>
                <div class="property-listing-description">
                    <p>
                        <?php if(!empty($token_checker)) { ?>
                            <a class="listing-link"  href="<?= base_url();?>other-property-details/<?= $new_listing->StandardFields->ListingKey;?>">
                                <b><?php echo $new_listing->StandardFields->UnparsedFirstLineAddress; ?></b>
                            </a>
                        <?php } ?>
                    </p>
                    <p>
                   <?php
                        if($account_info->Mls != "MLS BCS"){
                            $mystring = $new_listing->StandardFields->City;
                            $findme   = '*';
                            $pos = strpos($mystring, $findme);

                            if($pos === false)
                                echo $new_listing->StandardFields->City . ", " . $new_listing->StandardFields->StateOrProvince . " " . $new_listing->StandardFields->PostalCode;
                            else
                                echo $new['StandardFields']['PostalCity'] . ", " . $new_listing->StandardFields->StateOrProvince . " " . $new_listing->StandardFields->PostalCode;
                        }else{
                            $mystring = $new_listing->StandardFields->City;
                            $StateOrProvince = $new_listing->StandardFields->StateOrProvince;
                            $findme   = '*';
                            $pos = strpos($mystring, $findme);
                            $pCode = strpos($StateOrProvince, $findme);

                            if($pos === false && $pCode == false)
                                echo $new_listing->StandardFields->City;
                            else
                                echo $new_listing->StandardFields->PostalCity . ", " . $new_listing->StandardFields->StateOrProvince;
                        }
                    ?>
                    </p>
                </div>
            </div>
    <?php }
        $count++;?>

        