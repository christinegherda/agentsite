<div class="page-content tx-notice-services">
  <div class="container">
    <div class="tx-wrapper">
      <div class="logo logo-l">
        <img src="/assets/images/tx-trec.png" alt="">
      </div>
      <div class="logo logo-r">
        <img src="/assets/images/tx-eho.png" alt="">
      </div>
      <div class="text-center tx-heading">
        <h1 style="margin-bottom: 25px;"> Information About Brokerage Services </h1>
        <p><i>Texas law requires all real estate license holders to give the following information about <br> brokerage services to prospective buyers, tenants, sellers and landlords.</i></p>
      </div>
      <h3>TYPES OF REAL ESTATE LICENSE HOLDERS:</h3>
      <ul>
        <li> A BROKER is responsible for all brokerage activities, including acts performed by sales agents sponsored by the broker.</li>
        <li> A SALES AGENT must be sponsored by a broker and works with clients on behalf of the broker. </li>
      </ul>

      <h3>A BROKER’S MINIMUM DUTIES REQUIRED BY LAW (A client is the person or party that the broker represents):</h3>
      <ul>
        <li>Put the interests of the client above all others, including the broker’s own interests;</li>
        <li>Inform the client of any material information about the property or transaction received by the broker;</li>
        <li>Answer the client’s questions and present any offer to or counter-offer from the client; and</li>
        <li>Treat all parties to a real estate transaction honestly and fairly.</li>
      </ul>

      <h3>A LICENSE HOLDER CAN REPRESENT A PARTY IN A REAL ESTATE TRANSACTION:</h3>

      <p><b>AS AGENT FOR OWNER (SELLER/LANDLORD):</b> The broker becomes the property owner's agent through an agreement with the
      owner, usually in a written listing to sell or property management agreement. An owner's agent must perform the broker’s minimum
      duties above and must inform the owner of any material information about the property or transaction known by the agent, including
      information disclosed to the agent or subagent by the buyer or buyer’s agent.</p>

      <p><b>AS AGENT FOR BUYER/TENANT:</b> The broker becomes the buyer/tenant's agent by agreeing to represent the buyer, usually through a
      written representation agreement. A buyer's agent must perform the broker’s minimum duties above and must inform the buyer of any
      material information about the property or transaction known by the agent, including information disclosed to the agent by the seller or
      seller’s agent.</p>

      <p><b>AS AGENT FOR BOTH - INTERMEDIARY:</b> To act as an intermediary between the parties the broker must first obtain the written
      agreement of each party to the transaction. The written agreement must state who will pay the broker and, in conspicuous bold or
      underlined print, set forth the broker's obligations as an intermediary. A broker who acts as an intermediary: </p>

      <ul>
        <li>Must treat all parties to the transaction impartially and fairly;</li>
        <li>May, with the parties' written consent, appoint a different license holder associated with the broker to each party (owner and
      buyer) to communicate with, provide opinions and advice to, and carry out the instructions of each party to the transaction.</li>
        <li>Must not, unless specifically authorized in writing to do so by the party, disclose:
          <ul>
            <li>that the owner will accept a price less than the written asking price;</li>
            <li>that the buyer/tenant will pay a price greater than the price submitted in a written offer; and</li>
            <li>any confidential information or any other information that a party specifically instructs the broker in writing not to
      disclose, unless required to do so by law.</li>
          </ul>
        </li>
      </ul>

      <p><b>AS SUBAGENT:</b>  A license holder acts as a subagent when aiding a buyer in a transaction without an agreement to represent the
      buyer. A subagent can assist the buyer but does not represent the buyer and must place the interests of the owner first. </p>


      <p><b>TO AVOID DISPUTES, ALL AGREEMENTS BETWEEN YOU AND A BROKER SHOULD BE IN WRITING AND CLEARLY ESTABLISH: </b> </p>
      <ul>
        <li>The broker’s duties and responsibilities to you, and your obligations under the representation agreement.</li>
        <li>Who will pay the broker for services provided to you, when payment will be made and how the payment will be calculated.</li>
      </ul>

      <p><b>LICENSE HOLDER CONTACT INFORMATION:</b>  This notice is being provided for information purposes. It does not create an obligation for
      you to use the broker’s services. Please acknowledge receipt of this notice below and retain a copy for your records. </p>

      <?php 
        //broker
        if(isset($account_info->Office) && !empty($account_info->Office) && ($account_info->Office !== "********")) {
             $broker = $account_info->Office;
        }
        if(isset($account_info->LicenseNumber) && !empty($account_info->LicenseNumber)){
            $license_number = $account_info->LicenseNumber;
        }

        //email
        if (isset($user_info->email) && !empty($user_info->email)) {
            $email = $user_info->email;
        } elseif(isset($account_info->Emails[0]->Address) && !empty($account_info->Emails[0]->Address) && ($account_info->Emails[0]->Address !== "********")) {
            $email = $account_info->Emails[0]->Addres;
        }

        //phone_number
        if (isset($user_info->phone) && !empty($user_info->phone)) {
            $phone_number = $user_info->phone;
        } elseif(isset($account_info->Phones[0]->Number) && !empty($account_info->Phones[0]->Number) && ($account_info->Phones[0]->Number !== "********")) {
             $phone_number = $account_info->Phones[0]->Number;
        }

        //first_name and last_name
        if(isset($user_info->first_name) && !empty($user_info->last_name)) {
            $first_name = $user_info->first_name;
            $last_name = $user_info->last_name;
        }elseif(isset($account_info->FirstName) && !empty($account_info->LastName)){
            $first_name = $account_info->FirstName;
            $last_name = $account_info->LastName;
        }

        $supervisor = '';
        $supervisor_license_no = '';
        $supervisor_email = '';
        $supervisor_phone = '';
        
        if (isset($user_info->supervisor) && !empty($user_info->supervisor)) {
          $supervisor = $user_info->supervisor;
        }
        
        if (isset($user_info->supervisor_license_no) && !empty($user_info->supervisor_license_no)) {
          $supervisor_license_no = $user_info->supervisor_license_no;
        }
        
        if (isset($user_info->supervisor_email) && !empty($user_info->supervisor_email)) {
          $supervisor_email = $user_info->supervisor_email;
        }
        
        if (isset($user_info->supervisor_phone) && !empty($user_info->supervisor_phone)) {
          $supervisor_phone = $user_info->supervisor_phone;
        }
        
      ?>
      <div class="row">
        <div class="col-md-12">
          <table class="table table-border table-tx table-l1">
            <thead>
              <tr>
                <th width="40%" class="text-center">Licensed Broker /Broker Firm Name or Primary Assumed Business Name </th>
                <th class="text-center">License No. </th>
                <th class="text-center">Email </th>
                <th class="text-center">Phone </th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td class="text-center"><?=$broker; ?></td>
                <td class="text-center"><?=$license_number; ?></td>
                <td class="text-center"><?=$email; ?></td>
                <td class="text-center"><?=$phone_number; ?></td>
              </tr>
            </tbody>
          </table>
          <?php if (isset($broker_info)): ?>  
            <table class="table table-border table-tx">
              <thead>
                <tr>
                  <th width="40%" class="text-center">Designated Broker of Firm </th>
                  <th class="text-center">License No. </th>
                  <th class="text-center">Email </th>
                  <th class="text-center">Phone </th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td class="text-center"><?php echo $broker_info['Results']['0']['Name']; ?></td>
                  <td class="text-center"><?php echo $broker_info['Results']['0']['LicenseNumber']; ?></td>
                  <td class="text-center"><?php echo $broker_info['Results']['0']['Emails']['0']['Address']; ?></td>
                  <td class="text-center"><?php echo $broker_info['Results']['0']['Phones']['0']['Number']; ?></td>
                </tr>
              </tbody>
            </table>  
          <?php endif ?>  
          <?php if (isset($user_info->supervisor) && !empty($user_info->supervisor)): ?>
            <table class="table table-border table-tx">
              <thead>
                <tr>
                  <th width="40%" class="text-center">Licensed Supervisor of Sales Agent/Associate  </th>
                  <th class="text-center">License No. </th>
                  <th class="text-center">Email </th>
                  <th class="text-center">Phone </th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td class="text-center"><?=$user_info->supervisor; ?></td>
                  <td class="text-center"><?=$user_info->supervisor_license_no; ?></td>
                  <td class="text-center"><?=$user_info->supervisor_email; ?></td>
                  <td class="text-center"><?=$user_info->supervisor_phone; ?></td>
                </tr>
              </tbody>
            </table>   
              
          <?php endif ?>   
          <table class="table table-border table-tx">
            <thead>
              <tr>
                <th width="40%" class="text-center">Sales Agent/Associate’s Name </th>
                <th class="text-center">License No. </th>
                <th class="text-center">Email </th>
                <th class="text-center">Phone </th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td class="text-center"><?php echo $first_name." ".$last_name; ?></td>
                <td class="text-center"><?=$license_number; ?></td>
                <td class="text-center"><?=$email; ?></td>
                <td class="text-center"><?=$phone_number; ?></td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>


      <div class="row">
        <div class="col-md-6"><p><b>Regulated by the Texas Real Estate Commission</b></p></div>
        <div class="col-md-6 text-right"><p><b> Information available at <a href="http://www.trec.texas.gov/">www.trec.texas.gov</a></b><br><small>ABS 1-0</small></p></div>
      </div>
    </div>
  </div>
</div>