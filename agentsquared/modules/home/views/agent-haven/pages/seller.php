
<?php if(isset($settings->enable_cloudcma) && $settings->enable_cloudcma == 'true' && isset($settings->cloudcma_api_key) && $settings->cloudcma_api_key != '' && $plan_id == 2){ ?>
  <iframe src="https://cloudcma.com/api_widget/<?php echo $settings->cloudcma_api_key; ?>/show?post_url=cloudcma.com&source_url=ua" style="width: 100%; height: 100vh; border: none; margin: 0; margin-top: 145px;"></iframe>
<?php }else{ ?>
  <div class="page-content">
        <section class="property-detail-content">
            <div class="container">
                <div class="row">
                	<!-- Sidebar Area -->
        			<?php $this->load->view($theme."/session/sidebar"); ?>
        			<div class="col-md-9 col-sm-9">
                        <div class="seller-blurb">
                            <?=(isset($page_data) AND !empty($page_data)) ? $page_data['content'] : "";?>
                        </div>
                        <div class="property-details">
                            <div class="seller-return-msg"></div>
                            <form action="<?php echo site_url('home/home/save_customer_seller'); ?>" method="POST" id="seller_form">
                                <div class="property-information">
                                    <h4>Home Information</h4>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="">Address</label>
                                                <input type="text" name="seller[address]" class="form-control" maxlength="70" pattern="[a-zA-Z0-9-_.\s]+" title="Allowed characters: A-Z, 0-9, ., -, _">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="">City</label>
                                                <input type="text" name="seller[city]" class="form-control" maxlength="50" pattern="[a-zA-Z0-9-_.\s]+" title="Allowed characters: A-Z, 0-9, ., -, _">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="">State</label>
                                                <input type="text" name="seller[state]" class="form-control" maxlength="30" pattern="[a-zA-Z0-9-_.\s]+" title="Allowed characters: A-Z, 0-9, ., -, _">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="">Zip Code</label>
                                                <input type="text" name="seller[zip_code]" class="form-control" maxlength="15" pattern="(\d{5}([\-]\d{4})?)" title="Allowed characters: 0-9 {5}">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="">No. of Bedrooms<small class="text-red">*</small></label>
                                                <select name="seller[bedrooms]" id="" class="form-control" required>
                                                    <?php for($i=1; $i<=10; $i++) { ?>
                                                            <option value="<?=$i;?>"><?=$i;?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="">No. of Bathrooms<small class="text-red">*</small></label>
                                                <select name="seller[bathrooms]" id="" class="form-control" required>
                                                    <?php for($i=1; $i<=10; $i++) { ?>
                                                            <option value="<?=$i;?>"><?=$i;?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="">Square Feet</label>
                                                <select name="seller[square_feet]" id="" class="form-control" required>
                                                    <option value="Less than 1000">< 1000</option>
                                                    <option value="1000 - 2000">1000 - 2000</option>
                                                    <option value="2000 - 3000">2000 - 3000</option>
                                                    <option value="3000 - 4000">3000 - 4000</option>
                                                    <option value="4000 - 5000">4000 - 5000</option>
                                                    <option value="5000 - 6000">5000 - 6000</option>
                                                    <option value="6000 - 7000">6000 - 7000</option>
                                                    <option value="7000 - 8000">7000 - 8000</option>
                                                    <option value="8000 - 9000">8000 - 9000</option>
                                                    <option value="9000 - 10000">9000 - 10000</option>
                                                    <option value="Greater than 10000">> 1000</option>

                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="contact-information clearfix">
                                    <h4>Contact Information</h4>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="">First Name<small class="text-red">*</small></label>
                                                <input type="text" name="seller[fname]" value="<?= (isset($profile->first_name)) ? $profile->first_name : "" ; ?>" class="form-control" maxlength="50" pattern="[a-zA-Z0-9-_.\s]+" title="Allowed characters: A-Z, 0-9, ., -, _" required>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="">Last Name<small class="text-red">*</small></label>
                                                <input type="text" name="seller[lname]" value="<?= (isset($profile->last_name)) ? $profile->last_name : "" ; ?>" class="form-control" maxlength="50" pattern="[a-zA-Z0-9-_.\s]+" title="Allowed characters: A-Z, 0-9, ., -, _" required>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="">Email<small class="text-red">*</small></label>
                                                <input type="email" name="seller[email]" value="<?= (isset($profile->email)) ? $profile->email : "" ; ?>" class="form-control" required <?= (isset($profile->email)) ? "readonly" : "" ?> >
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="">Mobile<small class="text-red">*</small></label>
                                                <input type="tel" name="seller[mobile]" value="<?= (isset($profile->mobile)) ? $profile->mobile : "" ; ?>" class="form-control phone_number" required>
                                            </div>
                                        </div>
                                         <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="">Phone<small class="text-red">*</small></label>
                                                <input type="tel" name="seller[phone]" value="<?= (isset($profile->phone)) ? $profile->phone : "" ; ?>" class="form-control phone_number"  <?= (isset($profile->phone)) ? "readonly" : "" ?>>
                                            </div>
                                        </div>
                                         <div class="col-md-12">
                                            <div class="form-group">
                                                <div class="g-recaptcha" data-sitekey="6Lf9eRsUAAAAAB5lP4Em8vm4L-c1m0gSyAKOTort"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <button class="btn btn-default submit-button seller-sbt-btn">Get my home's worth</button>
                                    </div>
                                </div>
                            </form>
                        </div>

                    </div>
        		</div>
    		</div>
    	</section>
  </div>
<?php } ?>
