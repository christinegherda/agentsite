      
      <section class="property-map">
        <div id="map_canvas" style="border: 2px solid #3872ac;"></div>
        <style type="text/css">
            html,
            body,
            #map_canvas  {
                height: 600px;
                width: 100%;
                margin-top: 0px;
                padding: 0px;
                z-index: -1;
            }
        </style>
        
        <script type="text/javascript">

                function initialize() {
                            map = new google.maps.Map(
                                document.getElementById("map_canvas"), {
                                  center: new google.maps.LatLng(37.387474,-122.05754339999999),
                                   zoom: 13,
                                   scrollwheel: false,
                                   mapTypeId: google.maps.MapTypeId.ROADMAP
                                 });
                            geocoder = new google.maps.Geocoder();

                        <?php 
                            if(isset($active_listings) && $active_listings) {
                                foreach($active_listings as $active) {
                        ?>
                                    var l1 = "<?php echo $active->StandardFields->UnparsedFirstLineAddress; ?>";
                                    var l2 = "<?php echo $active->StandardFields->UnparsedAddress;?>";
                                    var l3 = base_url + "other-property-details/<?php echo $active->StandardFields->ListingKey;?>";
                                    var l4 = "<?php echo (isset($active->StandardFields->Photos[0]->Uri300)) ? $active->StandardFields->Photos[0]->Uri300 : ""; ?>";
                                    var locations = [
                                        [l1, l2, l3, l4]
                                    ];
                                    geocodeAddress(locations);
                         <?php  } } ?>

                 }

                        var geocoder;
                        var map;
                        var bounds = new google.maps.LatLngBounds();

                        function geocodeAddress(locations, i) {
                            var title = locations[0][0];
                            var address = locations[0][1];
                            var url = locations[0][2];
                            var img = locations[0][3];

                            geocoder.geocode({
                              'address': locations[0][1]
                            },

                            function(results, status) {
                                if (status == google.maps.GeocoderStatus.OK) {
                                    var marker = new google.maps.Marker({
                                          icon: '<?=$protocol;?>maps.google.com/mapfiles/ms/icons/red.png',
                                          map: map,
                                          position: results[0].geometry.location,
                                          title: title,
                                          animation: google.maps.Animation.DROP,
                                          address: address,
                                          url: url
                                    })
                                    //infoWindow(marker, map, title, address, url, img);

                                    google.maps.event.addListener(marker, 'mouseover', function() {
                                        var html = "<div style='position:relative'> <br><img src="+img+" alt="+address+" width='205'  height='100'><br>" + address + "<br><a href='" + url + "' target='_blank' >View Property</a></p></div>";
                                        iw = new google.maps.InfoWindow({
                                          content: html,
                                          maxWidth: 205,
                                          //position: "left"
                                        });
                                        iw.open(map, marker);

                                    });

                                    google.maps.event.addListener(marker, 'click', function() {
                                        var html = "<div style='position:relative'> <br><img src="+img+" alt="+address+" width='205'  height='100'><br>" + address + "<br><a href='" + url + "' target='_blank' >View Property</a></p></div>";
                                        iw = new google.maps.InfoWindow({
                                          content: html,
                                          maxWidth: 205,
                                          //position: "left"
                                        });
                                        iw.open(map, marker);

                                    });
          

                                    google.maps.event.addListener(marker, 'mouseout', function() {
                                        iw.close();
                                    });

                                    bounds.extend(marker.getPosition());
                                    map.fitBounds(bounds);
                                } else if (status == google.maps.GeocoderStatus.OVER_QUERY_LIMIT) { 

                                    wait = true;
                                    setTimeout(function() {
                                        geocodeAddress(locations, i);
                                    }, 1000 );
                                    
                                } else {
                                    //console.log( "Geocode was not successful for the following reason: " + status );
                                    //alert("Geocode was not successful for the following reason: " + status);
                                }
                             
                            });
                        }
      

            google.maps.event.addDomListener(window, "load", initialize);

             function infoWindow(marker, map, title, address, url, img) {
                  google.maps.event.addListener(marker, 'mouseover', function() {
                    var html = "<div><h3>" + title + "</h3><p>" + address + "<br><img src="+img+" alt="+address+" width='200'  height='150'><br><a href='" + url + "' target='_blank' >View Property</a></p></div>";
                    iw = new google.maps.InfoWindow({
                      content: html,
                      maxWidth: 350
                    });
                    iw.open(map, marker);
                  });
                }

        </script>
    </section>