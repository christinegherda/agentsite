
  <?php 

    $this->load->view('/home_view/slider-area'); 

    $this->load->view('/home_view/search-view');

    if( !empty($active_listings) ) { 

      $this->load->view('/home_view/new-default-active-listings');
    }

  ?>
    <section class="other-listing-area">
        <div class="container">
            <div class="row">

                 <?php if(!empty($new_listings)  && !empty($nearby_listings)) { 

                       $this->load->view('/home_view/default-new-listings'); 

                       $this->load->view('/home_view/default-nearby-listings'); 

                  } elseif(!empty($new_listings)){

                    $this->load->view('/home_view/custom-new-listings'); 

                  } elseif(!empty($nearby_listings)){

                     $this->load->view('/home_view/custom-nearby-listings');
                  }?>

            </div>
        </div>
    </section>

    <section class="agent-info-area">
        <div class="container">
            <div class="row">
                
                <?php $this->load->view('/home_view/contact-view'); ?>

                <div itemscope itemtype="http://schema.org/RealEstateAgent" class="col-md-8 col-sm-12 nopadding-left mobilepadding-0">

                  <?php $this->load->view('/home_view/agent-info-view'); ?> 

                    <?php if( isset($sold_listings) ) { 
                      $this->load->view('/home_view/sold-properties'); 
                    }
                   ?> 
                </div>
            </div>
        </div>
    </section>


