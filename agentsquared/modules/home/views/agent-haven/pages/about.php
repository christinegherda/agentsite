
      <div class="page-content">
          <section class="property-detail-content">
              <div class="container">
                  <div class="row">
                      <!-- Sidebar Area -->
                      <?php $this->load->view($theme."/session/sidebar"); ?>

                      <div class="col-md-3 col-sm-3 desktop-hide visible-xs contact-sidebar">
                          <div class="contact-panel-show">
                              <div class="filter-tab">
                                  <div class="property-agent-info">
                                     <!--  <h4 class="text-center">Agent</h4>
                                      <hr> -->
                                      <div class="agent-image">
                                        <?php
                                            if( isset($user_info->agent_photo) AND !empty($user_info->agent_photo)) {

                                                 if(!empty(is_freemium())){
                                                    $agent_photo = getenv('AWS_S3_ASSETS') . "uploads/photo/".$user_info->agent_photo;
                                                } else{
                                                    $agent_photo = base_url()."assets/upload/photo/".$user_info->agent_photo;
                                                }?>

                                                <img src="<?php echo $agent_photo ?>" alt="<?=isset($user_info->first_name) ? htmlentities($user_info->first_name) : $account_info->FirstName?> <?=isset($user_info->last_name) ? htmlentities($user_info->last_name) : $account_info->LastName?>"  class="img-thumbnail" width="400">
                                        <?php
                                            } else {
                                                if(isset($account_info->Images[0]->Uri) AND !empty($account_info->Images[0]->Uri)) {?>
                                                    <img src="<?=$account_info->Images[0]->Uri?>" alt="<?=isset($user_info->first_name) ? htmlentities($user_info->first_name) : $account_info->FirstName?> <?=isset($user_info->last_name) ? htmlentities($user_info->last_name) : $account_info->LastName?>" class="img-thumbnail" width="400">
                                        <?php   } else { ?>
                                        <img src="<?= base_url()?>assets/images/no-profile-img.gif" alt="No Profile Image">
                                        <?php
                                                }
                                            }
                                        ?>
                                      </div>
                                       <h4 class="agent-name">
                                          <?php echo isset($user_info->first_name) ? htmlentities($user_info->first_name) : $account_info->FirstName;?> 
                                          <?php echo isset($user_info->last_name) ? htmlentities($user_info->last_name) : $account_info->LastName;?> 
                                  </h4>
                                 <p>

                                     <?php
                                      //Office Phone
                                       if(isset($user_info->phone) AND !empty($user_info->phone)){?>

                                          <i class="fa fa-phone-square"></i> Office: <?= $user_info->phone; ?><br/>

                                       <?php }else{?>

                                        <?php   if(isset($account_info->Phones )) {
                                          foreach($account_info->Phones as $phone) {
                                              if($phone->Type == 'Office'){
                                                  echo "<i class='fa fa-phone-square'></i> ".$phone->Type." : ".$phone->Number.'<br>';
                                              }
                                              
                                          }
                                      }

                                       }
                                      ?>


                                      <?php
                                          //Mobile phone
                                       if(isset($user_info->mobile) AND !empty($user_info->mobile)){?>

                                          <i class="fa fa-2x fa-mobile"></i> Mobile: <?= $user_info->mobile; ?><br/>

                                       <?php }else{?>

                                        <?php   
                                            if(isset($account_info->Phones)) {
                                              foreach($account_info->Phones as $phone) {
                                                  if($phone->Type == 'Mobile'){
                                                      echo "<i class='fa fa-2x fa-mobile'></i> ".$phone->Type." : ".$phone->Number.'<br>';
                                                  }
                                                  
                                              }
                                          }
                                       }
                                      ?>


                                      <?php
                                          //FAX
                                         if(isset($account_info->Phones )) {
                                          foreach($account_info->Phones as $phone) {
                                              if($phone->Type == 'Fax'){
                                                  echo "<i class='fa fa-fax'></i> ".$phone->Type." : ".$phone->Number.'<br>';
                                              }
                                              
                                          }
                                      }?>
                                     
                                 </p>
                                  <p><i class="fa fa-envelope"></i> : <a href="mailto:<?php echo isset($user_info->email) ? $user_info->email  : $account_info->Emails[0]["Address"];?>">Email Me</a></p>
                                  </div>
                              </div>
                          </div>
                      </div>

                      <div class="col-md-9 col-sm-9">
                          <div class="col-md-12 col-md-offset-0 about-details">
                              <?php if (isset($user_info->about_agent) AND !empty($user_info->about_agent)) {?>
                              <h3>About us  </h3>
                            
                              <p><?php echo $user_info->about_agent ?></p>
                                
                              <hr>
                              <?php }?>
                              <h3>Contact us  </h3>
                              <div class="question-mess" ></div>  <br/>
                              <form action="<?php echo site_url('home/home/customer_questions'); ?>" method="POST" class="customer_questions" id="customer_questions" >
                                <div class="row">
                                    <div class="col-md-6">
                                      <div class="form-group">
                                        <label for="">First Name</label>
                                        <input type="text" name="first_name" value="<?= (isset($profile->first_name)) ? $profile->first_name : "" ; ?>" class="form-control" placeholder="First Name" required>
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="form-group">
                                        <label for="">Last Name</label>
                                        <input type="text" name="last_name" value="<?= (isset($profile->last_name)) ? $profile->last_name : "" ; ?>" class="form-control" placeholder="Last Name" required>
                                      </div>
                                    </div>
                                     <div class="col-md-6">
                                      <div class="form-group">
                                          <label for="">Email</label>
                                          <input type="email" name="email" value="<?= (isset($profile->email)) ? $profile->email : "" ; ?>" class="form-control" placeholder="Email" required <?= (isset($profile->email)) ? "readonly" : "" ; ?>>
                                      </div>
                                    </div>
                                     <div class="col-md-6">
                                      <div class="form-group">
                                          <label for="">Phone</label>
                                          <input type="phone" name="phone" value="<?= (isset($profile->phone)) ? $profile->phone : "" ; ?>" class="form-control phone_number" placeholder="Phone Number" required <?= (isset($profile->phone)) ? "readonly" : "" ; ?>>
                                      </div>
                                    </div>
                                    <div class="col-md-12">
                                      <div class="form-group">
                                        <label for="">Message</label>
                                        <textarea name="message" id="" value="" cols="30" rows="5" class="form-control" placeholder="Comments, Questions, Special Requests?" required></textarea>
                                      </div>
                                    </div>
                                </div>
                                  <button type="submit" class="btn btn-default btn-block submit-button submit-question-button">Submit</button>
                              </form>    
                          </div>
                      </div>

                    <?php

                        //$this->load->view($theme.'/pages/page_view/page-info-contact-mobile-view');

                    ?>

                  </div>
              </div>
          </section>
    </div>
