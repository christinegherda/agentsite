<?php
    //$this->load->view($theme.'/pages/advance_search_view/advance-search-page-form');  
    $this->load->model("search/search_model","search_model");
?> 

<?php //$this->load->view($theme.'/pages/findahome_view/search-map');?>
<section class="advance-search-area">
	<div class="navbar navigation-search">
		<div class="container">
			<ul class="search-type">
				<!-- <li class=""><a href="mapped-search"><i class="fa fa-map-marker"></i> Property List</a></li> -->
                <li class="active"><a href="advanced-search"><i class="fa fa-search"></i> Advanced Search</a></li>
                <!-- <li><a href="mapped-search"><i class="fa fa-map-marker"></i> Map Search</a></li> -->
			</ul>
		</div>
	</div>
</section>
<section class="property-main-content">
    <div class="container">
        <div class="row">
            <?php if( isset($_GET) AND !empty($_GET)) : ?>
            <div class="sorting">
                <div class="col-md-offset-3 col-md-10">
                    <div class="col-md-3 pull-right">
                        <a href="<?php echo site_url("home/home/save_search_properties?".http_build_query($_GET)); ?>" type="button" class="btn btn-default submit-button follow-search"><i class="fa fa-undo"></i> Save Search </a>
                    </div>
                </div>
            </div>
            <?php endif;?>
            <div class="col-md-12 col-ms-12">
                <div class="advance-search-form">
                    <!-- Nav tabs -->
                    <!-- <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#tab1">Advance Search</a></li>
                        <li class="pull-right active" ><a href="<?=base_url()?>mapped-search">Map Search</a></li>
                    </ul> -->
                    <!-- Tab panes -->

                    <div class="tab-content advance-search-tab">
                        <div role="tabpanel" class="tab-pane active" id="tab1">
	                        <form method="get" action="<?php echo base_url(); ?>advanced-search-results" id="advance_search_form">
	                        	<input type="hidden" name="Search" value="<?=($this->input->get('Search')) ? $this->input->get('Search') : ''; ?>">
                                <input type="hidden" name="offset" value="<?=($this->input->get('offset')) ? $this->input->get('offset') : ''; ?>">
                                <input type="hidden" name="page" value="<?=($this->input->get('page')) ? $this->input->get('page') : '1'; ?>">
                                <input type="hidden" name="totalrows" value="<?=($this->input->get('totalrows')) ? $this->input->get('totalrows') : ''; ?>">
                                <input type="hidden" name="current_segment" value="<?=($this->uri->segment(1)) ? $this->uri->segment(1) : ''; ?>">
                                <input type="hidden" name="search_id" value="<?=(isset($search_id) & !empty($search_id)) ? $search_id : ''; ?>">
	                            <div class="col-md-6">
	                                <div class="panel panel-default">
	                                    <div class="panel-heading">Address Information</div>
	                                    <div class="panel-body">
	                                        <div class="form-group">
	                                            <input type="text" class="form-control" name="StreetNumber" placeholder="Street Number">
	                                        </div>
	                                        <div class="form-group">
	                                            <input type="text" class="form-control" name="StreetName" placeholder="Street Name">
	                                        </div>
	                                        <div class="form-group">
	                                        	<?php
	                                        		foreach($search_fields as $sf) {
	                                        			if($sf->field_name == 'City') {
	                                        				$city_fields = $this->search_model->get_mls_data_search_fields_list_data($sf->id);
	                                        	?>
	                                           <!--  <input type="text" class="form-control" name="City" placeholder="City/Town Code"> -->
	                                        	<select class="form-control" name="City">
	                                        		<option value="">Select City</option>
	                                        		<?php 
	                                        			foreach($city_fields as $cf) {
	                                        				if($cf->field_value != 'Select One') {
	                                        		?>
	                                        		<option value="<?=$cf->field_value?>"><?=$cf->field_name?></option>
	                                        		<?php
	                                        				}
	                                        			}
	                                        		?>
	                                        	</select>
	                                        	<?php
	                                        			}
	                                        		}
	                                        	?>
	                                        </div>
	                                        <div class="form-group">
	                                            <?php
	                                        		foreach($search_fields as $sf) {
	                                        			if($sf->field_name == 'PostalCode') {
	                                        				$city_fields = $this->search_model->get_mls_data_search_fields_list_data($sf->id);
	                                        	?>
	                                           <!--  <input type="text" class="form-control" name="City" placeholder="City/Town Code"> -->
	                                        	<select class="form-control" name="PostalCode">
	                                        		<option value="">Select Zip Code</option>
	                                        		<?php 
	                                        			foreach($city_fields as $cf) {
	                                        				if($cf->field_value != '00000') {
	                                        		?>
	                                        		<option value="<?=$cf->field_value?>"><?=$cf->field_name?></option>
	                                        		<?php
	                                        				}
	                                        			}
	                                        		?>
	                                        	</select>
	                                        	<?php
	                                        			}
	                                        		}
	                                        	?>
	                                        </div>
							                <div class="form-group">
							                    <input type="text" class="form-control" name="SubdivisionName" placeholder="Subdivision">
							                </div>
	                                        <div class="form-group">
	                                    		<?php
	                                        		foreach($search_fields as $sf) {
	                                        			if($sf->field_name == 'CountyOrParish') {
	                                        				
	                                        				$search_fields_list = $this->search_model->get_mls_data_search_fields_list_data($sf->id);

	                                        	?>
	                                            <select name="CountyOrParish" class="form-control" placeholder="County">
	                                            	<option value="">Select One</option>
	                                            	<?php
		                                            	foreach($search_fields_list as $sfl) {		
		                                            		if($sfl->field_name != 'Select One') {
		                                            ?>
	                                            	<option value="<?=$sfl->field_value?>"><?=$sfl->field_name?></option>
	                                            	<?php
		                                            		}		                                            		
	                                            	?>
	                                            	
	                                            	<?php	                                            			
		                                            	}
	                                            	?>
	                                            </select>
	                                            <?php
	                                        			}
	                                        		}
	                                        	?>
	                                        </div>
	                                    </div>
	                                </div>
	                                <?php
	                                	$schoolShow = false;
	                                	foreach($search_fields as $sf) {
	                                    	if($sf->field_name == 'ElementarySchool' || $sf->field_name == 'MiddleOrJuniorSchool' || $sf->field_name == 'HighSchool') { 
	                                    		$schoolShow = true;
	                                    	}
	                                    }
	                                    if($schoolShow) {
	                                ?>
	                                <div class="panel panel-default">
	                                    <div class="panel-heading">School</div>
	                                    <div class="panel-body">
	                                    	<div class="form-group">
	                                    		<?php
	                                        		foreach($search_fields as $sf) {
	                                        			if($sf->field_name == 'ElementarySchool') {
	                                        				
	                                        				$search_fields_list = $this->search_model->get_mls_data_search_fields_list_data($sf->id);

	                                        	?>
	                                            <select name="ElementarySchool" class="form-control" placeholder="Elementary School">
	                                            	<option value="">Select One</option>
	                                            	<?php
		                                            	foreach($search_fields_list as $sfl) {		
		                                            		if($sfl->field_name != 'Select One') {
		                                            ?>
	                                            		<option value="<?=$sfl->field_value?>"><?=$sfl->field_name?></option>
	                                            	<?php
		                                            		}		                                            		
	                                            	?>
	                                            	
	                                            	<?php	                                            			
		                                            	}
	                                            	?>
	                                            </select>
	                                            <?php
	                                        			}
	                                        		}
	                                        	?>
	                                        </div>
	                                    	<div class="form-group">
	                                    		<?php
	                                        		foreach($search_fields as $sf) {
	                                        			if($sf->field_name == 'MiddleOrJuniorSchool') {
	                                        				
	                                        				$search_fields_list = $this->search_model->get_mls_data_search_fields_list_data($sf->id);

	                                        	?>
	                                            <select name="MiddleOrJuniorSchool" class="form-control" placeholder="Middle or Junior School">
	                                            	<option value="">Select One</option>
	                                            	<?php
		                                            	foreach($search_fields_list as $sfl) {		
		                                            		if($sfl->field_name != 'Select One') {
		                                            ?>
	                                            	<option value="<?=$sfl->field_value?>"><?=$sfl->field_name?></option>
	                                            	<?php
		                                            		}		                                            		
	                                            	?>
	                                            	
	                                            	<?php	                                            			
		                                            	}
	                                            	?>
	                                            </select>
	                                            <?php
	                                        			}
	                                        		}
	                                        	?>
	                                        </div>
	                                    	<div class="form-group">
	                                    		<?php
	                                        		foreach($search_fields as $sf) {
	                                        			if($sf->field_name == 'HighSchool') {
	                                        				
	                                        				$search_fields_list = $this->search_model->get_mls_data_search_fields_list_data($sf->id);

	                                        	?>
	                                            <select name="HighSchool" class="form-control" placeholder="High School">
	                                            	<option value="">Select One</option>
	                                            	<?php
		                                            	foreach($search_fields_list as $sfl) {		
		                                            		if($sfl->field_name != 'Select One') {
		                                            ?>
	                                            		<option value="<?=$sfl->field_value?>"><?=$sfl->field_name?></option>
	                                            	<?php
		                                            	}                                            		
	                                            	?>	                                            	
	                                            	<?php	                                            			
		                                            	}
	                                            	?>
	                                            </select>
	                                            <?php
	                                        			}
	                                        		}
	                                        	?>
	                                        </div>
	                                    </div>
	                                </div>
	                            	<?php } 
	                            	?>
	                            </div>
	                            <div class="col-md-6">
	                                <div class="panel panel-default">
	                                    <div class="panel-heading">General Property Description</div>
	                                    <div class="panel-body">
	                                        <div class="col-md-6">
	                                            <div class="form-group">
	                                            	<?php
		                                        		foreach($search_fields as $sf) {
		                                        			if($sf->field_name == 'PropertyClass') {
		                                        				
		                                        				$search_fields_list = $this->search_model->get_mls_data_search_fields_list_data($sf->id);
		                                        				
		                                        	?>
		                                            <select name="PropertyClass" class="form-control defaultval-property-type" placeholder="<?=$sf->field_label?>">
		                                            	<option value="">Select One</option>
		                                            	<?php
			                                            	foreach($search_fields_list as $sfl) {		
			                                            		if($sfl->field_name != 'Select One') {
			                                            			$decoded_mlscode = json_decode($sfl->field_applies_to_json);
			                                            ?>
		                                            		<option value="<?=$sfl->field_value?>" data-propertyclass-code='<?=$decoded_mlscode[0]?>' ><?=$sfl->field_name?></option>
		                                            	<?php
																}
															}
		                                            	?>		                                            		
		                                            	
		                                            </select>
		                                            <?php
		                                        			}
		                                        		}
		                                        	?>
	                                                <!-- <select class="form-control" name="PropertyType">
	                                                	<option value="">Select Property Type</option>
	                                                    <option value="A">Residential</option>
	                                                    <option value="B">Residential Rental</option>
	                                                    <option value="C">Land and Lots</option>
	                                                    <option value="D">Comm/Industry Sale</option>
	                                                    <option value="E">Comm/Industry Lease</option>
	                                                    <option value="F">Multiple Dwellings</option>
	                                                </select> -->
	                                            </div>
	                                        </div>
	                                        <div class="col-md-6">
	                                            <div class="form-group subtype-class" style="display:none" >
	                                            	<?php
	                                            		$field_lists = array("A","B","C","D","E","F");
		                                        		foreach($search_fields as $sf) {
		                                        			if($sf->field_name == 'PropertySubType') {                                        				
		                                        				$search_fields_list = $this->search_model->get_mls_data_search_fields_list_data($sf->id);
		                                        				//printA(json_decode($search_fields_list[0]->field_applies_to_json)); exit;
		                                        	?>
		                                        	<?php foreach($field_lists as $field) { ?>
			                                        	<!-- <div class="<?=$field;?>" style="display:none"> -->
				                                            <select style="display:none" name="PropertySubType" class="form-control <?=$field;?> set-property-sub" placeholder="<?=$sf->field_label?>">
				                                            	<option value="">-- Select --</option>
				                                            	<?php
					                                            	foreach($search_fields_list as $sfl) {		
					                                            		if($sfl->field_name != 'Select One') {
					                                            			$array_fields_lists = json_decode($sfl->field_applies_to_json);                                           			
					                                            ?>	
					                                            		<?php if( in_array($field, $array_fields_lists) ){ ?>
				                                            				<option value="<?=$sfl->field_value?>"><?=$sfl->field_name?></option>
				                                            			<?php } ?>
				                                            		
				                                            	<?php
					                                            		}
					                                            	}
				                                            	?>
				                                            </select>
			                                            <!-- </div> -->
		                                            <?php } ?>
		                                            <?php
		                                        			}
		                                        		}
		                                        	?>
	                                                <!-- <select class="form-control" name="PropertySubType">
	                                                    <option value="">Select Dwelling Type</option>
	                                                    <option value="Single Family - Detached">Single Family - Detached</option>
	                                                    <option value="Patio Home">Patio Home</option>
	                                                    <option value="Townhouse">Townhouse</option>
	                                                    <option value="Apartment Style/Flat">Apartment Style/Flat</option>
	                                                    <option value="Gemini/Twin Home">Gemini/Twin Home</option>
	                                                    <option value="Mfg/Mobile Housing">Mfg/Mobile Housing</option>
	                                                    <option value="Modular/Pre-Fab">Modular/Pre-Fab</option>
	                                                    <option value="Loft Style">Loft Style</option>
	                                                </select> -->
	                                            </div>
	                                        </div>
	                                        <div class="col-md-6">
	                                            <div class="form-group">
							                        <div class="input-group">
							                          <span class="input-group-addon">$</span>
	                                                  <input type="text" name="price_min" placeholder="Minimum Price" class="form-control input-number" maxlength="11">
							                        </div>
	                                            </div>
	                                        </div>
	                                        <div class="col-md-6">
	                                            <div class="form-group">
							                        <div class="input-group">
							                          <span class="input-group-addon">$</span>
	                                                  <input type="text" name="price_max" placeholder="Maximum Price" class="form-control input-number" maxlength="11">
							                        </div>
	                                            </div>
	                                        </div>
	                                        <!-- <div class="col-md-6">
	                                            <div class="form-group">
							                        <div class="input-group">
		                                                <input type="text" name="area_min" placeholder="Min. Lot Size" class="form-control input-number" maxlength="11">
								                        <span class="input-group-addon">Sq.Ft.</span>
							                        </div>
	                                            </div>
	                                        </div>
	                                        <div class="col-md-6">
	                                            <div class="form-group">
							                        <div class="input-group">
	                                                	<input type="text" name="area_max" placeholder="Max Lot Size" class="form-control input-number" maxlength="11">
								                        <span class="input-group-addon">Sq.Ft.</span>
							                        </div>
	                                            </div>
	                                        </div>	   -->    

	                                        <div class="col-md-6">
		                                        <div class="form-group">
		                                            <div class="input-group">
		                                                <input type="text" name="house_min" placeholder="Min. House Sq Ft" class="form-control input-number" maxlength="11">
								                        <span class="input-group-addon">Sq.Ft.</span>
		                                            </div>
		                                        </div>
		                                    </div>        
		                                    <div class="col-md-6">
		                                        <div class="form-group">
		                                            <div class="input-group">
		                                                <input type="text" name="house_max" placeholder="Max. House Sq Ft" class="form-control input-number" maxlength="11">
								                        <span class="input-group-addon">Sq.Ft.</span>
		                                            </div>
		                                        </div>
		                                    </div>
		                                                                     
	                                        <div class="col-md-6">
	                                            <div class="form-group">
	                                            	<input type="text" name="BedsTotal" placeholder="Bedrooms" class="form-control input-number" min="0" maxlength="4">
	                                            </div>
	                                        </div>
	                                        <div class="col-md-6">
	                                            <div class="form-group">
	                                            	<input type="text" name="BathsTotal" placeholder="Bathrooms" class="form-control input-number" min="0" maxlength="4">
	                                            </div>
	                                        </div>
	                                       
		                                                                                                    
                                    		
	                                        <div class="col-md-6">
	                                            <div class="form-group">
	                                            	<input type="text" name="BathsFull" placeholder="# of Interior Levels" class="form-control input-number" min="0" maxlength="4">
	                                            </div>
	                                        </div>
	                                        <?php
	                                        	if($account_info->MlsId == "20070913202326493241000000") {
	                                        ?>
	                                        <div class="col-md-6">
	                                            <div class="form-group">
	                                                <select class="form-control" name="Pool">
	                                                    <option value="">Pool</option>
	                                                    <option value="Both Private & Community">Both Private & Community</option>
	                                                    <option value="Community Only">Community Only</option>
	                                                    <option value="None">None</option>
	                                                    <option value="Private Only">Private Only</option>
	                                                </select>
	                                            </div>
	                                        </div>
	                                        <?php
	                                        	}
	                                        ?>
	                                        <div class="col-md-6">
	                                            <div class="form-group">
	                                            	<?php
		                                        		foreach($search_fields as $sf) {
		                                        			if($sf->field_name == 'MlsStatus') {
		                                        				
		                                        				$search_fields_list = $this->search_model->get_mls_data_search_fields_list_data($sf->id);

		                                        	?>
		                                            <select name="MlsStatus" class="form-control defaultval-mls-status" placeholder="<?=$sf->field_label?>">
		                                            	<option value="Active">Active</option>
		                                            	<?php
			                                            	foreach($search_fields_list as $sfl) {		
			                                            		if($sfl->field_name == 'Select One') {
			                                            ?>
		                                            	<option value=""><?=$sfl->field_name?></option>
		                                            	<?php
			                                            		}
			                                            		else {
		                                            	?>
		                                            	<option value="<?=$sfl->field_value?>"><?=$sfl->field_name?></option>
		                                            	<?php
		                                            			}
			                                            	}
		                                            	?>
		                                            </select>
		                                            <?php
		                                        			}
		                                        		}
		                                        	?>
	                                                <!-- <select class="form-control" name="MlsStatus">
	                                                    <option value="">MLS Status</option>
	                                                    <option value="Active">Active</option>
	                                                    <option value="CCBS (Contract Contingent on Buyer Sale)">CCBS (Contract Contingent on Buyer Sale)</option>
	                                                    <option value="UCB (Under Contract-Backups)">UCB (Under Contract-Backups)</option>
	                                                    <option value="Pending">Pending</option>
	                                                    <option value="Closed">Closed</option>
	                                                </select> -->
	                                            </div>
	                                        </div>
	                                        <div class="col-md-6">
	                                            <div class="form-group">
	                                                <input type="text" class="form-control input-number" name="YearBuilt" placeholder="Year Built" min="4" maxlength="4">
	                                            </div>
	                                        </div>
	                                    </div>
	                                </div>
	                                <div class="panel panel-default">
	                                    <div class="panel-heading">Visual Options</div>
	                                    <div class="panel-body">
	                                        <div class="col-md-6">
	                                            <label class="checkbox-inline">
	                                            	<input type="checkbox" name="VirtualToursCount" value="1"> Virtual Tour
	                                            </label>
	                                        </div>
	                                        <div class="col-md-6">
	                                            <label class="checkbox-inline">
	                                            	<input type="checkbox" name="VideosCount" value="1"> Videos
	                                            </label>
	                                        </div>
	                                    </div>
	                                </div>
	                                <!-- <div class="panel panel-default">
	                                    <div class="panel-heading">Area</div>
	                                    <div class="panel-body">
	                                    	<div class="form-group">
												<div class="input-group btn-custom-group">
												  <input type="text" class="form-control input-number" maxlength="6">
												  <span class="input-group-addon">
													  <div class="btn-group" role="group">
													    <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
													      <span class="selected-area">Select Area</span> <i class="caret"></i> 
													    </button>
													    <div class="dropdown-menu" id="area-select" aria-labelledby="">
													      <a class="dropdown-item" href="#">Acres</a>
													      <a class="dropdown-item" href="#">Square Feet</a>
													      <a class="dropdown-item" href="#">Square Meters</a>
													    </div>
													  </div>
												  </span>
												</div>
	                                    	</div>
	                                    </div>
	                                </div> -->
	                            </div>
	                            <?php
                                	if($account_info->MlsId == "20070913202326493241000000") {
                                ?>
	                            <div class="col-md-6">
	                                <div class="panel panel-default">
	                                    <div class="panel-heading">Roof</div>
	                                    <div class="panel-body">
	                                        <div class="col-md-6">
	                                            <label class="checkbox-inline">
	                                            	<input type="checkbox" name="Roof[]" value="All Tile">All Tile
	                                            </label>
	                                        </div>
	                                        <div class="col-md-6">
	                                            <label class="checkbox-inline">
	                                            	<input type="checkbox" name="Roof[]" value="Built-Up">Built-Up
	                                            </label>
	                                        </div>
	                                        <div class="col-md-6">
	                                            <label class="checkbox-inline">
	                                            	<input type="checkbox" name="Roof[]" value="Comp Shingle">Comp Shingle
	                                            </label>
	                                        </div>
	                                        <div class="col-md-6">
	                                            <label class="checkbox-inline">
	                                            	<input type="checkbox" name="Roof[]" value="Concrete">Concrete
	                                            </label>
	                                        </div>
	                                        <div class="col-md-6">
	                                            <label class="checkbox-inline">
	                                            	<input type="checkbox" name="Roof[]" value="Foam">Foam
	                                            </label>
	                                        </div>
	                                        <div class="col-md-6">
	                                            <label class="checkbox-inline">
	                                            	<input type="checkbox" name="Roof[]" value="Metal">Metal
	                                            </label>
	                                        </div>
	                                        <div class="col-md-6">
	                                            <label class="checkbox-inline">
	                                            	<input type="checkbox" name="Roof[]" value="Other (See Remarks)">Other (See Remarks)
	                                            </label>
	                                        </div>
	                                        <div class="col-md-6">
	                                            <label class="checkbox-inline">
	                                            	<input type="checkbox" name="Roof[]" value="Partial Tile">Partial Tile
	                                            </label>
	                                        </div>
	                                        <div class="col-md-6">
	                                            <label class="checkbox-inline">
	                                            	<input type="checkbox" name="Roof[]" value="Reflective Coating">Reflective Coating
	                                            </label>
	                                        </div>
	                                        <div class="col-md-6">
	                                            <label class="checkbox-inline">
	                                            	<input type="checkbox" name="Roof[]" value="Rock">Rock
	                                            </label>
	                                        </div>
	                                        <div class="col-md-6">
	                                            <label class="checkbox-inline">
	                                            	<input type="checkbox" name="Roof[]" value="Shake">Shake
	                                            </label>
	                                        </div>
	                                        <div class="col-md-6">
	                                            <label class="checkbox-inline">
	                                            	<input type="checkbox" name="Roof[]" value="Sub Tile Ventilation">Sub Tile Ventilation
	                                            </label>
	                                        </div>
	                                    </div>
	                                </div>
	                            </div>
	                            <?php
                                	}
                                ?>
	                            <div class="col-md-6">
	                                <div class="panel panel-default">
	                                    <div class="panel-heading">Parking Space</div>
	                                    <div class="panel-body">
	                                    	<div class="form-group">
	                                        	<input type="text" class="form-control input-number" name="GarageSpaces" placeholder="Garage Spaces" maxlength="4">
	                                        </div>
	                                        <div class="form-group">
	                                        	<input type="text" class="form-control input-number" name="CarportSpaces" placeholder="Carport Spaces" maxlength="4">
	                                        </div>
	                                        <div class="form-group">
	                                        	<input type="text" class="form-control input-number" name="Slab Parking Spaces" placeholder="Slab Parking Spaces" maxlength="4">
	                                        </div>
	                                    </div>
	                                </div>
	                            </div>                           
	                            <div class="row">
	                                <div class="col-md-12 text-right">
	                                    <button type="reset" class="btn btn-primary button-reset" style="background-color:#c50000">Clear</button>
	                                    <button type="button" class="btn btn-primary" id="submitFormSearch">Search</button>
	                                </div>
	                            </div>
	                        </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>