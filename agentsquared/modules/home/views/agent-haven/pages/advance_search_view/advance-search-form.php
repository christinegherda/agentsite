<form method="get" action="<?php echo base_url(); ?>search-results" id="advance_search_form">
    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading">Address Information</div>
            <div class="panel-body">
                <div class="form-group">
                    <input type="text" class="form-control" name="StreetNumber" placeholder="Street Number">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" name="StreetName" placeholder="Street Name">
                </div>
                <div class="form-group">
                	<?php
                		foreach($search_fields as $sf) {
                			if($sf->field_name == 'City') {
                				$city_fields = $this->search_model->get_mls_data_search_fields_list_data($sf->id);
                	?>
                   <!--  <input type="text" class="form-control" name="City" placeholder="City/Town Code"> -->
                	<select class="form-control" name="City">
                		<option value="">Select City</option>
                		<?php 
                    			foreach($city_fields as $cf) {
                    				if($cf->field_value != 'Select One') {
                		?>
                		<option value="<?=$cf->field_value?>"><?=$cf->field_name?></option>
                		<?php
                    				}
                    			}
                		?>
                	</select>
                	<?php
                			}
                		}
                	?> 
                </div>
                <div class="form-group">
                    <?php
                		foreach($search_fields as $sf) {
                			if($sf->field_name == 'PostalCode') {
                				$city_fields = $this->search_model->get_mls_data_search_fields_list_data($sf->id);
                	?>
                   
                	<select class="form-control" name="PostalCode">
                		<option value="">Select Zip Code</option>
                		<?php 
                    			foreach($city_fields as $cf) {
                    				if($cf->field_value != '00000') {
                		?>
                		<option value="<?=$cf->field_value?>"><?=$cf->field_name?></option>
                		<?php
                    				}
                    			}
                		?>
                	</select>
                	<?php
                			}
                		}
                	?>
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" name="SubdivisionName" placeholder="Subdivision">
                </div>
                <div class="form-group">
                    <?php
                        foreach($search_fields as $sf) {
                            if($sf->field_name == 'CountyOrParish') {
                                
                                $search_fields_list = $this->search_model->get_mls_data_search_fields_list_data($sf->id);

                    ?>
                    <select name="CountyOrParish" class="form-control" placeholder="County">
                        <?php
                                foreach($search_fields_list as $sfl) {      
                                    if($sfl->field_name == 'Select One') {
                        ?>
                        <option value=""><?=$sfl->field_name?></option>
                        <?php
                                    }
                                    else {
                        ?>
                        <option value="<?=$sfl->field_value?>"><?=$sfl->field_name?></option>
                        <?php
                                    }
                                }
                        ?>
                    </select>
                    <?php
                            }
                        }
                    ?>
                </div>
            </div>
        </div>
        <?php
            $schoolShow = false;
            foreach($search_fields as $sf) {
                if($sf->field_name == 'ElementarySchool' || $sf->field_name == 'MiddleOrJuniorSchool' || $sf->field_name == 'HighSchool') { 
                    $schoolShow = true;
                }
            }
            if($schoolShow) {
        ?>
        <div class="panel panel-default">
            <div class="panel-heading">School</div>
            <div class="panel-body">
                <div class="form-group">
                    <?php
                        foreach($search_fields as $sf) {
                            if($sf->field_name == 'ElementarySchool') {
                                
                                $search_fields_list = $this->search_model->get_mls_data_search_fields_list_data($sf->id);

                    ?>
                    <select name="ElementarySchool" class="form-control" placeholder="Elementary School">
                        <?php
                            foreach($search_fields_list as $sfl) {      
                                if($sfl->field_name == 'Select One') {
                        ?>
                        <option value=""><?=$sfl->field_name?></option>
                        <?php
                                }
                                else {
                        ?>
                        <option value="<?=$sfl->field_value?>"><?=$sfl->field_name?></option>
                        <?php
                                }
                            }
                        ?>
                    </select>
                    <?php
                            }
                        }
                    ?>
                </div>
                <div class="form-group">
                    <?php
                        foreach($search_fields as $sf) {
                            if($sf->field_name == 'MiddleOrJuniorSchool') {
                                
                                $search_fields_list = $this->search_model->get_mls_data_search_fields_list_data($sf->id);

                    ?>
                    <select name="MiddleOrJuniorSchool" class="form-control" placeholder="Middle or Junior School">
                        <?php
                            foreach($search_fields_list as $sfl) {      
                                if($sfl->field_name == 'Select One') {
                        ?>
                        <option value=""><?=$sfl->field_name?></option>
                        <?php
                                }
                                else {
                        ?>
                        <option value="<?=$sfl->field_value?>"><?=$sfl->field_name?></option>
                        <?php
                                }
                            }
                        ?>
                    </select>
                    <?php
                            }
                        }
                    ?>
                </div>
                <div class="form-group">
                    <?php
                        foreach($search_fields as $sf) {
                            if($sf->field_name == 'HighSchool') {
                                
                                $search_fields_list = $this->search_model->get_mls_data_search_fields_list_data($sf->id);

                    ?>
                    <select name="HighSchool" class="form-control" placeholder="High School">
                        <?php
                            foreach($search_fields_list as $sfl) {      
                                if($sfl->field_name == 'Select One') {
                        ?>
                        <option value=""><?=$sfl->field_name?></option>
                        <?php
                                }
                                else {
                        ?>
                        <option value="<?=$sfl->field_value?>"><?=$sfl->field_name?></option>
                        <?php
                                }
                            }
                        ?>
                    </select>
                    <?php
                            }
                        }
                    ?>
                </div>
            </div>
        </div>
        <?php
            }
        ?>
    </div><!-- -->
    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading">General Property Description</div>
            <div class="panel-body">
                <div class="col-md-6">
                    <div class="form-group">
                        <?php
                            foreach($search_fields as $sf) {
                                if($sf->field_name == 'PropertyClass') {
                                    
                                    $search_fields_list = $this->search_model->get_mls_data_search_fields_list_data($sf->id);

                        ?>
                        <select name="PropertyClass" class="form-control" placeholder="<?=$sf->field_label?>">
                            <?php
                                    foreach($search_fields_list as $sfl) {      
                                        if($sfl->field_name == 'Select One') {
                                ?>
                            <option value=""><?=$sfl->field_name?></option>
                            <?php
                                        }
                                        else {
                            ?>
                            <option value="<?=$sfl->field_value?>"><?=$sfl->field_name?></option>
                            <?php
                                        }
                                    }
                            ?>
                        </select>
                        <?php
                                }
                            }
                        ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <?php
                            foreach($search_fields as $sf) {
                                if($sf->field_name == 'PropertySubType') {
                                    
                                    $search_fields_list = $this->search_model->get_mls_data_search_fields_list_data($sf->id);

                        ?>
                        <select name="PropertySubType" class="form-control" placeholder="<?=$sf->field_label?>">
                            <?php
                                    foreach($search_fields_list as $sfl) {      
                                        if($sfl->field_name == 'Select One') {
                            ?>
                            <option value=""><?=$sfl->field_name?></option>
                            <?php
                                        }
                                        else {
                            ?>
                            <option value="<?=$sfl->field_value?>"><?=$sfl->field_name?></option>
                            <?php
                                        }
                                    }
                            ?>
                        </select>
                        <?php
                                }
                            }
                        ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon">$</span>
                            <input type="text" name="price_min" placeholder="Minimum Price" class="form-control input-number" maxlength="11">
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon">$</span>
                            <input type="text" name="price_max" placeholder="Maximum Price" class="form-control input-number" maxlength="11">
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="input-group">
                            <input type="text" name="area_min" placeholder="Min. Lot Size" class="form-control input-number" maxlength="11">
                            <span class="input-group-addon">Sq.Ft.</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="input-group">
                            <input type="text" name="area_max" placeholder="Max Lot Size" class="form-control input-number" maxlength="11">
                            <span class="input-group-addon">Sq.Ft.</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                    	<input type="text" name="BedsTotal" placeholder="Bedrooms" class="form-control">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                    	<input type="text" name="BathsTotal" placeholder="Bathrooms" class="form-control">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                    	<input type="text" name="BathsFull" placeholder="# of Interior Levels" class="form-control">
                    </div>
                </div>
                <?php
                    if($account_info->MlsId == "20070913202326493241000000") {
                ?>
                <div class="col-md-6">
                    <div class="form-group">
                        <select class="form-control" name="Pool">
                            <option value="">Pool</option>
                            <option value="Both Private & Community">Both Private & Community</option>
                            <option value="Community Only">Community Only</option>
                            <option value="None">None</option>
                            <option value="Private Only">Private Only</option>
                        </select>
                    </div>
                </div>
                <?php
                    }
                ?>
                <div class="col-md-6">
                    <div class="form-group">
                        <?php
                            foreach($search_fields as $sf) {
                                if($sf->field_name == 'MlsStatus') {
                                    
                                    $search_fields_list = $this->search_model->get_mls_data_search_fields_list_data($sf->id);

                        ?>
                        <select name="MlsStatus" class="form-control" placeholder="<?=$sf->field_label?>">
                            <?php
                                    foreach($search_fields_list as $sfl) {      
                                        if($sfl->field_name == 'Select One') {
                            ?>
                            <option value=""><?=$sfl->field_name?></option>
                            <?php
                                        }
                                        else {
                            ?>
                            <option value="<?=$sfl->field_value?>"><?=$sfl->field_name?></option>
                            <?php
                                        }
                                    }
                            ?>
                        </select>
                        <?php
                                }
                            }
                        ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <input type="text" class="form-control" name="YearBuilt" placeholder="Year Built">
                    </div>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">Visual Options</div>
            <div class="panel-body">
                <div class="col-md-6">
                    <label class="checkbox-inline">
                        <input type="checkbox" name="VirtualToursCount" value="1"> Virtual Tour
                    </label>
                </div>
                <div class="col-md-6">
                    <label class="checkbox-inline">
                        <input type="checkbox" name="VideosCount" value="1"> Videos
                    </label>
                </div>
            </div>
        </div>
    </div>
    <?php
        if($account_info->MlsId == "20070913202326493241000000") {
    ?>
    <hr>
    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading">Roof</div>
            <div class="panel-body">
                <div class="col-md-6">
                    <label class="checkbox-inline">
                    	<input type="checkbox" name="Roof[]" value="All Tile">All Tile
                    </label>
                </div>
                <div class="col-md-6">
                    <label class="checkbox-inline">
                    	<input type="checkbox" name="Roof[]" value="Built-Up">Built-Up
                    </label>
                </div>
                <div class="col-md-6">
                    <label class="checkbox-inline">
                    	<input type="checkbox" name="Roof[]" value="Comp Shingle">Comp Shingle
                    </label>
                </div>
                <div class="col-md-6">
                    <label class="checkbox-inline">
                    	<input type="checkbox" name="Roof[]" value="Concrete">Concrete
                    </label>
                </div>
                <div class="col-md-6">
                    <label class="checkbox-inline">
                    	<input type="checkbox" name="Roof[]" value="Foam">Foam
                    </label>
                </div>
                <div class="col-md-6">
                    <label class="checkbox-inline">
                    	<input type="checkbox" name="Roof[]" value="Metal">Metal
                    </label>
                </div>
                <div class="col-md-6">
                    <label class="checkbox-inline">
                    	<input type="checkbox" name="Roof[]" value="Other (See Remarks)">Other (See Remarks)
                    </label>
                </div>
                <div class="col-md-6">
                    <label class="checkbox-inline">
                    	<input type="checkbox" name="Roof[]" value="Partial Tile">Partial Tile
                    </label>
                </div>
                <div class="col-md-6">
                    <label class="checkbox-inline">
                    	<input type="checkbox" name="Roof[]" value="Reflective Coating">Reflective Coating
                    </label>
                </div>
                <div class="col-md-6">
                    <label class="checkbox-inline">
                    	<input type="checkbox" name="Roof[]" value="Rock">Rock
                    </label>
                </div>
                <div class="col-md-6">
                    <label class="checkbox-inline">
                    	<input type="checkbox" name="Roof[]" value="Shake">Shake
                    </label>
                </div>
                <div class="col-md-6">
                    <label class="checkbox-inline">
                    	<input type="checkbox" name="Roof[]" value="Sub Tile Ventilation">Sub Tile Ventilation
                    </label>
                </div>
            </div>
        </div>
    </div>
    <?php
        }
    ?>
    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading">Parking Space</div>
            <div class="panel-body">
            	<div class="form-group">
                	<input type="text" class="form-control" name="GarageSpaces" placeholder="Garage Spaces">
                </div>
                <div class="form-group">
                	<input type="text" class="form-control" name="CarportSpaces" placeholder="Carport Spaces">
                </div>
                <div class="form-group">
                	<input type="text" class="form-control" name="Slab Parking Spaces" placeholder="Slab Parking Spaces">
                </div>
            </div>
        </div>
    </div>
    <hr>	                            
    <div class="row">
        <div class="col-md-12 text-right">
            <button type="reset" class="btn btn-primary" style="background-color:#c50000">Clear</button>
            <button type="button" class="btn btn-primary" id="submitFormSearch">Search</button>
        </div>
    </div>
</form>