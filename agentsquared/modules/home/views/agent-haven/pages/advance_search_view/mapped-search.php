<?php
    $this->load->model("search/search_model","search_model");
?>
<?php if ($browser == "Internet Explorer"): ?>
    <div class="browser-issue">
       <div class="container">
            <h3>You are using an outdated browser that is no longer supported by Microsoft.</h3>
            <p>
               Please use <a href="https://www.microsoft.com/en-us/windows/microsoft-edge"><b>Microsoft Edge</b></a>, <a href="https://www.google.com/chrome/browser/desktop/index.html"><b>Google Chrome</b></a> or <a href="https://www.mozilla.org/en-US/firefox/new/"><b>Mozilla Firefox</b></a> in order to use this feature.
            </p>
       </div> 
    </div>
<?php else: ?>
    <div class="advance-search-content">
        <!-- search bar -->
        <section class="advance-search-area">
            <div class="navbar navigation-search">
                <div class="container">
                    <form action="" method="GET" class="sbt-search-from" id="sbt-search">
                    <div class="col-md-3 col-sm-2 mobile-ipad-padding-0">
                        <ul class="search-type">
                            <li>
                                <div class="form-search">
                                    <div id="data-city" data-city='<?php echo $city_list?>'></div>
                                    <input id="filter_search_field" name="Search" value='<?=(isset($_GET["Search"])) ? $_GET["Search"] : "";?>' placeholder="Address, City, State, or ZIP" type="text"  class="search_field_area"></input>
                                    <input type="hidden" name="order" value="newest" id="order_by">
                                    <input type="hidden" value='<?=(isset($_GET['polygonval']) && !empty($_GET['polygonval'])) ? json_encode($_GET['polygonval']) : '';?>' id="main_polygon_val" name="polygonval">
                                    <div class="visible-xs">
                                        <button type="submit">
                                            <span class="fa fa-search"></span> 
                                        </button>
                                        <div class="search-button">
                                            <button class="submit-float" type="submit">
                                            Search
                                            </button>
                                        </div>
                                    </div>
                                    <div class="search-suggestion">
                                            <!--for favorites-->
                                            <?php if(isset($favorites) && !empty($favorites)) : ?>
                                            <ul>
                                            <?php
                                                foreach ($favorites as $key) : 
                                            ?>
                                                <li><a href="<?php echo base_url()."other-property-details/".$key->property_id;?>" target="_blank"><i class="fa fa-heart"></i> <span>My Favorites - <?=$key->property_id;?></span></a></li>
                                                <?php endforeach;?> 
                                            </ul>
                                            <?php endif;?>
                                            <!--for favorites-->
                                            <?php if(isset($save_searches) && !empty($save_searches)) : ?>
                                            <ul>
                                            <?php
                                                foreach ($save_searches as $key) : 
                                            ?>
                                                <li><a href="<?php echo base_url()."search-results?".$key->url;?>"><i class="fa fa-save"></i> 
                                                    <span>My Save Search <?=(isset($key->name) && !empty($key->name)) ? "- <b>".$key->name : ""?> </b></span> </a>
                                                </li>
                                                <?php endforeach;?> 
                                            </ul>
                                            <?php endif;?>
                                            <!--for cookies-->
                                            <?php if(isset($_COOKIE["keywords_".$account_info->MlsId])) : ?>
                                            <ul>
                                            <?php
                                                foreach ($_COOKIE["keywords_".$account_info->MlsId] as $key => $value) : 
                                            ?>
                                                <li><a href="" class="keywords-history"><i class="fa fa-map-marker"></i> <span><?php echo $value;?></span></a></li>
                                                <?php endforeach;?> 
                                            </ul>
                                            <?php endif;?>
                                    </div>
                                </div>
                            </li>
                            <!-- <li class="active"><a href="mapped-search"><i class="fa fa-map-marker"></i> Map Search</a></li> -->
                        </ul>
                    </div>
                    <div class="col-md-7 col-sm-9 mobile-padding-0">
                        <ul class="nav navbar-nav search-options">
                            <li class="dropdown default-option property-type-dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                     <span class="search-label">Property Type</span><span class="fa fa-chevron-down"></span>
                                </a>
                                <?php if($account_info->MlsId == "20040823195642954262000000") { ?>
                                    <ul class="dropdown-menu ">  
                                        <!---AKMLS ONLY -->               
                                        <?php
                                            $flag = FALSE;
                                            if(!empty($property_types)){
                                                foreach ($property_types as $type) {
                                        ?>
                                            <li class="has-sub-dropdown">
                                                <div class="checkbox">
                                                    <input type="checkbox" class="property-parent" name="PropertyType[]" <?php echo ($flag) ? 'checked="checked"' : "";?> id="<?=$type["MlsName"]?>" value="<?=$type["MlsCode"]?>" />
                                                    <label for="<?=$type["MlsName"]?>"><?=$type["MlsName"]?></label>
                                                </div>
                                            </li>
                                        <?php
                                                }
                                            }else{
                                        ?>

                                        <?php ?>

                                            <li class="no-property-type">
                                                <p>No property type</p>
                                            </li>
                                        <?php 
                                            }
                                        ?>      
                                        <!---END AKMLS ONLY -->  

                                                                            
                                    </ul>
                                <?php } else { ?>

                                <ul class="dropdown-menu "> 

                                      <?php
                                        $flag = FALSE;
                                        $sub_flag = FALSE;
                                        $countX=0;
                                        $x=0;
                                        foreach($search_fields as $sf) {

                                            if($sf->field_name == 'PropertyClass') {
                                                $x=1;
                                                $search_fields_list = $this->search_model->get_mls_data_search_fields_list_data($sf->id);
                                                if (!empty($search_fields_list)) {
                                    ?>
                                                <?php
                                                    foreach($search_fields_list as $sfl) {
                                                           
                                                        if($sfl->field_name != 'Select One') {
                                                            $decoded_mlscode = json_decode($sfl->field_applies_to_json);
                                                            if(isset($_GET['PropertyClass']))
                                                            {
                                                                $flag = (in_array($sfl->field_value, $_GET['PropertyClass'])) ? TRUE : FALSE ;
                                                            }

                                                            if(isset($gsf_standard['PropertyClass']) && !empty($gsf_standard['PropertyClass'])){
                                                                $flag = (in_array($sfl->field_value, $gsf_standard['PropertyClass'])) ? TRUE : FALSE ;
                                                            }
                                                            
                                                ?>
                                                    <li class="has-sub-dropdown">
                                                        <div class="checkbox">
                                                            <input type="checkbox" class="property-parent" name="PropertyClass[]" <?php echo ($flag) ? 'checked="checked"' : "";?> id="<?=$sfl->field_name?>" value="<?=$sfl->field_value?>" />
                                                            <label for="<?=$sfl->field_name?>"><?=$sfl->field_name?></label>
                                                        </div>
                                                        <?php if(!empty($decoded_mlscode) && $decoded_mlscode != NULL) { ?>
                                                            <ul class="is-sub-dropdown" <?php echo ($flag) ? 'style="display:block"' : '' ;?>>
                                                                <?php
                                                                    $field_lists = $decoded_mlscode;
                                                                    foreach($search_fields as $sf2) {
                                                                        if($sf2->field_name == 'PropertySubType') {                                                  
                                                                            $sub_list = $this->search_model->get_mls_data_search_fields_list_data($sf2->id);
                                                                ?>
                                                                    <?php
                                                                        foreach($sub_list as $sfl2) {
                                                                            $countX++;   
                                                                            $countY=0;
                                                                            if($sfl2->field_name != 'Select One') {
                                                                                $array_fields_lists = json_decode($sfl2->field_applies_to_json);
                                                                                if(isset($_GET['PropertySubType']))
                                                                                {
                                                                                    $sub_flag = (in_array($sfl2->field_value, $_GET['PropertySubType'])) ? TRUE : FALSE ;
                                                                                }
                                                                    ?>                                                                    
                                                                                <?php foreach($array_fields_lists as $list_code) : 
                                                                                    $countY++;  
                                                                                    if(in_array($list_code, $field_lists) && $countY==1) : 
                                                                                ?>
                                                                                        <!-- No SubType for Alaska Multiple Listing Service, Inc. -->
                                                                                        <?php if($account_info->MlsId != "20040823195642954262000000") { ?>
                                                                                        <li>
                                                                                            <div class="checkbox">
                                                                                                <input type="checkbox" <?php echo ($sub_flag) ? 'checked="checked"' : "";?> name="PropertySubType[]" class="sub-property-item" value="<?=$sfl2->field_value?>" id="<?=$countX?>"/>
                                                                                                <label for="<?=$countX?>"><?=$sfl2->field_name?></label>
                                                                                            </div>
                                                                                        </li>
                                                                                        <?php } ?>
                                                                                    <?php endif;?>
                                                                                <?php endforeach; ?>
                                                                    <?php   } 
                                                                        }
                                                                      }
                                                                    } ?>                                                        
                                                            </ul>
                                                        <?php } ?>
                                                    </li>
                                                <?php
                                                        }
                                                    } break;
                                                } ?> 

                                    <?php
                                            }
                                        
                                        }
                                    ?>

                                    <?php if (empty($x)): ?>
                                        <li class="no-property-type">
                                            <p>No property type</p>
                                        </li>
                                    <?php endif ?>
                                    
                                </ul>
                                <?php } ?>
                            </li>
                            <li class="dropdown default-option price-dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    <span class="search-label"><span class="search-label-text">Price</span><span class="search-label-icon"><i class="fa fa-dollar"></i></span> <small class="price-range">(</span><span class="min-price"></span> - <span class="max-price"></span>)</small><span class="fa fa-chevron-down"></a>
                                <div class="dropdown-menu">
                                    <div class="col-md-12">
                                        <label>From</label>
                                        <input type="text" class="form-control" name="price_min" id="price_min" autocomplete="off" <?php if(isset($_GET['price_min']) && $_GET['price_min']) { echo "value='".$_GET['price_min']."'"; } ?> maxlength="12" placeholder="">
                                        
                                        <label>To</label>
                                        <input type="text" class="form-control" name="price_max" id="price_max" autocomplete="off" <?php if(isset($_GET['price_max']) && $_GET['price_max']) { echo "value='".$_GET['price_max']."'"; } ?> maxlength="12" placeholder="">    
                                    </div>
                                </div>
                            </li>
                            <li class="dropdown default-option bed-dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    <span id="BedsTotal_label"><?php if(isset($_GET['BedsTotal']) && $_GET['BedsTotal'] ) { echo $_GET['BedsTotal']."+"; } ?></span><span class="search-label"><span class="search-label-text">Beds</span><span class="search-label-icon"><i class="fa fa-bed"></i></span></span><span class="fa fa-chevron-down"></span>
                                </a>
                                <input type="hidden" name="BedsTotal" id="BedsTotal_value" value="<?php if(isset($_GET['BedsTotal']) && $_GET['BedsTotal'] ) { echo $_GET['BedsTotal']; } ?>">
                                <ul class="dropdown-menu">
                                    <div class="invisible-xs">
                                        <li><a href="javascript:void(0);" onclick="setBedValue('BedsTotal', 'Any')">Any</a></li>
                                        <li><a href="javascript:void(0);" onclick="setBedValue('BedsTotal', '1')">1+</a></li>
                                        <li><a href="javascript:void(0);" onclick="setBedValue('BedsTotal', '2')">2+</a></li>
                                        <li><a href="javascript:void(0);" onclick="setBedValue('BedsTotal', '3')">3+</a></li>
                                        <li><a href="javascript:void(0);" onclick="setBedValue('BedsTotal', '4')">4+</a></li>
                                        <li><a href="javascript:void(0);" onclick="setBedValue('BedsTotal', '5')">5+</a></li>
                                        <li><a href="javascript:void(0);" onclick="setBedValue('BedsTotal', '6')">6+</a></li>
                                        <li><a href="javascript:void(0);" onclick="setBedValue('BedsTotal', '7')">7+</a></li>
                                        <li><a href="javascript:void(0);" onclick="setBedValue('BedsTotal', '8')">8+</a></li>
                                    </div>
                                    <div class="visible-xs col-xs-3 mobile-ipad-padding-0">
                                        <select name="" id="" class="form-control beds-mobile" onchange="setBedVal();">
                                            <option value="Any">Any</option>
                                            <option value="1">1+</option>
                                            <option value="2">2+</option>
                                            <option value="3">3+</option>
                                            <option value="4">4+</option>
                                            <option value="5">5+</option>
                                            <option value="6">6+</option>
                                            <option value="7">7+</option>
                                            <option value="8">8+</option>
                                        </select>
                                    </div>
                                </ul>
                            </li>
                            <li class="dropdown more-dropdown">
                                <a href="#" class="dropdown-toggle more-hasdropdown" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                   <span class="search-label">More</span> <span class="fa fa-chevron-down"></span>
                                </a>
                                <div class="dropdown-menu more-isdropdown">
                                        <div class="basic-more-options">
                                            <div class="row">
                                                <div class="col-md-12 mb-5px">
                                                    <div class="bath-input">
                                                        <label class="col-md-3 col-xs-12 no-padding">Bath</label>
                                                        <select class="form-control" name="BathsTotal">
                                                            <option value="">Any</option>
                                                            <?php for($i=0;$i<=6;$i++) { ?>
                                                                <option value="<?=$i?>" <?php echo (isset($_GET["BathsTotal"]) && $_GET["BathsTotal"] == $i) ? 'selected="selected"' : "";?>><?=$i?>+</option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-12 mb-5px">
                                                    <div class="bath-input">
                                                        <label class="col-md-4 col-xs-12 no-padding">Property Size</label>
                                                        <span class="col-md-3 col-xs-5 no-padding">
                                                            <input type="text" name="house_min" class="form-control input-number" maxlength="11" value="<?=($this->input->get('house_min')) ? $this->input->get('house_min') : "";?>" placeholder="Min">
                                                        </span>
                                                        <span class="col-md-1 col-xs-2 no-padding text-center">
                                                            -
                                                        </span>
                                                        <span class="col-md-4 col-xs-5 no-padding">
                                                            <input type="text" name="house_max" class="form-control input-number" maxlength="11" value="<?=($this->input->get('house_max')) ? $this->input->get('house_max') : "";?>" placeholder="Max">
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="col-md-12 mb-5px clearfix">
                                                     <label class="col-md-3 no-padding">Year Built</label>
                                                     <span class="col-md-9 no-padding">
                                                         <input type="text" class="form-control input-number" value="<?=($this->input->get('YearBuilt')) ? $this->input->get('YearBuilt') : "";?>" name="YearBuilt" min="0" maxlength="4">
                                                     </span>
                                                </div>
                                                <!-- <div class="col-md-12 mb-5px listing-status">
                                                     <label for="">Listing Status </label>
                                                     <?php
                                                        $x=1;
                                                        foreach($search_fields as $sf) {
                                                            if($sf->field_name == 'MlsStatus' && $x==1) {
                                                                $status_fields_list = $this->search_model->get_mls_data_search_fields_list_data($sf->id);
                                                    ?>
                                                        <select class="form-control listing-status" name="MlsStatus[]" multiple>
                                                            <?php
                                                                printA($_GET);
                                                                foreach($status_fields_list as $sfl) {
                                                                    if($sfl->field_name != 'Select One') { ?>
                                                                        <option value="<?=$sfl->field_value?>" 
                                                                            <?php 
                                                                                //echo (isset($_GET["MlsStatus"]) && $_GET["MlsStatus"] == $sfl->field_value) ? 'selected="selected"' : "";
                                                                                if(isset($_GET["MlsStatus"]) && !empty($_GET["MlsStatus"])) {
                                                                                    /*if(in_array($sfl->field_value, $_GET["MlsStatus"])) {
                                                                                        echo "selected:'selected'";
                                                                                    }*/
                                                                                    foreach($_GET["MlsStatus"] as $status) {
                                                                                        if(trim($status) == trim($sfl->field_value)) {
                                                                                            echo "selected";
                                                                                        } else {
                                                                                            continue;
                                                                                        }
                                                                                    }
                                                                                }
                                                                            ?>
                                                                        >
                                                                            <?=$sfl->field_name?>
                                                                        </option>
                                                            <?php
                                                                    } 
                                                                }
                                                            ?>
                                                        </select>
                                                    <?php  $x=0;
                                                            }

                                                        }
                                                    ?>     
                                                </div> -->
                                                <div class="col-md-12 mb-5px listing-status">
                                                     <label for="">Listing Status </label>
                                                    
                                                        <select class="form-control listing-status" name="MlsStatus[]" multiple>
                                                            <?php
                                                                foreach($mlsStatus as $sfl) {
                                                                    if($sfl["Name"] != 'Select One') { ?>
                                                                        <option value="<?=$sfl["Value"]?>" 
                                                                            <?php 
                                                                                //echo (isset($_GET["MlsStatus"]) && $_GET["MlsStatus"] == $sfl->field_value) ? 'selected="selected"' : "";
                                                                                if(isset($_GET["MlsStatus"]) && !empty($_GET["MlsStatus"])) {
                                                                                    /*if(in_array($sfl->field_value, $_GET["MlsStatus"])) {
                                                                                        echo "selected:'selected'";
                                                                                    }*/
                                                                                    foreach($_GET["MlsStatus"] as $status) {
                                                                                        if(trim($status) == trim($sfl["Value"])) {
                                                                                            echo "selected";
                                                                                        } else {
                                                                                            continue;
                                                                                        }
                                                                                    }
                                                                                }
                                                                            ?>
                                                                        >
                                                                            <?=$sfl["Name"]?>
                                                                        </option>
                                                            <?php
                                                                    } 
                                                                }
                                                            ?>
                                                        </select>
                                                        
                                                </div>
                                            </div>
                                        </div>
                                        <h3>General Property Description</h3>
                                        <div class="form-group">
                                            <label for=""># of Interior Levels</label>
                                            <select class="form-control" name="InteriorLevels">
                                                <option value="0">0</option>
                                                <?php for($x=1;$x<=10;$x++) { ?>
                                                    <option value="<?=$x?>" <?=($this->input->get('InteriorLevels') && ($this->input->get('InteriorLevels') == $x)) ? "selected" : ""; ?>><?=$x?></option>
                                                <?php } ?>
                                            </select>                                                                                    
                                        </div>
                                        <?php if($account_info->MlsId == "20070913202326493241000000") { ?>
                                        <div class="form-group">
                                            <label for="">Pool</label>
                                            <select class="form-control" name="Pool">
                                                <option value="">Select One</option>
                                                <option value="Both Private & Community"  <?=($this->input->get('Pool') && ($this->input->get('Pool') == "Both Private & Community")) ? "selected" : ""; ?>>Both Private & Community</option>
                                                <option value="Community Only" <?=($this->input->get('Pool') && ($this->input->get('Pool') == "Community Only")) ? "selected" : ""; ?>>Community Only</option>
                                                <option value="None" <?=($this->input->get('Pool') && ($this->input->get('Pool') == "None")) ? "selected" : ""; ?>>None</option>
                                                <option value="Private Only" <?=($this->input->get('Pool') && ($this->input->get('Pool') == "Private Only")) ? "selected" : ""; ?>>Private Only</option>
                                                <option value="Private pools with or without community pools" <?=($this->input->get('Pool') && ($this->input->get('Pool') == "Private pools with or without community pools")) ? "selected" : ""; ?>>Private pools with or without community pools</option>
                                            </select>
                                        </div>
                                        <?php } ?>

                                        <?php if($account_info->MlsId == "20130226165731231246000000") {   ?>

                                        <div class="form-group">
                                            <label for="">Pool</label>
                                            <select class="form-control" name="Pools">
                                                <option value="">Select One</option>
                                                <option value="Pool" <?=($this->input->get('PoolB') && ($this->input->get('PoolB') == "Pool Bath")) ? "selected" : ""; ?>>Pool</option>
                                                <option value="Pool - Private"  <?=($this->input->get('PoolB') && ($this->input->get('PoolB') == "Room Pool")) ? "selected" : ""; ?>>Pool - Private</option>
                                                <option value="Pool-Private or Community" <?=($this->input->get('PoolB') && ($this->input->get('PoolB') == "Pool Bath")) ? "selected" : ""; ?>>Pool-Private or Community</option>
                                                <option value="Pool - Community"  <?=($this->input->get('PoolB') && ($this->input->get('PoolB') == "Community Pool")) ? "selected" : ""; ?>>Pool - Community</option>
                                            </select>
                                        </div> 
                                        <?php } ?>

                                        <h3>Address Information</h3>
                                        <div class="form-group">
                                            <label for="">Street Number</label>
                                            <input type="text" class="form-control" name="StreetNumber">
                                        </div>
                                        <div class="form-group">
                                            <label for="">Street Name</label>
                                            <input type="text" class="form-control" name="StreetName">
                                        </div>
                                        <?php $x=1;
                                            $countySelected = FALSE;
                                            foreach($search_fields as $sf) {
                                                if($sf->field_name == 'CountyOrParish' && $x==1) {
                                                    $search_fields_list = $this->search_model->get_mls_data_search_fields_list_data($sf->id);
                                                    if (!empty($search_fields_list)) {
                                        ?>
                                        <div class="form-group">
                                            <label for="">County</label>
                                            <select name="CountyOrParish[]" class="form-control city-field" placeholder="County" multiple >
                                                <option value="">Please Select</option>
                                                <?php
                                                    foreach($search_fields_list as $sfl) {      
                                                        if($sfl->field_name != 'Select One') {
                                                          if( isset($_GET["County"]) && $_GET["County"] == $sfl->field_value ){
                                                              $countySelected = TRUE;
                                                          }

                                                          if(isset($gsf_standard['CountyOrParish']) && !empty($gsf_standard['CountyOrParish'])){
                                                              $countySelected = (in_array($sfl->field_value, str_replace("_", " ", $gsf_standard['CountyOrParish']))) ? TRUE : FALSE ;
                                                            }
                                                ?>
                                                <option value="<?=$sfl->field_value?>" <?php echo ($countySelected) ? 'selected="selected"' : "";?> ><?=$sfl->field_name?></option>
                                                <?php
                                                        } 
                                                    }                                                          
                                                ?>
                                            </select>
                                        </div>
                                        <?php       }$x=0;
                                                }
                                            }
                                        ?>
                                        <?php $x=1;
                                                foreach($search_fields as $sf) {
                                                    if($sf->field_name == 'City' && $x==1) {
                                                        $city_fields = $this->search_model->get_mls_data_search_fields_list_data($sf->id);
                                                        if (!empty($city_fields)) {
                                            ?>
                                        <div class="form-group">
                                            <label for="">City</label>
                                               <!--  <input type="text" class="form-control" name="City" placeholder="City/Town Code"> -->
                                                <select class="form-control city-field" name="City[]" multiple >
                                                    <option value="">Please Select</option>
                                                    <?php
                                                        $citySelected = FALSE; 
                                                        foreach($city_fields as $cf) {
                                                            if($cf->field_value != 'Select One') {
                                                                if( isset($_GET["City"]) && $_GET["City"] == $cf->field_value ){
                                                                    $citySelected = TRUE;
                                                                }

                                                                if(isset($gsf_standard['City']) && !empty($gsf_standard['City'])){
                                                                  $citySelected = (in_array($cf->field_value, str_replace("_", " ", $gsf_standard['City']))) ? TRUE : FALSE ;
                                                                }
                                                    ?>
                                                    <option value="<?=$cf->field_value?>" <?php echo ($citySelected) ? 'selected="selected"' : "";?> ><?=$cf->field_name?></option>
                                                    <?php
                                                            }
                                                        }
                                                    ?>
                                                </select>
                                        </div>
                                        <?php       } $x=0;
                                                }
                                            }
                                        ?>
                                        <div class="form-group subdivision-form">
                                            <label for="">Subdivision</label>
                                            <input type="text" class="form-control" value="<?=($this->input->get('SubdivisionName')) ? $this->input->get('SubdivisionName') : "";?>" name="SubdivisionName" placeholder="Subdivision">
                                        </div>
                                        <?php $x=1;
                                                 $zipSelected = FALSE;
                                                foreach($search_fields as $sf) {
                                                    if($sf->field_name == 'PostalCode' && $x==1 ) {
                                                        $city_fields = $this->search_model->get_mls_data_search_fields_list_data($sf->id);
                                                        if (!empty($city_fields)) {
                                            ?>
                                        <div class="form-group">
                                            <label for="">Zip Code</label>
                                               <!--  <input type="text" class="form-control" name="City" placeholder="City/Town Code"> -->
                                                <select class="form-control city-field" name="PostalCode[]" multiple>
                                                    <option value="">Please Select</option>
                                                    <?php 
                                                        foreach($city_fields as $cf) {
                                                            if($cf->field_value != '00000') {

                                                              if( isset($_GET["PostalCode"]) && $_GET["PostalCode"] == $cf->field_value ){
                                                                $zipSelected = TRUE;
                                                              }

                                                              if(isset($gsf_standard['PostalCode']) && !empty($gsf_standard['PostalCode'])){
                                                                $zipSelected = (in_array($cf->field_value, $gsf_standard['PostalCode'])) ? TRUE : FALSE ;
                                                              }
                                                    ?>
                                                    <option value="<?=$cf->field_value?>" <?php echo ($zipSelected) ? 'selected="selected"' : "";?> ><?=$cf->field_name?></option>
                                                    <?php
                                                            }
                                                        } 
                                                    ?>
                                                </select>
                                        </div>
                                        <?php
                                                    } $x=0;
                                                } 
                                            }
                                        ?>
                                        <?php
                                            foreach($search_fields as $sf) {
                                                if($sf->field_name == 'ElementarySchool') {
                                                    $search_fields_list_elementary = $this->search_model->get_mls_data_search_fields_list_data($sf->id);
                                                } elseif ($sf->field_name == 'MiddleOrJuniorSchool') {
                                                    $search_fields_list_middlejunior = $this->search_model->get_mls_data_search_fields_list_data($sf->id);
                                                } elseif ($sf->field_name == 'HighSchool') {
                                                    $search_fields_list_highschool = $this->search_model->get_mls_data_search_fields_list_data($sf->id);
                                                }
                                            }
                                            if (!empty($search_fields_list_elementary) || !empty($search_fields_list_middlejunior) || !empty($search_fields_list_highschool)) { ?>
                                        <h3>School</h3>
                                        <?php  } ?>
                                        <?php
                                            $x=1;
                                            foreach($search_fields as $sf) {
                                                if($sf->field_name == 'ElementarySchool' && $x==1 ) {
                                                    
                                                    $search_fields_list = $this->search_model->get_mls_data_search_fields_list_data($sf->id);

                                                    if (!empty($search_fields_list)) {

                                        ?>
                                        <div class="form-group">
                                            <label for="">Elementary School</label>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <select name="ElementarySchool" id="" class="form-control">
                                                        <option value="">Select One</option>
                                                        <?php
                                                            foreach($search_fields_list as $sfl) {      
                                                                if($sfl->field_name != 'Select One') {
                                                        ?>
                                                            <option value="<?=$sfl->field_value?>" <?php echo (isset($_GET["ElementarySchool"]) && $_GET["ElementarySchool"] == $sfl->field_value) ? 'selected="selected"' : "";?> ><?=$sfl->field_name?></option>
                                                        <?php
                                                                }
                                                            }                                                         
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <?php 
                                                    }
                                                    $x=0;
                                                }
                                            }
                                        ?>
                                        <?php $x=1;
                                            foreach($search_fields as $sf) {
                                                if($sf->field_name == 'MiddleOrJuniorSchool' && $x==1 ) {
                                                    
                                                    $search_fields_list = $this->search_model->get_mls_data_search_fields_list_data($sf->id);

                                                    if (!empty($search_fields_list)) {

                                        ?>
                                        <div class="form-group">
                                            <label for="">Middle Or Junior School</label>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <select name="MiddleOrJuniorSchool" id="" class="form-control">
                                                        <option value="">Select One</option>
                                                        <?php
                                                            foreach($search_fields_list as $sfl) {      
                                                                if($sfl->field_name != 'Select One') {
                                                        ?>
                                                            <option value="<?=$sfl->field_value?>" <?php echo (isset($_GET["MiddleOrJuniorSchool"]) && $_GET["MiddleOrJuniorSchool"] == $sfl->field_value) ? 'selected="selected"' : "";?> ><?=$sfl->field_name?></option>
                                                        <?php
                                                                }
                                                            }                                                       
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <?php   
                                                    }
                                                    $x=0;

                                                }
                                            }
                                        ?>
                                        <?php   $x=1;
                                            foreach($search_fields as $sf) {
                                                if($sf->field_name == 'HighSchool' && $x==1) {
                                                    
                                                    $search_fields_list = $this->search_model->get_mls_data_search_fields_list_data($sf->id);

                                                    if (!empty($search_fields_list)) {

                                        ?>
                                        <div class="form-group">
                                            <label for="">HighSchool</label>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <select name="HighSchool" class="form-control" placeholder="High School">
                                                        <option value="">Select One</option>
                                                        <?php
                                                            foreach($search_fields_list as $sfl) {      
                                                                if($sfl->field_name != 'Select One') {
                                                        ?>
                                                            <option value="<?=$sfl->field_value?>" <?php echo (isset($_GET["HighSchool"]) && $_GET["HighSchool"] == $sfl->field_value) ? 'selected="selected"' : "";?> ><?=$sfl->field_name?></option>
                                                        <?php
                                                            } 
                                                            }                                      
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <?php  
                                                    } $x=0;
                                                }
                                            }
                                        ?>
                                        <h3>Parking Space</h3>
                                        <div class="form-group">
                                            <label for="">Garage Spaces</label>
                                            <input type="text" class="form-control input-number" value="<?=($this->input->get('GarageSpaces')) ? $this->input->get('GarageSpaces') : ""; ?>" name="GarageSpaces" min="0" maxlength="4">
                                        </div>
                                        <div class="form-group">
                                            <label for="">Carport Spaces</label>
                                            <input type="text" class="form-control input-number" value="<?=($this->input->get('CarportSpaces')) ? $this->input->get('CarportSpaces') : "";?>" name="CarportSpaces"  min="0" maxlength="4">
                                        </div>
                                        <!-- <div class="form-group">
                                            <label for="">Slab Parking Spaces</label>
                                            <input type="text" class="form-control input-number" value="<?=($this->input->get('Slab_Parking_Spaces')) ? $this->input->get('Slab_Parking_Spaces') : "";?>" name="Slab Parking Spaces" min="0" maxlength="4">
                                        </div> -->
                                        <h3 class="clearfix">Visual Options</h3>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="checkbox">
                                                    <input type="checkbox" id="Virtual Tour" name="VirtualToursCount" value="1" <?=($this->input->get('VirtualToursCount') && ($this->input->get('VirtualToursCount') == 1)) ? "checked" : ""; ?> />
                                                    <label for="Virtual Tour">Virtual Tour</label>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="checkbox">
                                                    <input type="checkbox" id="Videos" name="VideosCount" value="1" <?=($this->input->get('VideosCount') && ($this->input->get('VideosCount') == 1)) ? "checked" : ""; ?> />
                                                    <label for="Videos">Videos</label>
                                                </div>
                                            </div>
                                        </div>
                                        <?php if($account_info->MlsId == "20070913202326493241000000") { ?>
                                        <h3 class="clearfix">HOA</h3>
                                         <div class="row">
                                            <?php foreach(array('Y' => 'Yes', 'N' => 'No') as $k => $v): ?>
                                            <div class="col-md-12">
                                                <div class="checkbox">
                                                    <input type="checkbox" id="hoa-<?php echo $k; ?>" name="hoa[]" value="<?php echo $k; ?>" <?php echo in_array($this->input->get('hoa'), array('Y', 'N')) ? "checked" : ""; ?> />
                                                    <label for="hoa-<?php echo $k; ?>"><?php echo $v; ?></label>
                                                </div>
                                            </div>
                                            <?php endforeach; ?>
                                        </div>
                                        <h3>Roof</h3>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="checkbox">
                                                    <input type="checkbox" id="All Tile" name="Roof[]" value="All Tile" 
                                                    <?php if ($this->input->get('Roof') && !empty($this->input->get('Roof'))): ?>
                                                        <?php foreach ($this->input->get('Roof') as $rf) {
                                                            $roof = $rf;
                                                            if ($rf == "All Tile") {
                                                                echo "checked";
                                                            }
                                                        } ?>
                                                    <?php endif ?>
                                                    />
                                                    <label for="All Tile">All Tile</label>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="checkbox">
                                                    <input type="checkbox" id="Built-Up" name="Roof[]" value="Built-Up" 
                                                    <?php if ($this->input->get('Roof') && !empty($this->input->get('Roof'))): ?>
                                                        <?php foreach ($this->input->get('Roof') as $rf) {
                                                            $roof = $rf;
                                                            if ($rf == "Built-Up") {
                                                                echo "checked";
                                                            }
                                                        } ?>
                                                    <?php endif ?>
                                                    />
                                                    <label for="Built-Up">Built-Up</label>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="checkbox">
                                                    <input type="checkbox" id="Comp Shingle" name="Roof[]" value="Comp Shingle" 
                                                    <?php if ($this->input->get('Roof') && !empty($this->input->get('Roof'))): ?>
                                                        <?php foreach ($this->input->get('Roof') as $rf) {
                                                            $roof = $rf;
                                                            if ($rf == "Comp Shingle") {
                                                                echo "checked";
                                                            }
                                                        } ?>
                                                    <?php endif ?>
                                                    />
                                                    <label for="Comp Shingle">Comp Shingle</label>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="checkbox">
                                                    <input type="checkbox" id="Concrete" name="Roof[]" value="Concrete" 
                                                    <?php if ($this->input->get('Roof') && !empty($this->input->get('Roof'))): ?>
                                                        <?php foreach ($this->input->get('Roof') as $rf) {
                                                            $roof = $rf;
                                                            if ($rf == "Concrete") {
                                                                echo "checked";
                                                            }
                                                        } ?>
                                                    <?php endif ?>
                                                    />
                                                    <label for="Concrete">Concrete</label>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="checkbox">
                                                    <input type="checkbox" id="Foam" name="Roof[]" value="Foam" 
                                                    <?php if ($this->input->get('Roof') && !empty($this->input->get('Roof'))): ?>
                                                        <?php foreach ($this->input->get('Roof') as $rf) {
                                                            $roof = $rf;
                                                            if ($rf == "Foam") {
                                                                echo "checked";
                                                            }
                                                        } ?>
                                                    <?php endif ?>
                                                    />
                                                    <label for="Foam">Foam</label>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="checkbox">
                                                    <input type="checkbox" id="Metal" name="Roof[]" value="Metal" 
                                                    <?php if ($this->input->get('Roof') && !empty($this->input->get('Roof'))): ?>
                                                        <?php foreach ($this->input->get('Roof') as $rf) {
                                                            $roof = $rf;
                                                            if ($rf == "Metal") {
                                                                echo "checked";
                                                            }
                                                        } ?>
                                                    <?php endif ?>
                                                    />
                                                    <label for="Metal">Metal</label>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="checkbox">
                                                    <input type="checkbox" id="Partial Tile" name="Roof[]" value="Partial Tile" 
                                                    <?php if ($this->input->get('Roof') && !empty($this->input->get('Roof'))): ?>
                                                        <?php foreach ($this->input->get('Roof') as $rf) {
                                                            $roof = $rf;
                                                            if ($rf == "Partial Tile") {
                                                                echo "checked";
                                                            }
                                                        } ?>
                                                    <?php endif ?>
                                                    />
                                                    <label for="Partial Tile">Partial Tile</label>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="checkbox">
                                                    <input type="checkbox" id="Reflective Coating" name="Roof[]" value="Reflective Coating" 
                                                    <?php if ($this->input->get('Roof') && !empty($this->input->get('Roof'))): ?>
                                                        <?php foreach ($this->input->get('Roof') as $rf) {
                                                            $roof = $rf;
                                                            if ($rf == "Reflective Coating") {
                                                                echo "checked";
                                                            }
                                                        } ?>
                                                    <?php endif ?>
                                                    />
                                                    <label for="Reflective Coating">Reflective Coating</label>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="checkbox">
                                                    <input type="checkbox" id="Rock" name="Roof[]" value="Rock" 
                                                    <?php if ($this->input->get('Roof') && !empty($this->input->get('Roof'))): ?>
                                                        <?php foreach ($this->input->get('Roof') as $rf) {
                                                            $roof = $rf;
                                                            if ($rf == "Rock") {
                                                                echo "checked";
                                                            }
                                                        } ?>
                                                    <?php endif ?>
                                                    />
                                                    <label for="Rock">Rock</label>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="checkbox">
                                                    <input type="checkbox" id="Shake" name="Roof[]" value="Shake" 
                                                    <?php if ($this->input->get('Roof') && !empty($this->input->get('Roof'))): ?>
                                                        <?php foreach ($this->input->get('Roof') as $rf) {
                                                            $roof = $rf;
                                                            if ($rf == "Shake") {
                                                                echo "checked";
                                                            }
                                                        } ?>
                                                    <?php endif ?>
                                                    />
                                                    <label for="Shake">Shake</label>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="checkbox">
                                                    <input type="checkbox" id="Sub Tile Ventilation" name="Roof[]" value="Sub Tile Ventilation" 
                                                    <?php if ($this->input->get('Roof') && !empty($this->input->get('Roof'))): ?>
                                                        <?php foreach ($this->input->get('Roof') as $rf) {
                                                            $roof = $rf;
                                                            if ($rf == "Sub Tile Ventilation") {
                                                                echo "checked";
                                                            }
                                                        } ?>
                                                    <?php endif ?>
                                                    />
                                                    <label for="Sub Tile Ventilation">Sub Tile Ventilation</label>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="checkbox">
                                                    <input type="checkbox" id="Other (See Remarks)" name="Roof[]" value="Other (See Remarks)" 
                                                    <?php if ($this->input->get('Roof') && !empty($this->input->get('Roof'))): ?>
                                                        <?php foreach ($this->input->get('Roof') as $rf) {
                                                            $roof = $rf;
                                                            if ($rf == "Other (See Remarks)") {
                                                                echo "checked";
                                                            }
                                                        } ?>
                                                    <?php endif ?>
                                                    />
                                                    <label for="Other (See Remarks)">Other (See Remarks)</label>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                            }
                                        ?>
                                    <div class="show-less-link">
                                        <div class="invisible-xs">
                                            <span class="fa fa-chevron-up"></span>
                                        </div>
                                        <span class="icon-more-mobile visible-xs">
                                            <i class="fa fa-circle"></i>
                                            <i class="fa fa-circle"></i>
                                            <i class="fa fa-circle"></i>
                                        </span>
                                    </div>
                                    <div class="show-more-link">
                                        <div class="invisible-xs">
                                            <span class="fa fa-chevron-down"></span>
                                        </div>
                                        <span class="icon-more-mobile visible-xs">
                                            <i class="fa fa-circle"></i>
                                            <i class="fa fa-circle"></i>
                                            <i class="fa fa-circle"></i>
                                        </span>
                                    </div>
                                </div>
                            </li>
                        </ul>
                        <div class="save-button invisible-xs">
                            <button type="submit" class="btn">
                                <span class="fa fa-search"></span> 
                                <span class="search-label-text">Search</span>
                            </button>
                        </div>
                    </div> 
                    </form>
                    <div class="col-md-2 col-sm-1 mobile-ipad-padding-0">
                        <div class="pull-right">                    
                            <div class="save-button invisible-xs">
                                <?php
                                    $isSaved=false;

                                    if($this->session->userdata("customer_id")) {

                                        if(isset($_COOKIE["saved_searches_".$this->session->userdata("customer_id")])) {

                                            $query_string=$_SERVER['QUERY_STRING'];

                                            if(in_array(trim($query_string), $_COOKIE["saved_searches_".$this->session->userdata("customer_id")])) {
                                                $isSaved=true; ?>
                                                <button href="javascript:void(0)" data-save-search='<?=(isset($save_url)) ? $save_url : ""?>' type="button" class="btn btn-defautlt save-search-btn isSaved" id="save-search-btn"><span class="fa fa-save"></span> Search Saved</button>
                                <?php
                                            } else { 

                                                //$query_string=urldecode($query_string);
                                                //$query_string=str_replace(" ", "+", $query_string);
                                                $query_string=str_replace("%22", '"', $query_string);

                                                if(in_array(trim($query_string), $_COOKIE["saved_searches_".$this->session->userdata("customer_id")])) { 
                                                    $isSaved=true; ?>
                                                    <button href="javascript:void(0)" data-save-search='<?=(isset($save_url)) ? $save_url : ""?>' type="button" class="btn btn-defautlt save-search-btn isSaved" id="save-search-btn"><span class="fa fa-save"></span> Search Saved</button>
                                <?php
                                                } else { ?>
                                                    <button href="javascript:void(0)" data-save-search='<?=(isset($save_url)) ? $save_url : ""?>' type="button" class="btn btn-defautlt save-search-btn" id="save-search-btn"><span class="fa fa-save"></span> Save Search</button>
                                <?php
                                                }                                        
                                            }
                                        } else { ?>
                                             <button href="javascript:void(0)" data-save-search='<?=(isset($save_url)) ? $save_url : ""?>' type="button" class="btn btn-defautlt save-search-btn" id="save-search-btn"><span class="fa fa-save"></span> Save Search</button>
                                <?php
                                        }
                                    } else { ?>
                                        <button href="javascript:void(0)" data-save-search='<?=(isset($save_url)) ? $save_url : ""?>' type="button" class="btn btn-defautlt modalsignup" id="save-search-btn"><span class="fa fa-save"></span> Save Search</button>
                                <?php
                                    }

                                    if($isSaved) { ?>
                                        <script type="text/javascript">var searchIsSaved=true;</script>
                                <?php
                                    }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- ./ search bar -->
        <!-- search content -->
        <section class="property-main-content map-search-content" style="padding: 0;">
            <div class="tab-mapsearch">
                <div class="propmap-placeholder">
                    <div class="col-lg-4 col-md-4 col-sm-5 property-placeholder">
                        <div class="search-result-tab">
                            <h3>Search Results</h3>
                            <!-- <p class="total-listings"><span>1,345,435</span> Listings</p> -->
                              <!-- Nav tabs -->
                              <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active"><a href="#Loading" aria-controls="Loading" role="tab" data-toggle="tab">Loading properties...</a></li>
                              </ul>

                              <!-- Tab panes -->
                              <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="Loading">
                                    <?php for ($i = 1; $i <= 5; $i++) { ?>
                                    <div class="panel panel-default panel-placeholder">
                                        <div class='panel-body'>
                                            <div class='col-md-5 col-xs-3 no-padding'>
                                                <div class="placeholder-img"></div>
                                            </div>
                                            <div class='col-md-7 col-xs-9'>
                                                <p class="placeholder-address"></p>
                                                <p class="placeholder-city"></p>
                                                <b class="placeholder-price"></b>
                                                <span class='placeholder-propclass'></span>
                                                <p class="placeholder-details"></p></div>
                                        </div>
                                    </div>
                                    <?php } ?>
                                </div>
                              </div>
                        </div>
                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-7 no-padding">
                        <div class="placeholder-map">
                            <div class="map"><p>Map Loading...</p></div>
                        </div>
                    </div>
                </div>

                <!--  -->
                <div class="propmap" style="display:none">
                    <div class="col-lg-4 col-md-4 col-sm-5 left">
                        <div class="search-result-tab">
                            <h3>Search Results</h3>
                            <div class="search-keyword" style="display:none;">
                                <p>Keywords:</p>
                            </div>
                            <!-- <p class="total-listings"><span>1,345,435</span> Listings</p> -->
                              <!-- Nav tabs -->
                              <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active"><a href="javascript:void(0)" id="newest-tab" aria-controls="newest" role="tab" data-toggle="tab">Newest</a></li>
                                <li role="presentation"><a href="javascript:void(0)" id="cheapest-tab" aria-controls="cheapest" role="tab" data-toggle="tab">Cheapest</a></li>
                                <li role="presentation" class="pull-right visible-xs">
                                    <button class="btn btn-circle-blue btn-show-map"><i class="fa fa-map"></i></button>
                                    <div class="save-button">
                                        <button data-save-search="<?php echo (isset($save_url)) ? $save_url : ""?>" type="button" class="btn btn-circle-blue btn-default <?php echo (!$this->session->userdata('customer_id')) ? "modalsignup" : "save-search-btn" ?>" id="save-search-btn"><span class="fa fa-save"></span>
                                        </button> 
                                    </div>                 
                                </li>
                              </ul>

                              <!-- Tab panes -->
                              <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="newest">
                                    <div class="text-center">
                                        <ul class="map-pagination" style="display:none;">
                                            <li class="map-prev"><a href="javascript:void(0)" class="prev"><i class="fa fa-chevron-left"></i> <span>Prev</span></a></li>
                                            <li class="disabled"><a href="javascript:void(0)" class="text-center"><span class="start-range">1</span> - <span class="end-range">10</span> of <span class="total"></span></a></li>
                                            <li class="map-next"><a href="javascript:void(0)"  class="next"> <span>Next</span> <i class="fa fa-chevron-right"></i></a></li>
                                        </ul>
                                    </div>
                                    <div class="listingProperties"></div>
                                    <!-- <div id="map_canvas"></div> -->
                                </div>
                                <div role="tabpanel" class="tab-pane" id="cheapest">
                                    <div class="text-center">
                                        <ul class="map-pagination" style="display:none;">
                                            <li class="map-prev"><a href="javascript:void(0)" class="prev"><i class="fa fa-chevron-left"></i> <span>Prev</span></a></li>
                                            <li class="disabled"><a href="javascript:void(0)" class="text-center"><span class="start-range">1</span> - <span class="end-range">10</span> of <span class="total"></span></a></li>
                                            <li class="map-next"><a href="javascript:void(0)"  class="next"> <span>Next</span> <i class="fa fa-chevron-right"></i></a></li>
                                        </ul>
                                    </div>
                                    <div class="listingProperties"></div>
                                    <!-- <div id="map_canvas"></div> -->
                                </div>
                              </div>
                        </div>  
                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-7 no-padding" style="overflow: hidden;">
                        <?php if($this->config->item('is_paid')) : ?>
                            <div class="search-nav-polygon">
                                <div class="actions">
                                    <form method="GET" class="sbt-poly-search" id="sbt-poly-search">
                                        <input type="hidden" value="" id="polygonval" name="polygonval">
                                        <span class="empty-polygon"></span>
                                        <div id="color-palette"></div>
                                        <button type="submit" class="btn btn-apply" ><i class="fa fa-search"></i>Search</button>
                                        <a href="javascript:void(0)" class="btn btn-success btn-remove-selected" id="delete-button">Delete Selected Shape</a>
                                        <button type="button" class="btn btn-draw"><i class="fa fa-object-ungroup"></i>Polygon Search </button>
                                        <p class="polygon-text">Polygon Search</p>
                                    </form>
                                </div>
                            </div>
                         <?php endif;?>
                        <div id="map_canvas"></div>
                        <style type="text/css">
                            #map_canvas  {
                                height: 800px;
                                width: 100%;
                                margin-top: 0px;
                                padding: 0px;
                                max-width: 100%;
                                overflow: hidden;
                            }
                            #map_canvas img {
                                max-width: none!important;
                                background: none!important
                            }
                        </style>
                    </div>
                </div>

            </div>
            <!-- </div> -->
        </section>
        <!-- ./ search content -->
        <div class="modal fade" id="msg-err" tabindex="-1" role="dialog">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Error!</h4>
              </div>
              <div class="modal-body">
                <p id="msg-txt"></p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div><!-- /.modal-content -->
          </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
    </div>
<?php endif ?>
