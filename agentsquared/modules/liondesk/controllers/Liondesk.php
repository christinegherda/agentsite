<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// define('LIONDESK_CLIENT_ID', '926aff1db81f5bb2979ab7965c75a319c6bd48c7');
// define('LIONDESK_CLIENT_SECRET', '038c8254f79f916a33bf758af72727f636baffe6');
define('LIONDESK_CLIENT_ID', getenv('LIONDESK_CLIENT_ID'));
define('LIONDESK_CLIENT_SECRET', getenv('LIONDESK_CLIENT_SECRET'));

class Liondesk extends MX_Controller {
  private $agent_id = '';
  private $customer_id = '';
  private $access_token = false;
  private $is_premium = false;
  private $domain_user;
  
  function __construct(){
    parent::__construct();

      $this->domain_user = domain_to_user();
  }

  public function checkValidity(){
    $this->load->helpers('url');
    $this->load->library('liondesk_api');
    $this->load->model('liondesk_model');

    $plan = $this->liondesk_model->get_plan( $this->domain_user->id );
    $this->is_premium = ($plan != false && $plan->plan_id == 2)? true : false ;

    $this->agent_id = $this->domain_user->agent_id;
    $this->customer_id = $this->session->userdata('customer_id');

    if($this->is_premium){
      $result = $this->liondesk_model->getAccessToken($this->agent_id);

      if($result != false){

        // add code to refresh token here if expired.

        $this->access_token = $result->access_token;
        return true;
      }
    }
    return false;
  }

  /**
  * Get contact id. Used during creating tasks after User already signed up.
  * @param string $contact_id (required) customer id in AS, the primary key (id) from customer session
  */
  public function getLiondeskContactId($contact_id){
    if(!$this->checkValidity()){
      return false;
    }

    return $this->liondesk_model->getLiondeskContactId($contact_id);

  }

  /**
  * Add contact to liondesk
  * @param string $contact_id (required) Customer ID in AS. Not the primary key (id) but contact id from flex or locally generated.
  * @param assoc $data (required) Data required for LionDesk. Please see Liondesk API library.
  */
  public function pushContact($contact_id, $data = array()){

    if(!$this->checkValidity()){
      return false;
    }

    if($this->access_token){

      // caller is referrencing the contact_id column and not the primary key (id)
      $liondesk_id = $this->liondesk_model->getLiondeskContactId($contact_id, false); 

      if($liondesk_id == false || empty($liondesk_id)){

        // if contact does not have liondesk_contact_id, it will submit the contact to liondesk to get the id.
        $this->liondesk_api->loadToken($this->access_token);

        $result = $this->liondesk_api->submitContact($data);

        if($result && is_object($result)){

          if($contact_id !== null){
            // save the contact id from liondesk.
            $this->liondesk_model->saveContactId($contact_id, $result->id);
          }

          return $result;
        }
      }else{
        // mimic liondesk submit contact response if contact already has liondesk_contact_id field value.
        return (Object) array(
          'id' => $liondesk_id,
          'access_token' => $this->access_token,
        );
      }
    }
    // if everything else fails, returns false;
    return false;
  }

  /**
  * Add a task to a contact
  * @param assoc $data (required) Please see Liondesk API library.
  */
  public function createTask($data = array()){

    if(!$this->checkValidity()){
      return false;
    }

    if($this->access_token){
      $this->liondesk_api->loadToken($this->access_token);

      $result = $this->liondesk_api->submitTask($data);

      if($result && is_object($result)){
        return $result;
      }
    }
    return false;
  }

  private function log($message, $access = false){
    if($access){
      $file = APPPATH . 'modules/liondesk/logs/' . date("Y-m") . '-access.log';
    }else{
      $file = APPPATH . 'modules/liondesk/logs/' . date("Y-m") . '-error.log';
    }

    file_put_contents(
      $file,
      date('Y-m-d @ h:i:s A T').' : '.$message.";\n\r",
      FILE_APPEND
    );
  }
}
