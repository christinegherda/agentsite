<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Liondesk_model extends CI_Model{ 

  function __construct() {
    parent:: __construct();

    if(!$this->db->table_exists('liondesk')){
      return false;
    }
  }

  public function getAccessToken($agent_id){

    $this->db->select('*');
    $this->db->where('agent_id', $agent_id);

    $query = $this->db->get('liondesk')->row();
    if(!empty($query)){
      return $query;
    }
    return false;
  }

  /**
  * fetch the liondesk contact id of a specific contact
  * @param int $contact_id (required) the id (primary key) or the contact_id field.
  * @param boolean $index (optional) whether we want to fetch the contact by 'id' (primary key) or using `contact_id` column
  * @return mixed False if all else fails.
  */
  public function getLiondeskContactId($contact_id, $index = true){

    if($this->db->field_exists('liondesk_contact_id', 'contacts')){

      $this->db->select('liondesk_contact_id');
      if($index){
        $this->db->where('id', $contact_id);
      }else{
        $this->db->where('contact_id', $contact_id);
      }

      $query = $this->db->get('contacts')->row();

      if(!empty($query)){
        return $query->liondesk_contact_id;
      }
    }
    return false;
  }

  /**
  * updates the contact with liondesk contact id from liondesk.
  * @param string $contact_id (required) The contact_id field from flex or locally generated.
  * @param string $liondesk_contact_id (required) The id from liondesk response.
  * @return boolean
  */
  public function saveContactId($contact_id, $liondesk_contact_id){

    if(!$this->db->field_exists('liondesk_contact_id', 'contacts')){
      $this->load->dbforge();

      $this->dbforge->add_column('contacts', array(
        'liondesk_contact_id' => array(
          'type' => 'VARCHAR',
          'constraint' => '20',
        )
      ));
    }

    $this->db->where('contact_id', $contact_id);
    
    return $this->db->update('contacts', array(
      'liondesk_contact_id' => $liondesk_contact_id
    ));

  }

  /**
  * Get agent plan_id
  */
  public function get_plan($user_id){

    $this->db->where('subscriber_user_id', $user_id);

    $query = $this->db->get('freemium')->row();

    if(!empty($query)){
      return $query;
    }
    return false;
  }
}
