<?php

defined('BASEPATH') OR exit('No direct script access allowed');

define('HOME_VIEW', APPPATH . 'modules/home/views/');

class Src extends MX_Controller{
  
  protected $domain_user;

  public function __construct() {
    parent::__construct();
    
    $this->load->model("home/home_model", "data");
    $this->domain_user = domain_to_user();
  }
  
  public function css($filename = NULL, $param1 = NULL){
    if($filename !== NULL){
      if($filename !== 'widgets.css'){
        if($param1 !== NULL){
          $theme = $filename;
          $filename = $param1;
        }else{
          $user_info = $this->data->getUsersInfo($this->domain_user->id);
          $theme = $user_info->theme;
        }
        if(is_dir(HOME_VIEW . $theme . '/assets')){
          if(is_dir(HOME_VIEW . $theme . '/assets/css')){
            if(file_exists(HOME_VIEW . $theme . '/assets/css/' . $filename)){
              $contents = file_get_contents(HOME_VIEW . $theme . '/assets/css/' . $filename);
            }
          }
        }
        if(file_exists(COMMON_APPPATH_DIR . 'assets/css/' . $filename)){
          $contents = file_get_contents(COMMON_APPPATH_DIR . 'assets/css/' . $filename);
        }
      }else{
        if(isset($_GET['names'])){
          $widgets = explode(',', $_GET['names']);

          if(isset($_GET['theme'])){
            $theme = $_GET['theme'];
          }else{
            $user_info = $this->data->getUsersInfo($this->domain_user->id);
            $theme = $user_info->theme;
          }

          $contents = "/*\r\n\t Title: Widget Styles\r\n*/\r\n";
          $content_placeholder = '';
          foreach($widgets as $widget){
            if(is_dir(HOME_VIEW . $theme . '/assets')){
              if(is_dir(HOME_VIEW . $theme . '/assets/css')){
                if(file_exists(HOME_VIEW . $theme . '/assets/css/' . $widget . '.css')){
                  $content_placeholder = file_get_contents(HOME_VIEW . $theme . '/assets/css/' . $widget . '.css');
                }else{
                  $check_common = true;
                }
              }
            }
            if(isset($check_common) && $check_common){
              if(file_exists(COMMON_APPPATH_DIR . 'assets/css/' . $widget . '.css')){
                $content_placeholder = file_get_contents(COMMON_APPPATH_DIR . 'assets/css/' . $widget . '.css');
              }
            }
            $contents .= $content_placeholder;
            $contents .= "\r\n";
          }
          if(isset($_GET['minify']) && $_GET['minify']){
            $contents = str_replace(array("\r", "\n", "\t", " "), '', $contents);
          }
        }
      }
      if(isset($contents)){
        header('Content-Type: text/css');
        print($contents);
        exit();
      }
    }
    header("HTTP/1.0 404 Not Found");
    die();
  }

  public function js($filename = NULL, $param1 = NULL){
    if($filename !== NULL){
      if($param1 !== NULL){
        $theme = $filename;
        $filename = $param1;
      }else{
        $user_info = $this->data->getUsersInfo($this->domain_user->id);
        $theme = $user_info->theme;
      }
      if(is_dir(HOME_VIEW . $theme . '/assets')){
        if(is_dir(HOME_VIEW . $theme . '/assets/js')){
          if(file_exists(HOME_VIEW . $theme . '/assets/js/' . $filename)){
            $contents = file_get_contents(HOME_VIEW . $theme . '/assets/js/' . $filename);
          }
        }
      }
      if(file_exists(COMMON_APPPATH_DIR . 'assets/js/' . $filename)){
        $contents = file_get_contents(COMMON_APPPATH_DIR . 'assets/js/' . $filename);
      }
      if(isset($contents)){
        header('Content-Type: application/javascript');
        print($contents);
        exit();
      }
    }
    header("HTTP/1.0 404 Not Found");
    die();
  }
  
  public function font($filename = NULL, $param1 = NULL){
    if($filename !== NULL){
      if($param1 !== NULL){
        $theme = $filename;
        $filename = $param1;
      }else{
        $user_info = $this->data->getUsersInfo($this->domain_user->id);
        $theme = $user_info->theme;
      }
      if(is_dir(HOME_VIEW . $theme . '/assets')){
        if(is_dir(HOME_VIEW . $theme . '/assets/fonts')){
          $file = HOME_VIEW . $theme . '/assets/fonts/' . $filename;
        }
      }
      if(!isset($file) && is_dir(COMMON_APPPATH_DIR . '/assets/fonts')){
        $file = COMMON_APPPATH_DIR . 'assets/fonts/' . $filename;
      }
      if(isset($file) && file_exists($file)){
        $file_info = pathinfo($file);
        $contents = file_get_contents($file);
        switch($file_info['extension']){
          case 'woff2':
            header('Content-Type: font/woff2, application/font-woff2');
            break;
          case 'woff':
            header('Content-Type: application/font-woff');
            break;
          case 'svg':
            header('Content-Type: image/svg+xml');
            break;
          case 'ttf':
            header('Content-Type: application/x-font-ttf, application/x-font-truetype');
            break;
          case 'eot':
            header('Content-Type: application/vnd.ms-fontobject');
            break;
          case 'otf':
            header('Content-Type: application/x-font-opentype, application/font-sfnt');
            break;
          default:
            header("HTTP/1.0 404 Not Found");
            die();
        }
        print($contents);
        exit();
      }
    }
    header("HTTP/1.0 404 Not Found");
    die();
  }
  
  public function images($filename = NULL, $param1 = NULL){
    if($filename !== NULL){
      if($param1 !== NULL){
        $theme = $filename;
        $filename = $param1;
      }else{
        $user_info = $this->data->getUsersInfo($this->domain_user->id);
        $theme = $user_info->theme;
      }
      if(is_dir(HOME_VIEW . $theme . '/assets')){
        if(is_dir(HOME_VIEW . $theme . '/assets/images')){
          $file = HOME_VIEW . $theme . '/assets/images/' . $filename;
        }
      }
      if(!isset($file) && is_dir(COMMON_APPPATH_DIR . '/assets/images')){
        $file = COMMON_APPPATH_DIR . 'assets/images/' . $filename;
      }
      if(isset($file) && file_exists($file)){
        $file_info = pathinfo($file);
        switch(strtolower($file_info['extension'])){
          case "gif":
            header("Content-type: image/gif");
            break;
          case "jpg":
          case "jpeg":
            header("Content-type: image/jpeg");
            break;
          case "png":
            header("Content-type: image/png");
            break;
          case "bmp":
            header("Content-type: image/bmp");
            break;
          case "svg":
            header('Content-type: image/svg+xml');
            break;
          default:
            show_404();
        }
        header("Accept-Ranges: bytes");
        header('Content-Length: ' . filesize($file));
        $date = date('D, d M Y H:i:s T', filemtime($file));
        header("Last-Modified: {$date}");
        readfile($file);
        exit();
      }
    }
    header("HTTP/1.0 404 Not Found");
    die();
  }
}
