<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Agent_social_media extends MX_Controller {

    public $data;
    public $img_url;
    protected $domain_user;

    function __construct() {

        parent::__construct();

        $this->domain_user = domain_to_user();

        $userData = $this->domain_user->id;

        if(empty($userData)) {
            $dataConf = Modules::run('dsl/get_config');
            $userData = $dataConf['user_id'];
        }
        
        $this->load->library('facebook');
        $this->load->helper('url');
        $this->load->model("agent_social_media_model","agent_sm_model");
        $this->load->model("agent_sites/Agent_sites_model");

        //twitter config
        $this->load->library(array('Twconnect'));
        $this->load->library('twitteroauth/twitteroauth');

        //linkedin config
        $this->config->load('linkedin');
        $this->data['api_key'] = $this->config->item('api_key');
        $this->data['secret_key'] = $this->config->item('secret_key');
        $this->data['callbackUrl'] = site_url() . 'agent_social_media/linkedin_submit';
    }
    public function index(){

        $this->activate();
    }

    public function activate() {

        $data["is_reserved_domain"] = $this->Agent_sites_model->is_reserved_domain();
        $data["domain_info"] = $this->Agent_sites_model->fetch_domain_info();
        $data["facebook_options"] = $this->agent_sm_model->get_facebook_options();
        $data["twitter_options"] = $this->agent_sm_model->get_twitter_options();
        $data["linkedin_options"] = $this->agent_sm_model->get_linkedin_options();

        //printA($data["facebook_options"] );exit;
        /**************************************
        *         FACEBOOK                  *
      **************************************/ 
        //check if facebook user exist
        if($this->facebook_account_exist()) {
            $this->session->set_userdata('facebook_login', true);
            $data['fb'] = true;

        //check if facebook token is over 60 days
        $fetch_fbCreated = $this->agent_sm_model->fetch_facebook_data();
        $arrData['data'] = $fetch_fbCreated[0]['created'];
        $fb_created =  $arrData['data'];
        $newdate = strtotime ( '+60 day' , strtotime ($fb_created));
        $newdate = date ('Y-m-j' , $newdate);
        //var_dump($fb_created);exit;
        //var_dump($newdate);exit;
       
        $fbtime1 = new DateTime($fb_created);
        $fbtime2 = new DateTime($newdate);
        $interval = $fbtime1->diff($fbtime2);
        $date_interval = $interval->format('%a');
        //$date_interval = 60;
        //var_dump($date_interval);exit;

        if($date_interval > 59 ){
          $this->facebook->destroy_session();
          $this->session->unset_userdata('facebook_login');
          $delete = $this->agent_sm_model->delete_facebook_account();
          $this->session->set_flashdata('msg-expire', '<div class="alert alert-expire alert-danger text-center"><a href="#" style="color:#d9534f" class="close" data-dismiss="alert" aria-label="close">&times;</a>Your Facebook token has expired. Please activate to post to your socialmedia account!</div>');
        }
        
        } else

           $data['fb'] = false;

      /**************************************
        *         TWITTER                  *
      **************************************/ 
        //check if twitter user exist
        if($this->twitter_user_exist()) {
            $this->session->set_userdata('twitter_login', true);
           $data['tw'] = true;
        } else 
            $data['tw'] = false;

     /**************************************
        *         LINKEDIN               *
      **************************************/ 

        //check if linkedin user exist
        if($this->linkedin_user_exist()) {
            $this->session->set_userdata('linkedin_logged_in', true);
            $data['ln'] = true;

            //check if facebook token is over 60 days
            $fetch_lnCreated = $this->agent_sm_model->fetch_linkedin_data();
            $arrData['data'] = $fetch_lnCreated[0]['created'];
            $ln_created =  $arrData['data'];
            $newdate = strtotime ( '+59 day' , strtotime ($ln_created));
            $newdate = date ('Y-m-j' , $newdate);
            //var_dump($ln_created);exit;
            //var_dump($newdate);exit;
           
            $lntime1 = new DateTime($ln_created);
            $lntime2 = new DateTime($newdate);
            $interval = $lntime1->diff($lntime2);
            $date_interval = $interval->format('%a');
            //$date_interval = 60;
            //var_dump($date_interval);exit;

            if($date_interval  > 59){
                $this->session->unset_userdata('linkedin_logged_in');
                //$this->session->unset_userdata($access_data);
                $this->session->unset_userdata('oauth_request_token');
                $this->session->unset_userdata('oauth_request_token_secret');
                $this->session->unset_userdata('oauth_verifier');
                $this->session->unset_userdata('auth');
                $delete = $this->agent_sm_model->delete_linkedin_user();
                $this->session->set_flashdata('msg-expire-linkedin', '<div class="alert alert-expire alert-danger text-center"><a href="#" style="color:#d9534f" class="close" data-dismiss="alert" aria-label="close">&times;</a>Your Linkedin token has expired. Please activate to post to your socialmedia account!</div>');
            }

 
        } else {

            $data['ln'] = false;
        }
            
        $data['account_info'] = Modules::run('dsl/getAgentAccountInfo');
        $param["mlsID"] =  $data['account_info']->Id; 
        $data["spw_properties"] = $this->agent_sm_model->get_your_properties_listings( FALSE, $param );
        $data["active_listings"] = Modules::run('dsl/getAgentActivePropertyListings');
        $siteInfo = Modules::run('agent_sites/getInfo');
        $data['branding'] = $siteInfo['branding'];

        $data['website_type'] = $this->agent_sm_model->get_website_type(); 
        $data['title'] = "Agent Social Media Activate";
    
        $this->load->view('social_media_activate', $data);
    }

    /**************************************************************************
    *                              FACEBOOK CONTROLLER                        *
    ***************************************************************************/

    public function facebook_login(){
    
            $data['user'] = array();
            
            if ($this->session->userdata('facebook_login') != true){

                // Check if user is logged in
                if ($this->facebook->is_authenticated()){

                // User logged in, get user details
                $user = $this->facebook->request('get', '/me?fields=name,email');
                $pages = $this->facebook->request('get', '/me/accounts');

                 if (!isset($user['error'])){
                    $data['user'] = $user;
                     //printA($user);exit;

                    $this->agent_sm_model->create_facebook($user);
                    $this->session->set_flashdata('msg', '<div class="alert alert-flash alert-success text-center">Facebook account successfully activated!</div>');
                }

                 if (!isset($pages['error'])){
                    $data['pages'] = $pages;
                     //printA($pages);exit;

                    foreach($pages as $page){
                            foreach($page as $p){

                            $data['p'] = $p;
                             $this->agent_sm_model->create_facebook_page($p);
                        } break;
                    }

                    $this->session->set_flashdata('msg', '<div class="alert alert-flash alert-success text-center">Facebook account successfully activated!</div>'); 
                }

                $this->session->set_userdata('facebook_login', true);
            }

            //$this->load->view('home',$data);
            redirect('agent_social_media?post_socialmedia=1');
        }

    }

    public function deactivate_facebook(){
        $this->facebook->destroy_session();
        $this->session->unset_userdata('facebook_login');
        $delete = $this->agent_sm_model->delete_facebook_account();
        $this->session->set_flashdata('msg', '<div class="alert alert-flash alert-danger text-center">Facebook account successfully deactivated!</div>');
        redirect('agent_social_media/activate');
    }
 

    public function facebook_account_exist(){
        $checkaccount = $this->agent_sm_model->check_facebook_account();
        //printA($checkaccount);exit;

        if(empty($checkaccount)) {
            return false;
        }
        return true;    
    }

    /*************************************************************************
    *                              TWITTER CONTROLLER                        *
    **************************************************************************/
    public function twitter_redirect() {

        if($this->session->userdata('twitter_login') == true) {
            redirect('agent_social_media?post_socialmedia=1');
        } else {
            $ok = $this->twconnect->twredirect('agent_social_media/twitter_user_callback');       
            if (!$ok) 
                redirect('agent_social_media?post_socialmedia=1');
        }
    }

    public function twitter_callback() {
        
       if($this->session->userdata('twitter_login') == true)
            redirect('agent_social_media?post_socialmedia=1');
        else {
            $ok = $this->twconnect->twprocess_callback();
            if ($ok)
                redirect('agent_social_media/twitter_success'); 
            else 
                redirect ('agent_social_media/twitter_failure');
        }
    }

    public function twitter_user_callback() {
        $ok = $this->twconnect->twprocess_callback();
        if ($ok)
            redirect('agent_social_media/twitter_success'); 
        else 
            redirect ('agent_social_media/twitter_failure');
    }

    public function twitter_success() {
        
        $this->twconnect->twaccount_verify_credentials();
        $user_profile = $this->twconnect->tw_user_info;
        
        $this->session->set_userdata('twitter_login', true);
        
        //Get token from the Twconnect class and stored it in the array
        $this->twconnect->twprocess_callback();
        $token = $this->twconnect->tw_access_token;
        $access_token = $token['oauth_token'];
        $token_secret = $token['oauth_token_secret'];
    
        $arr = array(
            'id' => $user_profile->id,
            'name' => $user_profile->name,
            'screen_name' => $user_profile->screen_name,
            'location' => $user_profile->location,
            'description' => $user_profile->description,
            'profile_image_url' => $user_profile->profile_image_url,
            'access_token' => $access_token,
            'token_secret' => $token_secret
        );
        
        $this->agent_sm_model->create_twitter($arr);
        $this->load->view('social_media_activate');
        redirect('agent_social_media?post_socialmedia=1');
    }

    public function twitter_failure() {
        redirect('agent_social_media?post_socialmedia=1');
    }

    public function deactivate_twitter() {
        $this->twconnect->twclear_session_data();
        $delete = $this->agent_sm_model->delete_twitter_user();
        $this->session->set_flashdata('msg', '<div class="alert alert-flash alert-danger text-center">Twitter account successfully deactivated!</div>');
        redirect('agent_social_media/activate');
    }

    public function twitter_user_exist() {
        $checkuser = $this->agent_sm_model->check_twitter_user();
        if(empty($checkuser)) {
            return false;
        }
        return true;        
    }

    /*************************************************************************
    *                              LINKEDIN CONTROLLER                        *
    **************************************************************************/

    public function linkedin_login() {

        $this->load->library('Linkedin/Linkedin_agent', $this->data);

        $token = $this->linkedin_agent->get_request_token();

        $oauth_data = array(
            'oauth_request_token' => $token['oauth_token'],
            'oauth_request_token_secret' => $token['oauth_token_secret']
        );

        $this->session->set_userdata($oauth_data);

        $request_link = $this->linkedin_agent->get_authorize_URL($token);

        header("Location: " . $request_link);
    }

    /**
     *  Get Access Token
     */
    public function linkedin_submit() {

        $this->data['oauth_token'] = $this->session->userdata('oauth_request_token');
        $this->data['oauth_token_secret'] = $this->session->userdata('oauth_request_token_secret');
        $this->load->library('Linkedin/Linkedin_agent', $this->data);

        //If the user hits the cancel button
        if($this->input->get('oauth_problem') == 'user_refused') {
            redirect('agent_social_media?post_socialmedia=1');
        } else {
            $this->session->set_userdata('oauth_verifier', $this->input->get('oauth_verifier'));
            $tokens = $this->linkedin_agent->get_access_token($this->input->get('oauth_verifier'));

            $access_data = array(
                'oauth_access_token' => $tokens['oauth_token'],
                'oauth_access_token_secret' => $tokens['oauth_token_secret']
            );
                
            $this->session->set_userdata($access_data);
            
            //Store Linkedin info in a session
            $auth_data = array('linked_in' => serialize($this->linkedin_agent->token), 'oauth_secret' => $this->input->get('oauth_verifier'));
            $this->session->set_userdata(array('auth' => $auth_data));

            $this->linkedin_profile();

            $this->session->set_userdata('linkedin_logged_in', true);
            redirect('agent_social_media?post_socialmedia=1');
        }
    }

    /**
     * Fetch linkedin profile
     */
    public function linkedin_profile() {

        $auth_data = $this->session->userdata('auth');

        $this->load->library('Linkedin/Linkedin_agent', $this->data);

        $status_response = $this->linkedin_agent->getData('http://api.linkedin.com/v1/people/~', unserialize($auth_data['linked_in']));
        $get_profile_pic = $this->linkedin_agent->getData('http://api.linkedin.com/v1/people/~:(picture-url)', unserialize($auth_data['linked_in']));

        $xml = new SimpleXMLElement($status_response);
        $xml2 = new SimpleXMLElement($get_profile_pic);

        echo $xml->id;
        echo $xml->{'first-name'};
        echo $xml->{'last-name'};
        echo $xml2->{'picture-url'};
        echo $xml->headline;
        echo $xml->{'site-standard-profile-request'}->url;
        
        $arr = array(
            'id' => $xml->id,
            'name' => $xml->{'first-name'} . ' ' . $xml->{'last-name'},
            'app_token' => $this->session->userdata('oauth_access_token'),
            'token_secret' => $this->session->userdata('oauth_access_token_secret'),
            'social_media_account' => $xml->{'site-standard-profile-request'}->url,
            'profile_picture' => $xml2->{'picture-url'} 
        );

        $this->agent_sm_model->create_linkedin($arr);
    }

    /**
     *  Delete user's linkedin in the database
     */
    public function deactivate_linkedin() {
        $this->session->unset_userdata('linkedin_logged_in');
        $this->session->unset_userdata($access_data);
        $this->session->unset_userdata('oauth_request_token');
        $this->session->unset_userdata('oauth_request_token_secret');
        $this->session->unset_userdata('oauth_verifier');
        $this->session->unset_userdata('auth');
        $delete = $this->agent_sm_model->delete_linkedin_user();
        $this->session->set_flashdata('msg', '<div class="alert alert-flash alert-danger text-center">LinkedIn account successfully deactivated!</div>');
        redirect('agent_social_media/activate');
    }

    /**
     *  Check if linkedin user exist
     */
    public function linkedin_user_exist(){
        $checkuser = $this->agent_sm_model->check_linkedin_user();

        if(empty($checkuser)) {
            return false;
        }
        return true;    
    }

    public function social_post() {

        if(isset($_POST['post_facebook']) && $_POST['post_facebook'] == 1){

            if($this->session->userdata('facebook_login') == true){

            $facebook_options = $this->agent_sm_model->get_facebook_options();

                if(isset($facebook_options) && !empty($facebook_options)){

                    foreach($facebook_options as $option){

                        //posting to facebook using facebook  user account
                        if($option->post_value == "1" && $option->facebook_type == "user"){

                            $accessToken = $option->app_token;

                            header('Content-Type: application/json');
                            $result = $this->facebook->request('post', '/me/feed', array('message' => $this->input->post('your_post'),'link' => $this->input->post('property_link')),$accessToken);
                            echo json_encode($result);

                        }

                        //posting to facebook using facebook  page account
                        if($option->post_value == "1" && $option->facebook_type == "page"){

                            $accessToken = $option->app_token;
                            $page_id = $option->social_id;

                            header('Content-Type: application/json');
                            $result = $this->facebook->request('post', '/'.$page_id.'/feed', array('message' => $this->input->post('your_post'),'link' => $this->input->post('property_link')),$accessToken);
                            echo json_encode($result);

                        }
                    }
                }    
            } 
        }

        //check if twitter is selected
        if(isset($_POST['post_twitter']) && $_POST['post_twitter'] == 1) {

            //publish post to twitter account
            if($this->session->userdata('twitter_login') == true) {
                $my_post = $this->input->post('your_post'). ' '. $this->input->post('property_link');
                $img_url = $this->input->post('property_photo');
                $this->twconnect->tw_post_media_agent($my_post, $img_url);
            }
        }

        // check if linkedin is selected
        if(isset($_POST['post_linkedin']) && $_POST['post_linkedin'] == 1) {

            //publish post to linkedin account
            if($this->session->userdata('linkedin_logged_in') == true) {

                $title = $this->input->post('property_title');
                $tags = get_meta_tags($this->input->post('property_link'));
                $img_url = $this->input->post('property_photo');
                $comment = $this->input->post('your_post');
                $url = $this->input->post('property_link');
                $this->load->library('Linkedin/Linkedin_agent', $this->data);
                $karl = array('linked_in' => serialize($this->linkedin_agent->token));

                if(!$this->load->library('Linkedin/Linkedin_agent', $this->data)) {
                    
                    $status_response = $this->linkedin_agent->share($comment, $url, $img_url, $title, $tags['description'], unserialize($karl['linked_in']));
                } else {
                    
                    $status_response = $this->linkedin_agent->share($comment, $url, $img_url, $title, $tags['description'], unserialize($karl['linked_in']));
                }
            }
        }

        $is_facebook_selected = $this->agent_sm_model->is_facebook_post();
        $fb_selected = false;

        if(isset($is_facebook_selected) && !empty($is_facebook_selected)){

            $fb_selected = true;
        }

        if($fb_selected == true || $this->session->userdata('twitter_login') == true || $this->session->userdata('linkedin_logged_in') == true)
            $this->session->set_flashdata('msg', '<div class="alert alert-flash alert-success text-center">Post to Social Media Success!</div>');
        else
            $this->session->set_flashdata('msg', '<div class="alert alert-flash alert-danger text-center">Please activate at least 1 Social Media!</div>');
        
        redirect('agent_social_media/activate');
    }

    public function is_page_selected(){

        if( $this->input->is_ajax_request()){
            
            if( $this->input->post("facebook_id") == "" || empty($this->input->post("facebook_id")) )
            {
                $response = array( "success" => FALSE );
                return json_encode($response);
            }
            
            $val = ( $this->input->post("is_selected") == 0 ) ? 1 : 0;

            if( $this->agent_sm_model->update_is_selected() )
            {
                $response = array( "success" => TRUE , "val" => $val);
            }
            else
            {
                $response = array( "success" => FALSE, "val" => $val);
            }

            exit(json_encode($response));

        }
    }

    public function links() {

        $fetch_link = $this->agent_sm_model->fetch_link();
        $data = [
            'facebook' => '',
            'twitter'  => '',
            'linkedin' => '',
            'googleplus' => '',
            'instagram' => ''
        ];

        if($fetch_link)
        {
            $data['facebook']   = get_social_media_link_uri('facebook.com', $fetch_link[0]['facebook']);
            $data['twitter']    = get_social_media_link_uri('twitter.com', $fetch_link[0]['twitter']);
            $data['linkedin']   = get_social_media_link_uri('linkedin.com', $fetch_link[0]['linkedin']);
            $data['googleplus'] = get_social_media_link_uri('plus.google.com', $fetch_link[0]['googleplus']);
            $data['instagram']  = get_social_media_link_uri('instagram.com', $fetch_link[0]['instagram']);
        }

        $data['title'] = "Agent Social Media Links";
        $data['agent_email'] = $this->session->userdata('email');
        $siteInfo = Modules::run('agent_sites/getInfo');
        $data['branding'] = $siteInfo['branding'];
        $data["is_reserved_domain"] = $this->Agent_sites_model->is_reserved_domain();
        $data["domain_info"] = $this->Agent_sites_model->fetch_domain_info();

        $this->load->view('social_media_links', $data);

    }

    public function linksInfo() {

        $fetch_link = $this->agent_sm_model->fetch_link();
        $data = [
            'facebook' => '',
            'twitter'  => '',
            'linkedin' => '',
            'googleplus' => '',
            'instagram' => ''
        ];

        if ($fetch_link)
        {
            $data['facebook']   = get_social_media_link_url('facebook.com', $fetch_link[0]['facebook']);
            $data['twitter']    = get_social_media_link_url('twitter.com', $fetch_link[0]['twitter']);
            $data['linkedin']   = get_social_media_link_url('linkedin.com', $fetch_link[0]['linkedin']);
            $data['googleplus'] = get_social_media_link_url('plus.google.com', $fetch_link[0]['googleplus']);
            $data['instagram']  = get_social_media_link_url('instagram.com', $fetch_link[0]['instagram']);
            $data['instagram_id']  = $fetch_link[0]['instagram'];
        }

        $data['title'] = "Agent Social Media Links";
        $data['agent_email'] = $this->session->userdata('email');
        $siteInfo = Modules::run('agent_sites/getInfo');
        $data['branding'] = $siteInfo['branding'];

        return $data;
    }

    public function add_links(){

        $facebook = (isset($_POST["facebook"])) ? $_POST["facebook"] : $this->input->post('facebook_url');
        $twitter = (isset($_POST["twitter"])) ? $_POST["twitter"] :$this->input->post('twitter_url');
        $linkedin = (isset($_POST["linkedin"])) ? $_POST["linkedin"] :$this->input->post('linkedin_url');
        $googleplus = (isset($_POST["googleplus"])) ? $_POST["googleplus"] :$this->input->post('googleplus_url');

        $arr = array(
            'user_id' => $this->domain_user->id,
            'facebook' => $facebook,
            'twitter' => $twitter,
            'linkedin' => $linkedin,
            'googleplus' => $googleplus,
        );

        $fetch_link = $this->agent_sm_model->fetch_link();
        
        if(!$fetch_link) {
            $this->agent_sm_model->insert_link($arr);
        } else {
            $this->agent_sm_model->update_link($arr);
        }
        redirect('agent_social_media/links');
    }

    public function get_property_activate_data(){

        $response = array("success" => FALSE);

        if( $this->input->is_ajax_request() )
        {
            //get Spw Created sites
            $spw_property = $this->agent_sm_model->get_property_activate_data( $this->input->post("id"),$this->input->post("selected") );

            $account_info = Modules::run('dsl/getAgentAccountInfo');
            if(isset($account_info) && !empty($account_info)){

                $agent_name = $account_info->Name;
                $agent_phone = $account_info->Phones[0]->Number;
            }

            if( isset($spw_property) ){
                //get spw website created data
                $response = array(
                                      "success" => TRUE,
                                      "address"             => (isset($spw_property->address)) ? $spw_property->address : '',
                                      "unparseAddress"      => (isset($spw_property->unparsedaddr)) ? $spw_property->unparsedaddr : '',
                                      "primary_photo"       => (isset($spw_property->primary_photo)) ? $spw_property->primary_photo : '',
                                      "site_url"            => (isset($spw_property->site_url)) ? $spw_property->site_url : '',
                                      "site_description"    => (isset($spw_property->description)) ? $spw_property->description : '',
                                      "price"               => (isset($spw_property->price)) ? $spw_property->price : '',
                                      "price_previous"      => (isset($spw_property->price_previous)) ? $spw_property->price_previous : '',
                                      "broker"              => (isset($spw_property->broker)) ? $spw_property->broker : '',
                                      "broker_number"       => (isset($spw_property->broker_number)) ? $spw_property->broker_number : '',
                                      "agent_name"          => (isset($agent_name)) ? $agent_name : '',
                                      "agent_phone"         => (isset($agent_phone)) ? $agent_phone : ''
                                   );
            }

            exit( json_encode($response) );
            //printA($property);exit();
        }
    }

    public function get_property_listing_data(){

        $response = array("success" => FALSE);

        if( $this->input->is_ajax_request() )
        {
            $listing_id = $this->input->post("id");
            $user_id = $this->domain_user->id;
            $endpoints = "property/".$listing_id."/standard-fields/".$user_id."";
            $active_listings = Modules::run('dsl/get_endpoint', $endpoints);
            $active_property = json_decode($active_listings);
            
             $account_info = Modules::run('dsl/getAgentAccountInfo');
            if(isset($account_info) && !empty($account_info)){

                $agent_name = $account_info->Name;
                $agent_phone = $account_info->Phones[0]->Number;
            }

            if(!empty($active_property)){

                $active_description = str_limit($active_property->data->PublicRemarks,197);
                $active_image = $active_property->data->Photos[0]->Uri1024;
                $active_address = $active_property->data->UnparsedFirstLineAddress;
                $active_broker = $active_property->data->ListOfficeName;
                $active_broker_number = $active_property->data->ListOfficePhone;
                $listingId = $active_property->data->ListingKey;
                $price = $active_property->data->ListPrice;
                $price_previous = $active_property->data->PreviousListPrice;    
            }
            //get custom domain url
            $domain_info = $this->Agent_sites_model->fetch_domain_info();
            $is_reserved_domain = $this->Agent_sites_model->is_reserved_domain();

            if(!empty($domain_info)){

                if($domain_info[0]->status == "completed" || $domain_info[0]->is_subdomain == "1") { 

                   $base = "http://";
                   $base_url = $base.$is_reserved_domain['domain_name'].'/';

                 } elseif($domain_info[0]->status != "completed" && $domain_info[0]->is_subdomain == "1") { 

                    $base = "http://";
                    $base_url = $base.$is_reserved_domain['domain_name'].'/';

                 } else { 

                        $base_url = base_url();
                    }

            } else { 

                $base_url = base_url();
            } 


            if(!empty($active_property)) {

                //get active property data
                $response = array(
                                      "success" => TRUE, 
                                      "site_url"            => $base_url.'/property-details/'.$listingId,
                                      "site_description"    => (isset($active_description)) ? $active_description : '',
                                      "primary_photo"       => (isset($active_image)) ? $active_image : '',
                                      "address"             => (isset($active_address)) ? $active_address : '',
                                      "broker"              => (isset($active_broker)) ? $active_broker : '',
                                      "broker_number"       => (isset($active_broker_number)) ? $active_broker_number : '',
                                      "price"               => (isset($price)) ? $price : '',
                                      "price_previous"      => (isset($price_previous)) ? $price_previous : '',
                                      "agent_name"          => (isset($agent_name)) ? $agent_name : '',
                                      "agent_phone"         => (isset($agent_phone)) ? $agent_phone : ''
                                   );

            } 
            exit( json_encode($response) );
            //printA($property);exit();
        }
    }

    public function get_property_site_data(){

        //get agent listing
        $active_listings = Modules::run('dsl/getAgentActivePropertyListings');
        if(isset($active_listings)){
            foreach($active_listings as $active){

                $description = str_limit($active->StandardFields->PublicRemarks,197);
                $image = $active->StandardFields->Photos[0]->Uri1024;
            }
        }

        //get agent address
        $account_info = Modules::run('dsl/getAgentAccountInfo');
        if(isset($account_info)){
    
            $address = $account_info->Addresses[0]->Address;
            $broker = $account_info->Office;
            $broker_number = $account_info->Phones[0]->Number;
            $agent_name = $account_info->Name;
            $agent_phone = $account_info->Phones[0]->Number;
        }

        $domain_info = $this->Agent_sites_model->fetch_domain_info();
        $is_reserved_domain = $this->Agent_sites_model->is_reserved_domain();

        if(!empty($domain_info)){

            if($domain_info[0]->status == "completed" || $domain_info[0]->is_subdomain == "1") { 

               $base = "http://";
               $base_url = $base.$is_reserved_domain['domain_name'];

             } elseif($domain_info[0]->status != "completed" && $domain_info[0]->is_subdomain == "1") { 

                $base = "http://";
                $base_url = $base.$is_reserved_domain['domain_name'];

             } else { 

                    $base_url = base_url();
                }

        } else { 

            $base_url = base_url();
        } 


        $response = array("success" => FALSE);

        if( $this->input->is_ajax_request() ){
            
            if(!empty($account_info)){

                //get agent listing Data
                $response = array(
                                      "success" => TRUE, 
                                      "site_url"            => $base_url,
                                      "site_description"    => (isset($description)) ? $description : '',
                                      "primary_photo"       => (isset($image)) ? $image : '',
                                      "address"             => (isset($address)) ? $address : '',
                                      "broker"              => (isset($broker)) ? $broker : '',
                                      "broker_number"       => (isset($broker_number)) ? $broker_number : '',
                                      "agent_name"          => (isset($agent_name)) ? $agent_name : '',
                                      "agent_phone"         => (isset($agent_phone)) ? $agent_phone : ''
                                   );

            }
            
            exit( json_encode($response) );
        }
    }
}
