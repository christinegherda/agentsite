<?php 
    $this->load->view('session/header'); 
    $this->load->view('session/top-nav');
?> 

        <div class="content-wrapper">
          <div class="page-title">
            <h3 class="heading-title">
              <span>Activate Social Media</span>
            </h3>
          </div>
            <section class="content">
                <div class="row">
                    <div class="featured-list">
                        <div class="col-md-12">
                            <h2></h2>    
                        </div>
                    </div>
                    <div class="col-md-6 social-media-activate">
                        <h4>Activate Social Media</h4>
                        <div class="panel-group" role="tablist" aria-multiselectable="true">
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab">
                                    <h4 class="panel-title">
                                         <i class="fa fa-facebook"></i> <a class="social-title" data-toggle="collapse" data-parent="#accordion" href="#collapseFacebook">Facebook 

                                         <?php if(!$fb) { ?>

                                        <span class="activated pull-right inactive"> &nbsp;&nbsp;Inactive</span><i class="fa fa-exclamation-triangle pull-right inactive"></i></a>

                                        <?php } else { ?>

                                        <span class="activated pull-right active"> &nbsp;&nbsp;Active</span><i class="fa fa-check-circle pull-right active"></i></a>

                                        <?php } ?>
                                    </h4>

                                </div>
                                <div class="panel-collapse collapse moreDetails" role="tabpanel">
                                    <div class="panel-body">
                                        <div class="col-md-12 col-xs-12 col-sm-12">
                                            <p>To create a new Facebook login, press Activate below.</p>

                                              <?php if(!$fb) { ?>
                                                    <a href="javascript:void(0)" onclick ="fb_login();" ><p class="btn btn-save btn-success"> Activate</p></a>
                                                <?php } else { ?>
                                                    <a href="<?php echo site_url('agent_social_media/deactivate_facebook');?>" class="btn btn-danger btn-cancel">Deactivate</a>
                                                <?php } ?>

                                                <?php
                                                    if(isset($facebook_options) && !empty($facebook_options)){?>
                                                        <div class="facebook-pages-option">
                                                          <h5>Select Facebook Pages to post on facebook</h5>
                                                            <?php
                                                             foreach($facebook_options as $option){?>

                                                                <?php
                                                                    $fb_id = $option->social_id;
                                                                    $fb_profile_photo = "https://graph.facebook.com/".$fb_id."/picture";
                                                                    $fb_default_photo = base_url('assets/images/fb-default-photo.jpg');
                                                                ?>

                                                                <div class="checkbox">
                                                                   <label>
                                                                      <input type="checkbox" id="is_page_selected_<?=$option->id;?>" data-id="<?=$option->id;?>" class="is_selected_post" name="is_selected[]" value="<?php echo $option->post_value?>" <?php echo ( $option->post_value == "1") ? 'checked="checked"': ""; ?> >
                                                                      <input type="hidden" value="<?php echo $option->social_id?>">
                                                                      <span class="fb-photo"><img width="30" src="<?php echo isset($fb_profile_photo) ? $fb_profile_photo : $fb_default_photo ;?>"></span><span style="position: relative;left: -14px;top: 7px;" class="fb-icon"><img width="15px" src="<?php echo base_url()?>/assets/images/fb-icon.png"></span>
                                                                      <span class="facebook-page-name"><?php echo $option->name ?></span>
                                                                    </label>
                                                                </div>
                                                           <?php }?>
                                                        </div>
                                            <?php  } ?>
                                        </div>
                                        <div class="col-md-12 col-xs-12 col-sm-12">
                                            <div class="well">
                                                <h4>About Facebook</h4>
                                                <p>Millions of people use Facebook everyday to keep up with friends,
                                                upload an unlimited number of photos, share links and videos, and learn more about the people they meet.
                                                </p>

                                                <p><b>Don't have an account on Facebook?</b> Click <a href="">here</a></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab">
                                    <h4 class="panel-title">
                                       <i class="fa fa-twitter"></i> <a class="social-title" data-toggle="collapse" data-parent="#accordion" href="#collapseTwitter">Twitter

                                        <?php if(!$tw) { ?>

                                       <span class="activated pull-right inactive"> &nbsp;&nbsp;Inactive</span><i class="fa fa-exclamation-triangle pull-right inactive"></i></a>

                                        <?php } else { ?>

                                        <span class="activated pull-right active"> &nbsp;&nbsp;Active</span><i class="fa fa-check-circle pull-right active"></i></a>

                                        <?php } ?>
                                    </h4>

                                </div>
                                <div class="panel-collapse collapse" role="tabpanel">
                                    <div class="panel-body">
                                        <div class="col-md-12 col-xs-12 col-sm-12">
                                            <p>To create a new Twitter login, press Activate below.</p>
                                           <?php if(!$tw) { ?>
                                                <a href="javascript:void(0)" onclick ="twitt_login();"><p class="btn btn-save btn-success"> Activate</p></a>
                                            <?php } else { ?>
                                                <a href="<?php echo site_url('agent_social_media/deactivate_twitter');?>" class="btn btn-danger btn-cancel">Deactivate</a>
                                            <?php } ?>
                                        </div>
                                        <div class="col-md-12 col-xs-12 col-sm-12">
                                            <div class="well">
                                                <h4>About Twitter</h4>
                                                <p>Twitter is a real-time information network that connects you to the latest information about what you find interesting. Simply find the public streams you find most compelling and follow the conversations. 
                                                </p>

                                                <p><b>Don't have an account on Twitter?</b> Click <a href="">here</a></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab">
                                    <h4 class="panel-title"><i class="fa fa-linkedin"></i> <a href = "javascript:void(0)" class="collapsed" >LinkedIn</a>

                                         <?php if(!$ln) { ?>

                                            <span class="activated pull-right inactive"> &nbsp;&nbsp;Inactive</span><i class="fa fa-exclamation-triangle pull-right inactive"></i></a>

                                        <?php } else { ?>

                                        <span class="activated pull-right active"> &nbsp;&nbsp;Active</span><i class="fa fa-check-circle pull-right active"></i></a>

                                        <?php } ?>
                                    </h4>
                                </div>
                                <div class="panel-collapse collapse" role="tabpanel">
                                    <div class="panel-body">
                                        <div class="col-md-12 col-xs-12 col-sm-12">
                                            <p>To create a new LinkedIn login, press Activate below.</p>
                                            <?php if(!$ln) { ?>
                                                <a href="javascript:void(0)" onclick ="linkdn_login();"><p class="btn btn-save btn-success"> Activate</p></a>
                                            <?php } else { ?>
                                                <a href="<?php echo site_url('agent_social_media/deactivate_linkedin');?>" class="btn btn-danger btn-cancel">Deactivate</a>
                                            <?php } ?>
                                        </div>
                                        <div class="col-md-12 col-xs-12 col-sm-12">
                                            <div class="well">
                                                <h4>About LinkedIn</h4>
                                                <p>LinkedIn is a social networking site where alumni,graduates and other professionals connect online.
                                                </p>

                                                <p><b>Special Notes</b><br>
                                                    LinkedIn has posting limits of 25 a day.
                                                </p>

                                                <p><b>Don't have an account on LinkedIn</b> Click <a href="">here</a></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 social-postarea">
                        <h4>Post to your Social Media accounts</h4>
                            <div class="select-post">
                                <p>Post will distribute to: </p>
                                <ul class="list-inline">

                                    <!-- Show Facebook Profile and Page Photo -->
                                    <?php
                                        if ($fb){

                                            if(isset($facebook_options)  && !empty($facebook_options)){
                                                 foreach($facebook_options as $option){

                                                    $fb_id = $option->social_id;
                                                    $fb_profile_photo = "https://graph.facebook.com/".$fb_id."/picture";
                                                    $fb_default_photo = base_url('assets/images/fb-default-photo.jpg');

                                                    if($option->post_value == "1"){?>

                                                        <li><span class="fb-photo"><img width="30" src="<?php echo isset($fb_profile_photo) ? $fb_profile_photo : $fb_default_photo ;?>"></span><span style="position: relative;left: -14px;top: 7px;" class="fb-icon"><img width="15px" src="<?php echo base_url()?>/assets/images/fb-icon.png"></span></li>

                                            <?php   }

                                                }
                                            }
                                        }?>

                                    <!-- Show Twitter Profile Photo -->
                                     <?php
                                        if ($tw){

                                                if(isset($twitter_options)  && !empty($twitter_options)){

                                                    $tw_profile_photo = $twitter_options[0]->profile_picture;
                                                    $tw_default_photo = base_url('assets/images/fb-default-photo.jpg');

                                                ?>

                                                        <li><span class="tw-photo"><img width="30" src="<?php echo isset($tw_profile_photo) ? $tw_profile_photo : $tw_default_photo ;?>"></span><span style="position: relative;left: -14px;top: 7px;" class="tw-icon"><img width="15px" src="<?php echo base_url()?>/assets/images/twitter-icon.png"></span></li>

                                            <?php   

                                                }
                                        } ?>

                                    <!-- Show Linkedin Profile Photo -->
                                    <?php
                                        if ($ln){

                                                if(isset($linkedin_options)  && !empty($linkedin_options)){

                                                    $ln_profile_photo = $linkedin_options[0]->profile_picture;
                                                    $ln_default_photo = base_url('assets/images/fb-default-photo.jpg');

                                                ?>

                                                        <li><span class="ln-photo"><img width="30" src="<?php echo isset($ln_profile_photo) ? $ln_profile_photo : $ln_default_photo ;?>"></span><span style="position: relative;left: -14px;top: 7px;" class="ln-icon"><img width="15px" src="<?php echo base_url()?>/assets/images/linkedin-icon.png"></span></li>

                                        <?php   }

                                        } ?>
                                </ul>
                            </div>
                            <div class="display-alert">
                                <?php echo $this->session->flashdata('msg'); ?>
                            </div>
                             <?php if(!$fb) { ?>
                                <div class="display-alert col-md-12">
                                    <?php echo $this->session->flashdata('msg-expire'); ?>
                                </div>
                            <?php } else if(!$ln) { ?>
                                <div class="display-alert col-md-12">
                                    <?php echo $this->session->flashdata('msg-expire-linkedin'); ?>
                                </div>        
                            <?php } ?>

                            <p>Choose the property to feature</p>
                            <div class="social-post">
                                <div class="col-md-6 col-md-offset-0 no-padding-left">
                                <form id="propertySelected" method="POST" action="">
                                    <select name="property_selected" id="propertySelected"  class="form-control property-activated">
                                        <option value="">Select Property</option>
                                        <?php
                                        //Custom Domain Option for Socical media Posting
                                        if(isset($domain_info[0]->subdomain_url) || !empty($is_reserved_domain['domain_name'])){

                                            if($domain_info[0]->status == "completed" || $domain_info[0]->is_subdomain == "1") { 

                                                $base = !empty($is_reserved_domain['domain_name']) ? "http://" : "" ;
                                                
                                                $base_url = $base.$is_reserved_domain['domain_name'];

                                             } elseif($domain_info[0]->status != "completed" && $domain_info[0]->is_subdomain == "0") { 

                                                  $base_url = $domain_info[0]->subdomain_url;
                                             } 
                                        }?>

                                        <?php if(!empty($base_url)){?>

                                            <!-- <option value="0"><?php echo $base_url;?></option> -->

                                        <?php }?>

                                        <!-- SPW Created sites option for Social Media Posting -->
                                        <?php if(!empty($spw_properties)){

                                             foreach($spw_properties as $spw_prop) {?>
                                                <option data-type="spw_property" value="<?php echo $spw_prop->ID ?>"><?php echo $spw_prop->unparsedaddr ?> - spw</option>
                                        <?php } 

                                        }?>
                                           
                                        <!-- Active Property option for Social Media Posting -->
                                         <?php

                                         if(!empty($website_type)){
                                           
                                            if($website_type[0]->ApplicationType === "Agent_IDX_Website"){

                                              if(!empty($active_listings)){

                                                foreach($active_listings as $active_list) {?>
                                                     <option data-type="active_property" value="<?php echo $active_list->StandardFields->ListingKey ?>"><?php echo $active_list->StandardFields->UnparsedFirstLineAddress ?> - active</option>
                                            <?php   } 

                                             }
                                           }
                                         }?>
                                            
                                        </select>
                                    </form>  
                                </div>
                            </div>
                            
                            <p>Choose an introduction</p>
                            <div class="social-post">
                                <div class="col-md-offset-0 col-md-6 no-padding-left">
                                    <select name="" id="postSelected" disabled="disabled" onchange="PopulateOption()" class="form-control">
                                        <option id="selected-option1">Check out my new listing!</option>
                                        <option id="selected-option2">Come to my open house!</option>
                                        <option id="selected-option3">Price Reduced</option>
                                        <option id="selected-option4">Sold</option>
                                        <option  value="">Create Custom Post</option>
                                    </select>   
                                </div>    
                            </div>
                        <p class="clearfix"></p>
                        <br>

                        <?php

                        // Open form and set URL for submit form
                        $data= array(
                        'method'=> 'POST',
                        'id'=> 'socialPost',
                        'enctype' => 'mutlipart/form-data'
                        );
                        echo form_open('agent_social_media/social_post',$data);

                        // Post to Facebook
                        $data= array(
                       'type' => 'hidden',
                        'name'=> 'post_facebook',
                        'id'=> 'post_facebook',
                        'value' =>1
                        );
                        echo form_input($data);

                        // Post to Twitter
                        $data= array(
                       'type' => 'hidden',
                        'name'=> 'post_twitter',
                        'id'=> 'post_twitter',
                        'value' =>1
                        );
                        echo form_input($data);

                        // Post to LinkedIn
                        $data= array(
                       'type' => 'hidden',
                        'name'=> 'post_linkedin',
                        'id'=> 'post_linkedin',
                        'value' =>1
                        );
                        echo form_input($data);


                        // Show TextField in View Page
                        $data= array(
                        'name' => 'your_post',
                        'cols'  =>  '25',
                        'rows'  =>  '8',
                        // 'placeholder' => 'Check out my new website http://localhost.com',
                        'value'  =>  '',
                        'id'    =>  'post-area',
                        'class' => 'form-control post-area'
                        );
                        echo form_textarea($data);

                        //Hidden link field : get the value using ajax
                        $data= array(
                        'name' => 'property_link',
                        'cols'  =>  '10',
                        'rows'  =>  '2',
                        'value'  =>  '',
                        'id'    =>  'link-area',
                        'class' => 'form-control hide'
                        );
                        echo form_textarea($data);


                        //Hidden photo field : get the value using ajax
                        $data= array(
                        'name' => 'property_photo',
                        'cols'  =>  '10',
                        'rows'  =>  '2',
                        'value'  =>  '',
                        'id'    =>  'photo-area',
                        'class' => 'form-control hide'
                        );
                        echo form_textarea($data);

                         //Hidden title field : get the value using ajax
                        $data= array(
                        'name' => 'property_title',
                        'cols'  =>  '10',
                        'rows'  =>  '2',
                        'value'  =>  '',
                        'id'    =>  'title-area',
                        'class' => 'form-control hide'
                        );
                        echo form_textarea($data);

                    ?>

                    <div id="myModal" class="modal fade" role="dialog">
                        <div class="modal-dialog">
                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <p class="preview-text-post"></p>
                            </div>
                            <div class="modal-body">
                                <div id='preview-post'>
                                    <div id='preview-img'></div>
                                    <div id='preview-text'>
                                        <p class="property-address"></p>
                                        <p class="property-description"></p>
                                        <p class="property-broker"></p>
                                        <p style="color:#5C97BF;" class="property-url"></p> 
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <p>
                                    <button type="button" class="btn btn-reset btn-cancel btn-warning" data-dismiss="modal">Cancel</button>
                                    <button class='btn btn-reset btn-save-modal btn-success' data-loading-text="<i class='fa fa-spinner fa-spin'></i> Loading..."><span class="spinner"><i class="icon-spin icon-refresh"></i></span>Post</button>
                                </p>
                            </div>
                        </div>
                      </div>
                    </div>
                    <br/>
                     <?php echo form_close();?>
                    <div class="form_button">
                        <button id="btn-preview-post" class='btn btn-preview btn-info' data-toggle="modal" data-target="#myModal">
                            <i class="fa fa-eye"></i> Preview
                        </button>
                    </div>


                    </div>
                </div>
            </section>
        </div>

        <?php $this->load->view('session/footer');?>

        <script type="text/javascript">
            $(".is_selected_post").on("click", function (e) {
               
                var id = $(this).data("id");
                var val = $(this).val();
                var url = base_url + "agent_social_media/is_page_selected";

                $.ajax({
                        url : url,
                        type : "post",
                        data : {"is_selected" : val, "facebook_id" : id},
                        dataType: "json",                
                        success: function( data ){ 

                        console.log(data); 
                        if(data.success)
                        {
                            $("#is_page_selected_"+id).val(data.val);
                            // location.reload(true);
                              setTimeout(function(){location.reload();},3000);
                        }       
                            
                    }
                });
            });
        
        </script>

     
    