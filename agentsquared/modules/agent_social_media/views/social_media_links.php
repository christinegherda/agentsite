<?php 
    $this->load->view('session/header'); 
    $this->load->view('session/top-nav');
    // $this->load->view('session/left-nav');
?> 
<!-- Full Width Column -->
<div class="content-wrapper">
  <div class="page-title">
    <h3 class="heading-title">
      <span>Social Media Links</span>
    </h3>
    <p>Add your social media links</p>
  </div>
  <!-- Main content -->
  <section class="content">
                <div class="row">
                    <div class="featured-list">
                        <div class="col-md-12">
                            <h2></h2>    
                        </div>
                    </div>
                    <div class="col-md-6">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Social Media Links
                  <span class="social-media-note">(add your social media links here)</span></h3>
                </div>
                <!-- /.box-header -->
                  <div class="box-body">
                  <span class="display-status alert alert-success" style="display:none;"></span>
                    <div class="social-media-form form-horizontal">

                        <div class="form-group facebook-url-form">
                            <form method="post" id="facebookUrlForm" action="<?php echo site_url('agent_social_media/add_links'); ?>">
                                <div class="row">
                                    <div class="col-sm-1 col-xs-1">
                                        <label for="facebook_url" class="col-sm-1 control-label"><i class="fa fa-facebook"> </i></label>
                                    </div>
                                    <div class="col-sm-10 col-xs-10">
                                        <div class="input-group">
                                            <span class="input-group-addon">http://</span>
                                            <input type="text" class="form-control" id="facebook_url" name="facebook_url" value="<?php echo $facebook; ?>">
                                        </div>
                                    </div>
                                </div>
                                

                                <!-- <div class="col-sm-2"><button type="submit" class="btn btn-default btn-submit"> <i style="display:none" class="facebook-loading fa fa-spinner fa-spin"> </i> Save</button></div> -->
                            </form>
                        </div>
                        <div class="form-group twitter-url-form">
                            <form method="post" id="twitterUrlForm" action="<?php echo site_url('agent_social_media/add_links'); ?>">

                                <div class="row">
                                    <div class="col-sm-1 col-xs-1">
                                        <label for="twitter_url" class="col-sm-1 control-label"><i class="fa fa-twitter"> </i></label>
                                    </div>
                                    <div class="col-sm-10 col-xs-10">
                                        <div class="input-group">
                                            <span class="input-group-addon">http://</span>
                                            <input type="text" class="form-control" id="twitter_url" name="twitter_url" value="<?php echo $twitter; ?>">
                                        </div>
                                    </div>
                                </div>
                                <!-- <div class="col-sm-2"><button type="submit" class="btn btn-default btn-submit"><i style="display:none" class="twitter-loading fa fa-spinner fa-spin"> </i> Save</button></div> -->
                            </form>
                        </div>

                        <div class="form-group linkedin-url-form">
                            <form method="post" id="linkedinUrlForm" action="<?php echo site_url('agent_social_media/add_links'); ?>">
                                <div class="row">
                                    <div class="col-sm-1 col-xs-1">
                                        <label for="linkedin_url" class="col-sm-1 control-label"><i class="fa fa-linkedin"> </i></label>
                                    </div>
                                    <div class="col-sm-10 col-xs-10">
                                        <div class="input-group">
                                            <span class="input-group-addon">http://</span>
                                            <input type="text" class="form-control" id="linkedin_url" name="linkedin_url" value="<?php echo $linkedin; ?>">
                                        </div>
                                    </div>
                                </div>
                                <!-- <div class="col-sm-2"><button type="submit" class="btn btn-default btn-submit"><i style="display:none" class="linkedin-loading fa fa-spinner fa-spin"> </i> Save</button></div> -->
                            </form>
                        </div>

                        <div class="form-group googleplus-url-form">
                            <form method="post" id="googleplusUrlForm" action="<?php echo site_url('agent_social_media/add_links'); ?>">
                                <div class="row">
                                    <div class="col-sm-1 col-xs-1">
                                        <label for="googleplus_url" class="col-sm-1 control-label"><i class="fa fa-google-plus"> </i></label>
                                    </div>
                                    <div class="col-sm-10 col-xs-10">
                                        <div class="input-group">
                                            <span class="input-group-addon">http://</span>
                                            <input type="text" class="form-control" id="googleplus_url" name="googleplus_url" value="<?php echo $googleplus; ?>">
                                        </div>
                                    </div>
                                </div>
                                <!-- <div class="col-sm-2"><button type="submit" class="btn btn-default btn-submit"><i style="display:none" class="googleplus-loading fa fa-spinner fa-spin"> </i> Save</button></div> -->
                            </form>
                        </div>
                    </div>
                  </div>
                  <!-- /.box-body -->
              </div>
              <!-- /.box -->

                    </div>
                </div>
    </section>
</div>
        
<?php $this->load->view('session/footer'); ?>
