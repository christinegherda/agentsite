<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------
|  Facebook App details
| -------------------------------------------------------------------
|
| To get an facebook app details you have to be a registered developer
| at http://developer.facebook.com and create an app for your project.
|
|  facebook_app_id               string  Your facebook app ID.
|  facebook_app_secret           string  Your facebook app secret.
|  facebook_login_type           string  Set login type. (web, js, canvas)
|  facebook_login_redirect_url   string  URL tor redirect back to after login. Do not include domain.
|  facebook_logout_redirect_url  string  URL tor redirect back to after login. Do not include domain.
|  facebook_permissions          array   The permissions you need.
|  facebook_graph_version        string  Set Facebook Graph version to be used.
*/

$config['facebook_app_id']              = '867016703396837'; // AgentSquared APP ID.
$config['facebook_app_secret']          = 'd08f2f4416bd0823e6b93dedba356613'; // AgentSquared App Secret.
$config['facebook_login_type']          = 'web';
$config['facebook_graph_version']       = 'v3.2';
$config['facebook_login_redirect_url']  = 'agent_social_media/facebook_login';
$config['facebook_logout_redirect_url'] = 'agent_social_media/deactivate_facebook';
$config['facebook_permissions']         = ['public_profile','email','publish_to_groups','manage_pages','pages_show_list','publish_pages'];

