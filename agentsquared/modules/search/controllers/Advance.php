<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Advance extends MX_Controller {

	protected $domain_user;

	function __construct() {
		parent::__construct();

		$this->domain_user = domain_to_user();
		$userData = $this->domain_user->id; 
		// $this->domain_user->id;

        // if(empty($userData)) {
        //     $this->session->set_flashdata('flash_data', 'You don\'t have access!');
        //     redirect('login/auth/login', 'refresh');
        // }
        
		$this->load->model("dsl/dsl_model","dsl_model");
		$this->load->model("home/Page_model","page");
		$this->load->model("search_model");

		$this->load->model("home/Meta_model");
 		$this->load->model("home/Blog_model","blog");
 		$this->load->model("home/Home_model", "home");
		$this->load->library("idx_auth", $this->domain_user->agent_id);
	}
	public function check_advance_search($mls_id = NULL){
		$ret = false;
		if($mls_id) {

			$isHere = $this->db->select("*")->from("mls_data_search_fields")->where("mls_id", $mls_id)->limit(1)->get()->result();
			echo count($isHere);
			if(count($isHere)) {
				// Account Already has registered Data
				$ret = true;
			}
		}
		return $ret;
	}
	public function migrate_search_fields() {
		$mysql_properties = Modules::load("mysql_properties/mysql_properties");
		$dsl = Modules::load("dsl/dsl");
		$system_info = $mysql_properties->get_account($this->domain_user->id, "system_info");
		// Check if MlsId already migrated from system
		$isHere = $this->db->select("*")->from("mls_data_search_fields")->where("mls_id", $system_info->MlsId)->limit(1)->get()->result();
		if(count($isHere) > 0) {
			// Account Already has registered Data
			echo "You already here bruh";
		}
		else {
			// Do it here
			$standardFields = $dsl->getSparkProxy('standardfields');
			// $standardOrders = $dsl->getSparkProxy('fields/order');
			// $SearcheableFieldsVariables = array();
			// // echo "<pre>";
			// // var_dump($standardOrders[0]);
			// // echo "</pre>";
			echo "Migrating..";
			foreach($standardFields[0] as $sf) {
				if($sf->Searchable) {
					
					$field = str_replace("/v1/standardfields/", "", $sf->ResourceUri); 
					echo $this->register_field_v2($field)."<br>..";
				}
			}

		}
	}
	public function post_register_field() {
		/* Register Field Manually for fields that cannot be escaped */

		$this->load->view('static/post_advance_search_field_data');
	}
	public function register_field_v2($field_name = null, $field_type = 'standardfields') {
		if($field_name == null) {
			$field_name = $this->input->post('field_name');
			echo "field: ".$field_name."<br>";

			$field_type = $this->input->post('field_type');
		}
		$ret = array(
			'status' => false, 
			'message' => 'Invalid Field Name / Field not found on system'
		);
		if($field_name && $field_type) {
			/* Make API Call here */
			$dsl = modules::load("dsl");

			$token = $this->dsl_model->getUserAccessToken($this->domain_user->agent_id);
			$access_token = array(
				'access_token' 	=> $token->access_token,
				'field_name'	=> $field_name,
				'field_type'	=> $field_type
			);
			
			$endpoint = "spark-fields";
			$field = $dsl->post_endpoint($endpoint,$access_token);
			$field = json_decode($field);
			
			$endpoint = "agent/".$this->domain_user->id."/account-info";
			$agent = $dsl->get_endpoint($endpoint);
			$mls_id = $this->domain_user->mls_id;
			$agentDataObj = json_decode($agent);
	
			if (isset($agentDataObj->MlsId)) {
				$mls_id = $agentDataObj->MlsId;
			}

			$fieldItemName = urldecode($field_name);

			if(isset($field->status) && ($field->status == 200)) {
				$fieldData = $field->data;
				if($fieldData) {
					if(isset($fieldData[0]) && !empty($fieldData[0])) { // Check if field has options
						if($fieldData[0]->{"{$fieldItemName}"}->Searchable) {
							/* Save field to database */
							$data = array(
								'field_name' 		=> urldecode($field_name),
								'field_datatype' 	=> $fieldData[0]->{"{$fieldItemName}"}->Type,
								'field_result_json' => json_encode($fieldData[0]),
								'field_haslist'		=> $fieldData[0]->{"{$fieldItemName}"}->HasList,
								'field_category'	=> $fieldData[0]->{"{$fieldItemName}"}->FieldCategory,
								'field_label'		=> $fieldData[0]->{"{$fieldItemName}"}->Label,
								
								'field_max_size'	=> $fieldData[0]->{"{$fieldItemName}"}->MaxListSize,
								'field_multiselect'	=> isset($fieldData[0]->{"{$fieldItemName}"}->MultiSelect) ? $fieldData[0]->{"{$fieldItemName}"}->MultiSelect: null,
								'field_type'		=> $field_type,
								'mls_id'			=> $mls_id
							);

							$dataInserted = $this->search_model->register_search_fields($data);
							$ret = array(
								'status' 		=> true, 
								'message' 		=> 'Field exist',
								'is_registerd' 	=> $dataInserted
							);

							/*Register Fields List Items*/
							if(isset($fieldData[0]->{"{$fieldItemName}"}->FieldList)) {
								// $fieldData[0]->{"{$fieldItemName}"}->FieldList
								foreach($fieldData[0]->{"{$fieldItemName}"}->FieldList as $flItems) {
									$dataFields = array(
													'mls_searc_field_id' 	=> $dataInserted,
													'field_name' 			=> $flItems->Name,
													'field_value' 			=> $flItems->Value,
													'field_applies_to_json' => json_encode($flItems->AppliesTo),
												);
									$this->search_model->register_mls_data_search_fields($dataFields);
								}
							}
						}
						else {
							$ret['message'] = 'Field is not searcheable';
						}
					}
					else {
						$ret['message'] = 'Field has no options';
					}
				}
			}
		}
		echo json_encode($ret);		
	}
	public function register_field($field_name = null, $field_type = 'standardfields') {
		if($field_name == null) {
			$field_name = $this->input->post('field_name');
			echo "field: ".$field_name."<br>";

			$field_type = $this->input->post('field_type');
		}
		$ret = array(
			'status' => false, 
			'message' => 'Invalid Field Name / Field not found on system'
		);
		if($field_name && $field_type) {
			/* Make API Call here */
			$dsl = modules::load("dsl");

			$token = $this->dsl_model->getUserAccessToken($this->domain_user->agent_id);
			$access_token = array(
				'access_token' 	=> $token->access_token,
				'field_name'	=> $field_name,
				'field_type'	=> $field_type
			);
			
			$endpoint = "spark-fields";
			$field = $dsl->post_endpoint($endpoint,$access_token);
			$field = json_decode($field);
			
			$endpoint = "agent/".$this->domain_user->id."/account-info";
			$agent = $dsl->get_endpoint($endpoint);

			$agentDataObj = json_decode($agent);
			$fieldItemName = urldecode($field_name);

			if(isset($field->status) && ($field->status == 200)) {
				$fieldData = $field->data;
				if($fieldData) {
					if(isset($fieldData[0]) && !empty($fieldData[0])) { // Check if field has options
						if($fieldData[0]->{"{$fieldItemName}"}->Searchable) {
							/* Save field to database */
							$data = array(
								'field_name' 		=> urldecode($field_name),
								'field_datatype' 	=> $fieldData[0]->{"{$fieldItemName}"}->Type,
								'field_result_json' => json_encode($fieldData[0]),
								'field_haslist'		=> $fieldData[0]->{"{$fieldItemName}"}->HasList,
								'field_category'	=> $fieldData[0]->{"{$fieldItemName}"}->FieldCategory,
								'field_label'		=> $fieldData[0]->{"{$fieldItemName}"}->Label,
								'field_list_items'	=> json_encode(isset($fieldData[0]->{"{$fieldItemName}"}->FieldList) ? $fieldData[0]->{"{$fieldItemName}"}->FieldList: null),
								'field_max_size'	=> $fieldData[0]->{"{$fieldItemName}"}->MaxListSize,
								'field_multiselect'	=> isset($fieldData[0]->{"{$fieldItemName}"}->MultiSelect) ? $fieldData[0]->{"{$fieldItemName}"}->MultiSelect: null,
								'field_type'		=> $field_type,
								'mls_id'			=> $agentDataObj->MlsId
							);

							$dataInserted = $this->search_model->register_search_fields($data);
							$ret = array(
								'status' 		=> true, 
								'message' 		=> 'Field exist',
								'is_registerd' 	=> $dataInserted
							);
						}
						else {
							$ret['message'] = 'Field is not searcheable';
						}
					}
					else {
						$ret['message'] = 'Field has no options';
					}
				}
			}
		}
		echo json_encode($ret);
	}

	public function properties_ajax($limit = 20, $page  = 1, $search = '') {
		$dsl = modules::load('dsl/dsl');
		$obj = Modules::load("mysql_properties/mysql_properties");
		$gsf_fields = $this->home->get_gsf_fields($this->domain_user->agent_id);
		$account_info = $obj->get_account($this->domain_user->id, "account_info");
		$account_meta = $this->idx_auth->get('accounts/meta');
		$mls_status = Modules::run('search/mlsStatusFilterString');
		$token = $this->dsl_model->getUserAccessToken($this->domain_user->agent_id);
		$access_token = array('access_token' => $token->access_token);

		if(isset($account_meta["0"]["Mls"]) && !empty($account_meta["0"]["Mls"])){
			$ix = 0;
			foreach ($account_meta["0"]["Mls"] as $Ids) {
				if($ix === 0){
					$mlsIds = "'".$Ids["Id"]."'";
				}else{
					$mlsIds .= ", '".$Ids["Id"]."'";
				}
				$ix++;
			}
			$filter = "MlsId Eq ".$mlsIds."  And ";
		}
		else{
			$filter = "MlsId Eq '".$account_info->MlsId."'  And ";
		}
		$filter .= "(".$mls_status.") And (PhotosCount Gt 0)";
		$standardField = array();
		$standardFieldData = array();
		$filterGeo = '';
		if(isset($gsf_fields['gsf_standard_fields']) && empty($gsf_fields['gsf_standard_fields'])){
			if($search = $this->input->get('Search') && !empty($_GET["Search"])) {
				if($this->input->post('search') || $search) {
					$search = $this->input->post('search') ? $this->input->post('search') : $search;
					$searchobj = isset($search['Search']) ? $search['Search'] : $search ;
					
					if(isset($search_fields_list_data)){
						foreach($search_fields_list_data as $sfd) {
							$strHas = strcmp(strtolower($sfd->field_value),strtolower($searchobj));
							if ($strHas == 0) {
								$standardField[] = $sfd->mls_searc_field_id;
							}
						}
					}
					if(isset($search_fields)){
						foreach($search_fields as $sf) {
							foreach($standardField as $sdf) {
								if($sdf == $sf->id) {
									$standardFieldData[] = $sf->field_name;
								}
							}
						}
					}

					foreach($standardFieldData as $standdata) {
						$filter .= " And ".$standdata." Eq '".$searchobj."'";
					}
					if((count($standardField) == 0) && (count($standardFieldData) == 0)) {
						$filter .= " And (UnparsedAddress Eq '*".str_replace(' ', '*', $searchobj)."*')";
					}
				}
			}
		}else{
			$gsf_standard_fields = json_decode($gsf_fields['gsf_standard_fields']);
			if(isset($gsf_standard_fields->PropertyClass)){
				$filter .= " And ( ";
				for ($i=0; $i < count($gsf_standard_fields->PropertyClass); $i++) { 
					if($i == 0){
						$filter .= "PropertyClass Eq '*".$gsf_standard_fields->PropertyClass[$i]."*'";
					}else{
						$filter .= " Or PropertyClass Eq '*".$gsf_standard_fields->PropertyClass[$i]."*'";						
					}
				}
				$filter .= " )";				
			}else{
				if($account_info->MlsId == '20040823195642954262000000'){
					$filter .= " And (PropertyType Ne 'F') ";
				}else{
					$filter .= " And (PropertyClass Ne '*Rental*') ";
				}
			}

			if(isset($gsf_standard_fields->City)){
				for ($i=0; $i < count($gsf_standard_fields->City); $i++) { 
					$filterGeo .= ($filterGeo != '') ? " Or City Eq '".str_replace("_", " ", $gsf_standard_fields->City[$i])."'" : " City Eq '".str_replace("_", " ", $gsf_standard_fields->City[$i])."' ";
				}						
			}

			if(isset($gsf_standard_fields->CountyOrParish)){				
				for ($i=0; $i < count($gsf_standard_fields->CountyOrParish); $i++) { 
					$filterGeo .= ($filterGeo != '') ? " Or CountyOrParish Eq '".str_replace("_", " ", $gsf_standard_fields->CountyOrParish[$i])."'" : " CountyOrParish Eq '".str_replace("_", " ", $gsf_standard_fields->CountyOrParish[$i])."' ";					
				}				
			}

			if(isset($gsf_standard_fields->PostalCode)){
				for ($i=0; $i < count($gsf_standard_fields->PostalCode); $i++) { 
					$filterGeo .= ($filterGeo != '') ? " Or PostalCode Eq '".$gsf_standard_fields->PostalCode[$i]."'" : " PostalCode Eq '".$gsf_standard_fields->PostalCode[$i]."' ";
				}							
			}
		}

		if($filterGeo != ''){
			$filter .= " AND (".$filterGeo.")";
		}
		if(isset($gsf_fields['gsf_custom_fields']) && !empty($gsf_fields['gsf_custom_fields'])){
			$filter .= " AND ".$gsf_fields["gsf_custom_fields"];
		}
		
		$order = "-OnMarketDate";
		if($account_info->MlsId == '20050607154153857879000000'){
			$order = "+YearBuilt";
		}
		
		$param_select = "PropertySubType,PropertyClass,Photos(1).Uri300,UnparsedFirstLineAddress,PublicRemarks,UnparsedAddress,City,CountyOrParish,StateOrProvince,PostalCode,PostalCity,MlsStatus,CurrentPrice,BedsTotal,BathsTotal,LotSizeArea,Latitude,Longitude,LotSizeUnits,LotSizeSquareFeet,LotSizeAcres,ListingKey,ListAgentId,LotSizeDimensions,ListingUpdateTimestamp,ModificationTimestamp,MajorChangeTimestamp,BuildingAreaTotal,MlsId,BathsFull,BathsHalf";
		
		$results = $dsl->getSparkProxyPagination($page, $limit, urlencode($filter), false, $order, $param_select);

		// printA($results);

		if($results->Success) {
			exit(json_encode($results));
		}
		else {
			exit(json_encode(array('Success'=>false, 'Filter'=>$filter)));
		}
		
	}

	public function advance_search_ajax($limit = 20, $page  = 1, $search = '') {
		$get_search = $this->input->get('search') ? $this->input->get('search') : $search;
		$custom_fields = $this->home->get_static_custom_fields($this->domain_user->agent_id);
		$dsl = modules::load('dsl/dsl');
		$obj = Modules::load("mysql_properties/mysql_properties");
		$account_info = $obj->get_account($this->domain_user->id, "account_info");

		$this->load->model("search/search_model","search_model");

		$search_fields = $this->search_model->get_mls_search_data($account_info->MlsId);
		$search_fields_groups = $this->search_model->get_mls_search_group($account_info->MlsId);


		$search_fields_list_data = $this->search_model->get_mls_data_search_fields_list_data(NULL,$search_fields);
		$property_types 	= Modules::run('dsl/get_property_types');
		$property_subtypes 	= Modules::run('dsl/get_property_subtypes');
		$mls_status = Modules::run('search/mlsStatusFilterString');

		$account_meta = $this->idx_auth->get('accounts/meta');

		if(isset($_GET['debugger'])){
			printA($account_meta);
		}

		if(isset($account_meta["0"]["Mls"]) && !empty($account_meta["0"]["Mls"])){
			$ix = 0;
			foreach ($account_meta["0"]["Mls"] as $Ids) {
				if($ix === 0){
					$mlsIds = "'".$Ids["Id"]."'";
				}else{
					$mlsIds .= ", '".$Ids["Id"]."'";
				}
				$ix++;
			}
			$filterStr = "MlsId Eq ".$mlsIds."  And ";
		}
		else{
			$filterStr = "MlsId Eq '".$data['account_info']->MlsId."'  And ";
		}
		

		$filter = "(".$mls_status.") And PhotosCount Gt 0";
		$filterStr .= '(PhotosCount Gt 0) And ';

		if(isset($get_search['MlsStatus'])) {
			$filterStr .= "(MlsStatus Eq '".$get_search['MlsStatus']."')";
		}
		else {
			$filterStr .= Modules::run('search/mlsStatusFilterString');
		}
		if(isset($get_search['PropertyType'])) {
			$filterStr .= (empty($filterStr)) ? "(PropertyType Eq '".$get_search['PropertyType']."')": " And (PropertyType Eq '".$get_search['PropertyType']."')";
		}
		if(isset($get_search['PropertyClass'])) {
			$filterStr .= (empty($filterStr)) ? "(PropertyClass Eq '".$get_search['PropertyClass']."')": " And (PropertyClass Eq '".$get_search['PropertyClass']."')";
		}
		if(isset($get_search['PropertySubType'])) {
			$filterStr .= (empty($filterStr)) ? "(PropertySubType Eq '".$get_search['PropertySubType']."')": " And (PropertySubType Eq '".$get_search['PropertySubType']."')";
		}

		/* Standard Fields */
		$filterStrP1 = '';
		$standardField = array();
		$standardFieldData = array();
		if($search = $this->input->get('Search')){
			if(isset($search_fields_list_data) && !empty($search_fields_list_data)){
				foreach($search_fields_list_data as $sfd) {
					$strHas = strcmp(strtolower($sfd->field_value),strtolower($search));
					if ($strHas == 0) {
						$standardField[] = $sfd->mls_searc_field_id;
					}
				}
			}
			if(isset($data['search_fields']) && !empty($data['search_fields'])){
				foreach($data['search_fields'] as $sf) {
					foreach($standardField as $sdf) {
						if($sdf == $sf->id) {
							$standardFieldData[] = $sf->field_name;
						}
					}
				}
			}

			$stData = "";
			$iCount = 0;
			foreach($standardFieldData as $standdata) {
				// $filterStr .= " And ".$standdata." Eq '".$search."'";
				if($iCount == 0) {
					$stData = " And (".$standdata." Eq '".$get_search["Search"]."')";
				}
				$iCount++;
			}
			$filterStr .= $stData;

			if((count($standardField) == 0) && (count($standardFieldData) == 0)) {
				$filterStr .= " And (UnparsedAddress Eq '*".str_replace(' ', '*', $get_search["Search"])."*')";
			}
		}
		foreach($_GET['search'] as $key=>$val) {
			if(!empty($val)) {
				switch($key) {
					case 'City':
						$filterStrP1 .= ((empty($filterStrP1)) ? "" : " Or ")."City Eq '".$val."'";
					break;
					case 'PostalCode':
						$filterStrP1 .= ((empty($filterStrP1)) ? "" : " Or ")."PostalCode Eq '".$val."'";
					break;
					case 'CountyOrParish':
						$filterStrP1 .= ((empty($filterStrP1)) ? "" : " Or ")."CountyOrParish Eq '".$val."'";
					break;
				}
			}
		}
		$filterStrP2 = '';
		foreach($_GET['search'] as $key=>$val) {
			if(!empty($val)) {
				switch($key) {
					case 'HighSchool':
						$filterStrP2 .= ((empty($filterStrP2)) ? "" : " And ")."HighSchool Eq '".$val."'";
					break;
					case 'MiddleOrJuniorSchool':
						$filterStrP2 .= ((empty($filterStrP2)) ? "" : " And ")."MiddleOrJuniorSchool Eq '".$val."'";
					break;
					case 'ElementarySchool':
						$filterStrP2 .= ((empty($filterStrP2)) ? "" : " And ")."ElementarySchool Eq '".$val."'";
					break;
				}
			}
		}
		if($filterStrP1 != '') {
			$filterStr .= ' And ('.$filterStrP1.')';
		}
		if($filterStrP2 != '') {
			$filterStr .= ' And ('.$filterStrP2.')';
		}

		foreach($_GET['search'] as $key=>$val) {
			if(!empty($val)) {
				switch($key) {
					case 'StreetNumber':
						$filterStr .= ((empty($filterStr)) ? "" : " And ")."StreetNumber Eq '".$val."'";
					break;
					case 'StreetName':
						$filterStr .= ((empty($filterStr)) ? "" : " And ")."StreetName Eq '*".str_replace(' ', '*', $val)."*'";
					break;
					case 'Country':
						$filterStr .= ((empty($filterStr)) ? "" : " And ")."Country Eq '".$val."'";
					break;
					case 'price_min':
						$filterStr .= ((empty($filterStr)) ? "" : " And ")."ListPrice Ge ".$val."";
					break;
					case 'price_max':
						$filterStr .= ((empty($filterStr)) ? "" : " And ")."ListPrice Le ".$val."";
					break;
					case 'area_min':
						$filterStr .= ((empty($filterStr)) ? "" : " And ")."Approx Lot SqFt Ge ".$val."";
					break;
					case 'area_max':
						$filterStr .= ((empty($filterStr)) ? "" : " And ")."Approx Lot SqFt Le ".$val."";
					break;
					case 'house_min':
						$filterStr .= ((empty($filterStr)) ? "" : " And ")."BuildingAreaTotal Ge ".$val."";
					break;
					case 'house_max':
						$filterStr .= ((empty($filterStr)) ? "" : " And ")."BuildingAreaTotal Le ".$val."";
					break;
					case 'BedsTotal':
						$filterStr .= ((empty($filterStr)) ? "" : " And ")."BedsTotal Ge ".$val."";
					break;
					case 'BathsTotal':
						$filterStr .= ((empty($filterStr)) ? "" : " And ")."BathsTotal Ge ".$val."";
					break;
					case 'BathsFull':
						$filterStr .= ((empty($filterStr)) ? "" : " And ")."BathsFull Ge ".$val."";
					break;
					case 'Pool':
						$filterStr .= ((empty($filterStr)) ? "" : " And ")."PoolFeatures Eq '".$val."'";
					break;
					case 'YearBuilt':
						$filterStr .= ((empty($filterStr)) ? "" : " And ")."YearBuilt Eq ".$val."";
					break;
					case 'GarageSpaces':
						$filterStr .= ((empty($filterStr)) ? "" : " And ")."GarageSpaces Eq ".$val."";
					break;
					case 'CarportSpaces':
						$filterStr .= ((empty($filterStr)) ? "" : " And ")."CarportSpaces Eq ".$val."";
					break;
					case 'Slab_Parking_Spaces':
						$filterStr .= ((empty($filterStr)) ? "" : " And ").'"Parking Spaces"."Slab Parking Spaces" Eq '.$val;
					break;
					case 'RangeOven_Elec':
						$filterStr .= ((empty($filterStr)) ? "" : " And ").'"Kitchen Features"."RangeOven Elec" Eq '.$val;
					break;
					case 'Roof':
						if(count($val)) {
							$filterStr .= ((empty($filterStr)) ? "" : " And ")."Roof Eq ";
							$roof_items = "";
							foreach($val as $v) {
								if(empty($roof_items)) {
									$roof_items .= "'".$v."'";
								}
								else {
									$roof_items .= ",'".$v."'";
								}
							}
							$filterStr .= $roof_items;
						}
					break;
				}
			}
		}

		if($custom_fields){
			$filterStr .= " AND $custom_fields ";
		}

		$order = "-OnMarketDate";
 		if($_GET["order"] == 'highest') {
 			$order = "-ListPrice";
 		}
 		if($_GET["order"] == 'lowest') {
 			$order = "+ListPrice";
 		}
 		if($_GET["order"] == 'newest') {
 			$order = "-OnMarketDate";
 		}
 		if($_GET["order"] == 'oldest') {
 			$order = "+YearBuilt";
 		}

		// echo "<pre>";
		// var_dump($filterStr);
		// echo "</pre>";
		// exit;		
		#$filter .= " And PhotosCount Gt 0";
		$results = $dsl->getSparkProxyPagination($page, $limit, urlencode($filterStr), false, $order);
		// echo "----";
		// echo "<pre>";
		// var_dump($filter);
		// var_dump($results);
		// echo "</pre>";
		if($results->Success) {
			exit(json_encode($results));
		}
		else {
			exit(json_encode(array('Success'=>false, 'Filter'=>$filter)));
		}
		
	}

	public function new_advance_search_ajax($limit = 20, $page  = 1, $search = '') {
		$get_search = $this->input->get();
		$gsf_fields = $this->home->get_gsf_fields($this->domain_user->agent_id);
		$dsl = modules::load('dsl/dsl');
		$obj = Modules::load("mysql_properties/mysql_properties");
		$account_info = $obj->get_account($this->domain_user->id, "account_info");
		//set cookies for keywords
		$count = (isset($_COOKIE["keywords_".$account_info->MlsId])) ? count($_COOKIE["keywords_".$account_info->MlsId]) : 0;
		if(!isset($_COOKIE["keywords_".$account_info->MlsId])){
			if(!isset($get_search['g_street_number']) || !isset($get_search['g_route']) || !isset($get_search['g_locality'])){
				if(isset($get_search["Search"])) {
					setcookie("keywords_".$account_info->MlsId."[]", $get_search["Search"], time() + (86400 * 360), "/");
				}
			}
		}else{
			if(!isset($get_search['g_street_number']) || !isset($get_search['g_route']) || !isset($get_search['g_locality'])){
				if(isset($get_search["Search"]) && !in_array($get_search["Search"], $_COOKIE["keywords_".$account_info->MlsId])){
					$count = $count + 1;
					setcookie("keywords_".$account_info->MlsId."[".$count."]", $get_search["Search"], time() + (86400 * 360), "/");
				}
			}					
		}
		$this->load->model("search/search_model","search_model");
		$search_fields = $this->search_model->get_mls_search_data($account_info->MlsId);

		$search_fields_list_data = $this->search_model->get_mls_data_search_fields_list_data(NULL,$search_fields);		
		$mls_status = Modules::run('search/mlsStatusFilterString');

		$account_meta = $this->idx_auth->get('accounts/meta');
		if(isset($_GET['debugger'])){
			printA($account_meta);
		}
		
		if(isset($account_meta["0"]["Mls"]) && !empty($account_meta["0"]["Mls"])){
			$ix = 0;
			foreach ($account_meta["0"]["Mls"] as $Ids) {
				if($ix === 0){
					$mlsIds = "'".$Ids["Id"]."'";
				}else{
					$mlsIds .= ", '".$Ids["Id"]."'";
				}
				$ix++;
			}
			$filterStr = "MlsId Eq ".$mlsIds."  And ";
		}
		else{
			$filterStr = "MlsId Eq '".$account_info->MlsId."'  And ";
		}

		$filter = "(".$mls_status.") And PhotosCount Gt 0";
		$filterStr .= '(PhotosCount Gt 0) And ';

		if(isset($get_search['MlsStatus'])) {
			
			$s = 0;
			foreach ($get_search["MlsStatus"] as $statues => $status) {
				if($s == 0){
					$mlsStatus = "'".$status."'";
				}else{
					$mlsStatus .= ", '".$status."'";
				}
				$s++;
			}
			$filterStr .= "(MlsStatus Eq ".$mlsStatus." )";	
		}
		else {
			$filterStrStatus = Modules::run('search/mlsStatusFilterString');
			$filterStr .= "(".$filterStrStatus.")";	
		}
		
		// if(isset($get_search['PropertyType'])) {
		// 	$filterStr .= (empty($filterStr)) ? "(PropertyType Eq '".$get_search['PropertyType']."')": " And (PropertyType Eq '".$get_search['PropertyType']."')";
		// }
		
		if(isset($get_search['PropertyClass']) && !empty($get_search['PropertyClass']))
		{
			$count_type = 1;
			$filterStr .= " AND ( ";
			foreach ($get_search['PropertyClass'] as $key => $value) {
				if($count_type==1)
					$filterStr .= " PropertyClass Eq '*".$value."*'";
				else
					$filterStr .= " Or PropertyClass Eq '*".$value."*'";

				$count_type++;
			}
			$filterStr .= " )";
		}else{
				//for AKMLS ONLY not include some subtype
			if($account_info->MlsId == '20040823195642954262000000'){
				if(isset($_GET["Search"]) && empty($_GET["Search"]) && isset($_GET["polygonval"]) && empty($_GET["polygonval"])){
					$filterStr .= " And (PropertyType Ne '*F*')";	
				}
			}else{
				if(isset($_GET["Search"]) && empty($_GET["Search"]) && isset($_GET["polygonval"]) && empty($_GET["polygonval"])){
					$filterStr .= " And (PropertyClass Ne '*Rental*')";	
				}
			}
			

			
		}

		if(isset($get_search['PropertyType']) && !empty($get_search['PropertyType']))
		{
			$count_type = 1;
			$filterStr .= " AND ( ";
			foreach ($get_search['PropertyType'] as $key => $value) {
				if($count_type==1)
					$filterStr .= " PropertyType Eq '".$value."'";
				else
					$filterStr .= " Or PropertyType Eq '".$value."'";

				$count_type++;
			}
			$filterStr .= " )";
		}

		if(isset($get_search['PropertySubType']) && !empty($get_search['PropertySubType']))
		{	
			$count_subtype = 1;
			$filterStr .= " AND ( ";
			foreach ($get_search['PropertySubType'] as $key => $value) {
				if($count_subtype==1)
					$filterStr .= " PropertySubType Eq '*".$value."*'";
				else
					$filterStr .= " Or PropertySubType Eq '*".$value."*'";

				$count_subtype++;
			}
			$filterStr .= " )";
		}

		//for AKMLS ONLY not include some subtype
		// if($account_info->MlsId == '20040823195642954262000000'){
		// 	$filterStr .= " And (CurrentPrice GT 10)";
		// }
		
		/* Standard Fields */
		$filterStrP1 = '';
		$standardField = array();
		$standardFieldData = array();
		$ListingId = array();
		
		if($search = $this->input->get('Search') && !empty($_GET["Search"])) {
			//cabo = 20081211185657097606000000
			$mlsNoStandardResults = array('20081211185657097606000000','20140801122353246389000000','20140411164716491615000000','20050607154153857879000000');
			if(!in_array($account_info->MlsId, $mlsNoStandardResults)){

				if(isset($search_fields_list_data) && !empty($search_fields_list_data)){
					foreach($search_fields_list_data as $sfd) {
						$strHas = strcmp(strtolower($sfd->field_value),strtolower($get_search["Search"]));
						//echo ucwords(strtolower($get_search["Search"])); exit;
						if ($strHas == 0) {
							$standardField[] = $sfd->mls_searc_field_id;
						}
					}
				}
				
				if(isset($search_fields) && !empty($search_fields)){				
					foreach($search_fields as $sf) {
						
						foreach($standardField as $sdf) {
							if($sdf == $sf->id) {
								$standardFieldData[] = $sf->field_name;
							}
						}
					}
				}
			}

			$stData = "";
			$iCount = 0;

			//To include listingid to search
			if( (strlen($get_search["Search"]) >= 6) && is_numeric($get_search["Search"]) ){
				$stData = " And (ListingId Eq '*".ucwords(strtolower($get_search["Search"]))."*')";
			}
			
			foreach($standardFieldData as $standdata) {				
				if($iCount == 0) {
					//$stData = " And (".$standdata." Eq '".$get_search["Search"]."')";
					$stData = " And (".$standdata." Eq '*".ucwords(strtolower($get_search["Search"]))."*')";
				}
				$iCount++;
			}

			$filterStr .= $stData;		
			

			if((count($standardField) == 0) && (count($standardFieldData) == 0)) {
				
				if(isset($get_search['g_street_number']) && isset($get_search['g_route'])){
					$first_word_address = strstr($get_search['g_route'], ' ', TRUE);
					$trim_address = strstr($get_search['g_route'], ' ');
					$concat_address = $get_search['g_street_number'].' '.substr($first_word_address, 0,1).' '.$trim_address;
					$filterStr .= " And (UnparsedAddress Eq '*".str_replace(' ', '*',$concat_address)."*')";
					//set keyword to cookie
					if(!in_array($get_search["Search"], $_COOKIE["keywords_".$account_info->MlsId])){
						$count = $count + 1;
						setcookie("keywords_".$account_info->MlsId."[".$count."]", $concat_address, time() + (86400 * 360), "/");
					}
				}elseif (isset($get_search['g_street_number']) ) {
					$filterStr .= " And (StreetNumber Eq '".$get_search['g_street_number']."')";
					//set keyword to cookie
					if(!in_array($get_search["Search"], $_COOKIE["keywords_".$account_info->MlsId])){
						$count = $count + 1;
						setcookie("keywords_".$account_info->MlsId."[".$count."]", $get_search['g_street_number'], time() + (86400 * 360), "/");
					}
				}elseif (isset($get_search['g_route']) ) {
					$first_word_address = strstr($get_search['g_route'], ' ', TRUE);
					$trim_address = strstr($get_search['g_route'], ' ');
					$concat_address = substr($first_word_address, 0,1).' '.$trim_address;
					$filterStr .= " And (UnparsedAddress Eq '*".str_replace(' ', '*',$concat_address)."*')";
					//set keyword to cookie
					if(!in_array($get_search["Search"], $_COOKIE["keywords_".$account_info->MlsId])){
						$count = $count + 1;
						setcookie("keywords_".$account_info->MlsId."[".$count."]", $concat_address, time() + (86400 * 360), "/");
					}

				}elseif (isset($get_search['g_postal_code']) ) {
					
					$filterStr .= " And (PostalCode Eq '".$get_search['g_postal_code']."')";
					//set keyword to cookie
					if(!in_array($get_search["Search"], $_COOKIE["keywords_".$account_info->MlsId])){
						$count = $count + 1;
						setcookie("keywords_".$account_info->MlsId."[".$count."]", $get_search['g_postal_code'], time() + (86400 * 360), "/");
					}

				}elseif (isset($get_search['g_locality']) ) {
					
					$filterStr .= " And (City Eq '".$get_search['g_locality']."')";
					//set keyword to cookie
					if(!in_array($get_search["Search"], $_COOKIE["keywords_".$account_info->MlsId])){
						$count = $count + 1;
						setcookie("keywords_".$account_info->MlsId."[".$count."]", $get_search['g_locality'], time() + (86400 * 360), "/");
					}

				}else{
					$filteredSearch = explode(',', str_replace(' &', ',', $_GET["Search"]));
					if(count($filteredSearch) > 1){
						for ($i=0; $i < count($filteredSearch); $i++) { 
							if(str_replace(" ", '', $filteredSearch[$i]) != "USA"){
								if($i < 2) {									
									if(!in_array($account_info->MlsId, $mlsNoStandardResults)){
										
										if(isset($search_fields_list_data) && !empty($search_fields_list_data)){
											foreach($search_fields_list_data as $sfd) {
												$strHas = strcmp(strtolower($sfd->field_value),strtolower($filteredSearch[$i]));												
												if ($strHas == 0) {
													$standardField[] = $sfd->mls_searc_field_id;
												}
											}
										}
										
										if(isset($search_fields) && !empty($search_fields)){				
											foreach($search_fields as $sf) {
												foreach($standardField as $sdf) {
													if($sdf == $sf->id) {
														$standardFieldData[] = $sf->field_name;
													}
												}
											}
										}
									}

									$stData = "";
									$iCount = 0;
									
									foreach($standardFieldData as $standdata) {										
										if($iCount == 0) {
											$stData = " And (".$standdata." Eq '".$filteredSearch[$i]."')";
										}
										$iCount++;
									}
							
									if((count($standardField) > 0) && (count($standardFieldData) > 0)) {
										$filterStr .= $stData;
										break;
									}else{
										$NEWS = array(' North',' East',' West',' South', ' north',' east',' west',' south', ' NORTH',' EAST',' WEST',' SOUTH',' Rd', ' rd',' RD', ' Blvd', ' blvd',' BLVD','Pkwy','PKWY','pkwy','Avenue','Ave','avenue', 'ave', 'Dr','dr','DR','PL','Pl','pl','WAY','Way','way','LN','Ln','ln');
										$filteredNEWS = str_replace($NEWS, '', $filteredSearch[$i]);
										$filterStr .= " And (UnparsedAddress Eq '*".str_replace(' ', '*',$filteredNEWS)."*')";
										break;
									}
								}
							}		
						}
					}else{
						$NEWS = array(' North',' East',' West',' South', ' north',' east',' west',' south', ' NORTH',' EAST',' WEST',' SOUTH',' Rd', ' rd',' RD', ' Blvd', ' blvd',' BLVD','Pkwy','PKWY','pkwy','Avenue','Ave','avenue', 'Dr','dr','DR','PL','Pl','pl','WAY','Way','way','LN','Ln','ln');
						$filteredNEWS = str_replace($NEWS, '', $filteredSearch[0]);
						$filteredSearch2 = explode(' ', $filteredNEWS);
						if(count($filteredSearch2) == 3 || count($filteredSearch2) == 4 || count($filteredSearch2) == 5 || count($filteredSearch2) >= 6){
							$newAdd = $filteredSearch2[0].' '.$filteredSearch2[1].' '.$filteredSearch2[2];
							$filterStr .= " And (UnparsedAddress Eq '*".str_replace(' ', '*',$newAdd)."*')";
						}else{
							
							if(! (strlen($get_search["Search"]) >= 6) && is_numeric($get_search["Search"]) ){
								$filterStr .= " And (UnparsedAddress Eq '*".str_replace(' ', '*',$filteredNEWS)."*')";
							}
						}
					}					
				}
			}
		}
		
		if(!empty($_GET["City"]) || !empty($_GET["PostalCode"]) || !empty($_GET["CountyOrParish"])){
			foreach($_GET as $key=>$val) {
				if(!empty($val)) {
					switch($key) {
						case 'City':
							foreach ($_GET["City"] as $cities => $city) {
								if(!empty($city)) :	
									$filterStrP1 .= ((empty($filterStrP1)) ? "" : " Or ")."City Eq '".$city."'";
								endif;
							}							
						break;
						case 'PostalCode':
							//$filterStrP1 .= ((empty($filterStrP1)) ? "" : " Or ")."PostalCode Eq '".$val."'";
							foreach ($_GET["PostalCode"] as $postalcode => $pcode) {
								if(!empty($pcode)) :	
									$filterStrP1 .= ((empty($filterStrP1)) ? "" : " Or ")."PostalCode Eq '".$pcode."'";
								endif;
							}
						break;
						case 'CountyOrParish':
							//$filterStrP1 .= ((empty($filterStrP1)) ? "" : " Or ")."CountyOrParish Eq '".$val."'";
							foreach ($_GET["CountyOrParish"] as $countyparish => $county) {
								if(!empty($county)) :	
									$filterStrP1 .= ((empty($filterStrP1)) ? "" : " Or ")."CountyOrParish Eq '".$county."'";
								endif;
							}
						break;
					}
				}
			}
		}
		$filterStrP2 = '';
		if(!empty($_GET["HighSchool"]) || !empty($_GET["MiddleOrJuniorSchool"]) || !empty($_GET["ElementarySchool"])){
			foreach($_GET as $key=>$val) {
				if(!empty($val)) {
					switch($key) {
						case 'HighSchool':
							$filterStrP2 .= ((empty($filterStrP2)) ? "" : " And ")."HighSchool Eq '".$val."'";
						break;
						case 'MiddleOrJuniorSchool':
							$filterStrP2 .= ((empty($filterStrP2)) ? "" : " And ")."MiddleOrJuniorSchool Eq '".$val."'";
						break;
						case 'ElementarySchool':
							$filterStrP2 .= ((empty($filterStrP2)) ? "" : " And ")."ElementarySchool Eq '".$val."'";
						break;
					}
				}
			}
		}
		if($filterStrP1 != '') {
			$filterStr .= ' And ('.$filterStrP1.')';
		}
		if($filterStrP2 != '') {
			$filterStr .= ' And ('.$filterStrP2.')';
		}
		
		foreach($_GET as $key=>$val) {
			if($val != '') {
				switch($key) {
					case 'StreetNumber':
						$filterStr .= ((empty($filterStr)) ? "" : " And ")."StreetNumber Eq '".$val."'";
					break;
					case 'StreetName':
						$keywords = preg_split("/[\s,]+/", $val);
						if(isset($keywords[0]) && strlen($keywords[0]) == 1){
							$filterStr .= ((empty($filterStr)) ? "" : " And ")."StreetDirPrefix Eq '*".str_replace(' ', '*', $keywords[0])."*'";
						}else if(isset($keywords[0]) && strlen($keywords[0]) > 1) {
							$filterStr .= ((empty($filterStr)) ? "" : " And ")."StreetName Eq '*".str_replace(' ', '*', $keywords[0])."*'";
						}else if(isset($keywords[1]) && strlen($keywords[1]) > 1){
							$filterStr .= ((empty($filterStr)) ? "" : " And ")."StreetName Eq '*".str_replace(' ', '*', $keywords[1])."*'";
						}
					break;
					case 'Country':
						$filterStr .= ((empty($filterStr)) ? "" : " And ")."Country Eq '".$val."'";
					break;
					case 'price_min':
						$filterStr .= ((empty($filterStr)) ? "" : " And ")."ListPrice Ge ".$val."";
					break;
					case 'price_max':
						$filterStr .= ((empty($filterStr)) ? "" : " And ")."ListPrice Le ".$val."";
					break;
					case 'area_min':
						$filterStr .= ((empty($filterStr)) ? "" : " And ")."Approx Lot SqFt Ge ".$val."";
					break;
					case 'area_max':
						$filterStr .= ((empty($filterStr)) ? "" : " And ")."Approx Lot SqFt Le ".$val."";
					break;
					case 'house_min':
						$filterStr .= ((empty($filterStr)) ? "" : " And ")."BuildingAreaTotal Ge ".$val."";
					break;
					case 'house_max':
						$filterStr .= ((empty($filterStr)) ? "" : " And ")."BuildingAreaTotal Le ".$val."";
					break;
					case 'BedsTotal':
						if($val != 'Any')
							$filterStr .= ((empty($filterStr)) ? "" : " And ")."BedsTotal Ge ".$val."";
					break;
					case 'BathsTotal':
						$filterStr .= ((empty($filterStr)) ? "" : " And ")."BathsTotal Ge ".$val."";
					break;
					case 'BathsFull':
						$filterStr .= ((empty($filterStr)) ? "" : " And ")."BathsFull Ge ".$val."";
					break;
					case 'Pool-old':
						$filterStr .= ((empty($filterStr)) ? "" : " And ")."PoolFeatures Eq '".$val."'";
					break;
					case 'YearBuilt':
						$filterStr .= ((empty($filterStr)) ? "" : " And ")."YearBuilt Eq ".$val."";
					break;
					case 'GarageSpaces':
						$filterStr .= ((empty($filterStr)) ? "" : " And ")."GarageSpaces Eq ".$val."";
					break;
					case 'CarportSpaces':
						$filterStr .= ((empty($filterStr)) ? "" : " And ")."CarportSpaces Eq ".$val."";
					break;
					case 'Slab_Parking_Spaces':
						$filterStr .= ((empty($filterStr)) ? "" : " And ").'"Parking Spaces"."Slab Parking Spaces" Eq '.$val;
					break;
					case 'RangeOven_Elec':
						$filterStr .= ((empty($filterStr)) ? "" : " And ").'"Kitchen Features"."RangeOven Elec" Eq '.$val;
					break;
					case 'SubdivisionName':
						$filterStr .= ((empty($filterStr)) ? "" : " And ")."SubdivisionName Eq '".$val."'";
					break;
					case 'VideosCount':
						$filterStr .= ((empty($filterStr)) ? "" : " And ")."VideosCount Ge 1";
					break;
					case 'VirtualToursCount':
						$filterStr .= ((empty($filterStr)) ? "" : " And ")."VirtualToursCount Ge 1";
					break;
					case 'Roof':
						if(count($val)) {
							$filterStr .= ((empty($filterStr)) ? "" : " And ")."Roof Eq ";
							$roof_items = "";
							foreach($val as $v) {
								if(empty($roof_items)) {
									$roof_items .= "'".$v."'";
								}
								else {
									$roof_items .= ",'".$v."'";
								}
							}
							$filterStr .= $roof_items;
						}
					break;
					case 'hoa':
						if (count($val)) {
							$a = implode("','", $val);
							$filterStr .= ((empty($filterStr)) ? "" : " And ").'("AssocProp Info"."HOA YN" Eq \''. $a .'\' Or "Association Fees"."HOA YN2" Eq \''. $a .'\' Or "Rental Info"."HOA YN3" Eq \''.$a.'\')';
						}
						break;
				}
			}
		}

		if(isset($get_search['polygonval']) AND !empty($get_search['polygonval'])){
	      $polygon = json_decode($get_search['polygonval']);
	      //"Location Eq polygon('25.774252 -80.190262, 18.466465 -66.118292, 32.321384 -64.75737')"
	      $count=1;
	      foreach ($polygon as $value) {
	        
	        if($count == 1)
	          $polySearch = $value->lat .' '.$value->long; 
	        else
	          $polySearch .= ','.$value->lat .' '.$value->long;

	        $count++; 
	      }

	      $filterStr .= " And Location Eq polygon('".$polySearch."')";
	    }

		if(isset($get_search['InteriorLevels']) && !empty($get_search['InteriorLevels']))
		{	
			$interior = "\"General Property Description\".\"# of Interior Levels\" Eq ".$get_search['InteriorLevels'];
			$filterStr .= " AND (".$interior.")";
		}

		if(isset($get_search['Pools']) && !empty($get_search['Pools']))
		{	
			if($get_search['Pools']) {
				$pool = "\"General Property Description\".\"".$get_search['Pools']."\" Eq 'Yes'";
			}
			
			$filterStr .= " AND (".$pool.")";
		}

		if(isset($get_search['Pool']) && !empty($get_search['Pool']))
		{	
			if($get_search['Pool'] == 'Private pools with or without community pools'){
				$pool1 = "\"General Property Description\".\"Pool\" Eq 'Private Only' ";
				$pool2 = "\"General Property Description\".\"Pool\" Eq 'Community Only' ";
				$pool3 = "\"General Property Description\".\"Pool\" Ne 'Community Only' ";
				$pool = $pool1.' Or '.$pool2.' And'.$pool3;
			}else {
				$pool = "\"General Property Description\".\"Pool\" Eq '".$get_search['Pool']."' ";
			}

			// if($get_search['Pool']) {
			// 	$pool = "\"General Property Description\".\"Pool\" Eq '".$get_search['Pool']."' ";
			// }
			
			$filterStr .= " AND (".$pool.")";
		}
		
		if(isset($gsf_fields['gsf_custom_fields']) && !empty($gsf_fields['gsf_custom_fields'])){
			$filterStr .= " AND ".$gsf_fields["gsf_custom_fields"];
		}

		$order = "-OnMarketDate";
		
 		if(isset($_GET["order"]) && $_GET["order"] == 'highest') {
 			$order = "-ListPrice";
 		}
 		if(isset($_GET["order"]) && $_GET["order"] == 'lowest') {
 			$order = "+ListPrice";
 		}
 		if(isset($_GET["order"]) && $_GET["order"] == 'newest') {
 			$order = "-OnMarketDate";
 		}
 		if(isset($_GET["order"]) && $_GET["order"] == 'oldest') {
 			$order = "+YearBuilt";
 		}

 		if($account_info->MlsId == '20050607154153857879000000'){
			$order = "+YearBuilt";
		}
		
		if(isset($_GET['debugger'])){
			echo $filterStr; exit;
		}

 		$param_select = "PropertySubType,PropertyClass,Photos(1).Uri300,UnparsedFirstLineAddress,PublicRemarks,UnparsedAddress,City,CountyOrParish,StateOrProvince,PostalCode,PostalCity,MlsStatus,CurrentPrice,BedsTotal,BathsTotal,LotSizeArea,Latitude,Longitude,LotSizeUnits,LotSizeSquareFeet,LotSizeAcres,ListingKey,ListAgentId,LotSizeDimensions,ListingUpdateTimestamp,ModificationTimestamp,MajorChangeTimestamp,BuildingAreaTotal,NumberOfUnitsTotal,NumberOfUnitsVacant,MlsId,BathsFull,BathsHalf";

		$results = $dsl->getSparkProxyPagination($page, $limit, urlencode($filterStr), false, $order, $param_select);
		
		if($results->Success) {
			exit(json_encode($results));
		}
		else {
			exit(json_encode(array('Success'=>false, 'Filter'=>$filterStr)));
		}
		
	}

	public function get_photos($id = NULL) {
		$dsl = modules::load("dsl");

		if($this->input->is_ajax_request()) {

			$response["success"] = FALSE;

			if(!empty($id)) {
				$token = $this->home->getUserAccessToken($this->domain_user->agent_id);
				$type = $this->home->getUserAcessType($this->domain_user->agent_id);
				
				if($type == 0) {
					$body = array('access_token' => $token->access_token);
				} else {
					$body = array('bearer_token' => $token->access_token);
				}

				$photos = $dsl->post_endpoint('spark-listing/'.$id.'/photos', $body);
				$photodata = json_decode($photos, true);
				$photos = (isset($photodata['data'][0]['StandardFields']['Photos']))? $photodata['data'][0]['StandardFields']['Photos'] : array();
				$propertyphotos = [];
				
				foreach ($photos as $key => $photo) {
					$propertyphotos[] = $photo['Uri800'];
					
				}
				$response["propertyphotos"] = $propertyphotos;
				$response["success"] = TRUE;

			}
			exit(json_encode($response));
			
		}	
	}

	public function polygon_search($limit = 20, $page  = 1, $search = ''){
		$get = $this->input->get();
		$dsl = modules::load('dsl/dsl');
		$obj = Modules::load("mysql_properties/mysql_properties");
		$accountInfo = $obj->get_account($this->session->userdata('user_id'), "account_info");

	    if(isset($accountMeta["0"]->Mls) && !empty($accountMeta["0"]->Mls)){
	      $ix = 0;
	      foreach ($accountMeta["0"]->Mls as $Ids) {
	        if($ix === 0){
	          $mlsIds = "'".$Ids->Id."'";
	        }else{
	          $mlsIds .= ", '".$Ids->Id."'";
	        }
	        $ix++;
	      }
	      $filterStr = "MlsId Eq ".$mlsIds."  And ";
	    }
	    else{
	      $filterStr = "MlsId Eq '".$accountInfo->MlsId."'  And ";
	    }
	    $filterStr .= '(PhotosCount Gt 0) And ';
	    $filterStrStatus = Modules::run('search/mlsStatusFilterString');
	    $filterStr .= "(".$filterStrStatus.")";
	    //$filterStr .= " And (PropertyClass Ne '*Rental*')"; 
	    if(isset($get['polygonval'])){
	      $polygon = json_decode($get['polygonval']);
	      
	      //"Location Eq polygon('25.774252 -80.190262, 18.466465 -66.118292, 32.321384 -64.75737')"
	      $count=1;
	      foreach ($polygon as $value) {
	        
	        if($count == 1)
	          $polySearch = $value->lat .' '.$value->long; 
	        else
	          $polySearch .= ','.$value->lat .' '.$value->long;

	        $count++; 
	      }

	      $filterStr .= " And Location Eq polygon('".$polySearch."')";
	      
	      $param_select = "PropertySubType,PropertyClass,Photos(1).Uri300,UnparsedFirstLineAddress,PublicRemarks,UnparsedAddress,City,CountyOrParish,StateOrProvince,PostalCode,PostalCity,MlsStatus,CurrentPrice,BedsTotal,BathsTotal,LotSizeArea,Latitude,Longitude,LotSizeUnits,LotSizeSquareFeet,LotSizeAcres,ListingKey,ListAgentId,LotSizeDimensions,ListingUpdateTimestamp,ModificationTimestamp,MajorChangeTimestamp,BuildingAreaTotal,NumberOfUnitsTotal,NumberOfUnitsVacant,MlsId,BathsFull,BathsHalf";
	    	
	      $results = $dsl->getSparkProxyPagination($page, $limit, urlencode($filterStr), false, $order="-OnMarketDate", $param_select);
	      
	      if(isset($results->Success)) 
	        exit(json_encode($results));
	      else
	        exit(json_encode(array('Success'=>false, 'Filter'=>$filterStr)));
	    }

	    exit(json_encode(array('Success'=>false, 'Filter'=>$filterStr)));
	}


	public function getlatlon_ajax() {

		  $agent_address = urlencode($this->input->get('address'));
			$api_call = 'http://www.datasciencetoolkit.org/street2coordinates/'.$agent_address;
			$api_data = @file_get_contents($api_call);
			$decoded_data = json_decode($api_data, true);
			$data = $decoded_data[$this->input->get('address')];

			if(!empty($data)){

				$latlon = array(
					'lat' => $data['latitude'],
					'lon' => $data['longitude'],
				);

				exit(json_encode(array('latlon'=>$latlon)));
			}
	}

}
