<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Search extends MX_Controller {

	protected $domain_user;

	public function __construct() {

		parent::__construct();

		$this->domain_user = domain_to_user();
		$userData = $this->domain_user->id;

        // if(empty($userData)) {
        //     $this->session->set_flashdata('flash_data', 'You don\'t have access!');
        //     redirect('login/auth/login', 'refresh');
        // }
        
		$this->load->model("dsl/dsl_model","dsl_model");

	}
	public function get_mls_statuses() {
		$obj = modules::load("dsl");
		$jsOn_mlsStatus = $obj->getSparkProxy("standardfields/MlsStatus");
		$mlsStatusFieldsList = $jsOn_mlsStatus[0]->MlsStatus->FieldList;

		if($jsOn_mlsStatus[0]->MlsStatus->Searchable) {
			$statuses = array();
			foreach($mlsStatusFieldsList as $mfl) {
				$statuses[] = $mfl->Name; 
			}
			return $statuses; 
		}
		else {
			return false;
		}
	}

	public function testSearch() {
		$filterStatuses = '';
		$mlsStatusArray = $this->get_mls_statuses();
		foreach($mlsStatusArray as $msa) {
			if(!empty($msa)) {
				if(!empty($filterStatuses)) {
					$filterStatuses .= " Or MlsStatus Eq '".$msa."'";
				}
				else {
					$filterStatuses .= " MlsStatus Eq '".$msa."'";
				}
			}
		}
		
	}

	public function mlsStatusFilterString() {
		$filterStatuses = 'MlsStatus Eq ';
		$mlsStatusArray = $this->get_mls_statuses();
		foreach($mlsStatusArray as $msa) {
			if( $msa != 'Sold' && $msa != 'Closed' && $msa != 'Rented' && $msa != 'Leased' ){
				if(!empty($msa)) {
					if($filterStatuses === 'MlsStatus Eq ') {
						$filterStatuses .= "'".$msa."'";
					}
					else {
						$filterStatuses .= ",'".$msa."'";
					}
				}
			}
		}
		return $filterStatuses;
	}

	public function get_search_property($getData=NULL) {

		parse_str($getData, $getArr);

		$limit = (isset($getArr['size'])) ? $getArr['size'] : 25;
		$totalrows = (isset($getArr['totalrows'])) ? $getArr['totalrows'] : 0;

		if(isset($getArr['status'])) {
			$status = $getArr['status']; 
		}
		if(isset($getArr['Search'])) {
			//$search = $getArr['Search']; 
			$search = trim($getArr['Search']," ");
		}
		if(isset($getArr['bedroom'])) {
			$bedroom = $getArr['bedroom'];
		}
		if(isset($getArr['bathroom'])) {
			$bathroom = $getArr['bathroom']; 
		}
		if(isset($getArr['PropertyClass'])) {
			$type = $getArr['PropertyClass'];
		}
		if(isset($getArr['property_type'])) {
			$property_type = $getArr['property_type'];
		}
		if(isset($getArr['a_type'])) {
			$a_type = $getArr['a_type'];
		}
		if(isset($getArr['b_type'])) {
			$b_type = $getArr['b_type'];
		}
		if(isset($getArr['c_type'])) {
			$c_type = $getArr['c_type'];
		}
		if(isset($getArr['d_type'])) {
			$d_type = $getArr['d_type'];
		}
		if(isset($getArr['e_type'])) {
			$e_type = $getArr['e_type'];
		}
		if(isset($getArr['f_type'])) {
			$f_type = $getArr['f_type'];
		}
		if(isset($getArr['listing_change_type'])) {
			$listing_change_type = $getArr['listing_change_type'];
		}
		if(isset($getArr['recently_sold'])) {
			$recently_sold = $getArr['recently_sold'];
		}
		if(isset($getArr['min_price'])) {
			$minprice = $getArr['min_price'];
		}
		if(isset($getArr['max_price'])) {
			$maxprice = $getArr['max_price'];
		}
		if(isset($getArr['garage'])) {
			$garage = $getArr['garage'];
		}
		if(isset($getArr['features'])) {
			$features = $getArr['features'];
		}
		if(isset($getArr['house_age'])) {
			$house_age = $getArr['house_age'];
		}
		if(isset($getArr['lot_size'])) {
			$lot_size = $getArr['lot_size'];
		}
		if(isset($getArr['house_size'])) {
			$house_size = $getArr['house_size'];
		}
		if(isset($getArr['page'])) {
			$page = $getArr['page'];
		}
		else {
			$page = 1;
		}
		$filterStr = '';
		if(isset($getArr['offset']) && !isset($getArr['page'])) {
			
			$total_page = $totalrows / $limit;
			$remainder = $totalrows % $limit;

			if($remainder != 0){
				$total_page++;
			}

			$total_page = intval($total_page);
			
			$actual_page = $getArr['offset'] / $limit;
			$actual_page = intval($actual_page);
			$actual_page = ($actual_page <= 0) ? 1 : $actual_page;

			if($actual_page < $total_page) {
				$page += $actual_page;
			}
			/*$page = ($getArr['offset'] / ($limit));// + 1;
			$page = ($getArr['offset'] / ($limit - 1)) + 1;
			$page = intval ($page);*/
		}

		if(!empty($recently_sold)) {
			$filterStr = "(MlsStatus Eq 'Sold' Or MlsStatus Eq 'Closed')";
		} else {
			// $filterStr = "(MlsStatus Eq '**')";
			$filterStrStatuses = $this->mlsStatusFilterString();
			$filterStr = "(".$filterStrStatuses.") ";
		} 

		if(!empty($search)) {
			if(preg_match("/^[0-9]{26}$/", $search)) {
				$mlsid = $search;
				if(!empty($filterStr)) {
					$filterStr .= "And (MlsId Eq '".urldecode($mlsid) ."')";
				}
				else {
					$filterStr .= " (MlsId Eq '".urldecode($mlsid) ."')";
				}
			}
			else {
				$sSearch = preg_replace('/[^A-Za-z0-9\-\(\) ]/', '', $search);
				$sSearch = str_replace(' ', '*', $sSearch);
				if(!empty($filterStr)) {
					#$filterStr .= " And (UnparsedAddress Eq '*".urldecode($sSearch)."*' Or PublicRemarks Eq '*".urldecode($search)."*' Or ListingId Eq '".urldecode($search)."')";
					$filterStr .= " And (UnparsedAddress Eq '*".urldecode($sSearch)."*'";
					if(is_numeric($filterStr)) {
						$filterStr .= " Or ListingId Eq '".urldecode($search)."')";
					} 
					else {
						$filterStr .= ")";
					}
				}
				else {
					#$filterStr .= "(UnparsedAddress Eq '*".urldecode($sSearch)."*' Or PublicRemarks Eq '*".urldecode($search)."*' Or ListingId Eq '".urldecode($search)."')";
					#$filterStr .= "(UnparsedAddress Eq '*".urldecode($sSearch)."*' Or ListingId Eq '".urldecode($search)."')";
					$filterStr .= "(UnparsedAddress Eq '*".urldecode($sSearch)."*'";
					if(is_numeric($filterStr)) {
						$filterStr .= " Or ListingId Eq '".urldecode($search)."')";
					} 
					else {
						$filterStr .= ")";
					}
				}
			}
		}

		if(!empty($bedroom)) {
			$filterStr .= " And (BedsTotal Ge ".$bedroom.")";  
		}
		if(!empty($bathroom)) {
			$filterStr .= " And (BathsTotal Ge ".$bathroom.")";
		}
		if(!empty($type)) {
			$filterStr .= " And (PropertyType Eq '".$type."')";
		}
		if(!empty($minprice) && !empty($maxprice)) {
			$filterStr .= " And (CurrentPrice Bt ".$minprice.",".$maxprice.")";
		}
		if(!empty($property_type)) {
						if(!empty($filterStr)) {
				$str = " And (";
			}
			else {
				$str = "(";
			}
			$count = 0;
			foreach ($property_type as $key) {
				if($count == 0) {
					$str .= "PropertyClass Eq '*".$key."*'";
				} else {
					$str .= " Or PropertyClass Eq '*".$key."*'";
				}
				$count++;
			}
			$filterStr .= $str.")";
		}else{
			$filterStr .= " And (PropertyClass Ne 'Rental')";
		}
		
		//recently sold property
		if(!empty($recently_sold)) {
			$current_year = date("Y-m-d");

			$time = strtotime("-1 month", time());
			$recent_sold = date("Y-m-d", $time);

						if(!empty($filterStr)) {
				$str = " And (";
			}
			else {
				$str = "(";
			}
			//$filterStr = "(MlsStatus Eq 'Sold' Or MlsStatus Eq 'Closed')";
			$count = 0;
			foreach ($recently_sold as $key) {
				if($count == 0) {
					 $str .= "PropertyClass Eq '".$key ."' And CloseDate Bt ".$recent_sold.",".$current_year."";
				} else {
					$str .= " Or PropertyClass Eq '".$key ."' And CloseDate Bt ".$recent_sold.",".$current_year."";
				}
				$count++;
			}
			$filterStr .= $str.")";
		}
		
		if(!empty($house_size)) {
			$filterStr .= " And (BuildingAreaTotal Ge ".$house_size.")";
		}
		if(!empty($lot_size)) {
			$filterStr .= " And (LotSizeArea Ge ".$lot_size." Or BuildingAreaTotal Ge ".$lot_size." Or LotSizeSquareFeet Ge ".$lot_size.")";
		}
		if(!empty($house_age)) {
			$current_year = date("Y");
			if($house_age == 'fiveyears') {
				$year_built = $current_year - 5;
				$filterStr .= " And (YearBuilt Ge ".$year_built.")";
			} elseif($house_age == 'tenyears') {
				$year_built = $current_year - 10;
				$filterStr .= " And (YearBuilt Ge ".$year_built.")";
			} elseif($house_age == 'fifthteenyears') {
				$year_built = $current_year - 15;
				$filterStr .= " And (YearBuilt Ge ".$year_built.")";
			} elseif($house_age == 'twentyyears') {
				$year_built = $current_year - 20;
				$filterStr .= " And (YearBuilt Ge ".$year_built.")";
			} elseif($house_age == 'fiftyyears') {
				$year_built = $current_year - 50;
				$filterStr .= " And (YearBuilt Ge ".$year_built.")";
			} elseif($house_age == 'fiftyyearsplus') {
				$year_built = $current_year - 51;
				$filterStr .= " And (YearBuilt Le ".$year_built.")";
			}
		}
		/*** PROPERTY SUB TYPES ***/
		if(!empty($a_type)) {
			if(!empty($filterStr)) {
							if(!empty($filterStr)) {
				$str = " And (";
			}
			else {
				$str = "(";
			}
			}
			else {
				$str = "(";
			}
			$count = 0;
			foreach ($a_type as $key) {
				if($count == 0) {
					if($key == "Single Family")
						$str .= "PropertySubType Eq '*".$key."*' Or PropertySubType Eq '*Detached*'";
					else
						$str .= "PropertySubType Eq '*".$key."*' Or PublicRemarks Eq '*".$key."*'";
				} else {
					if($key == "Single Family")
						$str .= " Or PropertySubType Eq '*".$key."*' Or PropertySubType Eq '*Detached*'";
					else
						$str .= " Or PropertySubType Eq '*".$key."*' Or PublicRemarks Eq '*".$key."*'";
				}
				$count++;
			}
			$filterStr .= $str.")";
		}
		if(!empty($b_type)) {
			if(!empty($filterStr)) {
				$str = " And (";
			}
			else {
				$str = "(";
			}
			$count = 0;
			foreach ($b_type as $key) {
				if($count == 0) {
					$str .= "PropertySubType Eq '*".$key."*' Or PublicRemarks Eq '*".$key."*'";
				} else {
					$str .= " Or PropertySubType Eq '*".$key."*' Or PublicRemarks Eq '*".$key."*'";
				}
				$count++;
			}
			$filterStr .= $str.")";
		}
		if(!empty($c_type)) {
			if(!empty($filterStr)) {
				$str = " And (";
			}
			else {
				$str = "(";
			}
			$count = 0;
			foreach ($c_type as $key) {
				if($count == 0) {
					$str .= "PropertySubType Eq '*".$key."*' Or PublicRemarks Eq '*".$key."*'";
				} else {
					$str .= " Or PropertySubType Eq '*".$key."*' Or PublicRemarks Eq '*".$key."*'";
				}
				$count++;
			}
			$filterStr .= $str.")";
		}
		if(!empty($d_type)) {
			if(!empty($filterStr)) {
				$str = " And (";
			}
			else {
				$str = "(";
			}
			$count = 0;
			foreach ($d_type as $key) {
				if($count == 0) {
					$str .= "PropertySubType Eq '*".$key."*' Or PublicRemarks Eq '*".$key."*'";
				} else {
					$str .= " Or PropertySubType Eq '*".$key."*' Or PublicRemarks Eq '*".$key."*'";
				}
				$count++;
			}
			$filterStr .= $str.")";
		}
		if(!empty($e_type)) {
			if(!empty($filterStr)) {
				$str = " And (";
			}
			else {
				$str = "(";
			}
			$count = 0;
			foreach ($e_type as $key) {
				if($count == 0) {
					$str .= "PropertySubType Eq '*".$key."*' Or PublicRemarks Eq '*".$key."*'";
				} else {
					$str .= " Or PropertySubType Eq '*".$key."*' Or PublicRemarks Eq '*".$key."*'";
				}
				$count++;
			}
			$filterStr .= $str.")";
		}
		if(!empty($f_type)) {
			if(!empty($filterStr)) {
				$str = " And (";
			}
			else {
				$str = "(";
			}
			$count = 0;
			foreach ($f_type as $key) {
				if($count == 0) {
					$str .= "PropertySubType Eq '*".$key."*' Or PublicRemarks Eq '*".$key."*'";
				} else {
					$str .= " Or PropertySubType Eq '*".$key."*' Or PublicRemarks Eq '*".$key."*'";
				}
				$count++;
			}
			$filterStr .= $str.")";
		}
		if(!empty($listing_change_type)) {
			if(!empty($filterStr)) {
				$str = " And (";
			}
			else {
				$str = "(";
			}
			$count = 0;
			foreach ($listing_change_type as $key) {
				if($count == 0) {
					$str .= "MajorChangeType Eq '".$key ."'";
				} else {
					$str .= " Or MajorChangeType Eq '".$key ."'";
				}
				$count++;
			}
			$filterStr .= $str.")";
		}
		if(!empty($features)) {
			if(!empty($filterStr)) {
				$str = " And (";
			}
			else {
				$str = "(";
			}
			$count = 0;
			foreach($features as $key) {
				if($count == 0) {
					$str .= "PublicRemarks Eq '*".$key ."*'";
				} else {
					$str .= " Or PublicRemarks Eq '*".$key ."*'";
				}
				$count++;
			}
			$filterStr .= $str.")";
		}
		$filterStr = preg_replace('/\s+/', ' ',$filterStr);
		$obj = modules::load("dsl");
		$filter_url = rawurlencode($filterStr);
		$filter_string = substr($filter_url, 3, -3);

		$config = $obj->get_config();

		$token = $this->dsl_model->getUserAccessTokenOld($this->domain_user->agent_id);

		if($token) {
			$access_token = array(
				'access_token' => $token->access_token,
				'bearer_token' => ""
			);
		} else {
			$bearer = $this->dsl_model->getUserBearerToken($this->domain_user->agent_id);

			$access_token = array(
				'access_token' =>"",
				'bearer_token' => $bearer->bearer_token
			);
		}
		
		$endpoint = "spark-listings?_limit=".$limit."&_pagination=1&_page=".$page."&_filter=(".$filter_string.")&_expand=PrimaryPhoto&_orderby=StarRating";
		$endpoint2 = "spark-listings?_limit=".$limit."&_pagination=1&_page=".$page."&_filter=(".$filterStr.")&_expand=PrimaryPhoto&_orderby=StarRating";
		
		//$results = $obj->post_endpoint($endpoint, $access_token);
		//$return = json_decode($results, true);
		$return = $this->direct_search($filter_string,$access_token, $page, $limit);

		if($return['data']) {
			/* Has Data */
		}
		else {
			/* 
				====Empty====
				* Save Information to database
				
			*/
			$data = array(
				'search_filter' => $filter_string,
				'response_message' => json_encode($return),
				'response_type'	=> 'Direct',
				'status'	=> isset($return['status']) ? $return['status'] : 404,
				'ip_address' => $this->input->ip_address(),
				'agent_id' => $this->domain_user->agent_id
			);
			$this->db->insert('search_logs', $data);
		}
		// echo "<pre>";
		// var_dump($return);
		// echo "</pre>";
		// echo "<pre>";
		// var_dump($filter_string);
		// echo "</pre>";
		// exit;
		
		return $return;
	}
	
	public function search_ping_test($getData=NULL) {
		
		if (!empty($_GET['Search'])) {

			$getArr = $_GET;
			// var_dump(isset($getArr['Search'])); exit;
			//parse_str($getData, $getArr);

			if(isset($getArr['size'])){
				$limit = $getArr['size'];
			} else {
				$limit = 25;
			}

			if(isset($getArr['status'])) {
				$status = $getArr['status']; 
			}
			if(isset($getArr['Search'])) {
				//$search = $getArr['Search']; 
				$search = trim($getArr['Search']," ");
			}
			if(isset($getArr['bedroom'])) {
				$bedroom = $getArr['bedroom'];
			}
			if(isset($getArr['bathroom'])) {
				$bathroom = $getArr['bathroom']; 
			}
			if(isset($getArr['PropertyClass'])) {
				$type = $getArr['PropertyClass'];
			}
			if(isset($getArr['property_type'])) {
				$property_type = $getArr['property_type'];
			}
			if(isset($getArr['a_type'])) {
				$a_type = $getArr['a_type'];
			}
			if(isset($getArr['b_type'])) {
				$b_type = $getArr['b_type'];
			}
			if(isset($getArr['c_type'])) {
				$c_type = $getArr['c_type'];
			}
			if(isset($getArr['d_type'])) {
				$d_type = $getArr['d_type'];
			}
			if(isset($getArr['e_type'])) {
				$e_type = $getArr['e_type'];
			}
			if(isset($getArr['f_type'])) {
				$f_type = $getArr['f_type'];
			}
			if(isset($getArr['listing_change_type'])) {
				$listing_change_type = $getArr['listing_change_type'];
			}
			if(isset($getArr['recently_sold'])) {
				$recently_sold = $getArr['recently_sold'];
			}
			if(isset($getArr['min_price'])) {
				$minprice = $getArr['min_price'];
			}
			if(isset($getArr['max_price'])) {
				$maxprice = $getArr['max_price'];
			}
			if(isset($getArr['garage'])) {
				$garage = $getArr['garage'];
			}
			if(isset($getArr['features'])) {
				$features = $getArr['features'];
			}
			if(isset($getArr['house_age'])) {
				$house_age = $getArr['house_age'];
			}
			if(isset($getArr['lot_size'])) {
				$lot_size = $getArr['lot_size'];
			}
			if(isset($getArr['house_size'])) {
				$house_size = $getArr['house_size'];
			}
			if(isset($getArr['page'])) {
				$page = $getArr['page'];
			}
			else {
				$page = 1;
			}
			$filterStr = '';
			if( isset($getArr['offset']) && !isset($getArr['page']) ) {
				$page = ($getArr['offset'] / ($limit - 1)) + 1;
				$page = intval ($page);
			}

			if(!empty($recently_sold)) {
				$filterStr = "(MlsStatus Eq 'Sold' Or MlsStatus Eq 'Closed')";
			} else {
				// $filterStr = "(MlsStatus Eq '**')";
				$filterStrStatuses = $this->mlsStatusFilterString();
				$filterStr = "(".$filterStrStatuses.") ";
			} 

			if(!empty($search)) {
				if(preg_match("/^[0-9]{26}$/", $search)) {
					$mlsid = $search;
					if(!empty($filterStr)) {
						$filterStr .= "And (MlsId Eq '".urldecode($mlsid) ."')";
					}
					else {
						$filterStr .= " (MlsId Eq '".urldecode($mlsid) ."')";
					}
				} 
				else {
					$sSearch = preg_replace('/[^A-Za-z0-9\-\(\) ]/', '', $search);
					$sSearch = str_replace(' ', '*', $sSearch);
					if(!empty($filterStr)) {
						#$filterStr .= " And (UnparsedAddress Eq '*".urldecode($sSearch)."*' Or PublicRemarks Eq '*".urldecode($search)."*' Or ListingId Eq '".urldecode($search)."')";
						$filterStr .= " And (UnparsedAddress Eq '*".urldecode($sSearch)."*'";
						if(is_numeric($filterStr)) {
							$filterStr .= " Or ListingId Eq '".urldecode($search)."')";
						} 
						else {
							$filterStr .= ")";
						}
					}
					else {
						#$filterStr .= "(UnparsedAddress Eq '*".urldecode($sSearch)."*' Or PublicRemarks Eq '*".urldecode($search)."*' Or ListingId Eq '".urldecode($search)."')";
						#$filterStr .= "(UnparsedAddress Eq '*".urldecode($sSearch)."*' Or ListingId Eq '".urldecode($search)."')";
						$filterStr .= "(UnparsedAddress Eq '*".urldecode($sSearch)."*'";
						if(is_numeric($filterStr)) {
							$filterStr .= " Or ListingId Eq '".urldecode($search)."')";
						} 
						else {
							$filterStr .= ")";
						}
					}
				}
			}

			if(!empty($bedroom)) {
				$filterStr .= " And (BedsTotal Ge ".$bedroom.")";  
			}
			if(!empty($bathroom)) {
				$filterStr .= " And (BathsTotal Ge ".$bathroom.")";
			}
			if(!empty($type)) {
				$filterStr .= " And (PropertyType Eq '".$type."')";
			}
			if(!empty($minprice) && !empty($maxprice)) {
				$filterStr .= " And (CurrentPrice Bt ".$minprice.",".$maxprice.")";
			}
			if(!empty($property_type)) {
							if(!empty($filterStr)) {
					$str = " And (";
				}
				else {
					$str = "(";
				}
				$count = 0;
				foreach ($property_type as $key) {
					if($count == 0) {
						$str .= "PropertyClass Eq '*".$key."*'";
					} else {
						$str .= " Or PropertyClass Eq '*".$key."*'";
					}
					$count++;
				}
				$filterStr .= $str.")";
			}
			
			//recently sold property
			if(!empty($recently_sold)) {
				$current_year = date("Y-m-d");

				$time = strtotime("-1 month", time());
				$recent_sold = date("Y-m-d", $time);

							if(!empty($filterStr)) {
					$str = " And (";
				}
				else {
					$str = "(";
				}
				//$filterStr = "(MlsStatus Eq 'Sold' Or MlsStatus Eq 'Closed')";
				$count = 0;
				foreach ($recently_sold as $key) {
					if($count == 0) {
						 $str .= "PropertyClass Eq '".$key ."' And CloseDate Bt ".$recent_sold.",".$current_year."";
					} else {
						$str .= " Or PropertyClass Eq '".$key ."' And CloseDate Bt ".$recent_sold.",".$current_year."";
					}
					$count++;
				}
				$filterStr .= $str.")";
			}
			
			if(!empty($house_size)) {
				$filterStr .= " And (BuildingAreaTotal Ge ".$house_size.")";
			}
			if(!empty($lot_size)) {
				$filterStr .= " And (LotSizeArea Ge ".$lot_size." Or BuildingAreaTotal Ge ".$lot_size." Or LotSizeSquareFeet Ge ".$lot_size.")";
			}
			if(!empty($house_age)) {
				$current_year = date("Y");
				if($house_age == 'fiveyears') {
					$year_built = $current_year - 5;
					$filterStr .= " And (YearBuilt Ge ".$year_built.")";
				} elseif($house_age == 'tenyears') {
					$year_built = $current_year - 10;
					$filterStr .= " And (YearBuilt Ge ".$year_built.")";
				} elseif($house_age == 'fifthteenyears') {
					$year_built = $current_year - 15;
					$filterStr .= " And (YearBuilt Ge ".$year_built.")";
				} elseif($house_age == 'twentyyears') {
					$year_built = $current_year - 20;
					$filterStr .= " And (YearBuilt Ge ".$year_built.")";
				} elseif($house_age == 'fiftyyears') {
					$year_built = $current_year - 50;
					$filterStr .= " And (YearBuilt Ge ".$year_built.")";
				} elseif($house_age == 'fiftyyearsplus') {
					$year_built = $current_year - 51;
					$filterStr .= " And (YearBuilt Le ".$year_built.")";
				}
			}
			/*** PROPERTY SUB TYPES ***/
			if(!empty($a_type)) {
				if(!empty($filterStr)) {
								if(!empty($filterStr)) {
					$str = " And (";
				}
				else {
					$str = "(";
				}
				}
				else {
					$str = "(";
				}
				$count = 0;
				foreach ($a_type as $key) {
					if($count == 0) {
						if($key == "Single Family")
							$str .= "PropertySubType Eq '*".$key."*' Or PropertySubType Eq '*Detached*'";
						else
							$str .= "PropertySubType Eq '*".$key."*' Or PublicRemarks Eq '*".$key."*'";
					} else {
						if($key == "Single Family")
							$str .= " Or PropertySubType Eq '*".$key."*' Or PropertySubType Eq '*Detached*'";
						else
							$str .= " Or PropertySubType Eq '*".$key."*' Or PublicRemarks Eq '*".$key."*'";
					}
					$count++;
				}
				$filterStr .= $str.")";
			}
			if(!empty($b_type)) {
				if(!empty($filterStr)) {
					$str = " And (";
				}
				else {
					$str = "(";
				}
				$count = 0;
				foreach ($b_type as $key) {
					if($count == 0) {
						$str .= "PropertySubType Eq '*".$key."*' Or PublicRemarks Eq '*".$key."*'";
					} else {
						$str .= " Or PropertySubType Eq '*".$key."*' Or PublicRemarks Eq '*".$key."*'";
					}
					$count++;
				}
				$filterStr .= $str.")";
			}
			if(!empty($c_type)) {
				if(!empty($filterStr)) {
					$str = " And (";
				}
				else {
					$str = "(";
				}
				$count = 0;
				foreach ($c_type as $key) {
					if($count == 0) {
						$str .= "PropertySubType Eq '*".$key."*' Or PublicRemarks Eq '*".$key."*'";
					} else {
						$str .= " Or PropertySubType Eq '*".$key."*' Or PublicRemarks Eq '*".$key."*'";
					}
					$count++;
				}
				$filterStr .= $str.")";
			}
			if(!empty($d_type)) {
				if(!empty($filterStr)) {
					$str = " And (";
				}
				else {
					$str = "(";
				}
				$count = 0;
				foreach ($d_type as $key) {
					if($count == 0) {
						$str .= "PropertySubType Eq '*".$key."*' Or PublicRemarks Eq '*".$key."*'";
					} else {
						$str .= " Or PropertySubType Eq '*".$key."*' Or PublicRemarks Eq '*".$key."*'";
					}
					$count++;
				}
				$filterStr .= $str.")";
			}
			if(!empty($e_type)) {
				if(!empty($filterStr)) {
					$str = " And (";
				}
				else {
					$str = "(";
				}
				$count = 0;
				foreach ($e_type as $key) {
					if($count == 0) {
						$str .= "PropertySubType Eq '*".$key."*' Or PublicRemarks Eq '*".$key."*'";
					} else {
						$str .= " Or PropertySubType Eq '*".$key."*' Or PublicRemarks Eq '*".$key."*'";
					}
					$count++;
				}
				$filterStr .= $str.")";
			}
			if(!empty($f_type)) {
				if(!empty($filterStr)) {
					$str = " And (";
				}
				else {
					$str = "(";
				}
				$count = 0;
				foreach ($f_type as $key) {
					if($count == 0) {
						$str .= "PropertySubType Eq '*".$key."*' Or PublicRemarks Eq '*".$key."*'";
					} else {
						$str .= " Or PropertySubType Eq '*".$key."*' Or PublicRemarks Eq '*".$key."*'";
					}
					$count++;
				}
				$filterStr .= $str.")";
			}
			if(!empty($listing_change_type)) {
				if(!empty($filterStr)) {
					$str = " And (";
				}
				else {
					$str = "(";
				}
				$count = 0;
				foreach ($listing_change_type as $key) {
					if($count == 0) {
						$str .= "MajorChangeType Eq '".$key ."'";
					} else {
						$str .= " Or MajorChangeType Eq '".$key ."'";
					}
					$count++;
				}
				$filterStr .= $str.")";
			}
			if(!empty($features)) {
				if(!empty($filterStr)) {
					$str = " And (";
				}
				else {
					$str = "(";
				}
				$count = 0;
				foreach($features as $key) {
					if($count == 0) {
						$str .= "PublicRemarks Eq '*".$key ."*'";
					} else {
						$str .= " Or PublicRemarks Eq '*".$key ."*'";
					}
					$count++;
				}
				$filterStr .= $str.")";
			}
			$filterStr = preg_replace('/\s+/', ' ',$filterStr);
			$obj = modules::load("dsl");
			$filter_url = rawurlencode($filterStr);
			$filter_string = substr($filter_url, 3, -3);

			$config = $obj->get_config();

			$token = $this->dsl_model->getUserAccessTokenOld($this->domain_user->agent_id);

			if($token) {
				$access_token = array(
					'access_token' => $token->access_token,
					'bearer_token' => ""
				);
			} else {
				$bearer = $this->dsl_model->getUserBearerToken($this->domain_user->agent_id);

				$access_token = array(
					'access_token' =>"",
					'bearer_token' => $bearer->bearer_token
				);
			}
			
			$endpoint = "spark-listings?_limit=".$limit."&_pagination=1&_page=".$page."&_filter=(".$filter_string.")&_expand=PrimaryPhoto&_orderby=StarRating";
			$endpoint2 = "spark-listings?_limit=".$limit."&_pagination=1&_page=".$page."&_filter=(".$filterStr.")&_expand=PrimaryPhoto&_orderby=StarRating";
			
			//$results = $obj->post_endpoint($endpoint, $access_token);
			//$return = json_decode($results, true);
			$return = $this->direct_search($filter_string,$access_token, $page, $limit);

			if($return['data']) {
				/* Has Data */
			}
			else {
				/* 
					====Empty====
					* Save Information to database
					
				*/
				$data = array(
					'search_filter' => $filter_string,
					'response_message' => json_encode($return),
					'response_type'	=> 'Direct',
					'status'	=> isset($return['status']) ? $return['status'] : 404,
					'ip_address' => $this->input->ip_address(),
					'agent_id' => $this->domain_user->agent_id
				);
				$this->db->insert('search_logs', $data);
			}

		}
	
		if(isset($return) && !empty($return['data'])) {
			#header("HTTP/1.0 404 Not Found");
			echo "results found ".count($return['data']);
		}
		else {
			/* No Data Found */
			header("HTTP/1.0 404 Not Found");
		}
	}
	public function search_property_tester($getData=NULL) {
		$filterStr = '';
		$return = '';
		if (!empty($_GET['Search'])) {

			$getArr = $_GET;
			// var_dump(isset($getArr['Search'])); exit;
			//parse_str($getData, $getArr);

			if(isset($getArr['size'])){
				$limit = $getArr['size'];
			} else {
				$limit = 25;
			}

			if(isset($getArr['status'])) {
				$status = $getArr['status']; 
			}
			if(isset($getArr['Search'])) {
				//$search = $getArr['Search']; 
				$search = trim($getArr['Search']," ");
			}
			if(isset($getArr['bedroom'])) {
				$bedroom = $getArr['bedroom'];
			}
			if(isset($getArr['bathroom'])) {
				$bathroom = $getArr['bathroom']; 
			}
			if(isset($getArr['PropertyClass'])) {
				$type = $getArr['PropertyClass'];
			}
			if(isset($getArr['property_type'])) {
				$property_type = $getArr['property_type'];
			}
			if(isset($getArr['a_type'])) {
				$a_type = $getArr['a_type'];
			}
			if(isset($getArr['b_type'])) {
				$b_type = $getArr['b_type'];
			}
			if(isset($getArr['c_type'])) {
				$c_type = $getArr['c_type'];
			}
			if(isset($getArr['d_type'])) {
				$d_type = $getArr['d_type'];
			}
			if(isset($getArr['e_type'])) {
				$e_type = $getArr['e_type'];
			}
			if(isset($getArr['f_type'])) {
				$f_type = $getArr['f_type'];
			}
			if(isset($getArr['listing_change_type'])) {
				$listing_change_type = $getArr['listing_change_type'];
			}
			if(isset($getArr['recently_sold'])) {
				$recently_sold = $getArr['recently_sold'];
			}
			if(isset($getArr['min_price'])) {
				$minprice = $getArr['min_price'];
			}
			if(isset($getArr['max_price'])) {
				$maxprice = $getArr['max_price'];
			}
			if(isset($getArr['garage'])) {
				$garage = $getArr['garage'];
			}
			if(isset($getArr['features'])) {
				$features = $getArr['features'];
			}
			if(isset($getArr['house_age'])) {
				$house_age = $getArr['house_age'];
			}
			if(isset($getArr['lot_size'])) {
				$lot_size = $getArr['lot_size'];
			}
			if(isset($getArr['house_size'])) {
				$house_size = $getArr['house_size'];
			}
			if(isset($getArr['page'])) {
				$page = $getArr['page'];
			}
			else {
				$page = 1;
			}
			
			if( isset($getArr['offset']) && !isset($getArr['page']) ) {
				$page = ($getArr['offset'] / ($limit - 1)) + 1;
				$page = intval ($page);
			}

			if(!empty($recently_sold)) {
				$filterStr = "(MlsStatus Eq 'Sold' Or MlsStatus Eq 'Closed')";
			} else {
				// $filterStr = "(MlsStatus Eq '**')";
				$filterStrStatuses = $this->mlsStatusFilterString();
				$filterStr = "(".$filterStrStatuses.") ";
			} 

			if(!empty($search)) {
				if(preg_match("/^[0-9]{26}$/", $search)) {
					$mlsid = $search;
					if(!empty($filterStr)) {
						$filterStr .= "And (MlsId Eq '".urldecode($mlsid) ."')";
					}
					else {
						$filterStr .= " (MlsId Eq '".urldecode($mlsid) ."')";
					}
				} 
				else {
					$sSearch = preg_replace('/[^A-Za-z0-9\-\(\) ]/', '', $search);
					$sSearch = str_replace(' ', '*', $sSearch);
					if(!empty($filterStr)) {
						#$filterStr .= " And (UnparsedAddress Eq '*".urldecode($sSearch)."*' Or PublicRemarks Eq '*".urldecode($search)."*' Or ListingId Eq '".urldecode($search)."')";
						$filterStr .= " And (UnparsedAddress Eq '*".urldecode($sSearch)."*'";
						if(is_numeric($filterStr)) {
							$filterStr .= " Or ListingId Eq '".urldecode($search)."')";
						} 
						else {
							$filterStr .= ")";
						}
					}
					else {
						#$filterStr .= "(UnparsedAddress Eq '*".urldecode($sSearch)."*' Or PublicRemarks Eq '*".urldecode($search)."*' Or ListingId Eq '".urldecode($search)."')";
						#$filterStr .= "(UnparsedAddress Eq '*".urldecode($sSearch)."*' Or ListingId Eq '".urldecode($search)."')";
						$filterStr .= "(UnparsedAddress Eq '*".urldecode($sSearch)."*'";
						if(is_numeric($filterStr)) {
							$filterStr .= " Or ListingId Eq '".urldecode($search)."')";
						} 
						else {
							$filterStr .= ")";
						}
					}
				}
			}

			if(!empty($bedroom)) {
				$filterStr .= " And (BedsTotal Ge ".$bedroom.")";  
			}
			if(!empty($bathroom)) {
				$filterStr .= " And (BathsTotal Ge ".$bathroom.")";
			}
			if(!empty($type)) {
				$filterStr .= " And (PropertyType Eq '".$type."')";
			}
			if(!empty($minprice) && !empty($maxprice)) {
				$filterStr .= " And (CurrentPrice Bt ".$minprice.",".$maxprice.")";
			}
			if(!empty($property_type)) {
				if(!empty($filterStr)) {
					$str = " And (";
				}
				else 
				{
					$str = "(";
				}
				$count = 0;
				foreach ($property_type as $key) {
					if($count == 0) {
						$str .= "PropertyClass Eq '*".$key."*'";
					} 
					else {
						$str .= " Or PropertyClass Eq '*".$key."*'";
					}
					$count++;
				}
				$filterStr .= $str.")";
			}
			
			//recently sold property
			if(!empty($recently_sold)) {
				$current_year = date("Y-m-d");

				$time = strtotime("-1 month", time());
				$recent_sold = date("Y-m-d", $time);

							if(!empty($filterStr)) {
					$str = " And (";
				}
				else {
					$str = "(";
				}
				//$filterStr = "(MlsStatus Eq 'Sold' Or MlsStatus Eq 'Closed')";
				$count = 0;
				foreach ($recently_sold as $key) {
					if($count == 0) {
						 $str .= "PropertyClass Eq '".$key ."' And CloseDate Bt ".$recent_sold.",".$current_year."";
					} else {
						$str .= " Or PropertyClass Eq '".$key ."' And CloseDate Bt ".$recent_sold.",".$current_year."";
					}
					$count++;
				}
				$filterStr .= $str.")";
			}
			
			if(!empty($house_size)) {
				$filterStr .= " And (BuildingAreaTotal Ge ".$house_size.")";
			}
			if(!empty($lot_size)) {
				$filterStr .= " And (LotSizeArea Ge ".$lot_size." Or BuildingAreaTotal Ge ".$lot_size." Or LotSizeSquareFeet Ge ".$lot_size.")";
			}
			if(!empty($house_age)) {
				$current_year = date("Y");
				if($house_age == 'fiveyears') {
					$year_built = $current_year - 5;
					$filterStr .= " And (YearBuilt Ge ".$year_built.")";
				} elseif($house_age == 'tenyears') {
					$year_built = $current_year - 10;
					$filterStr .= " And (YearBuilt Ge ".$year_built.")";
				} elseif($house_age == 'fifthteenyears') {
					$year_built = $current_year - 15;
					$filterStr .= " And (YearBuilt Ge ".$year_built.")";
				} elseif($house_age == 'twentyyears') {
					$year_built = $current_year - 20;
					$filterStr .= " And (YearBuilt Ge ".$year_built.")";
				} elseif($house_age == 'fiftyyears') {
					$year_built = $current_year - 50;
					$filterStr .= " And (YearBuilt Ge ".$year_built.")";
				} elseif($house_age == 'fiftyyearsplus') {
					$year_built = $current_year - 51;
					$filterStr .= " And (YearBuilt Le ".$year_built.")";
				}
			}
			/*** PROPERTY SUB TYPES ***/
			if(!empty($a_type)) {
				if(!empty($filterStr)) {
								if(!empty($filterStr)) {
					$str = " And (";
				}
				else {
					$str = "(";
				}
				}
				else {
					$str = "(";
				}
				$count = 0;
				foreach ($a_type as $key) {
					if($count == 0) {
						if($key == "Single Family")
							$str .= "PropertySubType Eq '*".$key."*' Or PropertySubType Eq '*Detached*'";
						else
							$str .= "PropertySubType Eq '*".$key."*' Or PublicRemarks Eq '*".$key."*'";
					} else {
						if($key == "Single Family")
							$str .= " Or PropertySubType Eq '*".$key."*' Or PropertySubType Eq '*Detached*'";
						else
							$str .= " Or PropertySubType Eq '*".$key."*' Or PublicRemarks Eq '*".$key."*'";
					}
					$count++;
				}
				$filterStr .= $str.")";
			}
			if(!empty($b_type)) {
				if(!empty($filterStr)) {
					$str = " And (";
				}
				else {
					$str = "(";
				}
				$count = 0;
				foreach ($b_type as $key) {
					if($count == 0) {
						$str .= "PropertySubType Eq '*".$key."*' Or PublicRemarks Eq '*".$key."*'";
					} else {
						$str .= " Or PropertySubType Eq '*".$key."*' Or PublicRemarks Eq '*".$key."*'";
					}
					$count++;
				}
				$filterStr .= $str.")";
			}
			if(!empty($c_type)) {
				if(!empty($filterStr)) {
					$str = " And (";
				}
				else {
					$str = "(";
				}
				$count = 0;
				foreach ($c_type as $key) {
					if($count == 0) {
						$str .= "PropertySubType Eq '*".$key."*' Or PublicRemarks Eq '*".$key."*'";
					} else {
						$str .= " Or PropertySubType Eq '*".$key."*' Or PublicRemarks Eq '*".$key."*'";
					}
					$count++;
				}
				$filterStr .= $str.")";
			}
			if(!empty($d_type)) {
				if(!empty($filterStr)) {
					$str = " And (";
				}
				else {
					$str = "(";
				}
				$count = 0;
				foreach ($d_type as $key) {
					if($count == 0) {
						$str .= "PropertySubType Eq '*".$key."*' Or PublicRemarks Eq '*".$key."*'";
					} else {
						$str .= " Or PropertySubType Eq '*".$key."*' Or PublicRemarks Eq '*".$key."*'";
					}
					$count++;
				}
				$filterStr .= $str.")";
			}
			if(!empty($e_type)) {
				if(!empty($filterStr)) {
					$str = " And (";
				}
				else {
					$str = "(";
				}
				$count = 0;
				foreach ($e_type as $key) {
					if($count == 0) {
						$str .= "PropertySubType Eq '*".$key."*' Or PublicRemarks Eq '*".$key."*'";
					} else {
						$str .= " Or PropertySubType Eq '*".$key."*' Or PublicRemarks Eq '*".$key."*'";
					}
					$count++;
				}
				$filterStr .= $str.")";
			}
			if(!empty($f_type)) {
				if(!empty($filterStr)) {
					$str = " And (";
				}
				else {
					$str = "(";
				}
				$count = 0;
				foreach ($f_type as $key) {
					if($count == 0) {
						$str .= "PropertySubType Eq '*".$key."*' Or PublicRemarks Eq '*".$key."*'";
					} else {
						$str .= " Or PropertySubType Eq '*".$key."*' Or PublicRemarks Eq '*".$key."*'";
					}
					$count++;
				}
				$filterStr .= $str.")";
			}
			if(!empty($listing_change_type)) {
				if(!empty($filterStr)) {
					$str = " And (";
				}
				else {
					$str = "(";
				}
				$count = 0;
				foreach ($listing_change_type as $key) {
					if($count == 0) {
						$str .= "MajorChangeType Eq '".$key ."'";
					} else {
						$str .= " Or MajorChangeType Eq '".$key ."'";
					}
					$count++;
				}
				$filterStr .= $str.")";
			}
			if(!empty($features)) {
				if(!empty($filterStr)) {
					$str = " And (";
				}
				else {
					$str = "(";
				}
				$count = 0;
				foreach($features as $key) {
					if($count == 0) {
						$str .= "PublicRemarks Eq '*".$key ."*'";
					} else {
						$str .= " Or PublicRemarks Eq '*".$key ."*'";
					}
					$count++;
				}
				$filterStr .= $str.")";
			}
			$filterStr = preg_replace('/\s+/', ' ',$filterStr);
			$obj = modules::load("dsl");
			$filter_url = rawurlencode($filterStr);
			$filter_string = substr($filter_url, 3, -3);

			$config = $obj->get_config();

			$token = $this->dsl_model->getUserAccessTokenOld($this->domain_user->agent_id);

			if($token) {
				$access_token = array(
					'access_token' => $token->access_token,
					'bearer_token' => ""
				);
			} else {
				$bearer = $this->dsl_model->getUserBearerToken($this->domain_user->agent_id);

				$access_token = array(
					'access_token' =>"",
					'bearer_token' => $bearer->bearer_token
				);
			}
			
			$endpoint = "spark-listings?_limit=".$limit."&_pagination=1&_page=".$page."&_filter=(".$filter_string.")&_expand=PrimaryPhoto&_orderby=StarRating";
			$endpoint2 = "spark-listings?_limit=".$limit."&_pagination=1&_page=".$page."&_filter=(".$filterStr.")&_expand=PrimaryPhoto&_orderby=StarRating";
			
			//$results = $obj->post_endpoint($endpoint, $access_token);
			//$return = json_decode($results, true);
			$return = $this->direct_search($filter_string,$access_token, $page, $limit);

			if($return['data']) {
				/* Has Data */
			}
			else {
				/* 
					====Empty====
					* Save Information to database
					
				*/
				$data = array(
					'search_filter' => $filter_string,
					'response_message' => json_encode($return),
					'response_type'	=> 'Direct',
					'status'	=> isset($return['status']) ? $return['status'] : 404,
					'ip_address' => $this->input->ip_address(),
					'agent_id' => $this->domain_user->agent_id
				);
				$this->db->insert('search_logs', $data);
			}

		}
		?>
		<!DOCTYPE html>
		<html lang="en">
		  	<head>
			    <meta charset="utf-8">
			    <meta http-equiv="X-UA-Compatible" content="IE=edge">
			    <meta name="viewport" content="width=device-width, initial-scale=1">
			    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
			    <title>Search</title>

			    <!-- Bootstrap -->
			    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">

			    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
			    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
			    <!--[if lt IE 9]>
			      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
			      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
			    <![endif]-->
		  	</head>
		  	<body>
			  	<div class="container">
			  		<div class="row">
			  			<div class="col-md-3"></div>
			  			<div class="col-md-6 well">
			  				<form>
								<h1 class="text-center">AgentSquared Search Test</h1>
								<?php
									if(isset($return) && !empty($return['data'])) {
										echo "<b>Filter Constructed</b>: ".$filterStr."</br>";
										echo "<b>Encoded Filter</b>: <pre>".$filter_string."</pre></br>";
										echo "<b>Results Found</b>: ".count($return['data']);
										// echo "<pre>";
										// var_dump(count($return['data']));
										// echo "</pre>";	
									}
									else {
										echo "Filter Used: ".$filterStr;
										echo "<pre>";
										var_dump($return);
										echo "</pre>";	
									}
									
								?>
								<hr>
								<div class="form-group">
									<label>Enter Search : </label>
									<input type="text" name="Search" class="form-control" value="<?php if(isset($_GET['Search'])) { echo $_GET['Search']; } ?>">	
								</div>
								<div class="form-group">
									<input type="submit" class="btn btn-success">	
								</div>
							</form>
			  			</div>
			  			<div class="col-md-3"></div>
			  			
			  		</div>
			  		
			  	</div>
			   

			    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
			    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
			    <!-- Include all compiled plugins (below), or include individual files as needed -->
			    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		  	</body>
		</html>
			
		<?php
	}
	

	public function direct_search($filter_string="",$access_token = array(), $page="", $limit="", $repeated = FALSE) {
		$param_select = "PropertyClass,Photos(1).Uri300,UnparsedFirstLineAddress,PublicRemarks,UnparsedAddress,City,CountyOrParish,StateOrProvince,PostalCode,PostalCity,MlsStatus,CurrentPrice,BedsTotal,BathsTotal,LotSizeArea,Latitude,Longitude,LotSizeUnits,LotSizeSquareFeet,LotSizeAcres,ListingKey,ListAgentId,LotSizeDimensions,ListingUpdateTimestamp,ModificationTimestamp,MajorChangeTimestamp,BuildingAreaTotal";
		$accessToken = (!empty($access_token["access_token"])) ? $access_token["access_token"] : $access_token["bearer_token"];
		//$url = "https://sparkapi.com/v1/listings?_pagination=1&_page=".$page."&_limit=".$limit."&_filter=(".$filter_string.")&_expand=PrimaryPhoto";
		$url = "https://sparkapi.com/v1/listings?_pagination=1&_page=".$page."&_limit=".$limit."&_filter=(".$filter_string.")&_expand=PrimaryPhoto&_select=".$param_select."&_orderby=StarRating";
		$headers = array(
			'Content-Type: application/json',
			'User-Agent: Spark API PHP Client/2.0',
			'X-SparkApi-User-Agent: PHP-APP-In-Fifteen-Minutes/1.0',
		    'Authorization: OAuth '. $accessToken,
		);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_URL, $url);

		$result = curl_exec($ch);
		$result_info = json_decode($result, true);

		if(empty($result_info)) {
			if(!$repeated){
				$this->direct_search($filter_string,$access_token, $page, $limit, TRUE);
			}else{
				$result_info = curl_strerror($ch);
				return FALSE;
			}
				
			
		}
        curl_close($ch);
		// var_dump($url);
		// exit;
		$mapped_data = $this->map_results($result_info);
		return $mapped_data;
		//return ($result_info["D"]["Success"]) ? array('results' => $result_info["D"]["Results"], 'pagination'=>$result_info["D"]["Pagination"]) : FALSE;

	}
	
	public function map_results($result_info = array()){
	
		$mapped = array();
		$mapped_res = array();		
		if(!empty($result_info)){
			$i = 0;
			foreach($result_info  as $result){
				//printA($result); //exit;
				$mapped["status"] = 200;
				if( !empty($result["Results"]) ){
					foreach($result["Results"]  as $res){
						$mapped_res[$i]["StandardFields"] = $res["StandardFields"];
						if(isset($res["StandardFields"]["Photos"][0])){
							$mapped_res[$i]["Photos"] = $res["StandardFields"]["Photos"][0];
						}else{
							$mapped_res[$i]["Photos"] = array();
						}						
						$i++;	
					}
				}
				$mapped["data"] = $mapped_res;
				$mapped["pagination"] = isset($result["Pagination"])? $result["Pagination"]: null;
											
			}			
		}
		return $mapped;
		
	}

	public function search_custom_fields(){
		//$this->load->library("idx_auth", $this->domain_user->agent_id);
		$dsl = modules::load('dsl/dsl');
		$obj = Modules::load("mysql_properties/mysql_properties");
		$param_select = "PropertyClass,Photos(1).Uri300,UnparsedFirstLineAddress,PublicRemarks,UnparsedAddress,City,CountyOrParish,StateOrProvince,PostalCode,PostalCity,MlsStatus,CurrentPrice,BedsTotal,BathsTotal,LotSizeArea,Latitude,Longitude,LotSizeUnits,LotSizeSquareFeet,LotSizeAcres,ListingKey,ListAgentId,LotSizeDimensions,ListingUpdateTimestamp,ModificationTimestamp,MajorChangeTimestamp,BuildingAreaTotal";
		//$accessToken = (!empty($access_token["access_token"])) ? $access_token["access_token"] : $access_token["bearer_token"];
		//$url = "https://sparkapi.com/v1/listings?_pagination=1&_page=".$page."&_limit=".$limit."&_filter=(".$filter_string.")&_expand=PrimaryPhoto";
		//$url = "https://sparkapi.com/v1/listings?_pagination=1&_page=".$page."&_limit=".$limit."&_filter=(".$filter_string.")&_expand=PrimaryPhoto&_select=".$param_select."&_orderby=StarRating";
		$limit = 25;
		$page  = 1;
		$account_info = $obj->get_account($this->domain_user->id, "account_info");

		//$filter = '"\/"Amenities\/".\/"Lake Front\/" Eq "true"';
		$t1 = "Community Amenities";
		$t2 = "Heated Pool(s)";
		$filter = "\"$t1\".\"$t2\" Eq true";
		$filter2 = "\"Community Amenities\".\"Spa(s)\" Eq true";
		$filter3 = "\"Existing 2nd Ln Type\".\"AdjustableGraduated2\" Eq true";
		$filterStr = "MlsId Eq '".$account_info->MlsId."' And (PhotosCount Gt 0) AND (MlsStatus Eq 'Active') AND (".$filter." OR ".$filter2." OR ".$filter3.")";
		//$filterStr = "MlsId Eq '".$account_info->MlsId."' And (PhotosCount Gt 0) AND (MlsStatus Eq 'Active') AND (\"Community Amenities\".\"Heated Pool(s)\" Eq true OR \"Community Amenities\".\"Spa(s)\" Eq true OR \"Existing 2nd Ln Type\".\"AdjustableGraduated2\" Eq true)";
		echo $filterStr; 
		//$url = "https://sparkapi.com/v1/listings?_filter=(".$filter.")";
		//echo $url; exit;
		$accessToken = 'c4xmoo64zm6sjdjrfa2qs14bv';

		$results = $dsl->getSparkProxyPagination($page, $limit, urlencode($filterStr), false, $order = "+YearBuilt", $param_select);
		printA(count($results->Results));
		printA($results); exit;
		$headers = array(
			'Content-Type: application/json',
			'User-Agent: Spark API PHP Client/2.0',
			'X-SparkApi-User-Agent: PHP-APP-In-Fifteen-Minutes/1.0',
		    'Authorization: OAuth '. $accessToken,
		);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_URL, $url);

		$result = curl_exec($ch);
		$result_info = json_decode($result, true);
		printA($result); exit;
		if(empty($result_info)) {
			if(!$repeated){
				$this->direct_search($filter_string,$access_token, $page, $limit, TRUE);
			}else{
				$result_info = curl_strerror($ch);
				return FALSE;
			}
				
			
		}
        curl_close($ch);
		printA($result_info);
		// var_dump($url);
		exit;
		//$mapped_data = $this->map_results($result_info);
		//return $mapped_data;
	}

	public function is_searchable_custom(){
		
		$url = "https://sparkapi.com/v1/customfields";
		//echo $url; exit;
		$accessToken = 'c4xmoo64zm6sjdjrfa2qs14bv';

		$token = $this->dsl_model->getUserAccessTokenOld($this->domain_user->agent_id);

		if($token) {
			$access_token = array(
				'access_token' => $token->access_token,
				'bearer_token' => ""
			);
		} else {
			$bearer = $this->dsl_model->getUserBearerToken($this->domain_user->agent_id);

			$access_token = array(
				'access_token' =>"",
				'bearer_token' => $bearer->bearer_token
			);
		}

		$accessToken = (!empty($access_token["access_token"])) ? $access_token["access_token"] : $access_token["bearer_token"];

		$headers = array(
			'Content-Type: application/json',
			'User-Agent: Spark API PHP Client/2.0',
			'X-SparkApi-User-Agent: PHP-APP-In-Fifteen-Minutes/1.0',
		    'Authorization: OAuth '. $accessToken,
		);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_URL, $url);

		$result = curl_exec($ch);
		$result_info = json_decode($result, true);
		printA($result_info['D']['Results']); exit;
		if(empty($result_info)) {
			if(!$repeated){
				$this->direct_search($filter_string,$access_token, $page, $limit, TRUE);
			}else{
				$result_info = curl_strerror($ch);
				return FALSE;
			}
				
			
		}
        curl_close($ch);
		printA($result_info);
		// var_dump($url);
		exit;
		//$mapped_data = $this->map_results($result_info);
		//return $mapped_data;
	}

	public function is_searchable_standard(){
		
		$url = "https://sparkapi.com/v1/standardfields";
		//echo $url; exit;
		$accessToken = 'c4xmoo64zm6sjdjrfa2qs14bv';

		$token = $this->dsl_model->getUserAccessTokenOld($this->domain_user->agent_id);

		if($token) {
			$access_token = array(
				'access_token' => $token->access_token,
				'bearer_token' => ""
			);
		} else {
			$bearer = $this->dsl_model->getUserBearerToken($this->domain_user->agent_id);

			$access_token = array(
				'access_token' =>"",
				'bearer_token' => $bearer->bearer_token
			);
		}

		$accessToken = (!empty($access_token["access_token"])) ? $access_token["access_token"] : $access_token["bearer_token"];

		$headers = array(
			'Content-Type: application/json',
			'User-Agent: Spark API PHP Client/2.0',
			'X-SparkApi-User-Agent: PHP-APP-In-Fifteen-Minutes/1.0',
		    'Authorization: OAuth '. $accessToken,
		);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_URL, $url);

		$result = curl_exec($ch);
		$result_info = json_decode($result, true);
		printA($result_info['D']['Results']); exit;
		if(empty($result_info)) {
			if(!$repeated){
				$this->direct_search($filter_string,$access_token, $page, $limit, TRUE);
			}else{
				$result_info = curl_strerror($ch);
				return FALSE;
			}
				
			
		}
        curl_close($ch);
		printA($result_info);
		// var_dump($url);
		exit;
		//$mapped_data = $this->map_results($result_info);
		//return $mapped_data;
	}

	public function is_searchable_standard_with_ext(){
		
		$url = "https://sparkapi.com/v1/standardfields/".$_GET['ext'];
		//echo $url; exit;
		$accessToken = 'c4xmoo64zm6sjdjrfa2qs14bv';

		$token = $this->dsl_model->getUserAccessTokenOld($this->domain_user->agent_id);

		if($token) {
			$access_token = array(
				'access_token' => $token->access_token,
				'bearer_token' => ""
			);
		} else {
			$bearer = $this->dsl_model->getUserBearerToken($this->domain_user->agent_id);

			$access_token = array(
				'access_token' =>"",
				'bearer_token' => $bearer->bearer_token
			);
		}

		$accessToken = (!empty($access_token["access_token"])) ? $access_token["access_token"] : $access_token["bearer_token"];

		$headers = array(
			'Content-Type: application/json',
			'User-Agent: Spark API PHP Client/2.0',
			'X-SparkApi-User-Agent: PHP-APP-In-Fifteen-Minutes/1.0',
		    'Authorization: OAuth '. $accessToken,
		);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_URL, $url);

		$result = curl_exec($ch);
		$result_info = json_decode($result, true);
		printA($result_info['D']['Results']); exit;
		if(empty($result_info)) {
			if(!$repeated){
				$this->direct_search($filter_string,$access_token, $page, $limit, TRUE);
			}else{
				$result_info = curl_strerror($ch);
				return FALSE;
			}
				
			
		}
        curl_close($ch);
		printA($result_info);
		// var_dump($url);
		exit;
		//$mapped_data = $this->map_results($result_info);
		//return $mapped_data;
	}

	public function is_searchable_custom_with_ext(){
		
		$url = "https://sparkapi.com/v1/customfields/".$_GET['ext'];
		//echo $url; exit;
		$accessToken = 'c4xmoo64zm6sjdjrfa2qs14bv';

		$token = $this->dsl_model->getUserAccessTokenOld($this->domain_user->agent_id);

		if($token) {
			$access_token = array(
				'access_token' => $token->access_token,
				'bearer_token' => ""
			);
		} else {
			$bearer = $this->dsl_model->getUserBearerToken($this->domain_user->agent_id);

			$access_token = array(
				'access_token' =>"",
				'bearer_token' => $bearer->bearer_token
			);
		}

		$accessToken = (!empty($access_token["access_token"])) ? $access_token["access_token"] : $access_token["bearer_token"];

		$headers = array(
			'Content-Type: application/json',
			'User-Agent: Spark API PHP Client/2.0',
			'X-SparkApi-User-Agent: PHP-APP-In-Fifteen-Minutes/1.0',
		    'Authorization: OAuth '. $accessToken,
		);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_URL, $url);

		$result = curl_exec($ch);
		$result_info = json_decode($result, true);
		printA($result_info['D']['Results']); exit;
		if(empty($result_info)) {
			if(!$repeated){
				$this->direct_search($filter_string,$access_token, $page, $limit, TRUE);
			}else{
				$result_info = curl_strerror($ch);
				return FALSE;
			}
				
			
		}
        curl_close($ch);
		printA($result_info);
		// var_dump($url);
		exit;
		//$mapped_data = $this->map_results($result_info);
		//return $mapped_data;
	}
	
}
