<?php if( ! defined('BASEPATH') ) exit('No direct script access allowed');

class Search_model extends CI_Model { 

    public function __construct() {
        parent::__construct();

        $this->db_slave =  $this->load->database('db_slave', TRUE);
    }
    public function register_mls_data_search_fields($data = null) {
        $ret = false;
        if($data) {
            $this->db->insert('mls_data_search_fields_list_data', $data); 
            $ret = $this->db->insert_id();
        }
        return $ret;
    }
    public function register_search_fields($data = NULL) {
    	$ret = false;
    	if($data) {
    		$this->db->insert('mls_data_search_fields', $data); 
    		$ret = $this->db->insert_id();
    	}
    	return $ret;
    }
    public function get_mls_search_data($mls_id = null) {
        $ret = false;
        if($mls_id) {
            $this->db_slave->select("*")->from("mls_data_search_fields")->where("mls_id", $mls_id)->order_by('search_group_placement','asc');
            $ret = $this->db_slave->get()->result();
        }
        return $ret;
    }
    public function get_mls_data_search_fields_list_data($id = null, $search_fields = null) {
        $ret = false;
        if($id) {
            $this->db_slave->select("*")->from("mls_data_search_fields_list_data")->where("mls_searc_field_id", $id);
            $ret = $this->db_slave->get()->result();
        }
        else if($search_fields) {
            $sf_ids = array();
            foreach($search_fields as $sf) {
                if($sf->field_haslist) {
                    $sf_ids[] = $sf->id;
                }
                
            }
            $this->db_slave->select("*")->from("mls_data_search_fields_list_data")->where_in("mls_searc_field_id", $sf_ids);
            $ret = $this->db_slave->get()->result();
        }
        return $ret;
    }
    public function get_mls_search_group($mls_id = null) {
        $ret = false;
        if($mls_id) {
            $this->db_slave->select("*")->from("mls_data_search_fields_groups")->where("mls_id", $mls_id);
            $ret = $this->db_slave->get()->result();
        }
        return $ret;
    }
}
