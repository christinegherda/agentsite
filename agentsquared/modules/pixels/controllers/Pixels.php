<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pixels extends MX_Controller {
  private $agent_id = '';
  private $user_id = '';
  protected $domain_user;
  
  function __construct(){
    parent::__construct();
    
    $this->load->model('pixels/pixels_model', 'pixels');
    $this->domain_user = domain_to_user();
    $this->agent_id = $this->domain_user->agent_id;
  }
  
  public function facebook(){
    $result = $this->pixels->get($this->agent_id);
    if($result){
      return $result->facebook_pixel;
    }
    return false;
  }
}
?>
