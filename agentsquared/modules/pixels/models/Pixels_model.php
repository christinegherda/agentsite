<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pixels_model extends CI_Model{ 

  function __construct() {
      parent:: __construct();

      $this->db_slave =  $this->load->database('db_slave', TRUE);
  }
	
  function get($agent_id = ''){
    if(!empty($agent_id)){
      $this->db_slave->where('agent_id', $agent_id);
      $result = $this->db_slave->get('seo')->row();
      if($result && $result != NULL){
        return $result;
      }
    }
    return false;
  }
  
}

