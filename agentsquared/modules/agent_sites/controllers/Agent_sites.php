<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Agent_sites extends MX_Controller {

	protected $domain_user;

	function __construct() {

		parent::__construct();

		$this->domain_user = domain_to_user();
		$this->load->model("Agent_sites_model");
		$this->load->model("dsl/dsl_model");
		$this->load->model("idx_login/Idx_model");
		$this->load->library("idx_auth", $this->domain_user->agent_id);
	}

	public function index() {
		$this->infos();
	}

	public function delete_slider_photo($id = NULL) {

		$this->Agent_sites_model->delete_slider_photo($id);
		redirect("agent_sites/infos", "refresh");
	}

	public function upload_slider_photos() {
		
		if($_FILES) {	
			$return = $this->do_slider_photo_upload();
			$this->session->set_flashdata('session' , array('status' => TRUE,'message' => 'Slider Photo has been successfully added!'));
		} else {
			$this->session->set_flashdata('session' , array('status' => TRUE,'message' => 'Failed to update info!'));
		}
					
		redirect("agent_sites/infos", "refresh");	
	}

	public function do_slider_photo_upload(){

		$this->load->library('upload');

		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		$config['encrypt_name'] = TRUE;

		$files = $_FILES;

		$count = count($_FILES['userfile']['name']);
		//printA($_FILES); exit;
		for ($i=0; $i < $count; $i++) { 

			if( $_SERVER["SERVER_NAME"] == "localhost")
				{
						$config['upload_path'] = FCPATH.'assets/upload/slider';
				} 
					else
				{
					$config['upload_path'] = $_SERVER["DOCUMENT_ROOT"].'/agent-site/assets/upload/slider';
				}

				$this->load->library('upload', $config);

			    $_FILES['userfile']['name']= $files['userfile']['name'];
		        $_FILES['userfile']['type']= $files['userfile']['type'];
		        $_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'];
		        $_FILES['userfile']['error']= $files['userfile']['error'];
		        $_FILES['userfile']['size']= $files['userfile']['size']; 
			        
				$this->upload->initialize($config); 

			if( $this->upload->do_upload() )
			{
				$img_data = $this->upload->data();
				//printA($img_data); exit;

				//resize image:
		        $config['image_library'] = 'gd2';
		        $config['source_image'] = $img_data['full_path'];
		        $config['maintain_ratio'] = TRUE;
		        $config['width']     = 1120;
		        $config['height']   = 680;

		        $this->load->library('image_lib', $config); 
		        $this->image_lib->resize();

		        $slider_photo = $this->Agent_sites_model->add_slider_photo($img_data);

				if( $slider_photo )
				{
					$data = array('status' => TRUE);
				}
				else
				{
					$data = array('status' => FALSE);	
				}
				
			}
			else
			{
				$data = array('status' => FALSE, "error" => $this->upload->display_errors() );			
			}

		}
		
		return $data;
	}

	public function upload_agent_photo() {
		
				 if( $_FILES )
				 {	
					$return = $this->do_agent_photo_upload();

					$this->session->set_flashdata('session' , array('status' => TRUE,'message' => 'Agent Photo has been successfully added!'));

				  }else
				  {
				  	$this->session->set_flashdata('session' , array('status' => TRUE,'message' => 'Failed to update info!'));
				  }
								
			redirect("single_property_listings", "refresh");
		
	}

	public function do_agent_photo_upload(){

		$this->load->library('upload');

		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		$config['encrypt_name'] = TRUE;

		$files = $_FILES;

		$count = count($_FILES['userfile']['name']);
		//printA($_FILES); exit;
		for ($i=0; $i < $count; $i++) { 

			if( $_SERVER["SERVER_NAME"] == "localhost")
				{
						$config['upload_path'] = FCPATH.'assets/upload/photo';
				} 
					else
				{
					$config['upload_path'] = $_SERVER["DOCUMENT_ROOT"].'/agent-site/assets/upload/photo';
				}

				$this->load->library('upload', $config);

			    $_FILES['userfile']['name']= $files['userfile']['name'];
		        $_FILES['userfile']['type']= $files['userfile']['type'];
		        $_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'];
		        $_FILES['userfile']['error']= $files['userfile']['error'];
		        $_FILES['userfile']['size']= $files['userfile']['size']; 
			        
				$this->upload->initialize($config); 

			if( $this->upload->do_upload() )
			{
				$img_data = $this->upload->data();
				//printA($img_data); exit;

				//resize image:
		        $config['image_library'] = 'gd2';
		        $config['source_image'] = $img_data['full_path'];
		        $config['maintain_ratio'] = TRUE;
		        $config['width']     = 250;
		        $config['height']   = 250;

		        $this->load->library('image_lib', $config); 
		        $this->image_lib->resize();

		        $slider_photo = $this->Agent_sites_model->add_agent_photo($img_data);

				if( $slider_photo )
				{
					$data = array('status' => TRUE);
				}
				else
				{
					$data = array('status' => FALSE);	
				}
				
			}
			else
			{
				$data = array('status' => FALSE, "error" => $this->upload->display_errors() );			
			}

		}
		
		return $data;
	}

	public function getInfo() {

		$obj = Modules::load("mysql_properties/mysql_properties");

		$data["branding"] = $this->Agent_sites_model->get_branding_infos();
		$data["slider_photos"] = $this->Agent_sites_model->get_slider_photos();	
		$data['account_info'] = $obj->get_account($this->domain_user->id, "account_info");//Modules::run('dsl/getAgentAccountInfo');

		$param["mlsID"] = $data["account_info"]->Id;

		$data["properties"] = $this->Agent_sites_model->get_your_properties_listings(FALSE, $param);		
		
		return $data;
	}
	
	public function infos() {	
		$data['title'] = "Agent Site Info";

		if( $_POST )
		{	
			
			if( $this->Agent_sites_model->update() )
			{
				if( $_FILES )
				{	
					$return = $this->do_img_upload();

					$this->session->set_flashdata('session' , array('status' => TRUE,'message' => 'Agent info has been successfully added!'));

				}else
				{
					$this->session->set_flashdata('session' , array('status' => TRUE,'message' => 'Agent info has been successfully added!'));
				}
								
			}
			else
			{
				$this->session->set_flashdata('session' , array('status' => TRUE,'message' => 'Failed to update info!'));
			}

			redirect("agent_sites/infos", "refresh");
		}


		$data["branding"] = $this->Agent_sites_model->get_branding_infos();
		$data["is_reserved_domain"] = $this->Agent_sites_model->is_reserved_domain();
		$data["domain_info"] = $this->Agent_sites_model->fetch_domain_info();
		$data["slider_photos"] = $this->Agent_sites_model->get_slider_photos();	
		$data['account_info'] = Modules::run('dsl/getAgentAccountInfo');	
		$data["domain_info"] = $this->Agent_sites_model->fetch_domain_info();
		$data["js"] = array("plugins/jquery.msgBox.js","plugins/tinymce/tinymce.min.js","blogs/blogs.js","summernote-image-attributes.js");
		//printA($data["slider_photos"]); exit();

		$this->load->view('agent_site_info', $data);
	}

	public function insert_site_info($data) {
		$arr = array(
			'id' => $data['id'],
			'site_name' => $data['site_name'],
			'tag_line' => $data['tag_line'],
		);
		if( $_POST ) {

			if($this->Agent_sites_model->insert_info($arr)) {
				if( $_FILES )
				{	
					$this->do_img_upload();
				}
			}
		}
	}

	public function do_img_upload()
	{
		$this->load->library('upload');

		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size'] = '1150000';
		$config['max_width'] = '1024';
		$config['max_height'] = '1024';
		
		$files = $_FILES;
		$count = count($_FILES['userfile']['name']);
		//printA($_FILES); exit;
		//echo $_FILES['userfile["logo"]']; exit;
		for ($i=0; $i < $count; $i++) { 

			if( $i == '0' )
			{
				if( $_SERVER["SERVER_NAME"] == "localhost")
				{
					$config['upload_path'] = FCPATH.'assets/upload/logo';
				} 
				else
				{
					$config['upload_path'] = $_SERVER["DOCUMENT_ROOT"].'/agent-site/assets/upload/logo';
				}
						
			}
			else if ( $i == '1' )
			{
				if( $_SERVER["SERVER_NAME"] == "localhost")
				{
					$config['upload_path'] = FCPATH.'assets/upload/favicon';
						
				} 
				else
				{
					$config['upload_path'] = $_SERVER["DOCUMENT_ROOT"].'/agent-site/assets/upload/favicon';
				
				}
				
			}
			else
			{
				if( $_SERVER["SERVER_NAME"] == "localhost")
				{
					$config['upload_path'] = FCPATH.'assets/upload/photo';
						
				} 
				else
				{
					$config['upload_path'] = $_SERVER["DOCUMENT_ROOT"].'/agent-site/assets/upload/photo';
				
				}

			}
			//echo $config['upload_path']; exit;
			$this->load->library('upload', $config);

			$_FILES['userfile']['name']= $files['userfile']['name'][$i];
	        $_FILES['userfile']['type']= $files['userfile']['type'][$i];
	        $_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$i];
	        $_FILES['userfile']['error']= $files['userfile']['error'][$i];
	        $_FILES['userfile']['size']= $files['userfile']['size'][$i];   
	        
			$this->upload->initialize($config); 

			if( $this->upload->do_upload() )
			{
				$img_data = $this->upload->data();
				//printA($img_data); exit;
				if( $i == '0' )
				{
					$res = $this->Agent_sites_model->update_logo_info($img_data);
				}
				else if ( $i == '1' )
				{
					$res = $this->Agent_sites_model->update_favicon_info($img_data);
				}
				else
				{
					$res = $this->Agent_sites_model->update_agent_photo_info($img_data);
				}

				if( $res )
				{
					$data = array('status' => TRUE);
				}
				else
				{
					$data = array('status' => FALSE);	
				}
				
			}
			else
			{
				//echo $this->upload->display_errors(); exit;
				$data = array('status' => FALSE, "error" => $this->upload->display_errors() );					
			}

		}

		return $data;
	}

	public function spark_member()
	{
		$data["title"]  =  "Spark Member";
		$this->load->view('spark_member', $data);
	}

	public function saved_searches(){

		$data["title"] = "Saved Searches";

		$this->load->library('pagination');

		$param["offset"] = (isset($_GET["offset"])) ? $_GET["offset"] : 1;

		$param["limit"] = 24;
		$savedsearches = Modules::run('dsl/getAgentSavedSearch', array("page" => $param["offset"],  "limit" => $param["limit"]) );

		if(!empty($savedsearches["data"]) && !empty($savedsearches["count"])){

			$total = $savedsearches["count"];

			$config["base_url"] = base_url().'/agent_sites/saved_searches';
			$config["total_rows"] = $total;
			$config["per_page"] = $param["limit"];
			$config['page_query_string'] = TRUE;
			$config['query_string_segment'] = 'offset';
			
			$data["savedsearches"] = $savedsearches["data"];

			$this->pagination->initialize($config);

		}
		
		$data['css'] = array('dashboard/bootstrap-datepicker.css', 'dashboard/bootstrap-datepicker.standalone.css', 'dashboard/bootstrap-datepicker3.css');
		$data["js"] = array("plugins/tablesorter/jquery.tablesorter.js");

		$data["pagination"] = $this->pagination->create_links();
		$data["domain_info"] = $this->Agent_sites_model->fetch_domain_info();
		$data["is_reserved_domain"] = $this->Agent_sites_model->is_reserved_domain();
		$data["branding"] = $this->Agent_sites_model->get_branding_infos();

		$token = $this->dsl_model->getUserAccessToken($this->domain_user->agent_id);
		$access_token = $token->access_token;
		$saved_search = $this->saved_searches_live($access_token);
		$data["live_savedsearches"] = $saved_search["D"]["Results"];

		$this->load->view('saved-searches', $data);
	}

	public function edit_saved_search( $search_id = NULL )
	{	
		if(empty($search_id)) return FALSE;

		if( $_POST )
		{	
			$updateName = array(

			  'Name' => $this->input->post("search_name")
			);

			$dsl = modules::load('dsl/dsl'); 
			$updateSavedSearchName = $dsl->put_endpoint('savedsearch/'.$this->domain_user->id.'/'.$search_id.'/update', $updateName);

			if( $updateSavedSearchName)
			{
				$this->session->set_flashdata('session' , array('status' => TRUE, 'message'=>'Search name has been successfully edited!'));
			}
			else
			{
				$this->session->set_flashdata('session' , array('status' => FALSE,'message'=>'Failed to edit name!'));
			}
		}
		
		redirect(base_url()."agent_sites/saved_searches", "refresh");

	}

	public function saved_search( $search_id = NULL ) {  

		if(isset($_GET['page'])) {
			$page = $_GET['page'];
		}
		else {
			$page = 1;
		}
		
		$savedSearch = $this->idx_auth->getSavedSearch($search_id);

		$dsl = modules::load('dsl/dsl');
		$token = $this->dsl_model->getUserAccessToken($this->domain_user->agent_id);
		$access_token = array(
			'access_token' => $token->access_token
		);

		$filterStr = "(".$savedSearch[0]['Filter'].")";
		$filter_url = rawurlencode($filterStr);
		$filter_string = substr($filter_url, 3, -3);

		$endpoint = "spark-listings?_pagination=1&_page=".$page."&_limit=24&_expand=PrimaryPhoto&_filter=(".$filter_string.")";
		$results = $dsl->post_endpoint($endpoint, $access_token);
		$savedSearches['results'] = json_decode($results, true);

		$data["properties"] = $savedSearches['results'];
		//printA($data["properties"]);exit;

		$data['css'] = array('dashboard/bootstrap-datepicker.css', 'dashboard/bootstrap-datepicker.standalone.css', 'dashboard/bootstrap-datepicker3.css');
		
		$data["domain_info"] = $this->Agent_sites_model->fetch_domain_info();
		$data["branding"] = $this->Agent_sites_model->get_branding_infos();	
		
		$this->load->view('saved-search', $data);

	}

	public function saved_searches_live($access_token) {

		if($access_token) {
			$url = "https://sparkapi.com/v1/savedsearches";

			$headers = array(
				'Content-Type: application/json',
				'User-Agent: Spark API PHP Client/2.0',
				'X-SparkApi-User-Agent: PHP-APP-In-Fifteen-Minutes/1.0',
			    'Authorization: OAuth '. $access_token,
			);

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_URL, $url);

			$result = curl_exec($ch);
			$result_info = json_decode($result, true);

			if(empty($result_info)) {
				$result_info = curl_strerror($ch);
			}
            curl_close($ch);
			return $result_info;
		}  
	}

}
