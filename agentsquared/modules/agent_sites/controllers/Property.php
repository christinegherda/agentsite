<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Property extends MX_Controller {

	function __construct() {

		parent::__construct();
		
		$this->load->model('agent_sites_model', 'site_model');
		$this->load->model('featured_listings/featured_listings_model', 'featured');
	}
	
	function index() {

	}

	public function get_activated_listings($account_id) {
		$this->db->select("*")
                ->from("property_activated")
                ->where('list_agentId', $account_id);
        $listings = $this->db->get()->result();
        // echo "<pre>";
        // var_dump($listings);
        // echo "</pre>";
        return $listings;
	}

	public function get_property_listings($account_id) {
		$this->db->select("*")
                ->from("property_listing_data")
                ->where('ListingID', $account_id);
        $listings = $this->db->get()->result();
        echo "<pre>";
        var_dump($listings);
        echo "</pre>";
        return $listings;
	}

	public function linksInfo() {

		$fetch_link = $this->agent_sm_model->fetch_link();

		if(!$fetch_link) {
			$data['facebook'] = "";
			$data['twitter'] = "";
			$data['linkedin'] = "";
			$data['googleplus'] = "";
		} else {
			$data['facebook'] = $fetch_link[0]['facebook'];
			$data['twitter'] = $fetch_link[0]['twitter'];
			$data['linkedin'] = $fetch_link[0]['linkedin'];
			$data['googleplus'] = $fetch_link[0]['googleplus'];
		}
		return $data;

	}

}