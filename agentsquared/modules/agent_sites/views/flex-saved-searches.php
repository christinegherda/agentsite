<?php 
    $this->load->view('session/header'); 
    $this->load->view('session/top-nav');
    // $this->load->view('session/left-nav');
?>
<!-- Full Width Column -->
<div class="content-wrapper">
  <div class="page-title">
    <h3 class="heading-title">
      <span> Saved Search</span>
    </h3>
  </div>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <?php if(isset($update_saved_searches) && !empty($has_saved_search)){?>
                <div class="col-md-12">
                    <h4 class='alert alert-danger alert-dismissible fade in' role='alert'><i class='fa fa-exclamation-triangle pull-left'></i>You have an update of your saved search. Please Update it now!<button class='btn btn-submit pull-right update_list_btn' id='update_saved_searches'><i class='fa fa-cloud-download'></i> Update now</button></h4>
                </div>

            <?php }?>
            
            <?php if($has_saved_search) {?>
                <div class="col-md-12">                         
                        <?php $session = $this->session->flashdata('session');                                  
                            if( isset($session) )
                            {
                        ?>
                            <div class="alert <?php echo ($session["status"]) ? "alert-success" : "alert-danger" ?>" role="alert"><?php echo $session["message"]; ?></div>  
                        <?php } ?>
                    
                 </div>
                <div class="col-md-12">
                    <div class="meeting-list">
                        <table class="table-responsive tablesorter table table-striped" id="savedSearchTable">
                            <thead>
                                <tr>
                                    <!--<th>Select</th>-->
                                    <th>Name</th>
                                    <th>Links</th>
                                    <th>Modification Date</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if( !empty($savedsearches["saved_searches"]) ) : 
                                    foreach( $savedsearches["saved_searches"] as $savedsearch) :                                 ?>
                                        <tr>
                                            <!--<td> <input name="is_recommended[]" value="<?=$savedsearch->is_recommended_search;?>" type="checkbox" id="is_recomemded_search_<?=$savedsearch->id;?>" data-id="<?=$savedsearch->id;?>" class="is_recomemded_search" <?php echo ( $savedsearch->is_recommended_search == "1" ) ? 'checked="checked"': ""; ?>></td>-->
                                            <td><?php echo (isset($savedsearch["Name"])) ? $savedsearch["Name"]: ""; ?></td>
                                            <td><a href="<?php echo site_url('agent_sites/property/saved_search/'.$savedsearch["Id"]); ?>">View Search</a></td>
                                            <td><?php echo date("F d, Y H:m:s",strtotime($savedsearch["ModificationTimestamp"]))?></td>   
                                            <td><a href="<?php echo site_url('agent_sites/edit_saved_search/'.$savedsearch["Id"]); ?>" title="Edit Save Name" data-search-name="<?=$savedsearch["Name"];?>" data-search-id="<?=$savedsearch["Id"];?>" id="edit_search" class="edit_search"  ><span class="glyphicon glyphicon-edit" aria-hidden="true"></span> </a></td>   
                                        </tr>
                                    <?php endforeach;?>
                                <?php else : ?>
                                    <tr align="center"><td colspan="10"><i>no records found!</i></td></tr>
                                <?php endif;?>
                                
                            </tbody>
                        </table>

                        <div class="col-md-12">
                            <?php if( isset($pagination) ) : ?>
                                <div class="pagination-area pull-right">
                                    <nav>
                                        <ul class="pagination">
                                            <?php echo $pagination; ?>
                                        </ul>
                                    </nav>
                                </div>
                            <?php endif; ?>
                            
                        </div>
                    </div>
                </div>
            <?php } else { ?>
                <div class="col-md-12">
                    <h4 id="saved_search_tagline">You haven't saved your saved searches yet. Please the click the button below.</h4>
                    <button type="button" class="btn btn-default btn-submit" id="sync_saved_searches">Sync My Saved Searches</button>
                </div>
            <?php } ?> 
        </div>
    </section>
</div>
<div id="edit-search-modal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Edit Search Name </h4>
            </div>
            <div class="modal-body">
                <form action="" method="post" id="sbt-edit-search-name" class="sbt-edit-search-name" >
                    <div class="form-group">
                        <label for="exampleInputEmail1">Search  Name</label>
                        <input type="text" name="search_name" class="form-control search_name" id="search_name" placeholder="Name">
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<?php $this->load->view('session/footer'); ?>

<script type="text/javascript">
    
    $(".edit_search").on( "click", function(e) {
        e.preventDefault();
       
        $(".search_name").val($(this).data("search-name"));
        $("#sbt-edit-search-name").attr("action", $(this).attr("href") );
        $("#edit-search-modal").modal("show");

    }) ;

    $(".is_recomemded_search").on("click", function (e) {
       
        var id = $(this).data("id");
        var val = $(this).val();
        var url = base_url + "agent_sites/is_recommended";

        $.ajax({
                url : url,
                type : "post",
                data : {"is_recommended" : val, "search_id" : id},
                dataType: "json",                
                success: function( data ){ 

                    console.log(data); 
                    if(data.success)
                    {
                        $("#is_recomemded_search_"+id).val(data.val);
                    }       
                    
                }
            });

    });


  $(document).ready(function() 
    { 
      $("#savedSearchTable").tablesorter( ); 
    } 
  ); 

        
</script>