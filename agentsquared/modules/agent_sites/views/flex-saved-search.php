<?php 
    $this->load->view('session/header'); 
    $this->load->view('session/top-nav');
    // $this->load->view('session/left-nav');
?> 
  <div class="content-wrapper">
    <section class="content">
            <?php //if( !empty(config_item("idx_api_key")) AND !empty(config_item("idx_api_secret")) ) : ?>
                <?php $this->load->view('session/launch-view'); ?>
            <?php //endif; ?>
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header mobiletop-10">
                            Saved Search
                        </h1>
                    </div>
                    <div class="featured-list">
                        <?php 
                            if( !empty($properties) ) {
                                foreach($properties as $property) {
                        ?>
                                <div class="col-md-4 col-lg-3 col-sm-6">
                                    <div class="featured-list-item">
                                        <div class="featured-list-image">
                                            <?php if(isset( $property["StandardFields"]["Photos"]["0"]["Uri300"] )) { ?>
                                                <a href="<?=base_url('home/other_property?listingId=');?><?php echo $property["StandardFields"]["ListingKey"];?>" target="_blank"><img src="<?=$property["StandardFields"]["Photos"]["0"]["Uri300"]?>" alt="<?php echo $property["StandardFields"]["UnparsedFirstLineAddress"]; ?>" class="img-responsive" style="width:100%;"></a>
                                                
                                             <?php } else { ?>

                                                <a href="<?=base_url('home/other_property?listingId=');?><?php echo $property["StandardFields"]["ListingKey"];?>" target="_blank"><img src="<?=base_url()?>assets/images/image-not-available.jpg" alt="<?php echo $property["StandardFields"]["UnparsedFirstLineAddress"]; ?>" class="img-responsive" style="width:100%;"></a>

                                             <?php } ?>

                                        </div>
                                        <div class="property-quick-icons">
                                            <ul class="list-inline">
                                                <?php
                                                    if($property["StandardFields"]["BedsTotal"] && is_numeric($property["StandardFields"]["BedsTotal"])) {
                                                ?>
                                                <li><i class="fa fa-bed"></i> <?=$property["StandardFields"]["BedsTotal"]?> Bed</li>
                                                <?php
                                                    }
                                                    if($property["StandardFields"]["BathsTotal"] && is_numeric($property["StandardFields"]["BathsTotal"])) {
                                                ?>
                                                <li><i class="icon-toilet"></i> <?=$property["StandardFields"]["BathsTotal"]?> Bath</li>
                                                <?php
                                                    }
                                                ?>
                                            </ul>
                                        </div>
                                        <div class="property-listing-description">
                                            <h5><a href=""><?php echo $property["StandardFields"]["UnparsedFirstLineAddress"]; ?></a></h5>
                                            <p><i class="fa fa-map-marker"></i>
                                                <?php 
                                                    if(strpos($property["StandardFields"]["CountyOrParish"], '*'))
                                                        echo $property["StandardFields"]["CountyOrParish"] . " " . $property["StandardFields"]["StateOrProvince"] . ", " . $property["StandardFields"]["PostalCode"];
                                                    else
                                                        echo $property["StandardFields"]["PostalCity"] . " " . $property["StandardFields"]["StateOrProvince"] . ", " . $property["StandardFields"]["PostalCode"];
                                                
                                                ?>
                                            </p> 
                                            <p><i class="fa fa-usd"></i> <?=number_format($property["StandardFields"]["CurrentPrice"])?></p>
                                            <!-- <a href="<?= base_url();?>featured_listings/property_details?listingId=<?= $property->ListingKey; ?>" class="btn btn-default btn-submit"><i class="fa fa-pencil-square-o"></i> Edit Property</a> -->
                                            <a href="<?=base_url('home/other_property?listingId=');?><?php echo $property["StandardFields"]["ListingKey"];?>" target="_blank" class="btn btn-default btn-submit"><i class="fa fa-eye"></i> View Property</a>
                                        </div>
                                    </div>
                                </div>
                            <?php
                                }
                            }else{
                            ?>

                            <div class="col-md-12">
                                <p>Results not found. </p>
                            </div>
                            <?php } ?>
                    </div>
                </div>      
    </section>
</div>
<?php $this->load->view('session/footer'); ?>
