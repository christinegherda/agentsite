<?php 
    $this->load->view('session/header'); 
    $this->load->view('session/top-nav');
    // $this->load->view('session/left-nav');

    
?>
<!-- Full Width Column -->
<div class="content-wrapper">
  <div class="page-title">
    <h3 class="heading-title">
      <span> Agent Site Info</span>
    </h3>
    <p>Enter your site's details, logo and agent info</p>
  </div> 
  <!-- Main content -->
  <section class="content">
    <?php if( !empty(config_item("idx_api_key")) AND !empty(config_item("idx_api_secret")) ) : ?>
        <?php $this->load->view('session/launch-view'); ?>
    <?php endif; ?>
        <div class="row">
            <div class="col-md-12">
                 <p class="alert alert-info"><i class="fa fa-info-circle"></i>  All edits to the data will only be reflected on your website. This system will not update your MLS data</p>
            </div>                
            <?php $session = $this->session->flashdata('session');                                  
                if( isset($session) )
                {
            ?>
            <div class="col-md-12 alert-status"> 
                <div class="alert-flash alert <?php echo ($session["status"]) ? "alert-success" : "alert-danger" ?>" role="alert"><?php echo $session["message"]; ?></div>
            </div>  
            <?php } ?>  
        </div> 
            <div class="row">
                <div class="col-md-6">








          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Branding</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
             <form action="" class="form-horizontal" method="POST" enctype="multipart/form-data" id="saveAgentInfos">
              <div class="box-body">

                    <div class="form-group">
                        <label for="" class="col-md-2">Site Name</label>
                        <div class="col-md-10">
                            <input type="text" name="site_name" value="<?php echo ($branding->site_name) ? $branding->site_name : "";?>" class="form-control">    
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2">Tag Line</label>
                        <div class="col-md-10">
                            <input type="text" name="tag_line" value="<?php echo ($branding->tag_line) ? $branding->tag_line : "Your home away from home";?>" class="form-control">    
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2">Banner Tagline <br> <small>40 chars. limit</small></label>
                        <div class="col-md-10">
                            <input type="text" name="banner_tagline" value="<?php echo ($branding->banner_tagline) ? $branding->banner_tagline : "Everyone needs a place to call HOME";?>" maxlength="40" class="form-control">   
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2">Logo<br/><small>should be 250x40</small></label>
                        <div class="col-md-10">
                            <?php if( isset($branding->logo) AND !empty($branding->logo)) { ?>
                                <img src="<?php echo AGENT_DASHBOARD_URL ?>assets/upload/logo/<?php echo $branding->logo;?>" width="150"> 
                                <br /><br />
                                <div class="input-group imageupload">
                                    <input type="file" name="userfile[]" class="filestyle" data-buttonBefore="true" data-icon="false">
                                </div>  
                            <?php }else { ?> 
                                <div class="input-group imageupload">
                                    <input type="file" name="userfile[]" class="filestyle" data-buttonBefore="true" data-icon="false">
                                </div>  
                            <?php } ?>
                        </div>
                    </div>
                     <div class="form-group">
                        <label class="col-md-2">Favicon <a href="#" data-toggle="tooltip" title="an icon associated with a URL!"><i class="fa fa-question-circle" aria-hidden="true"></i></a><br/><small>should be 16x16</small></label>
                        <div class="col-md-10">
                            <?php if( isset($branding->favicon) AND !empty($branding->favicon)) { ?>
                                <img src="<?php echo AGENT_DASHBOARD_URL ?>assets/upload/favicon/<?php echo $branding->favicon;?>" width="150">
                                <br /><br />
                                <div class="input-group imageupload">
                                    <input type="file" name="userfile[]" class="filestyle" data-buttonBefore="true" data-icon="false">
                                </div>  

                <?php }else { ?> 
                    <div class="input-group imageupload">
                        <input type="file" name="userfile[]" class="filestyle" data-buttonBefore="true" data-icon="false">
                    </div>  
                <?php } ?> 
                  
            </div>
        </div>
        <hr>
        <h3>Agent Info</h3>
        <div class="form-group">
            <label class="col-md-2">Photo<br/><small>should be 400x400</small></label>
            <div class="col-md-10">
                <?php if( isset($branding->agent_photo) AND !empty($branding->agent_photo)) { ?>
                    <img src="<?php echo AGENT_DASHBOARD_URL ?>assets/upload/photo/<?php echo $branding->agent_photo;?>" class="img-circle" width="150">
                    <br /><br />
                    <div class="input-group imageupload">
                        <input type="file" name="userfile[]" class="filestyle" data-buttonBefore="true" data-icon="false">
                    </div>  
                <?php } else { ?>

                    <?php if (isset($account_info->Images[0]->Uri) AND !empty($account_info->Images[0]->Uri)){?>
                        <img src="<?=$account_info->Images[0]->Uri?>" class="img-circle" width="200"> 
                    <?php } else { ?>
                        <img src="<?= base_url()?>/assets/images/no-profile-img.gif" width="200" alt="No Profile Image">
                    <?php } ?>  
                     <div class="input-group imageupload">
                        <input type="file" name="userfile[]" class="filestyle" data-buttonBefore="true" data-icon="false">
                    </div>    
                <?php } ?>
            </div>
        </div>
         <div class="form-group">
            <label class="col-md-2">First Name</label>
            <div class="col-md-10">
                <input type="text" name="agent_fname" value="<?php echo ($branding->first_name) ? $branding->first_name : $account_info->FirstName;?>" class="form-control">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2">Last Name</label>
            <div class="col-md-10">
                <input type="text" name="agent_lname" value="<?php echo ($branding->last_name) ? $branding->last_name : $account_info->LastName;?>" class="form-control">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2">Phone No.</label>
            <div class="col-md-10">
                <input type="text" name="agent_office_no" value="<?php if(isset($branding->phone) AND !empty($branding->phone)) { ?><?php echo $branding->phone; ?><?php } elseif(isset($account_info->Phones)){foreach($account_info->Phones as $phone){if($phone->Name == "Office"){echo $phone->Number;}}}?> " class="form-control">    
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2">Mobile No.</label>
            <div class="col-md-10">
                <input type="text" name="agent_mobile_no" value="<?php if(isset($branding->mobile) AND !empty($branding->mobile)) { ?><?php echo $branding->mobile; ?><?php } elseif(isset($account_info->Phones)){foreach($account_info->Phones as $phone){if($phone->Name == "Mobile"){echo $phone->Number;}}}?> " class="form-control">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2">City</label>
            <div class="col-md-10">
                <input type="text" name="agent_city" value="<?php if(isset($branding->city) AND !empty($branding->city)) { ?><?php echo $branding->city; ?>
                    <?php } elseif(isset($account_info->Addresses[0]->City) AND !empty($account_info->Addresses[0]->City)) { ?><?php echo $account_info->Addresses[0]->City ?><?php } else { ?><?php echo "Not Specified" ;?><?php } ?> " class="form-control">  
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2">Address</label>
            <div class="col-md-10">
                <input type="text" name="agent_address" value="<?php if(isset($branding->address) AND !empty($branding->address)) { ?><?php echo $branding->address; ?><?php } elseif(isset($account_info->Addresses[0]->Address) AND !empty($account_info->Addresses[0]->Address)) { ?><?php echo $account_info->Addresses[0]->Address ?><?php } else { ?><?php echo "Not Specified" ;?><?php } ?> " class="form-control">  
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2">Zip</label>
            <div class="col-md-10">
                <input type="text" name="agent_zip" value="<?php if(isset($branding->zip) AND !empty($branding->zip)) { ?><?php echo $branding->zip; ?><?php } elseif(isset($account_info->Addresses[0]->PostalCode) AND !empty($account_info->Addresses[0]->PostalCode)) { ?><?php echo $account_info->Addresses[0]->PostalCode ?><?php } else { ?><?php echo "Not Specified" ;?><?php } ?> " class="form-control">    
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2">Email</label>
            <div class="col-md-10">
                <input type="email" name="agent_email" value="<?php if(isset($branding->email) AND !empty($branding->email)) { ?><?php echo $branding->email; ?><?php } elseif(isset($account_info->Emails[0]->Address) AND !empty($account_info->Emails[0]->Address)) { ?><?php echo $account_info->Emails[0]->Address ?><?php } else { ?><?php echo "Not Specified" ;?><?php } ?> " class="form-control">     
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2">About</label>
            <div class="col-md-10">
                <textarea class="form-control summernote" name="about_agent" cols="20" rows="9"><?php echo ($branding->about_agent) ? $branding->about_agent : "";?></textarea> 
            </div>
        </div>



              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-default btn-save btn-submit pull-right" ><i style="display: none;" class="save-loading fa fa-spinner fa-spin"></i> Save</button>
              </div>
            </form>
          </div>
          <!-- /.box -->


                </div> 
                <!-- End of col-md-6 -->
                <div class="col-md-6">
                      <!-- general form elements -->
                      <div class="box box-primary">
                        <div class="box-header with-border">
                          <h3 class="box-title">Slider Photo</h3>
                        </div>
                        <!-- /.box-header -->
                          <div class="box-body padding-0">

                            <div class="slider agent-site-info-panels">
                                <form id="upload" method="POST" action="<?= base_url()?>agent_sites/upload_slider_photos" enctype="multipart/form-data">
                                    <div id="drop" class="imageupload">Drop files anywhere to upload<br>or<br><a>Select Files</a><br><br><br>
                                        <input class="hide-choose-files" type="file" name="userfile" multiple="true" />
                                    </div>
                                    <ul>
                                        <!-- The file uploads will be shown here -->
                                    </ul>
                                    <div class="box-footer">
                                        <button type="submit" class="btn btn-default btn-save btn-submit pull-right" ><i style="display: none;" class="save-loading fa fa-spinner fa-spin"></i> Upload</button>
                                    </div>
                                </form>
                            </div>
                            <div class="slider-photos-preview">
                                <ul>
                                    
                                    <?php
                                        if(isset($slider_photos) AND !empty($slider_photos)){

                                          foreach($slider_photos as $photo){?>
                                             

                                                <li class="img-preview">
                                                    <span>
                                                        <a href="#" data-toggle="modal" data-target="#delete_slider_photo-modal-<?=$photo->id?>" data-id="<?=$photo->id?>">
                                                            <i class="fa fa-trash-o"></i>
                                                        </a>
                                                     </span>
                                                        <img src="<?php echo AGENT_DASHBOARD_URL ?>assets/upload/slider/<?=$photo->photo_url?>" width="200" height="150" alt="Slider Photos">
                                                </li>
                                        
                                        <!-- delete confirmation modal -->
                                            <div id="delete_slider_photo-modal-<?=$photo->id?>" class="modal fade " tabindex="-1" role="dialog" aria-labelledby="delete_slider_photo-modal-<?=$photo->id?>" aria-hidden="false">
                                          <div class="modal-dialog modal-md">
                                            <div class="modal-content">
                                              <div class="modal-body text-center">
                                                  <p class="del-photo">Are you sure you want to delete this photo?</p>
                                                    <img src="<?php echo AGENT_DASHBOARD_URL ?>assets/upload/slider/<?=$photo->photo_url?>" width="300" height="200" alt="Slider Photos">
                                              </div>
                                              <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                                                <a href="<?php echo site_url('agent_sites/delete_slider_photo/');?>/<?=$photo->id?>"><button type="button" class="btn btn-primary">Yes</button></a>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                        <?php }?>
                                    <?php  } ?>
                                </ul>
                            </div>

                          </div>
                          <!-- /.box-body -->


                      </div>
                      <!-- /.box -->
                </div>
            </div>
    </section>
</div>
<?php $this->load->view('session/footer'); ?>
<script>
    $(document).ready(function(){

        $('.summernote').summernote({
            height: 400,

            popover: {
            image: [
                ['imagesize', ['imageSize100', 'imageSize50', 'imageSize25']],
                ['float', ['floatLeft', 'floatRight', 'floatNone']],
                ['custom', ['imageAttributes', 'imageShape']],
                ['remove', ['removeMedia']]
            ],
        },
        
        });

    });
</script>
