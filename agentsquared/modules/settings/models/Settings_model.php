<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Settings_model extends CI_Model{

  public function __construct(){
    parent::__construct();
  }

  /**
  * Get the table name of the field name.
  * @param string $name The name of the field you are trying to get the table name to.
  *
  * @return mixed Returns the table name of the field. Returns false if field does not exist in any tables. Default: false
  */
  private function get_table($name = null){
    switch ($name) {  // always make sure to add new fields for every field from the controller
      case 'facebook_pixel':
      case 'gtm_head':
      case 'gtm_body':
      case 'robots':
      case 'remarketing_tag':
      case 'listtrac_page_view':
      case 'html_sitemap':
      case 'enable_cloudcma':
      case 'cloudcma_api_key':
        return 'seo';
        break;
      case 'global_search_filters':
        return 'settings';
        break;
      case 'global_search_filters_string':
        return 'settings';
        break;
    }
    return false;
  }

  /**
  * Checks if a string is json or not
  * @param string $string (required) The string to check.
  * 
  * @return boolean True if json, False if not
  */
  private function isJSON($string) {
    json_decode($string);
    return (json_last_error() == JSON_ERROR_NONE);
  }

  /**
  * Get value of a settings field
  * @param string $field_name (required) The db column field name.
  * @param string $agent_d (required) The long id of the agent.
  *
  * @return mixed Returns the row of a single query as Object. Default: false.
  */
  public function get($field_name, $agent_id){

    $table = $this->get_table($field_name);

    if( $table !== false && $this->db->table_exists($table) ){

      if( $this->db->field_exists($field_name, $table) ){

        $this->db->select('id, agent_id, '.$field_name);
        $this->db->from($table);
        $this->db->where('agent_id', $agent_id);

        $query = $this->db->get()->row();

        if(!empty($query)){

          if($this->isJSON($query->{$field_name})){
            $query->{$field_name} = json_decode($query->{$field_name});
          }

          return $query;
        }
      }

    }

    return false;

  }
}
