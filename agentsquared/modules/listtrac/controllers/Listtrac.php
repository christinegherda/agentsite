<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Listtrac extends MX_Controller {
	private $settings;
  protected $domain_user;
  
	public function __construct(){
		parent::__construct();
    
    $this->domain_user = domain_to_user();
		$this->load->model('agent_sites/Agent_sites_model', 'domain');
		$this->load->model('listtrac/Listtrac_model', 'listtrac');
    $this->settings = $this->listtrac->get($this->domain_user->agent_id);
	}
  
  public function actions(){
    return array('script', 'pageview');
  }
  
  public function script($args = array()){
    if(isset($this->settings->account_id)){
      if($this->settings->account_id != ''){
        $script = '<!-- Listtrac -->'."\r\n";
        // $script .= '<script src="//code.jquery.com/jquery-1.8.2.min.js" type="text/javascript"></script>'."\r\n";
        $script .= '<script src="//code.listtrac.com/monitor.ashx?acct='.$this->settings->account_id.'&nonjq=1" type="text/javascript"></script>'."\r\n";
        $script .= '<!-- end of Listtrac -->'."\r\n";
        return $script;
      }
    }
  }
  
  public function pageview($args = array()){
    if(isset($this->settings->account_id)){
      if($this->settings->pageview){
        $script = '';
        $listing_id = (isset($args['listing_id']))? $args['listing_id']: '';
        $zipcode = (isset($args['zipcode']))? $args['zipcode']: NULL;
        $agent_id = (isset($args['agent_id']))? $args['agent_id']: $this->settings->agent_id;
        $name = (isset($args['name']))? $args['name']: 'Page View of property '.$args['listing_id'];

        if($this->input->get('debug') == true){
          $script .= "<!-- $listing_id | $zipcode | $agent_id -->\r\n";
        }

        if($listing_id != '' && $agent_id != ''){
          $script .= "<!-- Listtrac page view event script -->\r\n";
          $script .= '<script type="text/javascript">'."\r\n";
          $script .= 'if (typeof(_LT) != \'undefined\' && _LT != null ) {';
          $script .= '_LT._trackEvent(_eventType.view, \''.$listing_id.'\', \''.$zipcode.'\', \''.$agent_id.'\',\'NULL\',\''.$name.'\');';
          $script .= '}';
          $script .= '</script>'."\r\n";
          $script .= "<!-- end of Listtrac page view event script -->\r\n";
          return $script;
        }
      }
    }
  }
  
  public function index(){
    $domain = $this->domain->fetch_domain_info();
    $data = array();
    $data['agent_id'] = $this->domain_user->agent_id;
    
    // checks default values
    $settings = $this->listtrac->get($data['agent_id']);
    $columns = $this->listtrac->columns();
    if($settings){
      $data['account_id'] = $settings->account_id;
      foreach($columns as $column){
        $column['value'] = $settings->{$column['key']};
        $data['settings'][$column['key']] = $column;
      }
    }else{
      $data['account_id'] = '';
      foreach($columns as $column){
        $data['settings'][$column['key']] = $column;
      }
    }
    
    $this->form_validation->set_rules('l_account_id', 'Listtrac Account ID', 'required');
    
    if($this->form_validation->run() !== FALSE){
      if(isset($_POST)){
        $_data = array();
        $_data['agent_id'] = $data['agent_id'];
        
        foreach($this->listtrac->fields() as $field){
          if($field['key'] == 'account_id'){
            $_data[$field['key']] = $this->input->post('l_'.$field['key']);
          }else{
            $_val = $this->input->post('l_'.$field['key']);
            $_data[$field['key']] = ($_val == 'on')? 1: 0;
          }
        }
        
        if($this->listtrac->save($_data)){
          $this->session->set_flashdata('msg','<div class="alert alert-success">Update Listtrac settings successful!</div>');
          redirect('/listtrac');
        }else{
          $this->session->set_flashdata('msg','<div class="alert alert-danger">There was a problem saving your data. Please contact support.</div>');
          redirect('/listtrac');
        }
      }
    }
    $this->load->view('listtrac/listtrac_view', $data);
  }
  
}

?>