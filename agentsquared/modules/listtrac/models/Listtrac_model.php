<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Listtrac_model extends CI_Model{ 

  function __construct() {
    parent:: __construct();
    
    $this->load->helper('form');
    //$this->load->db();
    $this->load->dbforge();

    $this->db_slave =  $this->load->database('db_slave', TRUE);
  }
  
  public function get($agent_id = '') {

    if($agent_id == ''){
			return false;
		}

    if($this->db_slave->table_exists('users_listtrac')) {
      $this->db_slave->where('users_listtrac.agent_id', $agent_id);
      $query = $this->db_slave->get('users_listtrac');
      if( $query->num_rows() > 0 ){
        return $query->row();
      }
    }    
		return false;
  }
  
}

