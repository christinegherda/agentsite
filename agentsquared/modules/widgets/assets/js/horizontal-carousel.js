/*
  Widget: nearby-listing-1
*/
$(document).ready(function(){
  $(".owl-carousel").owlCarousel({
      margin:10,
      nav : true,
	    navText: ["<i class='fa fa-chevron-left'></i>","<i class='fa fa-chevron-right'></i>"],
      responsive:{
          0:{
              items:1
          },
          600:{
              items:3
          },
          1000:{
              items:4
          }
      }
  });
});