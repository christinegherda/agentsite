/*
  Widget: photo-gallery
*/
$(document).ready(function() {
    // Horizontal slider
    $('#horizontal').lightSlider({
        gallery:true,
        item:1,
        thumbItem:5,
        slideMargin: 0,
        speed:500,
        auto:true,
        loop:true,
        onSliderLoad: function() {
            $('#horizontal').removeClass('cS-hidden');
        }  
    });

    // Vertical slider
    $('#vertical').lightSlider({
      gallery:true,
      item:1,
      vertical:true,
      verticalHeight:400,
      vThumbWidth:90,
      thumbItem:5,
      thumbMargin:4,
      slideMargin:0 
    });
});