/*
  Widget: basic-search
*/
$(document).ready(function(){
	var searchfield = $(".search-field .form-control.home-search");
  	searchfield.on("focus", function() {
	     $(".help-info").slideDown();
  	});
	searchfield.on("keypress", function() {
		var a = $(this).val();
		if (0 == a) {
			$(".help-info").stop().show();
		} else {
			$(".help-info").stop().slideUp();
		}
	});
	searchfield.on("mouseleave", function() {
        $(".help-info").stop().slideUp()
    });
});