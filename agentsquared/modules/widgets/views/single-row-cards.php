<?php
  // this widget is able to use the passed $data variable from the controller and you can absolutely overwrite some of the key pair values.
?>
<!-- widget-single-row-cards -->
<div class="widget widget-single-row-cards">
  <div class="property-other-listings property-detail-sections">
    <h2 class="widget-title"><?php echo $title;?></h2>
    <div id="nearby-listings">
      <div class="nearby-loader">
        <!-- <div class="preloader"></div> -->
      </div>
    </div>
  </div>
  <!-- cards -->
  <div class="cards">
    <?php if(!empty($listings) || $listings){?>
    <?php foreach($listings as $index => $listing){
        if($index >= 4){
          break;
        }
        // prepare data
        $address_first_line = (isset($listing->StandardFields->UnparsedFirstLineAddress) && $listing->StandardFields->UnparsedFirstLineAddress !== '********')? $listing->StandardFields->UnparsedFirstLineAddress : 'Firstline Address Unavailable';
        $address_city_state = 'City & State Unavailable';

        $mystring = $listing->StandardFields->City;
        $postalcode = $listing->StandardFields->PostalCode;
        $findme   = '*';
        $pos = strpos($mystring, $findme);
        $pos_code = strpos($postalcode, $findme);

        if($pos === false){
          if($pos_code === false){
             $address_city_state = $listing->StandardFields->City . ", " . $listing->StandardFields->StateOrProvince . " " . $listing->StandardFields->PostalCode;
          }else{
            $address_city_state = $listing->StandardFields->City . ", " . $listing->StandardFields->StateOrProvince;
          }
        }else{
          if($pos_code === false){
            $address_city_state = $listing->StandardFields->PostalCity . ", " . $listing->StandardFields->StateOrProvince . " " . $listing->StandardFields->PostalCode;
          }else{
            $address_city_state = $listing->StandardFields->PostalCity . ", " . $listing->StandardFields->StateOrProvince;     
          }
        }

        $property_class = (isset($listing->StandardFields->PropertyClass))? $listing->StandardFields->PropertyClass : 'Unavailable';

        // list price
        $list_price = 'N/A';
        if(isset($type) && $type === 'sold'){
          $list_price = 'Sold';
        }else{
          if(isset($listing->StandardFields->ListPrice) && $listing->StandardFields->ListPrice !== '********'){
            $list_price = '$ ' . number_format($listing->StandardFields->ListPrice, 2, '.', ',');
          }else if(isset($listing->StandardFields->CurrentPrice) && $listing->StandardFields->CurrentPrice !== '********'){
            $list_price = '$ ' . number_format($listing->StandardFields->CurrentPrice, 2, '.', ',');
          }
        }

        // beds
        $beds = 'N/A';
        if(isset($listing->StandardFields->BedsTotal) && !empty($listing->StandardFields->BedsTotal)){
          if(($listing->StandardFields->BedsTotal != "********")){
            $beds = $listing->StandardFields->BedsTotal;
          }
        }

        // baths
        $baths = 'N/A';
        if($account_info->Mls != "MLS BCS"){
          if(isset($sold->StandardFields->BathsTotal) && !empty($sold->StandardFields->BathsTotal)){
            if(($sold->StandardFields->BathsTotal != "********")){
              $baths = $sold->StandardFields->BathsTotal;
            }
          }
        }else{
          if(isset($sold->StandardFields->BathsFull) && !empty($sold->StandardFields->BathsFull)){
            if(($sold->StandardFields->BathsFull != "********")){
              $baths = $sold->StandardFields->BathsFull;
            }
          }
        }

        // area
        if(!empty($listing->StandardFields->BuildingAreaTotal) && ($listing->StandardFields->BuildingAreaTotal != "0")   && is_numeric($listing->StandardFields->BuildingAreaTotal)) {
          $area = number_format($listing->StandardFields->BuildingAreaTotal);
        }else if(!empty($listing->StandardFields->LotSizeArea) && ($listing->StandardFields->LotSizeArea != "0")   && is_numeric($listing->StandardFields->LotSizeArea)){
          if(!empty($listing->StandardFields->LotSizeUnits) && ($listing->StandardFields->LotSizeUnits) === "Acres"){
            $area = number_format($listing->StandardFields->LotSizeArea, 2, '.', ',' );
          }else{
            $area = number_format($listing->StandardFields->LotSizeArea);
          }
        }else if(!empty($listing->StandardFields->LotSizeSquareFeet) && ($listing->StandardFields->LotSizeSquareFeet != "0")   && is_numeric($listing->StandardFields->LotSizeSquareFeet)) {
          $area = number_format($listing->StandardFields->LotSizeSquareFeet);
        }else if(!empty($listing->StandardFields->LotSizeAcres) && ($listing->StandardFields->LotSizeAcres != "0")   && is_numeric($listing->StandardFields->LotSizeAcres)) {
          $area = number_format($listing->StandardFields->LotSizeAcres,2 ,'.',',');
        }else if(!empty($listing->StandardFields->LotSizeDimensions) && ($listing->StandardFields->LotSizeDimensions != "0")   && ($listing->StandardFields->LotSizeDimensions != "********")) {
          $area = $listing->StandardFields->LotSizeDimensions;
        }else{
          $area = 'N/A';
        }

        $img = '/assets/images/image-not-available.jpg';
        if(isset($listing->StandardFields->Photos) && !empty($listing->StandardFields->Photos)){
          $img = $listing->StandardFields->Photos[0]->Uri300;
        }else if(isset($listing->Photos) && !empty($listing->Photos)){
          $img = $listing->Photos->Uri300;
        }
        
        // url
        $url = '/other-property-details/'.$listing->StandardFields->ListingKey;
        if(isset($seo_url) && $seo_url){
          $address = url_title($listing->StandardFields->UnparsedFirstLineAddress . ' ' . $listing->StandardFields->PostalCode);
          if(isset($type) && ($type === 'active' || $type === 'sold')){
            $url = '/property-details/'.$address.'/'.$listing->StandardFields->ListingKey;
          }else{
            $url = '/other-property-details/'.$address.'/'.$listing->StandardFields->ListingKey;
          }
        }else{
          if(isset($type) && ($type === 'active' || $type === 'sold')){
            $url = '/property-details/'.$listing->StandardFields->ListingKey;
          }
        }
      ?>
      <div class="col-md-3 col-sm-6">
        <div class="item property-item ">
          <a href="<?php echo $url;?>" class="item-title">
            <h2><?php echo $address_first_line;?></h2>
            <p><?php echo $address_city_state;?></p>
            <div class="prop-type-price">
              <div class="col-md-5  col-sm-6 col-xs-6 padding-0">
                <div class="property-type">
                  <?php echo $property_class;?>
                </div>
              </div>
              <div class="col-md-7 col-sm-6 col-xs-6 ">
                <div class="property-price">
                  <?php echo $list_price;?>
                </div>
              </div>
            </div>
          </a>
          <div class="item-img">
            <a href="<?php echo $url;?>">
              <img src="<?php echo $img;?>" alt="<?php echo $address_first_line . ' Photo';?>" title="<?php echo $address_first_line . ' Photo';?>" />
            </a>
            <ul class="item-dets">
              <li>
                <p>
                  <span><?php echo $beds;?></span>
                  <br>
                  Beds
                </p>
              </li>
              <li>
                <p>
                  <span><?php echo $baths;?></span>
                  <br>
                  Baths
                </p>
              </li>
              <li>
                <p>
                  <span><?php echo $area;?></span>
                  <br>
                  Sqft
                </p>
              </li>
            </ul>
          </div>
        </div>
      </div>
    <?php }} ?>
  </div>
  <!-- ./ cards -->
  <?php if(isset($url_first_segment) && $url_first_segment){ ?>
    <div class="text-center">
      <a href="<?php echo base_url() . $url_first_segment; ?>" class="widget-button" >View All</a>
    </div>
  <?php } ?>
</div>
<!-- ./ widget-single-row-cards -->