<div class="search-field">
	<form action="<?php echo base_url(); ?>search-results" method="get" class="filter-property" id="basic_search_form">
		<div class="row">
			<div class="col-md-10">
				<input type="text" class="form-control home-search" autocomplete="off" name="Search" placeholder="Address, City, Zip Code" maxlength="100" value="<?=$this->input->get('Search');?>" required>
			</div>
			<div class="col-md-2">
				<a href="#" class="btn btn-blue submit-button">Search</a>
			</div>
		</div>
	</form>
	<div class="help-info">
	    <div class="arrow-up"></div>
	    <p>Examples:</p>
	    <ul>

	    	
	    	<?php if (isset($listings)): ?>
	    		<?php if ($account_info->MlsId != "MLS BCS"): ?>
		
	                <li>
	                    <div class="col-md-4 col-sm-4 col-xs-5">
	                        <p>City</p>
	                    </div>
	                    <div class="col-md-8 col-sm-8 col-xs-7">
	                        <p>
	                            <?php
	                                echo isset($listings[0]->StandardFields->City) ? $listings[0]->StandardFields->City . " ": "" ;
	                            ?>
	                        </p>
	                    </div>
	                </li>
	                <li>
	                    <div class="col-md-4 col-sm-4 col-xs-5">
	                        <p>Address</p>
	                    </div>
	                    <div class="col-md-8 col-sm-8 col-xs-7">
	                        <p><?php echo isset($listings[0]->StandardFields->UnparsedAddress) ? $listings[0]->StandardFields->UnparsedAddress : "" ; ?></p>
	                    </div>
	                </li>


	    		<?php else: ?>
	    		
	                <li>
	                    <div class="col-md-4 col-sm-4 col-xs-5">
	                        <p>City, State</p>
	                    </div>
	                    <div class="col-md-8 col-sm-8 col-xs-7">
	                        <p>
	                            <?php
	                                echo isset($listings[0]->StandardFields->City) ? $listings[0]->StandardFields->City . ", ": "" ;
	                                echo isset($listings[0]->StandardFields->StateOrProvince) ? $listings[0]->StandardFields->StateOrProvince : "" ;
	                            ?>
	                        </p>
	                    </div>
	                </li>
	                <li>
	                    <div class="col-md-4 col-sm-4 col-xs-5">
	                        <p>Zip Code</p>
	                    </div>
	                    <div class="col-md-8 col-sm-8 col-xs-7">
	                        <p><?php echo isset($listings[0]->StandardFields->PostalCode) ? $listings[0]->StandardFields->PostalCode : "" ; ?></p>
	                    </div>
	                </li>
	                <li>
	                    <div class="col-md-4 col-sm-4 col-xs-5">
	                        <p>Address</p>
	                    </div>
	                    <div class="col-md-8 col-sm-8 col-xs-7">
	                        <p><?php echo isset($listings[0]->StandardFields->UnparsedAddress) ? $listings[0]->StandardFields->UnparsedAddress : "" ; ?></p>
	                    </div>
	                </li>
	                <li>
	                    <div class="col-md-4 col-sm-4 col-xs-5">
	                        <p>County</p>
	                    </div>
	                    <div class="col-md-8 col-sm-8 col-xs-7">
	                        <p><?php echo isset($listings[0]->StandardFields->CountyOrParish) ? $listings[0]->StandardFields->CountyOrParish : "" ; ?></p>
	                    </div>
	                </li>



	    		<?php endif ?>


	                <li>
	                    <div class="col-md-4 col-sm-4 col-xs-5">
	                        <p>Street Address</p>
	                    </div>
	                    <div class="col-md-8 col-sm-8 col-xs-7">
	                        <p><?php echo isset($listings[0]->StandardFields->UnparsedFirstLineAddress) ? $listings[0]->StandardFields->UnparsedFirstLineAddress : "" ; ?></p>
	                    </div>
	                </li>
	    	<?php endif ?>
	    </ul>
	</div>
</div>