<?php
  // convert data to php assoc array
  $data = json_decode($data);

  shuffle($slider_photos);
  $photos = $slider_photos;

  if( !function_exists('randomness') ){
    function randomness($quantity, $iterate){
      $numbers = range(0, $quantity);
      shuffle($numbers);
      return ($iterate >= $quantity)? $iterate - (floor($iterate / $quantity) * $quantity) : $numbers[$iterate] ;
    }
  }
?>
<!-- widget-horizontal-carousel -->
<div class="widget widget-savedsearches-carousel">
  <div class="property-other-listings property-detail-sections">
    <h2 class="widget-title"><?php echo $title;?></h2>
    <div id="widget-horizontal-carousel-loader"></div>
  </div>
  <!-- owl-carousel -->
  <div class="owl-carousel">
      <?php foreach($data as $id => $value){?>
        <div class="item property-item" style="border: none; background-color: transparent;">
          <a href="<?php echo '/saved_searches/'.$id;?>" class="item-title" style="background-color: transparent;">
            <!-- save search image -->
            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="250px" height="300px" viewBox="0 0 250 300" enable-background="new 0 0 250 300" xml:space="preserve">
              <defs>
                <?php foreach($photos as $index => $photo){ ?>
                  <pattern id="image<?php echo $index; ?>" x="0" y="0" height="100%" width="100%" viewBox="0 0 300 200">
                    <image x="0" y="0" width="300" height="200" xlink:href="<?php echo (!empty($photo->photo_url))? AGENT_DASHBOARD_URL . 'assets/upload/slider/' . $photo->photo_url : '';?>"></image>
                  </pattern>
                <?php } ?>
              </defs>
              <g>
                <g>
                  <rect x="46.197" y="78.303" transform="matrix(-0.1112 0.9938 -0.9938 -0.1112 330.7446 24.6187)" fill="#FFFFFF" width="216.333" height="163.812"/>
                  <path fill="#939598" d="M86.759,45.815L245.58,63.587l-23.613,211.016l-158.82-17.771L86.759,45.815 M83.229,41.396L58.727,260.362l166.77,18.66L250,60.057L83.229,41.396L83.229,41.396z"/>
                </g>
                <rect x="81.37" y="106.42" transform="matrix(0.9938 0.1112 -0.1112 0.9938 18.539 -16.2117)" fill="url(#image<?php echo randomness(count($photos) - 1, 0); ?>)" width="146.461" height="103.336"/>
                <g>
                  <ellipse transform="matrix(0.8025 -0.5967 0.5967 0.8025 -57.0286 117.3199)" fill="#FFFFFF" stroke="#58595B" stroke-width="7" stroke-miterlimit="10" cx="148.685" cy="144.795" rx="28.182" ry="27.539"/>
                  <line fill="none" stroke="#58595B" stroke-width="10" stroke-linecap="round" stroke-miterlimit="10" x1="166.627" y1="168.924" x2="185.945" y2="194.903"/>
                </g>
                <path fill="#E6E7E8" d="M169.748,80.329c-0.158,1.388-1.408,2.386-2.794,2.231l-67.753-7.583c-1.387-0.155-2.386-1.404-2.231-2.792l1.1-9.829c0.156-1.387,1.406-2.386,2.794-2.231l67.751,7.581c1.388,0.156,2.387,1.407,2.23,2.794L169.748,80.329z"/>
                <path fill="#E6E7E8" d="M226.529,86.683c-0.155,1.387-1.406,2.386-2.794,2.23l-45.05-5.041c-1.386-0.155-2.384-1.406-2.229-2.792l1.099-9.828c0.156-1.387,1.404-2.387,2.793-2.231l45.049,5.041c1.388,0.155,2.386,1.406,2.232,2.793L226.529,86.683z"/>
                <path fill="#E6E7E8" d="M224.409,105.643c-0.155,1.386-1.406,2.386-2.795,2.23l-67.753-7.581c-1.387-0.155-2.386-1.405-2.23-2.794l1.101-9.827c0.155-1.388,1.407-2.386,2.794-2.231l67.752,7.581c1.387,0.157,2.386,1.407,2.23,2.794L224.409,105.643z"/>
                <path fill="#E6E7E8" d="M152.666,232.959c-0.153,1.391-1.405,2.386-2.791,2.231l-67.753-7.583c-1.387-0.152-2.387-1.403-2.23-2.792l1.1-9.826c0.155-1.387,1.406-2.387,2.793-2.232l67.753,7.582c1.387,0.154,2.386,1.404,2.23,2.793L152.666,232.959z"/>
                <path fill="#E6E7E8" d="M209.45,239.313c-0.156,1.389-1.055,2.428-2.009,2.32l-46.616-5.218c-0.955-0.107-1.604-1.319-1.446-2.706l1.099-9.826c0.155-1.388,1.054-2.426,2.011-2.321l46.614,5.218c0.955,0.106,1.604,1.318,1.446,2.705L209.45,239.313z"/>
              </g>
              <g>
                <g>
                  <rect x="66.24" y="32.819" transform="matrix(-1 0.0028 -0.0028 -1 296.6857 281.5554)" fill="#FFFFFF" width="163.812" height="216.332"/>
                  <path fill="#939598" d="M227.755,34.596l0.594,212.332l-159.812,0.448L67.945,35.042L227.755,34.596 M231.744,30.585L63.934,31.053l0.615,220.333l167.811-0.471L231.744,30.585L231.744,30.585z"/>
                </g>
                <rect x="74.91" y="87.184" transform="matrix(1 -0.0028 0.0028 1 -0.3864 0.4134)" fill="url(#image<?php echo randomness(count($photos) - 1, 0); ?>)" width="146.46" height="103.335"/>
                <g>
                  <ellipse transform="matrix(0.7292 -0.6843 0.6843 0.7292 -48.3261 130.5099)" fill="#FFFFFF" stroke="#58595B" stroke-width="7" stroke-miterlimit="10" cx="140.749" cy="126.32" rx="28.182" ry="27.542"/>
                  <line fill="none" stroke="#58595B" stroke-width="10" stroke-linecap="round" stroke-miterlimit="10" x1="161.323" y1="148.245" x2="183.477" y2="171.855"/>
                </g>
                <path fill="#E6E7E8" d="M154.326,59.872c0.002,1.396-1.126,2.531-2.521,2.535L83.63,62.598c-1.396,0.003-2.532-1.125-2.535-2.521l-0.027-9.89c-0.005-1.396,1.124-2.531,2.52-2.534l68.174-0.191c1.397-0.004,2.532,1.125,2.537,2.52L154.326,59.872z"/>
                <path fill="#E6E7E8" d="M211.462,59.712c0.004,1.397-1.126,2.531-2.521,2.536l-45.33,0.126c-1.397,0.004-2.53-1.124-2.535-2.521l-0.026-9.889c-0.005-1.396,1.123-2.531,2.521-2.535l45.33-0.126c1.396-0.004,2.529,1.125,2.534,2.521L211.462,59.712z"/>
                <path fill="#E6E7E8" d="M211.519,78.791c0.001,1.396-1.129,2.531-2.523,2.535l-68.175,0.191c-1.396,0.003-2.531-1.125-2.534-2.521l-0.027-9.89c-0.004-1.395,1.125-2.531,2.52-2.535l68.176-0.19c1.396-0.004,2.53,1.125,2.535,2.52L211.519,78.791z"/>
                <path fill="#E6E7E8" d="M154.753,213.456c0.005,1.396-1.123,2.531-2.52,2.535l-68.174,0.189c-1.397,0.004-2.531-1.125-2.536-2.521l-0.027-9.888c-0.004-1.397,1.125-2.53,2.521-2.537l68.174-0.189c1.396-0.002,2.532,1.126,2.534,2.522L154.753,213.456z"/>
                <path fill="#E6E7E8" d="M211.891,213.295c0.005,1.397-0.772,2.53-1.73,2.532l-46.907,0.134c-0.963,0.002-1.742-1.129-1.747-2.525l-0.027-9.889c-0.005-1.396,0.771-2.528,1.732-2.532l46.907-0.131c0.96-0.002,1.741,1.129,1.745,2.523L211.891,213.295z"/>
              </g>
              <g>
                <g>
                  <rect x="45.623" y="33.774" transform="matrix(-0.9933 0.1154 -0.1154 -0.9933 270.5878 268.2152)" fill="#FFFFFF" width="163.811" height="216.335"/>
                  <path fill="#939598" d="M194.646,27.261l24.508,210.916L60.411,256.62L35.904,45.707L194.646,27.261 M198.158,22.826L31.469,42.195l25.43,218.86l166.69-19.366L198.158,22.826L198.158,22.826z"/>
                </g>
                <rect x="54.051" y="88.152" transform="matrix(0.9933 -0.1154 0.1154 0.9933 -15.288 15.6259)" fill="url(#image<?php echo randomness(count($photos) - 1, 0); ?>)" width="146.462" height="103.336"/>
                <g>
                  <path fill="#FFFFFF" stroke="#58595B" stroke-width="7" stroke-miterlimit="10" d="M136.771,106.725c11.592,9.849,12.819,27.447,2.74,39.31c-10.076,11.861-27.644,13.492-39.236,3.641c-11.59-9.846-12.817-27.446-2.738-39.307C107.614,98.507,125.182,96.875,136.771,106.725z"/>
                  <line fill="none" stroke="#58595B" stroke-width="10" stroke-linecap="round" stroke-miterlimit="10" x1="141.44" y1="147.67" x2="166.11" y2="168.634"/>
                </g>
                <path fill="#E6E7E8" d="M124.532,60.647c0.159,1.387-0.833,2.641-2.219,2.803l-67.72,7.868c-1.386,0.161-2.643-0.833-2.803-2.219l-1.141-9.823c-0.162-1.387,0.832-2.642,2.219-2.803l67.719-7.869c1.387-0.162,2.642,0.833,2.803,2.218L124.532,60.647z"/>
                <path fill="#E6E7E8" d="M181.286,54.053c0.162,1.386-0.832,2.641-2.22,2.803l-45.027,5.231c-1.387,0.161-2.642-0.833-2.802-2.219l-1.14-9.823c-0.164-1.387,0.831-2.643,2.217-2.803l45.026-5.232c1.388-0.161,2.643,0.832,2.805,2.22L181.286,54.053z"/>
                <path fill="#E6E7E8" d="M183.489,73.004c0.161,1.386-0.833,2.642-2.22,2.803l-67.72,7.868c-1.387,0.16-2.641-0.833-2.802-2.22l-1.142-9.823c-0.16-1.386,0.832-2.641,2.219-2.802l67.721-7.869c1.384-0.161,2.643,0.832,2.802,2.218L183.489,73.004z"/>
                <path fill="#E6E7E8" d="M142.256,213.205c0.163,1.385-0.831,2.641-2.218,2.805l-67.72,7.868c-1.387,0.16-2.641-0.836-2.803-2.221l-1.142-9.824c-0.161-1.385,0.833-2.641,2.22-2.803l67.718-7.867c1.388-0.163,2.642,0.833,2.806,2.22L142.256,213.205z"/>
                <path fill="#E6E7E8" d="M199.012,206.609c0.162,1.386-0.481,2.604-1.435,2.711l-46.594,5.417c-0.955,0.109-1.858-0.923-2.02-2.312l-1.141-9.823c-0.163-1.384,0.48-2.6,1.436-2.713l46.593-5.414c0.955-0.109,1.857,0.925,2.02,2.312L199.012,206.609z"/>
              </g>
              <g>
                <g>
                  <rect x="25.594" y="38.272" transform="matrix(-0.9757 0.2191 -0.2191 -0.9757 244.4758 265.762)" fill="#FFFFFF" width="163.812" height="216.333"/>
                  <path fill="#939598" d="M162.198,25.343l46.529,207.172L52.802,267.536L6.271,60.363L162.198,25.343 M165.225,20.563L1.492,57.336l48.284,214.979l163.731-36.774L165.225,20.563L165.225,20.563z"/>
                </g>
                <rect x="33.801" y="92.689" transform="matrix(0.9757 -0.2191 0.2191 0.9757 -29.032 26.963)" fill="url(#image<?php echo randomness(count($photos) - 1, 0); ?>)" width="146.461" height="103.334"/>
                <g>
                  <ellipse transform="matrix(0.5639 -0.8259 0.8259 0.5639 -68.0861 138.5098)" fill="#FFFFFF" stroke="#58595B" stroke-width="7" stroke-miterlimit="10" cx="97.103" cy="133.721" rx="28.182" ry="27.541"/>
                  <line fill="none" stroke="#58595B" stroke-width="10" stroke-linecap="round" stroke-miterlimit="10" x1="121.935" y1="150.674" x2="148.673" y2="168.932"/>
                </g>
                <path fill="#E6E7E8" d="M95.978,65.911c0.307,1.361-0.55,2.714-1.912,3.02l-66.518,14.94c-1.362,0.305-2.715-0.55-3.02-1.912L22.36,72.31c-0.307-1.363,0.55-2.715,1.913-3.021L90.792,54.35c1.361-0.306,2.713,0.55,3.019,1.912L95.978,65.911z"/>
                <path fill="#E6E7E8" d="M151.727,53.389c0.305,1.363-0.551,2.714-1.913,3.021l-44.228,9.934c-1.363,0.306-2.715-0.55-3.021-1.912l-2.168-9.649c-0.305-1.361,0.551-2.715,1.913-3.021l44.227-9.933c1.363-0.307,2.716,0.55,3.022,1.912L151.727,53.389z"/>
                <path fill="#E6E7E8" d="M155.909,72.003c0.305,1.362-0.553,2.714-1.913,3.021l-66.519,14.94c-1.363,0.305-2.714-0.551-3.021-1.913l-2.167-9.648c-0.305-1.363,0.551-2.715,1.914-3.02l66.518-14.94c1.362-0.306,2.715,0.55,3.021,1.912L155.909,72.003z"/>
                <path fill="#E6E7E8" d="M129.636,215.761c0.304,1.362-0.551,2.715-1.914,3.02l-66.518,14.942c-1.363,0.305-2.714-0.552-3.021-1.914l-2.167-9.647c-0.306-1.362,0.551-2.717,1.914-3.021l66.517-14.942c1.362-0.305,2.714,0.551,3.021,1.915L129.636,215.761z"/>
                <path fill="#E6E7E8" d="M185.382,203.24c0.307,1.362-0.204,2.639-1.144,2.846l-45.765,10.281c-0.938,0.21-1.946-0.725-2.252-2.085l-2.168-9.649c-0.306-1.362,0.207-2.637,1.144-2.847l45.767-10.278c0.936-0.213,1.946,0.721,2.253,2.085L185.382,203.24z"/>
              </g>
            </svg>
            <!-- ./ save search image -->
            <h2 style="font-weight: normal;color: #712906;font-size: 21px;"><?php echo $value;?></h2>
          </a>
        </div>
      <?php }?>
  </div>
  <!-- ./ owl-carousel -->
  <?php if(isset($url_first_segment) && $url_first_segment){ ?>
    <div class="text-center">
      <a href="<?php echo base_url() . $url_first_segment; ?>" class="widget-button" >View All</a>
    </div>
  <?php } ?>
</div>
<!-- ./ widget-horizontal-carousel -->
