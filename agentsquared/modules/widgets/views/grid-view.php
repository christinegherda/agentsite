<?php // prepare data

/*
  REQUIRED Variables:
  - $current_page
  - $listing_base_url
  - $total_pages
  - $page_size
  - $total_count
  - $listings
  - $seo_url
  */

  if($current_page > 1){ // start of listing view logic
    $start = ($current_page - 1) * $limit + 1;
  }else{
    $start = 1;
  }

  if( ($page_size * $current_page) > $total_count){
    $end = $total_count;
  }else{
    $end = $page_size * $current_page;
  }

?>
<!-- widget-list-view -->
<div class="widget widget-grid-view">
  <?php if(isset($title) && $title != ''){ ?>
    <h2 class="title-red text-center" style="margin-bottom: 40px;"><?php echo $title;?></h2>
  <?php } ?>
  <div class="widget-header grid-view-controls">
    <?php if($total_pages > 1){ // if there are more listings than the limit ?>
      <div class="col-md-4"><strong><?php echo $start; ?> - <?php echo $end; ?></strong> of <strong><?php echo $total_count; ?></strong> Properties Found.</div>
      <div class="col-md-8 text-right">
        <div class="select">
          Sort By:
          <select class="sort-by">
            <option>Highest Price</option>
            <option>Lowest Price</option>
            <option>Newest</option>
            <option>Lowest</option>
          </select>
        </div>
        <?php
          $this->load->view(THEME_WIDGETS_VIEW . 'common/paginator', array(
            'current_page' => $current_page,
            'listing_base_url' => $listing_base_url,
            'total_pages' => $total_pages
          ));
        ?>
      </div>
    <?php }else{ // if there are less listings than the limit ?>
      <div class="col-md-6"><strong><?php echo $start; ?> - <?php echo $end; ?></strong> of <strong><?php echo $total_count; ?></strong> Properties Found.</div>
      <div class="col-md-6 text-right">Sort By:
        <select class="sort-by">
          <option>Highest Price</option>
          <option>Lowest Price</option>
          <option>Newest</option>
          <option>Lowest</option>
        </select>
      </div>
    <?php } ?>
  </div>
  <!-- widget-main -->
  <div class="widget-main">
    <ul class="listings-result">
      <?php if(isset($listings['data'])){ ?>
        <?php foreach($listings['data'] as $index => $listing){ ?>
          <li>
            <?php // Prepare data to be passed on to the card strip
              $data = $listing['StandardFields'];
              $data['Photos'] = isset($listing['Photos'])? $listing['Photos'] : $listing['StandardFields']['Photos'][0];

              $this->load->view(THEME_WIDGETS_VIEW . 'common/card-grid', array(
                'listing' => $data,
                'seo_url' => isset($seo_url)? $seo_url : false
              ));
            ?>
          </li>
        <?php } ?>
      <?php }else if(isset($listings)){ ?>
        <?php foreach($listings as $index => $listing){ ?>
          <li>
            <?php // Prepare data to be passed on to the card strip
              if(is_array($listing)){

                $data = $listing['StandardFields'];
                $data['Photos'] = isset($listing['Photos'])? $listing['Photos'] : $listing['StandardFields']['Photos'][0];

              }else{

                $data = (Array) $listing->StandardFields;
                $data['Photos'] = (Array) $listing->StandardFields->Photos[0];

              }

              $this->load->view(THEME_WIDGETS_VIEW . 'common/card-grid', array(
                'listing' => $data,
                'seo_url' => isset($seo_url)? $seo_url : false
              ));
            ?>
          </li>
        <?php } ?>
      <?php } ?>
    </ul>
    <?php //printa($listings);  // do your thing here. ?>
  </div>
  <!-- ./ widget-main -->
  <div class="widget-footer grid-view-controls">
    <?php if($total_pages > 1){ // if there are more listings than the limit ?>
      <div class="col-md-4"><strong><?php echo $start; ?> - <?php echo $end; ?></strong> of <strong><?php echo $total_count; ?></strong> Properties Found.</div>
      <div class="col-md-8 text-right">
        <div class="select">
          Sort By:
          <select class="sort-by">
            <option>Highest Price</option>
            <option>Lowest Price</option>
            <option>Newest</option>
            <option>Lowest</option>
          </select>
        </div>
        <?php
          $this->load->view(THEME_WIDGETS_VIEW . 'common/paginator', array(
            'current_page' => $current_page,
            'listing_base_url' => $listing_base_url,
            'total_pages' => $total_pages
          ));
        ?>
      </div>
    <?php }else{ // if there are less listings than the limit ?>
      <div class="col-md-6"><strong><?php echo $start; ?> - <?php echo $end; ?></strong> of <strong><?php echo $total_count; ?></strong> Properties Found.</div>
      <div class="col-md-6 text-right">Sort By:
        <select class="sort-by">
          <option>Highest Price</option>
          <option>Lowest Price</option>
          <option>Newest</option>
          <option>Lowest</option>
        </select>
      </div>
    <?php } ?>
  </div>
</div>
<!-- ./ widget-list-view -->
