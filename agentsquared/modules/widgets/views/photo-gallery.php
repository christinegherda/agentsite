<!-- widget-photo-gallery -->
<div class="widget widget-photo-gallery widget-photo-gallery-horizontal common">
    <ul id="horizontal" class="gallery list-unstyled cS-hidden">
        <?php foreach($feed as $index => $photo){
            // prepare data
            $thumb = $photo['UriThumb'];
            $img = $photo['ResourceUri'];
            if(isset($photo[$size])){
                $img = $photo[$size];
            }else{
                $img = $photo['Uri640'];
            }
        ?>
        <li data-thumb="<?php echo $thumb;?>"> 
            <img src="<?php echo $img;?>" alt="<?php echo $photo['Name'];?>" title="<?php echo $photo['Name'];?>"/>
        </li>
        <?php }?>
    </ul>
</div>
<!-- ./ widget-photo-gallery -->