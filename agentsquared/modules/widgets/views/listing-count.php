<!-- widget widget-listing-count -->
<div class="widget widget-listing-count">
  <div class="text-center">
    <?php
      $total = 0;
      foreach($count as $value){
        $total += intval($value);
      }
    ?>
    <h2 class="widget-title">Over <?php echo number_format($total);?> Listings</h2>
  </div>
  <div class="listing-count">
    <?php
      foreach($count as $key => $value){
        if((int) $value > 0){
    ?>
    <div class="count-item">
      <div class="item-count">
        <div class="red-circle">
          <p><?php echo number_format($value); ?></p>
        </div>
        <p class="item-label"><?php echo ucwords($key);?> Listings</p>
      </div>
    </div>
    <?php }}?>
  </div>
</div>
<!-- ./ widget widget-listing-count -->
