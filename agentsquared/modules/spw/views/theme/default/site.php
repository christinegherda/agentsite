<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="">
<!--<![endif]-->

<head itemscope itemtype="http://schema.org/WebPage">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php // prepare data
      $url = base_url().$_SERVER['REQUEST_URI'];
      $name = isset($account['Name'])? $account['Name']: 'AgentSquared';
      $phone = isset($account['Phones'][0]['Number'])? $account['Phones'][0]['Number']: '';
      $address = isset($property_standard_info['data']['UnparsedAddress'])? $property_standard_info['data']['UnparsedAddress']: 'Single Property';
      $listingId = isset($property_standard_info['data']['ListingId'])? $property_standard_info['data']['ListingId']: '';
      $currentPrice = isset($property_standard_info['data']['CurrentPrice'])? $property_standard_info['data']['CurrentPrice']: '';

      $title = $address.' | '.$name.' | '.$phone;
      $description = $address.' - Official Listing by '.$name.' and MLS # '.$listingId.'.';

      if ( isset($photos['data'][0]['StandardFields']['Photos'][0]['UriLarge']) && '' !== $photos['data'][0]['StandardFields']['Photos'][0]['UriLarge'])
      {
      	$image = $photos['data'][0]['StandardFields']['Photos'][0]['UriLarge'];
      }
      elseif ( isset($primary) && '' !== $primary )
	  {
	  	$image = $primary;
	  }
    ?>

    <title itemprop="name"><?php echo $title;?></title>
    <meta itemprop="description" name="description" content="<?php echo $description;?>">
    <link rel="canonical" href="<?php echo $url;?>" />

    <!-- Facebook Meta Tags -->
	<meta property="fb:app_id" content="<?php echo config_item('facebook_app_id'); ?>">
    <meta property="og:url" itemprop="url" content="<?php echo $url;?>" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="<?php echo $title;?>" />
    <meta property="og:description" content="<?php echo $description;?>" />
    <meta property="og:image" itemprop="image" content="<?php echo $image;?>" />
    <meta property="og:image:width" content="640" />
    <meta property="og:image:height" content="442" />

    <link rel="apple-touch-icon" href="apple-touch-icon.png">
    <link rel="stylesheet" href="<?=base_url().'assets/spw/'?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?=base_url().'assets/spw/'?>assets/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?=base_url().'assets/spw/'?>assets/css/main.css">
    <link rel="stylesheet" href="<?=base_url().'assets/spw/'?>assets/css/bootstrap-datepicker3.min.css">
    <link rel="stylesheet" href="<?=base_url().'assets/spw/'?>assets/css/lightslider.css">
    <link rel="stylesheet" href="<?=base_url().'assets/spw/'?>assets/css/lightgallery.css">
    <script src="<?=base_url().'assets/spw/'?>assets/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>

    <script type="application/ld+json">
      [
        {
          "@context": "http://schema.org",
          "@type": "WebPage",
          "url": "<?php echo $url;?>",
          "name": "<?php echo $title;?>",
          "description": "<?php echo $description;?>",
          "image": "<?php echo $image;?>",
          "publisher": {
            "@type": "Person",
            "name": "<?php echo $name;?>",
            "telephone": "<?php echo $phone;?>"
          }
        },{
          "@context": "http://schema.org",
            "@type": "Product",
          "url": "<?php echo $url;?>",
            "name": "<?php echo $address;?>",
          "description": "<?php echo $description;?>",
          "image": "<?php echo $image;?>",
          "offers": {
            "@type": "Offer",
            "price": "<?php echo $currentPrice;?>",
            "priceCurrency": "USD"
          },
          "productID": "<?php echo $listingId;?>"
        },{
          "@context": "http://schema.org",
            "@type": "Place",
          "address": "<?php echo $address;?>"
        }
      ]
    </script>
</head>
<body>
    <!-- Modal Sign_up to Capture Leads -->
    <div class="modal fade legal-modal" id="capture-leads-signup" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content theme-modal">
                <div class="modal-header">
                    <button type="button" class="close capture-leads-login-btn" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">X</span></button>
                </div>
                <div class="modal-body clearfix sign-up-modal">

                    <div class="content-leads-signup">
                        <div class="clearfix">
                            <span class="sign-up-message"></span>
                            <form class="form-signin signupForm" id="spwSignup" action="<?php echo base_url("spw/base/capture_leads_signup"); ?>" method="post">
                                <h2 class="text-center">Sign Up</h2>
                                <input type="hidden" name="property_id" value="<?=$property_standard_info['data']['ListingKey'];?>"/>
                                <input type="hidden" name="property_name" value="<?=$property_standard_info['data']['UnparsedFirstLineAddress'];?>"/>
                                <input type="hidden" name="type" value="spw_signup_form">
                                <input type="hidden" name="property_url" value="<?= (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";?>">

                                <div class="alert alert-success" role="alert" style="display:none;"></div>
                                <div class="alert alert-danger" role="alert" style="display: none;"></div>
                                <span class="display-text"></span>

                                <div class="form-group">
                                    <label for="First Name">First Name</label>
                                    <input type="text" name="fname" class="form-control" maxlength="50" pattern="[a-zA-Z0-9-_.\s]+" title="Allowed characters: A-Z, 0-9, ., -, _" required>
                                </div>
                                <div class="form-group">
                                    <label for="Last Name">Last Name</label>
                                    <input type="text" name="lname" class="form-control" maxlength="50" pattern="[a-zA-Z0-9-_.\s]+" title="Allowed characters: A-Z, 0-9, ., -, _" required>
                                </div>
                                <div class="form-group mb-25px">
                                    <label for="Email">Email</label>
                                    <input type="email" name="email" id="customer-email" class="form-control" required>
                                    <span class="valid-customer-email"></span>
                                </div>
                                <div class="form-group mb-25px">
                                    <label for="Phone Number">Cell / Phone Number</label>
                                    <input type="text" name="phone_signup" id="customer-number" class="form-control phone_number" required>
                                    <span class="valid-customer-number"></span>
                                </div>
                                <div class="form-group mb-25px">
                                    <div class="g-recaptcha" data-sitekey="6Lf9eRsUAAAAAB5lP4Em8vm4L-c1m0gSyAKOTort"></div>
                                </div>
                                <div class="col-md-12 col-sm-12 text-center">
                                    <input class="btn btn-orange signup_sbmt_btn" name="submit" id="signup-submit" type="submit" value="Sign up" />
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
    <div class="scroll-to-top">
        <a href="javascript:void(0);">
            <i class="fa fa-angle-up" aria-hidden="true"></i>
        </a>
    </div>
    <header id="home">
        <nav class="navbar navbar-fixed-top" role="navigation">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">
                    <?php if ($user_info->logo): ?>
                    <img src="<?php echo getenv('AWS_S3_ASSETS') . "uploads/logo/" . $user_info->logo;?>" height="50"/>
                    <?php endif; ?>
                    <?=$property_standard_info['data']['UnparsedFirstLineAddress'];?>
                </a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right main-menu">
                    <li class="active"><a href="#home">Home</a></li>
                    <li><a href="#property-detail-area">Property</a></li>
                    <li><a href="#slider-gallery-area">Gallery</a></li>
                    <li><a href="#property-map">Map</a></li>
                    <li>
                        <div class="visible-xs">
                            <a href="#agent-info-area">Agent Info</a>
                        </div>
                        <div class="triangle"></div>
                        <div class="agent-info">
                            <div class="col-md-7 col-sm-7">
                                <p><?php if ($username): ?>
                                    <?=$username; ?>
                                <?php endif ?></p>
                                <?php if (isset($user_info->phone) && !empty($user_info->phone)): ?>
                                    <p class="phone-number"><b><?php echo $user_info->phone; ?></b></p>
                                <?php elseif (isset($account['Phones'][0]['Number']) && !empty($account['Phones'][0]['Number'])): ?>
                                    <p class="phone-number"><b><?php echo $account['Phones'][0]['Number']; ?></b></p>
                                <?php endif ?>
                                <?php if(isset($account['Office']) && !empty($account['Office']) && ($account['Office'] !== "********")): ?>
                                    <p class="broker-office"><b><?php echo $account['Office']; ?></b></p>
                                <?php endif?>
                                <?php if ($account['Addresses'][0]['Region'] == 'CA'): ?>
                                    <p class="license-number"><b>CAL-BRE#<?=$account['LicenseNumber']; ?></b></p>
                                <?php elseif (isset($user_info->license_number) && !empty($user_info->license_number)): ?>
                                    <p class="license-number"><b><?= $user_info->license_number; ?></b></p>
                                <?php elseif (isset($account['LicenseNumber']) && !empty($account['LicenseNumber'])): ?>
                                    <p class="license-number"><b><?= $account['LicenseNumber']; ?></b></p>
                                <?php endif ?>
                                <a href="#agent-info-area" class="btn-btn-default" >More Agent Info</a>
                            </div>
                            <div class="col-md-5 col-sm-5">
                                <?php if ($account_photo): ?>
                                    <div style="background-image: url('<?php echo getenv('AWS_S3_ASSETS') . "uploads/photo/" . $account_photo; ?>')" class="img-agent-top"></div>
                                <?php elseif(!empty($account['Images'][0])): ?>
                                    <div style="background-image: url('<?=$account['Images'][0]['Uri'];?>')" alt="" class="img-agent-top"></div>
                                <?php else: ?>
                                    <div style="background-image: url('assets/img/agent-header100.png')" alt="" class="img-agent-top"></div>
                                <?php endif ?>
                            </div>
                        </div>
                    </li>
                </ul>

            </div>
        </nav>
    </header>
    <section class="featured-image-area">
        <img src="<?php echo (isset($primary) && !empty($primary)) ? $primary : $photos['data'][0]['StandardFields']['Photos'][0]['UriLarge'] ?>" alt="" class="img-responsive">
        <?php if($property_standard_info['data']['MlsStatus'] == "Sold") {?>
            <div class="sold-closed-property">
                <img src="<?=base_url().'assets/spw/'?>assets/img/sold-banner.png" alt="<?=$property_standard_info['data']['UnparsedFirstLineAddress'];?>">
            </div>
        <?php } elseif($property_standard_info['data']['MlsStatus'] == "Closed") {?>
            <div class="sold-closed-property">
                <img src="<?=base_url().'assets/spw/'?>/ssets/img/closed-banner.png" alt="<?=$property_standard_info['data']['UnparsedFirstLineAddress'];?>">
            </div>
        <?php } elseif($property_standard_info['data']['MlsStatus'] == "Pending") {?>
            <div class="pending-property">
                <img src="<?=base_url().'assets/spw/'?>assets/img/pending-banner.png" alt="<?=$property_standard_info['data']['UnparsedFirstLineAddress'];?>">
            </div>
        <?php }?>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="property-summary">
                        <?php if($property_standard_info['data']['MlsStatus'] == "Sold") {?>
                            <div class="sold-closed-status">
                                <span>This property is sold.</span>
                            </div>
                        <?php } elseif($property_standard_info['data']['MlsStatus'] == "Closed") {?>
                            <div class="sold-closed-status">
                                <span>This property is closed.</span>
                            </div>
                        <?php } elseif($property_standard_info['data']['MlsStatus'] == "Pending") {?>
                            <div class="pending-status">
                                <span>This property is pending.</span>
                            </div>
                        <?php }?>
                        <div class="address">
                            <h4><?=$property_standard_info['data']['UnparsedFirstLineAddress'];?>,</h4>
                            <h4><?=$property_standard_info['data']['City'];?>, <?=$property_standard_info['data']['StateOrProvince'];?>  <?=$property_standard_info['data']['PostalCode'];?></h4>
                            <h2>$<?php
                                #echo money_format('%i', $number);
                                echo number_format($property_standard_info['data']['CurrentPricePublic']);
                            ?></h2>
                            <p><?php
                                if(isset($property_standard_info['data']['BedsTotal']) &&
                                    $property_standard_info['data']['BedsTotal'] !== "********" &&
                                    !empty($property_standard_info['data']['BedsTotal'])
                                ) {
                                    echo $property_standard_info['data']['BedsTotal']." Bedroom";
                                }
                            ?> <?php
                                if(isset($property_standard_info['data']['BathsTotal']) &&
                                    $property_standard_info['data']['BathsTotal'] !== "********" &&
                                    !empty($property_standard_info['data']['BathsTotal'])
                                ) {
                                    echo $property_standard_info['data']['BathsTotal']." Bathroom";
                                }
                            ?> <?php
                                if(isset($property_standard_info['data']['BuildingAreaTotal']) && 
                                    $property_standard_info['data']['BuildingAreaTotal'] !== '********' && 
                                    !empty($property_standard_info['data']['BuildingAreaTotal'])) {

                                    echo $property_standard_info['data']['BuildingAreaTotal']." sqft";
                                        
                                } elseif(isset($property_standard_info['data']['LotSizeArea']) && 
                                    $property_standard_info['data']['LotSizeArea'] !== "********" && 
                                    !empty($property_standard_info['data']['LotSizeArea'])) {

                                    if(isset($property_standard_info['data']['LotSizeUnits']) && 
                                        ($property_standard_info['data']['LotSizeUnits'] !== '********') &&
                                        ($property_standard_info['data']['LotSizeUnits'] === 'Acres')) {
                                        echo number_format($property_standard_info['data']['LotSizeArea'], 2, '.', ',') .' sqft';
                                    } else {
                                        echo $property_standard_info['data']['LotSizeArea'] . ' sqft';
                                    }

                                } elseif(isset($property_standard_info['data']['LotSizeSquareFeet']) && 
                                    !empty($property_standard_info['data']['LotSizeSquareFeet']) &&
                                    is_numeric($property_standard_info['data']['LotSizeSquareFeet']) &&
                                    ($property_standard_info['data']['LotSizeSquareFeet'] != '0')) {

                                    echo $property_standard_info['data']['LotSizeSquareFeet'].' sqft';

                                } elseif(isset($property_standard_info['data']['LotSizeAcres']) && 
                                    !empty($property_standard_info['data']['LotSizeAcres']) &&
                                    is_numeric($property_standard_info['data']['LotSizeAcres']) &&
                                    ($property_standard_info['data']['LotSizeAcres'] != '0')) {

                                    echo number_format($property_standard_info['data']['LotSizeAcres'], 2, '.', ',');

                                } elseif(isset($property_standard_info['data']['LotSizeDimensions']) &&
                                    !empty($property_standard_info['data']['LotSizeDimensions']) &&
                                    ($property_standard_info['data']['LotSizeDimensions'] != '0') && 
                                    is_numeric($property_standard_info['data']['LotSizeDimensions'])) {

                                    echo $property_standard_info['data']['LotSizeDimensions'].' sqft';

                                } else {
                                    echo 'N/A';
                                }

                            ?></p>
                            <button class="btn btn-default" data-toggle="modal" data-target="#schedule-interview">SCHEDULE A SHOWING</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="property-detail-area" id="property-detail-area">
        <div class="container">
            <div class="row">
                <div class="property-detail-content">
                    <div class="col-md-12 col-lg-12">
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#details" aria-controls="details" role="tab" data-toggle="tab">Property Details</a></li>
                            <li role="presentation"><a href="#specs" aria-controls="specs" role="tab" data-toggle="tab">Full Specification</a></li>
                            <?php
                                $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
                                $vtour_url = isset($virtual_tours['data'][0]['StandardFields']['VirtualTours'][0]['Uri']) ? $virtual_tours['data'][0]['StandardFields']['VirtualTours'][0]['Uri'] : '';
                                // For the Youtube video to work in IFRAME
                                $search     = '#(.*?)(?:href="https?://)?(?:www\.)?(?:youtu\.be/|youtube\.com(?:/embed/|/v/|/watch?.*?v=))([\w\-]{10,12}).*#x';
                                if (preg_match($search, $vtour_url)) {
                                    $replace    = 'http://www.youtube.com/embed/$2';
                                    $vtour_url        = preg_replace($search,$replace,$vtour_url);
                                }

                                $protocol1 = parse_url($vtour_url, PHP_URL_SCHEME);
                                $protocol_vtour = $protocol1."://";


                            ?>
                            <?php if (isset($virtual_tours['data'][0]['StandardFields']['VirtualTours'][0]['Uri']) && !empty($virtual_tours['data'][0]['StandardFields']['VirtualTours'][0]['Uri'])): ?>
                                <?php if ($protocol != $protocol_vtour): ?>
                                    <!-- Open new tab -->
                                    <li class="virtualTour" role="presentation"><a href="<?=$vtour_url;?>"  target="_blank">Virtual Tour</a></li>
                                <?php else: ?>
                                    <!-- Open modal -->
                                    <li class="virtualTour" role="presentation"><a href="#tourModal" data-title="Tooltip" data-trigger="hover" data-toggle="modal" title="Virtual Tour">Virtual Tour</a></li>
                                <?php endif ?>
                            <?php endif ?>
                        </ul>

                        <div class="tab-content">

                            <div role="tabpanel" class="tab-pane active" id="details">
                                <h4>Description:</h4>
                                <p>
                                    <?=$property_standard_info['data']['PublicRemarks']; ?>
                                    <?php if ($property_standard_info['data']['Supplement']): ?>
                                        <?php echo $property_standard_info['data']['Supplement'] ?>
                                    <?php endif ?>
                                </p>

                                <div class="property-detail-lists clearfix">
                                    <div class="col-lg-6 col-md-6">
                                        <h4>Listing & Location Info::</h4>
                                        <ul>
                                            <li>
                                                <label>Current Price:</label> $<?=(isset($property_standard_info['data']['CurrentPrice']) && ($property_standard_info['data']['CurrentPrice'] != "********")) ? number_format($property_standard_info['data']['CurrentPrice']) : "n/a"; ?>
                                            </li>
                                            <li>
                                                <label>Property Sub Type:</label> <?=(isset($property_standard_info['data']['PropertySubType']) && ($property_standard_info['data']['PropertySubType'] != "********")) ? $property_standard_info['data']['PropertySubType'] : "n/a"; ?>
                                            </li>
                                            <li>
                                                <label>MLS Area Major:</label> <?=(isset($property_standard_info['data']['MLSAreaMajor']) && ($property_standard_info['data']['MLSAreaMajor'] != "********")) ? $property_standard_info['data']['MLSAreaMajor'] : "n/a"; ?>
                                            </li>
                                            <li>
                                                <label>List Price:</label> $<?=(isset($property_standard_info['data']['ListPrice']) && ($property_standard_info['data']['ListPrice'] != "********")) ? number_format($property_standard_info['data']['ListPrice']) : "n/a"; ?>
                                            </li>
                                            <li>
                                                <label>Street Number:</label> <?=(isset($property_standard_info['data']['StreetNumber']) && ($property_standard_info['data']['StreetNumber'] != "********")) ? $property_standard_info['data']['StreetNumber'] : "n/a"; ?>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-lg-6 col-md-6">
                                        <h4>Keywords Info:</h4>
                                        <ul>
                                            <li>
                                                <label>Beds Total:</label> <?=(isset($property_standard_info['data']['BedsTotal']) && ($property_standard_info['data']['BedsTotal'] != "********")) ? $property_standard_info['data']['BedsTotal'] : "n/a"; ?>
                                            </li>
                                            <li>
                                                <label>Baths Total:</label> <?=(isset($property_standard_info['data']['BathsTotal']) && ($property_standard_info['data']['BathsTotal'] != "********")) ? $property_standard_info['data']['BathsTotal'] : "n/a"; ?>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                            <!--/////////////////   THIS IS OLD Property FULL DESCRIPTION THAT FETCH DATA FROM DSL  ///////////////////////-->
                            <!-- <div role="tabpanel" class="tab-pane" id="specs">
                                <div class="panel-group" role="tablist" aria-multiselectable="true">
                                <?php

                                  if(!empty($property_desc['groups'])){
                                    foreach($property_desc['groups'] as $key => $df) {
                                        $showTitle = 0;
                                        $groupname = $df['group'];
                                        $fields = $df['fields'];

                                        foreach ($fields as $field) {

                                            if ($field['Value']) {
                                                $showTitle = 1;
                                            }

                                        }

                                        if ($showTitle) { ?>

                                            <div class="panel panel-default">
                                                <div class="panel-heading" role="tab">
                                                    <h4 class="panel-title">
                                                        <a href="javascript:void(0);"><?=$groupname?></a>
                                                        <i class="fa fa-angle-down pull-right" aria-hidden="true"></i>
                                                    </h4>
                                                </div>
                                                <div class="panel-collapse collapse in <?php if($key == 0) { echo "moreDetails"; } ?>" role="tabpanel">
                                                    <div class="panel-body">
                                                        <div class="col-md-12">
                                                            <ul>
                                                            <?php


                                                                foreach ($fields as $field) {
                                                                    $value = $field['Value'];

                                                                    if ($field['Value']) { ?>
                                                                        <li>
                                                                            <label><?=$field['Label'];?>:</label> <?=$field['Value'];?>
                                                                        </li>
                                                                    <?php
                                                                    }
                                                                }
                                                            ?>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php

                                        }
                                    }
                                   } else {?>
                                    <h3 class="text-center">No data to display!</h3>
                                <?php  }
                          
                                ?>
                                </div>
                            </div> -->

                            <div role="tabpanel" class="tab-pane" id="specs">
                                <div class="property-detail-sections">
                                    <ul class="property-detail-list">
                                        <div class="panel-group" id="accordion">
                                            <?php if(!empty($property_full_spec['data'])) {
                                                    $count=0;
                                                    $i=0;
                                                   foreach ($property_full_spec['data']['Main'] as $key => $value) {
                                                        foreach ($value as $key2 => $value2) {
                                                            $count++;
                                                            $i++;
                                                                if($count>0) { ?>
                                                                    <div class="panel panel-default">
                                                                        <div class="panel-heading" role="tab">
                                                                            <a  href="javascript:void(0);">
                                                                                <h4 class='panel-title'><?php echo $key2; ?><i class="indicator glyphicon glyphicon-chevron-up pull-right"></i></h4>
                                                                            </a>
                                                                        </div>
                                                                        <div id="<?=$i;?>" class="panel-collapse collapse in">
                                                                            <div class="panel-body">
                                                                                <ul>
                                                                                    <?php
                                                                                        foreach ($value2 as $key3 => $value3) {
                                                                                            foreach ($value3 as $key4 => $value4) { ?>
                                                                                            <li>
                                                                                                <strong><?php echo $key4; ?>: </strong>
                                                                                                <span><?php echo (isset($value4) && !empty($value4)) ? $value4 : 1; ?></span>
                                                                                            </li>
                                                                                    <?php
                                                                                            }
                                                                                        }
                                                                                    ?>

                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                <?php } ?>
                                                        <?php } ?>
                                                   <?php } ?>

                                            <?php } else { ?>

                                            <?php } ?>
                                        </div>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                        if (isset($virtual_tours['data'][0]['StandardFields']['VirtualTours'][0]['Name']) AND !empty($virtual_tours['data'][0]['StandardFields']['VirtualTours'][0]['Name'])) {

                           $tourName = $virtual_tours['data'][0]['StandardFields']['VirtualTours'][0]['Name'];
                        }
                    ?>

                        <!-- Virtual Tour Modal -->
                       <div id="tourModal" class="modal fade" data-backdrop="static">
                            <div class="modal-dialog modal-virtual-tour">
                                <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close close-tour" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">X</span></button>
                                            <h4 class="text-center"><?php echo isset($tourName) ? $tourName : ''; ?></h4>
                                            </div>
                                             <div class="virtual-tours-modal text-center">
                                                 <?php if (isset($virtual_tours['data'][0]['StandardFields']['VirtualTours'][0]['Uri']) AND !empty($virtual_tours['data'][0]['StandardFields']['VirtualTours'][0]['Uri'])) {

                                                        $vtour = $virtual_tours['data'][0]['StandardFields']['VirtualTours'][0]['Uri'];
                                                        $findDropbox   = 'dropbox';
                                                        $virtualTourdropbox = strpos($vtour, $findDropbox);

                                                        // var_dump($vtour);
                                                   ?>

                                                        <!-- For the Dropbox Link for Virtual Tour -->
                                                        <?php
                                                        if ($virtualTourdropbox !== false) { ?>
                                                                <div class="virtual-dropbox">
                                                                    <h2 class="alert alert-info"> Virtual Tour preview is not available!</h2><br>
                                                                    <p>Please click the link below for the preview</p>
                                                                    <p class="virtual-drop-link"><strong><a href="<?php echo $tour['data'][0]['StandardFields']['VirtualTours'][0]['Uri']; ?>" target="_blank">Virtual Tour</a></strong></p>
                                                                </div>

                                                        <?php } else { ?>


                                                         <?php
                                                            // For the Youtube video to work in IFRAME
                                                            $string     = $vtour;
                                                            $search     = '#(.*?)(?:href="https?://)?(?:www\.)?(?:youtu\.be/|youtube\.com(?:/embed/|/v/|/watch?.*?v=))([\w\-]{10,12}).*#x';
                                                            $replace    = 'http://www.youtube.com/embed/$2';
                                                            $url        = preg_replace($search,$replace,$string);

                                                        ?>

                                                         <?php
                                                             //Identify if there is a string "youtube"
                                                            $mystring = $vtour;
                                                            $findme   = 'youtube';
                                                            $pos = strstr($mystring, $findme);

                                                            if ($pos !== false) { ?>
                                                                <iframe class="virtualTours-iframe" width="100%" height="750px"  data-src="<?php echo $url;?>" src="about:blank" frameborder="0"></iframe>


                                                            <?php } else { ?>
                                                                     <iframe class="virtualTours-iframe" width="100%" height="750px" data-src="<?php echo $mystring; ?>" src="about:blank" frameborder="0"></iframe>

                                                <?php           }

                                                            }

                                                    }

                                                ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </section>
    <div id="slider-gallery-area">
        <div class="container">
            <div class="gallery-title">
                <div class="col-md-12">
                    <div class="container">
                        <div class="row">
                            <h2>Property Gallery</h2>
                        </div>
                    </div>
                </div>
            </div>
            <ul id="vertical">
                <?php if (!empty($photo_gallery)): ?>
                    <?php foreach ($photo_gallery as $pg): ?>
                        <li data-thumb="<?=$pg['Uri1024']; ?>" data-src="<?=$pg['Uri1024']; ?>">
                            <img src="<?=$pg['Uri1024']; ?>" class="img-responsive" />
                        </li>
                    <?php endforeach ?>
                <?php else: ?>
                    <img src="<?=isset($photos['data'][0]['StandardFields']['Photos'][0]['UriLarge']) ? $photos['data'][0]['StandardFields']['Photos'][0]['UriLarge'] :""; ?>" alt="" class="img-responsive">
                <?php endif ?>
            </ul>
        </div>
    </div>
    <section class="property-map" id="property-map">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="location-info-container">
                        <h2>Location</h2>
                        <p>
                            <?=$property_standard_info['data']['UnparsedFirstLineAddress'];?>,
                        </p>
                        <p>
                            <?=$property_standard_info['data']['City'];?>, <?=$property_standard_info['data']['StateOrProvince'];?> <?=$property_standard_info['data']['PostalCode'];?>
                        </p>
                        <p><b>Nearby:</b></p>
                        <label class="radio-inline">
                            <input type="radio" name="type" id="changemode-school" value="option1"> School
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="type" id="changemode-restaurants" value="option2"> Restaurant
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="type" id="changemode-store" value="option3"> Store
                        </label>
                        <button class="btn btn-default" data-toggle="modal" data-target="#schedule-interview">SCHEDULE A SHOWING</button>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=<?=getenv('GOOGLE_MAP_APIKEY'); ?>&libraries="></script>
        <script type="text/javascript">
            // When the window has finished loading create our google map below
            google.maps.event.addDomListener(window, 'load', init);

            var map;
            var infowindow;

            function init() {
                // Basic options for a simple Google Map
                // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
                latitude = <?=$lat; ?>;
                longitude = <?=$long;?>;
                var mapOptions = {
                    // How zoomed in you want the map to start at (always required)
                    zoom: 15,

                    scrollwheel: false,

                    // The latitude and longitude to center the map (always required)
                    center: new google.maps.LatLng(latitude, longitude), // New York

                    // How you would like to style the map.
                    // This is where you would paste any style found on Snazzy Maps.
                    styles: [{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#444444"}]},{"featureType":"administrative.country","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"administrative.country","elementType":"geometry","stylers":[{"visibility":"off"}]},{"featureType":"administrative.country","elementType":"geometry.fill","stylers":[{"visibility":"off"}]},{"featureType":"administrative.country","elementType":"geometry.stroke","stylers":[{"visibility":"off"}]},{"featureType":"administrative.province","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"administrative.locality","elementType":"labels","stylers":[{"hue":"#ffe500"}]},{"featureType":"landscape","elementType":"all","stylers":[{"color":"#f2f2f2"},{"visibility":"on"}]},{"featureType":"landscape.natural","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"landscape.natural.landcover","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"landscape.natural.terrain","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"landscape.natural.terrain","elementType":"geometry","stylers":[{"visibility":"on"}]},{"featureType":"landscape.natural.terrain","elementType":"geometry.fill","stylers":[{"visibility":"on"}]},{"featureType":"landscape.natural.terrain","elementType":"geometry.stroke","stylers":[{"visibility":"on"}]},{"featureType":"landscape.natural.terrain","elementType":"labels","stylers":[{"visibility":"on"}]},{"featureType":"landscape.natural.terrain","elementType":"labels.text","stylers":[{"visibility":"on"}]},{"featureType":"landscape.natural.terrain","elementType":"labels.text.fill","stylers":[{"visibility":"on"}]},{"featureType":"landscape.natural.terrain","elementType":"labels.text.stroke","stylers":[{"visibility":"on"}]},{"featureType":"landscape.natural.terrain","elementType":"labels.icon","stylers":[{"visibility":"on"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"poi.attraction","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"poi.business","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"poi.place_of_worship","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"poi.school","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":-100},{"lightness":45},{"visibility":"on"}]},{"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"transit.station","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"transit.station.airport","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#9bdffb"},{"visibility":"on"}]}]
                };

                // Get the HTML DOM element that will contain your map
                // We are using a div with id="map" seen below in the <body>
                var mapElement = document.getElementById('gmap_canvas');

                // Create the Google Map using our element and options defined above
                var map = new google.maps.Map(mapElement, mapOptions);

                var infowindow = new google.maps.InfoWindow({
                  content: '<?=$property_standard_info['data']['UnparsedFirstLineAddress'];?>, <br> <strong><?=$property_standard_info['data']['City'];?></strong>'
                });

                // Let's also add a marker while we're at it
                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(latitude, longitude),
                    map: map,
                    title: '<?=$property_standard_info['data']['UnparsedFirstLineAddress'];?>, <?=$property_standard_info['data']['City'];?>'
                });

                marker.addListener('click', function() {
                  infowindow.open(map, marker);
                });

                setupClickListener('changemode-school', "school", "school_location");
                setupClickListener('changemode-restaurants', "restaurant", "restaurant_location");
                setupClickListener('changemode-store', "store", "store_location");

                function setupClickListener(id, mode,loc) {


                    var radioButton = document.getElementById(id);

                    radioButton.addEventListener('click', function() {
                        modes = mode;


                        if(mode == "school")
                        {
                            mode_school( mode );
                        }

                        if(mode == "restaurant")
                        {
                            mode_restaurant( mode );
                        }

                        if(mode == "store")
                        {
                            mode_store( mode );
                        }
                    });
                }

            }
            // end of init

            // school
            function mode_school(mode) {
                console.log(mode);
                document.getElementById('restaurant_map').style.display = 'none';
                document.getElementById('store_map').style.display = 'none';
                document.getElementById('gmap_canvas').style.display = 'none';
                //marker.setVisible(false);
                var res_location = {lat: <?=$lat; ?>, lng: <?=$long;?>};

                document.getElementById('school_map').style.display = 'block';

                var pyrmont = {lat: <?=$lat; ?>, lng: <?=$long;?>};

                map = new google.maps.Map(document.getElementById('school_map'), {
                  center: pyrmont,
                  zoom: 15,
                  icon: 'https://www.google.com/intl/en_us/mapfiles/ms/micons/red-dot.png',
                  scrollwheel: false
                });

                infowindow = new google.maps.InfoWindow({
                  content: '<?=$property_standard_info['data']['UnparsedFirstLineAddress'];?>, <br> <strong><?=$property_standard_info['data']['City'];?></strong>'
                });

                // Let's also add a marker while we're at it
                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(latitude, longitude),
                    map: map,
                    title: '<?=$property_standard_info['data']['UnparsedFirstLineAddress'];?>, <?=$property_standard_info['data']['City'];?>'
                });

                marker.addListener('click', function() {
                  infowindow.open(map, marker);
                });

                var service = new google.maps.places.PlacesService(map);
                service.nearbySearch({
                  location: pyrmont,
                  radius: 500,
                  type: [mode]
                }, callback_school);

            }

          function callback_school(results, status) {
            if (status === google.maps.places.PlacesServiceStatus.OK) {
              for (var i = 0; i < results.length; i++) {
                createMarker_school(results[i]);
              }
            }
          }

          function createMarker_school(place) {
            var placeLoc = place.geometry.location;
            var marker = new google.maps.Marker({
              map: map,
              icon: 'https://www.google.com/intl/en_us/mapfiles/ms/micons/blue-dot.png',
              position: place.geometry.location
            });

            google.maps.event.addListener(marker, 'click', function() {
              infowindow.setContent(place.name);
              infowindow.open(map, this);
            });
          }

          // restaurant
            function mode_restaurant(mode) {
                console.log(mode);
                document.getElementById('school_map').style.display = 'none';
                document.getElementById('store_map').style.display = 'none';
                document.getElementById('gmap_canvas').style.display = 'none';
                //marker.setVisible(false);
                var res_location = {lat: <?=$lat; ?>, lng: <?=$long;?>};

                document.getElementById('restaurant_map').style.display = 'block';

                var pyrmont = {lat: <?=$lat; ?>, lng: <?=$long;?>};

                map = new google.maps.Map(document.getElementById('restaurant_map'), {
                  center: pyrmont,
                  zoom: 15,
                  icon: 'http://www.google.com/intl/en_us/mapfiles/ms/micons/red-dot.png',
                  scrollwheel: false
                });

                infowindow = new google.maps.InfoWindow({
                  content: '<?=$property_standard_info['data']['UnparsedFirstLineAddress'];?>, <br> <strong><?=$property_standard_info['data']['City'];?></strong>'
                });

                // Let's also add a marker while we're at it
                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(latitude, longitude),
                    map: map,
                    title: '<?=$property_standard_info['data']['UnparsedFirstLineAddress'];?>, <?=$property_standard_info['data']['City'];?>'
                });

                marker.addListener('click', function() {
                  infowindow.open(map, marker);
                });

                var service = new google.maps.places.PlacesService(map);
                service.nearbySearch({
                  location: pyrmont,
                  radius: 500,
                  type: [mode]
                }, callback_restaurant);

            }

          function callback_restaurant(results, status) {
            if (status === google.maps.places.PlacesServiceStatus.OK) {
              for (var i = 0; i < results.length; i++) {
                createMarker_restaurant(results[i]);
              }
            }
          }

          function createMarker_restaurant(place) {
            var placeLoc = place.geometry.location;
            var marker = new google.maps.Marker({
              map: map,
              icon: 'https://www.google.com/intl/en_us/mapfiles/ms/micons/yellow-dot.png',
              position: place.geometry.location
            });

            google.maps.event.addListener(marker, 'click', function() {
              infowindow.setContent(place.name);
              infowindow.open(map, this);
            });
          }

          // store
            function mode_store(mode) {
                console.log(mode);
                document.getElementById('school_map').style.display = 'none';
                document.getElementById('restaurant_map').style.display = 'none';
                document.getElementById('gmap_canvas').style.display = 'none';
                //marker.setVisible(false);
                var res_location = {lat: <?=$lat; ?>, lng: <?=$long;?>};

                document.getElementById('store_map').style.display = 'block';

                var pyrmont = {lat: <?=$lat; ?>, lng: <?=$long;?>};

                map = new google.maps.Map(document.getElementById('store_map'), {
                  center: pyrmont,
                  zoom: 15,
                  icon: 'https://www.google.com/intl/en_us/mapfiles/ms/micons/red-dot.png',
                  scrollwheel: false
                });

                infowindow = new google.maps.InfoWindow({
                  content: '<?=$property_standard_info['data']['UnparsedFirstLineAddress'];?>, <br> <strong><?=$property_standard_info['data']['City'];?></strong>'
                });

                // Let's also add a marker while we're at it
                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(latitude, longitude),
                    map: map,
                    title: '<?=$property_standard_info['data']['UnparsedFirstLineAddress'];?>, <?=$property_standard_info['data']['City'];?>'
                });

                marker.addListener('click', function() {
                  infowindow.open(map, marker);
                });

                var service = new google.maps.places.PlacesService(map);
                service.nearbySearch({
                  location: pyrmont,
                  radius: 500,
                  type: [mode]
                }, callback_store);

            }

          function callback_store(results, status) {
            if (status === google.maps.places.PlacesServiceStatus.OK) {
              for (var i = 0; i < results.length; i++) {
                createMarker_store(results[i]);
              }
            }
          }

          function createMarker_store(place) {
            var placeLoc = place.geometry.location;
            var marker = new google.maps.Marker({
              map: map,
              icon: 'https://www.google.com/intl/en_us/mapfiles/ms/micons/green-dot.png',
              position: place.geometry.location
            });

            google.maps.event.addListener(marker, 'click', function() {
              infowindow.setContent(place.name);
              infowindow.open(map, this);
            });
          }
        </script>
        <div style='overflow:hidden;height:470px;width:100%;'>
            <div id='gmap_canvas' style='height:470px;width:100%;'></div>
            <div id="school_map" style="height:470px;width:100%;"></div>
            <div id="restaurant_map" style="height:470px;width:100%;"></div>
            <div id="store_map" style="height:470px;width:100%;"></div>
            <style>
                #school_map, #restaurant_map, #store_map, #gmap_canvas img {
                    max-width: none!important;
                    background: none!important
                }
            </style>
        </div>
    </section>
    <section class="agent-info-area" id="agent-info-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="agent-title">Agent Info</h2>
                </div>
                <p class="clearfix"></p>
                <div class="agent-details">
                    <div class="col-md-6 col-sm-6">
                        <p class="text-center">
                            <?php if ($account_photo): ?>
                                <div class="agent-info-image" style="background-image: url('<?php echo getenv('AWS_S3_ASSETS') . "uploads/photo/" . $account_photo; ?>')"></div>
                            <?php elseif(!empty($account['Images'][0])): ?>
                                <div style="background-image: url('<?=$account['Images'][0]['Uri'];?>')" alt="" class="agent-info-image"></div>
                            <?php else: ?>
                                <div style="background-image: url('<?=base_url().'assets/spw/'?>assets/img/agent-header100.png')" alt="" class="agent-info-image"></div>
                            <?php endif ?>
                        </p>
                    </div>
                    <?php //print_r($account); ?>
                    <?php if (isset($account) && !empty($account)): ?>
                        <div class="col-md-6 col-sm-6">
                            <?php if ($username): ?>
                                <h2><?=$username; ?></h2>
                            <?php endif ?>
                            <pre style="display: none;">
                                <?php var_dump($account); ?>
                            </pre>
                            <ul>

                                <?php if (isset($user_info->phone) && !empty($user_info->phone)): ?>
                                    <li>Phone Number: <span><?=$user_info->phone; ?></span></li>
                                <?php elseif (isset($account['Phones'][0]['Number'])): ?>
                                    <li>Phone Number: <span><?=$account['Phones'][0]['Number']; ?></span></li>
                                <?php endif ?>
                                <?php if (isset($user_info->email) && !empty($user_info->email)): ?>
                                    <li>Email: <span><?=$user_info->email; ?></span></li>
                                <?php else: ?>
                                    <li>Email: <span><?=$account['Emails'][0]['Address']; ?></span></li>
                                <?php endif ?>
                                <?php if (isset($account['Office'])): ?>
                                    <li>Broker Name: <span><?=$account['Office']; ?></span></li>
                                <?php endif ?>
                                <?php if (isset($property_standard_info['ListOfficePhone'])): ?>
                                    <li>Broker Number: <span><?=$property_standard_info['ListOfficePhone']; ?></span></li>
                                <?php endif ?>
                                <?php if ($account['Addresses'][0]['Region'] == 'CA'): ?>
                                    <li>License No.: CAL-BRE#<span><?=$account['LicenseNumber']; ?></span></li>
                                <?php elseif (isset($user_info->license_number) && !empty($user_info->license_number)): ?>
                                    <li>License No.: <span><?= $user_info->license_number; ?></span></li>
                                <?php elseif (isset($account['LicenseNumber'])): ?>
                                    <li>License No.: <span><?= $account['LicenseNumber']; ?></span></li>
                                <?php endif ?>
                                <?php if (isset($property_standard_info['ListOfficeFax'])): ?>
                                    <li>Fax: <span><?=$property_standard_info['ListOfficeFax']; ?></span></li>
                                <?php endif ?>
                            </ul>
                        </div>
                    <?php endif ?>
                </div>
            </div>
        </div>
    </section>
    <section class="showing" id="showing">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>Interested in a Showing?</h2>
                    <p>
                        <?=$property_standard_info['data']['PublicRemarks']; ?>
                        <?php if ($property_standard_info['data']['Supplement']): ?>
                            <?php echo $property_standard_info['data']['Supplement'] ?>
                        <?php endif ?>
                    </p>
                    <p class="text-center"><button class="btn btn-default" data-toggle="modal" data-target="#schedule-interview">SCHEDULE A SHOWING</button></p>
                </div>
            </div>
        </div>
    </section>
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ul class="list-inline text-center social-media"><?php /** var array $sml */ ?>
                        <li><a href="<?= get_social_media_link_url('facebook.com', '' !== $sml['facebook'] ? $sml['facebook'] : 'AgentSquared') ?>" target="_blank"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="<?= get_social_media_link_url('twitter.com', '' !== $sml['twitter'] ? $sml['twitter'] : 'agentsquared') ?>" target="_blank"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="<?= get_social_media_link_url('linkedin.com', '' !== $sml['linkedin'] ? $sml['linkedin'] : 'company/agentsquared') ?>" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                        <li><a href="<?= get_social_media_link_url('instagram.com', '' !== $sml['instagram'] ? $sml['instagram'] : 'agentsquared') ?>" target="_blank"><i class="fa fa-instagram"></i></a></li>
                    </ul>
                    <p class="text-center">
                        Copyright 2016 All Rights Reserved. Developed by <a href="https://agentsquared.com" class="company" style="color:#FFF;">AgentSquared</a>
                    </p>
                </div>
            </div>
        </div>
    </footer>

    <div class="modal fade" id="terms" tabindex="-1" role="dialog" aria-labelledby="">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <!-- <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Modal title</h4>
                </div> -->
                <div class="modal-body">
                    <div class="col-md-12 no-padding">
                        <div class="modal-image-container">
                            <img src="<?=base_url().'assets/spw/'?>assets/img/terms.jpg" alt="" class="img-responsive">
                            <div class="modal-title"><h2 class="text-center">Terms and Conditions</h2></div>
                            <div class="modal-header-overlay"></div>
                        </div>
                    </div>
                    <div class="col-md-12 agreement-text">
                        <p>
                            PLEASE READ THESE TERMS AND CONDITIONS OF USE ("SITE TERMS") CAREFULLY. YOUR USE OF THE SITE IS CONDITIONED UPON YOUR ACCEPTANCE OF THESE SITE TERMS WITHOUT MODIFICATION. BY ACCESSING OR USING THIS WEB SITE, YOU AGREE TO BE BOUND BY THESE SITE TERMS AND ALL TERMS INCORPORATED BY REFERENCE. IF YOU DO NOT AGREE TO THESE SITE TERMS, DO NOT USE THIS WEB SITE.
                        </p>
                        <p>
                            The Web site you are accessing (the "Site") is hosted by AgentSquared.com on behalf of one of its real estate industry customers (the "Company"). AgentSquared and Company are independent contractors and are not partners or otherwise engaged in any joint venture in delivering the Site. Notwithstanding that fact, these Site Terms apply to both Agent Squared and Company and as such, any reference to "we", "us" or "our" herein will apply equally to both Agent Squared and Company. These Site Terms apply exclusively to your access to, and use of, the Site so please read them carefully. These Site Terms do not alter in any way the terms or conditions of any other agreement you may have with Agent Squared, Company, or their respective subsidiaries or affiliates, for services, products or otherwise. We reserve the right to change or modify any of the Site Terms and the Site, at any time. If we decide to change our Site Terms, we will post a new version on the Site and update the date. Any changes or modifications will be effective immediately upon posting of the revisions on the Site, and you waive any right you may have to receive specific notice of such changes or modifications. Your use of the Site following the posting of changes or modifications to the Site Terms will constitute your acceptance of the revised Site Terms. Therefore, you should frequently review the Site Terms and applicable policies from time-to-time to understand the terms and conditions that apply to your use of the Site. If you do not agree to the amended terms, you must immediately stop using the Site.
                        </p>

                        <div class="col-md-6">
                            <h4>Property Listing Data</h4>
                        <p>
                            Any real estate listing data provided to you in connection with the Site is not intended to be a representation of the complete Multiple Listing Service data for any of our MLS sources. We are not liable for and do not guarantee the accuracy of any listing data or other data or information found on the Site, and all such information should be independently verified. The information provided in connection with the Site is for the personal, non-commercial use of consumers and may not be used for any purpose other than to identify prospective properties consumers may be interested in purchasing. Some properties which appear for sale on this website may no longer be available because they are under contract, have sold or are no longer being offered for sale.
                        </p>

                        <h4>Use of Site; Limited License</h4>
                        <p>
                            You are granted a limited, non-sublicensable license to access and use the Site and all content, data, information and materials included in the Site (the "Site Materials") solely for your personal use, subject to the terms and conditions set forth in these Site Terms. You may not use the Site or any Site Materials for commercial purposes. You agree that you will not modify, copy, distribute, resell, transmit, display, perform, reproduce, publish, license, create derivative works from, frame in another Web page, use on any other web site or service any of the Site Materials. You will not use the Site or any of the Site Materials other than for its intended purpose or in any way that is unlawful, or harms Agent Squared, Company and/or their suppliers.
                        </p>
                        <p>
                            Without prejudice to the foregoing, you may not engage in the practices of "screen scraping," "database scraping," "data mining" or any other activity with the purpose of obtaining lists of users or other information from the Site or that uses web "bots" or similar data gathering or extraction methods. You may not obtain or attempt to obtain any materials or information through any means not intentionally made available or provided for through the Site.
                        </p>
                        <p>
                            Any use of the Site or the Site Materials other than as specifically authorized herein is strictly prohibited and will terminate the license granted herein. Such unauthorized use may also violate applicable laws including without limitation copyright and trademark laws and applicable communications regulations and statutes. Unless explicitly stated herein, nothing in these Site Terms shall be construed as conferring any license to intellectual property rights, whether by estoppel, implication or otherwise. This license is revocable by us at any time.
                        </p>
                        <h4>Responsibility for Your Conduct and User Content</h4>
                        <p>
                            You are solely liable for your conduct and for any information, text, photos, content, materials or messages that you upload, post or transmit to the Site (collectively the "User Content"). You may not provide false or misleading information to the Site or submit information under false pretenses.
                        </p>
                        <p>
                            Without limiting anything else in these Site Terms, we may immediately terminate your access to and use of any of the Sites if you violate any of the foregoing or if you provide false or misleading information or submit information under false pretenses.
                        </p>
                        <h4>Account Security</h4>
                        <p>
                            Each person that is provided with a password and user ID to use the Site must agree to abide by these Site Terms and is responsible for all activity under such user ID. You are responsible for maintaining the confidentiality and security of any password connected with your account.
                        </p>
                        <h4>Submissions</h4>
                        <p>
                            You agree that any materials, including but not limited to questions, comments, suggestions, ideas, plans, notes, drawings, original or creative materials or other information regarding Company or the Site, provided by you in the form of e-mail or submissions to us, or postings on the Sites, are non-confidential (subject to our Privacy Policy). We will own exclusive rights, including all intellectual property rights, and will be entitled to the unrestricted use of these materials for any purpose, commercial or otherwise, without acknowledgment or compensation to you.
                        </p>
                        <h4>Hyperlinks</h4>
                        <p>
                            We make no claim or representation regarding, and accept no responsibility for, the quality, content, nature or reliability of third-party Web sites accessible by hyperlink from the Site, or Web sites linking to the Site. Such sites are not under our control and we are not responsible for the content of any linked site or any link contained in a linked site, or any review, changes or updates to such sites. We provide these links to you only as a convenience, and the inclusion of any link does not imply affiliation, endorsement or adoption by us of any site or any information contained therein. When you leave the Site, you should be aware that our terms and policies no longer govern. You should review the applicable terms and policies, including privacy and data gathering practices, of any site to which you navigate from the Site.
                        </p>
                        <h4>Third Party Products and Services; Third-Party Content</h4>
                        <p>
                            We may run advertisements and promotions from third parties on the Site or may otherwise provide information about or links or referrals to third-party products or services on the Site. Your business dealings or correspondence with, or participation in promotions of, such third parties, and any terms, conditions, warranties or representations associated with such dealings or promotions are solely between you and such third party. We are not responsible or liable for any loss or damage of any sort incurred as the result of any such dealings or promotions or from any third party products or services, and you use such third party products and services at your own risk. We do not sponsor, endorse, recommend or approve any such third party who advertises their goods or services through the Site. You should investigate and use your independent judgment regarding the merits, quality and reputation of any individual, entity or information that you find on or through the Site. We do not represent or warrant that any third party who places advertisements on the Site is licensed, qualified, reputable or capable of performing any such service.
                        </p>
                        <p>
                            We do not make any assurance as to the timeliness or accuracy of any third-party content or information provided on or linked to from the Site ("Third Party Content"). We do not monitor or have any control over any Third Party Content, and do not endorse or adopt any Third Party Content and can make no guarantee as to its accuracy or completeness. We will not update or review any Third Party Content, and if you choose to use such Third Party Content you do so at your own risk.
                        </p>
                        </div>
                        <div class="col-md-6">
                            <h4>Privacy</h4>
                        <p>
                            We believe strongly in providing you notice of how we collect and use your data, including personally identifying information, collected from the Sites. We have adopted a Privacy Statement to which you should refer to fully understand how we collect and use personally identifying information.
                        </p>

                        <h4>Consent to Communications</h4>
                        <p>
                            By filling out any forms on the Site or making any inquiries through the Site you acknowledge that we have an established business relationship and you expressly consent to being contacted us, whether by phone, mobile phone, email, mail or otherwise.
                        </p>

                        <h4>Disclaimer</h4>
                        <p>
                            THE SITE AND THE SITE MATERIALS (INCLUDING ALL THIRD PARTY CONTENT), AND ALL LINKS, INFORMATION, MATERIALS, EVALUATIONS, RECOMMENDATIONS, SERVICES AND PRODUCTS PROVIDED ON OR THROUGH THE SITES ARE PROVIDED ON AN "AS IS" BASIS WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED. YOU EXPRESSLY AGREE THAT USE OF THE SITE AND THE MATERIALS IS AT YOUR SOLE RISK. COMPANY AND AGENT SQUARED AND THEIR RESPECTIVE SUPPLIERS DISCLAIM ALL WARRANTIES, EXPRESS OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, TITLE, AND NON-INFRINGEMENT AS TO THE SITE, THE SITE MATERIALS, LINKS, INFORMATION, MATERIALS, SERVICES AND PRODUCTS AVAILABLE ON OR THROUGH ON THE SITE. NEITHER COMPANY NOR AGENT SQUARED REPRESENTS OR WARRANTS THAT THE MATERIALS ARE ACCURATE, COMPLETE, RELIABLE, CURRENT, OR ERROR-FREE; OR IS RESPONSIBLE FOR TYPOGRAPHICAL ERRORS OR OMISSIONS RELATING TO PRICING, TEXT, OR PHOTOGRAPHY. WHILE WE ATTEMPT TO ENSURE YOUR ACCESS AND USE OF THE SITES IS SAFE, NEITHER COMPANY NOR AGENT SQUARED REPRESENTS OR WARRANTS THAT THE SITES OR ITS SERVER(S) ARE FREE OF VIRUSES OR OTHER HARMFUL COMPONENTS. AGENT SQUARED IS MERELY A SERVICE PROVIDER TO COMPANY, AND IN NO EVENT SHALL AGENT SQUARED BE LIABLE TO USER FOR ANY OF THE PRODUCTS, SERVICES, CONTENT OR INFORMATION PROVIDED THROUGH THE SITE OR OTHERWISE PROVIDED BY OR ON BEHALF OF COMPANY, AND AGENT SQUARED MAKES NO REPRESENTATION OR WARRANTY WITH RESPECT THERETO.
                        </p>
                        <h4>Limitation of Liability</h4>
                        <p>
                            IN NO EVENT WILL COMPANY OR AGENT SQUARED OR ANY OF THEIR RESPECTIVE SUPPLIERS BE LIABLE FOR ANY DIRECT, SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY OTHER DAMAGES OF ANY KIND, INCLUDING BUT NOT LIMITED TO LOSS OF USE, LOSS OF PROFITS, OR LOSS OF DATA, WHETHER IN AN ACTION IN CONTRACT, TORT (INCLUDING BUT NOT LIMITED TO NEGLIGENCE), OR OTHERWISE, ARISING OUT OF OR IN ANY WAY CONNECTED WITH THE USE OF THE SITE OR THE SITE MATERIALS.
                        </p>
                        <h4>Termination; Refusal to Provide Services.</h4>
                        <p>
                            We may terminate or suspend your access to the Site at any time, with or without cause, and with or without notice. Upon such termination or suspension, your right to use the Site will immediately cease. Furthermore, we reserve the right not to respond to any requests for information for any reason, or no reason.
                        </p>
                        <h4>Applicable Law and Venue</h4>
                        <p>
                            These terms and conditions are governed by and construed in accordance with the laws of the State of California, applicable to agreements made and entirely to be performed within the State of California, without resort to its conflict of law provisions. You agree that any action at law or in equity arising out of or relating to these terms and conditions can be filed only in state or federal court located in San Diego, California, and you hereby irrevocably and unconditionally consent and submit to the exclusive jurisdiction of such courts over any suit, action or proceeding arising out of these terms and conditions.
                        </p>
                        <h4>General Terms</h4>
                        <p>
                            If any part of the Site Terms is determined to be invalid or unenforceable pursuant to applicable law, then the invalid or unenforceable provision will be deemed superseded by a valid, enforceable provision that most closely matches the intent of the original provision and the remainder of the Site Terms will continue in effect. We may assign this Agreement, in whole or in part, at any time with or without notice to you. You may not assign this Agreement, or assign, transfer or sublicense your rights, if any, in the Sites. Except as expressly stated herein, the Site Terms constitute the entire agreement between you and us with respect to the Sites.
                        </p>
                        <h4>Copyright and Trademark</h4>
                        <p>
                            Unless otherwise indicated in the Site, the Site Materials and the selection and arrangement thereof are the proprietary property of Agent Squared, Company or their suppliers and are protected by U.S. and international copyright laws. The Company name, and any Company products and services slogans or logos referenced herein are either trademarks or registered trademarks of Company in the United States and/or other countries. The names of actual third party companies and products mentioned in the Site may be the trademarks of their respective owners. Any rights not expressly granted herein are reserved.

                        The parties have required that the Site Terms and all documents relating thereto be drawn up in <English class=""></English>
                        </p>
                        </div>


                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-modal-close" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="housing" tabindex="-1" role="dialog" aria-labelledby="">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <!-- <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Modal title</h4>
                </div> -->
                <div class="modal-body">
                    <div class="col-md-12 no-padding">
                        <div class="modal-image-container">
                            <img src="<?=base_url().'assets/spw/'?>assets/img/terms.jpg" alt="" class="img-responsive">
                            <div class="modal-title"><h2 class="text-center">Fair Housing Statement</h2></div>
                            <div class="modal-header-overlay"></div>
                        </div>
                    </div>
                    <div class="col-md-12 agreement-text">
                        <p>
                            Federal and state fair housing laws were put into effect to create an even playing field for homebuyers in all areas of a real estate transaction. These laws prohibit discrimination based on race, color, religion, sex, disability, familial status, and national origin.
                        </p>
                        <h4>Civil Rights Act of 1866</h4>
                        <p>
                            The Civil Rights Act of 1866 prohibits all racial discrimination in the sale or rental of property.
                        </p>
                        <h4>Fair Housing Act</h4>
                        <p>
                            The Fair Housing Act of 1968 makes fair housing a national policy. It prohibits discrimination in the sale, lease or rental of housing, or making housing otherwise unavailable because of race, color, religion, sex, disability, familial status or national origin.
                        </p>
                        <h4>Americans with Disabilities Act</h4>
                        <p>
                            Title III of the Americans with Disabilities Act prohibits discrimination against persons with disabilities in commercial facilities and places of public accommodation.
                        </p>
                        <h4>Equal Credit Opportunity Act</h4>
                        <p>
                            The Equal Credit Opportunity Act makes it unlawful to discriminate against anyone on a credit application due to race, color, religion, national origin, sex, marital status, age or because all or part of an applicant's income comes from any public assistance program.
                        </p>
                        <h4>THE RESPONSIBILITIES</h4>
                        <p>
                            The home seller, the home seeker, and the real estate professional all have rights and responsibilities under the law.
                        </p>
                        <h4>For the Home Seller</h4>
                        <p>
                            You should know that as a home seller or landlord you are obligated not to discriminate in the sale, rental and financing of property on the basis of race, color, religion, sex, disability, familial status, or national origin. You may not instruct the licensed broker or salesperson acting as your agent to convey for you any limitations in the sale or rental, because the real estate professional is also obligated not to discriminate. Under the law, a home seller or landlord cannot establish discriminatory terms or conditions in the purchase or rental; deny that housing is available or advertise that the property is available only to persons of a certain race, color, religion, sex, disability, familial status, or national origin.
                        </p>
                        <h4>For the Home Seeker</h4>
                        <p>
                            You have the right to expect that housing will be available to you without discrimination or other limitations based on race, color, religion, sex, disability, familial status, or national origin. This includes the right to expect:
                        </p>
                        <ul>
                            <li>Housing in your price range made available to you without discrimination</li>
                            <li>Equal professional service</li>
                            <li>The opportunity to consider a broad range of housing choices</li>
                            <li>No discriminatory limitations on communities or locations of housing</li>
                            <li>No discrimination in the financing, appraising or insuring of housing</li>
                            <li>Reasonable accommodations in rules, practices and procedures for persons with disabilities</li>
                            <li>Non-discriminatory terms and conditions for sale, rental, financing, or insuring of a dwelling</li>
                            <li>To be free from harassment</li>
                        </ul>

                        <h4>For the Real Estate Professional</h4>
                        <p>
                            Agents in a real estate transaction may not discriminate on the basis of race, color, religion, sex, disability, familial status or national origin. They also may not follow such instructions from a home seller or landlord.
                        </p>
                        <h4>What To Do if You Feel the Law Has Been Violated</h4>
                        <p>
                            Discrimination complaints about housing may be filed with the nearest office of the U.S. Dept. of Housing and Urban Development (HUD) or by calling HUD's toll-free numbers, 1-800-669-9777 (voice) or TTY (800) 927-9275 contact HUD on the Internet athttp://www.hud.gov/offices/fheo/index.cfm
                        </p>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-modal-close" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="privacy" tabindex="-1" role="dialog" aria-labelledby="">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <!-- <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Modal title</h4>
                </div> -->
                <div class="modal-body">
                    <div class="col-md-12 no-padding">
                        <div class="modal-image-container">
                            <img src="<?=base_url().'assets/spw/'?>assets/img/terms.jpg" alt="" class="img-responsive">
                            <div class="modal-title"><h2 class="text-center">Pricacy Policy</h2></div>
                            <div class="modal-header-overlay"></div>
                        </div>
                    </div>
                    <div class="col-md-12 agreement-text">
                        <p>
                             The website you are accessing (the "Site") is hosted by Agent Squared on behalf of one of its real estate industry customers (the "Company"). Agent Squared and Company are independent contractors and are not partners or otherwise engaged in any joint venture in delivering the Site. Notwithstanding that fact, this Privacy Policy ("Policy") applies to both Agent Squared and Company and as such, any reference to "we", "us" or "our" herein will apply equally to both Agent Squared and Company. This Policy explains how personal information is collected, used, and disclosed with respect to your use of the Site, so you can make an informed decision about using the Site.
                        </p>
                        <p>
                            We reserve the right to change this Policy at any time. If we make any material changes to the Policy, we will post a new statement on the Site and update the effective date set forth above. Therefore, we encourage you to check the effective date of the Policy whenever you visit the Site to see if it has been updated since your last visit.
                        </p>
                        <h4>Information We Collect</h4>
                        <ul>
                            <li>
                                <h4>Personal Information.</h4>
                                <p>As used herein, the term "personal information" means information that specifically identifies an individual (such as a name, address, telephone number, mobile number, or e-mail address,), and information about that individual's location or activities or demographic information when such information is directly linked to personally identifiable information. Personal information does not include "aggregate" information, which is data we collect about the use of the Site or about a group or category of products, services or users, from which individual identities or other personal information has been removed. This Policy in no way restricts or limits our collection and use of aggregate information.</p>
                            </li>
                            <li>
                                <h4>Collection</h4>
                                <p>Personal information may be collected in a number of ways when you visit the Site. We may collect the personal information that you voluntarily provide to us. For example, when you fill out a form, send us an email, make a request, complete a survey, register for certain services on the Site, or participate in certain activities on the Site, we may ask you to provide personal information such as your e-mail address, name, home or work address or telephone number. We may also collect demographic information, such as your ZIP code, age, gender, and preferences. Information we collect may be combined with information we obtained through other services we offer. We may collect information about your visit to the Site, including the pages you view, the links you click and other actions taken in connection with the Site and services. We may also collect certain standard information that your browser sends to every web site you visit, such as your IP address, browser type and language, access times and referring Web site addresses. If we provide you with various services related to home selling and home buying (e.g., estimates of current market value of homes, moving costs, mortgage or finance costs, current home listings, neighborhood information, etc.), we may collect additional information related to such services. </p>
                            </li>
                            <li>
                                <h4>Cookies and Web Beacons.</h4>
                                <p>
                                    We may automatically collect certain information through the use of "cookies" or web beacons. Cookies are small pieces of information or files that are stored on a user's computer by the Web server through a user's browser to enable the site to recognize users who have previously visited them and retain certain information such as customer preferences and history. We may use cookies to identify users, customize their experience on our site or to serve relevant ads. If we combine cookies with or link them to any of the personal information, we would treat this information as personal information. We do not use Web beacons, or similar technology on the Site. However, we may permit third parties to place cookies or Web beacons on the Site to monitor the effectiveness of advertising and for other lawful purposes. A Web beacon, also known as a "Web bug" or a "clear gif" is a small, graphic image on a Web page, Web-based document or in an e-mail message that is designed to allow the site owner or a third party to monitor who is visiting a site and user activity on the site. We do not use (or permit third parties to use) Web beacons to collect personally identifying information about you. If you wish to block, erase, or be warned of cookies, please refer to your browser instructions to learn about these functions. However, if a browser is set to not accept cookies or if a user rejects a cookie, some portions of the Site may not function properly. For example, you may not be able to sign in and may not be able to access certain Site features or services.
                                </p>
                            </li>
                            <li>
                                <h4>Third Party Cookies and Web Beacons; Use of Third Party Ad Networks.</h4>
                                <p>
                                    We may also use third parties to serve ads on the Site.These third parties may place cookies, Web beacons or other devices on your computer to collect nonpersonal information, and information provided by these devices may be used, among other things, to deliver advertising targeted to your interests and to better understand the usage and visitation of the Site and the other sites tracked by these third parties. For example, we may allow other companies, called third-party ad servers or ad networks, to display advertisements on the Site. Some of these ad networks may place a persistent cookie on your computer in order to recognize your computer each time they send you an online advertisement. In this way, ad networks may compile information about where you, or others who are using your computer, saw their advertisements and determine which ads are clicked on. This information allows an ad network to deliver targeted advertisements that they believe will be of most interest to you. We do not have access to the cookies that may be placed by the third-party ad servers or ad networks. This Policy does not apply to, and we are not responsible for, cookies in third party ads, and we encourage you to check the privacy policies of advertisers and/or ad services to learn about their use of cookies and other technology.
                                </p>
                            </li>
                        </ul>
                        <h4>Use Of Personal Information</h4>
                        <p>
                            We use personal information internally to serve users of the Site, to enhance and extend our relationship with Site users and to facilitate the use of, and administration and operation of, the Site. For example, we may use personal information you provide to deliver services to you, to process your requests or transactions, to provide you with information you request, to personalize content, features and advertising on the Site, to anticipate and resolve problems with the Site, to respond to customer support inquiries, to inform you of other events, promotions, products or services offered by third parties that we think will be of interest to you, to send you relevant survey invitations related to the Site and for our own internal operations, and for any purpose for which such information was collected. We may, from time to time, contact you on behalf of external business partners about a particular offering that may be of interest to you. In those cases, your personal information (e-mail, name, address, telephone number) is not transferred to the third party unless you have consented to such transfer.
                        </p>
                        <h4>Disclosure Of Personal Information</h4>
                        <p>
                            Except as set forth in this Policy, we do not disclose, sell, rent, loan, trade or lease any personal information collected on the Site to any third party. We may provide aggregate information to third parties without restriction. We will disclose personal information in the following circumstances:
                        </p>
                        <ul>
                            <li>
                                <h4>Consent.</h4>
                                <p>
                                    We may share personal information with third parties when you give us your express or implied consent to do so, such as when you fill out a form requesting certain information from a third party.
                                </p>
                            </li>
                            <li>
                                <h4>Internal Analysis and Operations; Service Providers.</h4>
                                <p>
                                    We will use and disclose information about you for our own internal statistical, design or operational purposes, such as to estimate our audience size, measure aggregate traffic patterns, and understand demographic, user interest, purchasing and other trends among our users. We may outsource these tasks and disclose personal information about you to third parties, provided the use and disclosure of your personal information by these third parties is restricted to performance of such tasks. We may also share personal information with other third-party vendors, consultants, and service providers ("Service Providers") who are engaged by or working with us in connection with the operation of the Site or our business In some cases, the Service Provider may be directly collecting the information from you on our behalf. We require our third party vendors to keep such information confidential and to only use the information for the purpose it was provided. However, we are not responsible for the actions of Service Providers or other third parties, nor are we responsible for any additional information you provide directly to these Service Providers or other third parties.
                                </p>
                            </li>
                            <li>
                                <h4>Protection of Company and Others:</h4>
                                <p>
                                    We may disclose personal information when we believe it is appropriate to comply with the law (e.g., a lawful subpoena, warrant or court order); to enforce or apply this Policy or our other policies or agreements; to initiate, render, bill, and collect for amounts owed to us; to respond to claims, to protect our or our users' rights, property or safety; or to protect us, our users or the public from fraudulent, abusive, harmful or unlawful activity or use of the Site; or if we reasonably believe that an emergency involving immediate danger of death or serious physical injury to any person requires disclosure of communications or justifies disclosure of records without delay.
                                </p>
                            </li>
                            <li>
                                <h4>Business Transfers.</h4>
                                <p>
                                    In addition, information about users of the Site, including personally identifiable information provided by users, may be disclosed or transferred as part of, or during negotiations regarding, any merger, acquisition, debt financing, sale of company assets, as well as in the event of an insolvency, bankruptcy or receivership in which personally identifiable information could be transferred to third parties as one of the business assets of the company.
                                </p>
                            </li>
                            <li>
                                <h4>Co-Branded Services.</h4>
                                <p>
                                    Some services available through the Site may be co-branded and offered in conjunction with other companies. If you register for or use such services, then those other companies may also receive any information collected in conjunction with the co-branded services.
                                </p>
                            </li>
                        </ul>
                        <h4>Liks</h4>
                        <p>
                            Please be aware that we may provide links to third party Web sites from the Site as a service to its users and we are not responsible for the content or information collection practices of those sites. We have no ability to control the privacy and data collection practices of such sites and the privacy policies of such sites may differ from this Policy. Therefore, we encourage you to review and understand the privacy policies of such sites before providing them with any information.
                        </p>
                        <h4>Security</h4>
                        <p>
                            We use commercially reasonable measures to store and maintain personally identifying information, to protect it from loss, misuse, alternation or destruction by any unauthorized party while it is under our control.
                        </p>
                        <h4>Children</h4>
                        <p>
                            We do not intend, and the Site is not designed, to collect personal information from children under the age of 13. If you are under 13 years old, you should not provide information on the Site.
                        </p>
                        <h4>Choise and Access</h4>
                        <p>
                            You may unsubscribe at any time from receiving non-service related communications from us. Site users have the following options for accessing, changing and deleting personal information previously provided, or opting out of receiving communications from Company or Agent Squared:
                        </p>
                        <ul>
                            <li>email us at support@agentsquared.com; or</li>
                            <li>call us at the following telephone number: 800-901-4428</li>
                        </ul>
                        <h4>Questions and Concerns</h4>
                        <p>
                            If you have any questions about this Policy, the practices applicable to the Site, or your dealings with the Site, please contact us at the following address:
                        </p>
                        <p>Agent Squared</p>
                        <p>1155 Camino Del Mar, #103</p>
                        <p>
                            Del Mar, CA 92014
                        </p>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-modal-close" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
     <div class="modal fade" id="schedule-interview" tabindex="-1" role="dialog" aria-labelledby="">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header text-center">
                   <span style="font-size:18px" class="modal-title">Schedule a Showing</span>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="fa fa-times" aria-hidden="true"></i></span></button>
                </div>
                <div class="modal-body">
                    <div class="col-md-12 agreement-text">
                        <form id="scheduleShowing" action="<?php echo base_url("spw/base/save_schedule_for_showing"); ?>" method="post">
                            <input type="hidden" name="property_id" value="<?=$property_standard_info['data']['ListingKey'];?>"/>
                            <input type="hidden" name="property_name" value="<?=$property_standard_info['data']['UnparsedFirstLineAddress'];?>"/>
                            <input type="hidden" name="type" value="spw"/>
                            <input type="hidden" name="property_url" value="<?= (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";?>">
        

                            <div class="alert alert-success" role="alert" style="display:none;">
                              Your showing request received, we will contact you shortly.
                           </div>
                           <div class="alert alert-danger" role="alert" style="display:none;"></div>
                            <span class="display-text"></span>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">First Name</label>
                                <div class="col-sm-9">
                                    <input type="text" name="fname" id="customer-name" class="form-control" placeholder="First Name" required>
                                     <span class="valid-customer-name"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Last Name</label>
                                <div class="col-sm-9">
                                    <input type="text" name="lname" id="customer-name" class="form-control" placeholder="Last Name" required>
                                     <span class="valid-customer-name"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Email</label>
                                <div class="col-sm-9">
                                    <input type="email" name="email" id="customer-email" class="form-control" placeholder="Email" required>
                                    <span class="valid-customer-email"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Contact No.</label>
                                <div class="col-sm-9">
                                    <input type="text" name="phone_signup" id="customer-number" class="form-control" placeholder="Contact" required>
                                    <span class="valid-customer-number"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Schedule Date</label>
                                <div class="col-sm-9">
                                    <div class="modal-datepicker">
                                        <div class="input-group">
                                            <input type="text" name="date_scheduled" id="customer-schedule-date" class="form-control" required>
                                            <span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                             <span class="valid-customer-schedule"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Message</label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" name="sched_for_showing" id="customer-message" cols="30" rows="10" required></textarea>
                                     <span class="valid-customer-message"></span>
                                </div>
                            </div>

                             <div class="form-group text-right">
                                 <input type="submit" name="schedule_submit" id="schedule-submit" value="Submit" class="btn btn-default btn-schedule-close"/>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="col-md-12">
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="<?=base_url().'assets/spw/'?>assets/js/vendor/jquery-1.11.2.min.js"><\/script>')</script>
    <script src="<?=base_url().'assets/spw/'?>assets/js/vendor/bootstrap.min.js"></script>
    <script src="<?=base_url().'assets/spw/'?>assets/js/main.js"></script>
    <script src="<?=base_url().'assets/spw/'?>assets/js/bootstrap-datepicker.min.js"></script>
    <!-- <script src="<?=base_url().'assets/spw/'?>assets/js/lightbox.min.js"></script>
    <script>
    //     $(document).ready(function(){
    //         lightbox.option({
    //           'resizeDuration': 200,
    //           'wrapAround': true
    //         });
    //     });
    // </script> -->
    <script>
        var nowDate = new Date();
        var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0);

        $('.modal-datepicker input').datepicker({
            dateFormat :'yy-mm-dd',
            todayHighlight: true,
            startDate: 'today',
        });
    </script>
     <script>
        // trigger virtual tour on click
        $(".virtualTour").click(function(){
            var iframe = $(".virtualTours-iframe");
            iframe.attr("src", iframe.data("src"));
        });
    </script>
    <script>
        //close virtual tour on click
        $(".close-tour").click(function(){
            var iframe = $(".virtualTours-iframe");
            iframe.removeAttr("src");
        });
    </script>
    <script src="<?=base_url().'assets/spw/'?>assets/js/lightslider.js"></script>
    <script>
        $(document).ready(function() {
            $('#vertical').lightSlider({
                gallery:true,
                item:1,
                slideMargin:0,
                speed:500,
                auto:false,
                loop:true,
                adaptiveHeight:true,
                vertical:true,
                verticalHeight:600,
                vThumbWidth:220,
                thumbItem:4,
                thumbMargin:5,
                    onSliderLoad: function(el) {
                    el.lightGallery({
                        selector: '#vertical .lslide'
                    });
                },
                responsive : [
                    {
                        breakpoint:1025,
                        settings: {
                            item:1,
                            slideMove:1,
                            gallery:true,
                            thumbItem:8,
                            vThumbWidth:120
                        }
                    },
                    {
                        breakpoint:769,
                        settings: {
                            item:1,
                            slideMove:1,
                            gallery:true,
                            thumbItem:8,
                            vThumbWidth:100,
                            verticalHeight:400
                        }
                    },
                    {
                        breakpoint:737,
                        settings: {
                            item:1,
                            slideMove:1,
                            gallery:false,
                            thumbItem:0,
                            pager: false
                          }
                    },
                    {
                        breakpoint:668,
                        settings: {
                            item:1,
                            slideMove:1,
                            gallery:false,
                            thumbItem:0,
                            pager: false
                          }
                    },
                    {
                        breakpoint:641,
                        settings: {
                            item:1,
                            slideMove:1,
                            gallery:false,
                            thumbItem:0,
                            pager: false
                          }
                    },
                    {
                        breakpoint:480,
                        settings: {
                            item:1,
                            slideMove:1,
                            gallery:false,
                            thumbItem:0,
                            pager: false
                        }
                    }
                ]
            });
        });
    </script>
    <script src="<?=base_url().'assets/spw/'?>assets/js/lightgallery.js"></script>
</html>
