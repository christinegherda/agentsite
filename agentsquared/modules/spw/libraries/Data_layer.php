<?php
class Data_layer {
	protected $connection_data = NULL;
	protected $site_config = NULL;
	function __construct($data = null, $config = null) {
		$this->connection_data = $data;
		$this->site_config = $config;

		// if(!$this->check_connection()) {
		// 	$_SESSION["access_token"] = (isset$this->connection_data->data)$this->connection_data->data->authToken;
		// 	$_SESSION["access_id"] = $this->connection_data->data->userId;
		// }
	}
	public function check_connection() {
		$status = false;
    if (isset($this->connection_data)) {
      if($this->connection_data->status !== 'error' && $this->connection_data->status === 'success') {
  			if(!isset($_SESSION["access_token"])) {
  				$_SESSION["access_token"] = $this->connection_data->data->authToken;
  				$_SESSION["access_id"] = $this->connection_data->data->userId;
  			}
  			$status = true;
  		}
  		else {
  			$status = false;
  		}
    }

		return $status;
	}

	/* CONNECT TO DSL: POST Type */
 	public function post_endpoint($endpoint = NULL, $body=array()) {

 		if($endpoint) {

 			$call_url = rtrim($this->site_config['connection_url'], '/')."/".$endpoint;

 			$headers = array(
                'X-Auth-Token: '.$_SESSION['access_token'],
                'X-User-Id: '.$_SESSION['access_id'],
                'Content-Type:application/json'
            );

 			$body_json = json_encode($body, true);

 			$ch = curl_init();
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_TIMEOUT, 0);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $body_json);
			curl_setopt($ch, CURLOPT_URL, $call_url);

			$result_info  = curl_exec($ch);
            curl_close($ch);

			if(!$result_info) {
				$result_info = curl_error($ch);
			}
			
			return $result_info;
	 	} else {
	 		return false;
	 	}
	 }

 	/*CONNECT TO DSL: GET Type */
	function get_endpoint($endpoint = NULL) {
		if($endpoint) {
            $headers = array(
                'X-Auth-Token: '.$_SESSION['access_token'],
                'X-User-Id: '.$_SESSION['access_id'],
                'Content-Type:application/json'
            );

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, rtrim($this->site_config['connection_url'], '/')."/".$endpoint);  
	 		//curl_setopt($ch, CURLOPT_URL, $this->site_config['connection_url'].$endpoint);  
			curl_setopt($ch, CURLOPT_RETURNTRANSFER , TRUE);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			$content = curl_exec($ch);
			curl_close($ch);
			return $content;
		}
		else {
			return false;
		}
	}
	function getProperty() {

		if(self::check_connection()) {

			$endpoints = array(
				'property' => 'property/'.$this->site_config['listingid'].'/details/'.$this->site_config['agent_user_id'],
				'property_standard_info' => 'property/'.$this->site_config['listingid'].'/standard-fields/'.$this->site_config['agent_user_id'],
			);

			$data['property'] = $this->get_endpoint($endpoints['property']);
			$data['property_standard_info'] = $this->get_endpoint($endpoints['property_standard_info']);
			
			if(json_decode($data['property_standard_info'])->data != ''){

				$prop_id = json_decode($data['property_standard_info'])->id;

				$data['virtual_tours'] = $this->get_endpoint('property/'.$prop_id.'/virtual-tours/'.$this->site_config['agent_user_id']);
				$data['photos'] = $this->get_endpoint('property/'.$prop_id.'/photos/'.$this->site_config['agent_user_id']);
			}
			$data['account_photo'] = $this->getAccountDetails();
			$data['social_media_links'] = $this->getSocialMedia();
			$data['account'] = $this->getAccountDetailsFull();

			return $data;
		}
		else {
			return false;;
		}
	}

	 function getPropertyProxy($body=array()){

	 	if(self::check_connection()) {

	 		$prop_id = $this->get_property_id();

	 		if(isset($body["bearer_token"])){
	 			$token = $body["bearer_token"];
	 		 } else {
	 		 	$token = $body["access_token"];
	 		}

	 		if(!empty($prop_id) && !empty($body)){

	 			//list of endpoint using dsl_proxy to get data from spark
		 		$endpoints = array(
		 			'virtual_tours' => 'spark-listing/'.$prop_id['property_id'].'/virtual-tours',
					'photos' => 'spark-listing/'.$prop_id['property_id'].'/photos',
					'property_standard_info' => 'spark-listing/'.$prop_id['property_id'].'/standard-fields',
					//'property' => 'property/'.$prop_id['property_id'].'/details/'.$this->site_config['agent_user_id'] //this endpoint is fetching data from DSL
				);

		 		//services that pulled data direct to spark using dsl_proxy
		 		$data = array(

		 			'virtual_tours' => $this->post_endpoint($endpoints['virtual_tours'],$body),
					'photos' => $this->post_endpoint($endpoints['photos'],$body),
					'property_standard_info' => $this->post_endpoint($endpoints['property_standard_info'],$body),
					//'property' => $this->get_endpoint($endpoints['property']),//this endpoint is fetching data from DSL
					'property_full_spec' => $this->get_property_custom_field($prop_id['property_id'],$token),//this endpoint is fetching data from DSL PROXY
					'account_photo' => $this->getAccountDetails(),
					'social_media_links' => $this->getSocialMedia(),
					'account' => $this->getAccountDetailsFull()
				);

				return $data;
	 		}

		}
		else {
			return false;;
		}
	 }

	 public function get_property_custom_field($id = NULL, $token = NULL) {

	    $ret = array();

	    if($id) {

	      if($token) {

	        $url = "https://sparkapi.com/v1/listings/".$id."?_select=CustomFields&_expand=CustomFields";

	        $headers = array(
	          'Authorization:OAuth '.$token,
	          'Accept:application/json',
	          'X-SparkApi-User-Agent:MyApplication',
	        );

	        $ch = curl_init();
	        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	        curl_setopt($ch, CURLOPT_URL, $url);

	        $result = curl_exec($ch);
            curl_close($ch);
	        $result_info = json_decode($result, true);

	        if(isset($result_info["D"]["Results"]) && !empty($result_info["D"]["Results"])) {
	          $ret["data"] = $result_info["D"]["Results"][0]["CustomFields"][0];
	        }
	      }
	    }

	    return $ret;
	  }

	function mysql_connection(){

		return $conn = new mysqli(
            getenv('DB_HOSTNAME'),
            getenv('DB_USERNAME'),
            getenv('DB_PASSWORD'),
            getenv('DB_NAME')
        );
	}

	function get_property_id() {

		// Create connection
        $conn = $this->mysql_connection();

		// Check connection
		if ($conn->connect_error) {
		    die("Connection failed: " . $conn->connect_error);
		    exit;
		}
		/* Fetch Property ID */
		$sql = "SELECT property_id FROM property_activated where ListingID = '".$this->site_config["listingid"]."'";
		$result = $conn->query($sql);
		
		$prop_id = $result->fetch_assoc();
		return $prop_id;
	}
	function getSocialMedia() {
		// Create connection
        $conn = $this->mysql_connection();

		// Check connection
		if ($conn->connect_error) {
		    die("Connection failed: " . $conn->connect_error);
		    exit;
		}
		/* Fetch User Data */ // $this->site_config['agent_user_id']
		$sql = "SELECT facebook, twitter, linkedin, googleplus, instagram FROM social_media_links where user_id = ". $this->site_config['agent_user_id'];
		$result = $conn->query($sql);
        return $result->fetch_assoc();
	}
	function getAccountDetails() {
		// Create connection
        $conn = $this->mysql_connection();

		// Check connection
		if ($conn->connect_error) {
		    die("Connection failed: " . $conn->connect_error);
		    exit;
		}

		/* Fetch User Data */ // $this->site_config['agent_user_id']
		$sql = "SELECT agent_photo FROM users where id = ". $this->site_config['agent_user_id'];
		$result = $conn->query($sql);
		$user_data = array();
		if (isset($result->num_rows) && $result->num_rows > 0) {
		    $user_data = $result->fetch_assoc();
		}
		return ($user_data['agent_photo']) ? $user_data['agent_photo'] : false;
	}
	function getAccountDetailsFull() {
		// Create connection
        $conn = $this->mysql_connection();

		// Check connection
		if ($conn->connect_error) {
		    die("Connection failed: " . $conn->connect_error);
		    exit;
		}

		/* Fetch User Data */ // $this->site_config['agent_user_id']
		$sql = "SELECT account FROM account_meta where user_id = ". $this->site_config['agent_user_id']." AND type='account_info'";
		$result = $conn->query($sql);
		$user_data = array();
		if (isset($result->num_rows) && $result->num_rows > 0) {
		    $user_data = $result->fetch_assoc();
		}
		// print_r($user_data);
   		if(!empty($user_data)) {
   			$account_data = gzdecode($user_data['account']);
			return ($account_data) ? $account_data : false;
   		}
	}
}
