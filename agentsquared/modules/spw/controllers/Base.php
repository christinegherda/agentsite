<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Base extends MX_Controller  {

  protected $domain_user;

  function __construct() {
    parent::__construct();

    $this->domain_user = domain_to_user();
    $this->load->library('Data_layer');
    $this->load->model("home/customer_model", "customer");
    $this->load->model("home/home_model");
    $this->load->library("idx_auth", $this->domain_user->agent_id);
  }
  public function index ($data = NULL) {

  }
  public function app ($uri_segment = NULL) { 
    if($uri_segment) {
      list($address, $listing_id, $user_id) = explode('~', $uri_segment);
      // echo "$address, $listing_id, $user_id";
      $login = false;
      $dsl = false;
      $config = $this->_get_config($user_id, $listing_id);

      if($config) {
        $login = $this->_get_login($config);
      }
      if ($login && $config) {
        $this->session->set_userdata('access_token', $login->data->authToken);
        $this->session->set_userdata('access_id', $login->data->userId);
        $dsl = new Data_layer($login,$config);

        $agent_data = $this->home_model->getUsersAgentId($user_id);

        //get users token
        $token = $this->home_model->getUserAccessToken($agent_data->agent_id);
        $type = $this->home_model->getUserAcessType($agent_data->agent_id);
        
        if($type == 0) {
          $body = array(
            'access_token' => $token->access_token
          );
        }
        else {
          $body = array(
            'bearer_token' => $token->access_token
          );
        }

        $user_info = $this->home_model->getUsersInfo($user_id);
        $dataProxy = $dsl->getPropertyProxy($body);
        $account = json_decode($dataProxy['account'], TRUE);

        if (isset($user_info)) {
          $user_name = $user_info->first_name.' '.$user_info->last_name;
        } else {
          $user_name = $account['Name'];
        }
        
        //FETCH DATA FROM DSL_PROXY
        $photos = json_decode($dataProxy['photos'], TRUE);
        $property_photos = $photos['data'][0]['StandardFields']['Photos'];

        $primary = "";

        if (isset($property_photos) && !empty($property_photos)) {
          foreach ($property_photos as $p) {
            if ($p['Primary']) {
              $primary = $p;
            }
          }
        }

        $standardInfo = json_decode($dataProxy['property_standard_info'], TRUE);

        $lat = $standardInfo['data']['Latitude'];
        $long = $standardInfo['data']['Longitude'];

        if ($lat == "********" && $long == "********") {
          $latlong = $this->getlatlon_ajax($standardInfo['data']['UnparsedAddress']);

          $lat = $latlong['lat'];
          $long = $latlong['lon'];

        }

        $data = array(
          'account' => json_decode($dataProxy['account'], TRUE),
          'user_info' => $user_info,
          'username' => $user_name,
          'property_full_spec' => $dataProxy['property_full_spec'],
          'account_photo' => $dataProxy['account_photo'],
          'photos' => json_decode($dataProxy['photos'], TRUE),
          'sml' => $dataProxy['social_media_links'],
          'property_standard_info' => json_decode($dataProxy['property_standard_info'], TRUE),
          'virtual_tours' => json_decode($dataProxy['virtual_tours'], TRUE),
          'photo_gallery' => $photos['data'][0]['StandardFields']['Photos'],
          'primary' => $primary['UriLarge'],
          'lat' => $lat,
          'long' => $long
        );

        //FETCH DATA FROM DSL
       /** $property = $dsl->getProperty();
        $photos = json_decode($property['photos'], TRUE);
        $data = array(
          'account' => json_decode($property['account'], TRUE),
      		'propert_desc' => json_decode($property['property'], TRUE),
      		'account_photo' => $property['account_photo'],
      		'photos' => json_decode($property['photos'], TRUE),
      		'sml' => $property['social_media_links'],
      		'property_standard_info' => json_decode($property['property_standard_info'], TRUE),
      		'virtual_tours' => json_decode($property['virtual_tours'], TRUE),
      		'photo_gallery' => $photos['data'][0]['StandardFields']['Photos']
        );
        **/

        $this->load->view('theme/default/site',$data);

        //added debugger on SPW
        $debug_photos = isset($_GET["debug_photo"])? $_GET["debug_photo"] : "";
        if($debug_photos){
          printA($photos);exit;
        }

        $debug_property = isset($_GET["debug_property"])? $_GET["debug_property"] : "";
        if($debug_property){
          printA($dataProxy);exit;
        }

        if(isset($_GET["debugger"]) && $_GET["debugger"] == TRUE) {
          printA($agent_data);
          printA($body);
          printA($data);exit;
        }
        
      }

    }
    else {
      header("location: ".base_url());
    }
  }
  public function app_test($user_id = NULL, $listing_id = NUll) {


    $config = $this->_get_config($user_id, $listing_id);
    $arrServices = array (
      "property" => "property/".$listing_id."/details/".$user_id,
      "property_standard_info" => "property/118013/standard-fields/".$user_id,
      "virtual_tours" => "property/".$listing_id."/virtual-tours/".$user_id,
      "photos" => "property/".$listing_id."/photos/".$user_id,
      "account" => "agent/".$user_id."/account-info"
    );
    $headers = array(
        'X-Auth-Token: '.$_SESSION['access_token'],
        'X-User-Id: '.$_SESSION['access_id'],
        'Content-Type:application/json'
    );
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $config['connection_url'].$arrServices['property_standard_info']);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER , TRUE);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    $content = curl_exec($ch);
    curl_close($ch);

    echo "<pre>";
    var_dump($config['connection_url'].$arrServices['property_standard_info']);
    var_dump($headers);
    var_dump($content);
    echo "</pre>";
  }
  public function domain_redirect($domain = NULL) {
    if($domain) {
      // $dsl = new Data_layer($login,$config);
    }
    else {
      return false;
    }
  }
  private function _get_config($user_id = NULL, $listing_id = NULL) {
    $config = false;
    if($user_id && $listing_id) {
      $config = array(
        'agent_user_id' => $user_id,
          'listingid' => $listing_id,
          'connection_user' => getenv('DSL_USERNAME'),
          'connection_pass' => getenv('DSL_PASSWORD'),
          'connection_url' => getenv('DSL_SERVER_URL')
          //'connection_url' => "http://dsl-prod-1482448710.us-west-2.elb.amazonaws.com:3000/api/v1"

      );
    }

    return $config;
  }

  private function _get_login($config = false) {
    $login = false;
    if($config) {
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, rtrim(getenv('DSL_SERVER_URL'), '/').'/login/');
    	curl_setopt($ch, CURLOPT_POST, 1);
    	curl_setopt($ch, CURLOPT_RETURNTRANSFER , TRUE);
      curl_setopt($ch, CURLOPT_POSTFIELDS, 'username='.$config['connection_user'].'&password='.$config['connection_pass']);
    	$content = curl_exec($ch);
    	$login = json_decode($content);
    	$conn = $login->status;
    	curl_close($ch);
    }
    else {
      echo "NOPE";
    }
    return $login;
  }
  public function save_schedule_for_showing() {

    if($this->input->is_ajax_request()) {
      
      $response['success'] = FALSE;
      $response['message'] = 'Could not create your account. Please check all fields.';

      if($this->input->post()) {

          $this->form_validation->set_rules("fname", "First Name", "required|trim");
          $this->form_validation->set_rules("lname", "Last Name", "required|trim");
          $this->form_validation->set_rules("email", "Email", "required|trim");
          $this->form_validation->set_rules("phone_signup", "Phone Number", "required|trim");
          $this->form_validation->set_rules("date_scheduled", "Schedule Date", "required|trim");
          $this->form_validation->set_rules("sched_for_showing", "Message", "required|trim");

          if($this->form_validation->run() == FALSE) {

            exit( json_encode($response) );

          } else {
              if(!$this->customer->is_email_exist()) {
                  $save_return = $this->save_to_flexmls($this->input->post());

                  $password = $this->input->post("phone_signup");
                  $type = ($this->input->post('type')) ? $this->input->post('type') : 'spw';

                  $contact_id = (isset($save_return[0]["Id"]) && !empty($save_return[0]["Id"])) ? $save_return[0]["Id"] :  generate_key("10");

                  $data = array(
                    'ip_address'  => $_SERVER['REMOTE_ADDR'],
                          'first_name'  => $this->input->post("fname"),
                          'last_name'   => $this->input->post("lname"),
                          'email'     => $this->input->post("email"),
                          'phone'     => $this->input->post("phone_signup"),
                          'password'    => password_hash($password, PASSWORD_DEFAULT),
                          'active'    => 1,
                          'type'      => $type,
                          'user_id'     => $this->domain_user->id,
                          'agent_id'    => $this->domain_user->agent_id,
                          'contact_id'  => $contact_id,
                          'created'     => date("Y-m-d H:m:s"),
                          'modified'    => date("Y-m-d H:m:s")
                  );
                  $customer_id = $this->customer->sign_up_spw($data);
                  if($customer_id) {
                    $scheduled_for_showing = $this->customer->save_schedule_for_showing_spw($customer_id);
                    if($scheduled_for_showing) {

                      $response['success'] = TRUE;
                      $response['message'] = "Showing has been successfully saved!";
                    
                      // Send email to agent and customer
                      Modules::run('crm/leads/send_leads_notification_to_agent');
                      Modules::run('crm/leads/send_login_credential_to_customer');

                    } else {
                      $response['message'] = "Failed to saved data!";
                    }

                    exit( json_encode($response) );
                    
                  }

              } else {

                   $customer_info = $this->customer->getContactInfo_spw($this->input->post('email'));
                   $customerID =$customer_info->id;
                   $scheduled_for_showing = $this->customer->save_schedule_for_showing_spw($customerID);

                  if($scheduled_for_showing) {

                    $this->sched_send_email_to_agent_spw($this->input->post());

                    $response['success'] = TRUE;
                    $response['message'] = "Showing has been successfully saved!";
                  } else {
                    $response['message'] = "Failed to saved data!";
                  }
              }

              exit( json_encode($response) );
          }
        exit( json_encode($response) );
      }
          
      
    }
  }

  public function capture_leads_signup() {

    if($this->input->is_ajax_request()) {

      $response['success'] = FALSE;
      $response['message'] = 'Could not create your account. Please check all fields.';

      if($this->input->post()) {

        $this->form_validation->set_rules("fname", "First Name", "required|trim");
        $this->form_validation->set_rules("lname", "Last Name", "required|trim");
        $this->form_validation->set_rules("email", "Email", "required|trim|valid_email");
        $this->form_validation->set_rules("phone_signup", "Phone Number", "required|trim");

        if($this->form_validation->run() == FALSE) {

          exit( json_encode($response) );

        } else {

            if(!$this->customer->is_email_exist()) {

              $save_return = $this->save_to_flexmls($this->input->post());

              $password = $this->input->post("phone_signup");
              $type = ($this->input->post("type")) ? $this->input->post("type") : "spw_signup_form";

              $contact_id = (isset($save_return[0]["Id"]) && !empty($save_return[0]["Id"])) ? $save_return[0]["Id"] :  generate_key("10");

              $data = array(
                  'ip_address' => $_SERVER['REMOTE_ADDR'],
                  'first_name' => $this->input->post('fname'),
                  'last_name'  => $this->input->post('lname'),
                  'email'      => $this->input->post('email'),
                  'phone'      => $this->input->post('phone_signup'),
                  'password'   => password_hash($password, PASSWORD_DEFAULT),
                  'active'     => 1,
                  'type'       => $type,
                  'user_id'    => $this->domain_user->id,
                  'agent_id'   => $this->domain_user->agent_id,
                  'contact_id' => $contact_id,
                  'created'    => date("Y-m-d H:m:s"),
                  'modified'   => date("Y-m-d H:m:s")
              );

              $customer_id = $this->customer->sign_up_spw($data);

              if($customer_id) {

                $response['success'] = TRUE;
                $response['message'] = 'Your account has been successfully created.';

                //Send email to customer and agent
                Modules::run('crm/leads/send_leads_notification_to_agent');
                Modules::run('crm/leads/send_login_credential_to_customer');

              } else {

                $response['success'] = FALSE;
                $response['message'] = 'Failed to save data.';
              }

                exit( json_encode($response) );

            } else {

                $response['message'] = 'Email already exists.';
            }

            exit( json_encode($response) );
        }

        exit( json_encode($response) );
      }
    }
  }

  /**
    **Check if user has turned ON or OFF the capture leads sign_up modal 
  **/
  public function checkSpwCaptureLeads() {
    $switch = $this->home_model->get_capture_leads();
    $leads_config = $this->customer->getSpwLeadsConfig();
    exit(json_encode(array('spw_capture_leads' => $switch->spw_capture_leads, 'leads_config' => $leads_config)));
  }

  private function save_to_flexmls($data=array()) {

    $arr = array(
      'DisplayName'   => $data["fname"].' '.$data["lname"],
            'GivenName'   => $data["fname"],
            'FamilyName'  => $data["lname"],
            'PrimaryEmail'  => $data["email"],
      'Notify'    => 1
        );

    $result_contacts = $this->idx_auth->AddContact($arr);

    return $result_contacts;
  }

  public function sched_send_email_to_agent_spw($post = array())
  {
    $this->load->library("email");

    $agent_profile = $this->customer->getAgentInfo();
    $contact_profile = $this->customer->getContactInfo();


    $subs = array(
      'first_name'    => $agent_profile->agent_name,
      'customer_name'   => $contact_profile->contact_name,
      'customer_email'  => $contact_profile->email,
      'customer_phone'  => $contact_profile->phone,
      'property_name'   => $post["property_name"],
      'property_url'    => $post["property_url"],
      'showing_schedule'  => $post['date_scheduled'],
      'lead_message'    => $post['sched_for_showing']
    );
    
    $email = $this->email->send_email_v3(
          'support@agentsquared.com', //sender email
          'AgentSquared', //sender name
          $agent_profile->email, //recipient email,
          'paul@agentsquared.com,aldwinj12345@gmail.com', //bccs
          '99b96223-9ef9-46b6-8c89-2de2803e36e9', //"a5e9a5bf-8c0e-4513-be76-9b94d070d4f2", //template_id
          array('subs'=>$subs, 'reply_to'=>$contact_profile->email)
        );
        
  }



  public function getlatlon_ajax($address) {

      $agent_address = urlencode($address);
      $api_call = 'http://www.datasciencetoolkit.org/street2coordinates/'.$agent_address;
      $api_data = @file_get_contents($api_call);
      $decoded_data = json_decode($api_data, true);
      $data = $decoded_data[$address];

      if(!empty($data)){

        $latlon = array(
          'lat' => $data['latitude'],
          'lon' => $data['longitude'],
        );

        return $latlon;
      }
  }

}
