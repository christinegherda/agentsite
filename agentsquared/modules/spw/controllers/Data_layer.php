<?php
class Data_layer extends MX_Controller {
	protected $connection_data;
	protected $site_config;
	function __construct($data = null, $config = null) {
    parent::__construct();
		$this->connection_data = $data;
		$this->site_config = $config;

		if(!$this->check_connection()) {
			$_SESSION["access_token"] = $this->connection_data->data->authToken;
			$_SESSION["access_id"] = $this->connection_data->data->userId;
		}
	}
	public function check_connection() {
		$status = false;
		if($this->connection_data->status !== 'error' && $this->connection_data->status === 'success') {
			if(!isset($_SESSION["access_token"])) {
				$_SESSION["access_token"] = $this->connection_data->data->authToken;
				$_SESSION["access_id"] = $this->connection_data->data->userId;
			}
			$status = true;
		}
		else {
			$status = false;
		}
		return $status;
	}
	function get_endpoint($endpoint = NULL) {
		if($endpoint) {
            $headers = array(
                'X-Auth-Token: '.$_SESSION['access_token'],
                'X-User-Id: '.$_SESSION['access_id'],
                'Content-Type:application/json'
            );

			$ch = curl_init();
	 		curl_setopt($ch, CURLOPT_URL, $this->site_config['connection_url'].$endpoint);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER , TRUE);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			$content = curl_exec($ch);
			curl_close($ch);
			return $content;
		}
		else {
			return false;
		}
	}
	function getProperty() {
		// http://138.68.10.202:3000/api/v1/property/20161019154149573190000000/details
		if(self::check_connection()) {
			$endpoints = array(
				'property' => 'property/'.$this->site_config['listingid'].'/details/'.$this->site_config['agent_user_id'],
				'property_standard_info' => 'property/'.$this->site_config['listingid'].'/standard-fields/'.$this->site_config['agent_user_id'],
				'virtual_tours' => 'property/'.$this->site_config['listingid'].'/virtual-tours/'.$this->site_config['agent_user_id'],
				'photos' => 'property/'.$this->site_config['listingid'].'/photos/'.$this->site_config['agent_user_id'],
				'account' => 'agent/'.$this->site_config['agent_user_id'].'/account-info',
			);
			$data['account'] = $this->get_endpoint($endpoints['account']);
			$data['property'] = $this->get_endpoint($endpoints['property']);
			$data['property_standard_info'] = $this->get_endpoint($endpoints['property_standard_info']);
			$data['virtual_tours'] = $this->get_endpoint($endpoints['virtual_tours']);
			$data['photos'] = $this->get_endpoint($endpoints['photos']);
			$data['account_photo'] = $this->getAccountDetails();
			$data['social_media_links'] = $this->getSocialMedia();
			return $data;
		}
		else {
			return false;;
		}
	}
	function getSocialMedia() {
		// Create connection
        $conn = new mysqli(
            getenv('DB_HOSTNAME'),
            getenv('DB_USERNAME'),
            getenv('DB_PASSWORD'),
            getenv('DB_NAME')
        );
		// Check connection
		if ($conn->connect_error) {
		    die("Connection failed: " . $conn->connect_error);
		    exit;
		}
		/* Fetch User Data */ // $this->site_config['agent_user_id']
		$sql = "SELECT facebook, twitter, linkedin, googleplus, instagram FROM social_media_links where user_id = ". $this->site_config['agent_user_id'];
		$result = $conn->query($sql);
        return $result->fetch_assoc();
	}
	function getAccountDetails() {
		// Create connection
        $conn = new mysqli(
            getenv('DB_HOSTNAME'),
            getenv('DB_USERNAME'),
            getenv('DB_PASSWORD'),
            getenv('DB_NAME')
        );
		// Check connection
		if ($conn->connect_error) {
		    die("Connection failed: " . $conn->connect_error);
		    exit;
		}

		/* Fetch User Data */ // $this->site_config['agent_user_id']
		$sql = "SELECT agent_photo FROM users where id = ". $this->site_config['agent_user_id'];
		$result = $conn->query($sql);
		$user_data = array();
		if (isset($result->num_rows) && $result->num_rows > 0) {
		    $user_data = $result->fetch_assoc();
		}
		return ($user_data['agent_photo']) ? $user_data['agent_photo'] : false;
	}
}
