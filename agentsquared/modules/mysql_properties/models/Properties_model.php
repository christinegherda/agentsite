<?php if( ! defined('BASEPATH') ) exit('No direct script access allowed');

class Properties_model extends CI_Model { 

	public function __construct() {
        parent::__construct();

        $this->db_slave =  $this->load->database('db_slave', TRUE);
    }
	
    public function get_account($user_id= NULL, $type=NULL){

        if($user_id && $type) {

            $this->db_slave->select("account")
                    ->from("account_meta")
                    ->where("type", $type)
                    ->where("user_id",$user_id)
                    ->limit(1);

            $query = $this->db_slave->get()->row();

            return ($query) ? $query : FALSE;
        }
    }

	/**
	 * Update for DEV - 2418
	 * Reason: Need to determine if the returned listings are more than
	 * four (4) rows and display a "View All" link in the homepage
	 *
	 * Sol.: Add a count all rows query into the select field
	 *
	 * @param null $user_id
	 * @param null $type
	 * @return array|bool
	 */
	public function get_property_new($user_id=NULL, $type=NULL) {

		if ( NULL === $user_id && NULL === $type)
		{
			return TRUE;
		}

		/**
		 * default limit to 5, the quickest way to figure out
		 * if a "View All" link in the homepage is to be displayed
		 */
		$limit = $type === 'sold' ? 8 : 5;

		$db = $this->db_slave;
		$where = [
			'type' => $type,
			'user_id' => $user_id,
			'listing_id !=' => ''
		];

		$db->select("listing")
			->from("homepage_properties")
			->where($where)
			->order_by("date_sold ", "desc")
			->limit($limit);

		return $db->get()->result();
	}
    
    public function get_property_old($user_id=NULL, $type=NULL) {

        if($user_id && $type) {

            $this->db_slave->select("listings")
                    ->from("homepage_properties")
                    ->where("type", $type)
                    ->where("user_id", $user_id)
                    ->limit(1);

            $query = $this->db_slave->get()->row();

            return ($query) ? $query : FALSE;
        }
    }

    public function check_listing($user_id=NULL, $type=NULL) {

        if($user_id && $type) {

            $this->db_slave->select("listing")
                    ->from("homepage_properties")
                    ->where("type", $type)
                    ->where("user_id", $user_id)
                    ->where("listing_id !=", "")
                    ->limit(1);

            $query = $this->db_slave->get()->row();

            return ($query) ? $query : FALSE;
        }
    }

     public function home_options($agent_id=NULL, $type=NULL) {

        if($agent_id && $type) {

            $this->db_slave->select("option_value")
                    ->from("home_options")
                    ->where("option_name", $type)
                    ->where("agent_id", $agent_id)
                    ->limit(1);

            $query = $this->db_slave->get()->row();

            return ($query) ? $query : FALSE;
        }
    }

     public function featured_options($agent_id=NULL) {

        if($agent_id) {

            $this->db_slave->select("option_title")
                    ->from("home_options")
                    ->where("is_featured", 1)
                    ->where("agent_id", $agent_id)
                    ->limit(1);

            $query = $this->db_slave->get()->row();

            return ($query) ? $query : FALSE;
        }
    }

    public function get_subscription($user_id=NULL) {

        if($user_id) {

            $this->db_slave->select("plan_id")
                    ->from("freemium")
                    ->where("subscriber_user_id", $user_id)
                    ->where("plan_id", 2)
                    ->limit(1);

            $query = $this->db_slave->get()->row();

            return ($query) ? $query : FALSE;
        }
    }
}