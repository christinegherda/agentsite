<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Mysql_properties extends MX_Controller {

	public function __construct() {

		parent::__construct();

		$this->load->model("properties_model", "pm");
		$this->load->model("dsl/dsl_model", "dsl");
		//$this->output->enable_profiler(TRUE);
	}

	public function get_account($user_id =NULL, $type=NULL){

		if($type && $user_id) {
	   		$account_json = $this->pm->get_account($user_id, $type);
	   		if(!empty($account_json)) {
	   			$account_data = gzdecode($account_json->account);
	   			$account_data = json_decode($account_data);
	   			return $account_data;
	   		}
	   		
	   		
	  	}

	}

	public function get_nearby_coordinates($user_id=NULL) {

		if($user_id) {

			$flag = FALSE;

			$lat="";
			$lon="";

			$status = array('active', 'office', 'new');

			$i=0;

			while($i<count($status)) {

				$prop = $this->get_properties($user_id, $status[$i]);

				$listing_checker = $this->pm->check_listing($user_id, $status[$i]);

				if(!empty($prop)) {

					if(!empty($listing_checker)){

							$pos = strpos($prop->StandardFields->Latitude, '*');
							$pos1 = strpos($prop->StandardFields->Longitude, '*');

							if($pos===FALSE && $pos1===FALSE) {

								$lat = $prop->StandardFields->Latitude;
								$lon = $prop->StandardFields->Longitude;
								$flag = TRUE;

								break;
							}

					} else {

						for($x=0; $x<count($prop); $x++) {

							$pos = strpos($prop[$x]->StandardFields->Latitude, '*');
							$pos1 = strpos($prop[$x]->StandardFields->Longitude, '*');

							if($pos===FALSE && $pos1===FALSE) {

								$lat = $prop[$x]->StandardFields->Latitude;
								$lon = $prop[$x]->StandardFields->Longitude;
								$flag = TRUE;

								break;

							}
						}

					}

				}

				if($flag)
					break;

				$i++;
			}

			if($flag) {

				return array('lat'=>$lat, 'lon'=>$lon);

			} else {

				$address = "";

				for($j=0; $j<count($status); $j++) {

					$prop = $this->get_properties($user_id, $status[$j]);
					$listing_checker = $this->pm->check_listing($user_id, $status[$j]);

					if(!empty($prop)) {

						if(!empty($listing_checker)){

							$pos = strpos($prop->StandardFields->UnparsedAddress, '*');

							if($pos===FALSE) {

								$address = $prop->StandardFields->UnparsedAddress;
								$flag = TRUE;

								break;
							}

						} else {

							for($x=0; $x<count($prop); $x++) {

								$pos = strpos($prop[$x]->StandardFields->UnparsedAddress, '*');

								if($pos===FALSE) {

									$address = $prop[$x]->StandardFields->UnparsedAddress;
									$flag = TRUE;

									break;

								}

							}

						}
					}

					if($flag)
						break;

				}

				$agent_address = urlencode($address);
				$api_call = 'http://www.datasciencetoolkit.org/street2coordinates/'.$agent_address;
				$api_data = @file_get_contents($api_call);
				$decoded_data = json_decode($api_data, true);
				$data = $decoded_data[$address];

				if(!empty($data)){

					return array('lat'=> $data['latitude'], 'lon'=> $data['longitude']);

				}
			}

		}

	}
	public function listings_checker($user_id=NULL, $type=NULL){

		$listing_checker = $this->pm->check_listing($user_id, $type);

		return !empty($listing_checker) ? 1 : 0;
	}

	/**
	 * Updated for DEV - 2418
	 * Need to determine if the returned listings are more than
	 * four (4) rows and display a "View All" link in the homepage
	 *
	 * @param null $user_id
	 * @param null $type
	 * @return array|mixed|string
	 */
	public function get_properties($user_id=NULL, $type=NULL)
	{
		//get data from listing(TEXT) column
		$listing_json = $this->pm->get_property_new($user_id, $type);
		$listings_data = array();

		if ( ! empty($listing_json) )
		{
			foreach($listing_json as $listing) {
				$listings_data[] = json_decode($listing->listing);
			}
		}
		else
		{
			//get data from listings(BLOB) column
			$listings_json = $this->pm->get_property_old($user_id, $type);

			if ( !empty($listings_json) ) {
				$listings_data = gzdecode($listings_json->listings);
				$listings_data = json_decode($listings_data);
			}
		}

		return $listings_data;
	}

	public function get_properties_old($user_id=NULL, $type=NULL) {

		if($type) {

			$listings_json = $this->pm->get_property_old($user_id, $type);

			if(!empty($listings_json)) {
				$listings_data = gzdecode($listings_json->listings);
				$listings_data = json_decode($listings_data);
				return $listings_data;
			}
				
		}

	}


	public function get_office($user_id=NULL, $type=NULL) {

		if($type) {

			$listing_checker = $this->pm->check_listing($user_id, $type);

			if(!empty($listing_checker)){

				//get data from listing(TEXT) column
				$listings_json = $this->pm->get_property_new($user_id, $type);

				$listings_data = array();

				if(!empty($listings_json)){
					foreach($listings_json as $listing){
						$listings_data[] = json_decode($listing->listing, true);
					}

					return $listings_data;	
				}

			} else {

				//get data from listings(BLOB) column
				$listings_json = $this->pm->get_property_old($user_id, $type);

				if(!empty($listings_json)) {
					$listings_data = gzdecode($listings_json->listings);
					$listings_data = json_decode($listings_data, true);

					return $listings_data;
				}
			}	
		}

	}
	public function get_office_old($user_id=NULL, $type=NULL) {

		if($type) {

			$listings_json = $this->pm->get_property_old($user_id, $type);

			if(!empty($listings_json)) {
				$listings_data = gzdecode($listings_json->listings);
				$listings_data = json_decode($listings_data, true);
				return $listings_data;
			}	
		}
	}

	public function home_options($agent_id=NULL, $type=NULL){

		$selected_options = $this->pm->home_options($agent_id, $type);

		return $selected_options->option_value;
	}

	public function featured_options($agent_id=NULL){

		$featured_options = $this->pm->featured_options($agent_id);

		return !empty($featured_options->option_title) ? $featured_options->option_title : "" ;
	}

	public function freemium_subscription($user_id=NULL){

		$subscription = $this->pm->get_subscription($user_id);

		return !empty($subscription) ? $subscription : "" ;
	}


}