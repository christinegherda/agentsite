<?php 
    $this->load->view('session/header'); 
    $this->load->view('session/top-nav');
?> 
        <div class="content-wrapper">
          <div class="page-title">
            <h3 class="heading-title">
              <span>Active Listings</span>
            </h3>
          </div>
            <section class="content">
                <?php $this->load->view('session/launch-view'); ?>
                <div class="row">
                    <div class="featured-list">
                        <?php
                            foreach($active_listings as $active) {
                            $url_rewrite = url_title("{$active->StandardFields->UnparsedFirstLineAddress} {$active->StandardFields->PostalCode}");
                        ?>
                        <div class="col-md-4 col-lg-3 col-sm-6">
                            <div class="featured-list-item">
                                <div class="featured-list-image">
                                    <?php if(isset($active->StandardFields->Photos[0]->Uri300)) { ?>
                                        <a href="<?=base_url('property-details/');?><?php echo $url_rewrite ?>/<?php echo $active->StandardFields->ListingKey;?>" target="_blank"><img src="<?=$active->StandardFields->Photos[0]->Uri300?>" alt="<?php echo $active->StandardFields->UnparsedFirstLineAddress; ?>" class="img-responsive" style="width:100%;"></a>
                                        
                                     <?php } else { ?>

                                        <a href="<?=base_url('property-details/');?><?php echo $url_rewrite ?>/<?php echo $active->StandardFields->ListingKey;?>" target="_blank"><img src="<?=base_url()?>assets/images/image-not-available.jpg" alt="<?php echo $active->StandardFields->UnparsedFirstLineAddress; ?>" class="img-responsive" style="width:100%;"></a>

                                     <?php } ?>

                                </div>
                                <div class="property-quick-icons">
                                    <ul class="list-inline">
                                        <?php
                                            if($active->StandardFields->BedsTotal && is_numeric($active->StandardFields->BedsTotal)) {
                                        ?>
                                        <li><i class="fa fa-bed"></i> <?=$active->StandardFields->BedsTotal?> Bed</li>
                                        <?php
                                            }
                                            if($active->StandardFields->BathsTotal && is_numeric($active->StandardFields->BathsTotal)) {
                                        ?>
                                        <li><i class="icon-toilet"></i> <?=$active->StandardFields->BathsTotal?> Bath</li>
                                        <?php
                                            }
                                        ?>
                                    </ul>
                                </div>
                                <div class="property-listing-description">
                                    <h5><a href="<?=base_url('property-details/');?><?php echo $url_rewrite ?>/<?php echo $active->StandardFields->ListingKey;?>"><?php echo $active->StandardFields->UnparsedFirstLineAddress; ?></a></h5>
                                    <p><i class="fa fa-map-marker"></i>
                                        <?php 
                                            if(strpos($active->StandardFields->CountyOrParish, '*'))
                                                echo $active->StandardFields->CountyOrParish . " " . $active->StandardFields->StateOrProvince . ", " . $active->StandardFields->PostalCode;
                                            else
                                                echo $active->StandardFields->PostalCity . " " . $active->StandardFields->StateOrProvince . ", " . $active->StandardFields->PostalCode;
                                        ?>
                                    </p> 
                                    <p><i class="fa fa-usd"></i> <?=number_format($active->StandardFields->CurrentPrice)?></p>
                                    <a href="<?=base_url('property-details/');?><?php echo $url_rewrite ?>/<?php echo $active->StandardFields->ListingKey;?>" target="_blank" class="btn btn-default btn-submit"><i class="fa fa-eye"></i> View Property</a>
                                </div>
                            </div>
                        </div>
                            <?php
                                }
                            ?>
                    </div>
                </div>      
            </section>
        </div>
<?php $this->load->view('session/footer'); ?>
