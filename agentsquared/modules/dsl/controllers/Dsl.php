<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
	Data Service Layer Endpoint Controller
	Fetching Data's from DSL is done here.
*/
class Dsl extends MX_Controller {

	private $authToken;
	private $userId;
	public $login_status = false;
	protected $domain_user;

	public function __construct() {
 		parent::__construct();

 		$this->domain_user = domain_to_user();
		$this->load->model("dsl_model");
 	}

 	public function login() {

 		$status = false;
 		$dsl = array(
 			'username' => getenv('DSL_USERNAME'), 
 			'password' => getenv('DSL_PASSWORD')
 		);

 		//init curl
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, rtrim(getenv('DSL_SERVER_URL'), '/').'/login/');
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER , TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, 'username='.$dsl['username'].'&password='.$dsl['password']);
		$content = curl_exec($ch);
        curl_close($ch);
		$login = json_decode($content);

		if(!empty($login) && $login->status == 'success') {
			$this->session->set_userdata(
				array(
					'login_status' 	=> true,
					'authToken' 	=> $login->data->authToken,
					'userId' 		=> $login->data->userId
					)
			);
			$status = true;
		}

		return $status;
 	}
 	public function getSparkProxy($method = 'listings', $params = '', $decode = false) {

		$call_url = 'https://sparkapi.com/v1/'.$method.'?'.$params;
		$tokenType = $this->dsl_model->getUserAcessType($this->domain_user->agent_id);
		$token = $this->dsl_model->getUserAccessToken($this->domain_user->agent_id);
		$header = array('Authorization: OAuth '.$token->access_token, 'Accept-Encoding: gzip, deflate', 'X-SparkApi-User-Agent: SparkAPIExamples', 'Content-Type:application/json');
		if($tokenType) {
			// bearer_token
			$header = array('Authorization: Bearer '.$token->access_token, 'Accept-Encoding: gzip, deflate', 'X-SparkApi-User-Agent: MyApplication' , 'Content-Type:application/json');
		} 

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_URL, $call_url);

		$this->dsl_model->log_request();
		$this->dsl_model->log_request_history($call_url, 'PROXY', $params);

		$content = curl_exec($ch);
    curl_close($ch);
    $unzipped_data = gunzip($content);
		$json_data = json_decode($unzipped_data,$decode);

		return isset($json_data->D->Results) ? $json_data->D->Results: null;
 	}
 	public function getSparkProxyPagination($page = 1, $limit = 10, $filter='', $decode = false, $order='',$param_select='') {

		$filterStr = "&_filter=(".$filter.")";
		$order = (!empty($order)) ? "&_orderby=".$order : "";

		$_select = (!empty($param_select)) ? "&_select=".$param_select : "";
		$call_url = 'https://sparkapi.com/v1/listings?_limit='.$limit.'&_page='.$page.'&_expand=PrimaryPhoto&_pagination=1'.$filterStr.$order.$_select;
		$tokenType = $this->dsl_model->getUserAcessType($this->domain_user->agent_id);
		$token = $this->dsl_model->getUserAccessToken($this->domain_user->agent_id);
		$header = array('Authorization: OAuth '.$token->access_token, 'Accept-Encoding: gzip, deflate', 'X-SparkApi-User-Agent: SparkAPIExamples', 'Content-Type:application/json');
		if($tokenType) {
			// bearer_token
			$header = array('Authorization: Bearer '.$token->access_token, 'Accept-Encoding: gzip, deflate', 'X-SparkApi-User-Agent: MyApplication' , 'Content-Type:application/json');
		}

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_URL, $call_url);

		$this->dsl_model->log_request();
		$this->dsl_model->log_request_history($call_url, 'PROXY', $filterStr);

		$content = curl_exec($ch);
    curl_close($ch);
		$unzipped_data = gunzip($content);
		$json_data = json_decode($unzipped_data,$decode);

		return isset($json_data->D) ? $json_data->D: $json_data;
 	}
  	public function getSparkProxyComplete($method = 'listings', $params = '', $decode = false) {

		$call_url = 'https://sparkapi.com/v1/'.$method.'?'.$params;
		$tokenType = $this->dsl_model->getUserAcessType($this->domain_user->agent_id);
		$token = $this->dsl_model->getUserAccessToken($this->domain_user->agent_id);
		$header = array('Authorization: OAuth '.$token->access_token, 'Accept-Encoding: gzip, deflate', 'X-SparkApi-User-Agent: SparkAPIExamples', 'Content-Type:application/json');
		if($tokenType) {
			// bearer_token
			$header = array('Authorization: Bearer '.$token->access_token, 'Accept-Encoding: gzip, deflate', 'X-SparkApi-User-Agent: MyApplication' , 'Content-Type:application/json');
		}

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_URL, $call_url);

		$this->dsl_model->log_request();
		$this->dsl_model->log_request_history($call_url, 'PROXY', $params);

		$content = curl_exec($ch);
    curl_close($ch);
    $unzipped_data = gunzip($content);
		$json_data = json_decode($unzipped_data,$decode);

		return isset($json_data->D) ? $json_data->D: $json_data;
 	}

 	public function logout() {
 		$array_items = array('login_status', 'authToken', 'userId','user_id','agent_id');
 		$this->session->unset_userdata($array_items);
 		return 1;
 	}

 	/* Returns JSON String of Agent Information */
 	public function get_endpoint($endpoint = NULL) {
 		
 		if($endpoint) {
 			$call_url = rtrim(getenv('DSL_SERVER_URL'), '/')."/".$endpoint; #'agent/'.$this->domain_user->id.'/system-info';

	 		$ch = curl_init();
	 		curl_setopt($ch, CURLOPT_URL, $call_url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER , TRUE);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-Auth-Token: '.$this->session->userdata('authToken'), 'Accept-Encoding: gzip, deflate', 'X-User-Id: '.$this->session->userdata('userId'), 'Content-Type:application/json'));
			$content = curl_exec($ch);
      curl_close($ch);

			if(strpos($endpoint, 'spark-')) {
				$this->dsl_model->log_request();
				$this->dsl_model->log_request_history($endpoint, 'GET', '');
			}

			$unzipped_data = gunzip($content);
			return $unzipped_data;
 		}
 		else {
 			echo 0;
 		}
 	}

 	/* POST Type */
 	public function post_endpoint($endpoint = NULL, $body=array()) {

 		if($endpoint) {

 			$call_url = rtrim(getenv('DSL_SERVER_URL'), '/')."/".$endpoint;

 			$headers = array(
	    		'Content-Type: application/json',
	    		'X-Auth-Token: '.$this->session->userdata('authToken'),
	    		'X-User-Id: '.$this->session->userdata('userId'),
	    		'Accept-Encoding: gzip, deflate',
	    	);

 			$body_json = json_encode($body, true);

 			$ch = curl_init();
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_TIMEOUT, 0);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $body_json);
			curl_setopt($ch, CURLOPT_URL, $call_url);

			$result_info  = curl_exec($ch);
			//$result_info = json_decode($result, true);

			if(!$result_info) {
				$result_info = curl_error($ch);
			}
      curl_close($ch);

			if(strpos($endpoint, 'spark-')) {
				$this->dsl_model->log_request();
				$this->dsl_model->log_request_history($endpoint, 'POST', $body);
			}

			$unzipped_data = gunzip($result_info);
			return $unzipped_data;
 		}
 	}

 	public function get_config() {
 		$config = array(
 			'agent_id' => $this->domain_user->agent_id,
 			'user_id' =>$this->domain_user->id,
 		);

 		return $config;
 	}

 	public function getAgentActivePropertyListings( $limit = FALSE , $status = "Active,Contingent,Pending" ) {

 		$endpoint = "properties/".$this->domain_user->id."/status/".$status."?_limit=".$limit;

 		if( !$limit) {
 			$endpoint = "properties/".$this->domain_user->id."/status/".$status;
 		}


		$active_listings = $this->get_endpoint($endpoint);
		$activeListingData = json_decode($active_listings);
		$activeList = array();

		if( !empty($activeListingData)){
			if(property_exists($activeListingData, 'data') ) {
				foreach ($activeListingData->data as $active){
			 		$activeList[] = $active->standard_fields[0];
			 	}
			}
		}
		echo "<pre>";
		var_dump($activeList);
		echo "</pre>";
		return $activeList; 
	}

	public function getAgentSoldPropertyListings($limit = FALSE) {

		$isCheck = $this->getStandardMlsStatus();		
		$soldList = array();		

		if( $isCheck )
		{
			$endpoint = "properties/".$this->domain_user->id."/sold"."?_limit=".$limit;
			if( !$limit)
			{
				$endpoint = "properties/".$this->domain_user->id."/sold";
			}
			$sold_listings =$this->get_endpoint($endpoint);
			$soldListingData = json_decode($sold_listings);

			if( !empty($soldListingData)){
				if(property_exists($soldListingData, 'data') ) {
				 	foreach ($soldListingData->data as $sold){
				 		$soldList[] = $sold->standard_fields[0];
				 	}
				}
			}

			return $soldList; 
		}
		
		return $soldList; 
	}

 	public function getOfficeListings() {

		$token = $this->dsl_model->getUserAccessToken($this->domain_user->agent_id);
		$access_token = array(
			'access_token' => $token->access_token,
			'bearer_token' => 'asdf1234'
		);
		$filterStr = "(MlsStatus Eq 'Active' Or MlsStatus Eq 'Pending' Or MlsStatus Eq 'Contingent')";
		$filter_url = rawurlencode($filterStr);
		$filter_string = substr($filter_url, 3, -3);

		$endpoint = "office/listings?_limit=4&_expand=PrimaryPhoto&_filter=(".$filter_string.")";
		//$endpoint = "spark-listings?_limit=4&_expand=PrimaryPhoto&_filter=(".$filter_string.")";
		$office_listings = $this->post_endpoint($endpoint,$access_token);
		$officelistingData = json_decode($office_listings,true);
		$officeList = array();

		if( isset($officelistingData['data']) && !empty($officelistingData)){

			foreach ($officelistingData['data'] as $office){
				$officeList[] = $office;
			}
		}
		var_dump($officeList);
		return $officeList; 
	}
	
 	public function getNewListings() {

		$endpoint = "new-listings/".$this->domain_user->id."/?limit=4";
		$new_listings = $this->get_endpoint($endpoint);
		$newlistingData = json_decode($new_listings);
		$newList = array();

		if( !empty($newlistingData)){
			if(property_exists($newlistingData, 'data')) {
				foreach ($newlistingData->data as $new){
			 		$newList[] = $new;
			 	}
			}
		 }
		return $newList; 
	}
	

 	public function getNewListingsProxy() {
 		
		$token = $this->dsl_model->getUserAccessToken($this->domain_user->agent_id);
		$access_token = array(
			'access_token' => $token->access_token,
			'bearer_token' => 'asdf1234'
		);
		
		$endpoint = "spark-listings?_limit=4&listing_change_type%5B%5D=New+Listing";
		$new_listings = $this->post_endpoint($endpoint,$access_token);
		$newlistingData = json_decode($new_listings);
		#$newList = array();

		// if( !empty($newlistingData)){

		// 	foreach ($newlistingData['data'] as $new){
		// 		$newList[] = (O$new;
		// 	}
		// }
		$newList = property_exists($newlistingData, 'data') ? $newlistingData->data: false;
		echo "<pre>";
		var_dump($newList);
		echo "</pre>";
		return $newList;
	}

	public function getNearbyListings($datas = array()) { 

		$endpoint = "nearby-listings/".$this->domain_user->id."?limit=4&_lat=".$datas["lat"]."&_lon=".$datas["lon"]."";
		$nearby_listings = $this->get_endpoint($endpoint);

		if($datas["lat"] != "********" && $datas["lon"] != "********"){

			$nearbylistingData = json_decode($nearby_listings);
			$nearbyList = array();

			if( !empty($nearbylistingData)){
				if(property_exists($nearbylistingData, 'data')) {
					foreach ($nearbylistingData->data as $nearby){
				 		$nearbyList[] = $nearby;
				 	}
				}
			 	
			 }
		}
		return $nearbyList; 
	}

	public function getNearbyListingsProxy( $datas = array() ) { 

		$token = $this->dsl_model->getUserAccessToken($this->domain_user->agent_id);
		$access_token = array(
				'access_token' => $token->access_token
		);

		$filterStr = "(MlsStatus Eq 'Active' Or MlsStatus Eq 'Pending' Or MlsStatus Eq 'Contingent')";
		$filter_url = rawurlencode($filterStr);
		$filter_string = substr($filter_url, 3, -3);

		$nearby_listings = $this->post_endpoint('spark-nearby-listings?_limit=4&_expand=PrimaryPhoto&_filter=('.$filter_string.')&_lat='.$datas["lat"].'&_lon='.$datas["lon"].'', $access_token);
			
		if($datas["lat"] != "********" && $datas["lon"] != "********"){

			$nearby_proxy = json_decode($nearby_listings);
			$nearbyList = array();

			if( !empty($nearby_proxy)){
				if(property_exists($nearby_proxy,'data')) {
				 	foreach ($nearby_proxy->data as $nearby){
				 		$nearbyList[] = $nearby;
				 	}
				}
			}
		}
		return $nearbyList; 
	}

	public function getAgentSavedSearch( $param = array() ) {

		$token = $this->dsl_model->getUserAccessToken($this->domain_user->agent_id);
		$access_token =  $token->access_token;

		$endpoint = "savedsearches/".$this->domain_user->id."";

		$page = ($param["page"] / $param["limit"]) + 1;

		$page = ( $page >= 2 ) ? $page: 1;
		$param = array(
			'_pagination' => $page,
			'_limit' => $param["limit"],
			'access_token' => $access_token
		);

		$saved_search = $this->post_endpoint($endpoint, $param);
		
		$savedSearchData = json_decode($saved_search,true);

		return $savedSearchData; 
	}

	public function getAllAgentSavedSearchFromSpark() {

		$token = $this->dsl_model->getUserAccessToken($this->domain_user->agent_id);
		$access_token =  $token->access_token;

		$param = array(
			'_pagination' => 1,
			'_limit' => 24,
			'access_token' => $access_token
		);

		$endpoint = "savedsearches/".$this->domain_user->id;
		
		$saved_search = $this->post_endpoint($endpoint, $param);
		
		$savedSearchData = json_decode($saved_search,true);

		return $savedSearchData; 
	}

	public function getAgentAccountInfo() {

		$endpoint = "agent/".$this->domain_user->id."/account-info";
		$account_info = $this->get_endpoint($endpoint);
		$content = json_decode($account_info);
		
		return $content;
	}

	public function getSystemInfo() {

		$endpoint = "agent/".$this->domain_user->id."/system-info";
		$system_info = $this->get_endpoint($endpoint);
		$content = json_decode($system_info);

		return $content;
	}

	public function get_property_types() {

		//$endpoint = "agent/".$this->domain_user->id."/property-types";
		$endpoint = "spark-agent-property-types/".$this->domain_user->id;
		$property_types = $this->get_endpoint($endpoint);
		$content = json_decode($property_types, true);
		
		return ($content["status"] == "success" && !empty($content["data"])) ? $content["data"] : FALSE;

	}

	public function get_property_subtypes() {

		$endpoint = "agent/".$this->domain_user->id."/property-subtypes";
		$property_subtypes = $this->get_endpoint($endpoint);
		$content = json_decode($property_subtypes, true);

		return (!empty($content["data"]) && (isset($content["data"]["FieldList"]) && !empty($content["data"]["FieldList"]))) ? $content["data"]["FieldList"] : FALSE;
	}
	
	public function getSelectedAgentSavedSearch($count = FALSE, $limit = 0, $offset = 0) {

		if($limit) {
			$endpoint = "savedsearches/".$this->domain_user->id.'?_isSelected=TRUE&_limit='.$limit.'&offset='.$offset;
		} else {
			$endpoint = "savedsearches/".$this->domain_user->id.'?_isSelected=TRUE';
		}

		$saved_search = $this->get_endpoint($endpoint);
		$savedSearchData = json_decode($saved_search,true);

		if($count){
			return isset($savedSearchData['count']) ? $savedSearchData['count'] : 0;
		}

		return isset($savedSearchData['data']) ? $savedSearchData['data']['saved_searches'] : $savedSearchData;

	}
	public function getFeaturedAgentSavedSearch( $limit = FALSE ) {

		$endpoint = "savedsearches/".$this->domain_user->id."?_limit=".$limit."&isFeatured=true";

		if( !$limit )
		{
			$endpoint = "savedsearches/".$this->domain_user->id;
		}
		
		$saved_search = $this->get_endpoint($endpoint);
		$savedSearchData = json_decode($saved_search,true);
		$featuredSavedSearch = array();

		if( isset($savedSearchData['data']) && !empty($savedSearchData)){

		 	foreach ($savedSearchData['data']['saved_searches'] as $featured){
		 		if(isset($featured['isFeatured'])){
		 			if($featured['isFeatured'] == 1){
		 				$featuredSavedSearch[] = $featured;
		 			}
		 		}
		 	}
		 }
		 
		return $featuredSavedSearch; 
	}

	public function syncSavedSearches() { 

		$token = $this->dsl_model->getUserAccessToken($this->domain_user->agent_id);
		$access_token = array(
				'access_token' => $token->access_token
		);

		$this->post_endpoint('sync-saved-searches', $access_token);

		if(!empty($this->getAllAgentSavedSearch())){

			redirect('agent_sites/saved_searches');
		} else {

			redirect('agent_sites/saved_searches');
		}
		
	}
  
  public function getListings() { 

		$token = $this->dsl_model->getUserAccessToken($this->domain_user->agent_id);
		$access_token = array(
			'access_token' => $token->access_token
		);

		$listing_data = $this->post_endpoint('spark-listings');
		$data = json_decode($listing_data, true);
		return $data['data'];
		
	}
  
	public function getListing( $id = NULL, $param_select ="") { 

		$token = $this->dsl_model->getUserAccessToken($this->domain_user->agent_id);
		$access_token = array(
				'access_token' => $token->access_token
		);

		$select = (!empty($param_select)) ? "?_select=".$param_select : "";
		$listing_data = $this->post_endpoint('spark-listing/'.$id.'/standard-fields'.$select.'', $access_token);
		$data = json_decode($listing_data, true);
		return $data['data'];
		
	}

	public function getListingPhotos( $id = NULL, $param_select ="") { 

		$token = $this->dsl_model->getUserAccessToken($this->domain_user->agent_id);
		$access_token = array(
				'access_token' => $token->access_token
		);

		$select = (!empty($param_select)) ? "?_select=".$param_select : "";
		$listing_photo = $this->post_endpoint('spark-listing/'.$id.'/photos'.$select.'', $access_token);
		$data = json_decode($listing_photo, true);
		return $data['data'];
		
	}

	public function getStandardMlsStatus( $type = NULL ){

		$endpoint = "agent/".$this->domain_user->id."/mls-status";
		$mlsstatus = $this->get_endpoint($endpoint);
		$result_info = json_decode($mlsstatus, true);
		
		if(!$result_info) {
			return FALSE;
		}
		
		$flag =  FALSE;

		foreach ($result_info["data"]["0"]["MlsStatus"]["FieldList"] as $key ) {
			if( $key["StandardStatus"] == "Closed" || $key["StandardStatus"] == "Sold" ){

				return TRUE;

				break;
			}
		}

		return $flag;

	}

	public function getMlsStatusFromSpark( $agent_id = NULL ){

		$call_url = 'https://sparkapi.com/v1/standardfields/MlsStatus';
		$tokenType = $this->dsl_model->getUserAcessType($agent_id);
		$token = $this->dsl_model->getUserAccessToken($agent_id);

		$header = array('Authorization: OAuth '.$token->access_token, 'Accept-Encoding: gzip, deflate', 'X-SparkApi-User-Agent: SparkAPIExamples', 'Content-Type:application/json');
		if($tokenType) {
			// bearer_token
			$header = array('Authorization: Bearer '.$token->access_token, 'Accept-Encoding: gzip, deflate', 'X-SparkApi-User-Agent: MyApplication' , 'Content-Type:application/json');
		}

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_URL, $call_url);

		$content = curl_exec($ch);
    curl_close($ch);
    $unzipped_data = gunzip($content);
		$json_data = json_decode($unzipped_data,$decode);

		return isset($json_data->D->Results) ? $json_data->D->Results: null;
	}

	public function getMlsStatus( $agent_id = NULL ){

		$jsOn_mlsStatus =  $this->getMlsStatusFromSpark($agent_id);
		$mlsStatusFieldsList = $jsOn_mlsStatus[0]->MlsStatus->FieldList;

		if($jsOn_mlsStatus[0]->MlsStatus->Searchable) {
			$statuses = array();
			foreach($mlsStatusFieldsList as $mfl) {
				$statuses[] = $mfl->Name;
			}
			return $statuses;
		}
		else {
			return false;
		}
	}

  	public function getBrokerFromSpark($method = 'account') {

		$call_url = 'https://sparkapi.com/v1/'.$method;
		$tokenType = $this->dsl_model->getUserAcessType($this->domain_user->agent_id);
		$token = $this->dsl_model->getUserAccessToken($this->domain_user->agent_id);
		$header = array('Authorization: OAuth '.$token->access_token, 'Accept-Encoding: gzip, deflate', 'X-SparkApi-User-Agent: SparkAPIExamples', 'Content-Type:application/json');
		if($tokenType) {
			// bearer_token
			$header = array('Authorization: Bearer '.$token->access_token, 'Accept-Encoding: gzip, deflate', 'X-SparkApi-User-Agent: MyApplication' , 'Content-Type:application/json');
		}

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_URL, $call_url);

		$this->dsl_model->log_request();
		$this->dsl_model->log_request_history($call_url, 'PROXY');

		$content = curl_exec($ch);
    curl_close($ch);
    $unzipped_data = gunzip($content);
		$json_data = json_decode($unzipped_data, TRUE);

		return isset($json_data['D']) ? $json_data['D']: null;
 	}



	public function getBrokerData( $officeId = NULL ){

		/*** Get office id ***/ 
		$endpoint = "my/accounts/".$officeId;
		$endpoint1 = "accounts/".$officeId;
		$accountData = $this->getBrokerFromSpark($endpoint1);

		/*** Get Broker's Data ***/
		$officeBrokerKey = $accountData['Results']['0']['OfficeBrokerKey'];
		$endpoint2 = "accounts/".$officeBrokerKey;
		$brokerage = $this->getBrokerFromSpark($endpoint2);

		return $brokerage;
	}



}
