<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['sitemap.xml'] = 'sitemap';
$route['sitemap-pages.xml'] = 'sitemap/pages';
$route['sitemap-savedsearches-(:num).xml'] = 'sitemap/savedsearches/$1';
$route['sitemap-sold-properties-(:num).xml'] = 'sitemap/sold_properties/$1';
$route['sitemap-office-listings-(:num).xml'] = 'sitemap/listings/office/$1';
$route['sitemap-active-listings-(:num).xml'] = 'sitemap/properties/$1';
$route['sitemap-new-listings-(:num).xml'] = 'sitemap/listings/new/$1';
$route['home_listings-(:num).xml'] = 'sitemap/home_listings/$1';

?>