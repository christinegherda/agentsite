<?php

function getPropertyType($propertyType = ''){
  switch ($propertyType) {
    case 'Land':
      return 'land';
    case 'Residential':
      return 'house';
    default:
      return 'other';
  }
}
echo '<?xml version="1.0" encoding="utf-8"?>'."\n";
?>
<listings>
  <title><?php echo $listings[0]->StandardFields->StateOrProvince; ?> Feed</title>
  <link rel="self" href="<?php echo base_url('/home_listings-'.$page.'.xml');?>"/>
  <?php
    foreach($listings as $listing):
      $fields = $listing->StandardFields;
      if(count($fields->Photos) < 1 ||
        $fields->Latitude == '********' ||
        $fields->Longitude == '********' ||
        $fields->SubdivisionName === NULL ||
        $fields->BathroomsTotalInteger === NULL ||
        $fields->BedsTotal === NULL ||
        $fields->CurrentPrice == '********' ||
        $fields->YearBuilt === NULL){ continue; }
      ?><listing>
    <home_listing_id><?php echo $listing->Id; ?></home_listing_id>
    <name><?php echo $fields->UnparsedAddress; ?></name>
    <availability>for_sale</availability>
    <description><?php echo $fields->PublicRemarks != NULL ? htmlspecialchars($fields->PublicRemarks) : '$'.number_format($fields->CurrentPrice); ?></description>
    <address format="simple">
      <component name="addr0"><?php echo $fields->UnparsedFirstLineAddress; ?></component>
      <component name="city"><?php echo $fields->City; ?></component>
      <component name="region"><?php echo $fields->StateOrProvince; ?></component>
      <component name="country"><?php echo $fields->Country; ?></component>
      <component name="postal_code"><?php echo $fields->PostalCode; ?></component>
    </address>
    <latitude><?php echo $fields->Latitude; ?></latitude>
    <longitude><?php echo $fields->Longitude; ?></longitude>
    <image>
      <url><?php echo $fields->Photos[0]->UriLarge; ?></url>
    </image>
    <neighborhood><?php echo htmlspecialchars($fields->SubdivisionName); ?></neighborhood>
    <listing_type>for_sale_by_agent</listing_type>
    <num_baths><?php echo $fields->BathroomsTotalInteger; ?></num_baths>
    <num_beds><?php echo $fields->BedsTotal; ?></num_beds>
    <num_units>1</num_units>
    <price><?php echo $fields->CurrentPrice; ?> USD</price>
    <property_type><?php echo getPropertyType($fields->PropertyClass);?></property_type>
    <url><?php echo 'https://listings.adbotformula.com/'.$listing->Id; ?></url>
    <year_built><?php echo $fields->YearBuilt; ?></year_built>
  </listing>
  <?php endforeach; ?>
</listings>
