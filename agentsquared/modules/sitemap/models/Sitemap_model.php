<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sitemap_model extends CI_Model{ 

  function __construct() {
      parent:: __construct();

      $this->db_slave =  $this->load->database('db_slave', TRUE);
  }
	
  function get_pages($agent_id = ''){
    if(!empty($agent_id)){
      $this->db_slave->where('agent_id', $agent_id);
      $query = $this->db_slave->get('pages');
      if($query && $query != NULL){
        return $query->result();
      }
    }
    return false;
  }
  
  function get($args = array()){
    if(!empty($args)){
      if(array_key_exists('agent_id', $args)){
        $this->db_slave->where('agent_id', $args['agent_id']);
        if(isset($args['status'])){
          $this->db_slave->where('status', $args['status']);
        }
        $query = $this->db_slave->get('pages');
        if($query && $query != NULL){
          return $query->result();
        }
      }
    }
    return false;
  }
}

