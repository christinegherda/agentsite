<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sitemap extends MX_Controller {
  private $agent_id = '';
  private $user_id = '';
  protected $domain_user;
  
  function __construct(){
    parent::__construct();

    $this->load->model('sitemap/sitemap_model', 'sitemap');
    $this->load->model("home/home_model");
    $this->domain_user = domain_to_user();
    $this->agent_id = $this->domain_user->agent_id;
    $this->user_id = $this->domain_user->id;
  }
  
  public function index(){

    $host_url = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]";
    $find_string = "agentsquared.com";

    if (strpos($host_url, $find_string) !== false) {
      exit;//only process sitemap for custom domain
    }

    $data['total'] = array();
    $page_limit = 10;
    
    $activelistings = Modules::run('dsl/getSparkProxyComplete', 'my/listings', "_filter=MlsStatus%20Eq%20'Active'&_pagination=count");
    if(!isset($activelistings->SparkQLErrors) && isset($activelistings->Pagination) && $activelistings->Pagination->TotalRows > 0){
      $activelistings_total_pages = $activelistings->Pagination->TotalPages;
      $data['total']['activelistings'] = ($activelistings_total_pages >= $page_limit) ? $page_limit : $activelistings_total_pages;
    }
    
    $savedsearches = Modules::run('dsl/getSparkProxyComplete', 'savedsearches', "_pagination=count");
    if(!isset($savedsearches->SparkQLErrors) && isset($savedsearches->Pagination) && $savedsearches->Pagination->TotalRows > 0){
      $savedsearches_total_pages = $savedsearches->Pagination->TotalPages;
      $data['total']['savedsearches'] = ($savedsearches_total_pages >= $page_limit) ? $page_limit : $savedsearches_total_pages;
    }
    
    // $soldlistings = Modules::run('dsl/getSparkProxyComplete', 'my/listings', "&_filter=MlsStatus%20Eq%20'Closed'&_pagination=count");
    // if(!isset($soldlistings->SparkQLErrors) && isset($soldlistings->Pagination) && $soldlistings->Pagination->TotalRows > 0){
    //   $soldlistings_total_pages = $soldlistings->Pagination->TotalPages;
    //   $data['total']['soldlistings'] = ($soldlistings_total_pages >= $page_limit) ? $page_limit : $soldlistings_total_pages;
    // }
    
    $officelistings = Modules::run('dsl/getSparkProxyComplete', 'office/listings', "_filter=MlsStatus%20Eq%20'Active'&_pagination=count");
    if(!isset($officelistings->SparkQLErrors) && isset($officelistings->Pagination) && $officelistings->Pagination->TotalRows > 0){
      $officelistings_total_pages = $officelistings->Pagination->TotalPages;
      $data['total']['officelistings'] = ($officelistings_total_pages >= $page_limit) ? $page_limit : $officelistings_total_pages;
    }

    $lastday = gmdate("Y-m-d\TH:i:s\Z",strtotime("-1 days"));
    $today =  gmdate("Y-m-d\TH:i:s\Z");
    $filterStr = "(MlsStatus Eq 'Active') And (OnMarketDate Bt ".$lastday.",".$today.")";
    $filterUrl = rawurlencode($filterStr);
    $filterString = substr($filterUrl, 3, -3);
    
    $newlistings = Modules::run('dsl/getSparkProxyComplete', 'listings', "_filter=(".$filterString.")&_pagination=count");
    if((isset($newlistings->Success) && $newlistings->Success) && isset($newlistings->Pagination) && $newlistings->Pagination->TotalRows > 0){
      $newlistings_total_pages = $newlistings->Pagination->TotalPages;
      $data['total']['newlistings'] = ($newlistings_total_pages >= $page_limit) ? $page_limit : $newlistings_total_pages;
    }
    
    //printA($data);exit;
    header('Content-type: application/xml');
    $this->load->view('sitemap', $data);

  }
  
  public function routes(){
    $routes = array();
    $soldlistings = Modules::run('dsl/getSparkProxyComplete', 'my/listings', "_limit=10&_filter=MlsStatus%20Eq%20'Closed'&_pagination=1");
    if(!isset($soldlistings->SparkQLErrors) && isset($soldlistings->Pagination) && $soldlistings->Pagination->TotalRows > 0){
      $routes['soldlistings'] = $soldlistings->Pagination->TotalPages;
    }
    return $routes;
  }
  
  public function pages(){
    $data['pages'] = array();
    if($this->agent_id != ''){
      $data['pages'] = $this->sitemap->get_pages($this->agent_id);
      $user_info = $this->home_model->getUsersInfo($this->domain_user->id);
      $data['plan_id'] = $user_info->plan_id;
    }
    
    header('Content-type: application/xml');
    $this->load->view('sitemap_pages', $data);
  }
  
  public function properties($page = '1'){
    $data['properties'] = array();
    
    $properties = Modules::run('dsl/getSparkProxyComplete', 'my/listings', "_limit=10&_page=".$page."&_filter=MlsStatus%20Eq%20'Active'&_select=UnparsedFirstLineAddress,PostalCode,Id,ListingUpdateTimestamp,ModificationTimestamp&_pagination=1");
    
    if(!isset($properties->SparkQLErrors)){
      if(property_exists($properties, 'Results')){
        $data['properties'] = $properties->Results;
      }else{
        redirect('/sitemap'); exit();
      }
    }
    
    $data['base_slug'] = 'property-details';
    $data['title'] = 'Active Property Listings';
    $data['frequency'] = 'daily';
    $data['priority'] = '1.0';
    $data['standard'] = true;
    $data['page'] = $page;
    
    header('Content-type: application/xml');
    $this->load->view('sitemap_properties', $data);
  }
  
  public function savedsearches($page = "1"){
    $data['properties'] = array();
    
    $properties = Modules::run('dsl/getSparkProxyComplete', 'savedsearches', "_limit=10&_page=".$page."&_pagination=1");
    
    if(!isset($properties->SparkQLErrors)){
      if(property_exists($properties, 'Results')){
        $data['properties'] = $properties->Results;
      }else{
        redirect('/sitemap'); exit();
      }
    }
    $data['base_slug'] = 'saved_searches';
    $data['title'] = 'Saved Searches';
    $data['frequency'] = 'daily';
    $data['priority'] = '1.0';
    $data['standard'] = false;
    $data['page'] = $page;
    
    header('Content-type: application/xml');
    $this->load->view('sitemap_properties', $data);
  }
  
  public function listings($type = 'new', $page = '1'){
    $data['properties'] = array();
    $data['base_slug'] = 'other-property-details';
    $data['title'] = 'Listings';
    $data['frequency'] = 'weekly';
    $data['priority'] = '0.8';
    $data['standard'] = true;
    $data['page'] = $page;
    
    if($type == 'new'){
      $data['title'] = 'New Listings';
      $data['frequency'] = 'weekly';
      $data['priority'] = '0.8';

      $lastday = gmdate("Y-m-d\TH:i:s\Z",strtotime("-1 days"));
      $today =  gmdate("Y-m-d\TH:i:s\Z");
      $filterStr = "(MlsStatus Eq 'Active') And (OnMarketDate Bt ".$lastday.",".$today.")";
      $filterUrl = rawurlencode($filterStr);
      $filterString = substr($filterUrl, 3, -3);
      $selectFields = "UnparsedFirstLineAddress,PostalCode,Id,ListingUpdateTimestamp,ModificationTimestamp";
      
      $properties = Modules::run('dsl/getSparkProxyComplete', 'listings', "_limit=10&_page=".$page."&_filter=(".$filterString.")&_select=".$selectFields."&_pagination=1");
      
    }else if($type == 'office'){
      $data['title'] = 'Office Listings';
      $data['frequency'] = 'daily';
      $data['priority'] = '1.0';
      
      $properties = Modules::run('dsl/getSparkProxyComplete', 'office/listings', "_limit=10&_page=".$page."&_filter=MlsStatus%20Eq%20'Active'&_select=UnparsedFirstLineAddress,PostalCode,Id,ListingUpdateTimestamp,ModificationTimestamp&_pagination=1");
      
    }else{
      redirect('/sitemap'); exit();
    }
    if($properties->Success || !isset($properties->SparkQLErrors)){
      if(property_exists($properties, 'Results')){
        $data['properties'] = $properties->Results;
      }else{
        redirect('/sitemap'); exit();
      }
    }
    
    header('Content-type: application/xml');
    $this->load->view('sitemap_properties', $data);
  }
  
  public function sold_properties($page = "1"){
    $data['properties'] = array();
    
    $properties = Modules::run('dsl/getSparkProxyComplete', 'my/listings', "_limit=10&_page=".$page."&_filter=MlsStatus%20Eq%20'Closed'&_select=UnparsedFirstLineAddress,PostalCode,Id,ListingUpdateTimestamp,ModificationTimestamp&_pagination=1");
    
    if(!isset($properties->SparkQLErrors)){
      if(property_exists($properties, 'Results')){
        $data['properties'] = $properties->Results;
      }else{
        redirect('/sitemap'); exit();
      }
    }
    $data['base_slug'] = 'property-details';
    $data['title'] = 'Sold Property Listings';
    $data['frequency'] = 'weekly';
    $data['priority'] = '0.7';
    $data['standard'] = true;
    $data['page'] = $page;
    
    header('Content-type: application/xml');
    $this->load->view('sitemap_properties', $data);
  }

  public function home_listings($page = '1'){
    $page = (int) $page;
    
    $lastday = gmdate("Y-m-d\TH:i:s\Z",strtotime("-1 days"));
    $today =  gmdate("Y-m-d\TH:i:s\Z");
    $filterStr = "(MlsStatus Eq 'Active') And (OnMarketDate Bt ".$lastday.",".$today.")";
    $filterUrl = rawurlencode($filterStr);
    $filterString = substr($filterUrl, 3, -3);
    $limit = 25;
    $selectFields = "Id,UnparsedAddress,PublicRemarks,CurrentPrice,StateOrProvince,UnparsedFirstLineAddress,City,Country,PostalCode,Latitude,Longitude,SubdivisionName,BathroomsTotalInteger,BedsTotal,YearBuilt,PropertyClass,Photos.UriLarge";

    $first25 = Modules::run('dsl/getSparkProxyComplete', 'listings', "_limit=".$limit."&_page=".($page * 1)."&_filter=(".$filterString.")&_select=".$selectFields."&_expand=PrimaryPhoto&_pagination=1");
    $second25 = Modules::run('dsl/getSparkProxyComplete', 'listings', "_limit=".$limit."&_page=".($page * 2)."&_filter=(".$filterString.")&_select=".$selectFields."&_expand=PrimaryPhoto&_pagination=1");
    $third25 = Modules::run('dsl/getSparkProxyComplete', 'listings', "_limit=".$limit."&_page=".($page * 3)."&_filter=(".$filterString.")&_select=".$selectFields."&_expand=PrimaryPhoto&_pagination=1");
    $fourth25 = Modules::run('dsl/getSparkProxyComplete', 'listings', "_limit=".$limit."&_page=".($page * 4)."&_filter=(".$filterString.")&_select=".$selectFields."&_expand=PrimaryPhoto&_pagination=1");

    if($first25->Success && $second25->Success && $third25->Success && $fourth25->Success){
      $listings = array_merge($first25->Results, $second25->Results, $third25->Results, $fourth25->Results);
      if(isset($_GET['debug']) && $_GET['debug']) printa($listings, true, (isset($_GET['vardump']) && $_GET['vardump']) ? true : false);
      header('Content-type: application/xml');
      $this->load->view('home_listings', array(
        'page' => $page,
        'listings' => $listings,
        'total' => count($listings)
      ));
    }else{
      show_error('There was problem fetching these properties.', 500);
    }
  }
}
