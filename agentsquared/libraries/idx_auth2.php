<?php if( ! defined('BASEPATH') ) exit('No direct script access allowed');

require APPPATH.'third_party/idx/sparkapi4p2/lib/Core.php';

class Idx_auth{

	/**
	  Created By: Rolen Brua
	**/

	public $api;
	public $result;	

	public $access_token = "ep5ofyvmca00u8p1p99fjvp88";
	public $refresh_token = "ex3nw7v2xgsgcb8hydyy637g6";
	
	function __construct( $agent_id = "" ) {

		$CI =& get_instance();
		$CI->load->database();
		$CI->load->library('email');

		//parent::__construct();	
		$GLOBALS['client_id']     = "a6b5t2xvveynb5ohmhohn4moc"; //"ahz16nuym5fzlk5gbnvbli04e a6b5t2xvveynb5ohmhohn4moc";
		$GLOBALS['client_secret'] = "c0ss5y0u9yi38ahn3c2oyurdv"; //"de76ykm90o521aecvlozd72e8 c0ss5y0u9yi38ahn3c2oyurdv";
		$GLOBALS['redirect_uri']  = AGENT_DASHBOARD_URL . "idx_login/idx_callback"; //"https://agentsquared.com/agent-site/idx_login/idx_callback";

		$this->api = new SparkAPI_Hybrid($GLOBALS['client_id'], $GLOBALS['client_secret'], $GLOBALS['redirect_uri']);
		
		// Let's identify our new application.
		$this->api->SetApplicationName("PHP-APP-In-Fifteen-Minutes/1.0");

		//$this->access_token = $this->api->oauth_access_token;
	    //$this->refresh_token = $this->api->oauth_refresh_token;

	   	$query = $CI->db->select("*")
	   					->from("users_idx_token")
	   					->where("agent_id",$agent_id)
	   					->get();

	    $token = $query->row();

	    if( !empty($token) )
	    {
	    	 $this->api->SetAccessToken($token->access_token);
	    	$this->api->SetRefreshToken($token->refresh_token); 	
	    } else {

		    	$query = $CI->db->select("*")
	   					->from("user_bearer_token")
	   					->where("agent_id", $agent_id)
	   					->get();

	    		$x = $query->row();

	    		$this->api->SetAccessToken($x->bearer_token);
		}
	   

	}
	
	public function getSaveSearchesListings( $resultQuery = '' ){

		return $this->api->GetListings($resultQuery);	

	}

	public function getNewPropertyListings(){

		$result_listings = $this->api->GetListings( 
			array(
			    '_limit' => 4,
			    '_filter' => "MajorChangeType Eq 'New Listing' And (MlsStatus Eq 'Active' Or MlsStatus Eq 'Contingent')",
			    '_expand' => 'PrimaryPhoto'
			) 
		);

		return $result_listings;
	}

	public function getNearByListings( $getLatLon = "")	{
		$result_listings = $this->api->GetNearbyListings($getLatLon);
		return $result_listings;
	}

	public function checker(){
		return $this->api->GetMyAccount();
	}

	public function getSavedSearch($search_id = "") {

		return $this->api->GetSavedSearch( $search_id );
	}
	
	public function addContact($data) {
		return $this->api->AddContact($data);
	} 

	public function getContact( $contact_id = "" ){
		return $this->api->GetContact( $contact_id );
	}

	public function GetListing( $property_id = "" ){
		return $this->api->GetListing( $property_id );
	}

	public function GetContacts( $i = 0 )
	{
		$results = $this->api->GetContacts(NULL, 
				array(
					'_pagination' => 1,
					'_page' => $i,
					'_limit' => 24,
				)
			);

		$data = $this->api;

		return array("results" => $results, "data" => $data);
	}

	public function GetMyListings(){
		return $this->api->GetMyListings();
	}

	public function GetPropertyTypes() {
		return $this->api->GetPropertyTypes();
	}

	public function GetRoomDimension($id) {
		return $this->api->GetRoomDimension($id);
	}
	
	public function GetStandardFieldList($field) {
		return $this->api->GetStandardFieldList($field);
	}
}
