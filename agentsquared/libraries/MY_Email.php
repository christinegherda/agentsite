<?php if( ! defined('BASEPATH') ) exit('No direct script access allowed');

class MY_Email extends CI_Email {

	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Send an email to a single recipient by calling a single function
	 *
	 * @param  string  the email address of the sender
	 * @param  string  the name of the sender
	 * @param  string  the email address of the recipient
	 * @param  string  the subject of the email
	 * @param  string  the name of the email template ( a view )
	 * @param  array   an optional array of view_data to inject into the email template
	 */

	/**
	  Created By: Rolen Brua
	**/
	public function send_email(
		$from_email,
		$from_name,
		$recipient_email,
		$subject,
		$email_template = "email/template/default-email-body",
		$email_data = array()
	) {

		//$recipient_email = "jocereymondelo@gmail.com"; /*test*/
    	$CI    = get_instance();
    	$url    = 'https://api.sendgrid.com/';
    	$request   =  $url.'api/mail.send.json';
    	$user    = getenv('SENDGRID_USERNAME');
    	$pass    = getenv('SENDGRID_PASSWORD');
    	$category   = $subject;
    	//$messageHtml = $CI->load->view($email_template, $email_data, TRUE);
    	$email_data['content'] = $CI->load->view( $email_template, $email_data, TRUE );
		$email_data['subject'] = $subject;
    	$messageText = $CI->load->view('email/template/boilerplate', $email_data, TRUE);
    	//$built_message = ci()->load->view( 'email/template/boilerplate', $email_data, TRUE );
    	//$messageText = strip_tags(str_replace(array("<i>", "</i>"), array("_", "_"), $messageHtml));
		//$email_data["to_bcc"] = "rolbru12@gmail.com,troy@agentsquared.com,eugene@agentsquared.com,agentsquared888@gmail.com,albert@agentsquared.com";
		//$email_data["to_bcc"] = "rolbru12@gmail.com,agentsquared888@gmail.com,karljohnhonrado@gmail.com";

    	if(isset($email_data["to_bcc"])) {
			$bcc = $email_data["to_bcc"];
			$the_one=FALSE;
		} else {
			$bcc = 'rolbru12@gmail.com';
			$the_one=TRUE;
		}

		if($the_one) {
			for($i=0; $i<=2; $i++) {
				if($i==0) {
					$recipient = $recipient_email;
				} else {
					$recipient = $bcc;
				}

				$json_string = array(
			      	'to' => array($recipient),
			      	'category' => $category
			    );

			    $params = array(
			      	'api_user'  => $user,
			      	'api_key'   => $pass,
			      	'x-smtpapi' => json_encode($json_string),
			      	'to'        => $recipient,
			      	'subject'   => $subject,
			      	'html'      => $messageText,
			      	'text'      => $messageText,
			      	'from'      => $from_email,
				'fromname'  => $from_name
			    );

		    	$session = curl_init($request);
		    	curl_setopt($session, CURLOPT_POST, true);
		    	curl_setopt($session, CURLOPT_POSTFIELDS, $params);
		    	curl_setopt($session, CURLOPT_HEADER, false);
		    	#curl_setopt($session, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
		    	curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
		    	$response = curl_exec($session);
		    	curl_close($session);
		    	$dec = json_decode($response);
			}

		} else {

			$bcc_arr = explode(',', $bcc);

			$count=-1;

			for($i=-1; $i<count($bcc_arr); $i++) {

				if($count==-1) {
					$recipient = $recipient_email;
				} else {
					$recipient = $bcc_arr[$i];
				}

			    $json_string = array(
			      	'to' => array($recipient),
			      	'category' => $category
			    );

			    $params = array(
			      	'api_user'  => $user,
			      	'api_key'   => $pass,
			      	'x-smtpapi' => json_encode($json_string),
			      	'to'        => $recipient,
			      	'subject'   => $subject,
			      	'html'      => $messageText,
			      	'text'      => $messageText,
			      	'from'      => $from_email,
              		'fromname'  => $from_name
			    );

		    	$session = curl_init($request);
		    	curl_setopt($session, CURLOPT_POST, true);
		    	curl_setopt($session, CURLOPT_POSTFIELDS, $params);
		    	curl_setopt($session, CURLOPT_HEADER, false);
		    	#curl_setopt($session, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
		    	curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
		    	$response = curl_exec($session);
		    	curl_close($session);
		    	$dec = json_decode($response);

		    	$count++;
		    	/*if(isset($dec->message) && $dec->message == "success") {
		    		$count++;
		    		//if($count == count($bcc))
		      			//return true;
		    	} else {
		      		//return false;
		    	}*/
		    }
		}

		return (isset($dec->message) && $dec->message == "success") ? TRUE : FALSE;

  	}	

  	//send email version 3
  	public function send_email_v3(
		$from_email,
		$from_name,
		$recipient_email,
		$to_bcc,
		$template_id,
		$email_data = array()
	) {
		
  		$url = "https://api.sendgrid.com/v3/mail/send";
		
		$key = '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';
		// $key = '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';
		

		$headers = array(
			'Content-Type: application/json',
		    'Authorization: Bearer '. $key,
		    //'Authorization: Bearer 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',
		);

		// $headers = array(
		// 	'Content-Type: application/json',
		//     'Authorization: Bearer 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',
		// );

		$whitelist = array(
		    '127.0.0.1',
		    '::1',
		    '172.18.0.1'
		);

		if(in_array($_SERVER['REMOTE_ADDR'], $whitelist)){
		    $recipient_email = 'serg.casquejo@gmail.com';
			$to_bcc = 'karljohnhonrado@gmail.com';
		} else {
			$to_bcc = 'joce@agentsquared.com, james@agentsquared.com, paul@agentsquared.com';
		}

		$bcc_arr = array();

		if(!empty($to_bcc)) {

			$bcc_explode = explode(",", $to_bcc);
			
			foreach($bcc_explode as $key) {

				//$bcc_arr[] = array('email'=>$key);
				if(trim($recipient_email)==trim($key)) {
					continue;
				} else {
					$bcc_arr[] = array('email'=>$key);
				}

			}

		}

		$params = array(
	    	'personalizations' => array(
	            array(
	                'to' => array(array('email' => $recipient_email)),
	                'bcc' => $bcc_arr,
	                'substitutions' => $email_data['subs']
                )
	        ),
	        'from' => array(
	        	'email' => $from_email,
	        	'name'  => $from_name
	        ),
	        'reply_to' => array('email'=>isset($email_data['reply_to']) ? $email_data['reply_to'] : $from_email),
	        'template_id' => $template_id
		);
		
		$params = json_encode($params,true);

		// print_r($params);
		// die;

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_URL, $url);

		$result = curl_exec($ch);

		$result_info = json_decode($result, true);

		return TRUE;
		
  	}
}
