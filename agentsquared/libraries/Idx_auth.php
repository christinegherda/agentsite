<?php if(!defined('BASEPATH') ) exit('No direct script access allowed');

class Idx_auth{

	/**
	  Created By: Rolen Brua
	**/

	private $access_token;
	private $refresh_token;
	private $domain_user;

	function __construct($agent_id = "") {
		
		$CI =& get_instance();
		$CI->load->database();
		$CI->load->library('session');
		$CI->load->library('email');
		$CI->domain_user = domain_to_user();

		if(isset($agent_id)) {

			$query = $CI->db->select("*")
	   					->from("users_idx_token")
	   					->where("agent_id", $agent_id)
	   					->get();

	    	$token = $query->row();

	    	if($token) {

		   		$this->access_token = $token->access_token;
		    	$this->refresh_token = $token->refresh_token;

		    } else {

		    	$query = $CI->db->select("*")
	   					->from("user_bearer_token")
	   					->where("agent_id", $agent_id)
	   					->get();

	    		$x = $query->row();

	    		$this->access_token = $x->bearer_token;
		    }
		}

	}

	public function checker(){
		
		$CI =& get_instance();
		$CI->load->database();
		$CI->load->model('dsl/dsl_model');

		$token = $CI->dsl_model->getUserAccessToken($CI->domain_user->agent_id);

		return (isset($token->is_valid) && $token->is_valid==1) ? '1' : '';

		//return $this->get('my/account');
	}

	public function getSavedSearch($search_id = "") {
		return $this->get('savedsearches/'.$search_id);
	}

	public function getSavedSearches($i = 0) {
		return $this->get_params('savedsearches', array('_pagination' => 1, '_page' => $i, '_limit' => 24));
	}

	public function GetListing($property_id = ""){
		return $this->get('listings/'.$property_id);
	}

	public function getContact($contact_id = ""){
		return $this->get('contacts/'.$contact_id);
	}

	public function GetContacts($i = 0) {
		return $this->get_params('contacts', array('_pagination' => 1, '_page' => $i, '_limit' => 24));
	}

	public function addContact($data) {
		return $this->post('contacts', $data);

	} 

	public function get($service) {
		$url = "https://sparkapi.com/v1/".$service;

		$headers = array(
			'Content-Type: application/json',
			'User-Agent: Spark API PHP Client/2.0',
			'X-SparkApi-User-Agent: PHP-APP-In-Fifteen-Minutes/1.0',
		    'Authorization: OAuth '. $this->access_token,
		);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_URL, $url);

		$result = curl_exec($ch);
		$result_info = json_decode($result, true);

		if(empty($result_info)) {
			$result_info = curl_strerror(curl_errno($ch));
		}

		return (isset($result_info['D']) && $result_info["D"]["Success"]) ? $result_info["D"]["Results"] : FALSE;
	}

	public function get_params($service, $param=array()) {

		$url = "https://sparkapi.com/v1/".$service."?_pagination=".$param['_pagination']."&_page=".$param['_page']."&_limit=".$param['_limit'];

		$headers = array(
			'Content-Type: application/json',
			'User-Agent: Spark API PHP Client/2.0',
			'X-SparkApi-User-Agent: PHP-APP-In-Fifteen-Minutes/1.0',
		    'Authorization: OAuth '. $this->access_token,
		);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_URL, $url);

		$result = curl_exec($ch);
		$result_info = json_decode($result, true);

		if(empty($result_info)) {
			$result_info = curl_strerror($ch);
		}

		if($result_info["D"]["Success"]) {
			return array('results' => $result_info["D"]["Results"], 'pagination'=>$result_info["D"]["Pagination"]);
		} else {
			return FALSE;
		}

	}

	public function post($service, $data=array()) {

		$url = "https://sparkapi.com/v1/".$service;

		$headers = array(
			'Content-Type: application/json',
			'User-Agent: Spark API PHP Client/2.0',
			'X-SparkApi-User-Agent: PHP-APP-In-Fifteen-Minutes/1.0',
		    'Authorization: OAuth '. $this->access_token,
		);

		$post = array('D'=>$data);
		$body = json_encode($post);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 0);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
		curl_setopt($ch, CURLOPT_URL, $url);

		$result = curl_exec($ch);
		$result_info = json_decode($result, true);

		if(empty($result_info)) {
			$result_info = curl_strerror($ch);
		}

		//return $result_info;
		return ($result_info["D"]["Success"]) ? $result_info["D"]["Results"] : FALSE;

	}

	public function isTokenValid() {

		$url = "https://sparkapi.com/v1/my/account";

		$headers = array(
			'Content-Type: application/json',
			'User-Agent: Spark API PHP Client/2.0',
			'X-SparkApi-User-Agent: PHP-APP-In-Fifteen-Minutes/1.0',
		    'Authorization: OAuth '. $this->access_token,
		);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_URL, $url);

		$result = curl_exec($ch);
		$result_info = json_decode($result, true);

		if(empty($result_info)) {
			$result_info = curl_strerror($ch);
		}

		return ($result_info["D"]["Success"]) ? TRUE : FALSE;

	}
}
