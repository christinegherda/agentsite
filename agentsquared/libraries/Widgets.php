<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Widgets extends MX_Controller{
  public $name = array();
  public $data = array();
  public $asset = THEME_CSS . 'widgets.css?names=';
  public $listings = array();
  public $args = array();
  protected $domain_user;

  /**
  * Enqueues the required dependencies such as CSS and JS files. Must be called inside the controller methods.
  */
  public function enqueue(){
    enqueue_style('widgets', $this->asset . implode(',', $this->name));
  }

  /**
  * Prepares the widget before its being rendered into the view. Must be called inside the controller methods.
  *
  * @param mixed $widgets (string) or (assoc array)
  * @param array $args (assoc array) of name value pair
  */
  public function prepare($widgets = NULL, $args = array()){
    if($widgets !== NULL){
      if(is_array($widgets)){ // if you pass in associative array
        foreach($widgets as $name){
          $key = array_search($name, $widgets);
          if(is_numeric($key)){
            $this->load_assets($name);
          }else{
            $this->load_assets($key);
          }
        }
      }else{ // if you pass in string value
        $this->load_assets($widgets);
      }
    }
    $this->args = $args;
  }

  /**
  * Loads and enqueues the required dependencies of this specific widget.
  *
  * @param string $name Name of the widget
  */
  private function load_assets($name){
    if(!in_array($name, $this->name)){
      array_push($this->name, $name);
    }
    switch($name){
      case 'savedsearches-carousel':
        enqueue_script($name, THEME_JS . 'savedsearches-carousel.js');
      case 'horizontal-carousel':
        enqueue_style('carousel', THEME_CSS . 'owl.carousel.min.css');
        enqueue_script('carousel', THEME_JS . 'owl.carousel.min.js');
        enqueue_script('horizontal-carousel', THEME_JS . 'horizontal-carousel.js');
      break;
      case 'photo-gallery':
      case 'photo-gallery-left':
      case 'photo-gallery-right':
        enqueue_style('lightslider', THEME_CSS . 'lightslider.css');
        enqueue_script('lightslider', THEME_JS . 'lightslider.js');
        enqueue_script('photo-gallery', THEME_JS . 'photo-gallery.js');
      case 'basic-search':
        enqueue_style('basic-search-css', THEME_CSS . 'basic-search.css');
        enqueue_script('basic-search-js', THEME_JS . 'basic-search.js');
      break;
    }
  }

  /**
  * Renders the widget into the view.
  *
  * @param string $name Name of the widget to be called.
  * @param array $args (assoc array) values to be passed on to this widget view
  */
  public function render($name = NULL, $args = array()){
    if(!in_array($name, $this->name)){
      echo '<p class="text text-danger">Widget ('.$name.') was not prepared.</p>';
      return;
    }
    $data = array();

    if(isset($this->args['shortcodes']) && !empty($this->args['shortcodes'])){
      foreach($this->args['shortcodes'] as $key => $value){
        $shortcode = '[' . $key . ']';
        if(isset($args['title'])){ // if we need to replace words in a string
          $args['title'] = str_replace($shortcode, $value, $args['title']);
        }
      }
    }

    if($name !== NULL && isset($args['type'])){ // if widgets require listing details to be displayed
      $data['type'] = $args['type'];

      switch ($data['type']) {
        case 'active':
          $data['url_first_segment'] = 'active_listings';
          break;
        case 'new':
          $data['url_first_segment'] = 'new_listings';
          break;
        case 'sold':
          $data['url_first_segment'] = 'sold_listings';
          break;
        case 'nearby':
          $data['url_first_segment'] = 'nearby_listings';
          break;
        case 'office':
          $data['url_first_segment'] = 'office_listings';
          break;
        default:
          $data['url_first_segment'] = false;
      }

      $data['listings'] = $this->feed($args['type'], $args); // retrieve the listings to be fed into the widget

      if(!empty($args)){
        $data = array_merge($data, $args);
      }

      switch($name){ // add new widget views here.
        case 'horizontal-carousel':
        case 'single-row-cards':
        case 'property-grid-view':
        case 'property-list-view':
        case 'basic-search':
          $this->load->view(COMMON_WIDGETS_VIEW . $name, $data);
        break;
      }
    }else if($name !== NULL && !isset($args['type'])){ // if widget does not require listing details

      if(!empty($args)){
        $data = array_merge($data, $args); // pass arguments to the view
      }

      switch($name){ // add new widget views here.
        case 'listing-count':
          if(isset($args['data']) && !empty($args['data'])){
            foreach($args['data'] as $index => $value){
              $this->feed($value);
              $data['count'][$value] = (isset($this->data[$value]['total']))? $this->data[$value]['total']: 0;
            }
          }
          $this->load->view(COMMON_WIDGETS_VIEW . $name, $data);
          break;
        case 'savedsearches-carousel':

          $data['url_first_segment'] = 'all_saved_searches'; // add additional data for saved searches widget
          
        case 'photo-gallery':
        case 'photo-gallery-left':
        case 'photo-gallery-right':
        case 'list-view':
        case 'grid-view':
          $this->load->view(COMMON_WIDGETS_VIEW . $name, $data);
          break;
        case 'custom-menu':
          $data['custom_menu_data'] = Modules::run('menu/get_custom_menu_data');
          $this->load->view(COMMON_WIDGETS_VIEW . $name, $data);
          break;
      }
    }else{
      echo '<p class="text text-danger">Widget name and data type are both required.</p>';
    }
  }

  /**
  * Fetches the required data and returns it.
  *
  * @param string $type What property to pull e.g. active, recent, new, sold, office, nearby, saved searches
  * @param array $args (associative array)
  */
  private function feed($type = NULL, $args = array()){
    $response = false;
    $listings = false;
    $limit = (isset($args['limit']))? (int) $args['limit']: 4;
    $return = array();
    $this->domain_user = domain_to_user();

    $select = '&_select='.implode(',', array( // add the fields that you want to pull here. The reason why I did this so the data does not wait too much from spark to return.
      'UnparsedAddress',
      'UnparsedFirstLineAddress',
      'City',
      'PostalCode',
      'PostalCity',
      'StateOrProvince',
      'PropertyClass',
      'ListPrice',
      'CurrentPrice',
      'BedsTotal',
      'BathsTotal',
      'BathsFull',
      'BuildingAreaTotal',
      'LotSizeArea',
      'LotSizeSquareFeet',
      'LotSizeAcres',
      'LotSizeDimensions',
      'Photos',
      'ListingKey',
      'MlsStatus'
    ));

    $obj = Modules::load("mysql_properties/mysql_properties");
    if(!array_key_exists($type, $this->listings)){ // checks if the type of listings data is already present
      switch($type){
        case 'active':
        
          $endpoint = "_limit=".$limit."&_page=1&_filter=MlsStatus%20Eq%20'Active'&_pagination=1&_expand=Photos".$select;
          $response = Modules::run('dsl/getSparkProxyComplete', 'my/listings', $endpoint);

          if(property_exists($response,'Success') && $response->Success){
            $listings = $response->Results;
            $this->data[$type]['total'] = $response->Pagination->TotalRows;
          }else{
            $listings = $obj->get_properties($this->domain_user->id, "active");
            $this->data[$type]['total'] = count($listings);
          }

          break;
        case 'recent':
        case 'new':

          $endpoint = "_limit=".$limit."&_filter=MajorChangeType%20Eq%20'New%20Listing'%20And%20MlsStatus%20Eq%20'Active'&_pagination=1&_expand=Photos".$select;
          $response = Modules::run('dsl/getSparkProxyComplete', 'listings', $endpoint);

          if(property_exists($response,'Success') && $response->Success){
            $listings = $response->Results;
            $this->data[$type]['total'] = $response->Pagination->TotalRows;
          }else{
            $listings = $obj->get_properties($this->domain_user->id, "new");
            $this->data[$type]['total'] = count($listings);
          }

          break;
        case 'sold':

          $listings = $obj->get_properties($this->domain_user->id, "sold");
          $this->data[$type]['total'] = count($listings);

          break;
        case 'office':

          $listings = $obj->get_properties($this->domain_user->id, "office");
          $this->data[$type]['total'] = count($listings);

          break;
        case 'nearby':

          if(isset($args['lat']) && isset($args['lon'])){
            
            $lat = $args['lat'];
            $lon = $args['lon'];

          }else{

            $location = array( 'address' => isset($args['user_info']->address)? $args['user_info']->address : '' );
            $options = http_build_query($location);
            $google_url = "https://maps.googleapis.com/maps/api/geocode/json?{$options}";
            $api_data = @file_get_contents( $google_url );
            $data = json_decode($api_data, true);

            if($data["status"] == "OK") {

              $lat = $data["results"][0]["geometry"]["location"]["lat"];
              $lon = $data["results"][0]["geometry"]["location"]["lng"];

            }else{

              $ch = curl_init();
              curl_setopt($ch, CURLOPT_URL, $google_url); // this is where I left off
              curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
              // curl_setopt($ch, URLOPTPRXYOT, 3128);
              // curlsetopt($ch, CURLOPT_SSL_VERIFHOS, 0);
              // curl_setopt($ch, CRLOP_SSLVERIYER, 0);
              $response = curl_exec($ch);
              curl_close($ch);
              
              $response = json_decode($response);
              
              $lat = $response->results[0]->geometry->location->lat;
              $lon = $response->results[0]->geometry->location->lng;
            }
          }

          $endpoint = "_limit={$limit}&_filter=MlsStatus%20Eq%20'Active'&_lat={$lat}&_lon={$lon}&_pagination=1&_expand=Photos".$select;
          $response = Modules::run('dsl/getSparkProxyComplete', 'listings/nearby', $endpoint);

          if(property_exists($response,'Success') && $response->Success){

            $listings = $response->Results;
            $this->data[$type]['total'] = $response->Pagination->TotalRows;

          }else{

            $listings = array();
            $this->data[$type]['total'] = 0;

          }

          break;
        // case 'savedsearches':
        //   $endpoint = "_limit=".$limit."&_pagination=1&_orderby=-ModificationTimestamp&_expand=ReferencingQuickSearchIds";
        //   $response = Modules::run('dsl/getSparkProxyComplete', 'savedsearches', $endpoint);
        //   if(property_exists($response,'Success') && $response->Success){
        //     $listings = $response->Results;
        //     $this->data[$type]['total'] = $response->Pagination->TotalRows;

        //     // $filter = rawurlencode($listings[0]->Filter); // get first saved search

        //     // $this->data[$type]['total'] = $response->Pagination->TotalRows;

        //     // $endpoint = "_limit=".$limit."&_page=1&_pagination=1&_filter=".$filter."&_expand=PrimaryPhoto&_orderby=-ModificationTimestamp";
        //     // $resp = Modules::run('dsl/getSparkProxyComplete', 'listings', $endpoint);

        //     // // this is where I freaking left off
        //     // //printa($resp); exit();
        //     // if(property_exists($resp,'Success') && $resp->Success){
        //     //   //printa($resp); exit();
        //     // }

        //   }else{
        //     $listings = array();
        //     $this->data[$type]['total'] = 0;
        //   }
        //   break;
      }
      $this->listings[$type] = $listings;
    }else{
      $listings = $this->listings[$type];
    }
    return $listings;
  }
}