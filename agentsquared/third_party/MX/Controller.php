<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

/** load the CI class for Modular Extensions **/
require dirname(__FILE__).'/Base.php';

/**
 * Modular Extensions - HMVC
 *
 * Adapted from the CodeIgniter Core Classes
 * @link	http://codeigniter.com
 *
 * Description:
 * This library replaces the CodeIgniter Controller class
 * and adds features allowing use of modules and the HMVC design pattern.
 *
 * Install this file as application/third_party/MX/Controller.php
 *
 * @copyright	Copyright (c) 2015 Wiredesignz
 * @version 	5.5
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 **/
class MX_Controller 
{
	public $autoload = array();
	
	public function __construct() 
	{
		$this->load->helper('cookie');

		$this->load->model("home/home_model", "home_model");
		/*
		$server_ip = isset($_SERVER['SERVER_ADDR']) ? $_SERVER['SERVER_ADDR'] : '127.0.0.1';

		if (!get_cookie('is_server_ip_whitelisted')) {

			$result = $this->home_model->get_sendgrid_rule_id_by_ip($server_ip);
			
			if (!$result) {

				$data = json_decode(sendgrid_curl("https://api.sendgrid.com/v3/access_settings/whitelist", array(
					'ips' => array(
						'ip' => $server_ip
					)
				)));
				
				if (isset($data->result[0])) {
					$this->home_model->save_whitelisted_ips(array(
						'rule_id' => $data->result[0]->id,
						'ip_address' => explode('/', $data->result[0]->ip)[0],
					));
				}
			}

			set_cookie('is_server_ip_whitelisted', true);
		}*/


		$class = str_replace(CI::$APP->config->item('controller_suffix'), '', get_class($this));
		log_message('debug', $class." MX_Controller Initialized");
		Modules::$registry[strtolower($class)] = $this;	
		
		/* copy a loader instance and initialize */
		$this->load = clone load_class('Loader');
		$this->load->initialize($this);	
		
		/* autoload module items */
		$this->load->_autoloader($this->autoload);


		/*redirect*/
		$str1 = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
		$str2 = "agentsquared.net";

		if (strpos($str1, $str2) !== false) {
		    // redirect to agentsquared.com
		    $redirectStr = str_replace("agentsquared.net", "agentsquared.com", $str1);
		    header("Location: ".$redirectStr);
		}

		if(empty(config_item("is_paid"))) {

			CI::$APP->db->select("receipt.rid")
						->from("receipt")
						->where("receipt.agent_id", CI::$APP->session->userdata('user_id'));

			$receipt = CI::$APP->db->get()->row(); 

			if($receipt) {

				CI::$APP->config->set_item('is_paid', TRUE);

			} else {

				CI::$APP->config->set_item('is_paid', FALSE); 

				if(!$this->config->item('trial')) {

					CI::$APP->db->select("u.trial, u.date_signed, p.price, p.plan")
			                ->from("users u")
			                ->join("pricing p", "u.id = p.user_id", "left")
			                ->where("u.id", CI::$APP->session->userdata('user_id'))
			                ->where("u.trial", 1)
			                ->where("p.type", "idx")
			                ->limit(1);

			        $res = CI::$APP->db->get()->row();

			        if(!empty($res)) {

			            $num = 0;

			            if(!empty($res->date_signed)) {

			                $from=date_create(date('Y-m-d'));
			                $to=date_create(date("Y-m-d", strtotime($res->date_signed)));
			                $diff=date_diff($from,$to);
			                $num = $diff->days;

			            }

			            $this->config->set_item('trial', TRUE);
						$this->config->set_item('left', $num);
						$this->config->set_item('pricing', $res->trial);
						$this->config->set_item('price', $res->price);
			        }
				}
			}
		}

		$this->check_subscription();

        if(CI::$APP->input->get('profiler'))
            CI::$APP->output->enable_profiler(TRUE);	   	
	}
	
	public function check_subscription() {

		if(!$this->config->item('agent_idx_website')) {
			
            $domain_user = domain_to_user();

			$arr = array(
				'UserId' => $domain_user->agent_id,
				'EventType' => 'purchased',
				'ApplicationType' => 'Agent_IDX_Website'
			);

			CI::$APP->db->select("EventType")
                		->from("agent_subscription")
                		->where($arr);

            $ret = CI::$APP->db->get()->result();

			if(!empty($ret)) { //If idx is purchased

            	$this->config->set_item('agent_idx_website', TRUE);

            } else { //If idx is cancelled or not been bought

            	$this->config->set_item('agent_idx_website', TRUE);

            	$cancel_arr = array(
					'UserId' => $domain_user->agent_id,
					'EventType' => 'canceled',
					'ApplicationType' => 'Agent_IDX_Website'
				);

				CI::$APP->db->select("EventType")
	                		->from("agent_subscription")
	                		->where($cancel_arr);

	           	$cancel_ret = CI::$APP->db->get()->result();

	           	if(!empty($cancel_ret)) { //If idx is cancelled, throw it to corp site
	           		
            		redirect('https://www.agentsquared.com/');
            		
	           	} else { //if no idx found, check the spw status
	           		$spw_arr = array(
                        'UserId' => $domain_user->agent_id,
						'EventType' => 'purchased',
						'ApplicationType' => 'Single_Property_Website'
					);

					CI::$APP->db->select("EventType")
		                		->from("agent_subscription")
		                		->where($spw_arr);

		           	$spw_ret = CI::$APP->db->get()->result();

		           	if(!empty($spw_ret)) {
		           		$this->config->set_item('agent_idx_website', FALSE);
		           	}
	           	}
            }
		}

		return TRUE;

	}

	public function __get($class) 
	{
		return CI::$APP->$class;
	}
}
