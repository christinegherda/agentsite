<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    
    <title><?php echo (isset($title)) ? $title : 'AgentSquared'; ?></title>
    <link rel="icon" type="image/x-icon"  href="<?php echo isset($branding->favicon) ? base_url().'assets/upload/favicon/'.$branding->favicon :  base_url().'assets/images/default-favicon.png';?>"/>
    <link rel="apple-touch-icon" href="<?= base_url()?>assets/img/home/apple-touch-icon.png">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="<?= base_url()?>assets/css/dashboard.css" type="text/css">
    <link rel="stylesheet" href="<?= base_url()?>assets/font-awesome/css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?= base_url()?>assets/fonts/fontello/css/fontello.css" type="text/css">
    <link rel="stylesheet" href="<?= base_url()?>assets/css/summernote.css" type="text/css">
    <link rel="stylesheet" href="<?= base_url()?>assets/css/jquery.domenu-0.48.53.css" type="text/css">
    <link rel="stylesheet" href="<?= base_url()?>assets/css/plugins/msgBoxLight.css" type="text/css">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css" type="text/css">
    <link rel="stylesheet" href="<?= base_url()?>assets/css/owl.carousel.css" type="text/css">
     <link rel="stylesheet" href="<?= base_url()?>assets/css/bootstrap-datepicker.css" type="text/css">
    <link rel="stylesheet" href="https://rawgit.com/enyo/dropzone/master/dist/dropzone.css" type="text/css">
    <?php if(isset($css)) :
        foreach($css as $key => $val ) :  ?>
        <link rel="stylesheet" href="<?= base_url()?>assets/css/<?php echo $val?>" type="text/css"> </link>
        <?php endforeach;?>
    <?php endif;?>
    <script type="text/javascript">var base_url = "<?php echo base_url(); ?>"; </script>
    
</head>

<body>

    <div id="wrapper" >
            <div class="container-fluid" >
                <div class="row" >
                    <div class="col-md-10 col-md-offset-0" style="border: solid 1px #dadada;">
                        <h1 class="page-header" style="text-align:center;">Existing Domains</h1>
                        <div style="padding: 28px 55px;">
                            <h4>For existing domains, all you have to do is to edit your website’s DNS Record and point it to our IP. After you have done that, you are all set. Note that this will take up to 48 hours to propagate.</h4>
                            <br/>
                            <h4>How do I create an A record to point my domain to an IP address?</h4>
                            <br/>
                            <h4>To create an A-Record, please follow these steps:</h4>
                            <ul>
                                <li>Sign in to your Hosting Account</li>
                                <li>Select your Domain that you want to move over to <strong>AgentSquared Servers</strong>.</li>
                                <li>Edit the DNS Settings of the selected Domain and Change the A-Record to <strong>104.236.110.186</strong>.</li>
                                <li>Then hit <strong>Save</strong>.</li> 
                            </ul>
                        </div>
                        <!-- <h1 class="page-header">
                            How to Change Your Custom Domain
                        </h1>
                        <p>
                            DNS, or "Domain Name System", is a naming system that is used to convert a server's host name into an IP address. DNS is what binds a domain name to a web server that is hosting that domain's content.
                        </p>
                        <p>
                            In this guide, we will bind a domain name to one of our Droplets by changing the domain name servers with the domain's registrar. The instructions will differ slightly depending on the domain registrar that you made your domain purchase through.
                        </p>
                        <p>Step-by-step guides can be found in this guide for the following registrars:</p>
                        <ul>
                            <li>GoDaddy</li>
                            <li>HostGator</li>
                            <li>Namecheap</li>
                            <li>1&1</li>
                            <li>Name.com</li>
                            <li>Network Solutions</li>
                            <li>eNom</li>
                            <li>Gandi</li>
                            <li>Register.com</li>
                            <li>A Small Orange</li>
                            <li>iwantmyname</li>
                            <li>Google Domains beta</li>
                        </ul>

                        <div class="panel-group" role="tablist" aria-multiselectable="true">
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab">
                                    <h4 class="panel-title">
                                        GoDaddy
                                        </a>
                                    </h4>

                                </div>
                                <div class="panel-collapse collapse in moreDetails" role="tabpanel">
                                    <div class="panel-body">
                                        <div class="col-md-12">
                                            <h3>Registrar: GoDaddy</h3>
                                            <ol>
                                                <li>
                                                    Sign in to your GoDaddy Account Manager.
                                                    <img src="<?= base_url(); ?>assets/images/dashboard/launch.png" alt="" class="img-responsive">
                                                </li>
                                                <li>
                                                    Next to Domains, click Launch.
                                                </li>
                                                <li>
                                                    Select the domain name that you want to use with your Droplet.
                                                </li>
                                                <li>
                                                    Under Nameservers, click Manage.
                                                    <img src="<?= base_url(); ?>assets/images/dashboard/godaddy-manage.png" alt="" class="img-responsive">
                                                </li>
                                                <li>
                                                    Under Setup type, select Custom.
                                                    <img src="<?= base_url(); ?>assets/images/dashboard/godaddy_custom.png" alt="" class="img-responsive">
                                                </li>
                                                <li>
                                                    Select Add Nameserver.
                                                </li>
                                                <li>
                                                    Enter the following nameservers:
                                                    <ul>
                                                        <li>ns1.digitalocean.com</li>
                                                        <li>ns2.digitalocean.com</li>
                                                        <li>ns3.digitalocean.com</li>
                                                    </ul>
                                                    <p>
                                                        Note: You'll need to hit Add Another for each nameserver. Once you have entered all three, you can hit Finish to return to the nameservers menu. Also note that you will not need to add any IP addresses when adding this type of nameserver.
                                                    </p>
                                                    <img src="<?= base_url(); ?>assets/images/dashboard/godaddy_add_nameserver.png" alt="" class="img-responsive">
                                                </li>
                                                <li>
                                                    Click Save to apply your changes. Now you are ready to move on to connecting the domain with your Droplet in the DigitalOcean control panel. Check out the Conclusion section at the end of this article to read on what to do next.
                                                    <img src="<?= base_url(); ?>assets/images/dashboard/godaddy_finish.png" alt="" class="img-responsive">
                                                </li>
                                            </ol>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab">
                                    <h4 class="panel-title">
                                        <a role="button" data-toggle="collapse" data-parent="#accordion" aria-expanded="true">
                                            HostGator
                                        </a>
                                    </h4>
                                </div>
                                <div class="panel-collapse collapse in" role="tabpanel">
                                    <div class="panel-body">
                                        <div class="col-md-12">
                                            <h3>Registrar: HostGator</h3>
                                            <ol>
                                                <li>
                                                    Click on Domains, then click on the Manage Domains tab.
                                                    <img src="<?= base_url(); ?>assets/images/dashboard/hostgator_manage_domains.png" alt="" class="img-responsive">
                                                </li>
                                                <li>
                                                    Sign in to your HostGator account.
                                                </li>
                                                <li>Select the domain name that you want to use with your Droplet.</li>
                                                <li>
                                                    Click on the Name Servers tab.
                                                </li>
                                                <li>
                                                    Enter the following nameservers:
                                                    <ul>
                                                        <li>ns1.digitalocean.com</li>
                                                        <li>ns2.digitalocean.com</li>
                                                        <li>ns3.digitalocean.com</li>
                                                    </ul>
                                                    
                                                </li>
                                                <li>Click Save Name Servers to apply your changes. Now you are ready to move on to connecting the domain with your Droplet in the DigitalOcean control panel. Check out the Conclusion section at the end of this article to read on what to do next.</li>
                                            </ol>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab">
                                    <h4 class="panel-title">
                                        <a role="button" data-toggle="collapse" data-parent="#accordion" aria-expanded="true">
                                        Namecheap
                                        </a>
                                    </h4>
                                </div>
                                <div class="panel-collapse collapse in" role="tabpanel">
                                    <div class="panel-body">
                                        <div class="col-md-12">
                                            <h3>Registrar: Namecheap</h3>
                                            <ol>
                                                <li>Sign in to your Namecheap account and go to Manage Domains.</li>
                                                <li>Select the domain name that you want to use with your Droplet.</li>
                                                <li>
                                                    Click on Transfer DNS to Webhost.
                                                    <img src="<?= base_url(); ?>assets/images/dashboard/namecheap_domain_menu.png" alt="" class="img-responsive">
                                                </li>
                                                <li>Select Specify Custom DNS Servers.</li>
                                                <li>
                                                    Enter the following nameservers:
                                                    <ul>
                                                        <li>ns1.digitalocean.com</li>
                                                        <li>ns2.digitalocean.com</li>
                                                        <li>ns3.digitalocean.com</li>
                                                    </ul>
                                                    <img src="<?= base_url(); ?>assets/images/dashboard/namecheap_nameservers.png" alt="" class="img-responsive">
                                                </li>
                                                <li>Click Save changes to apply your changes. Now you are ready to move on to connecting the domain with your Droplet in the DigitalOcean control panel. Check out the Conclusion section at the end of this article to read on what to do next.</li>
                                            </ol>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab">
                                    <h4 class="panel-title">
                                        <a href = "javascript:void(0)" class="collapsed" >1&1</a>
                                    </h4>
                                </div>
                                <div class="panel-collapse collapse in" role="tabpanel">
                                    <div class="panel-body">
                                        <div class="col-md-12">
                                            <h3>Registrar: 1&1</h3>
                                            <ol>
                                                <li>
                                                    Sign in to your 1&1 account and go to Domain Center.
                                                    <img src="<?= base_url(); ?>assets/images/dashboard/1and1_domain_center.png" alt="" class="img-responsive">
                                                </li>
                                                <li>Select the domain name that you want to use with your Droplet.</li>
                                                <li>
                                                    Select the DNS Settings tab, then select Edit.
                                                    <img src="<?= base_url(); ?>assets/images/dashboard/1and1_dns_settings.png" alt="" class="img-responsive">
                                                </li>
                                                <li>
                                                    Under Name servers, select Other name servers.
                                                    <img src="<?= base_url(); ?>assets/images/dashboard/1and1_other_name_servers.png" alt="" class="img-responsive">
                                                </li>
                                                <li>
                                                    Enter the following nameservers:
                                                    <ul>
                                                        <li>ns1.digitalocean.com</li>
                                                        <li>ns2.digitalocean.com</li>
                                                        <li>ns3.digitalocean.com</li>
                                                    </ul>
                                                    <p>Note: You'll need to select My secondary name servers under Additional name servers for the additional nameserver lines to appear.</p>
                                                    <img src="<?= base_url(); ?>assets/images/dashboard/1and1_name_server_settings.png" alt="" class="img-responsive">
                                                </li>
                                                <li>Click Save to apply your changes. Now you are ready to move on to connecting the domain with your Droplet in the DigitalOcean control panel. Check out the Conclusion section at the end of this article to read on what to do next.</li>
                                            </ol>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab">
                                    <h4 class="panel-title">
                                        <a href = "javascript:void(0)" class="collapsed" >Name.com</a>
                                    </h4>
                                </div>
                                <div class="panel-collapse collapse in" role="tabpanel">
                                    <div class="panel-body">
                                        <div class="col-md-12">
                                            <h3>Registrar: Name.com</h3>
                                            <ol>
                                                <li>
                                                    Sign in to your Name.com account.
                                                </li>
                                                <li>Select the domain name that you want to use with your Droplet.</li>
                                                <li>
                                                    Select the Nameservers tab, then select Delete All to remove the nameservers that are currently in place.
                                                    <img src="<?= base_url(); ?>assets/images/dashboard/name_dotcom_default_nameservers.png" alt="" class="img-responsive">
                                                </li>
                                                <li>
                                                    Enter the following nameservers:
                                                    <ul>
                                                        <li>ns1.digitalocean.com</li>
                                                        <li>ns2.digitalocean.com</li>
                                                        <li>ns3.digitalocean.com</li>
                                                    </ul>
                                                    <img src="<?= base_url(); ?>assets/images/dashboard/name_dotcom_add_nameservers.png" alt="" class="img-responsive">
                                                </li>
                                                <li>
                                                    Click Apply Changes. Now you are ready to move on to connecting the domain with your Droplet in the DigitalOcean control panel. Check out the Conclusion section at the end of this article to read on what to do next.
                                                </li>
                                            </ol>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab">
                                    <h4 class="panel-title">
                                        <a href = "javascript:void(0)" class="collapsed" >Network Solutions</a>
                                    </h4>
                                </div>
                                <div class="panel-collapse collapse in" role="tabpanel">
                                    <div class="panel-body">
                                        <div class="col-md-12">
                                            <h3>Registrar: Network Solutions</h3>
                                            <ol>
                                                <li>
                                                    Sign in to your Network Solutions account.
                                                </li>
                                                <li>
                                                    Select My Domain Names.
                                                    <img src="<?= base_url(); ?>assets/images/dashboard/network_solutions_my_domain_names.png" alt="" class="img-responsive">
                                                </li>
                                                <li>
                                                    Find the domain name that you want to use with your Droplet, then select Change Where Domain Points.
                                                    <img src="<?= base_url(); ?>assets/images/dashboard/network_solutions_change_where_domain_points.png" alt="" class="img-responsive">
                                                </li>
                                                <li>
                                                    Select Domain Name Server (DNS), then select Continue.
                                                    <img src="<?= base_url(); ?>assets/images/dashboard/network_solutions_domain_name_server.png" alt="" class="img-responsive">
                                                </li>
                                                <li>
                                                    Enter the following nameservers:
                                                    <ul>
                                                        <li>ns1.digitalocean.com</li>
                                                        <li>ns2.digitalocean.com</li>
                                                        <li>ns3.digitalocean.com</li>
                                                    </ul>
                                                    <img src="<?= base_url(); ?>assets/images/dashboard/network_solutions_add_nameservers.png" alt="" class="img-responsive">
                                                </li>
                                                <li>Select Continue, then confirm your changes at the next page by selecting Apply Changes. Now you are ready to move on to connecting the domain with your Droplet in the DigitalOcean control panel. Check out the Conclusion section at the end of this article to read on what to do next.</li>
                                            </ol>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab">
                                    <h4 class="panel-title">
                                        <a href = "javascript:void(0)" class="collapsed" >eNom</a>
                                    </h4>
                                </div>
                                <div class="panel-collapse collapse in" role="tabpanel">
                                    <div class="panel-body">
                                        <div class="col-md-12">
                                            <h3>Registrar: eNom</h3>
                                            <ol>
                                                <li>
                                                    Sign in to your eNom account.
                                                </li>
                                                <li>
                                                    Under Domains, select Registered. If you have multiple domains registered with eNom, select the domain name that you want to use with your Droplet.
                                                    <img src="<?= base_url(); ?>assets/images/dashboard/enom_registered_domains.png" alt="" class="img-responsive">
                                                </li>
                                                <li>
                                                    Select DNS Server Settings.
                                                </li>
                                                <li>
                                                    Under User our Name Servers?, select Custom.
                                                </li>
                                                <li>
                                                    Enter the following nameservers:
                                                    <ul>
                                                        <li>ns1.digitalocean.com</li>
                                                        <li>ns2.digitalocean.com</li>
                                                        <li>ns3.digitalocean.com</li>
                                                    </ul>
                                                    <img src="<?= base_url(); ?>assets/images/dashboard/enom_add_nameservers.png" alt="" class="img-responsive">
                                                </li>
                                                <li>Select save, then confirm your changes in the popup by selecting OK. Now you are ready to move on to connecting the domain with your Droplet in the DigitalOcean control panel. Check out the Conclusion section at the end of this article to read on what to do next.</li>
                                            </ol>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab">
                                    <h4 class="panel-title">
                                        <a href = "javascript:void(0)" class="collapsed" >Gandi</a>
                                    </h4>
                                </div>
                                <div class="panel-collapse collapse in" role="tabpanel">
                                    <div class="panel-body">
                                        <div class="col-md-12">
                                            <h3>Registrar: Gandi</h3>
                                            <ol>
                                                <li>
                                                    Sign in to your Gandi account.
                                                </li>
                                                <li>
                                                    Select the domain name that you want to use with your Droplet.
                                                </li>
                                                <li>
                                                    Under Name servers, select Modify servers.
                                                    <img src="<?= base_url(); ?>assets/images/dashboard/gandi_modify_servers.png" alt="" class="img-responsive">
                                                </li>
                                                <li>
                                                    Enter the following nameservers:
                                                    <ul>
                                                        <li>ns1.digitalocean.com</li>
                                                        <li>ns2.digitalocean.com</li>
                                                        <li>ns3.digitalocean.com</li>
                                                    </ul>
                                                    <img src="<?= base_url(); ?>assets/images/dashboard/gandi_add_nameservers.png" alt="" class="img-responsive">
                                                </li>
                                                <li>Select Submit to apply your changes. Now you are ready to move on to connecting the domain with your Droplet in the DigitalOcean control panel. Check out the Conclusion section at the end of this article to read on what to do next.</li>
                                            </ol>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab">
                                    <h4 class="panel-title">
                                        <a href = "javascript:void(0)" class="collapsed" >Register.com</a>
                                    </h4>
                                </div>
                                <div class="panel-collapse collapse in" role="tabpanel">
                                    <div class="panel-body">
                                        <div class="col-md-12">
                                            <h3>Registrar: Register.com</h3>
                                            <ol>
                                                <li>
                                                    Sign in to your Register.com account.
                                                </li>
                                                <li>
                                                    Select the domain name that you want to use with your Droplet.
                                                </li>
                                                <li>
                                                    Under DOMAIN NAME SYSTEM SERVERS (DNS SERVERS), enter the following nameservers into the New DNS Server fields:
                                                    <ul>
                                                        <li>ns1.digitalocean.com</li>
                                                        <li>ns2.digitalocean.com</li>
                                                        <li>ns3.digitalocean.com</li>
                                                    </ul>
                                                    <img src="<?= base_url(); ?>assets/images/dashboard/register_dotcom_add_nameservers.png" alt="" class="img-responsive">
                                                </li>
                                                <li>Select Continue, then confirm your changes at the next page by selecting Continue. Now you are ready to move on to connecting the domain with your Droplet in the DigitalOcean control panel. Check out the Conclusion section at the end of this article to read on what to do next.</li>
                                            </ol>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab">
                                    <h4 class="panel-title">
                                        <a href = "javascript:void(0)" class="collapsed" >A Small Orange</a>
                                    </h4>
                                </div>
                                <div class="panel-collapse collapse in" role="tabpanel">
                                    <div class="panel-body">
                                        <div class="col-md-12">
                                            <h3>Registrar: A Small Orange</h3>
                                            <ol>
                                                <li>
                                                    Sign in to your A Small Orange account and select My Domains.
                                                    <img src="<?= base_url(); ?>assets/images/dashboard/a_small_orange_my_domains.png" alt="" class="img-responsive">
                                                </li>
                                                <li>
                                                    Find the domain name that you want to use with your Droplet, then select Manage Domain to the right of that domain name.
                                                </li>
                                                <li>
                                                    By default, A Small Orange locks your domain to prevent it from being transferred away without your authorization. This means that before we can change the nameservers, we'll need to disable this lock. Select the Registrar Lock tab, then select Disable Registrar Lock.
                                                </li>
                                                <li>Select the Nameservers tab.</li>
                                                <li>
                                                    Enter the following nameservers:
                                                    <ul>
                                                        <li>ns1.digitalocean.com</li>
                                                        <li>ns2.digitalocean.com</li>
                                                        <li>ns3.digitalocean.com</li>
                                                    </ul>  
                                                    <img src="<?= base_url(); ?>assets/images/dashboard/a_small_orange_add_nameservers.png" alt="" class="img-responsive">                                           
                                                </li>
                                                <li>
                                                    Select Change Nameservers to apply your changes. Now you are ready to move on to connecting the domain with your Droplet in the DigitalOcean control panel. Check out the Conclusion section at the end of this article to read on what to do next.
                                                </li>
                                            </ol>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab">
                                    <h4 class="panel-title">
                                        <a href = "javascript:void(0)" class="collapsed" >iwantmyname</a>
                                    </h4>
                                </div>
                                <div class="panel-collapse collapse in" role="tabpanel">
                                    <div class="panel-body">
                                        <div class="col-md-12">
                                            <h3>Registrar: iwantmyname</h3>
                                            <ol>
                                                <li>
                                                    Sign in to your iwantmyname account and select the Domains tab.
                                                </li>
                                                <li>
                                                    Select the domain name that you want to use with your Droplet.
                                                </li>
                                                <li>
                                                    Under Nameservers, select update nameservers.
                                                </li>
                                                <li>Unlike many other domain registrars, iwantmyname features a menu of popular web hosts with preconfigured DNS settings.
                                                    <img src="<?= base_url(); ?>assets/images/dashboard/iwantmyname_popular_settings.png" alt="" class="img-responsive">
                                                </li>
                                                <li>
                                                    Enter the following nameservers:
                                                    <ul>
                                                        <li>ns1.digitalocean.com</li>
                                                        <li>ns2.digitalocean.com</li>
                                                        <li>ns3.digitalocean.com</li>
                                                    </ul>  
                                                    <img src="<?= base_url(); ?>assets/images/dashboard/a_small_orange_add_nameservers.png" alt="" class="img-responsive">
                                                    <p>
                                                        Choose DigitalOcean (ns1-3.digitalocean.com) from the dropdown menu, and the fields below will be automatically filled in with the correct settings.
                                                    </p>   
                                                    <img src="<?= base_url(); ?>assets/images/dashboard/iwantmyname_add_nameservers.png" alt="" class="img-responsive">                                    
                                                </li>
                                                <li>
                                                    Select Update nameservers to apply your changes. Now you are ready to move on to connecting the domain with your Droplet in the DigitalOcean control panel. Check out the Conclusion section at the end of this article to read on what to do next.
                                                </li>
                                            </ol>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab">
                                    <h4 class="panel-title">
                                        <a href = "javascript:void(0)" class="collapsed" >Google Domains beta</a>
                                    </h4>
                                </div>
                                <div class="panel-collapse collapse in" role="tabpanel">
                                    <div class="panel-body">
                                        <div class="col-md-12">
                                            <h3>Registrar: Google Domains beta</h3>
                                            <ol>
                                                <li>
                                                    Sign in to your Google Domains account.
                                                </li>
                                                <li>
                                                    Select the domain name that you want to use with your Droplet.
                                                </li>
                                                <li>
                                                    Select the Advanced tab, then select Use custom name servers.
                                                    <img src="<?= base_url(); ?>assets/images/dashboard/google_domains_use_custom_name_servers.png" alt="" class="img-responsive">
                                                </li>
                                                <li>
                                                    Enter the following nameservers:
                                                    <ul>
                                                        <li>ns1.digitalocean.com</li>
                                                        <li>ns2.digitalocean.com</li>
                                                        <li>ns3.digitalocean.com</li>
                                                    </ul> 
                                                    <img src="<?= base_url(); ?>assets/images/dashboard/google_domains_add_nameservers.png" alt="" class="img-responsive">
                                                    <p>
                                                        Note: You'll need to hit the + to the right of the nameserver field to make more fields visible.
                                                    </p>
                                                </li>
                                                <li>
                                                    Select Save to apply your changes. Now you are ready to move on to connecting the domain with your Droplet in the DigitalOcean control panel. Check out the Conclusion section below to read on what to do next.
                                                </li>
                                            </ol>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab">
                                    <h4 class="panel-title">
                                        <a href = "javascript:void(0)" class="collapsed" >Existing Domains</a>
                                    </h4>
                                </div>
                                <div class="panel-collapse collapse in" role="tabpanel">
                                    <div class="panel-body">
                                        <div class="col-md-12">
                                            <h3>Registrar: Existing Domains</h3>
                                            <p>For existing domains, all you have to do is to edit your website’s DNS Record and point it to our IP. After you have done that, you are all set. Note that this will take up to 48 hours to propagate.</p>
                                            <p>How do I create an A record to point my domain to an IP address?</p>
                                            <p>To create an A-Record, please follow these steps:</p>
                                            <ol>
                                                <li>
                                                    Sign in to your Hosting Account.
                                                </li>
                                                <li>
                                                    Select your Domain that you want to move over to AgentSquared Servers.
                                                </li>
                                                <li>
                                                   Edit the DNS Settings of the selected Domain and Change the A-Record to <strong>104.236.110.186</strong>.
                                                </li>
                                                <li>
                                                    Then hit Save.
                                                </li>
                                            </ol>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>-->
                    </div>
                    
                </div>
            </div>
    </div>
    <script src="<?= base_url()?>assets/js/dashboard/jquery.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>    
    <script src="<?= base_url()?>assets/js/dashboard/main.js"></script>  
</body>

</html>
