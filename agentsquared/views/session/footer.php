  <!-- Agent IDX Website -->
  <div id="agent-idx-website-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <div class="col-md-offset-5">
            <div class="main-header-container">
              <h3 class="main-header">Sell More Homes</h3>
              <h4 class="main-header">Build Your Online Presence Today!</h4>  
            </div>
          </div>
        </div>
        <div class="modal-body">
          <div class="col-md-offset-5">
            <div class="main-body">
              <h4>Websites integrate with your MLS through a direct connection in FlexMLS. Buyers can search your website in your local market with real-time, instant updates.</h4>
              <ul class="idx-modal-list">
                <li>Capture Leads</li>
                <li>Add Unlimited Pages</li>
                <li>Add Saved Searches to any page</li>
              </ul>
              <div class="pricing-section clearfix">
                <div class="pricing-header">
                  <h4 class="text-center">PRICING PLANS</h4>
                </div>
                <div class="col-sm-offset-3 col-md-offset-3 col-md-6 col-sm-6 no-padding-left">
                  <div class="large-monthly pricing">
                    <h1>
                      <span class="dollar">$</span>
                      99
                      <span class="permonth">/month</span>
                    </h1>
                    <p>Billed Monthly<!--  - <span class="discount">Save 20%</span> --></p>
                    <div class="bottom-trial">
                      <p>15 days FREE trial<!-- . $59 monthly until canceled. --></p>  
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-12">
                <a href="http://www.sparkplatform.com/appstore/apps/instant-agent-idx-website" class="btn btn-default btn-block" target="_blank">Click Here to Start Your FREE 15 Days Trial</a>  
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary">Save changes</button>
        </div>
      </div>
    </div>
  </div>




  <!-- Modal Pricing -->
  <div class="modal fade" id="modalPricing" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">You have 14 FREE trial days left!</h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-md-5 col-sm-5">
              <div class="price-item">
                <h4 class="price-title">Instant IDX Website</h4>
                <div class="price-desc">
                   <p><sup>$</sup>99</p>
                   <small>per month</small>
                </div>
                <a href="javascript:void(0)" data-type="monthly" data-price="99" id="purchase-idx-monthly" class="price-text">Purchase</a>
              </div>
            </div>
            <div class="col-md-1 col-sm-1">
              <p class="or-text">OR</p>
            </div>
            <div class="col-md-5 col-sm-5">
              <div class="price-item">
                <h4 class="price-title">IDX Annual</h4>
                <div class="price-desc">
                   <p><sup>$</sup>990</p>
                   <small>per year</small>
                </div>
                <a href="javascript:void(0)" data-type="annual" data-price="990" id="purchase-idx-yearly" class="price-text">Purchase</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Modal Payment -->
  <div class="modal fade" id="modalPayment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-label" id="myModalLabel">Checkout</h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-md-12">
              <p class="modal-text">Review Your Order :</p>
              <div class="table-responsive">
                <table class="table table-striped">
                  <thead>
                    <tr>
                      <th>Domain Name</th>
                      <th>Amount</th>
                      <th>Surcharge</th>
                      <th>Sub-Total</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>3060beachforeststreet.com</td>
                      <td>$9.98</td>
                      <td>$1</td>
                      <td>$10.98</td>
                      <td><a href="#"><i class="fa fa-close"></i></a></td>
                    </tr>
                    <!-- <tr>
                      <td>3060beachforeststreet.com</td>
                      <td>$9.98</td>
                      <td>$1</td>
                      <td>$10.98</td>
                      <td><a href="#"><i class="fa fa-close"></i></a></td>
                    </tr>
                    <tr>
                      <td>3060beachforeststreet.com</td>
                      <td>$9.98</td>
                      <td>$1</td>
                      <td>$10.98</td>
                      <td><a href="#"><i class="fa fa-close"></i></a></td>
                    </tr>
                    <tr>
                      <td>3060beachforeststreet.com</td>
                      <td>$9.98</td>
                      <td>$1</td>
                      <td>$10.98</td>
                      <td><a href="#"><i class="fa fa-close"></i></a></td>
                    </tr> -->
                  </tbody>
                </table>
              </div>
            </div>
            <div class="col-md-12 text-right">
              <p class="text-total">Total: <span>$30.98</span></p>
            </div>
            <div class="col-md-12">
              <div class="border-gray"></div>
            </div>
            <div class="col-md-6">
              <div class="row">
                <div class="col-md-5">
                  <p class="modal-text">Payment Method:</p>
                </div>
                <div class="col-md-6">
                  <img src="<?= base_url()?>assets/images/dashboard/payment_method.png" class="cc-accepted">
                </div>
              </div>
              <div class="cc-detail">
                <h3>Credit Card Details:</h3>
                <form action="">
                  <div class="row">
                    <div class="col-md-12 row mb-15px">
                      <div class="form-group">
                        <label for="nameoc" class="col-md-4">Name on card:</label>
                        <div class="col-md-8 no-padding">
                          <input type="text" class="form-control" id="nameoc">
                        </div>
                      </div>
                    </div>
                    <div class="col-md-12 row mb-15px">
                      <div class="form-group">
                        <label for="cardnumber" class="col-md-4">Card number:</label>
                        <div class="col-md-8 no-padding">
                          <input type="text" class="form-control" id="cardnumber">
                        </div>
                      </div>
                    </div>
                    <div class="col-md-12 row mb-15px">
                      <div class="form-group">
                        <label for="expirationdate" class="col-md-4">Expiration date:</label>
                        <div class="col-md-1 no-padding">
                          <select class="form-control">
                            <option>01</option>
                            <option>02</option>
                          </select>
                        </div>
                        <div class="col-md-1 no-padding">
                          <select class="form-control">
                            <option>01</option>
                            <option>02</option>
                          </select>
                        </div>
                        <label for="expirationdate" class="col-md-3 text-right">CVV code <i class="fa fa-question-circle"></i>:</label>
                        <div class="col-md-3 no-padding">
                          <input type="text" class="form-control" name="">
                        </div>
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
            <div class="col-md-6 text-center mclear">
              <h1 class="payment-total">$30.98</h1>
              <p class="payment-total-note1">Total is inclusive of VAT</p>
              <p class="text-left payment-total-note2">
                By clicking on the 'Place Order' button at the end of the order process, you are consenting to be bound by our terms and conditions contained in these Terms and Conditions and appearing anywhere on AgentSquared website.
              </p>
              <a href="#" class="btn btn-border-green">Place Order</a>
            </div>
            <div class="col-md-12">
              <div class="col-md-6">
                <ul class="list-link">
                  <li><a href="#" target="_blank">Disclaimer</a></li>
                  <li><a href="#" target="_blank">Privacy Policy</a></li>
                  <li><a href="#" target="_blank">Terms & Conditions</a></li>
                </ul>
              </div>
              <div class="col-md-6 text-right">
                <p class="powered-by">Powered By: <img src="<?= base_url()?>assets/images/dashboard/stripe-logo.png"></p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <footer class="main-footer footer-links">
      <div class="pull-right mobile-center">
        <b>AgentSquared</b> 
        <a href="https://www.facebook.com/AgentSquared/"><i class="fa fa-facebook-f"></i></a>
        <a href="https://twitter.com/agentsquared"><i class="fa fa-twitter"></i></a>
        <a href="https://plus.google.com/+Agentsquared/posts"><i class="fa fa-google"></i></a>
        <a href="https://www.linkedin.com/company/agentsquared"><i class="fa fa-linkedin"></i></a>
      </div>
      <strong>&nbsp;</strong>
    </div>
  </footer>
</div>
<!-- ./wrapper -->
    <script src="<?= base_url()?>assets/js/dashboard/jquery.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/additional-methods.min.js"></script>  
    <script src="<?= base_url()?>assets/js/dashboard/main.js"></script>    
    <script src="<?= base_url()?>assets/js/dashboard/bootstrap-filestyle.js"></script>
    <script src="<?= base_url()?>assets/js/dashboard/summernote.js"></script>
    <script src="<?= base_url()?>assets/js/dashboard/agent-social-media.js"></script>
    <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/1.4.5/numeral.min.js"></script>
    <script src="<?= base_url()?>assets/js/dashboard/bootstrap-datepicker.js"></script>
    <script src="<?= base_url()?>assets/js/dashboard/buyer-data-load.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.2/js/bootstrap-switch.min.js"></script>
    <!-- <script src="<?= base_url()?>assets/js/dashboard/seller-data-load.js"></script>-->

    <!-- Slider Photos JS -->
    <script src="<?= base_url()?>assets/js/slider/jquery.knob.js"></script>
    <script src="<?= base_url()?>assets/js/slider/jquery.ui.widget.js"></script>
    <script src="<?= base_url()?>assets/js/slider/jquery.iframe-transport.js"></script>
    <script src="<?= base_url()?>assets/js/slider/jquery.fileupload.js"></script>
    <script src="<?= base_url()?>assets/js/slider//script.js"></script>

    <script src="<?= base_url()?>assets/js/dashboard/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="<?= base_url()?>assets/js/dashboard/fastclick.min.js"></script>
    <script src="<?= base_url()?>assets/js/dashboard/app.min.js"></script>

    <script src="<?= base_url()?>assets/js/plugins/jquery.msgBox.js"></script>

    <!-- Location -->
  <!--<script type="text/javascript" src='http://maps.google.com/maps/api/js?sensor=false&libraries=places&key=AIzaSyBHzIGp5FJxKOLe5NtgKoLlNg6beKYdtm8'></script> -->
  <script type="text/javascript" src='https://maps.google.com/maps/api/js?sensor=false&libraries=places&key=AIzaSyBHzIGp5FJxKOLe5NtgKoLlNg6beKYdtm8'></script>
    <script src="<?= base_url()?>assets/js/dashboard/locationpicker.jquery.min.js"></script>

    <!-- File Uploader -->
    <script src="<?= base_url()?>assets/js/dashboard/fileUploader.min.js"></script>

    <!-- rating -->
    <script src="<?= base_url()?>assets/js/dashboard/plugins/jquery.rateyo.min.js"></script>

  <?php if(isset($js)) :
    foreach($js as $key => $val ) :  ?>
      <script src="<?= base_url()?>assets/js/dashboard/<?php echo $val?>" type="text/javascript"> </script>
  <?php endforeach;?>
  <?php endif;?>
  <?php if($this->uri->segment(1) == "crm") { ?>
  <script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"></script>
    <script>
    new Morris.Bar({
          // ID of the element in which to draw the chart.
          element: 'salesummary',
          // Chart data records -- each entry in this array corresponds to a point on
          // the chart.
          data: [
            { sold: 'January', Rent:2, Sale:1 },
            { sold: 'February', Rent:5, Sale:5 },
            { sold: 'March', Rent:2, Sale:1 },
            { sold: 'April', Rent:8, Sale:0 },
            { sold: 'May', Rent:1, Sale:6 }
          ],
          // The name of the data record attribute that contains x-values.
          xkey: 'sold',
          // A list of names of data record attributes that contain y-values.
          ykeys: ['Rent','Sale'],
          // Labels for the ykeys -- will be displayed when you hover over the
          // chart.
          labels: ['Rentals Sold','House Sales']
        });
    </script>
  <?php } ?>
  <?php if($this->uri->segment(2) == "schedules") { ?>
    <script type="text/javascript">
      $(document).ready(function(){
        $('.datepicker-area').datepicker({
            'todayHighlight':true
        });

        $('.select-date .input-group.date').datepicker({
            'todayHighlight':true
        });
      });
    </script>
  <?php } ?>
  <script src="<?= base_url()?>assets/js/dashboard/owl.carousel.js"></script>
    <script type="text/javascript">
      $(document).ready(function(){

        $('#audience-map').hide();
        $('#audience-map').locationpicker({
            location: {
                latitude: 0,
                longitude: 0
            },
            radius: 300,
            inputBinding: {
                latitudeInput: $('#us3-lat'),
                longitudeInput: $('#us3-lon'),
                radiusInput: $('#us3-radius'),
                locationNameInput: $('#audience-location')
            },
            enableAutocomplete: true,
            onchanged: function (currentLocation, radius, isMarkerDropped) {
                // Uncomment line below to show alert on each Location Changed event
                // var addressComponents = $(this).locationpicker('map').location.addressComponents;
                // updateControls(addressComponents);
            }
        });

        $( "#startdatepicker" ).datepicker({format: "dd-mm-yyyy"});
        $( "#enddatepicker" ).datepicker({format: "dd-mm-yyyy"});

        $( "#startdatepickerfb" ).datepicker({format: "dd-mm-yyyy"});
        $( "#enddatepickerfb" ).datepicker({format: "dd-mm-yyyy"});

        $(".property-detail-images").owlCarousel({
            navigation : true,
            pagination : true,
            slideSpeed : 700,
            paginationSpeed : 400,
            navigationText: ["<i class='fa fa-angle-left fa-lg'></i> <span>prev</span>","<span>next</span> <i class='fa fa-angle-right fa-lg'></i>"],
            items : 4,
            itemsDesktop : [1199,3],
            itemsDesktopSmall : [979,3]
            // "singleItem:true" is a shortcut for:
            // items : 1, 
            // itemsDesktop : false,
             // itemsDesktopSmall : false,
            // itemsTablet: false,
            // itemsMobile : false
        });

        

      });
    </script>

    <?php if($this->uri->segment(1) == "agent_social_media" || $this->uri->segment(2) == "activate" || $this->uri->segment(1) == "signup" ) { ?>
      <script src="<?= base_url()?>assets/js/socialmedia/home-agent.js"></script>
      <script src="<?= base_url()?>assets/js/socialmedia/popup.js"></script>
      <script>
            var fb_url = <?php echo json_encode($this->facebook->login_url());?>;
            var linkdn_url = <?php echo json_encode(site_url('agent_social_media/linkedin_login'));?>;
            var twitt_url = <?php echo json_encode(site_url('agent_social_media/twitter_redirect'));?>;
      </script>
      <script>
        function PopulateOption() {
            var dropdown = document.getElementById("postSelected");
            var field = document.getElementById("post-area");
            field.value = dropdown.value;
        }
      </script>

      <!-- <?php
        if(isset($_GET['post_socialmedia'])){
      ?> -->
        <script type="text/javascript">
             window.close();
             window.opener.location.reload(true);
        </script>
      <!-- <?php } ?> -->
          
    <?php } ?>


    <?php if($this->uri->segment(1) == "menu" || $this->uri->segment(1) == "customize_homepage" ) { ?>

      <script type="text/javascript">
        $(document).ready(function(){

          //custom menu link validation
          $("#customLinkMenu").validate({
                rules: {
                  custom_menu_url: {
                    required: true,
                  },
                  custom_menu_name: {
                    required: true,
                  }
                }
            });

          //Customize homepage edit label
          $("#navLabel").validate({
                rules: {
                  search_name: {
                    required: true,
                  },
                }
            });
        });
      </script>
    <?php } ?>

    <?php if($this->uri->segment(1) == "agent_social_media" || $this->uri->segment(2) == "activate" ) { ?>

     <script type="text/javascript">
      $(document).ready(function(){
  
        $(".panel-collapse").hide();
        $(".moreDetails").show();
        
        $(".panel-heading").on("click",function(){
          var $this = $(this);
          
          $this.stop().next().slideDown().parent().siblings().find('.panel-collapse').slideUp();
          $this.find('.fa-plus-circle').removeClass('fa-plus-circle').addClass('fa-minus-circle')
            .parents('.panel-default').siblings().find('.fa-minus-circle').removeClass('fa-minus-circle').addClass('fa-plus-circle');
        });

       });
    </script>
    <?php } ?>

    <script type="text/javascript">
      // This identifies your website in the createToken call below
      //Stripe.setPublishableKey('pk_test_EIuy5UKYdWcXPNBDUZIh50vD');
      Stripe.setPublishableKey('pk_live_LV03T30zGeqwNUUDKYKyG6zZ');
    </script> 
    <script type="text/javascript">
      jQuery(function($) {
        $('#stripe_sbt').submit(function(e) {

          $(".loader").fadeIn("slow");

          var $form = $('#stripe_sbt');
          
          // Disable the submit button to prevent repeated clicks
          $('#pay_now_btn').prop('disabled', true);
          Stripe.card.createToken($form, stripeResponseHandler);

          // Prevent the form from submitting with the default action
          return false;
        });


        var stripeResponseHandler = function(status, response) {
          var $form = $('#stripe_sbt');
          
          if (response.error) {
            $(".loader").fadeOut("slow");
            // Show the errors on the form
            $form.find('.payment-errors').css({"font-size" : "14px"});
            $form.find('.payment-errors').text(response.error.message);
            $('#pay_now_btn').prop('disabled', false);

          } else {
            // token contains id, last4, and card type
            var token = response.id;
            // Insert the token into the form so it gets submitted to the server
            $form.append($('<input type="hidden" name="stripeToken" />').val(token));
            // and re-submit
            //$form.get(0).submit();
            stripe_form_submit( $form.serialize() );
            
          }
        };

        function stripe_form_submit( post )
        {
            var url = base_url + "payment/pay";
            var datas = post;

            $.ajax({
                url : url,
                type : "post",
                data : datas,
                dataType: "json",
                success: function( data){
                    console.log(data);       
                    $(".loader").fadeOut("slow");        
                    if( data.success )
                    {
                      $(".payment-error").hide();
                      
                      $(".payment-message").fadeIn( function () {
                          $(".payment-message").html(data.message);
                          setTimeout(function(){
                            location.reload();
                          }, 1000);
                      });

                    }
                    else
                    {
                      
                      $(".payment-error").fadeIn( function () {
                          $(".payment-error").html(data.message);
                      });

                    }

                }
            });
        }
      });

      

</script>
  <script type="text/javascript">
      var launch_site = function(e)
      {
        e.preventDefault();
        var url = base_url + "dashboard/get_customer_infos";
        $('.launch-site-now').text('Loading...');
        $.ajax({
            url : url,
            type : "post",
            dataType: "json",
            beforeSend: function()
            {
              $('.launch-site-now').prop('disabled', true);
            },
            success: function( data)
            {
              console.log(data); 
              
              if( data.domain_name == null )
              {
                  window.location.href = base_url + "custom_domain" 
              }
              else
              {
                $('.launch-site-now').text('Launch your site now');
                //idx key
                $("#apikey").val(data.api_key);
                $("#apisecret").val(data.api_secret);
                //branding
                $("#sitename").val(data.site_name);
                $("#tagline").val(data.tag_line);
                //profile
                $("#fname").val(data.first_name);
                $("#email").val(data.email);
                $("#onumber").val(data.phone);
                //domain info
                $("#domainname").val(data.domain_name);
                $("#launch-site").modal("show");
              }
            }
        });
        
      };

      var launch_site_now_temp = function (e){
        e.preventDefault();
        var url = base_url + "dashboard/launch_site_now";
        $('.launch-site-now-temp').text('Loading...');

        $.ajax({
            url : url,
            type : "post",
            dataType: "json",
            beforeSend: function()
            {
              $('.launch-site-now-temp').prop('disabled', true);
            },
            success: function( data)
            {
              if(data.success)              
              {
                location.reload(true);
              }
              else
              {
                location.reload(true);   
              }
            }
        });
        
      };
      
      var check_agent_domain = function(e) {
        e.preventDefault();
        var url = base_url + "dashboard/check_domain_complete";

        $.ajax({
            url : url,
            type : "POST",
            dataType: "json",
            beforeSend: function() {
              console.log(url);
            },
            success: function(data) {
              if(data.success) {
                if(data.domain_complete) {
                  launch_site_now_temp(e);
                }
              } else {
                if(data.empty_domain) {
                  $("#message-container").css("display", "block");
                  $("#launch-response-message").html(" You don't have a domain yet. Click <a href='custom_domain'>here</a> to go to Custom Domain page.");
                } else {
                  $("#message-container").css("display", "block");
                  $("#launch-response-message").html("  Your custom domain may take up to 48 hours to work as the global DNS propagates.");
                }
              }
            }
        });
      }

      $(".launch-site-now").on("click", launch_site);
      $(".launch-site-now-temp").on("click", check_agent_domain);
  </script>
  <script>
    $(document).ready(function() {
      $("div.ratefixed").each(function(){
        $rate = $(this).attr("data-rate");
        $(this).rateYo({
            rating: $rate,
            readOnly: true,
            fullStar: true
        });
      });

      $(".message-modal").on('click', function(){
        $(".message-content").text($(this).attr("data-content"));
      });
    });
    /*$(function () {
      $(".ratefixed").rateYo({
          rating: 3,
          readOnly: true,
          fullStar: true
      });
    });*/
  </script>

<?php if($this->uri->segment(1) == "dashboard" ) { ?>

  <!-- DASHBOARD -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
  <script src="<?= base_url()?>assets/js/dashboard/morris.min.js"></script>
  <script>    
  </script>
<?php } ?>

<script type="text/javascript">
    $(window).load(function(){        
        //$('#modalPricing').modal('show');
        $("#purchase-idx-monthly").click( function (){
            $('#modalPayment').modal('show');
        });
    });
</script>

  <script type="text/javascript">
    function downloadJSAtOnload() {
      var element = document.createElement("script");
      element.src = "<?= base_url()?>assets/js/dashboard/main.js";
      document.body.appendChild(element);
    }
    
    if (window.addEventListener)
      window.addEventListener("load", downloadJSAtOnload, false);
    else if (window.attachEvent)
      window.attachEvent("onload", downloadJSAtOnload);
    else window.onload = downloadJSAtOnload;
  </script>
</body>
</html>
