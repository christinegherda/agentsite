<div class="row">
    <div class="col-md-12">    	

		<?php if (isset($update_listing)): ?>
			<?php echo $update_listing ?>
		<?php endif ?>

    	<?php if(!config_item("is_paid")) : ?>
    		<!-- <h4 id="launch-response-message"></h4> -->
        	<h4 class="alert alert-warning"><i class="fa fa-exclamation-triangle"></i> Your website is not live yet and is not yet visible to the public. <a href="javascript:void(0)" class="btn btn-warning btn-submit launch-site-now-temp pull-right"><i class="fa fa-globe"></i> Launch your site now</a></h4>
	    	<div class="alert alert-danger fade in" id="message-container" style="display:none;"><i class="fa fa-exclamation-triangle"></i>
              	<a href="#" class="close" data-dismiss="alert">&times;</a>
              	<span id="launch-response-message"></span>
          	</div>
	    	<?php else : ?>
	    		<?php if(!config_item("is_domain_completed")) : ?>
	    			<h4 class="alert alert-success"><i class="fa fa-exclamation-triangle"></i> Your custom domain may take up to 48 hours to work as the global DNS propagates. <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span style='color:#777' aria-hidden='true'>&times;</span></button></h4>
	    		<?php endif; ?>    		
   		<?php endif;?>
    </div>
</div>

<div id="launch-site" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static">	  
    <div class="modal-content">
	      <div class="modal-header">
	        <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> -->
	        <h4 class="modal-title">Launch my site now!</h4>
	      </div>
	      <div class="modal-body">
		        <div class="container">
				  	<div class="row">
				  		<div class="col-md-4">
				  			<h4><div class="label label-info">IDX Information</div></h4>
				  			<!-- <form>
								  <div class="form-group">
								    <label for="exampleInputEmail1">API Key</label>
								    <input type="text" class="form-control" value="" id="apikey" disabled >
								  </div>
								  <div class="form-group">
								    <label for="exampleInputPassword1">API Secret</label>
								     <input type="text" class="form-control" value="" id="apisecret" disabled >
								  </div>								  
							</form> -->
							<h4><div class="label label-info">Branding</div></h4>  			
				  			<form >
								<div class="form-group">
								    <label for="exampleInputEmail1">Site Name</label>
								    <input type="text" class="form-control" value="" id="sitename" style="width: 350px;" disabled >
								</div>
								<div class="form-group">
								    <label for="exampleInputPassword1">Tag Line</label>
								    <input type="text" class="form-control" value="" id="tagline" style="width: 350px;" disabled >
								</div>						  
							</form>
				  		</div>
				  		<div class="col-md-4">
				  			<h4><div class="label label-info">My Profile</div></h4>
				  			<form >
								<div class="form-group">
								    <label for="exampleInputPassword1">Name</label>
								     <input type="text" class="form-control" value="" id="fname"  style="width: 350px;" disabled >
								</div>	
								<div class="form-group">
								    <label for="exampleInputPassword1">Email</label>
								     <input type="text" class="form-control" value="" id="email" style="width: 350px;" disabled >
								</div>	
								<div class="form-group">
								    <label for="exampleInputPassword1">Office Number</label>
								     <input type="text" class="form-control" value="" id="onumber" style="width: 350px;" disabled >
								</div>							  
							</form>
				  			<h4><div class="label label-info">Custom Domain</div></h4>	  			
				  			<form >
								<div class="form-group">
								    <label for="exampleInputPassword1">Domain Name</label>
								     <input type="text" class="form-control" value="" id="domainname"  style="width: 350px;" disabled >
								</div>													  
							</form>	
						</div>
				  		<div class="col-md-4">
				  			<h4><div class="label label-info">Themes</div></h4>
				  			<div class="theme-picture clearfix">
                            <img src="<?= base_url()?>assets/images/dashboard/arillo.jpg" alt="" class="img-responsive">    
                            <div class="theme-description">
                                <div class="col-md-5 col-sm-5">
                                    <h4 class="theme-name">Agent Haven</h4>
                                </div>
                                <div class="col-md-7 col-sm-7">
                                    <div id="selected" class="activated agent-haven-selected">
                                        <button class="btn btn-default" data-toggle="modal" data-target="#preview-site">Selected</button>    
                                    </div>
                                    <div id="preview" class="activate-buttons">
                                        <form action="<?php echo site_url('choose_theme/activate_theme'); ?>" method="post" id="choose_theme">
                                            <input type="hidden" value="agent-haven" name="theme"/>
                                        </form> 
                                        <button class="btn btn-default" data-toggle="modal" data-target="#preview-site">Preview</button>  
                                    </div>
                                </div>
                            </div>
                            <div class="preview-button">
                                <h4>Agent Haven</h4>
                                <p>1 Slider Home Page</p>
                                <p>Very professional and minimalistic theme for real estate. Contains agent info on home page
                                and featured property listings with their locations.</p>
                            </div>
                        </div>	
						</div>
				  	</div>
				  	<hr>
				  	<div class="row">
				  		<div class="col-md-12">
				  			<h4><div class="label label-info">Payment Cart</div></h4>
				  			<table class="table table-striped">
							 	<thead>
							 		<tr>
							 			<th></th>
							 			<th>Product Name</th>
							 			<th>Description</th>
							 			<th>Amount</th>
							 			<th>Total Amount</th>
							 		</tr>
							 	</thead>
							 	<tbody>
							 		<tr>
							 			<td></td>
							 			<td>IDX Website</td>
							 			<td>test description</td>
							 			<td>$125</td>
							 			<td>$125</td>
							 		</tr>							 		
							 		
							 	</tbody>
							</table>
				  		</div>				
				  	</div>
				  	<hr>
				  	<br>
				  	<div class="row">
				  		<div class="col-md-12">
				  			<h4><div class="label label-info">Enter Your Card Details</div></h4>
				  			</br>
				  			<form class="form-horizontal stripe_sbt" id="stripe_sbt" action="" method="POST">
				  				<div class="col-md-12"><div class="col-md-12 payment-errors label label-danger"></div><br></div>
				  				<div class="col-md-12"><div class="col-md-12 payment-message label label-success"></div><br></div>
								<div class="col-md-6">
								  	<div class="form-group">
									    <label for="inputEmail3" class="col-sm-3 control-label">Card Number</label>
									    <div class="col-sm-8">
									    	<input type="input" class="form-control" size="20"  id="" data-stripe="number" placeholder="Card Number" required >
									    </div>
									</div>
									<div class="form-group">
									    <label for="inputPassword3" class="col-sm-3 control-label">CVV</label>
									    <div class="col-sm-8">
									    	<input type="input" class="form-control" size="4" id="" data-stripe="cvc" placeholder="CVV" required>
									    </div>
									</div>
								</div>
								<div class="col-md-6">
								  	<div class="form-group">
									    <label for="inputEmail3" class="col-sm-3 control-label">Expiry(MM)</label>
									    <div class="col-sm-8">
									      <input type="input" class="form-control" size="2" id="" placeholder="MM" data-stripe="exp-month" required>
									    </div>
									  </div>
									  <div class="form-group">
									    <label for="inputPassword3" class="col-sm-3 control-label">Expiry(YYYY)</label>
									    <div class="col-sm-8">
									      <input type="input" class="form-control" id="" size="4" data-stripe="exp-year" placeholder="YYYY" required>
									    </div>
									  </div>
								</div>		
				  			
				  		</div>				
				  	</div>
				  	<br/><br/>
					<div class="row">
						<div> Powered By: <img src="<?php echo base_url()."assets/images/stripe-logo.png"; ?>" width="10%"> </div>
					</div>
				</div>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	        <button type="submit" class="btn btn-success" id="pay_now_btn"> Pay Now</button>				
	      </div>
	  	</form>
    </div><!-- /.modal-content -->	 
</div><!-- /.modal -->