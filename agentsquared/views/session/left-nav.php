<div class="collapse navbar-collapse navbar-ex1-collapse"> 
    <ul class="nav navbar-nav side-nav">
        <li>
            <a <?php if($this->uri->segment(2) == 'spark_member') echo "href=".site_url('dashboard'); ?> data-toggle="collapse" data-target="#agent-site" style="cursor:pointer;"><span class="icon"><i class="fa fa-home"></i></span> Agent Website <i class="fa fa-fw fa-caret-down"></i></a>
            

            <ul id="agent-site" class="<?php if($this->uri->segment(1) == 'dashboard' || $this->uri->segment(1) == 'idx_integration' || $this->uri->segment(1) == 'choose_theme' || $this->uri->segment(1) == 'custom_domain' || $this->uri->segment(1) == 'featured_listings' || $this->uri->segment(1) == 'home' || $this->uri->segment(2) == 'infos' || $this->uri->segment(2) == 'saved_searches' || $this->uri->segment(2) == 'saved_search' || $this->uri->segment(1) == 'pages' || $this->uri->segment(1) == 'menu' || $this->uri->segment(1) == 'dashboard_blogs') echo 'collapse in'; else echo 'collapse'?>">           

                <li class="<?php echo ($this->uri->segment(1) == "dashboard") ? "active-sub" : "" ?>">
                    <a href="<?php echo site_url("dashboard"); ?>"><span class="icon"><i class="fa fa-tachometer"></i></span>  Dashboard</a>
                </li>     
               <!--  <li class="<?php echo ($this->uri->segment(1) == "idx_integration") ? "active-sub" : "" ?>">
                    <a href="<?php echo site_url("idx_integration"); ?>"><span class="icon"><i class="fa fa-exchange"></i></span>  IDX Integration</a>
                </li> -->
                
                <li class="<?php echo ($this->uri->segment(1) == "featured_listings") ? "active-sub" : "" ?>">
                    <a href="<?php echo site_url("featured_listings"); ?>"><span class="icon"><i class="fa fa-list"></i></span> Active Listing</a>
                </li>
                
                <li class="<?php echo ($this->uri->segment(1) == "choose_theme") ? "active-sub" : "" ?>">
                    <a href="<?php echo site_url("choose_theme"); ?>"><span class="icon"><i class="fa fa-desktop"></i></span> Choose Theme</a>
                </li>
              <!-- <li class="<?php echo ($this->uri->segment(1) == "menu") ? "active-sub" : "" ?>">
                    <a href="<?php echo site_url("menu"); ?>"><span class="icon"><i class="fa fa-link"></i></span> Menu</a>
                </li>-->
                
                <li class="<?php echo ($this->uri->segment(2) == "infos") ? "active-sub" : "" ?>">
                    <a href="<?php echo site_url("agent_sites/infos"); ?>"><span class="icon"><i class="fa fa-info-circle"></i></span> Site Info</a>
                </li>

                <li class="<?php echo ($this->uri->segment(2) == "saved_searches" || $this->uri->segment(2) == "saved_search") ? "active-sub" : "" ?>">
                    <a href="<?php echo site_url("agent_sites/saved_searches"); ?>"><span class="icon"><i class="fa fa-info-circle"></i></span> Saved Searches</a>
                </li>
                
                <li class="<?php echo ($this->uri->segment(1) == "custom_domain") ? "active-sub" : "" ?>">
                    <a href="<?php echo site_url("custom_domain"); ?>"><span class="icon"><i class="fa fa-globe"></i></span> Custom Domain</a>
                </li>
                <!-- <li class="<?php echo ($this->uri->segment(1) == "media") ? "active-sub" : "" ?>">
                    <a href="<?php echo site_url("media"); ?>"><span class="icon"><i class="fa fa-file-image-o"></i></span> Media</a>
                </li>
                <li class="<?php echo ($this->uri->segment(1) == "pages") ? "active-sub" : "" ?>">
                    <a href="<?php echo site_url("pages"); ?>"><span class="icon"><i class="fa fa-book"></i></span> Pages </a>
                </li>
               <li class="<?php echo ($this->uri->segment(1) == "dashboard_blog") ? "active-sub" : "" ?>" >
                    <a href="<?php echo site_url("dashboard_blogs"); ?>"><span class="icon"><i class="fa fa-thumb-tack"></i></span> Blog</a>
                </li>-->
                <!-- <li>
                <?php 
                    if(!config_item("is_paid")) { ?>
                        <a href="<?php echo site_url("home"); ?>" target="blank"><span class="icon"><i class="fa fa-eye"></i></span> View Site</a>
                <?php } 
                    else { 
                        if(isset($domain_info[0]->status) && isset($domain_info[0]->is_subdomain)) {
                            if($domain_info[0]->status == "completed" || $domain_info[0]->is_subdomain == "1") { ?>
                                <a href="http://<?php echo isset($domain_info[0]->domains) ? $domain_info[0]->domains : site_url("home"); ?>" target="_blank"><span class="icon"><i class="fa fa-eye"></i></span> View Site</a>
                <?php       } 
                            else if($domain_info[0]->status != "completed" && $domain_info[0]->is_subdomain == "0") { ?>
                                <a href="<?php echo !empty($domain_info[0]->subdomain_url) ? $domain_info[0]->subdomain_url : site_url("home"); ?>" target="_blank"><span class="icon"><i class="fa fa-eye"></i></span> View Site</a>
                <?php       }
                            else { ?>
                                <a href="<?php echo site_url("home"); ?>" target="_blank"><span class="icon"><i class="fa fa-eye"></i></span> View Site</a>
                <?php       }
                        }
                        else { ?>
                            <a href="<?php echo site_url("home"); ?>" target="_blank"><span class="icon"><i class="fa fa-eye"></i></span> View Site</a>
                <?php
                        }   
                    }   ?>
                </li> -->
                <li>
                    <?php if(!config_item("is_paid")) { ?>
                                <a href="<?php echo site_url("home"); ?>" target="blank"><span class="icon"><i class="fa fa-eye"></i></span> View Site</a>
                    <?php } else { 
                                if(isset($domain_info[0])) {
                                    if($domain_info[0]->status == "completed") {
                                        if($domain_info[0]->is_subdomain == "1") { ?>
                                            <a href="<?php echo isset($domain_info[0]->subdomain_url) ? $domain_info[0]->subdomain_url : site_url("home"); ?>" target="_blank"><span class="icon"><i class="fa fa-eye"></i></span> View Site</a>
                    <?php               } else { ?>
                                            <a href="http://<?php echo isset($domain_info[0]->domains) ? $domain_info[0]->domains : site_url("home"); ?>" target="_blank"><span class="icon"><i class="fa fa-eye"></i></span> View Site</a>
                    <?php               }
                                    } else {
                                        if($domain_info[0]->is_subdomain == "1") { ?>
                                            <a href="<?php echo isset($domain_info[0]->subdomain_url) ? $domain_info[0]->subdomain_url : site_url("home"); ?>" target="_blank"><span class="icon"><i class="fa fa-eye"></i></span> View Site</a>
                    <?php               } else { ?>
                                            <a href="<?php echo site_url("home"); ?>" target="_blank"><span class="icon"><i class="fa fa-eye"></i></span> View Site</a>
                    <?php               }
                                    }
                                } else { ?>
                                    <a href="<?php echo site_url("home"); ?>" target="_blank"><span class="icon"><i class="fa fa-eye"></i></span> View Site</a>
                    <?php       }
                            } 
                    ?>
                </li>   
            </ul>
            
        </li>
        <li class="<?php echo ($this->uri->segment(2) == "single_listings" || $this->uri->segment(2) == "spark_member") ? "active" : "" ?>">
            <a href="<?php echo site_url("agent_sites/single_listings"); ?>"><span class="icon"><i class="fa fa-user-secret"></i></span> Single Property Listings</a>
        </li>
        
        <li>
            <a data-toggle="collapse" data-target="#agent-site-social" style="cursor:pointer;"><span class="icon"><i class="fa fa-share-alt"></i></span> Social Media <i class="fa fa-fw fa-caret-down"></i></a>
            <ul id="agent-site-social"class="<?php if($this->uri->segment(2) == 'activate' || $this->uri->segment(2) == 'links') echo 'collapse in'; else echo 'collapse'; ?>">
                <li class="<?php echo ($this->uri->segment(2) == "activate") ? "active-sub" : "" ?>">
                    <a href="<?php echo site_url("agent_social_media/activate"); ?>"><span class="icon"><i class="fa fa-cog"></i></span> Activate Social Media</a>
                </li>
                <li class="<?php echo ($this->uri->segment(2) == "links") ? "active-sub" : "" ?>">
                    <a href="<?php echo site_url("agent_social_media/links"); ?>"><span class="icon"><i class="fa fa-link"></i></span> Social Media Links</a>
                </li>
            </ul>
        </li>
        
        <li>
            <a data-toggle="collapse" data-target="#crm" style="cursor:pointer;"><span class="icon"><i class="fa fa-line-chart"></i></span> CRM <i class="fa fa-fw fa-caret-down"></i></a>
            <ul id="crm" class="<?php if($this->uri->segment(2) == 'reports' || $this->uri->segment(2) == 'buyer_leads' || $this->uri->segment(2) == 'seller_leads' || $this->uri->segment(2) == 'messages' || $this->uri->segment(2) == 'customer_leads' || $this->uri->segment(2) == 'schedules') echo 'collapse in'; else echo 'collapse'; ?>">
                <li class="<?php echo ($this->uri->segment(2) == "reports") ? "active-sub" : "" ?>">
                    <a href="<?php echo site_url("crm/reports"); ?>"><span class="icon"><i class="fa fa-file-text-o"></i></span> Reports</a>
                </li>
                <li class="<?php echo ($this->uri->segment(2) == "buyer_leads") ? "active-sub" : "" ?>">
                    <a href="<?php echo site_url("crm/buyer_leads"); ?>"><span class="icon"><i class="fa fa-bullseye"></i></span> Buyer Leads</a>
                </li>
                <li class="<?php echo ($this->uri->segment(2) == "seller_leads") ? "active-sub" : "" ?>">
                    <a href="<?php echo site_url("crm/seller_leads"); ?>"><span class="icon"><i class="fa fa-usd"></i></span> Seller Leads</a>
                </li>
                <li class="<?php echo ($this->uri->segment(2) == "messages") ? "active-sub" : "" ?>">
                    <a href="<?php echo site_url("crm/messages"); ?>"><span class="icon"><i class="fa fa-commenting-o"></i></span> Question Leads</a>
                </li>
                <li class="<?php echo ($this->uri->segment(2) == "customer_leads") ? "active-sub" : "" ?>">
                    <a href="<?php echo site_url("crm/customer_leads"); ?>"><span class="icon"><i class="fa fa-user"></i></span> Customer Leads</a>
                </li>
                 <li class="<?php echo ($this->uri->segment(2) == "schedules") ? "active-sub" : "" ?>">
                    <a href="<?php echo site_url("crm/schedules"); ?>"><span class="icon"><i class="fa fa-folder"></i></span> Scheduled showings</a>
                </li>
                <!--  <li class="<?php echo ($this->uri->segment(2) == "schedules") ? "active-sub" : "" ?>">
                     <a href="<?php echo site_url("crm/leads"); ?>"><span class="icon"><i class="fa fa-users"></i></span> Leads</a>
                </li> -->
            </ul>
        </li>
       
    </ul>
</div>
</nav>
<div id="page-wrapper">