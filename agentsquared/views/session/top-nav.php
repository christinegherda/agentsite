<body class="hold-transition skin-blue fixed layout-top-nav">
<div class="wrapper">
<header class="main-header">
    <nav class="navbar navbar-static-top">
    <?php 
        if($this->session->userdata('agent_idx_website')) { ?>
        <div class="navbar-header">
            <?php $agent_data = $this->session->userdata("agentdata"); ?>
            <a href="<?php echo site_url("dashboard"); ?>" class="navbar-brand">
            <?php if(isset($branding->logo) AND !empty($branding->logo)) { ?>
                    <img src="<?php echo AGENT_DASHBOARD_URL ?>assets/upload/logo/<?php echo $branding->logo;?>" alt="" class="dashboard-logo">
            <?php } else { ?>
            <img src="<?php echo AGENT_DASHBOARD_URL ?>assets/img/home/agentsquared-logo.png" alt="Agent Logo"/> 
            <?php } ?>
            </a>
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse"><i class="fa fa-bars"></i></button>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="<?php echo ($this->uri->segment(1) == "dashboard") ? "active" : "" ?>">
                    <a href="<?php echo site_url("dashboard"); ?>"><i class="fa fa-tachometer"></i> <span>Dashboard</span></a>
                </li> 
                <li class="dropdown <?php if($this->uri->segment(1) == 'customize_homepage' || $this->uri->segment(1) == 'featured_listings' || $this->uri->segment(1) == 'choose_theme' || $this->uri->segment(2) == 'infos' || $this->uri->segment(2) == 'saved_searches' || $this->uri->segment(1) == 'custom_domain' || $this->uri->segment(1) == 'pages' || $this->uri->segment(1) == 'menu' || $this->uri->segment(1) == 'dashboard_blogs' || $this->uri->segment(1) == 'home') echo 'active';?>">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user-secret"></i> <span>Agent Site</span> <i class="caret"></i></a>
                    <ul class="dropdown-menu" role="menu">
                        <li class="<?php echo ($this->uri->segment(1) == "featured_listings") ? "active" : "" ?>">
                            <a href="<?php echo site_url("featured_listings"); ?>"><span class="icon"><i class="fa fa-list"></i></span> Active Listing</a>
                        </li>
                        <li class="<?php echo ($this->uri->segment(1) == "customize_homepage") ? "active" : "" ?>">
                            <a href="<?php echo site_url("customize_homepage"); ?>"><span class="icon"><i class="fa fa-cog"></i></span> Customize Homepage</a>
                        </li>
                        <li class="<?php echo ($this->uri->segment(1) == "choose_theme") ? "active" : "" ?>">
                            <a href="<?php echo site_url("choose_theme"); ?>"><span class="icon"><i class="fa fa-desktop"></i></span> Choose Theme</a>
                        </li>
                        <li class="<?php echo ($this->uri->segment(2) == "infos") ? "active" : "" ?>">
                            <a href="<?php echo site_url("agent_sites/infos"); ?>"><span class="icon"><i class="fa fa-info-circle"></i></span> Site Info</a>
                        </li>
                        <li class="<?php echo ($this->uri->segment(2) == "saved_searches" || $this->uri->segment(2) == "saved_search") ? "active" : "" ?>">
                            <a href="<?php echo site_url("agent_sites/saved_searches"); ?>"><span class="icon"><i class="fa fa-save"></i></span> Saved Searches</a>
                        </li>
                        <li class="<?php echo ($this->uri->segment(1) == "custom_domain") ? "active" : "" ?>">
                            <a href="<?php echo site_url("custom_domain"); ?>"><span class="icon"><i class="fa fa-globe"></i></span> Custom Domain</a>
                        </li>
                        <!-- <li class="<?php echo ($this->uri->segment(1) == "media") ? "active" : "" ?>">
                            <a href="<?php echo site_url("media"); ?>"><span class="icon"><i class="fa fa-file-image-o"></i></span> Media</a>
                        </li>-->
                        <li class="<?php echo ($this->uri->segment(1) == "pages") ? "active" : "" ?>">
                            <a href="<?php echo site_url("pages"); ?>"><span class="icon"><i class="fa fa-book"></i></span> Page  </a>
                        </li>
                        <!--<li class="<?php echo ($this->uri->segment(1) == "dashboard_blog") ? "active" : "" ?>" >
                            <a href="<?php echo site_url("dashboard_blogs"); ?>"><span class="icon"><i class="fa fa-thumb-tack"></i></span> Blog</a>
                        </li>-->
                        <li class="<?php echo ($this->uri->segment(1) == "menu") ? "active" : "" ?>">
                            <a href="<?php echo site_url("menu"); ?>"><span class="icon"><i class="fa fa-link"></i></span> Menu</a>
                        </li>
                        <li class="<?php echo ($this->uri->segment(1) == "reviews") ? "active" : "" ?>">
                            <a href="<?php echo site_url("reviews"); ?>"><span class="icon"><i class="fa fa-star"></i></span> Reviews</a>
                        </li>
                        <li>
                        <?php 
                            if(!config_item("is_paid")) { ?>
                                <a href="<?php echo site_url("home"); ?>" target="blank"><span class="icon"><i class="fa fa-eye"></i></span> View Site</a>
                        <?php } 
                            else { 
                                if(isset($domain_info[0]->status) && isset($domain_info[0]->is_subdomain)) {
                                    if($domain_info[0]->status == "completed" || $domain_info[0]->is_subdomain == "1") { ?>
                                        <a href="http://<?php echo isset($is_reserved_domain['domain_name']) ? $is_reserved_domain['domain_name'] : site_url("home"); ?>" target="_blank"><span class="icon"><i class="fa fa-eye"></i></span> View Site</a>
                        <?php       } 
                                    elseif($domain_info[0]->status != "completed" && $domain_info[0]->is_subdomain == "0") { ?>
                                        <a href="<?php echo !empty($domain_info[0]->subdomain_url) ? $domain_info[0]->subdomain_url : site_url("home"); ?>" target="_blank"><span class="icon"><i class="fa fa-eye"></i></span> View Site</a>
                        <?php       }
                                    else { ?>
                                        <a href="<?php echo site_url("home"); ?>" target="_blank"><span class="icon"><i class="fa fa-eye"></i></span> View Site</a>
                        <?php       }
                                }
                                else { ?>
                                    <a href="<?php echo site_url("home"); ?>" target="_blank"><span class="icon"><i class="fa fa-eye"></i></span> View Site</a>
                        <?php
                                }   
                            }   ?>
                        </li>
                    </ul>
                </li>
                <li class="dropdown <?php if($this->uri->segment(2) == 'reports' || $this->uri->segment(2) == 'buyer_leads' || $this->uri->segment(2) == 'seller_leads' || $this->uri->segment(2) == 'messages' || $this->uri->segment(2) == 'customer_leads' || $this->uri->segment(2) == 'schedules') echo 'active';?> ">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-line-chart"></i> <span>CRM</span> <i class="caret"></i></a>
                    <ul class="dropdown-menu" role="menu" id="crm" class="<?php if($this->uri->segment(2) == 'activate' || $this->uri->segment(2) == 'links') echo 'collapse in'; else echo 'collapse'; ?>">
                        <li class="<?php echo ($this->uri->segment(2) == "leads") ? "active" : "" ?>">
                            <a href="<?php echo site_url("crm/leads"); ?>"><span class="icon"><i class="fa fa-file-text-o"></i></span> Leads</a>
                        </li>
                        <!-- <li class="<?php echo ($this->uri->segment(2) == "reports") ? "active" : "" ?>">
                            <a href="<?php echo site_url("crm/reports"); ?>"><span class="icon"><i class="fa fa-file-text-o"></i></span> Reports</a>
                        </li>
                        <li class="<?php echo ($this->uri->segment(2) == "buyer_leads") ? "active" : "" ?>">
                            <a href="<?php echo site_url("crm/buyer_leads"); ?>"><span class="icon"><i class="fa fa-bullseye"></i></span> Buyer Leads</a>
                        </li>
                        <li class="<?php echo ($this->uri->segment(2) == "seller_leads") ? "active" : "" ?>">
                            <a href="<?php echo site_url("crm/seller_leads"); ?>"><span class="icon"><i class="fa fa-usd"></i></span> Seller Leads</a>
                        </li> -->
                        <!-- <li class="<?php echo ($this->uri->segment(2) == "messages") ? "active" : "" ?>">
                            <a href="<?php echo site_url("crm/messages"); ?>"><span class="icon"><i class="fa fa-commenting-o"></i></span> Question Leads</a>
                        </li>
                        <li class="<?php echo ($this->uri->segment(2) == "customer_leads") ? "active" : "" ?>">
                            <a href="<?php echo site_url("crm/customer_leads"); ?>"><span class="icon"><i class="fa fa-user"></i></span> Customer Leads</a> -->
                        </li>
                         <li class="<?php echo ($this->uri->segment(2) == "schedules") ? "active" : "" ?>">
                            <a href="<?php echo site_url("crm/spw_schedules"); ?>"><span class="icon"><i class="fa fa-folder"></i></span> Scheduled showings</a>
                        </li>
                        <!--  <li class="<?php echo ($this->uri->segment(2) == "schedules") ? "active" : "" ?>">
                             <a href="<?php echo site_url("crm/leads"); ?>"><span class="icon"><i class="fa fa-users"></i></span> Leads</a>
                        </li> -->
                    </ul>
                </li>
                <li class="<?php echo ($this->uri->segment(1) == "single_property_listings" || $this->uri->segment(2) == "spark_member") ? "active" : "" ?>">
                    <a href="<?php echo site_url("single_property_listings"); ?>"> <i class="fa fa-building"></i>  <span>Single Property Listings</span></a>
                </li>
                <li class="dropdown <?php if($this->uri->segment(2) == 'activate' || $this->uri->segment(2) == 'links') echo 'active';?> ">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-share-alt"></i> <span>Marketing </span> <i class="caret"></i></a>
                    <ul class="dropdown-menu" role="menu" id="agent-site-social" class="<?php if($this->uri->segment(2) == 'activate' || $this->uri->segment(2) == 'links') echo 'collapse in'; else echo 'collapse'; ?>">
                        <li class="<?php echo ($this->uri->segment(2) == "activate") ? "active" : "" ?>">
                            <a href="<?php echo site_url("agent_social_media/activate"); ?>"><span class="icon"><i class="fa fa-cog"></i></span> Activate Social Media</a>
                        </li>
                        <li class="<?php echo ($this->uri->segment(2) == "links") ? "active" : "" ?>">
                            <a href="<?php echo site_url("agent_social_media/links"); ?>"><span class="icon"><i class="fa fa-link"></i></span> Social Media Links</a>
                        </li>
                    </ul>
                </li>
                <li><a href="http://www.agentsquared.com/support/" target="_blank"><i class="fa fa-gear"></i> <span>Support</span></a></li>
                <li class="viewsite">
                <?php 
                    if(!config_item("is_paid")) { ?>
                        <a class="btn-view btn-primary" href="<?php echo site_url("home"); ?>" target="blank"> View Site</a>
                <?php } 
                    else { 
                        if(isset($domain_info[0]->status) && isset($domain_info[0]->is_subdomain)) {
                            if($domain_info[0]->status == "completed" || $domain_info[0]->is_subdomain == "1") { ?>
                                <a class="btn-view btn-primary" href="http://<?php echo isset($is_reserved_domain['domain_name']) ? $is_reserved_domain['domain_name'] : site_url("home"); ?>" target="_blank"> View Site</a>
                <?php       } 
                            else if($domain_info[0]->status != "completed" && $domain_info[0]->is_subdomain == "0") { ?>
                                <a class="btn-view btn-primary" href="<?php echo !empty($domain_info[0]->subdomain_url) ? $domain_info[0]->subdomain_url : site_url("home"); ?>" target="_blank"> View Site</a>
                <?php       }
                            else { ?>
                                <a class="btn-view btn-primary" href="<?php echo site_url("home"); ?>" target="_blank"> View Site</a>
                <?php       }
                        }
                        else { ?>
                            <a class="btn-view btn-primary" href="<?php echo site_url("home"); ?>" target="_blank"> View Site</a>
                <?php
                        }   
                    }   ?>
                </li>
            </ul>
        </div>
        <?php
        } else { ?>
        <!-- SPW tabs -->
            <div class="navbar-header">
            <?php $agent_data = $this->session->userdata("agentdata"); ?>
                <a href="<?php echo site_url("agent_sites/single_listings"); ?>" class="navbar-brand">
            <?php if (isset($branding->logo) AND !empty($branding->logo)) {?>
                    <img src="<?php echo AGENT_DASHBOARD_URL ?>assets/upload/logo/<?php echo $branding->logo;?>" alt="" class="dashboard-logo">
            <?php } else { ?>
                <img src="<?php echo AGENT_DASHBOARD_URL ?>assets/img/home/agentsquared-logo.png" alt="Agent Logo"/> 
            <?php } ?>
                </a>
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse"><i class="fa fa-bars"></i></button>
            </div>
            <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
                <ul class="nav navbar-nav">
                    <!-- <li><a href="javascript:;" class="disabled-agent-btn"><i class="fa fa-tachometer"></i> <span>Dashboard</span></a></li> -->
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user-secret"></i> <span>Agent Site</span> <i class="caret"></i></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="javascript:;" class="disabled-agent-btn"><span class="icon"><i class="fa fa-list"></i></span> Active Listing</a></li>
                            <li><a href="javascript:;" class="disabled-agent-btn"><span class="icon"><i class="fa fa-cog"></i></span> Customize Homepage</a>
                            <li><a href="javascript:;" class="disabled-agent-btn"><span class="icon"><i class="fa fa-desktop"></i></span> Choose Theme</a></li>
                            <li><a href="javascript:;" class="disabled-agent-btn"><span class="icon"><i class="fa fa-info-circle"></i></span> Site Info</a></li>
                            <li><a href="javascript:;" class="disabled-agent-btn"><span class="icon"><i class="fa fa-save"></i></span> Saved Searches</a></li>
                            <li><a href="javascript:;" class="disabled-agent-btn"><span class="icon"><i class="fa fa-globe"></i></span> Custom Domain</a></li>
                            <li><a href="javascript:;" class="disabled-agent-btn"><span class="icon"><i class="fa fa-book"></i></span> Page</a></li>
                            <li><a href="javascript:;" class="disabled-agent-btn"><span class="icon"><i class="fa fa-link"></i></span> Menu</a></li>
                            <li><a href="javascript:;" class="disabled-agent-btn"><span class="icon"><i class="fa fa-star"></i></span> Reviews</a></li>
                            <li><a href="javascript:;" class="disabled-agent-btn"><span class="icon"><i class="fa fa-eye"></i></span> View Site</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-line-chart"></i> <span>CRM</span> <i class="caret"></i></a>
                        <ul class="dropdown-menu" role="menu" id="crm" class="<?php if($this->uri->segment(2) == 'activate' || $this->uri->segment(2) == 'links') echo 'collapse in'; else echo 'collapse'; ?>">
                            <li><a href="javascript:;" class="disabled-agent-btn"><span class="icon"><i class="fa fa-file-text-o"></i></span> Leads</a></li>
                            <li class="<?php echo ($this->uri->segment(2) == "schedules") ? "active" : "" ?>">
                                <a href="<?php echo site_url("crm/spw_schedules"); ?>"><span class="icon"><i class="fa fa-folder"></i></span> Scheduled showings</a>
                            </li>
                        </ul>
                    </li>
                    <li class="<?php echo ($this->uri->segment(1) == "single_property_listings" || $this->uri->segment(2) == "spark_member") ? "active" : "" ?>">
                      <a href="<?php echo site_url("single_property_listings"); ?>"><i class="fa fa-building"></i> <span>Single Property Listings</span></a>
                    </li>
                    <li class="dropdown <?php if($this->uri->segment(2) == 'activate' || $this->uri->segment(2) == 'links') echo 'active';?> ">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-share-alt"></i> <span>Marketing </span> <i class="caret"></i></a>
                        <ul class="dropdown-menu" role="menu" id="agent-site-social" class="<?php if($this->uri->segment(2) == 'activate' || $this->uri->segment(2) == 'links') echo 'collapse in'; else echo 'collapse'; ?>">
                            <li class="<?php echo ($this->uri->segment(2) == "activate") ? "active" : "" ?>">
                                <a href="<?php echo site_url("agent_social_media/activate"); ?>"><span class="icon"><i class="fa fa-cog"></i></span> Activate Social Media</a>
                            </li>
                            <li class="<?php echo ($this->uri->segment(2) == "links") ? "active" : "" ?>">
                                <a href="<?php echo site_url("agent_social_media/links"); ?>"><span class="icon"><i class="fa fa-link"></i></span> Social Media Links</a>
                            </li>
                        </ul>
                    </li>
                    <li><a href="http://www.agentsquared.com/support/" target="_blank"><i class="fa fa-gear"></i> <span>Support</span></a></li>
                    <li class="viewsite"><a class="btn-view btn-primary disabled-agent-btn" href="javascript:;"> View Site</a></li>
                </ul>
            </div>
        <?php
            }
        ?>
        <!-- End of SPW tabs -->

        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
            <?php if (!$this->session->userdata('agent_idx_website')): ?>
                <!-- <li>
                    <p class="update-site">
                        <span style="color:#fff;"> Want to have an IDX site of your own?  </span><span> <a href="http://sparkplatform.com/appstore/apps/instant-agent-idx-website" target="_blank" class="btn btn-default btn-submit "><i class="fa fa-upload"></i> Click here </a> </span>
                    </p>
                </li> -->
            <?php endif ?>

                <!-- User Account Menu -->
                <li class="dropdown user user-menu">
                    <!-- Menu Toggle Button -->
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <!-- The user image in the navbar-->
                    <?php 
                        if(isset($branding->agent_photo) AND !empty($branding->agent_photo)) { ?>
                            <img src="<?php echo AGENT_DASHBOARD_URL ?>assets/upload/photo/<?php echo $branding->agent_photo;?>" alt="Agent Photo" class="user-image">
                    <?php } else { ?>
                        <?php if(isset($agent_data) AND !empty($agent_data)) { ?>
                                <img  src="<?php  echo $agent_data["agent_photo"] ;?>" class="user-image" alt="Agent Photo" class="user-image"> 
                        <?php } else { ?>
                                <img src="<?= base_url()?>/assets/images/no-profile-img.gif" class="user-image" alt="Agent Photo" class="user-image">  
                        <?php } ?>  
                    <?php } ?> 
                    <!-- hidden-xs hides the username on small devices so only the image appears. -->
                        <span class="hidden-xs"><?php echo ($agent_data) ? $agent_data['agent_name']: "Admin";?></span> &nbsp;<i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- The user image in the menu -->
                        <li class="user-header">
                            <div class="agent-img">
                                <?php if( isset($branding->agent_photo) AND !empty($branding->agent_photo)) { ?>
                                    <img src="<?php echo AGENT_DASHBOARD_URL ?>assets/upload/photo/<?php echo $branding->agent_photo;?>" alt="Agent Photo" class="img-circle">
                                <?php } else { ?>
                                    <?php if(isset($agent_data) AND !empty($agent_data)) { ?>
                                        <img  src="<?php  echo $agent_data["agent_photo"] ;?>" class="img-circle" alt="Agent Photo" > 
                                    <?php } else { ?>
                                        <img src="<?= base_url()?>/assets/images/no-profile-img.gif" class="img-circle" alt="Agent Photo">  
                                    <?php }
                                } ?> 
                                <p style="margin-top:5px; color:#fff;"> <?php echo ($agent_data) ? $agent_data['agent_name']: "Admin";?></p>
                                <div class="change-profile-container">
                                    <button class="btn btn-default btn-upload">
                                        <i class="fa fa-camera-retro" aria-hidden="true"></i> Update Photo
                                    </button>
                                </div>
                                <div class="overlay-photo"></div>
                            </div>
                            <form action="<?php echo base_url()?>agent_sites/upload_agent_photo" class="form-horizontal content-form agent-upload-form" method="POST" enctype="multipart/form-data" id="updateAgentPhoto">
                                <div class="form-group">
                                    <input type="file" name="userfile" class="form-control image-upload">
                                    <button style="display:none" type="submit" class="btn btn-default btn-submit btn-agent-photo show-btn">Upload</button>
                                </div>
                            </form>
                        </li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="<?php  echo site_url('login/auth/change_password'); ?>" class="btn btn-default btn-transparent btn-flat">Change Password</a>
                            </div>
                            <div class="pull-right">
                                <a href="<?php  echo site_url('login/auth/logout'); ?>" class="btn btn-default btn-transparent btn-flat">Sign out</a>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
        <!-- /.navbar-custom-menu -->
    </nav>
  </header>
