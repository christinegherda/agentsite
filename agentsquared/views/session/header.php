<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">

  <title><?php echo (isset($title)) ? $title : 'AgentSquared'; ?></title>
  <link rel="icon" type="image/x-icon"  href="<?php if (isset($branding->favicon) AND !empty($branding->favicon)) {?><?php echo "/assets/upload/favicon/$branding->favicon"?><?php } else { ?><?php echo "/assets/images/default-favicon.png"?><?php } ?>"/>

  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="apple-touch-icon" href="/assets/img/home/apple-touch-icon.png">
  <link href='//fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
  <link href='//fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" type="text/css">
  <link rel="stylesheet" href="/assets/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="/assets/fonts/fontello/css/fontello.css" type="text/css">
  <link rel="stylesheet" href="/assets/css/dashboard/ionicons.min.css">
  <link rel="stylesheet" href="/assets/css/dashboard/AdminLTE.min.css">
  <link rel="stylesheet" href="/assets/css/dashboard/skin-blue.min.css">
  <link rel="stylesheet" href="/assets/css/dashboard/common.css">
  <link rel="stylesheet" href="/assets/css/dashboard/dashboard.css">
  <link rel="stylesheet" href="/assets/css/dashboard/fileUploader.css">
  <link rel="stylesheet" href="/assets/css/dashboard/summernote.css" type="text/css">
  <link rel="stylesheet" href="/assets/css/dashboard/jquery.domenu-0.48.53.css" type="text/css">
  <link rel="stylesheet" href="/assets/css/dashboard/plugins/msgBoxLight.css" type="text/css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css" type="text/css">
  <link rel="stylesheet" href="/assets/css/dashboard/owl.carousel.css" type="text/css">
  <link rel="stylesheet" href="/assets/css/dashboard/bootstrap-datepicker.css" type="text/css">
  <link rel="stylesheet" href="https://rawgit.com/enyo/dropzone/master/dist/dropzone.css" type="text/css"> 
  <link rel="stylesheet" href="/assets/css/dashboard/plugins/jquery.rateyo.min.css" type="text/css"/>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.2/css/bootstrap3/bootstrap-switch.min.css" type="text/css"/>
  <link rel="stylesheet" href="/assets/css/dashboard/menu.css" type="text/css"/>
  <?php if(isset($css)) :
      foreach($css as $key => $val ) :  ?>
      <link rel="stylesheet" href="/assets/css/<?php echo $val?>" type="text/css"> </link>
      <?php endforeach;?>
  <?php endif;?>
  <script type="text/javascript">var base_url = "<?php echo base_url(); ?>"; </script>

  <script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '662338450585957');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=662338450585957&ev=PageView&noscript=1"
/></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->

</head>
