<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| Hooks
| -------------------------------------------------------------------------
| This file lets you define "hooks" to extend CI without hacking the core
| files.  Please see the user guide for info:
|
|   https://codeigniter.com/user_guide/general/hooks.html
|
*/

/*
 * This function replaces the need for modules/dsl/config/dsl.php.
 * What it does is get the domain/DNS and see if it's in domains table,
 * if it does it will save the user_id and agent_id into the session.
 * Otherwise, it will redirect to the root domain of Agent Squared.
 */
$hook['pre_controller'] = function() {
    $ci =& get_instance(); 

    $user = domain_to_user();

    if( $user ) {
        $ci->session->set_userdata( array(
            'user_id' => (int)$user->id,
            'agent_id' => $user->agent_id,
            'auth_type' => $user->auth_type,
            'logged_in_auth' => true
        ) );

        set_agent_cookies();
    } else {
        redirect($ci->config->item('root_domain'));
    }
};
