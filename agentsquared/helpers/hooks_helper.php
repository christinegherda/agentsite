<?php

/* 
 * Author: Jovanni G <Jovanni G at AgentSquared.com>
 * License: GNU 3.0
 * Project: AgentSquared Theming
 */

defined('BASEPATH') OR exit('No direct script access allowed');

define('COMMON_APPPATH_DIR', APPPATH . 'modules/widgets/');
define('COMMON_WIDGETS_VIEW',  'widgets/');

define('THEME_IMG', '/src/images/');
define('THEME_CSS', '/src/css/');
define('THEME_JS', '/src/js/');
define('THEME_FONTS', '/src/fonts/');

if( !function_exists('get_predefined') ){
  function get_predefined($name = NULL){
    if($name !== NULL){
      switch($name){
        case 'bootstrap':
          return array(
            'css' => base_url() . 'src/css/bootstrap.min.css',
            'js' => base_url() . 'src/js/bootstrap.min.js'
          );
        case 'jquery':
          return array(
            'js' => base_url() . 'src/js/jquery.min.js'
          );
        case 'fontawesome':
          return array(
            'css' => base_url() . 'src/css/font-awesome.min.css'
          );
      }
    }
    return false;
  }
}

global $hooks;
$hooks = array();
if( !function_exists('hook')){
  /**
   * Hook into a function and execute a given function of your own.
   * 
   * @param string $name Name of the function to hook with.
   * @param function $function Function to execute on a given hook.
   */
  function hook($name, $function){
    global $hooks;
    $hooks[$name] = $function;
  }
}
if( !function_exists('add_hook')){
  /**
   * Adds a hook where you can execute functions outside or inside a theme folder
   * 
   * @param string $name Name of the function hook to execute.
   * @param mixed $data (Optional) can be anything you want to pass to this function hook.
   */
  function add_hook($name, $data = NULL){
    global $hooks;
    if(array_key_exists($name, $hooks)){
      if($data !== NULL){
        $hooks[$name]($data);
      }else{
        $hooks[$name]();
      }
    }
  }
}

global $filters;
$filters = array();
if( !function_exists('filter')){
  /**
   * Hook into a function and execute a given function to filter a data. Works the same way as hook() but you must return the data passed on to the function.
   * 
   * @param string $name (Required) Name of the function to hook with.
   * @param function $function (Required) Function to execute on a given hook.
   * 
   * @sample
   * filter('function name', function($data){
   *    // do your data filtering here.
   *    return $data;
   * });
   */
  function filter($name, $function){
    global $hooks;
    $hooks[$name] = $function;
  }
}
if( !function_exists('add_filter')){
  /**
   * Adds a hook where you can execute a function and filter its $data outside or inside a theme folder. Works the same way as add_hook(), only it returns the $data that was filtered out.
   * 
   * @param string $name (Required) Name of the function filter to execute.
   * @param mixed $data (Required) Arguments can be anything you want to pass to this filter function.
   * @return mixed $data Returns the filtered $data values.
   * 
   * @sample
   * $data = add_filter('function name', $data);
   */
  function add_filter($name, $data){
    global $hooks;
    if(array_key_exists($name, $hooks)){
      if($data !== NULL){
        return $hooks[$name]($data);
      }else{
        return $hooks[$name]();
      }
    }else{
      return $data;
    }
  }
}

global $styles;
$styles = array();
if( !function_exists('enqueue_style') ){
  /**
   * 
   * @global type $styles
   * @param mixed $name If string is passed, will look for predefined existing resources. If array is passed, will enqueue via key value pair if its an associative array. If its a regular array, it will just enqueue it directly.
   * @param mixed $src If string is passed, will enqueue the string with respect to the name. If number is passed, it will reorder the enqueues accordingly and will insert this 
   * @param array $args Extra attributes for this meta tag.
   */
  function enqueue_style($name = '', $src = NULL, $args = array()) {
    global $styles;
    if(is_array($name)){
      $keys = array_keys($name);
      foreach($keys as $key){
        if(is_array($name[$key])){
          $nem = $key;
          if(is_numeric($key)){
            $nem = 'style-'.$key;
          }else if(array_key_exists('name', $name[$key])){
            $nem = $name[$key];
          }
          
          $src = '';
          if(array_key_exists('src', $name[$key])){
            $src = $name[$key]['src'];
          }else{
            $src = $name[$key][0];
          }
          
          $args_str = '';
          if(array_key_exists('args', $name[$key])){
            if(is_array($name[$key]['args'])){
              $kys = array_keys($name[$key]['args']);
              foreach($kys as $ky){
                $arg = $ky;
                if(is_numeric($ky)){
                  $arg = 'args-'.$ky;
                }
                $args_str .= ' '.$arg.'="'.$name[$key]['args'][$ky].'"';
              }
            }else{
              $args_str = ' '.$name[$key]['args'];
            }
          }
          
          $link = '<link id="as-'.$nem.'" rel="stylesheet" href="';
          $link .= $src;
          $link .= '?version='.AS_VERSION.'" type="text/css"'.$args_str.' />';
          
          if(array_key_exists('order', $name[$key])){
            $inserted = array(
              'name' => $nem,
              'link' => $link
            );
            $order = (int) $name[$key];
            if(!isset($styles[$order]) || !array_key_exists($order, $styles)){
              $styles[$order] = $inserted;
            }else{
              array_splice($styles, $order, 0, array($inserted));
            }
          }else{
            array_push($styles, array(
              'name' => $nem,
              'link' => $link
            ));
          }
        }else{
          $nem = $key;
          if(is_numeric($key)){
            $nem = 'style-'.$key;
          }
          $files = get_predefined($name[$key]);
          if($files){
            if(array_key_exists('css', $files)){
              $nem = $name[$key];
              $name[$key] = $files['css'];
            }
          }
          
          $link = '<link id="as-'.$nem.'" rel="stylesheet" href="';
          $link .= $name[$key];
          $link .= '?version='.AS_VERSION.'" type="text/css" />';
          array_push($styles, array(
            'name' => $nem,
            'link' => $link
          ));
        }
      }
    }else{
      $order = -1;
      $files = get_predefined($name);
      if($src === NULL && $files){
        if(array_key_exists('css', $files)){
          $src = $files['css'];
        }
      }else if(is_numeric($src) && $files){
        $order = (int) $src;
        if(array_key_exists('css', $files)){
          $src = $files['css'];
        }
      }
      $link = '<link id="as-'.$name.'" rel="stylesheet" href="';
      $link .= $src;
      $args_str = '';
      $keys = array_keys($args);
      foreach($keys as $key){
        $args_str .= ' '.$key.'="'.$args[$key].'"';
      }
      $link .= '?version='.AS_VERSION.'" type="text/css"'.$args_str.'/>';
      if($order > -1){
        $inserted = array(
          'name' => $name,
          'link' => $link
        );
        if(!isset($styles[$order]) || !array_key_exists($order, $styles)){
          $styles[$order] = $inserted;
        }else{
          array_splice($styles, $order, 0, array($inserted));
        }
      }else{
        $flag = false;
        foreach($styles as $item){
          if($item['name'] == $name){
            $flag = true;
          }
        }
        if(!$flag){
          array_push($styles, array(
            'name' => $name,
            'link' => $link
          ));
        }
      }
    }
  }
}
if( !function_exists('dequeue_style') ){
  /**
   * 
   * @global type $styles
   * @param string $name Name of the style to dequeue.
   */
  function dequeue_style($name = ''){
    global $styles;
    $i = 0;
    foreach($styles as $style){
      if($style['name'] === $name){
        array_splice($styles, $i, 1);
        break;
      }
      $i++;
    }
  }
}
if( !function_exists('enqueue_styles') ){
  /**
   * 
   * @global type $styles
   * @param array $exclude (Optional) You can exclude styles by adding this parameter.
   */
  function enqueue_styles($exclude = array()) {
    global $styles;
    if(!empty($styles)){
      echo "<!-- Stylesheets -->\r\n\t";
      foreach($styles as $style){
        if(!in_array($style['name'], $exclude)){
          echo $style['link']."\n  ";
        }
      }
      echo "\r\n<!-- end of Stylesheets -->\r\n";
    }
  }
}

global $scripts;
$scripts = array();
if( !function_exists('enqueue_script') ){
  /**
   * 
   * @global type $scripts
   * @param mixed $name If string is passed, will look for predefined existing resources. If array is passed, will enqueue via key value pair if its an associative array. If its a regular array, it will just enqueue it directly.
   * @param mixed $src If string is passed, will enqueue the string with respect to the name. If number is passed, it will reorder the enqueues accordingly and will insert this 
   * @param array $args Extra attributes for this meta tag.
   */
  function enqueue_script($name = '', $src = NULL, $args = array()) {
    global $scripts;
    if(is_array($name)){
      $keys = array_keys($name);
      foreach($keys as $key){
        if(is_array($name[$key])){
          $nem = $key;
          if(is_numeric($key)){
            $nem = 'style-'.$key;
          }else if(array_key_exists('name', $name[$key])){
            $nem = $name[$key];
          }
          
          $src = '';
          if(array_key_exists('src', $name[$key])){
            $src = $name[$key]['src'];
          }else{
            $src = $name[$key][0];
          }
          
          $args_str = '';
          if(array_key_exists('args', $name[$key])){
            if(is_array($name[$key]['args'])){
              $kys = array_keys($name[$key]['args']);
              foreach($kys as $ky){
                $arg = $ky;
                if(is_numeric($ky)){
                  $arg = 'args-'.$ky;
                }
                $args_str .= ' '.$arg.'="'.$name[$key]['args'][$ky].'"';
              }
            }else{
              $args_str = ' '.$name[$key]['args'];
            }
          }
          $script = '<script id="as-script-'.$nem.'" src="';
          $script .= $src;
          $script .= '?version='.AS_VERSION.'"'.$args_str.'></script>';
          
          if(array_key_exists('order', $name[$key])){
            $inserted = array(
              'name' => $nem,
              'script' => $script
            );
            $order = (int) $name[$key];
            if(!isset($scripts[$order]) || !array_key_exists($order, $scripts)){
              $scripts[$order] = $inserted;
            }else{
              array_splice($scripts, $order, 0, array($inserted));
            }
          }else{
            array_push($scripts, array(
              'name' => $nem,
              'script' => $script
            ));
          }
        }else{
          $nem = $key;
          if(is_numeric($key)){
            $nem = 'script-'.$key;
          }
          $files = get_predefined($name[$key]);
          if($files){
            if(array_key_exists('js', $files)){
              $nem = $name[$key];
              $name[$key] = $files['js'];
            }
          }
          $script = '<script id="as-script-'.$nem.'" src="';
          $script .= $name[$key];
          $script .= '?version='.AS_VERSION.'"></script>';
          array_push($scripts, array(
            'name' => $nem,
            'script' => $script
          ));
        }
      }
    }else{
      $order = -1;
      $files = get_predefined($name);
      if($src === NULL && $files){
        if(array_key_exists('js', $files)){
          $src = $files['js'];
        }
      }else if(is_numeric($src) && $files){
        $order = (int) $src;
        if(array_key_exists('js', $files)){
          $src = $files['js'];
        }
      }
      $script = '<script id="as-script-'.$name.'" src="';
      $script .= $src;
      $args_str = '';
      if($args && !empty($args)){
        $keys = array_keys($args);
        foreach($keys as $key){
          $args_str .= ' '.$key.'="'.$args[$key].'"';
        }
      }
      $script .= '?version='.AS_VERSION.'"'.$args_str.' defer></script>';
      if($order > -1){
        $inserted = array(
          'name' => $name,
          'script' => $script
        );
        if(!isset($scripts[$order]) || !array_key_exists($order, $scripts)){
          $scripts[$order] = $inserted;
        }else{
          array_splice($scripts, $order, 0, array($inserted));
        }
      }else{
        $flag = false;
        foreach($scripts as $item){
          if($item['name'] == $name){
            $flag = true;
          }
        }
        if(!$flag){
          array_push($scripts, array(
            'name' => $name,
            'script' => $script
          ));
        }
      }
    }
  }
}
if( !function_exists('dequeue_script') ){
  function dequeue_script($name = ''){
    global $scripts;
    $i = 0;
    foreach($scripts as $script){
      if($script['name'] === $name){
        array_splice($scripts, $i, 1);
        break;
      }
      $i++;
    }
  }
}
if( !function_exists('enqueue_scripts') ){
  function enqueue_scripts($exclude = array()) {
    global $scripts;
    if(isset($scripts)){
      if(empty($scripts)){ return false; }
      echo "<!-- Scripts -->\r\n\t";
      foreach($scripts as $script){
        if(!in_array($script['name'], $exclude)){
          echo $script['script']."\n    ";
        }
      }
      echo "\r\n<!-- end of Scripts-->\r\n";
    }
  }
}

global $jsvars;
$jsvars = array();
global $_jsvars;
$_jsvars = array();
global $jsvar_types;
$jsvar_types = array();
if( !function_exists('add_jsvar') ){
  /**
   * @param string $name 'Variable name.'
   * @param string $value 'Value of the variable.'
   **/
  function add_jsvar($name, $value, $echo = 'json', $single = false) {
    global $jsvars;
    global $_jsvars;
    global $jsvar_types;
    if(isset($name) && isset($value)){
      if($echo == 'json' && !$single){
        $jsvars[$name] = $value;
      }else{
        $_jsvars[$name] = $value;
        $jsvar_types[$name] = $echo;
      }
    }
  }
}
if( !function_exists('get_jsvars') ){
  /**
   * @param boolean $clean 'Display variables with tabs and indents.'
   **/
  function get_jsvars() {
    global $jsvars;
    global $_jsvars;
    global $jsvar_types;
    if(!empty($jsvars) || !empty($_jsvars)){
      echo "<script type=\"text/javascript\">\r\n\t";
      echo "var globals = ";
      echo json_encode($jsvars);
      echo ";";
      foreach($_jsvars as $key => $value){
        if($jsvar_types[$key] == 'json'){
          echo "var " . $key . " = " . json_encode($value) . ";";
        }else if($jsvar_types[$key] == 'string'){
          echo "var " . $key . " = \"" . $value . "\";";
        }else{
          $value = (string) $value;
          echo "var " . $key . " = " . $value . ";";
        }
      }
      echo "\r\n</script>";
    }
  }
}

global $metas;
$metas = array();
if( !function_exists('set_metas') ){
  /**
   * 
   * @global type $metas
   * @param array $args Key value pair for meta.
   */
  function set_metas($args = array()){
    global $metas;
    if(!empty($args)){
      foreach($args as $key => $value){
        $metas[$key] = $value;
      }
    }
  }
}
if( !function_exists('get_metas') ){
  /**
   * @global array $metas
   * @param boolean $echo If given, this will display html meta elements instead of returning an array.
   * @param array $exclude If you want to exclude some metas from returning or displaying, add those meta keys here.
   * @return array
   */
  function get_metas($echo = false, $exclude = array()){
    global $metas;
    if($echo){
      $keys = array_keys($metas);
      foreach($keys as $key){
        if(!in_array($key, $exclude)){
          if($key == 'title'){
            echo "<title>{$metas['title']}</title>\r\n";
          }else{
            echo "<meta name=\"{$key}\" content=\"{$metas[$key]}\">";
          }
        }
      }
    }else{
      return $metas;
    }
  }
}
if( !function_exists('get_meta') ){
  /**
   * 
   * @global array $metas
   * @param string $key the meta key.
   * @return boolean|string Value if key exists, false if not.
   */
  function get_meta($key){
    global $metas;
    if(isset($key) && array_key_exists($key, $metas)){
      return $metas[$key];
    }
    return false;
  }
}

global $view;
$view = '';
if( !function_exists('set_view') ){
  function set_view($current = NULL){
    global $view;
    if($current !== NULL){
      $view = strtolower($current);
    }
  }
}
if( !function_exists('is_view') ){
  function is_view($target = NULL){
    global $view;
    $target = trim(strtolower($target));
    return $target == $view;
  }
}
if( !function_exists('get_view') ){
  function get_view(){
    global $view;
    return $view;
  }
}

if( !function_exists('get_elements') ){
  function get_elements($html = NULL, $search = '*', $include = array()){
    $search = '//' . $search;
    if($html !== NULL && $html !== ''){
      $dom = new DOMDocument();
      $dom->loadHTML($html);

      $xpath = new DOMXPath($dom);
      $result = $xpath->query($search);

      $elements = array();
      
      foreach($result as $elem){

        $element = array(
          'object' => $elem,
          'textContent' => $elem->textContent,
        );

        if(in_array('attributes', $include)){
          foreach($elem->attributes as $attr){
            $element['attributes'][$attr->name] = $attr->value;
          }
        }

        array_push($elements, $element);
      }
      return $elements;
    }
    return array();
  }
}

global $widgets;
$widgets = array(
  'horizontal-carousel' => array(
    'dependencies' => array(
      'assets' //this is where I left off.
    )
  ),
);
if( !function_exists('prepare_widget') ){
  function prepare_widget($name = NULL, $data = NULL){
    
  }
}
if( !function_exists('render_widget') ){
  function render_widget(){

  }
}


global $builder;
if( !function_exists('prepare_builder') ){
  function prepare_builder($data = NULL){
    global $builder;
    if($data !== NULL){
      $builder = $data;
    }
  }
}
if( !function_exists('load_builder') ){
  function load_builder(){
    global $builder;
    if(isset($builder)){
      // printa($builder);

      ci()->load->view(COMMON_WIDGETS_VIEW . 'horizontal-carousel');
    }
  }
}

if( !function_exists('assoc_to_string')){
  function assoc_to_string($assoc, $separator = "\n\r"){

    $arr = array();

    foreach($assoc as $key => $value){
      array_push($arr, $key.' : '.$value);
    }

    return implode($separator, $arr);
  }
}

