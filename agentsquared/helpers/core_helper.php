<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
 * Returns the CodeIgniter object.
 *
 * Example: ci()->db->get('table');
 *
 * @return \CI_Controller
 */
function ci()
{
	return get_instance();
}

if( !function_exists('printA') )
{
	function printA($arr, $exit = false, $vardump = false) {
		echo '<pre>';
		if($vardump){
      var_dump($arr);
    }else{
      print_r($arr);
    }
		echo '</pre>';
    if($exit){
      exit();
    }
	}
}

if( !function_exists('is_freemium')){

  function is_freemium() {

    $ci = ci();
    $domain_user = domain_to_user();

    $ci->db->select("subscriber_user_id,plan_id")
          ->from("freemium")
          ->where("subscriber_user_id", $domain_user->id)
          ->limit(1);

    $result = $ci->db->get()->row();

    return (!empty($result)) ? $result : FALSE;

  }
}


if( !function_exists('get_idx_records') )
{
		
	function get_idx_records( $type = NULL) {

		if( isset($_SESSION["user_id"]) )
		{

      $domain_user = domain_to_user();
			$idx = ci()->db->select("api_key, api_secret")
							->from("property_idx")
							->where("user_id", $domain_user->id)
							->limit(1)
							->get()->row();



			if( !empty($idx) )
			{
				ci()->config->set_item('idx_api_key', $idx->api_key );
				ci()->config->set_item('idx_api_secret', $idx->api_secret );	

				return TRUE;		
			}
			else
			{
				return FALSE;
			}

		}
		return FALSE;
	}
}

if( !function_exists('generate_code') )
{
	function generate_key( $characters = "" ) {

		/* numbers, letters */
		//$possible = '1234567890987654321aAbBcCdDeEfFgGhHiIjJkKlLmMnNoOpPqQrRsStTuUvVwWxXyYzZ';
		$possible = '1234567890987654321abcdefghijklmnopqrstuvwxyzyxwvutsrqponmlkjihgfedcba';
		$code = '';
		$i = 0;
		while ($i < $characters) { 
			$code .= substr($possible, mt_rand(0, strlen($possible)-1), 1);
			$i++;
		}
		
		return date("Y").$code;
	}
}

if( !function_exists('isCustomerIdxActive') )
{
	function isCustomerIdxActive( $code = NULL ) {

		if( empty($code) ) 
		{
			return FALSE;
		}

		$code = ci()->db->select('code,id')
						->from('users')
						->where("code", $code)
						->limit(1)
						->get()->row();

		if( !empty($code) )
		{
			$idx = ci()->db->select("api_key, api_secret")
						->from("property_idx")
						->where("user_id", $code->id )
						->limit(1)
						->get()->row();

			if( !empty($idx) )
			{
				ci()->config->set_item('idx_api_key', $idx->api_key );
				ci()->config->set_item('idx_api_secret', $idx->api_secret );	

				return TRUE;		
			}
			else
			{
				return FALSE;
			}

		}
		
		return FALSE;

	}

}

if( !function_exists('str_replace_limit') )
{
	function str_replace_limit($search, $replace, $string, $limit = 1) {
        $pos = strpos($string, $search);

        if ($pos === false) {
          return $string;
        }

        $searchLen = strlen($search);

        for ($i = 0; $i < $limit; $i++) {
             $string = substr_replace($string, $replace, $pos, $searchLen);

            $pos = strpos($string, $search);

             if ($pos === false) {
             	break;
             }
        }

        return $string;
    }
}

if( !function_exists('str_limit') )
{
	function str_limit($str, $len = 100)
	{
	    if(strlen($str) < $len)
	    {
	        return $str;
	    }

	    $str = preg_replace("/\s+/", ' ', str_replace(array("\r\n", "\r", "\n"), ' ', $str));

	    if(strlen($str) <= $len)
	    {
	        return $str;
	    }

	    $out = '';
	    foreach(explode(' ', trim($str)) as $val)
	    {
	        $out .= $val . ' ';

	        if(strlen($out) >= $len)
	        {
	            $out = trim($out);
	            return (strlen($out) == strlen($str)) ? $out : $out;
	        }
	    }
	}
}


if( !function_exists('save_activities') )
{
		
	function save_activities( $table_id = NUll , $activity_id = "", $activity = "" , $activity_type= "send_email", $user_type = "agent", $contact_id = "" ) {

		$save_activity["table_id"] = $table_id;
		$save_activity["activity_id"] = $activity_id;
		$save_activity["activity"] = $activity ;
		$save_activity["activity_type"] = $activity_type;
		$save_activity["user_type"] = $user_type;
		$save_activity["created"] = date("Y-m-d H:i:s");
		$save_activity["contact_id"] = $contact_id;
    $domain_user = domain_to_user();
		$save_activity["agent_id"] = $domain_user->agent_id;

		ci()->db->insert("core_activities", $save_activity);

		return ( ci()->db->insert_id() ) ? ci()->db->insert_id() : FALSE;
	}
}

if( !function_exists('clean_url') ){

	function clean_url($string) {
	   $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.

	   return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
	}
}

if( !function_exists('get_ga')){
  
  function get_ga(){
    echo Modules::run('as_ga/getTrackingCode');
  }
}

if( !function_exists('get_ga_raw')){
  
  function get_ga_raw() {
    $file = FCPATH .'ga.txt';
    if(file_exists($file)){
      echo file_get_contents($file);
    }
  }
}

if( !function_exists('enqueue_listtrac') ){
  /**
  * @params $action = listtrac property key
  * @params $slug = slug to filter or slug where the code will be displayed
  * @params $args = listtrac property params
  */
  function enqueue_listtrac($action = '', $slug = '', $args = array()){
    $actions = Modules::run('listtrac/actions');
    if(in_array($action, $actions)){
      if($slug == 'property-details' || $slug == 'other-property-details'){
        echo Modules::run('listtrac/'.$action, $args);
      }
    }
  }
}

if( !function_exists('get_gtm')){
  function get_gtm($noscript = false){
    $gtm_id = Modules::run('gtm/getGTMid');
    if($gtm_id){
      if($noscript){
        echo '<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id='.$gtm_id.'"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->'."\n";
      }else{
        echo "<!-- Start Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','".$gtm_id."');</script>
    <!-- End Google Tag Manager -->\n";
      }
    }
  }
}

if( !function_exists('get_fb_pixel') ){
  function get_fb_pixel(){
    $actions = Modules::run('pixels/facebook');
    if($actions != ''){
      print($actions . "\r\n");
    }
  }
}

function is_secure() {
  return
    (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off')
    || $_SERVER['SERVER_PORT'] == 443 
    || ( isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https' );
}

function strip_domain_scheme($url) {
    return str_replace(
        array(
            'www.',
            'http://',
            'https://'
        ), 
        '', 
        $url 
    );
}

function domain_to_user() {
    $ci = ci();

    $host = strip_domain_scheme($_SERVER['HTTP_HOST']);

    $user = $ci->db->select('u.id, u.agent_id, u.auth_type, domains AS domain_name, subdomain_url, is_ssl, status, mls_id')
                    ->from('domains d')
                    ->join('users u', 'u.id = d.agent', 'LEFT')
                    ->where('d.domains', $host)
                    ->or_where('d.subdomain_url', $host)
                    ->get()
                    ->row();

    if( isset($user) && isset($user->id) ) {
        if( !is_secure() && $user->is_ssl == 1 ) {
            $user_domain = $user->status == 'pending' ? 'http://' . strip_domain_scheme($user->subdomain_url) : 'https://www.' . strip_domain_scheme($user->domain_name);
            header("Location: $user_domain");
        }
    }

    return $user;
}

function leads_capture_config() {

  $ci = ci();
  $domain_user = domain_to_user();

  $ci->db->select("l.viewSite, l.searchSubmit, l.viewLIsting, l.minutesPass, l.countSearchPerf, l.countListingView, l.minute_capture, l.search_count, l.listing_view_count, l.allowSkip")
        ->from("leads_config l")
        ->join("users u", "u.id = l.user_id", "left")
        ->join("receipt r", "r.agent_id = l.user_id") // check if user is not freemium or trial
        ->where("l.user_id", $domain_user->id)
        ->where("u.capture_leads", 1) // check if user has enabled it's capture leads
        ->limit(1);

  $result = $ci->db->get()->row();

  return (!empty($result)) ? $result : FALSE;

}

if( !function_exists('get_agent_token') ) {

  function get_agent_token() {

    $ci = ci();
    $domain_user = domain_to_user();

    $ci->db->select('users_idx_token.access_token')
          ->from('users_idx_token')
          ->where('users_idx_token.agent_id', $domain_user->agent_id)
          ->limit(1);

    $result = $ci->db->get()->row();

    if(!empty($result)) {
      return $result;
    } else {

      $ci->db->select('user_bearer_token.bearer_token as access_token')
            ->from('user_bearer_token')
            ->where('user_bearer_token.agent_id', $domain_user->agent_id)
            ->limit(1);

      $result = $ci->db->get()->row();
      
      return (!empty($result)) ? $result : FALSE;

    }

    return FALSE;

  }

}

if( !function_exists('set_agent_cookies') ) {

  function set_agent_cookies() {

    $ci = ci();
    $domain_user = domain_to_user();

    if($ci->input->get('modal_welcome')) {

      $ci->db->select('agent_ip.agent_id')
              ->from('agent_ip')
              ->where('agent_ip.agent_id', $domain_user->agent_id)
              ->where('agent_ip.ip_address', $ci->input->ip_address())
              ->limit(1);

      $query = $ci->db->get()->row();
      
      if(!empty($query)) {

        if($ci->input->cookie('agent_ip_'.$domain_user->agent_id)) {

          $count = count($ci->input->cookie('agent_ip_'.$domain_user->agent_id))-1;

          if(!in_array($ci->input->ip_address(), $ci->input->cookie('agent_ip_'.$domain_user->agent_id))) {
            $count++;
            setcookie('agent_ip_'.$domain_user->agent_id."[".$count."]", $ci->input->ip_address(), time() + (86400 * 360), "/");
          }

        } else {

          setcookie('agent_ip_'.$domain_user->agent_id."[]", $ci->input->ip_address(), time() + (86400 * 360), "/");
        
        }
      
      }

    }

  }

}

if( !function_exists('agent_customer_cookie') ) {

  function agent_customer_cookie() {

    $ci = ci();
    $domain_user = domain_to_user();

    $ip_address=str_replace('.', '_', $ci->input->ip_address());

    if($ci->input->cookie('customer_'.$domain_user->id.'_'.$ip_address)) {
      
      $contact_id = $ci->input->cookie('customer_'.$domain_user->id.'_'.$ip_address);

      $ci->db->select('c.id, c.email')
            ->from('contacts c')
            ->where('c.user_id', $domain_user->id)
            ->where('c.agent_id', $domain_user->agent_id)
            ->where('c.contact_id', $contact_id)
            ->limit(1);

      $query = $ci->db->get()->row();

      if(!empty($query)) {

        $customer['customer_id']    = $query->id;
        $customer['customer_email'] = $query->email;
        $customer['contact_id']     = $contact_id;

        $ci->session->set_userdata($customer);

        return TRUE;

      }

    }

    return FALSE;

  }

}

if( !function_exists('isIDXOwner') ) {

  function isIDXOwner() {

    $ci = ci();
    $domain_user = domain_to_user();
    
    if($ci->input->cookie('agent_ip_'.$domain_user->agent_id)) {
      return (in_array($ci->input->ip_address(), $ci->input->cookie('agent_ip_'.$domain_user->agent_id))) ? TRUE : FALSE;
    }

    if($ci->input->get('modal_welcome')) {

      $ci->db->select('agent_ip.agent_id')
              ->from('agent_ip')
              ->where('agent_ip.agent_id', $domain_user->agent_id)
              ->where('agent_ip.ip_address', $ci->input->ip_address())
              ->limit(1);

      $query = $ci->db->get()->row();

      return (!empty($query)) ? TRUE : FALSE;

    }

    return FALSE;
    
  }

}

global $hooks;
$hooks = array();
if( !function_exists('hook')){
  /**
   * Hook into a function and execute a given function of your own.
   * 
   * @param string $name Name of the function to hook with.
   * @param function $function Function to execute on a given hook.
   */
  function hook($name, $function){
    global $hooks;
    $hooks[$name] = $function;
  }
}
if( !function_exists('add_hook')){
  /**
   * Adds a hook where you can execute functions outside or inside a theme folder
   * 
   * @param string $name Name of the function hook to execute.
   * @param mixed $args Arguments can be anything you want to pass to this function hook.
   * @param mixed $args2 Arguments can be anything you want to pass to this function hook.
   * @return mixed
   */
  function add_hook($name, $args = NULL, $args2 = NULL){
    global $hooks;
    if(array_key_exists($name, $hooks)){
      if($args !== NULL && $args !== NULL){
        return $hooks[$name]($args, $args2);
      }else if($args !== NULL && $args === NULL){
        return $hooks[$name]($args, $args2);
      }else{
        return $hooks[$name]();
      }
    }else{
      return $args;
    }
  }
}


if( !function_exists('html_cut')){

	function html_cut($text, $max_length){

    $tags   = array();
    $result = "";

    $is_open   = false;
    $grab_open = false;
    $is_close  = false;
    $in_double_quotes = false;
    $in_single_quotes = false;
    $tag = "";

    $i = 0;
    $stripped = 0;

    $stripped_text = strip_tags($text);

    while ($i < strlen($text) && $stripped < strlen($stripped_text) && $stripped < $max_length)
    {
        $symbol  = $text{$i};
        $result .= $symbol;

        switch ($symbol)
        {
           case '<':
                $is_open   = true;
                $grab_open = true;
                break;

           case '"':
               if ($in_double_quotes)
                   $in_double_quotes = false;
               else
                   $in_double_quotes = true;

            break;

            case "'":
              if ($in_single_quotes)
                  $in_single_quotes = false;
              else
                  $in_single_quotes = true;

            break;

            case '/':
                if ($is_open && !$in_double_quotes && !$in_single_quotes)
                {
                    $is_close  = true;
                    $is_open   = false;
                    $grab_open = false;
                }

                break;

            case ' ':
                if ($is_open)
                    $grab_open = false;
                else
                    $stripped++;

                break;

            case '>':
                if ($is_open)
                {
                    $is_open   = false;
                    $grab_open = false;
                    array_push($tags, $tag);
                    $tag = "";
                }
                else if ($is_close)
                {
                    $is_close = false;
                    array_pop($tags);
                    $tag = "";
                }

                break;

            default:
                if ($grab_open || $is_close)
                    $tag .= $symbol;

                if (!$is_open && !$is_close)
                    $stripped++;
        }

        $i++;
    }

    while ($tags)
        //$result .= "</".array_pop($tags).">";

    return $result;
}
}

/**
 * PHPMailer email validation
 * Check that a string looks like an email address.
 * Validation patterns supported:
 * * `auto` Pick best pattern automatically;
 * * `pcre8` Use the squiloople.com pattern, requires PCRE > 8.0;
 * * `pcre` Use old PCRE implementation;
 * * `php` Use PHP built-in FILTER_VALIDATE_EMAIL;
 * * `html5` Use the pattern given by the HTML5 spec for 'email' type form input elements.
 * * `noregex` Don't use a regex: super fast, really dumb.
 * Alternatively you may pass in a callable to inject your own validator, for example:
 *
 * ```php
 * validateAddress('user@example.com', function($address) {
 *     return (strpos($address, '@') !== false);
 * });
 * ```
 *
 * You can also set the PHPMailer::$validator static to a callable, allowing built-in methods to use your validator.
 *
 * @param string          $address       The email address to check
 * @param string|callable $patternselect Which pattern to use
 *
 * @return bool
 */
if ( ! function_exists('validateAddress') )
{
	/**
	 * @param string $address
	 * @param string $patternselect
	 * @return bool|mixed
	 */
	function validateAddress($address, $patternselect = 'php')
	{
		if (is_callable($patternselect)) {
			return call_user_func($patternselect, $address);
		}

		//Reject line breaks in addresses; it's valid RFC5322, but not RFC5321
		if (strpos($address, "\n") !== false || strpos($address, "\r") !== false) {
			return false;
		}

		switch ($patternselect) {
			case 'pcre': //Kept for BC
			case 'pcre8':
				/*
				 * A more complex and more permissive version of the RFC5322 regex on which FILTER_VALIDATE_EMAIL
				 * is based.
				 * In addition to the addresses allowed by filter_var, also permits:
				 *  * dotless domains: `a@b`
				 *  * comments: `1234 @ local(blah) .machine .example`
				 *  * quoted elements: `'"test blah"@example.org'`
				 *  * numeric TLDs: `a@b.123`
				 *  * unbracketed IPv4 literals: `a@192.168.0.1`
				 *  * IPv6 literals: 'first.last@[IPv6:a1::]'
				 * Not all of these will necessarily work for sending!
				 *
				 * @see       http://squiloople.com/2009/12/20/email-address-validation/
				 * @copyright 2009-2010 Michael Rushton
				 * Feel free to use and redistribute this code. But please keep this copyright notice.
				 */
				return (bool) preg_match(
					'/^(?!(?>(?1)"?(?>\\\[ -~]|[^"])"?(?1)){255,})(?!(?>(?1)"?(?>\\\[ -~]|[^"])"?(?1)){65,}@)' .
					'((?>(?>(?>((?>(?>(?>\x0D\x0A)?[\t ])+|(?>[\t ]*\x0D\x0A)?[\t ]+)?)(\((?>(?2)' .
					'(?>[\x01-\x08\x0B\x0C\x0E-\'*-\[\]-\x7F]|\\\[\x00-\x7F]|(?3)))*(?2)\)))+(?2))|(?2))?)' .
					'([!#-\'*+\/-9=?^-~-]+|"(?>(?2)(?>[\x01-\x08\x0B\x0C\x0E-!#-\[\]-\x7F]|\\\[\x00-\x7F]))*' .
					'(?2)")(?>(?1)\.(?1)(?4))*(?1)@(?!(?1)[a-z0-9-]{64,})(?1)(?>([a-z0-9](?>[a-z0-9-]*[a-z0-9])?)' .
					'(?>(?1)\.(?!(?1)[a-z0-9-]{64,})(?1)(?5)){0,126}|\[(?:(?>IPv6:(?>([a-f0-9]{1,4})(?>:(?6)){7}' .
					'|(?!(?:.*[a-f0-9][:\]]){8,})((?6)(?>:(?6)){0,6})?::(?7)?))|(?>(?>IPv6:(?>(?6)(?>:(?6)){5}:' .
					'|(?!(?:.*[a-f0-9]:){6,})(?8)?::(?>((?6)(?>:(?6)){0,4}):)?))?(25[0-5]|2[0-4][0-9]|1[0-9]{2}' .
					'|[1-9]?[0-9])(?>\.(?9)){3}))\])(?1)$/isD',
					$address
				);
			case 'html5':
				/*
				 * This is the pattern used in the HTML5 spec for validation of 'email' type form input elements.
				 *
				 * @see http://www.whatwg.org/specs/web-apps/current-work/#e-mail-state-(type=email)
				 */
				return (bool) preg_match(
					'/^[a-zA-Z0-9.!#$%&\'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}' .
					'[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/sD',
					$address
				);
			case 'php':
			default:
				return (bool) filter_var($address, FILTER_VALIDATE_EMAIL);
		}
	}
}


 /**
  * @param string $base_domain
  * @param string $url
  * @return string
  */
function get_social_media_link_uri($base_domain, $url = '')
{
    $url = ltrim(rtrim(rtrim($url, "\r\n"), '/'), '/');

    $url = preg_replace(
        '/^((http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/|www\.))?('.$base_domain.')?(\/.*)?$/i',
        '$4',
        $url
    );

    return ltrim($url, '/');
}

 /**
  * @param string $base_domain Social media domain name
  * @param string $uri         Social media profile name/URI
  * @return string
  */
function get_social_media_link_url($base_domain, $uri = '')
{
    if ( '' === $uri )
    {
        return $uri;
    }

    $uri = get_social_media_link_uri($base_domain, $uri);
    $uri = ltrim(rtrim(rtrim($uri, "\r\n"), '/'), '/');

    return "https://www.{$base_domain}/{$uri}/";
}



function sendgrid_curl( $url, $params = array(), $is_post = true ) {
    $headers = array(
      'Content-Type: application/json',
      'Authorization: Bearer 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',
    );
    $params = json_encode($params, true);
	
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		if ($is_post) {
    	curl_setopt($ch, CURLOPT_POST, true);
    	curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
		}
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_URL, $url);

    $result = curl_exec($ch);
		
    return $result;
}

if( !function_exists('gunzip')){

  function gunzip($zipped) {

    $offset = 0;
    if (substr($zipped,0,2) == "\x1f\x8b")
       $offset = 2;
    if (substr($zipped,$offset,1) == "\x08")  {
       return gzinflate(substr($zipped, $offset + 8));
    }
    return "Unknown Format";

  }
}

