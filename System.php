<?php
header('Content-type: application/json');
$system_info = array(
    'version' => trim(file_get_contents('version')),
    'build_num' => substr(exec('cat .git/HEAD | cut -d \  -f 2'), 0, 7)
);

echo json_encode($system_info);
?>
